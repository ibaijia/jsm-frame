package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.http.ann.HttpMapper;
import cn.ibaijia.jsm.http.ann.HttpMapping;
import cn.ibaijia.jsm.utils.ExceptionUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.apache.http.Header;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author longzl
 */
public class DefaultHttpMapperProcessorImpl extends AbstractHttpMapperProcessor {

    @Override
    public Object process(HttpMapper httpMapper, HttpMapping mapping, Method method, Object[] args) {
        boolean withExtra = mapping.withExtra() || httpMapper.withExtra();
        boolean allowAllSsl = mapping.allowAllSsl() || httpMapper.allowAllSsl();

        List<Header> headerList = getHeaderList(method, args);
        String url = formatUrl(mapping.uri(), mapping.baseUri(), httpMapper.baseUri(), method, args);
        Object requestData = getRequestData(method, args);
        HttpClientProcessor httpClientProcessor = findHttpClientProcessor(args);
        if (httpClientProcessor != null) {
            processByHttpClient(mapping, withExtra, allowAllSsl, headerList, url, requestData, httpClientProcessor);
            return null;
        }
        String res = null;
        if (mapping.useRestTemplate()) {
            res = getResultByRestTemplate(mapping, headerList, url, requestData);
        } else {
            res = getResultByHttpClient(mapping, withExtra, allowAllSsl, headerList, url, requestData);
        }
        logger.debug("res:{}", res);
        Object resultObj = null;
        if (!StringUtil.isEmpty(res) && !method.getReturnType().equals(String.class)) {
            logger.debug("parse to:{}", method.getGenericReturnType());
            resultObj = JsonUtil.parseObject(res, method.getGenericReturnType());
        } else {
            resultObj = res;
        }
        return resultObj;
    }

    private String getResultByRestTemplate(HttpMapping mapping, List<Header> headerList, String url, Object requestData) {
        RestTemplate restTemplate = SpringContext.getBean(RestTemplate.class);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (headerList != null && !headerList.isEmpty()) {
            for (Header header : headerList) {
                headers.add(header.getName(), header.getValue());
            }
        }
        HttpEntity<String> requestEntity = null;
        if (requestData == null) {
            requestEntity = new HttpEntity(headers);
        } else {
            requestEntity = new HttpEntity(JsonUtil.toJsonString(requestData), headers);
        }
        ResponseEntity<String> responseEntity = null;
        if (mapping.method().equals(RequestMethod.GET)) {
            responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
        } else if (mapping.method().equals(RequestMethod.POST)) {
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        } else if (mapping.method().equals(RequestMethod.PUT)) {
            responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);
        } else if (mapping.method().equals(RequestMethod.DELETE)) {
            responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, String.class);
        }
        if (responseEntity == null) {
            ExceptionUtil.failed(BasePairConstants.HTTP_URL_ERROR);
        }
        return responseEntity.getBody();
    }

}
