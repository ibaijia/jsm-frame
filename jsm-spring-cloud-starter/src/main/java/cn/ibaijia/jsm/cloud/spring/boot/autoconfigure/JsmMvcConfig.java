package cn.ibaijia.jsm.cloud.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.interceptor.JsmFilter;
import cn.ibaijia.jsm.context.interceptor.JsmHttpRequestInterceptor;
import cn.ibaijia.jsm.context.interceptor.JsmRestInterceptor;
import cn.ibaijia.jsm.context.interceptor.JsmTimeoutCallableProcessingInterceptor;
import cn.ibaijia.jsm.context.listener.JsmHttpSessionListener;
import cn.ibaijia.jsm.context.listener.JsmServletContextListener;
import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.DruidProperties;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author longzl
 */
@Configuration
@ComponentScan(basePackages = {
        "cn.ibaijia.jsm.cloud",
        })
@AutoConfigureAfter(AopAutoConfiguration.class)
public class JsmMvcConfig implements WebMvcConfigurer {

    protected Logger logger = LogUtil.log(getClass());

    @Bean
    public JsmHttpRequestInterceptor jsmHttpRequestInterceptor(){
        return new JsmHttpRequestInterceptor();
    }

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(Collections.singletonList(jsmHttpRequestInterceptor()));
        return restTemplate;
    }

}