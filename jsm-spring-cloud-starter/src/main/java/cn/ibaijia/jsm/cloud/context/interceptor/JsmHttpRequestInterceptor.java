package cn.ibaijia.jsm.cloud.context.interceptor;

import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import io.seata.core.context.RootContext;
import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * 使RestTemplate可以传header
 *
 * @Author: LongZL
 * @Date: 2022/9/2 19:03
 */
public class JsmHttpRequestInterceptor implements ClientHttpRequestInterceptor {
    private static Logger logger = LogUtil.log(JsmHttpRequestInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = request.getHeaders();
        logger.debug("{} url:{}", request.getMethodValue(),request.getURI());
        WebContext.checkAndSetHeaders(headers);
        String xid = RootContext.getXID();
        if (!StringUtil.isEmpty(xid)) {
            headers.add(RootContext.KEY_XID, xid);
        }
        return execution.execute(request, body);
    }
}
