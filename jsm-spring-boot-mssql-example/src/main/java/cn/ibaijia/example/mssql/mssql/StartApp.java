package cn.ibaijia.example.mssql.mssql;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan(basePackages = {"cn.ibaijia.example.mssql.mssql.dao.mapper"})
@SpringBootApplication(scanBasePackages = {"cn.ibaijia.example.mssql.mssql"})
@EnableScheduling()
public class StartApp {

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }

}
