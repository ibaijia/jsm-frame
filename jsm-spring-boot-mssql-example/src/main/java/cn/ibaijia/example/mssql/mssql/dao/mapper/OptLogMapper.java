package cn.ibaijia.example.mssql.mssql.dao.mapper;

import cn.ibaijia.jsm.context.dao.model.OptLog;
import cn.ibaijia.jsm.context.dao.model.Page;

import java.util.List;

public interface OptLogMapper {
	
	Long add(OptLog optLog);
	
	List<OptLog> pageList(Page<OptLog> page);

}
