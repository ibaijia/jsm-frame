package cn.ibaijia.example.mssql.mssql.consts;

import cn.ibaijia.jsm.consts.BasePermissionConstants;

public class PermissionConsts extends BasePermissionConstants {

	// 系统管理
	public static final String SYSTEM_MANAGEMENT = "SYSTEM_MANAGEMENT";
	//系统日志
	public static final String SYSTEM_LOG = "SYSTEM_LOG";
	//用户管理
	public static final String USER_MANAGEMENT = "USER_MANAGEMENT";
	//角色管理
	public static final String ROLE_MANAGEMENT = "ROLE_MANAGEMENT";

}
