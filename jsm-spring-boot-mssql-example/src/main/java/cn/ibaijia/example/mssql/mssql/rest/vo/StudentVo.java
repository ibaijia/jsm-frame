package cn.ibaijia.example.mssql.mssql.rest.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class StudentVo implements ValidateModel {
    @FieldAnn
    public String name;

    @FieldAnn
    public String studentCode;

    @FieldAnn
    public String studentNumber;
}
