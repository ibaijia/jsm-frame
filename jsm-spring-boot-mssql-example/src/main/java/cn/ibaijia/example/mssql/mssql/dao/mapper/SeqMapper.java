package cn.ibaijia.example.mssql.mssql.dao.mapper;

import cn.ibaijia.example.mssql.mssql.dao.model.Seq;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Update;
public interface SeqMapper {

    @Insert("<script>insert into seq_t (name,prefix,seqVal) values (#{name},#{prefix},#{seqVal})</script>")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int add(Seq seq);

    @Update("<script>update seq_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.prefix,prefix)'>,prefix=#{prefix}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.seqVal,seqVal)'>,seqVal=#{seqVal}</if> where `id`=#{id}</script>")
    int update(Seq seq);

    Seq findByName(@Param("name") String name);

}
