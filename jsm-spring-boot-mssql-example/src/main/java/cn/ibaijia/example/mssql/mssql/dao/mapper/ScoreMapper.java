package cn.ibaijia.example.mssql.mssql.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.example.mssql.mssql.dao.model.Score;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.example.mssql.mssql.rest.vo.score.*;
/**
 * tableName:score
 */
public interface ScoreMapper {

    /**
	 * 新增Score
	 */
    @Insert("<script>insert into score (course,score) values (#{course},#{score})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Score score);

    /**
	 * 批量新增Score
	 */
    @Insert("<script>insert into score (course,score) values <foreach collection='list' item='item' index='index' separator=','> (#{item.course},#{item.score})</foreach></script>")
	int addBatch(List<Score> scoreList);

	/**
	 * 修改Score(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update score set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.course,course)'>,course=#{course}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.score,score)'>,score=#{score}</if> where `id`=#{id}</script>")
	int update(Score score); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from score t where t.id=#{id}</script>")
	Score findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from score t where t.id=#{id}</script>")
	Score findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from score where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from score t order by t.id desc</script>")
	List<Score> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<Score> pageList(Page<Score> page);
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<ScoreVo> pageListVo(Page<ScoreVo> page);
}