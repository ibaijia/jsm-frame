package cn.ibaijia.example.mssql.mssql.rest.vo.score;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class ScoreVo implements ValidateModel {
    private static final long serialVersionUID = 1L;

    @FieldAnn(required = true, comments = "记录ID")
    public Long id;

	/**
	 * defaultVal:
	 * comments:课程
	 */
    @FieldAnn(required = true, maxLen = 20, comments = "课程")
	public String course;
	/**
	 * defaultVal:
	 * comments:分数
	 */
    @FieldAnn(required = true, comments = "分数")
	public Integer score;
}