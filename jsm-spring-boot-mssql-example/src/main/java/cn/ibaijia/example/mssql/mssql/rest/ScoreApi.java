package cn.ibaijia.example.mssql.mssql.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.example.mssql.mssql.service.ScoreService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import javax.annotation.Resource;
import cn.ibaijia.example.mssql.mssql.rest.req.score.*;
import cn.ibaijia.example.mssql.mssql.rest.vo.score.*;
import cn.ibaijia.example.mssql.mssql.dao.model.Score;

@RestController
@RequestMapping("/v1/score")
public class ScoreApi extends BaseRest {

    @Resource
    private ScoreService scoreService;

    @ApiOperation(value = "Score分页列表", notes = "")
    @RequestMapping(value = "/page-list", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<ScorePageReq<ScoreVo>> pageList(ScorePageReq<ScoreVo> req) {
        RestResp<ScorePageReq<ScoreVo>> resp = new RestResp<>();
        scoreService.pageList(req);
        resp.result = req;
        return resp;
    }

    @ApiOperation(value = "Score新增", notes = "")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> add(@RequestBody ScoreAddReq req) {
        RestResp<Integer> resp = new RestResp<>();
        resp.result = scoreService.add(req);
        return resp;
    }

    @ApiOperation(value = "Score详情", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<Score> findById(@PathVariable("id") Long id) {
        RestResp<Score> resp = new RestResp<>();
        resp.result = scoreService.findById(id);
        return resp;
    }

    @ApiOperation(value = "Score修改", notes = "")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> update(@RequestBody ScoreUpdateReq req) {
        RestResp<Integer> resp = new RestResp<>();
        resp.result = scoreService.update(req);
        return resp;
    }

    @ApiOperation(value = "Score删除", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> deleteById(@PathVariable("id") Long id) {
        RestResp<Integer> resp = new RestResp<>();
        resp.result = scoreService.deleteById(id);
        return resp;
    }

}