package cn.ibaijia.example.mssql.mssql.dao.model;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * tableName:score
 */
public class Score extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:
	 * comments:课程
	 */
    @FieldAnn(required = true, maxLen = 20, comments = "课程")
	public String course;
	/**
	 * defaultVal:
	 * comments:分数
	 */
    @FieldAnn(required = true, comments = "分数")
	public Integer score;
}