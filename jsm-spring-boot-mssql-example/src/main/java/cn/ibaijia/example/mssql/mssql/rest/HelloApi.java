package cn.ibaijia.example.mssql.mssql.rest;

import cn.ibaijia.example.mssql.mssql.dao.mapper.StudentMapper;
import cn.ibaijia.example.mssql.mssql.dao.model.Student;
import cn.ibaijia.example.mssql.mssql.rest.req.StudentReq;
import cn.ibaijia.example.mssql.mssql.rest.vo.StudentVo;
import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.BeanUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/v1")
public class HelloApi extends BaseRest {

    @Resource
    private StudentMapper studentMapper;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<String> hello() {
        RestResp<String> resp = new RestResp<String>();
        resp.result = "ok";
        try {
            Student student = new Student();
            student.name = "隆治利";
            student.score = "100";
            student.code = "200";
            studentMapper.add(student);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE,repeatCheck = false)
    public RestResp<StudentVo> testPost(@RequestBody StudentReq student) {
        StudentVo respVo = new StudentVo();
        BeanUtil.copy(student,respVo);
        return success(respVo);
    }

}
