package cn.ibaijia.example.mssql.mssql.service;

import cn.ibaijia.jsm.context.dao.model.OptLog;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.stat.JsmOptLogService;
import org.springframework.stereotype.Service;
import cn.ibaijia.example.mssql.mssql.dao.mapper.OptLogMapper;

import javax.annotation.Resource;

@Service
public class OptLogService implements JsmOptLogService {
    @Resource
    private OptLogMapper optLogMapper;

    @Override
    public Long add(OptLog optLog) {
        return optLogMapper.add(optLog);
    }

    @Override
    public void pageList(Page<OptLog> page) {
        optLogMapper.pageList(page);
    }
}
