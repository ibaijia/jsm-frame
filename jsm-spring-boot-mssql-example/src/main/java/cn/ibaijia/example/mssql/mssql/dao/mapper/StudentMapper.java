package cn.ibaijia.example.mssql.mssql.dao.mapper;

import cn.ibaijia.example.mssql.mssql.dao.model.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

public interface StudentMapper {

    @Insert("<script>insert into student (name,code) values (#{name},#{code})</script>")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int add(Student student);

}
