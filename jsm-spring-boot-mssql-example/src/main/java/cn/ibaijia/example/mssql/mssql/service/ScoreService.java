package cn.ibaijia.example.mssql.mssql.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import cn.ibaijia.example.mssql.mssql.dao.model.Score;
import cn.ibaijia.example.mssql.mssql.dao.mapper.ScoreMapper;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.example.mssql.mssql.rest.req.score.*;
import cn.ibaijia.example.mssql.mssql.rest.vo.score.*;
@Service
public class ScoreService extends BaseService {

    @Resource
    private ScoreMapper scoreMapper;
    public void pageList(ScorePageReq<ScoreVo> req) {
        //example,只使用了db model,mapper xml 也可使用vo对象
        scoreMapper.pageListVo(req);
    }

    public int add(ScoreAddReq req) {
        Score score = new Score();
        BeanUtil.copy(req, score);
        return scoreMapper.add(score);
    }

    public int update(ScoreUpdateReq req) {
        Score dbScore = scoreMapper.findByIdForUpdate(req.id);
        if ( dbScore == null ) {
            notFound("id not found:" + req.id);
        }
        BeanUtil.copy(req, dbScore);
        return scoreMapper.update(dbScore);
    }
    public int deleteById(Long id) {
        return scoreMapper.deleteById(id);
    }

    public Score findById(Long id) {
        Score dbScore = scoreMapper.findById(id);
        if ( dbScore == null ) {
            notFound("id not found:" + id);
        }
        return dbScore;
    }

}