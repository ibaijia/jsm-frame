package cn.ibaijia.example.mssql.mssql.rest.req.score;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

public class ScoreAddReq implements ValidateModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:
	 * comments:课程
	 */
    @FieldAnn(required = true, maxLen = 20, comments = "课程")
	public String course;
	/**
	 * defaultVal:
	 * comments:分数
	 */
    @FieldAnn(required = true,  comments = "分数")
	public Integer score;
}