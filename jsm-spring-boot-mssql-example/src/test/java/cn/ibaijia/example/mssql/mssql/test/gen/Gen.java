package cn.ibaijia.example.mssql.mssql.test.gen;

import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.gen.GenService;
import cn.ibaijia.jsm.gen.DefaultMsSqlGenerationStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Gen {

    @Resource
    private GenService genService;

    @Test
    public void gen() {
        JsmConfigurer.setDbGenerationStrategy(new DefaultMsSqlGenerationStrategy());
        genService.gen("score", true,true);
    }

}
