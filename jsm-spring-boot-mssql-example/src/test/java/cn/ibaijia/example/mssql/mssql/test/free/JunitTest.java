package cn.ibaijia.example.mssql.mssql.test.free;

import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.junit.Test;

public class JunitTest {

    @Test
    public void testRemoveAsList(){
        String str = "1,2,3,4";
        System.out.println(StringUtil.removeAsList(str,',',"3"));

//        System.out.println(PropertyNamingStrategy.valueOf("SnakeCase").translate("studentNumber"));
//        System.out.println(((com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy)com.fasterxml.jackson.databind.PropertyNamingStrategy.SNAKE_CASE).translate("studentNumber"));
        String field = "studentNumberPer";
        System.out.println(JsonUtil.getStrategyName(field, PropertyNamingStrategy.LOWER_CAMEL_CASE));
        System.out.println(JsonUtil.getStrategyName(field, PropertyNamingStrategy.UPPER_CAMEL_CASE));
        System.out.println(JsonUtil.getStrategyName(field, PropertyNamingStrategy.SNAKE_CASE));
        System.out.println(JsonUtil.getStrategyName(field, PropertyNamingStrategy.KEBAB_CASE));
        System.out.println(JsonUtil.getStrategyName(field, PropertyNamingStrategy.LOWER_CASE));

    }

}
