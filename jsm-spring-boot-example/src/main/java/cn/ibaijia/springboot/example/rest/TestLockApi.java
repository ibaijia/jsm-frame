package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.context.lock.LockService;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-lock")
public class TestLockApi extends BaseRest {

    @Resource
    private LockService lockService;

    @Resource
    private JedisService jedisService;

    @RequestMapping(value = "/lock", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> lock() {
        String key = "abc";
        boolean lock = lockService.lock(key);
        logger.info("lock:{}",lock);
        if(lock){
            try {
                Thread.sleep(1000*60*5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            boolean res = lockService.unlock(key);
            logger.info("unlock:{}",res);
        }
        return success(lock);
    }


    @RequestMapping(value = "/unlock", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> unlock() {
        String key = "abc";
        boolean res = lockService.unlock(key);
        logger.info("unlock:{}",res);
        return success(true);
    }

}
