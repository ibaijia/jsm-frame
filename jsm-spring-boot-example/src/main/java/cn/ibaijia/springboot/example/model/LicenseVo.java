package cn.ibaijia.springboot.example.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class LicenseVo implements ValidateModel {

    @FieldAnn(required = false, maxLen = 50, comments = "名称")
    public String name;

    @FieldAnn(required = false, maxLen = 250, comments = "说明")
    public String comments;

    @FieldAnn(required = false, maxLen = 30, comments = "客户端ID")
    public String clientId;

    @FieldAnn(required = false, maxLen = 50, comments = "客户端KEY")
    public String clientKey;

    @FieldAnn(required = false, comments = "开始时间")
    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    public Date beginTime;

    @FieldAnn(required = false, comments = "结束时间")
    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    public Date endTime;

    @FieldAnn(required = false, maxLen = 250, comments = "授权数据")
    public String authData;

}