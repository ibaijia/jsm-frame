package cn.ibaijia.springboot.example.job;


import cn.ibaijia.jsm.annotation.ClusterJobLockAnn;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author longzl
 */
@Component
public class TestJob {
    private Logger logger = LogUtil.log(TestJob.class);

    public TestJob() {
        logger.info("TestJob create............");
    }

    @ClusterJobLockAnn
    @Scheduled(cron = "1/5 * * * * ?")
    public void execute() {
        logger.info("TestJob:: begin.");
        logger.info("TestJob:: end.");
    }
}
