package cn.ibaijia.springboot.example.dao.model;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * tableName:action_t
 */
public class Action extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:服务ID
	 */
    @FieldAnn(required = false, comments = "服务ID")
	public Long serviceId;
	/**
	 * defaultVal:NULL
	 * comments:名称
	 */
    @FieldAnn(required = false, maxLen = 100, comments = "名称")
	public String name;
}