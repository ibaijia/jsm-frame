package cn.ibaijia.springboot.example.job;


import cn.ibaijia.jsm.annotation.ClusterJobLockAnn;
import cn.ibaijia.jsm.utils.LogUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;

/**
 * @author longzl
 */
public class TestDynamicAddedJob implements Job {

    private Logger logger = LogUtil.log(TestDynamicAddedJob.class);

    @Override
    @ClusterJobLockAnn
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("TestDynamicAddedJob:: begin.");
        logger.info("TestDynamicAddedJob:: end.");
    }
}
