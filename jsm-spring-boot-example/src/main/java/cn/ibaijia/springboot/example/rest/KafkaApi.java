//package cn.ibaijia.springboot.example.rest;
//
//import cn.ibaijia.jsm.annotation.AuthType;
//import cn.ibaijia.jsm.annotation.RestAnn;
//import cn.ibaijia.jsm.annotation.Transaction;
//import cn.ibaijia.jsm.base.BaseRest;
//import cn.ibaijia.jsm.rest.resp.RestResp;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.SendResult;
//import org.springframework.util.concurrent.ListenableFuture;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//
//@RestController
//@RequestMapping("/v1")
//public class KafkaApi extends BaseRest {
//
//    @Resource
//    private KafkaTemplate<String, String> kafkaTemplate;
//
//    @RequestMapping(value = "/kafka-produce/{content}", method = RequestMethod.GET)
//    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
//    public RestResp<String> hello(@PathVariable("content") String content) {
//        RestResp<String> resp = new RestResp<>();
//        ListenableFuture<SendResult<String, String>> res = kafkaTemplate.send("testTopic", content);
//        SendResult<String, String> r = null;
//        try {
//            r = res.get();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        resp.result = r.toString();
//        return resp;
//    }
//
//
//}
