package cn.ibaijia.springboot.example.rest.req.action;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

public class ActionAddReq implements ValidateModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:服务ID
	 */
    @FieldAnn(required = false,  comments = "服务ID")
	public Long serviceId;
	/**
	 * defaultVal:NULL
	 * comments:名称
	 */
    @FieldAnn(required = false, maxLen = 100, comments = "名称")
	public String name;
}