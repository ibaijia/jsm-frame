package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1")
public class HelloApi extends BaseRest {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<String> hello() {
        return success(WebContext.getRemoteIp());
    }

}
