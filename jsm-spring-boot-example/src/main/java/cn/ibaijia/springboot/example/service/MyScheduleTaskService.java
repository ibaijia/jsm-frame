package cn.ibaijia.springboot.example.service;

import cn.ibaijia.jsm.context.job.ScheduleTaskService;
import cn.ibaijia.jsm.utils.QuartzUtil;
import cn.ibaijia.springboot.example.job.TestDynamicAddedJob;

/**
 * @author longzl
 */
public class MyScheduleTaskService implements ScheduleTaskService {

    @Override
    public void loadTasks() {
        QuartzUtil.addJob(TestDynamicAddedJob.class, "1/5 * * * * ?");
        QuartzUtil.startJobs();
    }

    @Override
    public void destroy() {
        QuartzUtil.shutdownJobs();
    }
}
