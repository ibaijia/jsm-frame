package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.springboot.example.dao.model.User;
import cn.ibaijia.springboot.example.rest.req.user.UserAddReq;
import cn.ibaijia.springboot.example.rest.req.user.UserPageReq;
import cn.ibaijia.springboot.example.rest.req.user.UserUpdateReq;
import cn.ibaijia.springboot.example.rest.vo.user.UserVo;
import cn.ibaijia.springboot.example.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/user")
public class UserApi extends BaseRest {

    @Resource
    private UserService userService;

    @ApiOperation(value = "User列表", notes = "test jsm-mock")
    @RequestMapping(value = "/list1", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<List<UserVo>> list1() {
        return success(null);
    }

    @ApiOperation(value = "User列表", notes = "test jsm-mock")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<List<List<UserVo>>> users() {
        RestResp<List<List<UserVo>>> resp = new RestResp<>();
        return success(null);
    }

    @ApiOperation(value = "User分页列表", notes = "")
    @RequestMapping(value = "/page-list", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<UserPageReq<UserVo>> pageList(UserPageReq<UserVo> req) {
        userService.pageList(req);
        return success(req);
    }

    @ApiOperation(value = "User新增", notes = "")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> add(@RequestBody UserAddReq req) {
        Integer result = userService.add(req);
        return success(result);
    }

    @ApiOperation(value = "User详情", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<User> findById(@PathVariable("id") Long id) {
        User result = userService.findById(id);
        return success(result);
    }

    @ApiOperation(value = "User修改", notes = "")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> update(@RequestBody UserUpdateReq req) {
        Integer result = userService.update(req);
        return success(result);
    }

    @ApiOperation(value = "User删除", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> deleteById(@PathVariable("id") Long id) {
        Integer result = userService.deleteById(id);
        return success(result);
    }

}