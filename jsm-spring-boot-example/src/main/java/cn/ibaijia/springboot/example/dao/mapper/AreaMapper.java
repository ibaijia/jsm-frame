package cn.ibaijia.springboot.example.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.springboot.example.dao.model.Area;
import cn.ibaijia.jsm.context.dao.model.Page;

/**
 * tableName:area_t
 */
public interface AreaMapper {

    /**
	 * 新增Area
	 */
    @Insert("<script>insert into area_t (name,parentId,level,idx) values (#{name},#{parentId},#{level},#{idx})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Area area);

    /**
	 * 批量新增Area
	 */
    @Insert("<script>insert into area_t (name,parentId,level,idx) values <foreach collection='list' item='item' index='index' separator=','> (#{item.name},#{item.parentId},#{item.level},#{item.idx})</foreach></script>")
	int addBatch(List<Area> areaList);

	/**
	 * 修改Area(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update area_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.parentId,parentId)'>,parentId=#{parentId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.level,level)'>,level=#{level}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.idx,idx)'>,idx=#{idx}</if> where `id`=#{id}</script>")
	int update(Area area); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from area_t t where t.id=#{id}</script>")
	Area findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from area_t t where t.id=#{id}</script>")
	Area findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from area_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from area_t t order by t.id desc</script>")
	List<Area> listAll();
	
	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<Area> pageList(Page<Area> page);
	
}