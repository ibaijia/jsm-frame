package cn.ibaijia.springboot.example.rest.vo.user;

import cn.ibaijia.jsm.excel.ExcelField;

/**
 * @author longzl
 */
public class UserExportVo {

    @ExcelField(name = "记录ID", width = 20)
    public Long id;

    /**
     * defaultVal:NULL
     * comments:用户名
     */
    @ExcelField(name = "用户名", width = 20)
    public String username;
    /**
     * defaultVal:NULL
     * comments:密码
     */
    @ExcelField(ignore = true)
    public String password;
    /**
     * defaultVal:NULL
     * comments:手势密码
     */
    @ExcelField(ignore = true)
    public String gesturePwd;
    /**
     * defaultVal:NULL
     * comments:状态 -1 删除 1可用 2 禁用
     */
    @ExcelField(name = "状态", replaceRule = "删除|-1;可用|1;禁用|2")
    public Integer status;
}