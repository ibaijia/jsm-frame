package cn.ibaijia.springboot.example;

import cn.ibaijia.jsm.http.ann.HttpMapperScan;
import cn.ibaijia.jsm.utils.LogUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author longzl
 */
@HttpMapperScan(basePackages = {"cn.ibaijia.springboot.example.httpMapper"})
@MapperScan(basePackages={"cn.ibaijia.springboot.example.dao.mapper"})
@SpringBootApplication(exclude = {QuartzAutoConfiguration.class},scanBasePackages = "cn.ibaijia.springboot.example")
@EnableScheduling
public class StartApp {

    private static Logger logger = LogUtil.log(StartApp.class);

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
        logger.info("start ok.");
    }

}
