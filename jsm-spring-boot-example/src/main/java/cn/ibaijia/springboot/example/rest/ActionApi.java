package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.springboot.example.dao.model.Action;
import cn.ibaijia.springboot.example.rest.req.action.ActionAddReq;
import cn.ibaijia.springboot.example.rest.req.action.ActionPageReq;
import cn.ibaijia.springboot.example.rest.req.action.ActionUpdateReq;
import cn.ibaijia.springboot.example.rest.vo.action.ActionVo;
import cn.ibaijia.springboot.example.service.ActionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/action")
public class ActionApi extends BaseRest {

    @Resource
    private ActionService actionService;

    @ApiOperation(value = "Action分页列表", notes = "")
    @RequestMapping(value = "/page-list", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<ActionPageReq<ActionVo>> pageList(ActionPageReq<ActionVo> req) {
        actionService.pageList(req);
        return success(req);
    }

    @ApiOperation(value = "Action新增", notes = "")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> add(@RequestBody ActionAddReq req) {
        Integer result = actionService.add(req);
        return success(result);
    }

    @ApiOperation(value = "Action详情", notes = "")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<Action> find(@PathVariable("id") Long id) {
        Action result = actionService.findById(id);
        return success(result);
    }

    @ApiOperation(value = "Action修改", notes = "")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> update(@RequestBody ActionUpdateReq req) {
        Integer result = actionService.update(req);
        return success(result);
    }

    @ApiOperation(value = "Action删除", notes = "")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> deleteById(@PathVariable("id") Long id) {
        Integer result = actionService.deleteById(id);
        return success(result);
    }

}