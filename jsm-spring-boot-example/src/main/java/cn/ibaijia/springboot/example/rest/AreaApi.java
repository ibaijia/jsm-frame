package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.springboot.example.service.AreaService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import javax.annotation.Resource;
import cn.ibaijia.springboot.example.rest.req.area.*;
import cn.ibaijia.springboot.example.dao.model.Area;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/area")
public class AreaApi extends BaseRest {

    @Resource
    private AreaService areaService;

    @ApiOperation(value = "Area分页列表", notes = "")
    @RequestMapping(value = "/page-list", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<AreaPageReq<Area>> pageList(AreaPageReq<Area> req) {
        areaService.pageList(req);
        return success(req);
    }

    @ApiOperation(value = "Area新增", notes = "")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> add(@RequestBody AreaAddReq req) {
        Integer result = areaService.add(req);
        return success(result);
    }

    @ApiOperation(value = "Area修改", notes = "")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> update(@RequestBody AreaUpdateReq req) {
        Integer result = areaService.update(req);
        return success(result);
    }

    @ApiOperation(value = "Area删除", notes = "")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> deleteById(@PathVariable("id") Long id) {
        Integer result = areaService.deleteById(id);
        return success(result);
    }

}