package cn.ibaijia.springboot.example.rest.req.area;
import cn.ibaijia.jsm.context.dao.model.Page;

public class AreaPageReq<T> extends Page<T> {
    private static final long serialVersionUID = 1L;

	/**
	 * 这里定义的字段需要get/set方法才有效
     * @FieldAnn(required = false, maxLen = 100, comments = "名称")
     * public String name;
	 */

}