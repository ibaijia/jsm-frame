package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.oauth.gateway.OauthContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-oauth")
public class TestOauthApi extends BaseRest {

    @RequestMapping(value = "/gateway-auth", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.JSM_GATEWAY, transaction = Transaction.NONE)
    public RestResp<Boolean> gateway() {
        logger.info(OauthContext.getVerifyResult());
        return success(true);
    }

}
