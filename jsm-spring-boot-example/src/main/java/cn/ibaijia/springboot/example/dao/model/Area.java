package cn.ibaijia.springboot.example.dao.model;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * tableName:area_t
 */
public class Area extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:名称
	 */
    @FieldAnn(required = false, maxLen = 50, comments = "名称")
	public String name;
	/**
	 * defaultVal:NULL
	 * comments:parentId
	 */
    @FieldAnn(required = false, comments = "parentId")
	public Long parentId;
	/**
	 * defaultVal:NULL
	 * comments:level 0 国家 1 省/直辖市 2 市/区 3 县 4 镇 
	 */
    @FieldAnn(required = false, comments = "level 0 国家 1 省/直辖市 2 市/区 3 县 4 镇 ")
	public Integer level;
	/**
	 * defaultVal:NULL
	 * comments:排序 同级内排序
	 */
    @FieldAnn(required = false, comments = "排序 同级内排序")
	public Integer idx;
}