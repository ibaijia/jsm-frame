package cn.ibaijia.springboot.example.consts;

import cn.ibaijia.jsm.consts.BasePermissionConstants;

/**
* @author jsm_auto_gen
*/
public class Permissions extends BasePermissionConstants {

    /**
    * 系统日志
    */
    public static final String OPT_LOG = "OPT_LOG";
    /**
    * 邮件配置
    */
    public static final String MAIL_CONFIG = "MAIL_CONFIG";
    /**
    * 用户列表
    */
    public static final String USER_MANAGE = "USER_MANAGE";
    /**
    * 角色列表
    */
    public static final String ROLE_MANAGE = "ROLE_MANAGE";

}