package cn.ibaijia.springboot.example.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import cn.ibaijia.springboot.example.dao.model.Area;
import cn.ibaijia.springboot.example.dao.mapper.AreaMapper;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.springboot.example.rest.req.area.*;

@Service
public class AreaService extends BaseService {

    @Resource
    private AreaMapper areaMapper;

    public void pageList(AreaPageReq req) {
        //example,只使用了db model,mapper xml 也可使用vo对象
        areaMapper.pageList(req);
    }

    public int add(AreaAddReq req) {
        Area area = new Area();
        BeanUtil.copy(req, area);
        return areaMapper.add(area);
    }

    public int update(AreaUpdateReq req) {
        Area dbArea = areaMapper.findByIdForUpdate(req.id);
        if ( dbArea == null ) {
            notFound("id not found:" + req.id);
        }
        BeanUtil.copy(req, dbArea);
        return areaMapper.update(dbArea);
    }

    public int deleteById(Long id) {
        return areaMapper.deleteById(id);
    }

}