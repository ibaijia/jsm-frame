//package cn.ibaijia.springboot.example.service;
//
//import cn.ibaijia.jsm.base.BaseService;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.support.Acknowledgment;
//import org.springframework.stereotype.Service;
//
//@Service
//public class KafkaConsumerService extends BaseService {
//
//    @KafkaListener(topics = {"testTopic"})
//    public void listen(ConsumerRecord<?, ?> record) {
//        String value = (String) record.value();
//        logger.info("testTopic recv:" + value);
//    }
//
//    @KafkaListener(topics = {"jsmLog"})
//    public void listenLog(ConsumerRecord<?, ?> record, Acknowledgment ack) {
//        String value = (String) record.value();
//        System.out.println("listenLog recv:" + value);
//        ack.acknowledge();
//    }
//
//}
