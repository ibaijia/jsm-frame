package cn.ibaijia.springboot.example.dao.mapper;

import cn.ibaijia.jsm.context.dao.model.OptLog;
import cn.ibaijia.jsm.context.dao.model.Page;

import java.util.List;

public interface OptLogMapper {

	Long add(OptLog optLog);

	List<OptLog> pageList(Page<OptLog> page);

    List<OptLog> listAll();
}
