package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.springboot.example.rest.vo.user.StudentInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-cache/")
public class TestCacheApi extends BaseRest {

    @Resource
    private CacheService cacheService;
    @Resource
    private JedisService jedisService;

    @RequestMapping(value = "/set", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<String> testSet() {
        RestResp<String> resp = new RestResp<>();
        cacheService.set("testKey",30,"testValue");
        resp.result = cacheService.get("testKey",String.class);
        return resp;
    }

    @RequestMapping(value = "/dic-ann", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<StudentInfo> dicAnn() {
        RestResp<StudentInfo> resp = new RestResp<>();

        cacheService.hset("dic.user","1","longzl1");
        cacheService.hset("dic.user","2","admin1");

        StudentInfo studentInfo = new StudentInfo(1L, 2L, 3L);
        studentInfo.createBy = 1L;
        studentInfo.modifyBy = 2L;

        studentInfo.users = new ArrayList<>();
        studentInfo.users.add(1L);
        studentInfo.users.add(2L);
        studentInfo.users.add(3L);

        studentInfo.users2 = new ArrayList<>();
        studentInfo.users2.add(1L);
        studentInfo.users2.add(2L);
        studentInfo.users2.add(3L);

        studentInfo.users3[0] = (1L);
        studentInfo.users3[1] = (2L);
        studentInfo.users3[2] = (3L);

        resp.result = studentInfo;
        return resp;
    }

    @RequestMapping(value = "/dic-ann2", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<StudentInfo> dicAnn2() {
        StudentInfo studentInfo = new StudentInfo(1L, 2L, 3L);
        studentInfo.createBy = 1L;
        studentInfo.modifyBy = 2L;
        return success(studentInfo);
    }



}
