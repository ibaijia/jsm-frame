package cn.ibaijia.springboot.example.httpMapper;

import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.http.ann.*;
import cn.ibaijia.springboot.example.rest.req.area.AreaAddReq;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: LongZL
 * @Date: 2022/6/20 11:02
 */
@HttpMapper(baseUri = "${service.baseUrl}")
public interface TestHttpMapper {

    @HttpMapping(method = RequestMethod.GET, uri = "/v1/test-get")
    public RestResp<String> testGet();

    /**
     * 访问url /v1/test-get/xxx?a=xxx
     * @param name1
     * @param name2
     * @return
     */
    @HttpMapping(method = RequestMethod.GET, uri = "/v1/test-get/#{name1}?a=#{name2}")
    public RestResp<String> testGetVariable(@HttpPathVariable("name1") String name1, @HttpPathVariable("name2") String name2);

    /**
     * 访问url /v1/test-get?name1=xxx&name2=xxx
     * @param name1
     * @param name2
     * @return
     */
    @HttpMapping(method = RequestMethod.GET, uri = "/v1/test-get")
    public RestResp<String> testGetParam(@HttpParam("name1") String name1, @HttpParam("name2") String name2);

    /**
     * Post请求
     * @param req
     * @return
     */
    @HttpMapping(method = RequestMethod.POST, uri = "/v1/test-post")
    public RestResp<String> testPost(AreaAddReq req);

    /**
     * /v1/test-post/xxx
     * @param name2
     * @param req
     * @return
     */
    @HttpPostMapping(uri = "/v1/test-post/#{name}")
    public RestResp<String> testPostVariable(@HttpPathVariable("name") String name2, AreaAddReq req);

    /**
     * header
     * name:xxx
     *
     * HttpHeader 支持List<Header> 或者 Set<Header>
     *
     * @param name
     * @param req
     * @return
     */
    @HttpPostMapping(uri = "/v1/test-post")
    public RestResp<String> testPostHeader(@HttpHeader("name") String name, AreaAddReq req);

}
