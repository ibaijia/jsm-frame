package cn.ibaijia.springboot.example.rest.vo.user;

import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.excel.ExcelField;

import java.util.List;

/**
 * @author longzl
 */
public class StudentInfo implements ValidateModel {
    private static final long serialVersionUID = 1L;

    public StudentInfo(Long scoreA, Long scoreB, Long total) {
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.total = total;
    }

    @ExcelField(name = "scoreA")
    @FieldAnn(required = false)
    public Long scoreA;

    @ExcelField(name = "scoreB")
    @FieldAnn(required = false)
    public Long scoreB;

    @ExcelField(name = "total")
    @FieldAnn(required = false)
    public Long total;

    @FieldAnn(required = false)
    @DicAnn(name = "dic.user")
    public Long createBy;

    @FieldAnn(required = false)
    @DicAnn(name = "dic.user")
    public Long modifyBy;

    @FieldAnn(required = false, mockRule = "1-2", mockValue = "[\"a\",\"b\",\"c\"]")
    @DicAnn(name = "dic.user")
    public List<Long> users;

    @FieldAnn(required = false)
    @DicAnn(name = "dic.user", toDicKV = true, dicKeyNameComment = "用户ID", dicValueNameComment = "用户名")
    public List<Long> users2;

    @FieldAnn(required = false, mockValue = "[{\"a\":\"b\"}]")
    @DicAnn(name = "dic.user", toDicKV = true, dicKeyNameComment = "用户ID", dicValueNameComment = "用户名")
    public Long[] users3 = new Long[3];

    public String name3;

    public Boolean gender;

    public boolean gender2;

    public Integer age;

    public int age2;

    public List<String> stuList;

    public String[] stuList2;


}