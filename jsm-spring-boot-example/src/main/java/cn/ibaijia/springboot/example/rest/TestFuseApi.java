package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.RestFuseAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-fuse/")
public class TestFuseApi extends BaseRest {

    /**
     * 异常里调用 helloError
     * @param name
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RestFuseAnn(fallbackMethod = "helloError")
    public RestResp<String> hello(@RequestParam("name") String name) {
        throw new RuntimeException("my error.");
//        return success("hello");
    }

    public RestResp<String> helloError(@RequestParam("name") String name) {
        return success("helloError");
    }

}
