package cn.ibaijia.springboot.example.rest.req.user;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class UserAddReq implements ValidateModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:用户名
	 */
    @FieldAnn(required = false, maxLen = 50, comments = "用户名")
	public String username;
	/**
	 * defaultVal:NULL
	 * comments:密码
	 */
    @FieldAnn(required = false, maxLen = 50, comments = "密码")
	public String password;
	/**
	 * defaultVal:NULL
	 * comments:手势密码
	 */
    @FieldAnn(required = false, maxLen = 50, comments = "手势密码")
	public String gesturePwd;
	/**
	 * defaultVal:NULL
	 * comments:状态 -1 删除 1可用 2 禁用
	 */
    @FieldAnn(required = false,  comments = "状态 -1 删除 1可用 2 禁用")
	public Integer status;
}