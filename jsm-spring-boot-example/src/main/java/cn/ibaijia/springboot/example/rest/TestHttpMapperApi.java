package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.springboot.example.httpMapper.TestHttpMapper;
import cn.ibaijia.springboot.example.rest.req.area.AreaAddReq;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-hcs/")
public class TestHttpMapperApi extends BaseRest {

    @Resource
    private TestHttpMapper testHttpMapper;

    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public RestResp<String> get() {
        RestResp<String> res = testHttpMapper.testGetParam("longzl");
        return res;
    }

    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/post", method = RequestMethod.GET)
    public RestResp<String> post() {
        AreaAddReq areaAddReq = new AreaAddReq();
        areaAddReq.name = "cqaqq";
        RestResp<String> res = testHttpMapper.testPostParam("longzl22",areaAddReq);
        return res;
    }

}
