package cn.ibaijia.springboot.example.service;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.elastic.ElasticQueryService;
import cn.ibaijia.jsm.elastic.ElasticService;
import cn.ibaijia.jsm.elastic.query.Builder;
import cn.ibaijia.jsm.elastic.query.SortCondition;
import cn.ibaijia.springboot.example.rest.vo.user.StudentInfo;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author longzl
 */
@Service
public class MyEsService extends BaseService {
//    @Resource
//    private ElasticConfig elasticConfig;

    /**
     * 可创建和查询 功能比较全
     */
    private ElasticService<StudentInfo> elasticService;

    /**
     * 只查询的Service,更量级不需要传入表的Mappings
     */
    private ElasticQueryService elasticQueryService;

    @PostConstruct
    private void init() {
//        String address = elasticConfig.getAddress();
//        String idx = elasticConfig.getSysStatIdx();
//        elasticService = new ElasticService(address, idx, jsonOption);
//        elasticQueryService = new ElasticQueryService(address, idx);
    }

    public List<StudentInfo> list() {
        Builder builder = new Builder();
        builder.addSort(new SortCondition("time", false));
//        builder.bool()
//                .must()
//                .term("env", elasticConfig.getEnv())
//                .term("appName", appName)
//                .term("name", name)
//                .range("time", RangeOperator.GT, startTime.getTime())
//                .range("time", RangeOperator.LT, endTime.getTime());

        String queryJson = builder.build();
        logger.info("queryJson:{}", queryJson);
        List<StudentInfo> list = elasticQueryService.list(queryJson);
        return list;
    }
}
