package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.rest.resp.vo.ExcelVo;
import cn.ibaijia.jsm.excel.ExcelUtil;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.springboot.example.dao.model.User;
import cn.ibaijia.springboot.example.rest.req.user.UserImportModel;
import cn.ibaijia.springboot.example.rest.vo.user.UserExportVo;
import cn.ibaijia.springboot.example.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/1/26 9:37
 */
@RestController
@RequestMapping("/v1/test-excel/")
public class TestExcelApi extends BaseRest {


    @Resource
    private UserService userService;

    @ApiOperation(value = "User导出", notes = "")
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<ExcelVo> exportUser() {
        List<UserExportVo> list = new ArrayList<>();
        List<User> allUser = userService.findAll();
        for (User user : allUser) {
            UserExportVo userExportVo = new UserExportVo();
            BeanUtil.copy(user, userExportVo);
            list.add(userExportVo);
        }
        ExcelVo excelVo = new ExcelVo();
        excelVo.workbook = ExcelUtil.writeToNew(list);
        excelVo.fileName = "所有用户信息.xlsx";
        return success(excelVo);
    }

    @ApiOperation(value = "User导入", notes = "")
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<Integer> importUser(@RequestParam("file") MultipartFile file) {
        List<UserImportModel> list = ExcelUtil.readExcel(file, 0, UserImportModel.class, 1);
        logger.info("import user list:{}", JsonUtil.toJsonString(list));
        return success(list.size());
    }

}
