package cn.ibaijia.springboot.example.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.springboot.example.dao.model.Action;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.springboot.example.rest.vo.action.*;

/**
 * tableName:action_t
 */
public interface ActionMapper {

    /**
	 * 新增Action
	 */
    @Insert("<script>insert into action_t (serviceId,name) values (#{serviceId},#{name})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Action action);

    /**
	 * 批量新增Action
	 */
    @Insert("<script>insert into action_t (serviceId,name) values <foreach collection='list' item='item' index='index' separator=','> (#{item.serviceId},#{item.name})</foreach></script>")
	int addBatch(List<Action> actionList);

	/**
	 * 修改Action(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update action_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.serviceId,serviceId)'>,serviceId=#{serviceId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if> where `id`=#{id}</script>")
	int update(Action action); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from action_t t where t.id=#{id}</script>")
	Action findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from action_t t where t.id=#{id}</script>")
	Action findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from action_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from action_t t order by t.id desc</script>")
	List<Action> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<Action> pageList(Page<Action> page);

	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<Action> pageListVo(Page<ActionVo> page);

}