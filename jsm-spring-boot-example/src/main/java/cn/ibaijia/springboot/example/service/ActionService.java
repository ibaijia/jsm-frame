package cn.ibaijia.springboot.example.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import cn.ibaijia.springboot.example.dao.model.Action;
import cn.ibaijia.springboot.example.dao.mapper.ActionMapper;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.springboot.example.rest.req.action.*;
import cn.ibaijia.springboot.example.rest.vo.action.*;

@Service
public class ActionService extends BaseService {

    @Resource
    private ActionMapper actionMapper;

    public void pageList(ActionPageReq<ActionVo> req) {
        //example,只使用了db model,mapper xml 也可使用vo对象
        actionMapper.pageListVo(req);
    }

    public int add(ActionAddReq req) {
        Action action = new Action();
        BeanUtil.copy(req, action);
        return actionMapper.add(action);
    }

    public int update(ActionUpdateReq req) {
        Action dbAction = actionMapper.findByIdForUpdate(req.id);
        if ( dbAction == null ) {
            notFound("id not found:" + req.id);
        }
        BeanUtil.copy(req, dbAction);
        return actionMapper.update(dbAction);
    }

    public int deleteById(Long id) {
        return actionMapper.deleteById(id);
    }

    public Action findById(Long id) {
        Action dbAction = actionMapper.findById(id);
        if ( dbAction == null ) {
            notFound("id not found:" + id);
        }
        return dbAction;
    }

}