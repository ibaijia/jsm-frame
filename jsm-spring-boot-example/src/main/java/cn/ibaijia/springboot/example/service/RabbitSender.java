//package cn.ibaijia.springboot.example.service;
//
//import org.springframework.amqp.AmqpException;
//import org.springframework.amqp.core.MessagePostProcessor;
//import org.springframework.amqp.rabbit.connection.CorrelationData;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.MessageHeaders;
//import org.springframework.messaging.support.MessageBuilder;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
//@Component
//public class RabbitSender {
//
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    final RabbitTemplate.ConfirmCallback confirmCallback = new RabbitTemplate.ConfirmCallback() {
//        @Override
//        public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//            System.err.println("消息ACK结果:" + ack + ", correlationData: " + correlationData);
//        }
//    };
//
//    public void send(String message, Map<String, Object> properties) throws Exception {
//
//        MessageHeaders mhs = new MessageHeaders(properties);
//        Message<?> msg = MessageBuilder.createMessage(message, mhs);
//
//        rabbitTemplate.setConfirmCallback(confirmCallback);
//
//        // 	指定业务唯一的iD
////        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
//
//        MessagePostProcessor mpp = new MessagePostProcessor() {
//
//            @Override
//            public org.springframework.amqp.core.Message postProcessMessage(org.springframework.amqp.core.Message message)
//                    throws AmqpException {
//                System.err.println("---> post to do: " + message);
//                return message;
//            }
//        };
//
//        rabbitTemplate.convertAndSend("comment.comment-number", msg, mpp);
//
//    }
//
//    public void send(String key, String message) throws Exception {
//        MessageHeaders mhs = new MessageHeaders(null);
//        Message<?> msg = MessageBuilder.createMessage(message, mhs);
//        rabbitTemplate.setConfirmCallback(confirmCallback);
//        rabbitTemplate.convertAndSend(key, msg);
//
//    }
//}
