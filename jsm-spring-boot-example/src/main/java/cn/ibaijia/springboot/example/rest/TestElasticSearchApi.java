package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.springboot.example.rest.vo.user.StudentInfo;
import cn.ibaijia.springboot.example.service.MyEsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-elastic-search/")
public class TestElasticSearchApi extends BaseRest {

    @Resource
    private MyEsService myEsService;

    /**
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<List<StudentInfo>> list() {
        return success(myEsService.list());
    }

}
