package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.RestLimitAnn;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-limit/")
public class TestLimitApi extends BaseRest {

    /**
     * 超过2个并发 调用 helloLimit
     * @param name
     * @return
     */
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @RestLimitAnn(value = 2, fallbackMethod = "helloLimit")
    public RestResp<String> hello(@RequestParam("name") String name) {
        return success("hello");
    }

    public RestResp<String> helloLimit(@RequestParam("name") String name) {
        return success("helloLimit");
    }

}
