package cn.ibaijia.springboot.example.service;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.elastic.ElasticObject;
import cn.ibaijia.jsm.elastic.ElasticService;
import cn.ibaijia.jsm.elastic.query.Builder;
import cn.ibaijia.jsm.elastic.query.RangeOperator;
import cn.ibaijia.jsm.elastic.query.SortCondition;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2021/11/23 20:03
 */
@Service
public class TestElasticService extends BaseService {
    private static String jsonOption = "{\"mappings\":{\"properties\":{\"time\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"level\":{\"type\":\"keyword\"},\"name\":{\"type\":\"keyword\"},\"traceId\":{\"type\":\"keyword\"},\"comments\":{\"type\":\"keyword\"},\"ip\":{\"type\":\"keyword\"},\"clusterId\":{\"type\":\"keyword\"},\"appName\":{\"type\":\"keyword\"},\"env\":{\"type\":\"keyword\"},\"gitHash\":{\"type\":\"keyword\"}}}}";
    private Logger logger = LogUtil.log(this.getClass());


    private ElasticService<Alarm> elasticService;

//    @PostConstruct
//    private void init() {
//        String address = "http://localhost:9002/";
//        String idx = "test_idx";
//        elasticService = new ElasticService(address, idx, jsonOption);
//    }


    public int matchAlarmRule(Date startTime, Date endTime, String name, String appName) {
        logger.info("matchAlarmRule: name :{} appName :{} ", name, appName);
        Builder builder = new Builder();
        builder.addSort(new SortCondition("time", false));

        builder.bool()
                .must()
                .term("env", "prod")
                .term("appName", appName)
                .term("name", name)
                .range("time", RangeOperator.GT, startTime.getTime())
                .range("time", RangeOperator.LT, endTime.getTime());

        String queryJson = builder.build();
        logger.info("queryJson:{}", queryJson);
        List<ElasticObject<Alarm>> list = elasticService.list(queryJson);
        logger.info("queryRes:{}", JsonUtil.toJsonString(list));
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public void add(Alarm alarmInfo) {
        elasticService.write(alarmInfo);
    }


    public List<Alarm> pageListVo(String appName, String alarmName, Date startTime, Date endTime, int from, int size) {
        List<Alarm> list = new ArrayList<>();
        Builder builder = new Builder();
        builder.from(from).size(size).addSort(new SortCondition("time", false));

        builder.bool()
                .must()
                .term("env", "prod")
                .term("appName", appName)
                .range("time", RangeOperator.GT, startTime.getTime())
                .range("time", RangeOperator.LT, endTime.getTime())
                .rangeIfTrue(alarmName != null, "alarmName", RangeOperator.LT, alarmName);

        String queryJson = builder.build();
        logger.info("queryJson:{}", queryJson);
        List<ElasticObject<Alarm>> esList = elasticService.list(queryJson);
        for (ElasticObject<Alarm> elasticObject : esList) {
            list.add(elasticObject.source);
        }
        return list;
    }
}
