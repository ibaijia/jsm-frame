package cn.ibaijia.springboot.example.service;

import cn.ibaijia.jsm.annotation.CacheAnn;
import cn.ibaijia.jsm.annotation.CacheType;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.springboot.example.dao.mapper.UserMapper;
import cn.ibaijia.springboot.example.dao.model.User;
import cn.ibaijia.springboot.example.rest.req.user.UserAddReq;
import cn.ibaijia.springboot.example.rest.req.user.UserPageReq;
import cn.ibaijia.springboot.example.rest.req.user.UserUpdateReq;
import cn.ibaijia.springboot.example.rest.vo.user.UserVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService extends BaseService {

    @Resource
    private UserMapper userMapper;


    public void pageList(UserPageReq<UserVo> req) {
        //example,只使用了db model,mapper xml 也可使用vo对象
        userMapper.pageListVo(req);
    }

    public int add(UserAddReq req) {
        User user = new User();
        BeanUtil.copy(req, user);
        return userMapper.add(user);
    }

    public int update(UserUpdateReq req) {
        User dbUser = userMapper.findByIdForUpdate(req.id);
        if (dbUser == null) {
            notFound("id not found:" + req.id);
        }
        BeanUtil.copy(req, dbUser);
        return userMapper.update(dbUser);
    }

    public int deleteById(Long id) {
        return userMapper.deleteById(id);
    }

    @CacheAnn(cacheType = CacheType.L1)
    public User findById(Long id) {
        User dbUser = userMapper.findById(id);
        if (dbUser == null) {
            notFound("id not found:" + id);
        }
        return dbUser;
    }

    public List<User> findAll() {
        return userMapper.listAll();
    }
}