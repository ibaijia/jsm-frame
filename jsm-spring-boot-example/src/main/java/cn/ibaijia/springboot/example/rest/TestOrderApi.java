package cn.ibaijia.springboot.example.rest;

import cn.ibaijia.jsm.annotation.*;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.order.BaseOrderLine;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.exception.InfoException;
import cn.ibaijia.jsm.utils.StringUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 测试下单排队
 * @author longzl
 */
@RestController
@RequestMapping("/v1/test-order")
public class TestOrderApi extends BaseRest {


    private Queue<String> queue = new ConcurrentLinkedQueue<>();

    /**
     * 下单公平排队--访问时 插入一个有序结合，或者减少有序结合的分数，再判断些元素是不是第一个元素，这样从一定程序上保证了用户的公平性
     * 返回一个 可用token（业务订单这个token有效期）让，客户端用户在规定的时间内使用token去下单。
     * @param code
     * @return
     */
    @OrderAnn(type = LineType.FAIR,productKey = "product-#{code}",userKey = "user-#{userId}")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/fair-token", method = RequestMethod.GET)
    public RestResp<String> fairToken(@RequestParam("code") String code, @RequestParam("userId") String userId) {
        // 实际业务应该是去数据库或者其它中间件拿,
        if(queue.isEmpty()){
            // 拿不到token根据实际业务情况 检查活动是否结束，没有结束重新排队，否则提示活动结束。
            throw new InfoException(BasePairConstants.ACTIVITY_NO_STOCK);
        }
        String token = queue.poll();
        // TODO 拿到加入一个有效期缓存（用户-令牌）,
        return success(token);
    }


    /**
     * 下单不公平排队--访问时去请求一个全局锁，如果拿到了就进入获取令牌的逻辑，否则不
     * 返回一个 可用token（业务订单这个token有效期）让，客户端用户在规定的时间内使用token去下单。
     * @param code
     * @return
     */
    @OrderAnn(type = LineType.UNFAIR,productKey = "product-#{code}",userKey = "user-#{userId}")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/unfair-token", method = RequestMethod.GET)
    public RestResp<String> unfairToken(@RequestParam("code") String code, @RequestParam("userId") String userId) {
        // 实际业务应该是去数据库或者其它中间件拿,
        if(queue.isEmpty()){
            // 拿不到token根据实际业务情况 检查活动是否结束，没有结束重新排队，否则提示活动结束。
            throw new InfoException(BasePairConstants.ACTIVITY_NO_STOCK);
        }
        String token = queue.poll();
        // TODO 拿到加入一个有效期缓存（用户-令牌）,
        return success(token);
    }

    /**
     * 随机生成一些token供测试使用
     * @return
     */
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/gen-token", method = RequestMethod.GET)
    public RestResp<Boolean> genToken() {
        for(int i=0; i<10; i++){
            queue.add(StringUtil.genRandomString(10));
        }
        return success(true);
    }


    /**
     * 测试创建活动
     * @return
     */
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/create-activity", method = RequestMethod.GET)
    public RestResp<Boolean> createActivity() {
        Map<String,BaseOrderLine> beansMap =  SpringContext.getBeansOfType(BaseOrderLine.class);
        for(BaseOrderLine orderLine:beansMap.values()){
            orderLine.setStatus(BasePairConstants.ACTIVITY_NOT_STARTED);
        }
        return success(true);
    }


    /**
     * 测试开始活动
     * @return
     */
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/start-activity", method = RequestMethod.GET)
    public RestResp<Boolean> startActivity() {
        Map<String,BaseOrderLine> beansMap =  SpringContext.getBeansOfType(BaseOrderLine.class);
        for(BaseOrderLine orderLine:beansMap.values()){
            orderLine.setStatus(BasePairConstants.ACTIVITY_STARTED);
        }
        return success(true);
    }

    /**
     * 测试结束活动
     * @return
     */
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RequestMapping(value = "/over-activity", method = RequestMethod.GET)
    public RestResp<Boolean> overActivity() {
        Map<String,BaseOrderLine> beansMap =  SpringContext.getBeansOfType(BaseOrderLine.class);
        for(BaseOrderLine orderLine:beansMap.values()){
            orderLine.setStatus(BasePairConstants.ACTIVITY_OVER);
        }
        return success(true);
    }


}
