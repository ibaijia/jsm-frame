package cn.ibaijia.springboot.example.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaiduCalendarMonthVo {

    @JsonProperty("ExtendedLocation")
    public String extendedLocation;
    @JsonProperty("originQuery")
    public String OriginQuery;
    @JsonProperty("SiteId")
    public String siteId;
    @JsonProperty("StdStg")
    public String stdStg;
    @JsonProperty("StdStl")
    public String stdStl;
    @JsonProperty("_select_time")
    public String selectTime;
    @JsonProperty("_update_time")
    public String updateTime;
    @JsonProperty("_version")
    public String version;
    @JsonProperty("almanac")
    public BaiduCalendarDayVo[] almanac;
}
