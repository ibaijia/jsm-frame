//package cn.ibaijia.springboot.example.test.gen;
//
//import cn.ibaijia.jsm.utils.LogUtil;
//import cn.ibaijia.jsm.utils.StringUtil;
//import cn.ibaijia.springboot.example.service.RabbitSender;
//import cn.ibaijia.springboot.example.test.models.SendCourseNoticeRequest;
//import cn.ibaijia.springboot.example.test.models.UpdateCommentCountRequest;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import javax.annotation.Resource;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class TestRabbitMq {
//
//    private Logger logger = LogUtil.log(TestRabbitMq.class);
//
//    @Resource
//    private RabbitSender rabbitSender;
//
//    @Test
//    public void testComment() {
//        UpdateCommentCountRequest request = new UpdateCommentCountRequest("1", 2, "CN");
//        try {
//            rabbitSender.send("comment.comment-number", StringUtil.toJson(request));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testFollow() {
//        SendCourseNoticeRequest request = new SendCourseNoticeRequest();
//        try {
//            rabbitSender.send("course.follow.queue", StringUtil.toJson(request));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//}
