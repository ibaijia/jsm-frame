package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.collection.DelaySet;
import cn.ibaijia.jsm.collection.DelaySetProcessor;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.junit.Test;

import java.util.Set;

/**
 * @Author: LongZL
 * @Date: 2022/8/10 15:28
 */
public class TestDelaySet {

    @Test
    public void test() {
        DelaySet<String> ds = new DelaySet<>("ds", 5000, new DelaySetProcessor() {
            @Override
            public void process(Set set) {
                System.out.println("process:" + JsonUtil.toJsonString(set));
            }

            @Override
            public void throwable(Throwable throwable) {
                System.out.println("throwable");
            }
        });
        createData(ds);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        createData(ds);
//        ds.stop();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void createData(DelaySet<String> ds) {

        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                for (int j = 0; j < 2000; j++) {
                    ds.add(StringUtil.genRandomNum(2));
                }
            }).start();
        }
    }

}
