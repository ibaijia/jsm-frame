package cn.ibaijia.springboot.example.test.models;

import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.excel.ExcelField;
import cn.ibaijia.jsm.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class StudentInfo implements ValidateModel {
    private static final long serialVersionUID = 1L;

    public StudentInfo() {
    }

    public StudentInfo(Long scoreA, Long scoreB, Long total) {
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.total = total;
    }

    @ExcelField(name = "scoreA")
    @FieldAnn(required = false)
    public Long scoreA;

    @ExcelField(name = "scoreB")
    @FieldAnn(required = false)
    public Long scoreB;

    @ExcelField(name = "total")
    @FieldAnn(required = false)
    public Long total;

    @DicAnn(name = "dic.user", toDicKV = true)
    public Long createBy;

    @DicAnn(name = "dic.user")
    public Long modifyBy;

    @DicAnn(name = "dic.user", toDicKV = true)
    public List<Long> userList;

    @DicAnn(name = "dic.user", toDicKV = true)
    public long[] userArr;

    public String name3;

    public Boolean gender;

    public boolean gender2;

    public Integer age;

    public int age2;

    public List<String> stuList;

    public String[] stuList2;

    public List<StudentImportModel> stuList3;

    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    public Date time = new Date();

}