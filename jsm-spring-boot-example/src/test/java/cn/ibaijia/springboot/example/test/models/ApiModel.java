package cn.ibaijia.springboot.example.test.models;

import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/10/12 17:53
 */
public class ApiModel implements ValidateModel {
    private static final long serialVersionUID = 1L;

    public String appName;
    public String url;
    public String spendTime;
    public Integer count;

}
