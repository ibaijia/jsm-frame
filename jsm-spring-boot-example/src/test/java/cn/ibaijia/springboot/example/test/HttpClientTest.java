package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.springboot.example.rest.req.area.AreaAddReq;
import org.junit.Test;

public class HttpClientTest {

    @Test
    public void getBaiduTest(){
//        String url = "https://www.baidu.com";
        String url = "https://www.google.com";
        HttpClient httpClient = new HttpClient();
        httpClient.setSocksProxy("127.0.0.1", 9980);
        System.out.println(httpClient.get(url));
    }

    @Test
    public void testHttps(){
//        HttpClient httpClient = new HttpClient(true);
//        String res = httpClient.get("https://www.baidu.com");
//        System.out.println(res);
//        String res2 = httpClient.get("https://dev.successinfo.com.cn/");
//        System.out.println(res2);

        HttpClient httpClient2 = new HttpClient(true);
        httpClient2.setAllowAllSsl(true);
//        String res3 = httpClient2.get("https://www.baidu.com");
//        System.out.println(res3);
        String res4 = httpClient2.get("https://dev.successinfo.com.cn/");
        System.out.println(res4);
    }

    @Test
    public void testBuildUrl(){
        HttpClient httpClient2 = new HttpClient(true);
        String url = "http://www.baidu.com";
        AreaAddReq data = new AreaAddReq();
        data.idx = 1;
        data.name = "123";
        System.out.println(httpClient2.buildUrl(url,data));
    }

}
