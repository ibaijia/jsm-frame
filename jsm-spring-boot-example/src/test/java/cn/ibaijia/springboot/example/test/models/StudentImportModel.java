package cn.ibaijia.springboot.example.test.models;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.excel.ExcelField;

public class StudentImportModel implements ValidateModel {
    private static final long serialVersionUID = 1L;

    @ExcelField(name = "scoreA")
    @FieldAnn(required = false)
	public Long scoreA;

    @ExcelField(name = "scoreB")
    @FieldAnn(required = false)
	public Long scoreB;

    @ExcelField(name = "total")
    @FieldAnn(required = false)
	public Long total;
}