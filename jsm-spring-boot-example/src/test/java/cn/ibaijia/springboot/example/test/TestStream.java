package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import cn.ibaijia.springboot.example.test.models.StudentInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;

public class TestStream {

    private static Logger logger = LogManager.getLogger(TestStream.class);

    @Test
    public void testAnyMatch() {
        List<StudentInfo> list = new ArrayList<>();
        list.add(new StudentInfo(10l, 20l, 30l));
        list.add(new StudentInfo(20l, 20l, 30l));
        list.add(new StudentInfo(30l, 20l, 30l));
        list.add(new StudentInfo(40l, 20l, 30l));
        list.add(new StudentInfo(50l, 20l, 30l));

        System.out.println(list.stream().allMatch(s -> s.scoreA == 30));

    }


    @Test
    public void testFile() {
        System.out.println(StringUtil.toJson(SystemUtil.getDiskStat()));
    }

    @Test
    public void testLog() {
        System.out.println(HttpMethod.GET.name());
        String a = "1123";
        String b = "1+1";
        String c = "${http://localhost:8081/example/v1/hello}";
        logger.info("hello");
        logger.info(b);
        logger.info(c);
        logger.info("hello {}", a);
        logger.info("hello1 {} ${a}", b);
    }


}
