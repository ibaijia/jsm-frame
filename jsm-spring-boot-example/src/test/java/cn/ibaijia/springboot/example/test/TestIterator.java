package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import cn.ibaijia.springboot.example.test.models.StudentImportModel;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestIterator {

    @Test
    public void TestIt() {
        List<StudentImportModel> list = new ArrayList<>();
        list.add(new StudentImportModel());
        list.add(new StudentImportModel());
        list.add(new StudentImportModel());
        list.add(new StudentImportModel());
        list.add(null);
        System.out.println(list.size());

        Iterator<StudentImportModel> it = list.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj == null) {
                it.remove();
            }
        }

        System.out.println(list.size());
    }


    @Test
    public void testFile() {
        System.out.println(StringUtil.toJson(SystemUtil.getDiskStat()));
    }

    @Test
    public void testExtractIp(){
        String str = "DROP       all  --  124.115.0.199        0.0.0.0/0";
        System.out.println(str.split(" ")[0]);
        System.out.println(StringUtil.normalize(str).split(" ")[3]);
    }

}
