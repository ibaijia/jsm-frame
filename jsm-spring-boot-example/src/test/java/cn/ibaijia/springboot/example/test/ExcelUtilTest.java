package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.excel.ExcelUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.springboot.example.test.models.StudentImportModel;
import cn.ibaijia.springboot.example.test.models.StudentImportModel1;
import org.junit.Test;

import java.util.List;

public class ExcelUtilTest {

    @Test
    public void readFormula() {
        String excelFile = "D:/test/test1.xlsx";
        List<StudentImportModel> list = ExcelUtil.readExcel(excelFile, 0, StudentImportModel.class, 1);
        System.out.println(StringUtil.toJson(list));
    }

    @Test
    public void readFormula1() {
        String excelFile = "D:/test/test1.xlsx";
        Page<StudentImportModel1> page = ExcelUtil.readExcel(excelFile, 0, StudentImportModel1.class, 1, new Page(10));
        System.out.println(StringUtil.toJson(page));
    }

}
