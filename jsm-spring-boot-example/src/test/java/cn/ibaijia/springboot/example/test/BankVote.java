package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.http.HttpBrowser;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BankVote {

    @Test
    public void vote() {
        String id = "11";
        List<Header> headerList0 = new ArrayList<>();
        headerList0.add(new BasicHeader("Host", "wact1.banyer.cn"));
        headerList0.add(new BasicHeader("Connection", "keep-alive"));
        headerList0.add(new BasicHeader("Upgrade-Insecure-Requests", "1"));
        headerList0.add(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) WindowsWechat(0x63030532)"));
        headerList0.add(new BasicHeader("Host", "wact1.banyer.cn"));
        HttpBrowser httpClient = HttpBrowser.open(headerList0);
        String res = httpClient.get("http://wact1.banyer.cn/wx/nh_vote/test1.php?aid=" + id);
//        System.out.println(res);
//        String token = getToken(res);
//        System.out.println("token:" + token);
//        res = httpClient.get("http://wact1.banyer.cn/wx/nh_vote/test1.php?action=getInfo&token=" + token + "&id=" + id);
//        System.out.println(res);
//        List<Header> headerList = new ArrayList<>();
//        headerList.add(new BasicHeader("Referer", "http://wact1.banyer.cn/wx/nh_vote/test1.php?aid=15"));
//        headerList.add(new BasicHeader("Host", "wact1.banyer.cn"));
//        headerList.add(new BasicHeader("Connection", "keep-alive"));
//        headerList.add(new BasicHeader("Accept", "application/json, text/javascript, */*; q=0.01"));
//        headerList.add(new BasicHeader("Origin", "http://wact1.banyer.cn"));
//        headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
//        headerList.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"));
//        headerList.add(new BasicHeader("Accept-Encoding", "gzip, deflate"));
//        headerList.add(new BasicHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7"));
        List<NameValuePair> nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("action", "help"));
        nameValuePairList.add(new BasicNameValuePair("token", "game_nh_vote_20210902b65729f6f0f9da5f8569d5a61ec36b1c"));
        nameValuePairList.add(new BasicNameValuePair("uid", "15089"));
        nameValuePairList.add(new BasicNameValuePair("work_id", "11"));
        res = httpClient.post("http://wact1.banyer.cn/wx/nh_vote/test1.php", nameValuePairList);
        System.out.println(res);

    }


    @Test
    public void voteTest() {
        String token = "game_nh_vote_202109028270598b779caddba97f70bd6af01ee9";
        int uid = 15000;
        int i = 0;
        List<HttpBrowser> list = createHttpClient();
        //TODO
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        for (HttpBrowser httpBrowser : list) {
            i++;
            int finalI = i;
            Future future = executorService.submit(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> nameValuePairList = new ArrayList<>();
                    nameValuePairList.add(new BasicNameValuePair("action", "help"));
                    nameValuePairList.add(new BasicNameValuePair("token", token));
                    nameValuePairList.add(new BasicNameValuePair("uid", String.valueOf(uid + finalI)));
                    nameValuePairList.add(new BasicNameValuePair("work_id", "15"));
                    String res = httpBrowser.post("http://wact1.banyer.cn/wx/nh_vote/test1.php", nameValuePairList);
                    System.out.println(res);
                }
            });
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } finally {
            }
        }
    }

    private List<HttpBrowser> createHttpClient() {
        List<HttpBrowser> list = new ArrayList<>();

        List<Header> headerList0 = new ArrayList<>();
        headerList0.add(new BasicHeader("Host", "wact1.banyer.cn"));
        headerList0.add(new BasicHeader("Connection", "keep-alive"));
        headerList0.add(new BasicHeader("Upgrade-Insecure-Requests", "1"));
        headerList0.add(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) WindowsWechat(0x63030532)"));
        headerList0.add(new BasicHeader("Host", "wact1.banyer.cn"));
        headerList0.add(new BasicHeader("Referer", "http://wact1.banyer.cn/wx/nh_vote/test1.php?aid=15"));
        headerList0.add(new BasicHeader("Origin", "http://wact1.banyer.cn"));
        headerList0.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
        headerList0.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"));
        headerList0.add(new BasicHeader("Accept-Encoding", "gzip, deflate"));
        headerList0.add(new BasicHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7"));
        headerList0.add(new BasicHeader("Cookie", "p_h5_upload_u=EC4C7372-352F-466A-88D6-9FAB2BEC8FE5; PHPSESSID=fdke8nfdjlvuf2nccftle8k0c1"));
        for (int i = 0; i < 1000; i++) {
            HttpBrowser httpBrowser = HttpBrowser.open(headerList0);
            list.add(httpBrowser);
        }
        return list;
    }


}
