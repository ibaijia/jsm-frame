package cn.ibaijia.springboot.example.test.models;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.excel.ExcelField;

public class StudentImportModel1 implements ValidateModel {
    private static final long serialVersionUID = 1L;

    @ExcelField(name = "scoreA",format = "0.00")
    @FieldAnn(required = false)
	public Double scoreA;

    @ExcelField(name = "scoreB")
    @FieldAnn(required = false)
	public Float scoreB;

    @ExcelField(name = "total",format = "0.00")
    @FieldAnn(required = false)
	public Double total;
}