package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.RsaUtil;
import org.junit.Test;

import java.security.KeyPair;

public class TestRSA {

    @Test
    public void genRSA() throws Exception {
        String data = "{\"beginTime\":\"2021-01-01\",\"endTime\":\"2030-01-01\",\"authData\":\"\"}";
        KeyPair keyPair = RsaUtil.getKeyPair();
        //公钥
        String publicKey = RsaUtil.getPublicKey(keyPair);
        //私钥
        String privateKey = RsaUtil.getPrivateKey(keyPair);

        System.out.println("公钥：\n" + publicKey);
        System.out.println("私钥：\n" + privateKey);

        String s1 = RsaUtil.encryptByPrivateKey(data, privateKey);
        System.out.println("私钥加密后的数据:" + s1);
        String s2 = RsaUtil.decryptByPublicKey(s1, publicKey);
        System.out.println("公钥解密后的数据:" + s2);

    }

    @Test
    public void licenseGen() throws Exception {
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDYGjgfELB1Aie3CaV5fsCTZpB3PJ+R9yFB+C89YGwfvqsn4thuBHFXZuJs2HG8lKK2Fq0hfnlUFpkk3Wbpq2NZk/XSGHszLWUz1pdfCSePfKZaOtPCxOpSMsqpkXVxkbkeB5VMKHmKtSbTrxLYNzABHTUFYBkIvNF2XYM+lepXoQIDAQAB";
        String privateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANgaOB8QsHUCJ7cJpXl+wJNmkHc8n5H3IUH4Lz1gbB++qyfi2G4EcVdm4mzYcbyUorYWrSF+eVQWmSTdZumrY1mT9dIYezMtZTPWl18JJ498plo608LE6lIyyqmRdXGRuR4HlUwoeYq1JtOvEtg3MAEdNQVgGQi80XZdgz6V6lehAgMBAAECgYBJS3tFbg+rX/PAbREmyArg86SEDJfCliM9kMPU+WMy0SST/qiRiAg5MLgTWsYYxAD76W0HMzJLhu6iRkWlc2/aTl5rD8xbArpxJKzuL76ZumHnThaGYRlQ38tq8c1vpkhCEzEJIBWvtERBHbZb082BL/lELYSvjUFpok7px4qXwQJBAPRHslrgLUjO853Gj6nmry2mUwJV6TwvmKd8wMSMB7IfN589gTLa4xs7Axxg1A/RUqA2lf+7lAa6fKvdO0vdG7UCQQDieHAyKhP0R4WoF5VpJiPJdAIPp31nAtg2cZSev0pph6cpm8kalHnhGMLeQMNnQDgfWvvl3DkH5TwUx9NldTe9AkEArlcBJrR5/U2eNsuAutxlx+m9spwH8qlxhI+XsG7UCz8WXnLx446uqT+Lr9Q3ZPaaxrdOhI6mTcsIsUwaRsGORQJBAJUV4g40AxTRrJwegl5JHE/3DZBOm1Af4EPe+46RsSPNiG7svjPAQIe9bDMxgTFMZmXF2IPWOLkI4Edx0Kk+xCUCQQCAplFoRSsCGFeAcLADo9OhgyN/2wpIs+Z6vCltaOFPYm8RcJTXb2cfSluXA23OONsm7FZUydf8iq+smOV4quCb";
        String data = "{\"beginTime\":\"2021-01-01\",\"endTime\":\"2030-01-01\",\"authData\":\"\"}";//TODO 修改这个
        String s1 = RsaUtil.encryptByPrivateKey(data, privateKey);
        System.out.println("私钥加密后的数据:" + s1);
    }

}
