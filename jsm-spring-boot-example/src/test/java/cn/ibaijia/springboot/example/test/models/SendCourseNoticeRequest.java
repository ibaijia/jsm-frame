package cn.ibaijia.springboot.example.test.models;

public class SendCourseNoticeRequest {
    //系列Id
    public String id = "1";
    public String learn_type = "PROCESSING";
    public String [] user_ids;
    public String [] roles;
    public Boolean user_exclude = false;
    public String remind_type = "COURSE";
    public String alert_description = "";
    public String send_user_id = "2001";
    public String course_id = "1";
    public String course_type = "SERIES";

    public String alert_title;

    public String subsidiary = "CN";
}
