package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.springboot.example.test.models.StudentInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DicAnnTest {

    private Logger logger = LogUtil.log(DicAnnTest.class);

    @Resource
    private JedisService jedisService;
    @Resource
    private CacheService cacheService;

    @Test
    public void dicAnn() {
        StudentInfo studentInfo = new StudentInfo(1l, 2l, 3l);
        studentInfo.createBy = 1L;
        studentInfo.modifyBy = 2L;

        cacheService.hset("dic.user", "1", "longzl1");
        cacheService.hset("dic.user", "2", "admin1");

        studentInfo.userList = new ArrayList<>();
        studentInfo.userList.add(1L);
        studentInfo.userList.add(2L);

        String res = JsonUtil.toJsonString(studentInfo);
        System.out.println(res);

    }

    @Test
    public void dicAnnDecode() {
        String jsonStr = "{\"scoreA\":1,\"scoreB\":2,\"total\":3,\"createBy\":{\"name\":\"longzl1\",\"id\":\"1\"},\"modifyBy\":\"admin1\",\"userArr\":[{\"name\":\"longzl1\",\"id\":\"1\"},{\"name\":\"admin1\",\"id\":\"2\"}],\"name3\":\"\",\"gender\":false,\"gender2\":false,\"age\":0,\"age2\":0,\"stuList\":[],\"stuList2\":[],\"stuList3\":[],\"time\":\"2022-05-17 11:55:18\"}";
        StudentInfo studentInfo = JsonUtil.parseObject(jsonStr, StudentInfo.class);
        System.out.println(jsonStr);
        System.out.println(JsonUtil.toJsonString(studentInfo));
    }

}
