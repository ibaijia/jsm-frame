package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.ReflectionUtil;
import cn.ibaijia.springboot.example.test.models.ApiModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2022/6/29 15:57
 */
public class JsonUtilTest {

    @Test
    public void testIsJsonStr() {
        System.out.println(JsonUtil.isJsonString("123"));
        System.out.println(JsonUtil.isJsonString("[1,2]"));
        System.out.println(JsonUtil.isJsonString("[]"));
        System.out.println(JsonUtil.isJsonString("{\"a\":\"b\"}"));
        System.out.println(JsonUtil.isJsonString("{}"));
    }

    @Test
    public void testToStr() {
        System.out.println(JsonUtil.toJsonString("123"));
        System.out.println(JsonUtil.toJsonString(123));
    }


    @Test
    public void testParseObject() {
        String str = "123abc";

        System.out.println(JsonUtil.parseObject(str, ReflectionUtil.getSuperClassGenricType(String.class)));
    }

    @Test
    public void testJson() throws InstantiationException, IllegalAccessException {
        List list = aggs(null, ApiModel.class);

    }


    private <T> List<T> aggs(String queryJson, Class<T> clazz) throws InstantiationException, IllegalAccessException {
        String res = "{\"appName\":{\"doc_count_error_upper_bound\":0,\"sum_other_doc_count\":0,\"buckets\":[{\"key\":\"admin-api\",\"doc_count\":742,\"url\":{\"doc_count_error_upper_bound\":0,\"sum_other_doc_count\":0,\"buckets\":[{\"key\":\"GET /admin-api/v1/hello\",\"doc_count\":729,\"spendTime\":{\"value\":18.0}},{\"key\":\"GET /admin-api/v1/user-info/page-list\",\"doc_count\":5,\"spendTime\":{\"value\":16.0}},{\"key\":\"GET /admin-api/v1/organization/page-list\",\"doc_count\":3,\"spendTime\":{\"value\":294.0}},{\"key\":\"GET /admin-api/v1/product-publish-apply/process-list\",\"doc_count\":1,\"spendTime\":{\"value\":22.0}},{\"key\":\"GET /admin-api/v1/user-info/invite/2067\",\"doc_count\":1,\"spendTime\":{\"value\":58.0}},{\"key\":\"POST /admin-api/v1/user-info/add\",\"doc_count\":1,\"spendTime\":{\"value\":32.0}},{\"key\":\"PUT /admin-api/v1/user-info/delete/2066\",\"doc_count\":1,\"spendTime\":{\"value\":29.0}},{\"key\":\"PUT /admin-api/v1/user-info/delete/2067\",\"doc_count\":1,\"spendTime\":{\"value\":30.0}}]}},{\"key\":\"app-api\",\"doc_count\":732,\"url\":{\"doc_count_error_upper_bound\":0,\"sum_other_doc_count\":0,\"buckets\":[{\"key\":\"GET /app-api/v1/hello\",\"doc_count\":728,\"spendTime\":{\"value\":14.0}},{\"key\":\"GET //app-api/v1/user/captcha/3169.jpg\",\"doc_count\":1,\"spendTime\":{\"value\":9.0}},{\"key\":\"GET /app-api/v1/product/my-product-list\",\"doc_count\":1,\"spendTime\":{\"value\":4.0}},{\"key\":\"GET /app-api/v1/statistics/product/count\",\"doc_count\":1,\"spendTime\":{\"value\":4.0}},{\"key\":\"GET /app-api/v1/user-info/profile\",\"doc_count\":1,\"spendTime\":{\"value\":3.0}}]}},{\"key\":\"base-api\",\"doc_count\":728,\"url\":{\"doc_count_error_upper_bound\":0,\"sum_other_doc_count\":0,\"buckets\":[{\"key\":\"GET /base-api/v1/hello\",\"doc_count\":728,\"spendTime\":{\"value\":20.0}}]}},{\"key\":\"os-api\",\"doc_count\":737,\"url\":{\"doc_count_error_upper_bound\":0,\"sum_other_doc_count\":0,\"buckets\":[{\"key\":\"GET /os-api/v1/hello\",\"doc_count\":728,\"spendTime\":{\"value\":79.0}},{\"key\":\"POST /os-api/v1/manage-file/list\",\"doc_count\":5,\"spendTime\":{\"value\":157.0}},{\"key\":\"GET /os-api/v1/manage-file/page-list\",\"doc_count\":3,\"spendTime\":{\"value\":26.0}},{\"key\":\"GET /os-api/v1/manage-file/user-space-page-list\",\"doc_count\":1,\"spendTime\":{\"value\":23.0}}]}}]}}";
        JsonNode node = JsonUtil.readTree(res);
        List<T> list = new ArrayList<>();
        Map.Entry<String, JsonNode> entry = node.fields().next();
        readField(list, entry, clazz, null);
        return list;
    }

    private <T> void readField(List<T> list, Map.Entry<String, JsonNode> entry, Class<T> clazz, T parent) throws InstantiationException, IllegalAccessException {
        String fieldName = entry.getKey();
        JsonNode fieldNode = entry.getValue();
        if (fieldNode.has("buckets")) {
            ArrayNode buckets = (ArrayNode) fieldNode.get("buckets");
            for (JsonNode jsonNode : buckets) {
                T t = clazz.newInstance();
                if (parent != null) {
                    BeanUtil.copy(parent, t);
                }
                ReflectionUtil.setFieldValue(t, fieldName, jsonNode.get("key").asText());
                ReflectionUtil.setFieldValue(t, "count", jsonNode.get("doc_count").intValue());
                Map.Entry<String, JsonNode> nextEntry = findNextEntry(jsonNode);
                if (nextEntry == null) {
                    list.add(t);
                    continue;
                } else {
                    String nextKey = nextEntry.getKey();
                    JsonNode nextJsonNode = nextEntry.getValue();
                    // avg min max
                    if (!nextJsonNode.has("buckets")) {
                        String value = nextJsonNode.get("value").asText();
                        ReflectionUtil.setFieldValue(t, nextKey, value);
                        list.add(t);
                        continue;
                    } else {
                        readField(list, nextEntry, clazz, t);
                    }
                }
            }
        }
    }

    private Map.Entry<String, JsonNode> findNextEntry(JsonNode jsonNode) {
        Iterator<Map.Entry<String, JsonNode>> it = jsonNode.fields();
        while (it.hasNext()) {
            Map.Entry<String, JsonNode> entry = it.next();
            it.remove();
            JsonNode nextJsonNode = entry.getValue();
            if (nextJsonNode.isObject()) {
                return entry;
            }
        }
        return null;
    }


}
