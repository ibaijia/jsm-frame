package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.EncryptUtil;
import cn.ibaijia.jsm.utils.HttpClientUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.springboot.example.test.models.BaiduCalendarMonthVo;
import cn.ibaijia.springboot.example.test.models.BaiduCalendarVo;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class TestCnHoliday {

    private Logger logger = LogUtil.log(TestCnHoliday.class);

    @Test
    public void testGetCnHoliday() {
        BaiduCalendarMonthVo data = queryMonth(2023, 11);
        logger.info("data:{}", JsonUtil.toJsonString(data.almanac[6]));
        logger.info("data:{}", JsonUtil.toJsonString(data.almanac[5]));
    }

    public BaiduCalendarMonthVo queryMonth(int year, int month) {
        String url = "https://opendata.baidu.com/api.php";
        url = url + "?" + getQueryStr(year, month);
        logger.info("完整请求地址: {}", url);
        String response = HttpClientUtil.get(url);
        logger.info("原始响应结果: {}", response);
        BaiduCalendarVo vo = JsonUtil.parseObject(response, BaiduCalendarVo.class);
        return vo.data[0];
    }

    public JsonNode queryCalendarList(int year, int month) {
        String url = "https://opendata.baidu.com/api.php";
        url = url + "?" + getQueryStr(year, month);
        logger.info("完整请求地址: {}", url);
        String response = HttpClientUtil.get(url);
        logger.info("原始响应结果: {}", response);
        //去除多余字符，提取JSON部分(返回结果为JSONP时)
        String jsonStr = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
        logger.info("JSON响应体: {}", jsonStr);
        //解析返回结果
        JsonNode json = JsonUtil.readTree(jsonStr);
        //仅当状态=0时调用成功
        if (!"0".equals(json.get("status").textValue())) {
            logger.info("接口调用失败");
            return null;
        }
        //提取data数组
        JsonNode data = json.get("data");
        if (data.isEmpty()) {
            return null;
        }
        //取第一个元素
        JsonNode data_0 = data.get(0);
        //提取almanac数组并返回
        return data_0.get("almanac");
    }

    private String getQueryStr(int year, int month) {
        Map<String, String> params = new HashMap<>();
        params.put("tn", "wisetpl");
        params.put("format", "json");
        params.put("resource_id", "39043");
        params.put("t", System.currentTimeMillis() + "");
//        params.put("cb", "callback"); //不加这个参数返回结果为JSON，加了返回的是JSONP
        params.put("query", year + "年" + month + "月");

        StringBuffer sb = new StringBuffer();
        params.forEach((k, v) -> {
            sb.append(k).append("=").append(EncryptUtil.urlEncode(v)).append("&");
        });

        return sb.toString();
    }

}
