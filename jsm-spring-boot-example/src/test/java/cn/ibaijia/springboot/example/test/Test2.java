//package cn.ibaijia.springboot.example.test;
//
//public class Test2 {
//
//    private static Object locker = new Object();
//
//    private static volatile int count = 1;
//
//    static class MyPrinter implements Runnable {
//
//        private String name;
//
//        MyPrinter(String name) {
//            this.name = name;
//        }
//
//        @Override
//        public void run() {
//
//            synchronized (locker) {
//                for (int i = 0; i < 50; i++) {
//                    System.out.println(name + " --- " + count);
//                    count++;
//                    try {
//                        locker.notify();
//                        locker.wait();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//
//        }
//    }
//
//    public static void main(String[] args) throws InterruptedException {
//        Thread thread1 = new Thread(new MyPrinter("Printer1"));
//        Thread thread2 =  new Thread(new MyPrinter("Printer2"));
//        thread1.start();
//        thread2.start();
//    }
//
//}
