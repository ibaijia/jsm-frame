package cn.ibaijia.springboot.example.test.proxy;

import org.junit.Test;

/**
 * @Author: LongZL
 * @Date: 2022/6/20 11:34
 */
public class TestProxy {

    @Test
    public void testProxy(){

        SomeApi someApi = MapperFactory.getProxyMapper();
        someApi.hello();
    }

}
