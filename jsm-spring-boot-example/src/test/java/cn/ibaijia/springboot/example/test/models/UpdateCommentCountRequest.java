package cn.ibaijia.springboot.example.test.models;

public class UpdateCommentCountRequest {

    public UpdateCommentCountRequest(String targetId, Integer total, String subsidiary) {
        this.target_id = targetId;
        this.total = total;
        this.subsidiary = subsidiary;
    }

    public String target_id;

    public Integer total;

    public String subsidiary;

}
