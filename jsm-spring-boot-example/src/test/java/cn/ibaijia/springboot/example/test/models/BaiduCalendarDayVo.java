package cn.ibaijia.springboot.example.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * "animal":"兔",
 * "avoid":"开业.掘井.开渠.造船.造桥",
 * "cnDay":"日",
 * "day":"1",
 * "festivalList":"国庆节,国际音乐日",
 * "gzDate":"壬辰",
 * "gzMonth":"辛酉",
 * "gzYear":"癸卯",
 * "isBigMonth":"1",
 * "lDate":"十七",
 * "lMonth":"八",
 * "lunarDate":"17",
 * "lunarMonth":"8",
 * "lunarYear":"2023",
 * "month":"10",
 * "oDate":"2023-09-30T16:00:00.000Z",
 * "status":"1",
 * "suit":"结婚.出行.搬家.理发.合婚订婚.搬新房.订盟.动土.栽种.安床.纳畜.安葬.挂匾.祭祀.修造.拆卸.入殓.成服.出火.除服.收养子女.开光.破土.作梁",
 * "term":"国庆节",
 * "timestamp":"1696089600",
 * "type":"h",
 * "value":"国际音乐日",
 * "year":"2023",
 * "yjJumpUrl":"https://mobile.51wnl-cq.com/huangli_tab_h5/?posId=BDSS&STIME=2023-10-01",
 * "yj_from":"51wnl"
 */
public class BaiduCalendarDayVo {

    /**
     * 生肖年
     */
    @JsonProperty("animal")
    public String animal;
    /**
     * 忌
     */
    @JsonProperty("avoid")
    public String avoid;
    /**
     * 星期描述（一二三四五六日）
     */
    @JsonProperty("cnDay")
    public String cnDay;
    /**
     * 日（阳历）
     */
    @JsonProperty("day")
    public String day;
    /**
     *
     */
    @JsonProperty("festivalList")
    public String festivalList;
    /**
     * 日-干支
     */
    @JsonProperty("gzDate")
    public String gzDate;
    /**
     * 月-干支
     */
    @JsonProperty("gzMonth")
    public String gzMonth;
    /**
     * 年-干支
     */
    @JsonProperty("gzYear")
    public String gzYear;
    /**
     * 是否是农历大月（1-是）
     */
    @JsonProperty("isBigMonth")
    public String isBigMonth;
    /**
     * 日（农历描述）
     */
    @JsonProperty("lDate")
    public String lDate;
    /**
     * 月（农历描述）
     */
    @JsonProperty("lMonth")
    public String lMonth;
    /**
     * 日（农历）
     */
    @JsonProperty("lunarDate")
    public String lunarDate;
    /**
     * 月（农历）
     */
    @JsonProperty("lunarMonth")
    public String lunarMonth;
    /**
     * 年（农历）
     */
    @JsonProperty("lunarYear")
    public String lunarYear;
    /**
     * 月（阳历）
     */
    @JsonProperty("month")
    public String month;
    /**
     * 时间（0时区）
     */
    @JsonProperty("oDate")
    public String oDate;
    /**
     * 节假日状态（1-休假，2-补班）
     */
    @JsonProperty("status")
    public String status;
    /**
     * 节假日描述（含节气）
     */
    @JsonProperty("term")
    public String term;
    @JsonProperty("timestamp")
    public String timestamp;
    /**
     * 节假日类型
     */
    @JsonProperty("type")
    public String type;
    /**
     * 节假日(国际)
     */
    @JsonProperty("value")
    public String value;
    /**
     * 年（阳历）
     */
    @JsonProperty("year")
    public String year;
    @JsonProperty("yjJumpUrl")
    public String yjJumpUrl;
    @JsonProperty("yj_from")
    public String yjFrom;

}
