package cn.ibaijia.springboot.example.test.gen;

import cn.ibaijia.jsm.gen.GenService;
import cn.ibaijia.jsm.utils.LogUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Gen {

    private Logger logger = LogUtil.log(Gen.class);

    @Resource
    private GenService genService;

    @Test
    public void gen() {
        genService.gen("MessageTemplate");
    }

}
