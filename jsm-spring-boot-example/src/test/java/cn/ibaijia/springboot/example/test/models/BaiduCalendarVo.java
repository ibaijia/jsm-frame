package cn.ibaijia.springboot.example.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaiduCalendarVo {

    public String status;
    public String t;
    @JsonProperty("set_cache_time")
    public String setCacheTime;
    @JsonProperty("data")
    public BaiduCalendarMonthVo[] data;

}
