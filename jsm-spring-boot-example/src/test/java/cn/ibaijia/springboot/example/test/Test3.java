//package cn.ibaijia.springboot.example.test;
//
//public class Test3 {
//
//    public static void main(String[] args) {
//        String fromUserId = "张三";
//        String toUserId = "李四";
//        Long amount = 100L;
//        try {
//            transfer(fromUserId, amount, toUserId);
//            System.out.println(String.format("[%s]向[%s]转账[%s]成功", fromUserId, toUserId, amount));
//        } catch (Exception e) {
//            //logger.error(e)
//            System.out.println(String.format("[%s]向[%s]转账[%s]失败,原因：%s", fromUserId, toUserId, amount, e.getMessage()));
//        }
//
//    }
//
//    private static synchronized void transfer(String fromUserId, Long amount, String toUserId) {
//        //查询fromUserId 账户金额，对比是否够扣
//        long remainAmount = queryAmountByUserId(fromUserId);
//        if (remainAmount - amount < 0) {
//            throw new RuntimeException("余额不足");
//        }
//        try {
//            doTransfer(fromUserId, amount, toUserId);
//        } catch (Exception e) {
//            //logger.error(e)
//            throw new RuntimeException("转败失败:" + e.getMessage());
//        }
//
//    }
//
//    //@Transactional 遇到Exception 自动回滚
//    private static void doTransfer(String fromUserId, Long amount, String toUserId) {
//        // 1 更新fromUserId account 为 account-amount
//        long remainAmount = queryAmountByUserId(fromUserId);
//        updateAccount(fromUserId, remainAmount - amount);
//        // 2 添加转账日志
//        addLog(fromUserId, toUserId, -amount, "转账");
//        // 1 更新fromUserId account 为 account-amount
//        long remainAmount2 = queryAmountByUserId(toUserId);
//        updateAccount(fromUserId, remainAmount2 + amount);
//        // 2 添加转账日志
//        addLog(fromUserId, toUserId, amount, "收账");
//    }
//
//    private static void addLog(String fromUserId, String toUserId, Long amount, String 转账) {
//        //TODO addLog to db
//    }
//
//    private static void updateAccount(String fromUserId, long amount) {
//        //TODO update amount to db
//    }
//
//    private static Long queryAmountByUserId(String fromUserId) {
//        return 200L;
//    }
//
//}
