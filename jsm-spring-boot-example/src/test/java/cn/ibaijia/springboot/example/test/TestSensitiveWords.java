package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.SensitiveWordUtil;
import org.junit.Test;

/**
 * @Author: LongZL
 * @Date: 2022/8/27 19:09
 */
public class TestSensitiveWords {

    @Test
    public void testSensitive() {
        String str = "读者法轮功意见处理与法伦功反馈汇总";
        String res = SensitiveWordUtil.escapeSensitiveWord(str, SensitiveWordUtil.MATCH_TYPE_MIN, '*');
        System.out.println(res);
    }

}
