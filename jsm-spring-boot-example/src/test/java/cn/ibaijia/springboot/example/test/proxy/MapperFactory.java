package cn.ibaijia.springboot.example.test.proxy;

import cn.ibaijia.jsm.http.ann.HttpMapper;
import cn.ibaijia.jsm.http.ann.HttpMapping;
import cn.ibaijia.jsm.utils.JsonUtil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Author: LongZL
 * @Date: 2022/6/20 11:06
 */
public class MapperFactory {

    public static <T> T getProxyMapper() {
        T mapper = (T) Proxy.newProxyInstance(MapperFactory.class.getClassLoader(), new Class<?>[]{SomeApi.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                HttpMapper apiBaseMapperAnn = proxy.getClass().getAnnotation(HttpMapper.class);
                HttpMapping apiMapperAnn = method.getAnnotation(HttpMapping.class);
                System.out.println(JsonUtil.toJsonString(apiBaseMapperAnn));
                System.out.println(JsonUtil.toJsonString(apiMapperAnn));
                return null;
            }
        });
        return mapper;
    }

}

