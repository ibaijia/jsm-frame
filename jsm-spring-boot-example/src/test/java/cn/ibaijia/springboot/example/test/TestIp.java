package cn.ibaijia.springboot.example.test;

import cn.ibaijia.jsm.utils.IpUtil;
import org.junit.Test;

/**
 * @Author: LongZL
 * @Date: 2022/4/1 11:30
 */
public class TestIp {

    @Test
    public void testRange(){
        System.out.println(IpUtil.isInRange("192.168.1.127", "192.168.1.64"));
        System.out.println(IpUtil.isInRange("192.168.1.127", "192.168.1.127"));
        System.out.println(IpUtil.isInRange("192.168.1.127", "192.168.1.64/26"));
        System.out.println(IpUtil.isInRange("192.168.1.64", "192.168.1.64/26"));
        System.out.println(IpUtil.isInRange("192.168.1.2", "192.168.0.0/23"));
        System.out.println(IpUtil.isInRange("192.168.0.1", "192.168.0.0/24"));
        System.out.println(IpUtil.isInRange("192.168.0.0", "192.168.0.0/32"));
    }

    @Test
    public void testInternal(){
        System.out.println(IpUtil.isInternalIp("127.0.0.1"));
        System.out.println(IpUtil.isInternalIp("192.168.1.127"));
        System.out.println(IpUtil.isInternalIp("125.84.183.73"));
    }


}
