package cn.ibaijia.springboot.example.test.proxy;

import cn.ibaijia.jsm.http.ann.HttpMapper;
import cn.ibaijia.jsm.http.ann.HttpMapping;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: LongZL
 * @Date: 2022/6/20 11:02
 */
@HttpMapper(baseUri = "")
public interface SomeApi {

    @HttpMapping(method = RequestMethod.GET, uri = "/v1/hello")
    public RestResp<String> hello();

}
