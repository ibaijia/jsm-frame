package cn.ibaijia.test.sca.test.free;

import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.test.sca.service.AccountService;
import org.junit.Test;

/**
 * @author jsm_auto_gen
 */
public class JunitTest {

    @Test
    public void testRemoveAsList(){
        String str = "1,2,3,4";
        System.out.println(StringUtil.removeAsList(str,',',"3"));
    }

    @Test
    public void testGetClazzName(){
        System.out.println(StringUtil.lowerCaseFirst(AccountService.class.getSimpleName()));
    }


}
