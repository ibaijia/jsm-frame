package cn.ibaijia.test.sca.service;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.elastic.util.JsonUtil;
import cn.ibaijia.test.sca.httpMapper.StockApiMapper;
import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @Author: LongZL
 * @Date: 2022/11/4 15:01
 */
@Service
public class OrderTccService extends BaseService implements OrderTcc {

    @Resource
    private StockApiMapper stockApiMapper;

    @Override
    @TwoPhaseBusinessAction(name = "reduceTcc", commitMethod = "reduceTccConfirm", rollbackMethod = "reduceTccCancel")
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void reduceTcc(ReduceStockReq req) {
        logger.info("reduceTcc: req:{}", JsonUtil.toJsonString(req));
        throw new RuntimeException("reduceTcc 出错了！");
//        stockApiMapper.reduceTcc(req);
    }

    @Override
    public void reduceTccConfirm(BusinessActionContext actionContext, ReduceStockReq req) {
        logger.info("reduceTccCancel: req:{}", JsonUtil.toJsonString(req));
        throw new RuntimeException("假装出错了！");
    }

    @Override
    public void reduceTccCancel(BusinessActionContext actionContext, ReduceStockReq req) {
        logger.info("reduceTccCancel: req:{}", JsonUtil.toJsonString(req));
        //TODO 异常后其它操作的补偿
    }
}
