package cn.ibaijia.test.sca.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.JobUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jsm_auto_gen
 */
@RestController
@RequestMapping("/v1/test-job")
@RefreshScope
public class TestJobApi extends BaseRest {


    @ApiOperation("刷新job")
    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> refresh() {
        JobUtil.freshJsmJob(null);
        return success(true);
    }

}
