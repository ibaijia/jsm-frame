package cn.ibaijia.test.sca.job;

import cn.ibaijia.jsm.annotation.ClusterJobLockAnn;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * job加 @RefreshScope 里面的配置不能刷新， 只能用一个专门的配置类加
 * @author longzl
 */
@Component
public class TestJob {
    private Logger logger = LogUtil.log(this.getClass());

    @Value("${jobContent:1111}") //nacos修改后不更新
    private String jobContent;

    @Resource
    private JobConfig jobConfig;

    /**
     * @Scheduled 支持nacos但是动态改不了，要想动态配置 手动创建 继承BaseJsmJob或者实现JsmJob接口
     */
    @ClusterJobLockAnn
    @Scheduled(cron = "${jobCron:5/10 * * * * ?}")
    public void execute() {
        logger.info("TestJob:: begin.");
        logger.info("TestJob:: running getJob1Content:{}" ,jobConfig.getJob1Content());
        logger.info("TestJob:: end.");
    }

}
