package cn.ibaijia.test.sca.service;

import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.LocalTCC;

/**
 * @Author: LongZL
 * @Date: 2022/11/4 14:01
 */
@LocalTCC
public interface OrderTcc {

    //    @TwoPhaseBusinessAction(name = "reduceTcc", commitMethod = "reduceTccConfirm", rollbackMethod = "reduceTccCancel")
    public void reduceTcc(ReduceStockReq req);

    public void reduceTccConfirm(BusinessActionContext actionContext, ReduceStockReq req);

    public void reduceTccCancel(BusinessActionContext actionContext, ReduceStockReq req);

}
