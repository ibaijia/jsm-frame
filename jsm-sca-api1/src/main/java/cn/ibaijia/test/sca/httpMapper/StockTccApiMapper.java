package cn.ibaijia.test.sca.httpMapper;

import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.http.ann.HttpMapper;
import cn.ibaijia.jsm.http.ann.HttpPutMapping;
import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

/**
 * @author longzl
 */
@LocalTCC
@HttpMapper(baseUri = "${service.stockApi}")
public interface StockTccApiMapper {

    /**
     * 减库存 TCC  一般commitMethod 清除准备补偿的数据，rollbackMethod 补偿操作
     */
    @HttpPutMapping(uri = "/v1/stock/reduce-tcc")
    @TwoPhaseBusinessAction(name = "reduceTcc", commitMethod = "reduceTccConfirm", rollbackMethod = "reduceTccCancel")
    RestResp<Boolean> reduceTcc(ReduceStockReq req);

    @HttpPutMapping(uri = "/v1/stock/reduce-tcc-confirm")
    Boolean reduceTccConfirm(BusinessActionContext actionContext);


    @HttpPutMapping(uri = "/v1/stock/reduce-tcc-cancel")
    Boolean reduceTccCancel(BusinessActionContext actionContext);


}

