package cn.ibaijia.test.sca.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.test.sca.dao.model.Order;
import cn.ibaijia.jsm.context.dao.model.Page;

/**
 * @author jsm_auto_gen
 * tableName:order_t
 */
public interface OrderMapper {

    /**
	 * 新增Order
	 */
    @Insert("<script>insert into order_t (orderNo,productId,number) values (#{orderNo},#{productId},#{number})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Order order);

    /**
	 * 批量新增Order
	 */
    @Insert("<script>insert into order_t (orderNo,productId,number) values <foreach collection='list' item='item' index='index' separator=','> (#{item.orderNo},#{item.productId},#{item.number})</foreach></script>")
	int addBatch(List<Order> orderList);

	/**
	 * 修改Order(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update order_t set id=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.orderNo,orderNo)'>,orderNo=#{orderNo}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.productId,productId)'>,productId=#{productId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.number,number)'>,number=#{number}</if> where id=#{id}</script>")
	int update(Order order); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from order_t t where t.id=#{id}</script>")
	Order findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from order_t t where t.id=#{id}</script>")
	Order findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from order_t where id=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from order_t t order by t.id desc</script>")
	List<Order> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<Order> pageList(Page<Order> page);
}