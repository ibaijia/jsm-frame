package cn.ibaijia.test.sca.dao.model;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author jsm_auto_gen
 * tableName:order_t
 */
public class Order extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, maxLen = 10, comments = "")
	public String orderNo;
	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, comments = "")
	public Long productId;
	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, comments = "")
	public Integer number;
}