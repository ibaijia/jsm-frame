package cn.ibaijia.test.sca.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.http.ann.HttpParam;
import cn.ibaijia.test.sca.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author jsm_auto_gen
 */
@RestController
@RequestMapping("/v1/test-seata")
@RefreshScope
public class TestSeataApi extends BaseRest {

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private OrderService orderService;


    @ApiOperation("下单by httpMapper AT OR XA")
    @RequestMapping(value = "/order-by-http-mapper", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> orderByHttpMapper(@RequestParam("throwError") boolean throwError) {
        orderService.orderByHttpMapper(throwError);
        return success(true);
    }


    @ApiOperation("下单by restTemplate  AT OR XA")
    @RequestMapping(value = "/order-by-rest-template", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> orderByRestTemplate(@RequestParam("throwError") boolean throwError) {
        orderService.orderByRestTemplate(throwError);
        return success(true);
    }


    @ApiOperation("下单by TCC")
    @RequestMapping(value = "/order-tcc", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> orderTcc(@RequestParam("throwError") boolean throwError) {
        orderService.orderTcc(throwError);
        return success(true);
    }
    

}
