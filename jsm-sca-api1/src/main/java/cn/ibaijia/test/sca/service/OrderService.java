package cn.ibaijia.test.sca.service;

import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.elastic.util.JsonUtil;
import cn.ibaijia.test.sca.dao.mapper.OrderMapper;
import cn.ibaijia.test.sca.dao.model.Order;
import cn.ibaijia.test.sca.httpMapper.StockApiMapper;
import cn.ibaijia.test.sca.httpMapper.StockTccApiMapper;
import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author jsm_auto_gen
 */
@Service
public class OrderService extends BaseService {

    @Resource
    private OrderMapper orderMapper;
    @Resource
    private StockApiMapper stockApiMapper;
    @Resource
    private StockTccApiMapper stockTccApiMapper;
    @Resource
    private OrderTccService orderTccService;
    @Resource
    private RestTemplate restTemplate;


    public int deleteById(Long id) {
        return orderMapper.deleteById(id);
    }

    public Order findById(Long id) {
        Order dbOrder = orderMapper.findById(id);
        if (dbOrder == null) {
            notFound("id not found:" + id);
        }
        return dbOrder;
    }

    @GlobalTransactional
    public void orderByHttpMapper(boolean throwError) {
        logger.info("当前 XID: {}", RootContext.getXID());
        Order order = new Order();
        order.orderNo = "123";
        order.productId = 1L;
        order.number = 2;
        orderMapper.add(order);
        ReduceStockReq req = new ReduceStockReq();
        req.number = 2;
        req.id = 1L;
        stockApiMapper.reduce(req);
        if(throwError){
            throw new RuntimeException("假装出错了！");
        }
    }

    @GlobalTransactional
    public void orderByRestTemplate(boolean throwError) {
        logger.info("当前 XID: {}", RootContext.getXID());
        Order order = new Order();
        order.orderNo = "1234";
        order.productId = 1L;
        order.number = 2;
        orderMapper.add(order);

        ReduceStockReq req = new ReduceStockReq();
        req.number = 2;
        req.id = 1L;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestEntity = new HttpEntity(JsonUtil.toJsonString(req), headers);
//        HttpEntity requestEntity = new HttpEntity(JsonUtil.toJsonString(req));
        ResponseEntity<RestResp> response = restTemplate.exchange("http://jsm-sca-api2/jsm-sca-api2/v1/stock/reduce", HttpMethod.PUT, requestEntity, RestResp.class);
        logger.info(JsonUtil.toJsonString(response.getBody()));
        if(throwError){
            throw new RuntimeException("假装出错了！");
        }
    }

    @GlobalTransactional
    public void orderTcc(boolean throwError) {
        logger.info("当前 XID: {}", RootContext.getXID());
        Order order = new Order();
        order.orderNo = "123";
        order.productId = 1L;
        order.number = 2;
        orderMapper.add(order);
        ReduceStockReq req = new ReduceStockReq();
        req.number = 2;
        req.id = 1L;
        stockTccApiMapper.reduceTcc(req);
        if(throwError){
            throw new RuntimeException("假装出错了！");//这里异常会回滚
        }
    }

}