package cn.ibaijia.test.sca.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.Server;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author jsm_auto_gen
 */
@RestController
@RequestMapping("/v1/test-nacos")
@RefreshScope
public class TestNacosApi extends BaseRest {

    @Value("${useLocalCache:false}")
    private Boolean useLocalCache;

    @Resource
    public RestTemplate restTemplate;


    @ApiOperation("动态获取配置，配置dataId 规则 默认{application.name} {application.name}-{active}  {application.name}-{active}.{extension}")
    @RequestMapping(value = "/get-config", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> getConfig() {
        return success(useLocalCache);
    }

    @ApiOperation("调用远程服务，restTemplate, load balance")
    @RequestMapping(value = "/get-remote-user", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<String> getUser() {
        String res = restTemplate.getForObject("http://jsm-sca-api2/jsm-sca-api2/v1/provider/user", String.class);
        return success(res);
    }

//    @RequestMapping(value = "/nacos-rule", method = RequestMethod.GET)
//    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
//    public RestResp<Server> nacosRule() {
//        IRule iRule = SpringContext.getBean(IRule.class);
//        return success(iRule.choose("jsm-sca-api2"));
//    }

}
