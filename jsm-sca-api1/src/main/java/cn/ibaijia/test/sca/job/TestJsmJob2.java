package cn.ibaijia.test.sca.job;

import cn.ibaijia.jsm.annotation.ClusterJobLockAnn;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.job.BaseJsmJob;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author longzl
 */
@Component
public class TestJsmJob2 extends BaseJsmJob {

    /**
     * 这个不会动态刷新 配置写到配置Bean 加@RefreshScope 也没用
     */
    @Value("${jobValue:1}")
    private String jobValue;
    @Resource
    private JobConfig jobConfig;

    @Override
    public void onLoad() {
        // 这里可以做一些配置，比如检查并把Job写入配置中心或者DB
    }

    @Override
    public String getCron() {
        return jobConfig.getJob2Cron();
    }

    @Override
    public boolean isOpen() {
        return jobConfig.isJob2Open();
    }
    @ClusterJobLockAnn
    @Override
    public void doJob(){
        logger.info("TestJsmJob2:: begin.");
        logger.info("TestJsmJob2:: running getJob2Content:{} AppContext:{} jobValue:{}",jobConfig.getJob2Content(), AppContext.get("job2Content"),jobValue);
        logger.info("TestJsmJob2:: end.");
    }
}
