package cn.ibaijia.test.sca.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.ibaijia.test.sca.dao.model.User;
import cn.ibaijia.test.sca.dao.mapper.UserMapper;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.utils.BeanUtil;

/**
 * @author jsm_auto_gen
 */
@Service
public class UserService extends BaseService {

    @Resource
    private UserMapper userMapper;


    public int deleteById(Long id) {
        return userMapper.deleteById(id);
    }

    public User findById(Long id) {
        User dbUser = userMapper.findById(id);
        if ( dbUser == null ) {
            notFound("id not found:" + id);
        }
        return dbUser;
    }

}