package cn.ibaijia.test.sca.dao.model;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author jsm_auto_gen
 * tableName:account_t
 */
public class Account extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, comments = "")
	public Long money;

	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, comments = "")
	public Long userId;
}