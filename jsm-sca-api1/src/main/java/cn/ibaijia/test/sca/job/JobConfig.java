package cn.ibaijia.test.sca.job;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author longzl
 */
@Component
@RefreshScope
public class JobConfig {

    @Value("${job1Content:1}")
    private String job1Content;

    @Value("${job1Cron:5/10 * * * * ?}")
    private String job1Cron;

    @Value("${job1Open:true}")
    private boolean job1Open;


    @Value("${job2Content:1}")
    private String job2Content;

    @Value("${job2Cron:5/10 * * * * ?}")
    private String job2Cron;

    @Value("${job2Open:true}")
    private boolean job2Open;

    public String getJob1Content() {
        return job1Content;
    }

    public void setJob1Content(String job1Content) {
        this.job1Content = job1Content;
    }

    public String getJob1Cron() {
        return job1Cron;
    }

    public void setJob1Cron(String job1Cron) {
        this.job1Cron = job1Cron;
    }

    public boolean isJob1Open() {
        return job1Open;
    }

    public void setJob1Open(boolean job1Open) {
        this.job1Open = job1Open;
    }

    public String getJob2Content() {
        return job2Content;
    }

    public void setJob2Content(String job2Content) {
        this.job2Content = job2Content;
    }

    public String getJob2Cron() {
        return job2Cron;
    }

    public void setJob2Cron(String job2Cron) {
        this.job2Cron = job2Cron;
    }

    public boolean isJob2Open() {
        return job2Open;
    }

    public void setJob2Open(boolean job2Open) {
        this.job2Open = job2Open;
    }
}
