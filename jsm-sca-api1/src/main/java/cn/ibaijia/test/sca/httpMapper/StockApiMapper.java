package cn.ibaijia.test.sca.httpMapper;

import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.http.ann.HttpMapper;
import cn.ibaijia.jsm.http.ann.HttpPutMapping;
import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;

/**
 * @author longzl
 */
@HttpMapper(baseUri = "${service.stockApi}")
public interface StockApiMapper {

    /**
     * 减库存
     */
    @HttpPutMapping(uri = "/v1/stock/reduce")
    RestResp<Boolean> reduce(ReduceStockReq req);

}

