## jsm-frame是基于spring+mybatis的增量开发脚手架。

### 分支说明
| 分支     | 说明       |
|--------|----------|
| dev    | 开发中的分支   |
| master | 最新可用快照分支 |
| others | 历史可用分支   |

### 版本说明
| jsm 版本 | jdk 版本   |
|--------|----------|
| 历史版本   | &gt;= 8  |
| 2.2.0+ | &gt;= 11 |

### [快速开始](docs/GET_START.md)
### [常用功能文档](docs/FUNCTION_DOC.md)
### [缓存相关文档](docs/FUNCTION_CACHE.md)
### [EXCEL相关文档](docs/FUNCTION_EXCEL.md)
### [远程接口调用](docs/FUNCTION_HTTP_MAPPER.md)
### [JOB相关文档](docs/FUNCTION_JOB.md)
### [ELASTIC相关文档](docs/FUNCTION_ELASTIC.md)
### [进阶相关文档](docs/FUNCTION_SENIOR.md)
### [最佳实践文档](docs/FUNCTION_BEST.md)
### [前端插件文档](docs/FUNCTION_FRONT.md)

### maven引入
SpringBoot版本
``` xml
    <dependency>
         <groupId>cn.ibaijia</groupId>
         <artifactId>jsm-spring-boot-starter</artifactId>
         <version>latest</version>
    </dependency>
```
SpringCloud版本(nacos2.1.1+seata1.5.2)
``` xml
    <dependency>
         <groupId>cn.ibaijia</groupId>
         <artifactId>jsm-spring-cloud-starter</artifactId>
         <version>latest</version>
    </dependency>
```
