package cn.ibaijia.jsmdeed.spring.boot.autoconfigure;

import cn.ibaijia.jsm.license.LicenseService;
import cn.ibaijia.jsmdeed.spring.boot.autoconfigure.properties.DeedProperties;
import cn.ibaijia.soe.client.SoeClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author longzl
 */
@Configuration
@EnableConfigurationProperties({DeedProperties.class})
public class DeedConfig implements DisposableBean {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private DeedProperties deedProperties;

    private LicenseService licenseService;

    @Bean(name = {"licenseService"})
    @ConditionalOnMissingBean(LicenseService.class)
    public LicenseService createLicenseService() {
        if (deedProperties == null) {
            throw new RuntimeException("Deed Config not fund. see jsm.deed.*");
        }
        logger.info("createDeedClient host:{} port:{} appId:{} password:{} ", deedProperties.host, deedProperties.port, deedProperties.appId, deedProperties.password);
        SoeClient soeClient = new SoeClient(deedProperties.host, deedProperties.port, deedProperties.appId, deedProperties.password);
        licenseService = new DefaultLicenseService(soeClient,deedProperties);
        licenseService.init();
        return licenseService;
    }

    @Override
    public void destroy() {
        logger.debug("licenseService.destroy");
        if (licenseService != null) {
            licenseService.destroy();
        }
    }
}