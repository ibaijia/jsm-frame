package cn.ibaijia.jsmdeed.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.license.LicenseReq;
import cn.ibaijia.jsm.license.LicenseService;
import cn.ibaijia.jsm.license.LicenseVo;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.RsaUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsmdeed.spring.boot.autoconfigure.properties.DeedProperties;
import cn.ibaijia.soe.client.SoeClient;
import cn.ibaijia.soe.client.listener.SoeLoginFailedListener;
import cn.ibaijia.soe.client.listener.SoeLoginSuccessListener;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.session.SoeSession;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author longzl
 */
@Service
public class DefaultLicenseService extends BaseService implements LicenseService {

    private SoeClient soeClient;

    private LicenseVo licenseVo;

    //offline data
    private DeedProperties deedProperties;

    private static final String key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDYGjgfELB1Aie3CaV5fsCTZpB3PJ+R9yFB+C89YGwfvqsn4thuBHFXZuJs2HG8lKK2Fq0hfnlUFpkk3Wbpq2NZk/XSGHszLWUz1pdfCSePfKZaOtPCxOpSMsqpkXVxkbkeB5VMKHmKtSbTrxLYNzABHTUFYBkIvNF2XYM+lepXoQIDAQAB";

    public DefaultLicenseService(SoeClient soeClient, DeedProperties deedProperties) {
        this.soeClient = soeClient;
        this.deedProperties = deedProperties;
    }

    @Override
    public boolean init() {
        if (!StringUtil.isEmpty(deedProperties.data)) {
            try {
                String str = RsaUtil.decryptByPublicKey(deedProperties.data, key);
                licenseVo = JsonUtil.parseObject(str, new TypeReference<LicenseVo>() {
                });
                if (licenseVo == null) {
                    logger.error("license data read error.");
                    System.exit(0);
                }
                return true;
            } catch (Exception e) {
                logger.error("read license error.", e);
                System.exit(0);
            }
        }

        //连接soe client
        soeClient.setSoeLoginSuccessListener(new SoeLoginSuccessListener() {
            @Override
            public void run(SoeSession session) {
                loadLicense();
            }
        });
        soeClient.setSoeLoginFailedListener(new SoeLoginFailedListener() {
            @Override
            public void run(Short appId) {
                System.exit(0);
            }
        });
        soeClient.addService(new LicenseReloadService((short) 1001, "Reload"));
        soeClient.addService(new ClientCloseService((short) 1002, "Close"));
        soeClient.start();
        return true;
    }

    @Override
    public boolean check() {
        if (licenseVo == null) {
            logger.debug("licenseVo null");
            return false;
        }
        if (!licenseVo.isValid()) {
            logger.debug("licenseVo is not valid");
            return false;
        }
        return true;
    }

    @Override
    public boolean destroy() {
        if (soeClient != null) {
            try {
                soeClient.shutdown();
                return true;
            } catch (Exception e) {
                logger.error("soeClient.shutdown() error:", e);
                return false;
            }
        }
        return true;
    }

    private void loadLicense() {
        //获取数据
        SoeObject respObject = soeClient.sendSync(deedProperties.serviceId, (short) 1001, 1, new LicenseReq(deedProperties.clientId, deedProperties.clientKey));
        //验证数据
        if (respObject == null) {
            logger.error("license server connect failed.");
            System.exit(0);
        }
        String respStr = respObject.getBodyAsString();
        if (StringUtil.isEmpty(respStr)) {
            logger.error("license result is empty.");
            System.exit(0);
        }
        licenseVo = StringUtil.parseObject(respStr, new TypeReference<LicenseVo>() {
        });
        if (licenseVo == null) {
            logger.error("license data read error.");
            System.exit(0);
        }
        if (!StringUtil.isEmpty(licenseVo.authData)) {
            Map<String, String> authDataMap = StringUtil.parseObject(licenseVo.authData, new TypeReference<HashMap<String, String>>() {
            });
            if (authDataMap == null) {
                logger.error("authData read error.");
                System.exit(0);
            }
            for (Map.Entry<String, String> entry : authDataMap.entrySet()) {
                AppContext.set(entry.getKey(), entry.getValue());
            }
        }
    }

    class LicenseReloadService extends cn.ibaijia.soe.client.provider.BaseService {

        public LicenseReloadService(short id, String name) {
            super(id, name);
        }

        @Override
        public void doService(SoeObject soeObject) {
            if (soeObject.getFromId() == deedProperties.serviceId) {
                loadLicense();
            }
        }
    }

    class ClientCloseService extends cn.ibaijia.soe.client.provider.BaseService {

        public ClientCloseService(short id, String name) {
            super(id, name);
        }

        @Override
        public void doService(SoeObject soeObject) {
            if (soeObject.getFromId() == deedProperties.serviceId) {
                System.exit(0);
            }
        }
    }
}
