package cn.ibaijia.jsmsoe.spring.boot.autoconfigure;

import cn.ibaijia.jsmsoe.spring.boot.autoconfigure.properties.SoeProperties;
import cn.ibaijia.soe.client.SoeClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({SoeProperties.class})
//@ConditionalOnClass(MyBatisSystemException.class)
public class SoeConfig implements DisposableBean {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SoeProperties soeProperties;

    private SoeClient soeClient;

    @Bean(name = {"soeClient"})
    @ConditionalOnMissingBean(SoeClient.class)
    public SoeClient createSoeClient() {
        if (soeProperties == null) {
            throw new RuntimeException("Soe Config not fund.see soe.client.*");
        }
        logger.info("createSoeClient host:{} port:{} appId:{} password:{} ", soeProperties.host, soeProperties.port, soeProperties.appId, soeProperties.password);
        soeClient = new SoeClient(soeProperties.host, soeProperties.port, soeProperties.appId, soeProperties.password);
        soeClient.start();
        return soeClient;
    }

    @Override
    public void destroy() throws Exception {
        logger.debug("soeClient.shutdown");
        if (soeClient != null) {
            soeClient.shutdown();
        }
    }
}