package cn.ibaijia.creator.test.gen;


import cn.ibaijia.jsm.utils.FtlUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.junit.Test;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SpringCloudMybatisCreator {

    private static Logger logger = LogUtil.log(SpringCloudMybatisCreator.class);

    private String tplRootDir = "/META-INF/SpringCloudMybatis/";
    private String tplMainDir = tplRootDir+"/main/";
    private String tplTestDir = tplRootDir+"/test/";
    private String tplResourcesDir = tplRootDir+"/resources/";

    @Test
    public void createProject() {

        String groupId = "cn.cq";
        String artifactId = "tech";
        String projectName = "app-api";
        String jsmVersion = "2.2.0-SNAPSHOT";

        String dbHost = "192.168.0.200:3306";
        String dbName = "jsm-creator";
        String dbUser = "root";
        String dbPassword = "root";

        String redisHost = "192.168.0.200:6379";
        String redisOauth = "dev123";
        String redisIdx = "1";

        String projectPath = System.getProperty("user.dir") + "/genProjects/" + projectName;
        //gen pom
        Map<String, String> data = getProjectInfo(groupId, artifactId, projectName);
        data.put("dbHost", dbHost);
        data.put("dbName", dbName);
        data.put("dbUser", dbUser);
        data.put("dbPassword", dbPassword);
        data.put("redisHost", redisHost);
        data.put("redisOauth", redisOauth);
        data.put("redisIdx", redisIdx);
        data.put("jsmVersion", jsmVersion);
        String str = FtlUtil.build(this.getClass(), tplRootDir, data, "pom.xml.ftl");
        writeFile(str, projectPath + "/pom.xml", true);

        str = FtlUtil.build(this.getClass(), tplRootDir, data, ".gitignore.ftl");
        writeFile(str, projectPath + "/.gitignore", true);

        str = FtlUtil.build(this.getClass(), tplRootDir, data, "Dockerfile.ftl");
        writeFile(str, projectPath + "/Dockerfile", true);

        createResourceFile(data, projectPath);

        createJavaFile(data, projectPath);

        createTestFile(data, projectPath);

    }

    private void createJavaFile(Map<String, String> data, String projectPath) {
        String outputDir = "/src/main/java/";
        String tplDir = tplMainDir;
        String javaDir = null;
        try {
            javaDir = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "resources";
        } catch (IOException e) {
            logger.error("", e);
        }
        outputDir = outputDir + data.get("rootPkg").replace(".", File.separator);
        File restDirFile = new File(javaDir + tplDir);
        createDirectoryFile(data, projectPath, tplDir, outputDir, restDirFile);
    }

    private void createTestFile(Map<String, String> data, String projectPath) {
        String outputDir = "/src/test/java/";
        String tplDir = tplTestDir;
        String javaDir = null;
        try {
            javaDir = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "resources";
        } catch (IOException e) {
            logger.error("", e);
        }
        outputDir = outputDir + data.get("rootPkg").replace(".", File.separator);
        File restDirFile = new File(javaDir + tplDir);
        createDirectoryFile(data, projectPath, tplDir, outputDir, restDirFile);
    }

    private void createDirectoryFile(Map<String, String> data, String projectPath, String tplDir, String outputDir, File fileDirectory) {
        for (File file : fileDirectory.listFiles()) {
            if (file.isDirectory()) {
                makeDirs(projectPath + outputDir + File.separator + file.getName());
                createDirectoryFile(data, projectPath, tplDir + File.separator + file.getName(), outputDir + File.separator + file.getName(), file);
            } else {
                createFile(data, projectPath, tplDir, outputDir + File.separator, file.getName());
            }
        }
    }

    private void createResourceFile(Map<String, String> data, String projectPath) {
        String outputDir = "/src/main/resources/";
        String tplDir = tplResourcesDir;
        String resourceDir = null;
        try {
            resourceDir = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "resources";
        } catch (IOException e) {
            logger.error("", e);
        }
        File resourceDirFile = new File(resourceDir + tplDir);
        for (File file : resourceDirFile.listFiles()) {
            String fileName = file.getName();
            if (file.isDirectory()) {
                for (File subFile : file.listFiles()) {
                    if (!subFile.isDirectory()) {
                        createFile(data, projectPath, tplDir + fileName, outputDir + fileName + File.separator, subFile.getName());
                    }
                }
            } else {
                createFile(data, projectPath, tplDir, outputDir, fileName);
            }
        }
    }

    private void createFile(Map<String, String> data, String projectPath, String tplDir, String outputDir, String tplName) {
        String modelStr = FtlUtil.build(this.getClass(), tplDir, data, tplName);
        boolean modelRes = writeFile(modelStr, projectPath + outputDir + tplName.replace(".ftl", ""), true);
    }

    private Map<String, String> getProjectInfo(String groupId, String artifactId, String projectName) {
        Map<String, String> data = new HashMap<>();
        data.put("groupId", groupId);
        data.put("artifactId", artifactId);
        data.put("projectName", projectName);
        String rootPkg = groupId + "." + artifactId;
        data.put("rootPkg", rootPkg);
        data.put("daoPkg", rootPkg + ".dao");
        data.put("daoMapperPkg", rootPkg + ".dao.mapper");
        data.put("daoModelPkg", rootPkg + ".dao.model");
        data.put("servicePkg", rootPkg + ".service");
        data.put("jobPkg", rootPkg + ".job");
        data.put("restPkg", rootPkg + ".rest");
        data.put("restReqPkg", rootPkg + ".rest.req");
        data.put("restVoPkg", rootPkg + ".rest.vo");
        data.put("testPkg", rootPkg + ".test");
        data.put("constPkg", rootPkg + ".const");
        data.put("validatorPkg", rootPkg + ".rest.validator");

        return data;
    }

    private boolean writeFile(String text, String filePath, boolean override) {
        filePath = filePath.replace('\\', File.separatorChar).replace('/', File.separatorChar);
        makeDirs(filePath.substring(0, filePath.lastIndexOf(File.separatorChar)));
        logger.info("create file:{}", filePath);
        File modelFile = new File(filePath);
        if (!override && modelFile.exists()) {
            logger.warn("file:{} exists, ignore! override:{}", filePath, override);
            return false;
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            fos.write(text.getBytes());
            return true;
        } catch (Exception e) {
            logger.error("", e);
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                logger.error("", e);
            }
        }
    }

    private void makeDirs(String dirPath) {
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }


}
