package cn.ibaijia.creator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author jsm_auto_gen
 */
@MapperScan(basePackages={"cn.ibaijia.creator.dao.mapper"})
@SpringBootApplication(scanBasePackages = {"cn.ibaijia.creator"})
@EnableScheduling()
public class StartApp {

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }

}
