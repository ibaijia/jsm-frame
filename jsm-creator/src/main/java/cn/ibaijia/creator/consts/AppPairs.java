package cn.ibaijia.creator.consts;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author jsm_auto_gen
 */
public class AppPairs extends BasePairConstants {

	public static final String SESSION_KEY = "jedis_session_key";

	/**
     * 登录模块
     */
	public static final Pair<String> NO_LOGIN = new Pair<String>("1201", "需要登录");
	public static final Pair<String> NO_PERMISSION = new Pair<String>("1202", "无权限");
	public static final Pair<String> USER_EXISTS = new Pair<String>("1203", "帐号已经存在");
	public static final Pair<String> USER_OR_PASSWORD_ERROR = new Pair<String>("1204", "账号或密码错误!");
	public static final Pair<String> USER_NOT_EXISTS = new Pair<String>("1205", "帐号不存在!");
	public static final Pair<String> USER_DISABLE = new Pair<String>("1206", "该帐号已禁用，请联系管理员！");
	public static final Pair<String> PASSWORD_ERROR = new Pair<String>("1207", "原密码错误!");

}
