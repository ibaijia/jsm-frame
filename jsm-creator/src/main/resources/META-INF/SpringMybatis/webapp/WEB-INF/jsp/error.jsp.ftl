<#noparse><%@ page import="cn.ibaijia.jsm.consts.BasePairConstants" %>
<%@ page import="cn.ibaijia.jsm.rest.resp.RestResp" %>
<%@ page import="cn.ibaijia.jsm.utils.ResponseUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    RestResp restResp = new RestResp();
    if (response.getStatus() == 404) {
        restResp.setPair(BasePairConsts.NOT_FOUND);
    } else {
        restResp.setPair(BasePairConsts.ERROR);
    }
    restResp.code = response.getStatus();
    ResponseUtil.outputRestResp(response,restResp);
%>
</body>
</html></#noparse>