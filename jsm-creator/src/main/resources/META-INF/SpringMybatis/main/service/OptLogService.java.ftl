package ${servicePkg};

import cn.ibaijia.jsm.dao.model.OptLog;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.stat.JsmOptLogService;
import org.springframework.stereotype.Service;
import ${daoMapperPkg}.OptLogMapper;

import javax.annotation.Resource;

@Service
public class OptLogService implements LogService {
    @Resource
    private OptLogMapper optLogMapper;

    @Override
    public Long addLog(OptLog optLog) {
        return optLogMapper.add(optLog);
    }

    @Override
    public void pageList(Page<OptLog> page) {
        optLogMapper.pageList(page);
    }
}
