package ${daoMapperPkg};

import ${daoModelPkg}.Seq;
import org.apache.ibatis.annotations.Param;

public interface SeqMapper {

    public Seq findByName(@Param("name") String name);

}
