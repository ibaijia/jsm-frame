<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:p="http://www.springframework.org/schema/p"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:util="http://www.springframework.org/schema/util"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
    http://www.springframework.org/schema/context
    http://www.springframework.org/schema/context/spring-context-4.1.xsd
    http://www.springframework.org/schema/tx
    http://www.springframework.org/schema/tx/spring-tx-4.1.xsd
    http://www.springframework.org/schema/aop
    http://www.springframework.org/schema/aop/spring-aop-4.1.xsd
    http://www.springframework.org/schema/util
    http://www.springframework.org/schema/util/spring-util-4.1.xsd">

	<context:component-scan base-package="cn.ibaijia.jsm.jedis" />
<#noparse>
	<bean id="jedisPoolConfig" class="redis.clients.jedis.JedisPoolConfig">
		<property name="maxTotal">
			<value>${redis.pool.maxActive}</value>
		</property>
		<property name="maxIdle">
			<value>${redis.pool.maxIdle}</value>
		</property>
		<property name="testOnBorrow" value="false" />
		<property name="testOnReturn" value="true" />
	</bean>
	
	<bean id="shardedJedisPool" class="redis.clients.jedis.ShardedJedisPool" scope="singleton">
		<constructor-arg index="0" ref="jedisPoolConfig" />
		<constructor-arg index="1">
			<list>
				<bean class="redis.clients.jedis.JedisShardInfo">
					<constructor-arg name="host" value="${redis.uri}" />
				</bean>
			</list>
		</constructor-arg>
	</bean>
	<bean id="jedisPool" class="redis.clients.jedis.JedisPool" scope="singleton">
        <constructor-arg name="poolConfig" ref="jedisPoolConfig"/>
        <constructor-arg name="uri" value="${redis.uri}"/>
    </bean>
</beans>
</#noparse>