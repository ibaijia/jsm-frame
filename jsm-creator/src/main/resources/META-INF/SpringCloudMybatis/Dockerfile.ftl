FROM longzhili/openjdk:11-alpine-cn
# copy jar
ARG APP_NAME=${projectName}
<#noparse>

ARG ENV=default
ARG GIT_HASH=default

ENV JSM_ENV=${ENV}
ENV JSM_GIT_HASH=${GIT_HASH}
ENV APP_NAME=${APP_NAME}
ENV PORT 8080

COPY target/${APP_NAME}-1.0.0-SNAPSHOT.jar /
EXPOSE ${PORT}
HEALTHCHECK --interval=60s --timeout=10s --retries=10 CMD wget -O/dev/null -q http://127.0.0.1:${PORT}/${APP_NAME}/v1/hello || exit 1
ENV JAVA_OPTS="-server -Xmx500M -Xms256M -Xmn200M -Xss256K -Duser.timezone=GMT+08"
ENTRYPOINT java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /${APP_NAME}-1.0.0-SNAPSHOT.jar
</#noparse>