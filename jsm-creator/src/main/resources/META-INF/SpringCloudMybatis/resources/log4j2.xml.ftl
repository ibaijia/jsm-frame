<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="INFO" packages="cn.ibaijia.jsm.log4j2">
    <Properties>
        <Property name="logName">${projectName}</Property>
    </Properties>
    <Appenders>
        <Console name="console" target="SYSTEM_OUT">
            <PatternLayout pattern="[%d{MM-dd HH:mm:ss}] %p [%c][%t][%z] - %m%n"/>
        </Console>
        <RollingFile name="file" fileName="logs/${r'${logName}'}.log"  filePattern="${r'logs/${logName}.log.%d{yyyy-MM-dd}'}">
            <PatternLayout pattern="[%d{MM-dd HH:mm:ss}] %p [%c][%t][%z] - %m%n"/>
            <Policies>
                <!-- 每天滚存日志 -->
                <TimeBasedTriggeringPolicy modulate="true" interval="1"/>
            </Policies>
            <DefaultRolloverStrategy>
                <Delete basePath="logs" maxDepth="1">
                    <IfFileName glob="*.log.*"/>
                    <!-- 日志保存时间 -->
                    <IfLastModified age="10d"/>
                </Delete>
            </DefaultRolloverStrategy>
        </RollingFile>
        <JsmAppender name="jsmAppender" >
            <PatternLayout pattern="[%d{MM-dd HH:mm:ss}] %p [%c][%t][%z] - %m%n"/>
        </JsmAppender>
    </Appenders>
    <Loggers>
        <logger name="cn.ibaijia.jsm" level="info"/>
        <logger name="${rootPkg}" level="debug"/>
        <logger name="${rootPkg}.dao.mapper" level="debug"/>
        <Root level="info">
            <AppenderRef ref="console"/>
            <AppenderRef ref="file"/>
            <AppenderRef ref="jsmAppender"/>
        </Root>
    </Loggers>
</Configuration>