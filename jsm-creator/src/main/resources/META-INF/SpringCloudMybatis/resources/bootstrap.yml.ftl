<#setting number_format="#"/>
spring:
  application:
    name: ${projectName}
  profiles:
    active: env
  freemarker:
    checkTemplateLocation: false
  messages:
    basename: messages/message
  cloud:
    nacos:
      config:
        server-addr: '192.168.0.17:8848'
        file-extension: yaml
      discovery:
        server-addr: '192.168.0.17:8848'
    # seata 服务分组，要与服务端nacos-config.txt中service.vgroup_mapping的后缀对应
#    alibaba:
#      seata.tx-service-group: my_test_tx_group

seata:
  enabled: true
  tx-service-group: default-tx-group #事务分组配置（在v1.5之后默认值为default_tx_group）
  # 一定要单独指定，默认是AT模式 XA模式不能与AT模式共存，两种模式在同一个服务中只能存在其一 ,不直接访问数据库的服务不用配置
  data-source-proxy-mode: AT
  config:
    type: nacos
    nacos:
      server-addr: '192.168.0.17:8848'
      namespace: df95c8ea-6d2c-4869-ab59-d033c8e451f3
      group: SEATA_GROUP
      username: nacos
      password: nacos
      data-id: seata-server.properties
  registry:
    type: nacos
    nacos:
      server-addr: '192.168.0.17:8848'
      application: seata-server #Seata服务名（应与seata-server实际注册的服务名一致）
      namespace: df95c8ea-6d2c-4869-ab59-d033c8e451f3
      group: SEATA_GROUP
      username: nacos
      password: nacos

server:
  port: 8080
  servlet:
    context-path: /${projectName}

logging:
  config: classpath:log4j2.xml

jsm:
  appName: ${projectName}
  clusterId: ${projectName}
  dev.model: true
  json:
    showNull: true
    booleanNullFalse: false
    numberNullZero: false
    stringNullEmpty: false
    arrayNullEmpty: false
    namingStrategy: lowerCamelCase # lowerCamelCase snakeCase upperCamelCase lowerCase kebabCase
  log:
    type: none #  none console(consoleStrategy) elastic(elasticLogStrategy) redis(redisLogStrategy) kafka(kafkaLogStrategy) rabbitmq(rabbitmqLogStrategy) custom(自定义的customLogStrategy)
    idx: jsm_log_idx
  apiStat:
    type: none #  none console elastic kafka rabbitmq custom
    idx: jsm_api_stat_idx
  sysStat:
    type: none
    idx: jsm_sys_stat_idx
    interval: 300
  alarm:
    type: none
    idx: jsm_alarm_idx
  optLog:
    type: none
    idx: jsm_opt_log_idx
  cors:
    open: true
  cache:
    removeOnUpdate: true
  slow:
    apiLimit: 5000
    sqlLimit: 5000
  web:
    maxUploadSize: 209715200
    encoding: UTF-8
    staticDir: /static/
  mybatis:
    typeAliasesPackage: ${daoModelPkg},${restVoPkg}
    mapperLocations:
      - classpath*:mappers/*Mapper.xml
    pageDbDialect: mysql
  swagger:
    enable: true
    restPackage: '${restPkg}'
    title: '${projectName} Restful APIs'
    description: '${projectName} APIs'
    version: '1.0'
    headers: '' # ${r'token:{dev_token}'},
