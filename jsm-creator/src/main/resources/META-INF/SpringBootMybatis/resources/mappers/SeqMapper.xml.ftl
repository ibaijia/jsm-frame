<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://www.mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${daoMapperPkg}.SeqMapper">

    <select id="findByName" resultType="Seq" parameterType="String">
        select t.* from seq_t t where t.name=${r'#{name}'} limit 0,1
    </select>

</mapper>
