<#setting number_format="#"/>
spring:
  profiles:
    active: env
  freemarker:
    checkTemplateLocation: false
  messages:
    basename: messages/message

server:
  port: 8080
  servlet:
    context-path: /${projectName}

logging:
  config: classpath:log4j2.xml

jsm:
  appName: ${projectName}
  clusterId: ${projectName}
  dev.model: true
  json:
    showNull: true
    booleanNullFalse: false
    numberNullZero: false
    stringNullEmpty: false
    arrayNullEmpty: false
    namingStrategy: lowerCamelCase # lowerCamelCase snakeCase upperCamelCase lowerCase kebabCase
  log:
    type: none #  none console(consoleStrategy) elastic(elasticLogStrategy) redis(redisLogStrategy) kafka(kafkaLogStrategy) rabbitmq(rabbitmqLogStrategy) custom(自定义的customLogStrategy)
    idx: jsm_log_idx
  apiStat:
    type: none #  none console elastic kafka rabbitmq custom
    idx:  jsm_api_stat_idx
  sysStat:
    type: none
    idx: jsm_sys_stat_idx
    interval: 300
  alarm:
    type: none
    idx: jsm_alarm_idx
  optLog:
    type: none
    idx: jsm_opt_log_idx
  cors:
    open: true
  cache:
    removeOnUpdate: true
  slow:
    apiLimit: 5000
    sqlLimit: 5000
  web:
    maxUploadSize: 209715200
    encoding: UTF-8
    staticDir: /static/
  mybatis:
    typeAliasesPackage: ${daoModelPkg},${restVoPkg}
    mapperLocations:
     - classpath*:mappers/*Mapper.xml
    pageDbDialect: mysql
  swagger:
    enable: true
    restPackage: '${restPkg}'
    title: '${projectName} Restful APIs'
    description: '${projectName} APIs'
    version: '1.0'
    headers: '${r'jsm-token:{dev_token}'}'

