<#setting number_format="#"/>
server:
  port: 8080

jsm:
  db:
    master:
      driverClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://${dbHost}/${dbName}?useUnicode=true&autoReconnect=true&characterEncoding=UTF-8&useSSL=true&serverTimezone=Asia/Shanghai
      username: ${dbUser}
      password: ${dbPassword}
      initialSize: 2
      maxActive: 10
      maxIdle: 4
      minIdle: 2
      validationQuery: SELECT 1
      testOnBorrow: true
    slave:
      driverClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://${dbHost}/${dbName}?useUnicode=true&autoReconnect=true&characterEncoding=UTF-8&useSSL=true&serverTimezone=Asia/Shanghai
      username: ${dbUser}
      password: ${dbPassword}
      initialSize: 2
      maxActive: 10
      maxIdle: 4
      minIdle: 2
      validationQuery: SELECT 1
      testOnBorrow: true
  redis:
    nodes: ${redisHost}
    password: ${redisOauth}
    dbIdx: ${redisIdx}
    pool:
      maxTotal: 10
      maxIdle: 8
      minIdle: 5
      maxWaitMillis: 20000
      testOnBorrow: false
      testOnReturn: true
  swagger:
    enable: true