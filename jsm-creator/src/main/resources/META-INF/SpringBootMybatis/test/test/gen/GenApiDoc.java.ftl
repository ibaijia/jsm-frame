package ${rootPkg}.test.gen;

import cn.ibaijia.jsm.gen.GenService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author jsm_auto_gen
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GenApiDoc {

    @Resource
    private GenService genService;

    @Test
    public void genApiDoc() {
        genService.genApiHtml();
    }

}
