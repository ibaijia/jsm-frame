package ${rootPkg};

import cn.ibaijia.jsm.http.ann.HttpMapperScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author jsm_auto_gen
 */
@HttpMapperScan(basePackages = {"${rootPkg}.httpmapper"})
@MapperScan(basePackages={"${daoMapperPkg}"})
@SpringBootApplication(scanBasePackages = {"${rootPkg}"})
@EnableScheduling()
public class StartApp {

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }

}
