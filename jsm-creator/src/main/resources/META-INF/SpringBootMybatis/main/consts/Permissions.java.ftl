package ${rootPkg}.consts;

import cn.ibaijia.jsm.consts.BasePermissionConstants;

/**
 * @author jsm_auto_gen
 */
public class Permissions extends BasePermissionConstants {

    /**
     * 系统管理
     **/
	public static final String SYSTEM_MANAGEMENT = "SYSTEM_MANAGEMENT";
    /**
     * 系统日志
     **/
	public static final String SYSTEM_LOG = "SYSTEM_LOG";
    /**
     * 用户管理
     **/
	public static final String USER_MANAGEMENT = "USER_MANAGEMENT";
    /**
     * 角色管理
     **/
	public static final String ROLE_MANAGEMENT = "ROLE_MANAGEMENT";

}
