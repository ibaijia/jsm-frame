package ${rootPkg}.consts;
import cn.ibaijia.jsm.consts.BaseConstants;


/**
 * @author jsm_auto_gen
 */
public class AppConstants extends BaseConstants {

	public static final String SESSION_USERNAME_KEY = "username";
	public static final String SESSION_COMPANY_ID_KEY = "companyId";

	/**
	 * 日志类型
	 */
	public static final String LOG_TYPE_USER = "用户管理";
	public static final String LOG_TYPE_ROLE = "角色管理";
	public static final String LOG_TYPE_DIC = "字典管理";
	public static final String LOG_TYPE_RECEIVE_ADDRESS = "地区管理";


}
