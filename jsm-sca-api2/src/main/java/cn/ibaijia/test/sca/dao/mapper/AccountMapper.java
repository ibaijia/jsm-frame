package cn.ibaijia.test.sca.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.test.sca.dao.model.Account;
import cn.ibaijia.jsm.context.dao.model.Page;

/**
 * @author jsm_auto_gen
 * tableName:account_t
 */
public interface AccountMapper {

    /**
	 * 新增Account
	 */
    @Insert("<script>insert into account_t (money) values (#{money})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Account account);

    /**
	 * 批量新增Account
	 */
    @Insert("<script>insert into account_t (money) values <foreach collection='list' item='item' index='index' separator=','> (#{item.money})</foreach></script>")
	int addBatch(List<Account> accountList);

	/**
	 * 修改Account(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update account_t set id=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.money,money)'>,money=#{money}</if> where id=#{id}</script>")
	int update(Account account); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from account_t t where t.id=#{id}</script>")
	Account findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from account_t t where t.id=#{id}</script>")
	Account findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from account_t where id=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from account_t t order by t.id desc</script>")
	List<Account> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<Account> pageList(Page<Account> page);
}