package cn.ibaijia.test.sca.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.test.sca.dao.model.User;
import cn.ibaijia.jsm.context.dao.model.Page;

/**
 * @author jsm_auto_gen
 * tableName:user_t
 */
public interface UserMapper {

    /**
	 * 新增User
	 */
    @Insert("<script>insert into user_t (name) values (#{name})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(User user);

    /**
	 * 批量新增User
	 */
    @Insert("<script>insert into user_t (name) values <foreach collection='list' item='item' index='index' separator=','> (#{item.name})</foreach></script>")
	int addBatch(List<User> userList);

	/**
	 * 修改User(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update user_t set id=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if> where id=#{id}</script>")
	int update(User user); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from user_t t where t.id=#{id}</script>")
	User findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from user_t t where t.id=#{id}</script>")
	User findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from user_t where id=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from user_t t order by t.id desc</script>")
	List<User> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<User> pageList(Page<User> page);
}