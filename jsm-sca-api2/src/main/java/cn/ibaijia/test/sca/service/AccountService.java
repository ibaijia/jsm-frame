package cn.ibaijia.test.sca.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.ibaijia.test.sca.dao.model.Account;
import cn.ibaijia.test.sca.dao.mapper.AccountMapper;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.utils.BeanUtil;

/**
 * @author jsm_auto_gen
 */
@Service
public class AccountService extends BaseService {

    @Resource
    private AccountMapper accountMapper;


    public int deleteById(Long id) {
        return accountMapper.deleteById(id);
    }

    public Account findById(Long id) {
        Account dbAccount = accountMapper.findById(id);
        if ( dbAccount == null ) {
            notFound("id not found:" + id);
        }
        return dbAccount;
    }

}