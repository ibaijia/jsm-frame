package cn.ibaijia.test.sca.dao.model;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author jsm_auto_gen
 * tableName:user_t
 */
public class User extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, maxLen = 50, comments = "")
	public String name;
}