package cn.ibaijia.test.sca.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.elastic.util.JsonUtil;
import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;
import cn.ibaijia.test.sca.service.StockService;
import io.seata.core.context.RootContext;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author jsm_auto_gen
 */
@RestController
@RequestMapping("/v1/stock")
public class StockApi extends BaseRest {

    @Resource
    private StockService stockService;

    @RequestMapping(value = "/reduce", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> reduce(@RequestBody ReduceStockReq req) {
        logger.info("xid:{}", RootContext.getXID());
        stockService.reduce(req);
        return success(true);
    }

    @RequestMapping(value = "/reduce-tcc", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<Boolean> reduceTcc(@RequestBody ReduceStockReq req) {
        BusinessActionContext businessActionContext = BusinessActionContextUtil.getContext();
        logger.info("xid:{} businessActionContext:{}", RootContext.getXID(), JsonUtil.toJsonString(businessActionContext));
        stockService.reduceTccPrepare(req);
        return success(false);
    }

    @RequestMapping(value = "/reduce-tcc-confirm", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public Boolean reduceTccConfirm(@RequestBody BusinessActionContext actionContext) {
        logger.info("xid:{} actionContext:{}", RootContext.getXID(), JsonUtil.toJsonString(actionContext));
        logger.info("reduceTccConfirm,清除准备补偿的日志或者数据");
        return true;
    }

    @RequestMapping(value = "/reduce-tcc-cancel", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public Boolean reduceTccCancel(@RequestBody BusinessActionContext actionContext) {
        logger.info("xid:{} actionContext:{}", RootContext.getXID(), JsonUtil.toJsonString(actionContext));
        logger.info("reduceTccCancel,补偿操作");
        return true;
    }


}
