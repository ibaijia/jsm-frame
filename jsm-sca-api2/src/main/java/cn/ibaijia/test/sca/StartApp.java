package cn.ibaijia.test.sca;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author jsm_auto_gen
 */
@MapperScan(basePackages={"cn.ibaijia.test.sca.dao.mapper"})
@SpringBootApplication(scanBasePackages = {"cn.ibaijia.test.sca"})
@EnableScheduling()
@EnableDiscoveryClient
public class StartApp {

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }

}
