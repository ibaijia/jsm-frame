package cn.ibaijia.test.sca.dao.mapper;

import org.apache.ibatis.annotations.*;
import java.util.List;
import cn.ibaijia.test.sca.dao.model.Stock;
import cn.ibaijia.jsm.context.dao.model.Page;

/**
 * @author jsm_auto_gen
 * tableName:stock_t
 */
public interface StockMapper {

    /**
	 * 新增Stock
	 */
    @Insert("<script>insert into stock_t (stock,productId) values (#{stock},#{productId})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Stock stock);

    /**
	 * 批量新增Stock
	 */
    @Insert("<script>insert into stock_t (stock,productId) values <foreach collection='list' item='item' index='index' separator=','> (#{item.stock},#{item.productId})</foreach></script>")
	int addBatch(List<Stock> stockList);

	/**
	 * 修改Stock(配合findByIdForUpdate更佳)
	 */
    @Update("<script>update stock_t set id=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.stock,stock)'>,stock=#{stock}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.productId,productId)'>,productId=#{productId}</if> where id=#{id}</script>")
	int update(Stock stock); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from stock_t t where t.id=#{id}</script>")
	Stock findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
    @Select("<script>select t.* from stock_t t where t.id=#{id}</script>")
	Stock findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
    @Delete("<script>delete from stock_t where id=#{id}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from stock_t t order by t.id desc</script>")
	List<Stock> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 */
	List<Stock> pageList(Page<Stock> page);
}
