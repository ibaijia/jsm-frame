package cn.ibaijia.test.sca.service;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.elastic.util.JsonUtil;
import cn.ibaijia.test.sca.dao.mapper.StockMapper;
import cn.ibaijia.test.sca.dao.model.Stock;
import cn.ibaijia.test.sca.rest.req.stock.ReduceStockReq;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author jsm_auto_gen
 */
@Service
public class StockService extends BaseService {

    @Resource
    private StockMapper stockMapper;


    public int deleteById(Long id) {
        return stockMapper.deleteById(id);
    }

    public Stock findById(Long id) {
        Stock dbStock = stockMapper.findById(id);
        if (dbStock == null) {
            notFound("id not found:" + id);
        }
        return dbStock;
    }

    public void reduce(ReduceStockReq req) {
        Stock stock = stockMapper.findByIdForUpdate(req.id);
        if (stock == null) {
            failed("库存不存在");
        }
        if (stock.stock < req.number) {
            failed("库存不足");
        }
        stock.stock = stock.stock - req.number;
        stockMapper.update(stock);
    }

    /**
     * TCC模式定义中提到：如果confirm或者cancel方法执行失败，要一直重试直到成功。
     *
     * @param req
     */
    public void reduceTccPrepare(ReduceStockReq req) {
        logger.info("reduceTccPrepare req:{}", JsonUtil.toJsonString(req));
        //TODO 其它操作
        Stock stock = stockMapper.findById(req.id);
        if (stock == null) {
            failed("库存不存在");
        }
        if (stock.stock < req.number) {
            failed("库存不足");
        }
        stock.stock = stock.stock - req.number;
        stockMapper.update(stock);
//        failed("远程confirm的异常"); //provider异常，只要consumer能抛出异常也一样会rollback
    }

//    public void reduceTccConfirm(BusinessActionContext actionContext, ReduceStockReq req) {
//        logger.info("reduceTccConfirm: req:{}", JsonUtil.toJsonString(req));
//        Stock stock = stockMapper.findByIdForUpdate(req.id);
//        if (stock == null) {
//            failed("库存不存在");
//        }
//        if (stock.stock < req.number) {
//            failed("库存不足");
//        }
//        stock.stock = stock.stock - req.number;
//        stockMapper.update(stock);
//    }
//
//    public void reduceTccCancel(BusinessActionContext actionContext, ReduceStockReq req) {
//        logger.info("reduceTccCancel: req:{}", JsonUtil.toJsonString(req));
//        //TODO 异常后其它操作的补偿
//    }
}