package cn.ibaijia.test.sca.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jsm_auto_gen
 */
@RestController
@RequestMapping("/v1/provider")
public class ProviderApi extends BaseRest {


    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    public RestResp<String> userInfo() {
        logger.info("traceId:{}", WebContext.getTraceId());
        return success("longzl");
    }


}
