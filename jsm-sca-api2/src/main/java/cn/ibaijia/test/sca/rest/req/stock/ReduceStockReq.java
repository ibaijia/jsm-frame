package cn.ibaijia.test.sca.rest.req.stock;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/11/2 10:36
 */
public class ReduceStockReq implements ValidateModel {

    @FieldAnn(required = false, comments = "")
    public Integer number;

    @FieldAnn(required = false, comments = "")
    public Long id;

}
