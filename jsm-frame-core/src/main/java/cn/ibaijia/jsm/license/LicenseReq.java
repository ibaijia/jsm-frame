package cn.ibaijia.jsm.license;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class LicenseReq implements ValidateModel {

    public LicenseReq(String clientId, String clientKey) {
        this.clientId = clientId;
        this.clientKey = clientKey;
    }

    @FieldAnn(required = false, maxLen = 30, comments = "客户端ID")
	public String clientId;

    @FieldAnn(required = false, maxLen = 50, comments = "客户端KEY")
	public String clientKey;

}