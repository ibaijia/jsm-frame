package cn.ibaijia.jsm.license;

public interface LicenseService {

    boolean init();

    boolean check();

    boolean destroy();

}
