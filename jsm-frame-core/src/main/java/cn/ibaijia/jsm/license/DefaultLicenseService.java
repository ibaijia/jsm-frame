package cn.ibaijia.jsm.license;

import org.springframework.stereotype.Service;

/**
 * @author longzl
 */
@Service
public class DefaultLicenseService implements LicenseService {
    @Override
    public boolean init() {
        return true;
    }

    @Override
    public boolean check() {
        return true;
    }

    @Override
    public boolean destroy() {
        return false;
    }
}
