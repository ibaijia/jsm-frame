package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.IpUtil;

/**
 * @author longzl
 */
public class Alarm implements ValidateModel {
    @FieldAnn(required = false, comments = "发生时间")
    public Long time;
    @FieldAnn(required = false, comments = "级别 0 ERROR 1 WARN 2 INFO 3 DEBUG")
    public int level = 0;
    @FieldAnn(required = false, maxLen = 50, comments = "名称")
    public String name;
    @FieldAnn(required = false, maxLen = 100, comments = "描述")
    public String comments;
    @FieldAnn(required = false, maxLen = 50, comments = "请求ID")
    public String traceId = WebContext.getTraceId();
    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId = AppContext.getClusterId();
    @FieldAnn(required = false, maxLen = 50, comments = "IP")
    public String ip = IpUtil.getHostIp();

    public Alarm(int level, String name, String comments) {
        this.level = level;
        this.name = name;
        this.comments = comments;
    }

    public Alarm(String name, String comments) {
        this.name = name;
        this.comments = comments;
    }

    public Alarm() {
    }
}
