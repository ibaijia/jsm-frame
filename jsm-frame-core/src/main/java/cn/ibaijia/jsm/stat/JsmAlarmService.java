package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.stat.model.Alarm;

import java.util.List;

/**
 * @author longzl
 */
public interface JsmAlarmService {

    public void add(Alarm alarm);

    void clear();

    List<Alarm> pull();
}
