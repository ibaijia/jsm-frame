package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.stat.model.SystemStat;

/**
 * @author longzl
 */
public interface JsmSysStatService {

    public void add(SystemStat sysStat);

    public void writeNow();

    public void start();

}
