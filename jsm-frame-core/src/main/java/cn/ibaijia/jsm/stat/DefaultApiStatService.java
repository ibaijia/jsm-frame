package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.stat.model.ApiStat;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.springframework.stereotype.Service;

/**
 * @author longzl
 */
@Service
public class DefaultApiStatService extends BaseService implements JsmApiStatService {

    private int slowApiLimit;

    private LogStrategy logStrategy;

    public DefaultApiStatService() {
    }

    private void loadLogStrategy() {
        if (logStrategy == null) {
            this.slowApiLimit = AppContext.getAsInteger(AppContextKey.SLOW_API_LIMIT, 3000);
            String logType = AppContext.get(AppContextKey.API_STAT_TYPE);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(logType)) {
                logStrategy = SpringContext.getBean("consoleStrategy");
            } else {
                logStrategy = SpringContext.getBean(logType + "ApiStatStrategy");
            }
        }
    }

    @Override
    public void add(ApiStat apiStat) {
        loadLogStrategy();
        if (apiStat.spendTime > slowApiLimit) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_SLOW_API, StringUtil.toJson(apiStat)));
        }
        if (logStrategy != null) {
            logStrategy.write(apiStat);
        } else {
            logger.warn(JsonUtil.toJsonString(apiStat));
        }
    }
}
