package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.stat.model.ApiStat;

/**
 * @author longzl
 */
public interface JsmApiStatService {

    public void add(ApiStat apiStat);

}
