package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.dao.model.OptLog;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.utils.JsonUtil;
import org.springframework.stereotype.Service;

/**
 * @Author: LongZL
 * @Date: 2021/11/23 16:31
 */
@Service
public class DefaultOptLogService extends BaseService implements JsmOptLogService {

    private LogStrategy logStrategy;

    public DefaultOptLogService() {
    }

    private void loadLogStrategy() {
        if (logStrategy == null) {
            String logType = AppContext.get(AppContextKey.OPT_LOG_TYPE);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(logType)) {
                logStrategy = SpringContext.getBean("consoleStrategy");
            } else {
                logStrategy = SpringContext.getBean(logType + "OptLogStrategy");
            }
        }
    }

    @Override
    public void add(OptLog optLog) {
        loadLogStrategy();
        if (logStrategy != null) {
            logStrategy.write(optLog);
        } else {
            logger.warn(JsonUtil.toJsonString(optLog));
        }
    }

}
