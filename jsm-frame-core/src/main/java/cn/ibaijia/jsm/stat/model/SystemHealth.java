package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

import java.util.List;

public class SystemHealth implements ValidateModel {

    @FieldAnn(comments = "系统状态")
    public SystemStat systemStat;

    @FieldAnn(comments = "告警信息列表")
    public List<Alarm> alarmList;

}
