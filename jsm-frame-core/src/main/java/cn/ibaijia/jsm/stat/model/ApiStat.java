package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.context.AppContext;

/**
 * @author longzl
 */
public class ApiStat extends BaseModel {
    /**
     * defaultVal:NULL
     * comments:c
     */
    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId = AppContext.getClusterId();
    /**
     * defaultVal:NULL
     * comments:请求ID
     */
    @FieldAnn(required = false, maxLen = 50, comments = "请求ID")
    public String traceId;
    /**
     * defaultVal:NULL
     * comments:URL
     */
    @FieldAnn(required = false, maxLen = 250, comments = "URL")
    public String url;
    /**
     * defaultVal:NULL
     * comments:Method
     */
    @FieldAnn(required = false, maxLen = 10, comments = "Method")
    public String method;
    /**
     * defaultVal:NULL
     * comments:开始时间
     */
    @FieldAnn(required = false, comments = "开始时间")
    public Long beginTime;
    /**
     * defaultVal:NULL
     * comments:结束时间
     */
    @FieldAnn(required = false, comments = "结束时间")
    public Long endTime;
    /**
     * defaultVal:NULL
     * comments:耗时(毫秒)
     */
    @FieldAnn(required = false, comments = "耗时(毫秒)")
    public Long spendTime;
    /**
     * defaultVal:NULL
     * comments:Http响应代码
     */
    @FieldAnn(required = false, comments = "Http响应代码")
    public Integer httpStatus;
    /**
     * defaultVal:NULL
     * comments:业务响应代码
     */
    @FieldAnn(required = false, maxLen = 20, comments = "业务响应代码")
    public String respCode;
    /**
     * defaultVal:NULL
     * comments:创建时间
     */
    @FieldAnn(required = false, comments = "时间")
    public Long time;
}
