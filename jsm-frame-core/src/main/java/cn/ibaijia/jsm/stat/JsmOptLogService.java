package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.dao.model.OptLog;

/**
 * @author longzl
 */
public interface JsmOptLogService {

	public void add(OptLog optLog);

}
