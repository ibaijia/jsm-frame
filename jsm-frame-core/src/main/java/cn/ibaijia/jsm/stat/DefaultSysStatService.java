package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.stat.model.SystemStat;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author longzl
 */
@Service
public class DefaultSysStatService extends BaseService implements JsmSysStatService {

    private LogStrategy logStrategy;

    @Value("${" + AppContextKey.SYS_STAT_INTERVAL + ":600}")
    private Integer interval;

    public DefaultSysStatService() {
    }

    private void loadLogStrategy() {
        if (logStrategy == null) {
            String logType = AppContext.get(AppContextKey.SYS_STAT_TYPE);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(logType)) {
                logStrategy = SpringContext.getBean("consoleStrategy");
            } else {
                logStrategy = SpringContext.getBean(logType + "SysStatStrategy");
            }
        }
    }

    @Override
    public void add(SystemStat sysStat) {
        loadLogStrategy();
        if (logStrategy != null) {
            logStrategy.write(sysStat);
        } else {
            logger.warn(JsonUtil.toJsonString(sysStat));
        }
    }

    @Override
    public void writeNow() {
        this.add(SystemUtil.getSystemStat());
    }

    @Override
    public void start() {
        try {
            logger.info("start JsmSysStatService delay:60 interval:{}", interval);
            ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1);
            scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    writeNow();
                }
            }, 60, interval, TimeUnit.SECONDS);
        } catch (Exception e) {
            logger.error("start JsmSysStatService error.", e);
        }
    }

}
