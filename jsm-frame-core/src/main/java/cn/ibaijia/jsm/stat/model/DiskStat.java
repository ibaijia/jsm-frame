package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class DiskStat implements ValidateModel {
    @FieldAnn(required = true, comments = "路径")
    public String path;
    @FieldAnn(required = true, comments = "总量容量")
    public long total;
    @FieldAnn(required = true, comments = "剩余容量")
    public long free;

}
