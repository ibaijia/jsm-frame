package cn.ibaijia.jsm.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public abstract class DefaultHttpClientProcessor implements HttpClientProcessor {
    @Override
    public void before(HttpRequestBase httpRequestBase) {
    }

    @Override
    public abstract void complete(HttpResponse response);

    @Override
    public abstract void exception(Exception e);

    @Override
    public void after() {
    }
}
