package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.apache.http.*;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.status.StatusLogger;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HttpAsyncClient {
    private org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();
    private Charset DEFAULT_CHARSET = Consts.UTF_8;
    private HttpHost proxyHost;
    private HttpClientContext httpClientContext = HttpClientContext.create();
    private PoolingHttpClientConnectionManager connManager;
    private RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build();
    private CookieStore cookieStore = new BasicCookieStore();
    private boolean withExtra = true;
    private boolean useCookie = true;
    private CloseableHttpAsyncClient closeableHttpAsyncClient = null;

    public HttpAsyncClient() {
        if (useCookie) {
            httpClientContext.setCookieStore(cookieStore);
            httpClientContext.setRequestConfig(requestConfig);
        }
        getClient();
    }

    public HttpAsyncClient(boolean useCookie) {
        this.useCookie = useCookie;
        if (useCookie) {
            httpClientContext.setCookieStore(cookieStore);
            httpClientContext.setRequestConfig(requestConfig);
        }
        getClient();
    }

    private void addAlarm(String method, String url, String errorMessage) {
        SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_HTTP_ASYNC_CLIENT, "" + method + "," + url + "," + errorMessage));
    }

    public boolean isWithExtra() {
        return withExtra;
    }

    public void setWithExtra(boolean withExtra) {
        this.withExtra = withExtra;
    }

    public void setSocksProxy(String ip, int port) {
        Registry<ConnectionSocketFactory> reg = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new MyConnectionSocketFactory())
                .register("https", new MySSLConnectionSocketFactory(SSLContexts.createSystemDefault()))
                .build();
        connManager = new PoolingHttpClientConnectionManager(reg);
        httpClientContext.setAttribute("socks.address", new InetSocketAddress(ip, port));
    }

    public HttpHost getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(HttpHost proxyHost) {
        this.proxyHost = proxyHost;
    }

    public void get(String url, FutureCallback futureCallback) {
        get(null, url, futureCallback);
    }

    public void get(List<Header> headers, String url, FutureCallback futureCallback) {
        logger.debug("get url:" + url);
        CloseableHttpAsyncClient httpclient = getClient();
        try {
            HttpGet httpGet = new HttpGet(url);
            if (headers != null) {
                httpGet.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpGet);
            httpclient.execute(httpGet, futureCallback);
        } catch (Exception e) {
            logger.error("http get error:" + url, e);
            addAlarm("get", url, e.getMessage());
        }
    }

    public void post(String url, List<NameValuePair> nvps, FutureCallback futureCallback) {
        post(null, url, nvps, futureCallback);
    }

    public void post(List<Header> headers, String url, List<NameValuePair> nvps, FutureCallback futureCallback) {
        logger.debug("post url:" + url);
        logger.debug("request nvps:{}", StringUtil.toJson(nvps));
        CloseableHttpAsyncClient httpclient = getClient();
        try {
            HttpPost httpPost = new HttpPost(url);
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpPost);
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(nvps, DEFAULT_CHARSET);
            logger.debug(EntityUtils.toString(httpEntity));
            httpPost.setEntity(httpEntity);
            httpclient.execute(httpPost, futureCallback);
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
            addAlarm("post", url, e.getMessage());
        }
    }

    public void post(String url, Object data, FutureCallback futureCallback) {
        if (data instanceof String) {
            post(null, url, (String) data, futureCallback);
        } else {
            post(null, url, StringUtil.toJson(data), futureCallback);
        }
    }

    public void post(String url, String strEntity, FutureCallback futureCallback) {
        post(null, url, strEntity, futureCallback);
    }

    public void post(List<Header> headers, String url, Object data, FutureCallback futureCallback) {
        if (data instanceof String) {
            post(headers, url, (String) data, futureCallback);
        } else {
            post(headers, url, StringUtil.toJson(data), futureCallback);
        }
    }

    public void post(List<Header> headers, String url, String strEntity, FutureCallback futureCallback) {
        logger.debug("post url:" + url);
        logger.debug("requestBody:{}", strEntity);
        CloseableHttpAsyncClient httpclient = getClient();
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity entity = new StringEntity(strEntity, DEFAULT_CHARSET);//解决中文乱码问题
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            } else {
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json; charset=UTF-8");
            }
            setExtraHeaders(httpPost);
            httpPost.setEntity(entity);
            httpclient.execute(httpPost, futureCallback);
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
            addAlarm("post", url, e.getMessage());
        }
    }

    public void put(String url, Object data, FutureCallback futureCallback) {
        if (data instanceof String) {
            put(null, url, (String) data, futureCallback);
        } else {
            put(null, url, StringUtil.toJson(data), futureCallback);
        }
    }

    public void put(List<Header> headers, String url, String strEntity, FutureCallback futureCallback) {
        logger.debug("put url:" + url);
        logger.debug("requestBody:{}", strEntity);
        CloseableHttpAsyncClient httpclient = getClient();
        try {
            HttpPut httpPut = new HttpPut(url);
            StringEntity entity = new StringEntity(strEntity, DEFAULT_CHARSET);//解决中文乱码问题
            if (headers != null) {
                httpPut.setHeaders(headers.toArray(new Header[headers.size()]));
            } else {
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json; charset=UTF-8");
            }
            setExtraHeaders(httpPut);
            httpPut.setEntity(entity);
            httpclient.execute(httpPut, futureCallback);
        } catch (Exception e) {
            logger.error("http put error:" + url, e);
            addAlarm("put", url, e.getMessage());
        }
    }

    public void delete(String url, FutureCallback futureCallback) {
        delete(null, url, futureCallback);
    }

    public void delete(List<Header> headers, String url, FutureCallback futureCallback) {
        logger.debug("delete url:" + url);
        String res = null;
        CloseableHttpAsyncClient httpclient = getClient();
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            if (headers != null) {
                httpDelete.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpDelete);
            httpclient.execute(httpDelete, futureCallback);
        } catch (Exception e) {
            logger.error("http delete error:" + url, e);
            addAlarm("delete", url, e.getMessage());
        }
    }


    public void postFile(String url, String filePath, FutureCallback futureCallback) {
        Map<String, Object> fileMap = new HashMap<>();
        fileMap.put("file", filePath);
        postFile(null, url, fileMap, null, futureCallback);
    }

    public void postFile(String url, String name, String filePath, FutureCallback futureCallback) {
        Map<String, Object> fileMap = new HashMap<>();
        fileMap.put(name, filePath);
        postFile(null, url, fileMap, null, futureCallback);
    }

    public void postFile(String url, Map<String, Object> fileMap, List<NameValuePair> nvps, FutureCallback futureCallback) {
        postFile(null, url, fileMap, nvps, futureCallback);
    }

    public void postFile(List<Header> headers, String url, Map<String, Object> fileMap, List<NameValuePair> nvps, FutureCallback futureCallback) {
        logger.debug("postFile url:{},fileMap Size:{}，nvps:{}", url, fileMap.size(), StringUtil.toJson(nvps));
        CloseableHttpAsyncClient httpclient = getClient();
        try {
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(5000)
                    .setConnectionRequestTimeout(1000)
                    .setSocketTimeout(600000)// 10mins
                    .build();
            HttpPost httpPost = new HttpPost(url);
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpPost);
            httpPost.setConfig(requestConfig);
            MultipartEntityBuilder requestBuilder = MultipartEntityBuilder.create()
                    .setCharset(DEFAULT_CHARSET)
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            if (fileMap != null) {
                for (Map.Entry<String, Object> entry : fileMap.entrySet()) {
                    Object value = entry.getValue();
                    if (value instanceof File) {
                        logger.debug("addBinaryBody file, {}:{}", entry.getKey(), ((File) entry.getValue()).getAbsolutePath());
                        requestBuilder.addBinaryBody(entry.getKey(), (File) entry.getValue());
                    } else if (value instanceof InputStream) {
                        logger.debug("addBinaryBody InputStream, {}", entry.getKey());
                        requestBuilder.addBinaryBody(entry.getKey(), (InputStream) entry.getValue());
                    } else if (value instanceof byte[]) {
                        logger.debug("addBinaryBody byte[], {}", entry.getKey());
                        requestBuilder.addBinaryBody(entry.getKey(), (byte[]) entry.getValue());
                    } else if (value instanceof String) {
                        logger.debug("addBinaryBody filePath, {}:{}", entry.getKey(), (String) entry.getValue());
                        requestBuilder.addBinaryBody(entry.getKey(), new File((String) entry.getValue()));
                    }
                }
            }
            if (nvps != null) {
                for (NameValuePair nvp : nvps) {
                    logger.debug("form addPart, {}:{}", nvp.getName(), nvp.getValue());
                    StringBody value = new StringBody(nvp.getValue(), ContentType.create("text/plain", DEFAULT_CHARSET));
                    requestBuilder.addPart(nvp.getName(), value);
                }
            }
            HttpEntity reqEntity = requestBuilder.build();
            httpPost.setEntity(reqEntity);
            httpclient.execute(httpPost, futureCallback);
        } catch (Exception e) {
            logger.error("http postFile error:" + url, e);
            addAlarm("post file", url, e.getMessage());
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                logger.error("httpclient close error!", e);
            }
        }
    }

    private void setExtraHeaders(HttpRequestBase httpEntity) {
        if (!withExtra) {
            return;
        }
        String requestId = WebContext.getTraceId();
        String at = WebContext.getRequestAt();
        String ht = WebContext.getRequestHt();
        String token = WebContext.getRequestToken();
        String authorization = WebContext.getRequestAuthorization();
        if (!StringUtil.isEmpty(requestId)) {
            httpEntity.setHeader(WebContext.JSM_TRACE_ID, requestId);
        }
        if (!StringUtil.isEmpty(at)) {
            httpEntity.setHeader(WebContext.JSM_AT, at);
        }
        if (!StringUtil.isEmpty(ht)) {
            httpEntity.setHeader(WebContext.JSM_HT, ht);
        }
        if (!StringUtil.isEmpty(token)) {
            httpEntity.setHeader(WebContext.JSM_TOKEN, token);
        }
        if (!StringUtil.isEmpty(authorization)) {
            httpEntity.setHeader(WebContext.JSM_AUTHORIZATION, authorization);
        }
    }

    private CloseableHttpAsyncClient getClient() {
        if (this.closeableHttpAsyncClient == null) {
            this.closeableHttpAsyncClient = createClient();
            this.closeableHttpAsyncClient.start();
        }
        if (!this.closeableHttpAsyncClient.isRunning()) {
            try {
                this.closeableHttpAsyncClient.close();
            } catch (IOException e) {
                logger.error("", e);
            }
            this.closeableHttpAsyncClient = createClient();
            this.closeableHttpAsyncClient.start();
        }
        return this.closeableHttpAsyncClient;
    }

    private CloseableHttpAsyncClient createClient() {
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            }).build();

            HttpAsyncClientBuilder hcb = HttpAsyncClientBuilder.create();
            if (proxyHost != null) {
                hcb.setProxy(proxyHost);
            }
            hcb.setSSLContext(sslContext);
            ConnectingIOReactor ioReactor = new DefaultConnectingIOReactor();
            PoolingNHttpClientConnectionManager cm = new PoolingNHttpClientConnectionManager(ioReactor);
            cm.setMaxTotal(100);
            hcb.setConnectionManager(cm);
            return hcb.build();
        } catch (Exception e) {
            logger.error("create ssl socket factory error!", e);
            return null;
        }
    }

}