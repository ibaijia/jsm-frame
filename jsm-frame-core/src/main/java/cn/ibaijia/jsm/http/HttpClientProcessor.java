package cn.ibaijia.jsm.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public interface HttpClientProcessor {

    void before(HttpRequestBase httpRequestBase);

    void complete(HttpResponse response);

    void exception(Exception e);

    void after();

}
