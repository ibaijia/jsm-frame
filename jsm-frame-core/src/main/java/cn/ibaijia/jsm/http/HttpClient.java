package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.apache.http.*;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.status.StatusLogger;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HttpClient {
    private org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();
    private Charset DEFAULT_CHARSET = Consts.UTF_8;
    private HttpHost httpProxy;
    private HttpClientContext httpClientContext = HttpClientContext.create();
    private PoolingHttpClientConnectionManager connManager;
    private RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build();
    private CookieStore cookieStore = new BasicCookieStore();
    private boolean withExtra = true;
    private boolean useCookie = true;
    private boolean allowAllSsl = false;

    public HttpClient() {
        if (useCookie) {
            httpClientContext.setCookieStore(cookieStore);
            httpClientContext.setRequestConfig(requestConfig);
        }
    }

    public HttpClient(boolean useCookie) {
        this.useCookie = useCookie;
        if (useCookie) {
            httpClientContext.setCookieStore(cookieStore);
            httpClientContext.setRequestConfig(requestConfig);
        }
    }

    public boolean isAllowAllSSL() {
        return allowAllSsl;
    }

    public void setAllowAllSSL(boolean allowAllSsl) {
        this.allowAllSsl = allowAllSsl;
    }

    private void addAlarm(String method, String url, String errorMessage) {
        SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_HTTP_CLIENT, "" + method + "," + url + "," + errorMessage));
    }

    public boolean isWithExtra() {
        return withExtra;
    }

    public void setWithExtra(boolean withExtra) {
        this.withExtra = withExtra;
    }

    public void setSocksProxy(String ip, int port) {
        Registry<ConnectionSocketFactory> reg = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new MyConnectionSocketFactory())
                .register("https", new MySSLConnectionSocketFactory(SSLContexts.createSystemDefault()))
                .build();
        connManager = new PoolingHttpClientConnectionManager(reg);
        httpClientContext.setAttribute("socks.address", new InetSocketAddress(ip, port));
    }

    public void setHttpProxy(HttpHost httpHost) {
        this.httpProxy = httpHost;
    }

    public <T> T get(String url, Class<T> clazz) {
        String res = get(null, url);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T get(List<Header> headers, String url, Class<T> clazz) {
        String res = get(headers, url);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String get(String url) {
        return get(null, url);
    }

    public String get(List<Header> headers, String url) {
        logger.debug("get url:" + url);
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            if (headers != null) {
                httpGet.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpGet);
            response = httpclient.execute(httpGet, httpClientContext);
            logger.debug("get url status:" + response.getStatusLine());
            res = EntityUtils.toString(response.getEntity(), DEFAULT_CHARSET);
            logger.debug("responseBody:{}", res);
        } catch (Exception e) {
            logger.error("http get error:" + url, e);
            addAlarm("get", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }

        return res;
    }

    public void get(String url, HttpClientProcessor processor) {
        get(null, url, processor);
    }

    public void get(List<Header> headers, String url, HttpClientProcessor processor) {
        logger.debug("get url:" + url);
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            if (headers != null) {
                httpGet.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpGet);
            processor.before(httpGet);
            response = httpclient.execute(httpGet, httpClientContext);
            logger.debug("get url:" + response.getStatusLine());
            processor.complete(response);
        } catch (Exception e) {
            logger.error("http get error:" + url, e);
            addAlarm("get", url, e.getMessage());
            processor.exception(e);
        } finally {
            close(httpclient, response);
            processor.after();
        }
    }

    public <T> T post(String url, List<NameValuePair> nvps, Class<T> clazz) {
        String res = post(null, url, nvps);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T post(List<Header> headers, String url, List<NameValuePair> nvps, Class<T> clazz) {
        String res = post(headers, url, nvps);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String post(String url, List<NameValuePair> nvps) {
        return post(null, url, nvps);
    }

    public String post(List<Header> headers, String url, List<NameValuePair> nvps) {
        logger.debug("post url:" + url);
        logger.debug("request nvps:{}", StringUtil.toJson(nvps));
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpPost);
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(nvps, DEFAULT_CHARSET);
            logger.debug(EntityUtils.toString(httpEntity));
            httpPost.setEntity(httpEntity);
            response = httpclient.execute(httpPost, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            res = EntityUtils.toString(response.getEntity(), DEFAULT_CHARSET);
            logger.debug("responseBody:{}", res);
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
            addAlarm("post", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }

        return res;
    }

    public void post(String url, List<NameValuePair> nvps, HttpClientProcessor processor) {
        post(null, url, nvps, processor);
    }

    public void post(List<Header> headers, String url, List<NameValuePair> nvps, HttpClientProcessor processor) {
        logger.debug("post url:" + url);
        logger.debug("request nvps:{}", StringUtil.toJson(nvps));
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpPost);
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(nvps, DEFAULT_CHARSET);
            logger.debug(EntityUtils.toString(httpEntity));
            httpPost.setEntity(httpEntity);
            processor.before(httpPost);
            response = httpclient.execute(httpPost, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            processor.complete(response);
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
            addAlarm("post", url, e.getMessage());
            processor.exception(e);
        } finally {
            close(httpclient, response);
            processor.after();
        }
    }

    public <T> T post(String url, Object data, Class<T> clazz) {
        String res = post(null, url, data);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T post(List<Header> headers, String url, Object data, Class<T> clazz) {
        String res = post(headers, url, data);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String post(String url, Object data) {
        if (data instanceof String) {
            return post(null, url, (String) data);
        } else {
            return post(null, url, StringUtil.toJson(data));
        }
    }

    public String post(String url, String strEntity) {
        return post(null, url, strEntity);
    }

    public String post(List<Header> headers, String url, Object data) {
        if (data instanceof String) {
            return post(headers, url, (String) data);
        } else {
            return post(headers, url, StringUtil.toJson(data));
        }
    }

    public String post(List<Header> headers, String url, String strEntity) {
        logger.debug("post url:" + url);
        logger.debug("requestBody:{}", strEntity);
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity entity = new StringEntity(strEntity, DEFAULT_CHARSET);//解决中文乱码问题
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            } else {
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json; charset=UTF-8");
            }
            setExtraHeaders(httpPost);
            httpPost.setEntity(entity);
            response = httpclient.execute(httpPost, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            res = EntityUtils.toString(response.getEntity());
            logger.debug("responseBody:{}", res);
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
            addAlarm("post", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return res;
    }

    public void post(String url, String strEntity, HttpClientProcessor processor) {
        post(null, url, strEntity, processor);
    }

    public void post(List<Header> headers, String url, String strEntity, HttpClientProcessor processor) {
        logger.debug("post url:" + url);
        logger.debug("requestBody:{}", strEntity);
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity entity = new StringEntity(strEntity, DEFAULT_CHARSET);//解决中文乱码问题
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            } else {
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json; charset=UTF-8");
            }
            setExtraHeaders(httpPost);
            httpPost.setEntity(entity);
            processor.before(httpPost);
            response = httpclient.execute(httpPost, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            processor.complete(response);
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
            addAlarm("post", url, e.getMessage());
            processor.exception(e);
        } finally {
            close(httpclient, response);
            processor.after();
        }
    }

    public <T> T put(String url, Object data, Class<T> clazz) {
        String res = put(null, url, data);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T put(List<Header> headers, String url, Object data, Class<T> clazz) {
        String res = put(headers, url, data);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String put(String url, Object data) {
        if (data instanceof String) {
            return put(null, url, (String) data);
        } else {
            return put(null, url, StringUtil.toJson(data));
        }
    }

    public String put(List<Header> headers, String url, Object data) {
        if (data instanceof String) {
            return put(headers, url, (String) data);
        } else {
            return put(headers, url, StringUtil.toJson(data));
        }
    }

    public String put(List<Header> headers, String url, String strEntity) {
        logger.debug("put url:" + url);
        logger.debug("requestBody:{}", strEntity);
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpPut httpPut = new HttpPut(url);
            StringEntity entity = new StringEntity(strEntity, DEFAULT_CHARSET);//解决中文乱码问题
            if (headers != null) {
                httpPut.setHeaders(headers.toArray(new Header[headers.size()]));
            } else {
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json; charset=UTF-8");
            }
            setExtraHeaders(httpPut);
            httpPut.setEntity(entity);
            response = httpclient.execute(httpPut, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            res = EntityUtils.toString(response.getEntity());
            logger.debug("responseBody:{}", res);
        } catch (Exception e) {
            logger.error("http put error:" + url, e);
            addAlarm("put", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return res;
    }

    public void put(String url, String strEntity, HttpClientProcessor processor) {
        put(null, url, strEntity, processor);
    }

    public void put(List<Header> headers, String url, String strEntity, HttpClientProcessor processor) {
        logger.debug("put url:" + url);
        logger.debug("requestBody:{}", strEntity);
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpPut httpPut = new HttpPut(url);
            StringEntity entity = new StringEntity(strEntity, DEFAULT_CHARSET);//解决中文乱码问题
            if (headers != null) {
                httpPut.setHeaders(headers.toArray(new Header[headers.size()]));
            } else {
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json; charset=UTF-8");
            }
            setExtraHeaders(httpPut);
            httpPut.setEntity(entity);
            processor.before(httpPut);
            response = httpclient.execute(httpPut, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            processor.complete(response);
        } catch (Exception e) {
            logger.error("http put error:" + url, e);
            addAlarm("put", url, e.getMessage());
            processor.exception(e);
        } finally {
            close(httpclient, response);
            processor.after();
        }
    }

    public <T> T delete(String url, Class<T> clazz) {
        String res = delete(null, url);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T delete(List<Header> headers, String url, Class<T> clazz) {
        String res = delete(headers, url);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String delete(String url) {
        return delete(null, url);
    }

    public String delete(List<Header> headers, String url) {
        logger.debug("delete url:" + url);
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            if (headers != null) {
                httpDelete.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpDelete);
            response = httpclient.execute(httpDelete, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            res = EntityUtils.toString(response.getEntity(), DEFAULT_CHARSET);
            logger.debug("responseBody:{}", res);
        } catch (Exception e) {
            logger.error("http delete error:" + url, e);
            addAlarm("delete", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return res;
    }

    public void delete(String url, HttpClientProcessor processor) {
        delete(null, url, processor);
    }

    public void delete(List<Header> headers, String url, HttpClientProcessor processor) {
        logger.debug("delete url:" + url);
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            if (headers != null) {
                httpDelete.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpDelete);
            processor.before(httpDelete);
            response = httpclient.execute(httpDelete, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            processor.complete(response);
        } catch (Exception e) {
            logger.error("http delete error:" + url, e);
            addAlarm("delete", url, e.getMessage());
            processor.exception(e);
        } finally {
            close(httpclient, response);
            processor.after();
        }
    }

    public <T> T getFile(String url, String savePath, Class<T> clazz) {
        String res = getFile(null, url, savePath);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T getFile(List<Header> headers, String url, String savePath, Class<T> clazz) {
        String res = getFile(headers, url, savePath);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String getFile(String url, String savePath) {
        return getFile(null, url, savePath);
    }

    public String getFile(List<Header> headers, String url, String savePath) {
        logger.debug("getFile url:" + url);
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        FileOutputStream output = null;
        InputStream fis = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            if (headers != null) {
                httpGet.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpGet);
            response = httpclient.execute(httpGet, httpClientContext);
            logger.debug("get file url:" + response.getStatusLine());
            output = new FileOutputStream(savePath);
            //得到网络资源的字节数组,并写入文件
            fis = response.getEntity().getContent();
            byte b[] = new byte[1024];
            int j = 0;
            while ((j = fis.read(b)) != -1) {
                output.write(b, 0, j);
            }
            return savePath;

        } catch (Exception e) {
            logger.error("http getFile error:" + url, e);
            addAlarm("get file", url, e.getMessage());
            return null;
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (output != null) {
                    output.flush();
                    output.close();
                }
                close(httpclient, response);
            } catch (IOException e) {
                logger.error("httpclient getFile close error!", e);
            }
        }
    }

    public <T> T postFile(String url, String filePath, Class<T> clazz) {
        String res = postFile(url, filePath);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T postFile(String url, String name, String filePath, Class<T> clazz) {
        String res = postFile(url, name, filePath);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T postFile(String url, Map<String, Object> fileMap, List<NameValuePair> nvps, Class<T> clazz) {
        String res = postFile(null, url, fileMap, nvps);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T postFile(List<Header> headers, String url, Map<String, Object> fileMap, List<NameValuePair> nvps, Class<T> clazz) {
        String res = postFile(headers, url, fileMap, nvps);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String postFile(String url, String filePath) {
        Map<String, Object> fileMap = new HashMap<>();
        fileMap.put("file", filePath);
        return postFile(null, url, fileMap, null);
    }

    public String postFile(String url, String name, String filePath) {
        Map<String, Object> fileMap = new HashMap<>();
        fileMap.put(name, filePath);
        return postFile(null, url, fileMap, null);
    }

    public String postFile(String url, Map<String, Object> fileMap, List<NameValuePair> nvps) {
        return postFile(null, url, fileMap, nvps);
    }

    public String postFile(List<Header> headers, String url, Map<String, Object> fileMap, List<NameValuePair> nvps) {
        logger.debug("postFile url:{},fileMap Size:{}，nvps:{}", url, fileMap.size(), StringUtil.toJson(nvps));
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(5000)
                    .setConnectionRequestTimeout(1000)
                    .setSocketTimeout(600000)// 10mins
                    .build();
            HttpPost httpPost = new HttpPost(url);
            if (headers != null) {
                httpPost.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpPost);
            httpPost.setConfig(requestConfig);
            MultipartEntityBuilder requestBuilder = MultipartEntityBuilder.create()
                    .setCharset(DEFAULT_CHARSET)
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            if (fileMap != null) {
                for (Map.Entry<String, Object> entry : fileMap.entrySet()) {
                    Object value = entry.getValue();
                    if (value instanceof File) {
                        logger.debug("addBinaryBody file, {}:{}", entry.getKey(), ((File) entry.getValue()).getAbsolutePath());
                        requestBuilder.addBinaryBody(entry.getKey(), (File) entry.getValue());
                    } else if (value instanceof InputStream) {
                        logger.debug("addBinaryBody InputStream, {}", entry.getKey());
                        requestBuilder.addBinaryBody(entry.getKey(), (InputStream) entry.getValue());
                    } else if (value instanceof byte[]) {
                        logger.debug("addBinaryBody byte[], {}", entry.getKey());
                        requestBuilder.addBinaryBody(entry.getKey(), (byte[]) entry.getValue());
                    } else if (value instanceof String) {
                        logger.debug("addBinaryBody filePath, {}:{}", entry.getKey(), (String) entry.getValue());
                        requestBuilder.addBinaryBody(entry.getKey(), new File((String) entry.getValue()));
                    }
                }
            }
            if (nvps != null) {
                for (NameValuePair nvp : nvps) {
                    logger.debug("form addPart, {}:{}", nvp.getName(), nvp.getValue());
                    StringBody value = new StringBody(nvp.getValue(), ContentType.create("text/plain", DEFAULT_CHARSET));
                    requestBuilder.addPart(nvp.getName(), value);
                }
            }
            HttpEntity reqEntity = requestBuilder.build();
            httpPost.setEntity(reqEntity);
            response = httpclient.execute(httpPost, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            res = EntityUtils.toString(response.getEntity());
            logger.debug("postFile res:" + res);
        } catch (Exception e) {
            logger.error("http postFile error:" + url, e);
            addAlarm("post file", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return res;
    }


    public <T> T putFile(String url, String filePath, Class<T> clazz) {
        String res = putFile(null, url, "file", filePath, null);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T putFile(String url, String name, String filePath, Class<T> clazz) {
        String res = putFile(null, url, name, filePath, null);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T putFile(String url, String name, String filePath, List<NameValuePair> nvps, Class<T> clazz) {
        String res = putFile(null, url, name, filePath, nvps);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public <T> T putFile(List<Header> headers, String url, String name, String filePath, List<NameValuePair> nvps, Class<T> clazz) {
        String res = putFile(headers, url, name, filePath, nvps);
        return res == null ? null : StringUtil.parseObject(res, clazz);
    }

    public String putFile(String url, String filePath) {
        return putFile(null, url, "file", filePath, null);
    }

    public String putFile(String url, String name, String filePath) {
        return putFile(null, url, name, filePath, null);
    }

    public String putFile(String url, String name, String filePath, List<NameValuePair> nvps) {
        return putFile(null, url, name, filePath, nvps);
    }

    public String putFile(List<Header> headers, String url, String name, String filePath, List<NameValuePair> nvps) {
        logger.debug("putFile url:{},filePath:{}", url, filePath);
        String res = null;
        CloseableHttpClient httpclient = createClient();
        CloseableHttpResponse response = null;
        try {
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(5000)
                    .setConnectionRequestTimeout(1000)
                    .setSocketTimeout(600000)// 10mins
                    .build();
            HttpPut httpPut = new HttpPut(url);
            if (headers != null) {
                httpPut.setHeaders(headers.toArray(new Header[headers.size()]));
            }
            setExtraHeaders(httpPut);
            httpPut.setConfig(requestConfig);
            FileBody bin = new FileBody(new File(filePath));
            long len = bin.getContentLength();
            logger.debug("putFile size:" + len / 1024 + "KB");
            MultipartEntityBuilder requestBuilder = MultipartEntityBuilder.create()
                    .addBinaryBody(name, new File(filePath))
                    .setCharset(DEFAULT_CHARSET)
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            for (NameValuePair nvp : nvps) {
                requestBuilder.addTextBody(nvp.getName(), nvp.getValue());
            }
            HttpEntity reqEntity = requestBuilder.build();
            httpPut.setEntity(reqEntity);
            response = httpclient.execute(httpPut, httpClientContext);
            logger.debug(response.getStatusLine().toString());
            res = EntityUtils.toString(response.getEntity());
            logger.debug("putFile res:" + res);
        } catch (Exception e) {
            logger.error("http putFile error:" + url, e);
            addAlarm("put file", url, e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return res;
    }

    private void close(CloseableHttpClient httpclient, CloseableHttpResponse response) {
        try {
            if (response != null) {
                response.close();
            }
            if (httpclient != null) {
                httpclient.close();
            }
        } catch (IOException e) {
            logger.error("httpclient close error!", e);
        }
    }

    private HttpRequestRetryHandler myRetryHandler = new HttpRequestRetryHandler() {
        @Override
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            return false;
        }
    };

    private void setExtraHeaders(HttpRequestBase httpEntity) {
        if (!withExtra) {
            return;
        }
        String requestId = WebContext.getTraceId();
        String at = WebContext.getRequestAt();
        String ht = WebContext.getRequestHt();
        String token = WebContext.getRequestToken();
        String authorization = WebContext.getRequestAuthorization();
        if (!StringUtil.isEmpty(requestId)) {
            httpEntity.setHeader(WebContext.JSM_TRACE_ID, requestId);
        }
        if (!StringUtil.isEmpty(at)) {
            httpEntity.setHeader(WebContext.JSM_AT, at);
        }
        if (!StringUtil.isEmpty(ht)) {
            httpEntity.setHeader(WebContext.JSM_HT, ht);
        }
        if (!StringUtil.isEmpty(token)) {
            httpEntity.setHeader(WebContext.JSM_TOKEN, token);
        }
        if (!StringUtil.isEmpty(authorization)) {
            httpEntity.setHeader(WebContext.JSM_AUTHORIZATION, authorization);
        }
    }

    private CloseableHttpClient createClient() {
        SSLConnectionSocketFactory sslcsf = null;
        try {
            if(allowAllSsl){
                SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(new TrustStrategy() {
                    @Override
                    public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        return true;
                    }
                }).build();
                sslcsf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
//            }else {
//                sslcsf = new SSLConnectionSocketFactory(SSLContexts.createSystemDefault(), new DefaultHostnameVerifier());
            }
        } catch (Exception e) {
            logger.error("create ssl socket factory error!", e);
        }

        HttpClientBuilder hcb = HttpClientBuilder.create();
//        		.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())  
//	            .setRedirectStrategy(new DefaultRedirectStrategy());
//	            .setDefaultRequestConfig(requestConfig)  
//              .setDefaultCookieStore(cookieStore)
//              .setDefaultHeaders(this.headers);
        if (httpProxy != null) {
            hcb.setProxy(httpProxy);
        }
        if (sslcsf != null) {
            hcb.setSSLSocketFactory(sslcsf);
        }
        hcb.setRetryHandler(myRetryHandler);
        hcb.setConnectionManager(connManager);
        return hcb.build();
    }

}