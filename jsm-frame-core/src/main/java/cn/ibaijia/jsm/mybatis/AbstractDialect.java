package cn.ibaijia.jsm.mybatis;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.MappedStatement.Builder;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author longzl
 */
public abstract class AbstractDialect implements Dialect {

    protected static Pattern fromPattern = Pattern.compile("\\s+from\\s+", Pattern.CASE_INSENSITIVE);
    protected static Pattern orderByPattern = Pattern.compile("\\s+order\\s+by\\s+", Pattern.CASE_INSENSITIVE);
    protected static int ORDER_BY_START_IDX = 16;

    protected Properties properties;

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * 复制BoundSql对象
     */
    @Override
    public BoundSql createNewBoundSql(MappedStatement ms, BoundSql boundSql, String sql) {
        BoundSql newBoundSql = new BoundSql(ms.getConfiguration(), sql, boundSql.getParameterMappings(), boundSql.getParameterObject());
        for (ParameterMapping mapping : boundSql.getParameterMappings()) {
            String prop = mapping.getProperty();
            if (boundSql.hasAdditionalParameter(prop)) {
                newBoundSql.setAdditionalParameter(prop, boundSql.getAdditionalParameter(prop));
            }
        }
        return newBoundSql;
    }

    /**
     * 复制BoundSql对象
     */
    @Override
    public MappedStatement createNewMappedStatement(MappedStatement ms, BoundSql boundSql, String sql) {
        BoundSql newBoundSql = createNewBoundSql(ms, boundSql, sql);
        return createNewMappedStatement(ms, getSqlSource(newBoundSql));
    }

    /**
     * 复制MappedStatement对象
     */
    @Override
    public MappedStatement createNewMappedStatement(MappedStatement ms, SqlSource newSqlSource) {
        Builder builder = new Builder(ms.getConfiguration(), ms.getId(), newSqlSource, ms.getSqlCommandType());

        builder.resource(ms.getResource());
        builder.fetchSize(ms.getFetchSize());
        builder.statementType(ms.getStatementType());
        builder.keyGenerator(ms.getKeyGenerator());
        if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
            StringBuilder keyProperties = new StringBuilder();
            for (String keyProperty : ms.getKeyProperties()) {
                keyProperties.append(keyProperty).append(",");
            }
            keyProperties.delete(keyProperties.length() - 1, keyProperties.length());
            builder.keyProperty(keyProperties.toString());
        }
        builder.timeout(ms.getTimeout());
        builder.parameterMap(ms.getParameterMap());
        builder.resultMaps(ms.getResultMaps());
        builder.resultSetType(ms.getResultSetType());
        builder.cache(ms.getCache());
        builder.flushCacheRequired(ms.isFlushCacheRequired());
        builder.useCache(ms.isUseCache());

        return builder.build();
    }

    @Override
    public String concatCountSql(String sql) {
        StringBuilder sb = new StringBuilder("select count(*) from ");
        int beginIndex = findBeginIdx(sql);
        Matcher orderByMatcher = orderByPattern.matcher(sql);
        int lastIndex = sql.lastIndexOf(")");
        if (orderByMatcher.find(lastIndex > 0 ? lastIndex : ORDER_BY_START_IDX)) {
            sb.append(sql, beginIndex, orderByMatcher.start());
        } else {
            sb.append(sql.substring(beginIndex));
        }
        return sb.toString();
    }

    @Override
    public String getSqlBody(String sql) {
        Matcher fromMatcher = fromPattern.matcher(sql);
        int beginIndex = 0;
        if (fromMatcher.find()) {
            beginIndex = fromMatcher.end();
        }
        return sql.substring(beginIndex);
    }

    @Override
    public SqlSource getSqlSource(final BoundSql boundSql) {

        return new SqlSource() {
            @Override
            public BoundSql getBoundSql(Object parameterObject) {
                return boundSql;
            }
        };

    }

    protected int findBeginIdx(String sql) {
        int beginIdx = 7;
        int idxFrom = beginIdx;
        sql = sql.toLowerCase();
        sql = sql.replace("(", " ").replace(")", " ");
        StringBuilder sb = new StringBuilder(sql);
        while (beginIdx < sql.length()) {
            int idxSelect = sb.indexOf(" select ", beginIdx);
            idxFrom = sb.indexOf(" from ", beginIdx);
            //下一个select 不存在或者在第一个from之后 就可以直接返回
            if (idxSelect == -1 || idxSelect > idxFrom) {
                break;
            } else {//否则
                beginIdx = idxFrom + 6;
            }
        }
        return idxFrom + 6;
    }

}
