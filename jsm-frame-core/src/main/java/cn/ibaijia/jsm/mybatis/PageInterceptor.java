package cn.ibaijia.jsm.mybatis;

import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;


/**
 * @author longzl
 */
@Intercepts(
        {
                @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
                @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}),
        }
)
public class PageInterceptor implements Interceptor {

    private static Logger logger = LogUtil.log(PageInterceptor.class);
    private Dialect dialect = new MysqlDialect();
    private static final String MYSQL = "mysql";
    private static final String ORACLE = "oracle";
    private static final String DB2 = "db2";
    private static final String MSSQL = "mssql";

    private static final String FOR_UPDATE_SUFFIX = ".findByIdForUpdate";
    private String countSuffix = "_COUNT";
    private long slowSqlLimit = AppContext.getAsLong(AppContextKey.SLOW_SQL_LIMIT, 5000L);


    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        //获取拦截方法的参数
        Object[] args = invocation.getArgs();
        logger.trace("query args:{}", args.length);
        MappedStatement ms = (MappedStatement) args[0];
        logger.trace("id:{}", ms.getId());
        Object parameterObject = args[1];
        BoundSql boundSql = null;
        if (args.length == 4) {
            boundSql = ms.getBoundSql(parameterObject);
        } else {
            boundSql = (BoundSql) args[5];
        }
        // 分页参数作为参数对象parameterObject的一个属性
        String sql = boundSql.getSql();
        if (boundSql.getParameterObject() instanceof Page) {
            Page<?> page = (Page<?>) (boundSql.getParameterObject());
            String pageSql = null;
            PreparedStatement countStmt = null;
            Connection connection = null;
            ResultSet rs = null;
            String countSql = null;
            int totalCount = 0;
            try {
                countSql = findManualCountSql(ms.getConfiguration(), ms.getId() + countSuffix, parameterObject);
                sql = StringUtil.normalizeSQL(sql);
                if (countSql == null) {
                    countSql = dialect.concatCountSql(sql);
                    logger.debug("use concatCountSql:{}", ms.getId());
                }
                logger.info("countSql: {}", countSql);
                pageSql = dialect.concatPageSql(sql, page.getBeginIndex(), page.getPageSize());
                connection = ms.getConfiguration().getEnvironment().getDataSource().getConnection();
                countStmt = connection.prepareStatement(countSql);
                BoundSql countBoundSql = dialect.createNewBoundSql(ms, boundSql, countSql);
                DefaultParameterHandler parameterHandler = new DefaultParameterHandler(ms, parameterObject, countBoundSql);
                parameterHandler.setParameters(countStmt);
                rs = countStmt.executeQuery();
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            } catch (Exception e) {
                logger.error("count records for page error!sql:" + sql, e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (countStmt != null) {
                        countStmt.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (Exception e) {
                    logger.error("count records for page close conn error!sql:" + sql, e);
                }
            }
            page.setTotalCount(totalCount);

            MappedStatement newMs = dialect.createNewMappedStatement(ms, boundSql, pageSql);
            invocation.getArgs()[0] = newMs;
            Object list = getProceedResult(invocation, sql);
            page.setList((List) list);
            return list;
        } else {
            Object object = getProceedResult(invocation, sql);
            if (ms != null && ms.getId() != null && ms.getId().endsWith(FOR_UPDATE_SUFFIX) && object != null) {
                if (object instanceof List) {
                    List list = (List) object;
                    if (!list.isEmpty() && (list.get(0) instanceof BaseModel)) {
                    }
                    for (Object o : list) {
                        ((BaseModel) o).createSnapshot();
                    }
                } else {
                    if (object instanceof BaseModel) {
                        ((BaseModel) object).createSnapshot();
                    }
                }
            }
            return object;
        }
    }

    private Object getProceedResult(Invocation invocation, String sql) throws InvocationTargetException, IllegalAccessException {
        long begin = System.currentTimeMillis();
        Object object = invocation.proceed();
        long spend = System.currentTimeMillis() - begin;
        if (spend > slowSqlLimit) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_SLOW_SQL, String.format("spend:%s sql:%s", spend, sql)));
        }
        return object;
    }

    private String findManualCountSql(Configuration configuration, String msId, Object parameterObject) {
        try {
            if (configuration.hasStatement(msId, false)) {
                MappedStatement mappedStatement = configuration.getMappedStatement(msId, false);
                logger.debug("use findManualCountSql:{}", msId);
                return mappedStatement.getBoundSql(parameterObject).getSql();
            }
        } catch (Exception e) {
            logger.error("findManualCountSql error." + msId);
        }
        return null;
    }

    /**
     * 拦截类型StatementHandler ||ResultSetHandler
     */
    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
        logger.debug("properties:{}", StringUtil.toJson(properties));
        String dbDialect = properties.getProperty("dbDialect");
        if (MYSQL.equalsIgnoreCase(dbDialect)) {
            this.dialect = new MysqlDialect();
        } else if (ORACLE.equalsIgnoreCase(dbDialect)) {
            this.dialect = new OracleDialect();
        } else if (DB2.equalsIgnoreCase(dbDialect)) {
            this.dialect = new Db2Dialect();
        } else if (MSSQL.equalsIgnoreCase(dbDialect)) {
            this.dialect = new MssqlDialect();
        } else {
            logger.warn("dbDialect not specify, use default:mysql");
        }

    }

}