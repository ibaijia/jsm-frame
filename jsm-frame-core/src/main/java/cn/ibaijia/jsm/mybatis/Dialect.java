package cn.ibaijia.jsm.mybatis;

import java.util.Properties;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

public interface Dialect {
	
	void setProperties(Properties properties);
	
	MappedStatement createNewMappedStatement(MappedStatement ms,SqlSource newSqlSource);
	
	MappedStatement createNewMappedStatement(MappedStatement ms, BoundSql boundSql, String sql);
	
	BoundSql createNewBoundSql(MappedStatement ms, BoundSql boundSql, String sql);
	
	String concatCountSql(String sql);
	
	String concatPageSql(String sql,int startRow,int pageSize);
	
	SqlSource getSqlSource(BoundSql boundSql);
	
	String getSqlBody(String sql);
	
}
