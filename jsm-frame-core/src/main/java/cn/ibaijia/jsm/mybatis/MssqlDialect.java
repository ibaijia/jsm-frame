package cn.ibaijia.jsm.mybatis;

import java.util.regex.Matcher;

/**
 * @author longzl more than 2012
 */
public class MssqlDialect extends AbstractDialect {


    @Override
    public String concatPageSql(String sql, int startRow, int pageSize) {
        StringBuilder sqlBuilder = new StringBuilder(sql.length() + 80);
        sqlBuilder.append(sql);
        Matcher orderByMatcher = orderByPattern.matcher(sql);
        int lastIndex = sql.lastIndexOf(")");
        if (!orderByMatcher.find(lastIndex > 0 ? lastIndex : ORDER_BY_START_IDX)) {
            sqlBuilder.append(" order by id ");
        }
        sqlBuilder.append(" offset ").append(startRow).append(" rows ");
        sqlBuilder.append(" fetch next ").append(pageSize).append(" rows only");
        return sqlBuilder.toString();
    }

}
