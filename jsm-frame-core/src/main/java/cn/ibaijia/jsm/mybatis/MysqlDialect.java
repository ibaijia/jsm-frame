package cn.ibaijia.jsm.mybatis;

public class MysqlDialect extends AbstractDialect {

    @Override
    public String concatPageSql(String sql, int startRow, int pageSize) {
        StringBuilder sb = new StringBuilder(sql.length() + 30);
        sb.append(sql);
        sb.append(" limit ").append(startRow).append(" , ").append(pageSize);
        return sb.toString();
    }

}
