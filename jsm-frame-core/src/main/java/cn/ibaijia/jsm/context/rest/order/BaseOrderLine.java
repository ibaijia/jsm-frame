package cn.ibaijia.jsm.context.rest.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.Pair;

public abstract class BaseOrderLine extends BaseService implements OrderLine {

    protected Pair<String> status;

    @Override
    public abstract Pair<String> order(String key,OrderAnn orderAnn);

    public Pair<String> getStatus() {
        return status;
    }

    public void setStatus(Pair<String> status) {
        this.status = status;
    }
}
