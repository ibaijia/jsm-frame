package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.utils.StringUtil;

/**
 * @author longzl
 */
public class StringResp<T> extends CodeResp<String> {
    @FieldAnn(comments = "响应结果")
    public T result;

    public StringResp() {
        super();
        this.setPair(BasePairConstants.OK);
    }

    public void setPair(Pair<String> pair) {
        this.code = pair.getCode();
        this.message = pair.getMessage();
    }

    @Override
    public String toString() {
        return StringUtil.toJson(this);
    }

}
