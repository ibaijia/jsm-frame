
package cn.ibaijia.jsm.context.dao;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * entity基类.
 * @author longzl
 */
public class BaseModel implements ValidateModel {

    private static final long serialVersionUID = 1L;

    protected Logger logger = LogUtil.log(getClass());

    @JSONField(serialize = false)
    @JsonIgnore
    @ApiParam(hidden = true)
    public Object snapshot;

    @FieldAnn(required = true, name = "id", comments = "记录ID")
    public Long id;

    public String toJSONString() {
        return StringUtil.toJson(this);
    }

    @ApiParam(hidden = true)
    public void createSnapshot() {
        if (this.snapshot == null) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(this);

                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                this.snapshot = objectInputStream.readObject();
            } catch (Exception e) {
                logger.error("create snapshot error!", e);
            }
        }
    }

    @ApiParam(hidden = true)
    public Object getSnapshot() {
        return snapshot;
    }

    @ApiParam(hidden = true)
    public void setSnapshot(Object snapshot) {
        this.snapshot = snapshot;
    }
}
