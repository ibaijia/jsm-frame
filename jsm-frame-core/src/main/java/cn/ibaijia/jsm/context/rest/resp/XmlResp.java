package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
/**
 * @author longzl
 */
public class XmlResp implements JsmResp {
	@FieldAnn(comments = "xml String")
	public String xml;
}
