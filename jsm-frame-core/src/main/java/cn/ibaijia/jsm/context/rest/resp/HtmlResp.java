package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
/**
 * @author longzl
 */
public class HtmlResp implements JsmResp {
	@FieldAnn(comments = "html String")
	public String html;
}
