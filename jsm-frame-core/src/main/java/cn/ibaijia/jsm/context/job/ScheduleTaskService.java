package cn.ibaijia.jsm.context.job;

public interface ScheduleTaskService {
    /**
     * 这种动态job不支持 @ClusterJobLockAnn
     */
    void loadTasks();

    void destroy();
}
