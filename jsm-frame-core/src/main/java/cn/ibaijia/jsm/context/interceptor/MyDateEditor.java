package cn.ibaijia.jsm.context.interceptor;

import java.beans.PropertyEditorSupport;
import java.util.Date;

import cn.ibaijia.jsm.utils.DateUtil;

public class MyDateEditor extends PropertyEditorSupport {
	 @Override
     public void setAsText(String text) {
         setValue(DateUtil.parse(text));
     }

     @Override
     public String getAsText() {
         Date date = (Date) getValue();
         return DateUtil.format(date, DateUtil.DATETIME_PATTERN);
     }
}
