package cn.ibaijia.jsm.context.rest.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.context.rest.order.BaseOrderLine;
import cn.ibaijia.jsm.consts.Pair;

import javax.annotation.Resource;

public class UnfairLineService extends BaseOrderLine {

    @Resource
    private JedisService jedisService;

    @Override
    public Pair<String> order(String key, OrderAnn orderAnn) {

        if (getStatus().equals(BasePairConstants.ACTIVITY_NOT_STARTED) || getStatus().equals(BasePairConstants.ACTIVITY_OVER)) {
            return getStatus();
        }
        String requestId = "1";
        boolean res = jedisService.lock(key, requestId, 0, 30);
        if (res) {
            return BasePairConstants.ACTIVITY_TURN;
        } else {
            return BasePairConstants.ACTIVITY_QUEUEING;
        }
    }
}
