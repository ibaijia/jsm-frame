package cn.ibaijia.jsm.context.session;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class Session {
    private static Logger logger = LoggerFactory.getLogger(Session.class);
    private String token;
    private String sessionKey;
    private JedisService jedisService;
    private SessionUser sessionUser;
    private HttpSession session;
    private boolean invalid = false;

    /**
     * session中的 userInfo的key
     */
    public final static String SESSION_PREFIX = "jsm:session:";
    /**
     * session中的 userInfo的key
     */
    private final static String SESSION_USER_ATTR_KEY = "jsm:sessionUser";
    /**
     * 每个用户的userId 对应的 token
     */
    private final static String USER_TOKEN_PREFIX = "jsm:user:token:";

    private static Map<String, HttpSession> loginSession = new HashMap<>();

    public static Session create(String token) {
        Session session = new Session(token);
        session.set(SESSION_PREFIX, token);
        session.live();
        return session;
    }

    public Session(String token) {
        super();
        this.token = token;
        this.sessionKey = SESSION_PREFIX + token;
        if (AppContext.isJedisSession()) {
            this.jedisService = SpringContext.getBean(JedisService.class);
        } else {
            session = WebContext.getRequest().getSession();
        }
    }

    public void set(String key, String value) {
        if (AppContext.isJedisSession()) {
            jedisService.hset(sessionKey, key, value);
        } else {
            session.setAttribute(key, value);
        }
    }

    /**
     * @param key
     * @return if jedis will string
     */
    public Object get(String key) {
        if (AppContext.isJedisSession()) {
            return jedisService.hget(sessionKey, key);
        } else {
            return session.getAttribute(key);
        }
    }

    public void del(String key) {
        if (AppContext.isJedisSession()) {
            jedisService.hdel(sessionKey, key);
        } else {
            session.removeAttribute(key);
        }
    }

    /**
     * jedis only
     */
    public void live() {
        logger.debug("live check!");
        if (AppContext.isJedisSession() && !this.invalid) {
            logger.debug("live");
            int liveTime = AppContext.getSessionExpireTime();
            jedisService.expire(sessionKey, liveTime);
            if (this.sessionUser != null) {
                String userTokenKey = USER_TOKEN_PREFIX + this.sessionUser.getUid();
                jedisService.expire(userTokenKey, liveTime);
            }
        }
    }

    public String getToken() {
        return token;
    }

    public SessionUser getSessionUser() {
        if (this.sessionUser == null) {
            if (AppContext.isJedisSession()) {
                String str = (String) get(SESSION_USER_ATTR_KEY);
                if (!StringUtil.isEmpty(str)) {
                    this.sessionUser = JsonUtil.parseObject(str, SessionUser.class);
                }
            } else {
                this.sessionUser = (SessionUser) session.getAttribute(SESSION_USER_ATTR_KEY);
            }
        }
        return sessionUser;
    }

    public void setSessionUser(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        if (AppContext.isJedisSession()) {
            set(SESSION_USER_ATTR_KEY, StringUtil.toJson(sessionUser));
            String userTokenKey = USER_TOKEN_PREFIX + this.sessionUser.getUid();
            jedisService.set(userTokenKey, this.token);
            logger.debug("setSessionUser key:{} , token:{}", userTokenKey, token);
            this.live();
        } else {
            session.setAttribute(SESSION_USER_ATTR_KEY, sessionUser);
            loginSession.put(sessionUser.getUid(), session);
        }
    }

    public boolean isExpire() {
        if (this.invalid) {
            return true;
        }
        if (AppContext.isJedisSession()) {
            return !jedisService.exists(sessionKey);
        } else {
            if (this.session != null) {
                return this.session == null;
            }
        }
        return false;
    }

    public void flush() {
        if (!this.invalid) {
            setSessionUser(sessionUser);
        }
    }

    /**
     * 使当前session过期
     */
    public void invalidate() {
        logger.debug("session invalidate.");
        this.invalid = true;
        if (AppContext.isJedisSession()) {
            if (this.getSessionUser() != null) {
                String userTokenKey = USER_TOKEN_PREFIX + this.getSessionUser().getUid();
                jedisService.del(userTokenKey);
                logger.debug("invalidate key:{}", userTokenKey);
            }
            jedisService.del(sessionKey);
            logger.debug("invalidate token:{}", token);
        } else {
            if (this.session != null) {
                this.session.invalidate();
            }
            if (this.sessionUser != null) {
                loginSession.remove(this.sessionUser.getUid());
            }
        }
    }

    public static boolean expire(String uid) {
        if (AppContext.isJedisSession()) {
            JedisService jedisService = SpringContext.getBean(JedisService.class);
            String userTokenKey = USER_TOKEN_PREFIX + uid;
            String token = jedisService.get(userTokenKey);
            jedisService.del(userTokenKey);
            if (!StringUtil.isEmpty(token)) {
                jedisService.del(token);
            }
            logger.debug("expire userTokenKey:{} , token:{}", userTokenKey, token);
        } else {
            HttpSession session = loginSession.get(uid);
            if (session != null) {
                session.invalidate();
            }
            loginSession.remove(uid);
        }
        return true;
    }

    public static boolean isLogon(String uid) {
        if (AppContext.isJedisSession()) {
            JedisService jedisService = SpringContext.getBean(JedisService.class);
            String userTokenKey = USER_TOKEN_PREFIX + uid;
            boolean res = jedisService.exists(userTokenKey);
            logger.debug("uid:{}, isLogon:{}", uid, res);
            return res;
        } else {
            return loginSession.get(uid) != null;
        }
    }

}
