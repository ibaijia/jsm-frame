package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.consts.BasePairConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author longzl
 */
public class RestResp<T> extends StringResp<T> {

    @JsonIgnore
    public boolean isOK() {
        return BasePairConstants.OK.getCode().equals(this.code);
    }

    @JsonIgnore
    public int getHttpStatusCode() {
        return 200;
    }

}
