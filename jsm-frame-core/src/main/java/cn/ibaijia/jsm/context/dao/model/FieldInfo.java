package cn.ibaijia.jsm.context.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;

import java.io.Serializable;
import java.util.List;

/**
 * @author longzl
 */
public class FieldInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    public String fieldName;
    public String fieldType;
    public String defaultVal;
    public String comments;
    public Boolean required = false;
    public Integer maxLength = 250;

    /**
     * 非普通对象 和 map可预见的字段对象
     */
    public List<FieldInfo> fieldInfoList;
    /**
     * 所处对象的层级 0 开始
     */
    public int level;
    /**
     * 字段的 注解
     */
    public FieldAnn fieldAnn;

    public boolean show = true;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getDefaultVal() {
        return defaultVal;
    }

    public void setDefaultVal(String defaultVal) {
        this.defaultVal = defaultVal;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }
}
