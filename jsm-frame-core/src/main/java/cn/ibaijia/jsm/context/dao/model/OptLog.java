package cn.ibaijia.jsm.context.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.dao.BaseModel;

/**
 * @author zhili
 * createOn 2016年11月30日
 */
public class OptLog extends BaseModel {
    private static final long serialVersionUID = -969033916740549421L;
    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId;
    @FieldAnn(required = false, comments = "时间")
    public Long time;
    @FieldAnn
    public String ip;
    @FieldAnn
    public String traceId;
    @FieldAnn
    public String mac;
    @FieldAnn
    public String uid;
    @FieldAnn
    public String funcName;
    @FieldAnn
    public String optDesc;

    public OptLog() {

    }

    public OptLog(String ip, String mac, String uid, String funcName, String optDesc) {
        super();
        this.ip = ip;
        this.mac = mac;
        this.uid = uid;
        this.funcName = funcName;
        this.optDesc = optDesc;
    }

}
