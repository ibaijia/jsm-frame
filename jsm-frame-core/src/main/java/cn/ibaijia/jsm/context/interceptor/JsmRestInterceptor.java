package cn.ibaijia.jsm.context.interceptor;

import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JsmRestInterceptor extends HandlerInterceptorAdapter {

    private static Logger logger = LogUtil.log(JsmRestInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("preHandle");
//        WebContext.setRequest(request);
//        WebContext.setResponse(response);
        //OrderAnn
        //mock -->  验证内网 -->验证IP -->验证登录 -->validate --> 验证重复提交 TODO 取不取参数值？
//        HandlerMethod hm = (HandlerMethod) handler;
//        Method method = hm.getMethod();
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
		logger.debug("postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
		logger.debug("afterCompletion");
    }

}
