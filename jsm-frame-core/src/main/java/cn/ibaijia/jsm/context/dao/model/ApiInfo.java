package cn.ibaijia.jsm.context.dao.model;

import java.io.Serializable;
import java.util.List;

public class ApiInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    public String name;
    public String httpMethod;
    public String url;
    public List<FieldInfo> pathVarList;
    public List<FieldInfo> reqParamList;
    public List<FieldInfo> bodyParamList;
    public List<FieldInfo> respParamList;
    public String comments;

    public String dataVarName;
    public String fullVarName;

    public boolean pathVarListShow = true;
    public boolean reqParamListShow = true;
    public boolean bodyParamListShow = true;
    public boolean respParamListShow = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<FieldInfo> getPathVarList() {
        return pathVarList;
    }

    public void setPathVarList(List<FieldInfo> pathVarList) {
        this.pathVarList = pathVarList;
    }

    public List<FieldInfo> getReqParamList() {
        return reqParamList;
    }

    public void setReqParamList(List<FieldInfo> reqParamList) {
        this.reqParamList = reqParamList;
    }

    public List<FieldInfo> getBodyParamList() {
        return bodyParamList;
    }

    public void setBodyParamList(List<FieldInfo> bodyParamList) {
        this.bodyParamList = bodyParamList;
    }

    public List<FieldInfo> getRespParamList() {
        return respParamList;
    }

    public void setRespParamList(List<FieldInfo> respParamList) {
        this.respParamList = respParamList;
    }


    public String getDataVarName() {

        return dataVarName;
    }

    public String getFullVarName() {
        if (this.fullVarName == null) {
            StringBuilder sb = new StringBuilder();
            if (this.pathVarList != null) {
                for (FieldInfo fieldInfo : this.pathVarList) {
                    if (sb.length() > 0) {
                        sb.append(" ");
                    }
                    sb.append(fieldInfo.fieldName).append(",");
                }
            }
            if (this.reqParamList != null) {
                for (FieldInfo fieldInfo : this.reqParamList) {
                    if (sb.length() > 0) {
                        sb.append(" ");
                    }
                    sb.append(fieldInfo.fieldName).append(",");
                }
            }
            if (this.bodyParamList != null) {
                for (FieldInfo fieldInfo : this.bodyParamList) {
                    if (sb.length() > 0) {
                        sb.append(" ");
                    }
                    sb.append(fieldInfo.fieldName).append(",");
                }
            }
            if (sb.length() > 0) {
                sb.append(" ");
            }
            this.fullVarName = sb.toString();
        }
        return fullVarName;
    }
}
