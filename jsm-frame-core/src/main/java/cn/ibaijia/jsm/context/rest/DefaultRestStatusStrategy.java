package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.context.rest.resp.RestResp;
import org.springframework.stereotype.Service;

/**
 * 业务结果对应响应 httpStatus 转换策略
 * @author longzl
 */
@Service
public class DefaultRestStatusStrategy implements RestStatusStrategy {
    @Override
    public int httpStatusCode(RestResp restResp) {
        return 200;
    }
}
