package cn.ibaijia.jsm.context.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.annotation.FieldType;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.EncryptUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;

import java.util.*;


/**
 * desc 说明
 *
 * @param <T>
 * @author zhili
 * createOn 2016年11月30日
 */
public class Page<T> implements ValidateModel {
    private static Logger logger = LogUtil.log(Page.class);

    public Page() {
        super();
    }

    public Page(int pageSize) {
        super();
        this.pageSize = pageSize;
    }

    @ApiParam(hidden = true)
    @FieldAnn(required = false, comments = "当前页数据列表")
    public List<T> list;
    @FieldAnn(required = true, type = FieldType.NUMBER, comments = "当前页数")
    public int pageNo = 1;
    @FieldAnn(required = true, type = FieldType.NUMBER, comments = "每页条数")
    public int pageSize = 15;
    @ApiParam(hidden = true)
    @FieldAnn(required = false, comments = "总条数")
    public long totalCount = 0;
    @FieldAnn(required = false, comments = "查询条件")
    public String queryString = "";
    @ApiParam(hidden = true)
    @JsonIgnore
    private Map<String, Object> queryMap;
    @ApiParam(hidden = true)
    @JsonIgnore
    private String dbDialect = null;

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public void convertParamToInteger(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Integer)) {
            this.getQueryMap().put(key, StringUtil.toInteger(val.toString()));
        }
    }

    public void convertParamToLong(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Long)) {
            this.getQueryMap().put(key, StringUtil.toLong(val.toString()));
        }
    }

    public void convertParamToFloat(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Float)) {
            this.getQueryMap().put(key, StringUtil.toFloat(val.toString()));
        }
    }

    public void convertParamToDouble(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Double)) {
            this.getQueryMap().put(key, StringUtil.toDouble(val.toString()));
        }
    }

    public void convertParamToDate(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Date)) {
            this.getQueryMap().put(key, DateUtil.parse(val.toString()));
        }
    }

    public void convertParamToDateOfBegin(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Date)) {
            Date date = DateUtil.parse(val.toString());
            this.getQueryMap().put(key, DateUtil.dayOfBegin(date));
        }
    }

    public void convertParamToDateOfEnd(String key) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Date)) {
            Date date = DateUtil.parse(val.toString());
            this.getQueryMap().put(key, DateUtil.dayOfEnd(date));
        }
    }

    public void convertParamToDate(String key, String pattern) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Date)) {
            this.getQueryMap().put(key, DateUtil.parse(val.toString(), pattern));
        }
    }


    public void convertParamToDateOfBegin(String key, String pattern) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Date)) {
            Date date = DateUtil.parse(val.toString(), pattern);
            this.getQueryMap().put(key, DateUtil.dayOfBegin(date));
        }
    }

    public void convertParamToDateOfEnd(String key, String pattern) {
        Object val = this.getQueryMap().get(key);
        if (val != null && !(val instanceof Date)) {
            Date date = DateUtil.parse(val.toString(), pattern);
            this.getQueryMap().put(key, DateUtil.dayOfEnd(date));
        }
    }

    public void setParam(String key, Object value) {
        this.getQueryMap().put(key, value);
    }

    public String getParamAsString(String key) {
        return (String) this.getQueryMap().get(key);
    }

    public Integer getParamAsInteger(String key) {
        convertParamToInteger(key);
        return (Integer) this.getQueryMap().get(key);
    }

    public Long getParamAsLong(String key) {
        convertParamToLong(key);
        return (Long) this.getQueryMap().get(key);
    }

    public Float getParamAsFloat(String key) {
        convertParamToFloat(key);
        return (Float) this.getQueryMap().get(key);
    }

    public Date getParamAsDate(String key) {
        convertParamToDate(key);
        return (Date) this.getQueryMap().get(key);
    }

    public Date getParamAsDateOfBegin(String key) {
        convertParamToDateOfBegin(key);
        return (Date) this.getQueryMap().get(key);
    }

    public Date getParamAsDateOfEnd(String key) {
        convertParamToDateOfEnd(key);
        return (Date) this.getQueryMap().get(key);
    }

    public Date getParamAsDate(String key, String pattern) {
        convertParamToDate(key, pattern);
        return (Date) this.getQueryMap().get(key);
    }

    public Date getParamAsDateOfBegin(String key, String pattern) {
        convertParamToDateOfBegin(key, pattern);
        return (Date) this.getQueryMap().get(key);
    }

    public Date getParamAsDateOfEnd(String key, String pattern) {
        convertParamToDateOfEnd(key, pattern);
        return (Date) this.getQueryMap().get(key);
    }

    public Map<String, Object> getQueryMap() {

        if (queryMap != null) {
            return queryMap;
        } else {
            if (!StringUtil.isEmpty(this.queryString)) {
                try {
                    queryMap = new HashMap<String, Object>();
                    String[] paramArr = this.queryString.split("&");
                    for (String param : paramArr) {
                        String[] arr = param.split("=");
                        if (arr.length >= 2) {
                            queryMap.put(arr[0], StringUtil.trim(EncryptUtil.urlDecode(StringUtil.trim(arr[1]))));
                        }
                    }

                } catch (Exception e) {
                    logger.error("queryString parse map error!" + this.queryString, e);
                    queryMap = new HashMap<String, Object>();
                }
            } else {
                queryMap = new HashMap<String, Object>();
            }
            return queryMap;
        }

    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    /**
     * 获取当前页号.
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * 设置当前页号,序号从1开始,低于1时自动调整为1.
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo < 1 ? 1 : pageNo;
    }

    /**
     * 获取每页显示记录条数.
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 设置每页的记录条数,低于2时自动调整为20.
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize < 1 ? 15 : pageSize;
    }

    /**
     * 获取总记录条数.
     */
    public long getTotalCount() {
        return totalCount;
    }

    /**
     * 设置总记录条数.
     */
    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 获取总页数(最后一页的页号).
     */
    public long getTotalPage() {
        if (totalCount <= 0) {
            return 1;
        } else {
            long count = totalCount / pageSize;
            if (totalCount % pageSize > 0) {
                count++;
            }
            return count;
        }
    }

    /**
     * 是否有上一页.
     */
    public boolean isHasPrevPage() {
        return (pageNo - 1 >= 1);
    }

    /**
     * 获取上一页的页号.
     */
    public int getPrevPage() {
        return isHasPrevPage() ? (pageNo - 1) : pageNo;
    }

    /**
     * 是否有下一页.
     */
    public boolean isHasNextPage() {
        return (pageNo + 1 <= getTotalPage());
    }

    /**
     * 获取下一页的页号.
     */
    public int getNextPage() {
        return isHasNextPage() ? (pageNo + 1) : pageNo;
    }

    /**
     * 获取当前页面上第一条记录对应数据库中的索引.
     */
    public int getBeginIndex() {
        return (pageNo - 1) * pageSize;
    }

    public String getDbDialect() {
        return dbDialect;
    }

    public void setDbDialect(String dbDialect) {
        this.dbDialect = dbDialect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Page<?> page = (Page<?>) o;
        return pageNo == page.pageNo &&
                pageSize == page.pageSize &&
                totalCount == page.totalCount &&
                Objects.equals(queryString, page.queryString);
    }

    @Override
    public int hashCode() {

        return Objects.hash(pageNo, pageSize, totalCount, queryString);
    }
}
