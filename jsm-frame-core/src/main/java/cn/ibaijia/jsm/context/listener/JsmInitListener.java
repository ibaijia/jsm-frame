package cn.ibaijia.jsm.context.listener;

import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.stat.JsmSysStatService;
import cn.ibaijia.jsm.context.job.ScheduleTaskService;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author longzl / @createOn 2010-8-28
 * 启动服务时 初始工作
 */
public class JsmInitListener implements ServletContextListener {
    private static Logger logger = LogUtil.log(JsmInitListener.class);

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        JsmAppListenerService appListenerService = SpringContext.getBean(JsmAppListenerService.class, wac);
        if (appListenerService != null) {
            appListenerService.close(sce);
        }
//        QuartzUtil.shutdownJobs();
        CacheService cacheService = SpringContext.getBean(CacheService.class);
        if (cacheService != null) {
            cacheService.close();
        }

        ScheduleTaskService scheduleTaskService = SpringContext.getBean(ScheduleTaskService.class, wac);
        if (scheduleTaskService != null) {
            scheduleTaskService.destroy();
        }
        logger.info("app close complete!");
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebContext.setContextPath(sce.getServletContext().getContextPath());// /ljdy
        WebContext.setRealPath(sce.getServletContext().getRealPath("/"));// d:/ljdy
        System.setProperty("file.encoding", "utf-8");
        WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        JsmAppListenerService appListenerService = SpringContext.getBean(JsmAppListenerService.class, wac);
        if (appListenerService != null) {
            appListenerService.init(sce);
        }

        // 加载初始化数据
        ScheduleTaskService scheduleTaskService = SpringContext.getBean(ScheduleTaskService.class, wac);
        if (scheduleTaskService != null) {
            logger.info("scheduleTaskService.loadTasks... 动态job不支持 @ClusterJobLockAnn,请job自行解决集群环境中的问题");
            scheduleTaskService.loadTasks();
        }

        // 执行服务状态日志输出
        JsmSysStatService jsmSysStatService = SpringContext.getBean(JsmSysStatService.class, wac);
        if (jsmSysStatService != null) {
            jsmSysStatService.start();
        }
        logger.info("jsm-frame init complete!");
    }

}
