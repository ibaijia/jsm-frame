package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.context.rest.resp.RestResp;

public interface RestStatusStrategy {

    int httpStatusCode(RestResp restResp);

}
