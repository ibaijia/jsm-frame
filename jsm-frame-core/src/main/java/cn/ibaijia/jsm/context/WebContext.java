package cn.ibaijia.jsm.context;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.context.session.SessionUser;
import cn.ibaijia.jsm.utils.*;
import org.apache.logging.log4j.status.StatusLogger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * web工具类
 */
public class WebContext {
    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();
    public static final String JSM_TRACE_ID = "jsm-trace-id";
    public static final String JSM_AT = "at";
    public static final String JSM_HT = "ht";
    public static final String JSM_TOKEN = "jsm-token";
    public static String JSM_AUTHORIZATION = "Authorization";//variable 业务设置
    public static final String NO_AUTH_HEADER = "no-auth-header";//?tell jsm frame no auth header , get auth header return null
    public static final String JSM_RESP_CODE = "jsm-resp-code";
    public static final String JSM_MOCK = "jsm-mock";
    private static AtomicInteger traceIdAi = new AtomicInteger(0);

    private static String clsId;

    private static String getClsId() {
        if (clsId == null) {
            clsId = "req-" + AppContext.getClusterId() + "-";
        }
        return clsId;
    }

    private static Map<String, String> traceIdMap = new HashMap<>();

    public static void setRequest(HttpServletRequest request) {
        ThreadLocalUtil.requestTL.set(request);
    }

    public static HttpServletRequest getRequest() {
        if (ThreadLocalUtil.requestTL.get() != null) {
            return ThreadLocalUtil.requestTL.get();
        } else {
            return getJspRequest();
        }
    }

    public static boolean isRequestMethod(String method) {
        if (getRequest() == null) {
            return false;
        }
        return getRequest().getMethod().equals(method);
    }

    public static void clearTraceId() {
        String key = "traceId_" + Thread.currentThread().getName();
        traceIdMap.remove(key);
    }

    public static void setTraceId(String traceId) {
        setTraceId(Thread.currentThread().getName(), traceId);
    }

    public static void setTraceId(String threadName, String traceId) {
        String key = "traceId_" + threadName;
        traceIdMap.put(key, traceId);
    }

    public static String getTraceId(String threadName) {
        String key = "traceId_" + threadName;
        return traceIdMap.get(key);
    }

    public static String getTraceId() {
        HttpServletRequest request = getRequest();
        String key = "traceId_" + Thread.currentThread().getName();
        String traceId = traceIdMap.get(key);
        if (traceId == null && request != null) {
            traceId = request.getHeader(WebContext.JSM_TRACE_ID);
            if (traceId == null) {
                traceId = request.getParameter(WebContext.JSM_TRACE_ID);
            }
            if (traceId == null) {
                traceId = (String) request.getAttribute(WebContext.JSM_TRACE_ID);
            }
            if (traceId == null) {//如果还没有则生成个
                StringBuilder traceSb = new StringBuilder();
                String hexIp = IpUtil.ipToHex(getRemoteIp());
                int intCount = traceIdAi.getAndIncrement();
                if (intCount > Integer.MAX_VALUE - 100) {
                    traceIdAi.set(0);
                }
                traceSb.append(getClsId()).append(hexIp).append("-").append(intCount);
                traceId = traceSb.toString();
                request.setAttribute(WebContext.JSM_TRACE_ID, traceId);
            }
            traceIdMap.put(key, traceId);
        }
        return traceId;
    }

    public static String getRequestAt() {
        HttpServletRequest request = getRequest();
        String at = null;
        if (request != null && StringUtil.isEmpty(request.getAttribute(NO_AUTH_HEADER))) {
            return getFromAllScope(WebContext.JSM_AT);
        }
        return at;
    }

    public static String getRequestHt() {
        HttpServletRequest request = getRequest();
        String ht = null;
        if (request != null && StringUtil.isEmpty(request.getAttribute(NO_AUTH_HEADER))) {
            return getFromAllScope(WebContext.JSM_HT);
        }
        return ht;
    }

    public static String getRequestToken() {
        HttpServletRequest request = getRequest();
        String token = null;
        if (request != null && StringUtil.isEmpty(request.getAttribute(NO_AUTH_HEADER))) {
            return getFromHeaderAndParam(WebContext.JSM_TOKEN);
        }
        return token;
    }

    public static String getRequestAuthorization() {
        HttpServletRequest request = getRequest();
        String authorization = null;
        if (request != null && StringUtil.isEmpty(request.getAttribute(NO_AUTH_HEADER))) {
            return getFromHeaderAndParam(WebContext.JSM_AUTHORIZATION);
        }
        return authorization;
    }

    private static String getFromAllScope(String key) {
        HttpServletRequest request = getRequest();
        String value = request.getHeader(key);
        if (value == null) {
            value = request.getParameter(key);
        }
        if (value == null) {
            if (value == null) {
                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals(key)) {
                            value = cookie.getValue();
                        }
                    }
                }
            }
        }
        return value;
    }

    public static String getFromHeaderAndParam(String key) {
        HttpServletRequest request = getRequest();
        String value = request.getHeader(key);
        if (value == null) {
            value = request.getParameter(key);
        }
        return value;
    }

    private static HttpServletRequest getJspRequest() {
        ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        return servletRequestAttributes == null ? null : servletRequestAttributes.getRequest();
    }

    public static String currentSessionId() {
        Session session = currentSession();
        return session == null ? null : session.getToken();
    }

    public static Session currentSession() {
        HttpServletRequest request = getRequest();
        return (Session) (request == null ? null : request.getAttribute(BasePairConstants.SESSION_ATTR_KEY));
    }

//    public static void removeRequest() {
//        ThreadLocalUtil.requestTL.remove();
//    }

    public static void setResponse(HttpServletResponse response) {
        ThreadLocalUtil.responseTL.set(response);
    }

    public static HttpServletResponse getResponse() {
        if (ThreadLocalUtil.responseTL.get() != null) {
            return ThreadLocalUtil.responseTL.get();
        } else {
            return getJspResponse();
        }
    }

    public static HttpServletResponse getJspResponse() {
        ServletWebRequest servletWebRequest = ((ServletWebRequest) RequestContextHolder.getRequestAttributes());
        return servletWebRequest == null ? null : servletWebRequest.getResponse();
    }

//    public static void removeResponse() {
//        ThreadLocalUtil.responseTL.remove();
//    }

    public static void redirect(String action) throws IOException {
        getResponse().sendRedirect(getContextPath() + action);
    }

    public static boolean isRest() {
        String uri = getRequest().getRequestURI().toLowerCase();
        return !uri.contains(".");
    }

    private static void sendAjaxError(String msg) {
        RestResp<String> baseResp = new RestResp<String>();
        baseResp.setPair(BasePairConstants.ERROR);
        baseResp.result = msg;
        try {
            getResponse().addHeader("Content-Type", "text/json;charset=UTF-8");
            getResponse().addHeader("Content-Type", "application/json;charset=UTF-8");
            getResponse().getWriter().write(StringUtil.toJson(baseResp));
            getResponse().getWriter().flush();
            getResponse().getWriter().close();
        } catch (Exception e) {
            logger.error("sendAjaxError error!", e);
        }
    }

    private static String contextPath;

    private static String realPath;

    public static String getRealPath() {
        return realPath;
    }

    public static void setRealPath(String realPath) {
        WebContext.realPath = realPath;
    }

    public static String getContextPath() {
        return contextPath;
    }

    public static void setContextPath(String contextPath) {
        WebContext.contextPath = contextPath;
    }


    /**
     * @param ext 返回一个随机产生指定扩展名的文件名
     */
    public static String getRandomFileName(String type, String ext) {
        StringBuilder sb = new StringBuilder();
        sb.append(type).append("_").append(DateUtil.format(new Date(), "yyyyMMddHHmmss"));
        sb.append("_").append(new Random().nextInt(1000)).append(".").append(ext);
        return sb.toString();
    }

    /**
     * 保存excel对象并返回保存后的文件名
     */
    public static String saveExcel(HSSFWorkbook workbook) throws IOException {
        String fileName = getRandomFileName("temp", "xls");
        OutputStream os = new FileOutputStream(fileName);
        workbook.write(os);
        os.close();
        return fileName.substring(fileName.lastIndexOf("\\") + 1);
    }

    /**
     * 取得当前用户
     */
    public static SessionUser currentUser() {
        Session session = currentSession();
        if (session == null) {
            return null;
        } else {
            return session.getSessionUser();
        }
    }

    public static String currentUserId() {
        SessionUser currentUser = currentUser();
        if (currentUser == null) {
            return null;
        } else {
            return currentUser.getUid();
        }
    }

    /**
     * 取得当前用户权限
     */
    public static List<String> currentPermissions() {
        SessionUser sessionUser = currentUser();
        if (sessionUser == null) {
            return null;
        }
        return sessionUser.getPermissions();
    }

    /**
     * 取得当前用户角色
     */
    public static List<Long> currentRoles() {
        SessionUser sessionUser = currentUser();
        if (sessionUser == null) {
            return null;
        }
        return sessionUser.getRoles();
    }

    /**
     * 有些问题
     */
    public static boolean isAjax() {
        String type = getRequest().getHeader("X-Requested-With");
        return "XMLHttpRequest".equals(type);
    }

    public static String getFirstForwardIp() {
        String[] ips = getForwardedIps();
        if (ips == null) {
            return null;
        }
        return ips[0];
    }

    public static String getLastForwardIp() {
        String[] ips = getForwardedIps();
        if (ips == null) {
            return null;
        }
        return ips[ips.length - 1];
    }

    public static String[] getForwardedIps() {
        HttpServletRequest request = getRequest();
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip != null && ip.contains(",")) {
            ip = ip.replaceAll(" ", "");
        }
        return ip == null ? null : ip.split(",");
    }

    public static String getRemoteIp() {
        HttpServletRequest request = getRequest();
        if (request == null) {
            return null;
        }
        return getRemoteIp(request);
    }

    public static String getRemoteIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip != null && ip.contains(",")) {
            ip = ip.split(",")[0];
        }
        return ip;
    }

    public static void setJspPath(String path) {
        ThreadLocalUtil.jspPathTL.set(path);
    }

    public static String getJspPath() {
        String path = ThreadLocalUtil.jspPathTL.get();
        ThreadLocalUtil.jspPathTL.remove();
        return path;
    }

    public static String getBackUrl() {
        if ("GET".equals(WebContext.getRequest().getMethod())) {
            return getActionUri();
        } else {
            String url = WebContext.getRequest().getHeader("REFERER");
            return url.substring(url.indexOf("=") + 1);
        }
    }

    public static byte[] getRequestBodyBytes(HttpServletRequest request) {
        try {
            int contentLength = request.getContentLength();
            if (contentLength < 0) {
                return null;
            }
            byte buffer[] = new byte[contentLength];
            for (int i = 0; i < contentLength; ) {
                int readlen = request.getInputStream().read(buffer, i, contentLength - i);
                if (readlen == -1) {
                    break;
                }
                i += readlen;
            }
            return buffer;
        } catch (IOException e) {
            logger.error("getRequestBodyBytes error!", e);
            return null;
        }
    }

    public static String getRequestBody(HttpServletRequest request) {
        try {
            byte buffer[] = getRequestBodyBytes(request);
            String charEncoding = request.getCharacterEncoding();
            if (charEncoding == null) {
                charEncoding = "UTF-8";
            }
            if (buffer != null) {
                return new String(buffer, charEncoding);
            }
        } catch (Exception e) {
            logger.error("getRequestBody error!", e);
        }
        return null;
    }

    public static String getRequestBody() {
        return getRequestBody(WebContext.getRequest());
    }

    /**
     * 取得相对于当前系统actionURI
     */
    public static String getActionUri() {
        return WebContext.getRequest().getRequestURI().substring(WebContext.getContextPath().length());
    }
}
