package cn.ibaijia.jsm.context;

import cn.ibaijia.jsm.utils.IpUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.PropUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 加载顺序 project.properties --> app-base.properties --> app.properties
 */
public class AppContext {
    private static Logger logger = LogUtil.log(AppContext.class);
    private static Map<String, String> propMap = new HashMap<>();
    private static Environment environment;
    private static Boolean devModel;
    private static Boolean jedisSession;
    private static String projectName;
    private static String clusterId;
    private static String optLogUrl;
    private static Integer sessionExpireTime;
    private static Integer atExpireTime;

    static {
        setInitData();
    }

    private static void setInitData() {
        set(AppContextKey.REGEX_VALID_EMAIL, "[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?");
        set(AppContextKey.REGEX_VALID_CHINESE, " [\\u4E00-\\u9FA5]");
        set(AppContextKey.REGEX_VALID_IPV4, "^\\d+\\.\\d+\\.\\d+\\.\\d+$");
        set(AppContextKey.REGEX_VALID_DATE, "^\\d{4}-\\d{1,2}-\\d{1,2}$");
        set(AppContextKey.REGEX_VALID_DATETIME, "^\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}$");
        set(AppContextKey.REGEX_VALID_URL, "[a-zA-z]+://[^\\s]*");
        set(AppContextKey.REGEX_VALID_IDCARD, "^(\\d{6})(\\d{4})(\\d{2})(\\d{2})(\\d{3})([0-9]|X)$");
        set(AppContextKey.REGEX_VALID_TELNO, "^(\\(\\d{3,4}-)|\\d{3.4}-)?\\d{7,8}$");
        set(AppContextKey.REGEX_VALID_MOBNO, "^(13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\\d{8}$");
        set(AppContextKey.REGEX_VALID_NUMBER, "^[0-9]*$");
        set(AppContextKey.REGEX_VALID_ENNUM, "^[A-Za-z0-9]+$");
        set(AppContextKey.REGEX_VALID_ACCOUNT, "^[a-zA-Z][a-zA-Z0-9_]+$");
        set(AppContextKey.REGEX_VALID_MONEY, "^(([1-9]\\d{0,9})|0)(\\.\\d{1,2})?$");
        set(AppContextKey.REGEX_VALID_BANKCARD, "^(\\d{16}|\\d{19})$");
        set(AppContextKey.REGEX_EXTRACT_MONEY, "(([1-9]\\d{0,9})|0)(\\.\\d{1,2})?");
        set(AppContextKey.REGEX_EXTRACT_NUMBER, "\\d+(\\.\\d+)?");
    }

    public static boolean isDevModel() {
        if (devModel == null) {
            devModel = getAsBoolean(AppContextKey.DEV_MODEL);
        }
        return devModel;
    }

    public static boolean isJedisSession() {
        if (jedisSession == null) {
            jedisSession = getAsBoolean(AppContextKey.JEDIS_SESSION);
        }
        return jedisSession;
    }

    public static String getProjectName() {
        if (projectName == null) {
            projectName = get(AppContextKey.PROJECT_NAME);
        }
        return projectName;
    }

    public static String getClusterId() {
        if (clusterId == null) {
            clusterId = get(AppContextKey.CLUSTER_ID);
            if (clusterId == null) {
                clusterId = getProjectName() + "." + IpUtil.ipToHex(IpUtil.getHostIp());
            }
        }
        return clusterId;
    }

    public static String getOptLogUrl() {
        if (optLogUrl == null) {
            optLogUrl = get(AppContextKey.OPT_LOG_RECORD_URL, "");
        }
        return optLogUrl;
    }

    public static int getSessionExpireTime() {
        if (sessionExpireTime == null) {
            sessionExpireTime = AppContext.getAsInteger(AppContextKey.SESSION_EXPIRE_TIME, 14400);
        }
        return sessionExpireTime;
    }

    public static int getAtExpireTime() {
        if (atExpireTime == null) {
            atExpireTime = AppContext.getAsInteger(AppContextKey.AT_EXPIRE_TIME, 60000);
        }
        return atExpireTime;
    }

    public static String getDirect(String key) {
        String val = PropUtil.get(key, "app.properties");
        if (val != null) {
            return val;
        }
        val = PropUtil.get(key, "project.properties");
        if (val != null) {
            return val;
        }
        return null;
    }

    public static String get(String key, String defaultVal) {
        String res = get(key);
        return res == null ? defaultVal : res;
    }

    public static String get(String key) {
        String res = getFromSpringEnv(key);
        if (!StringUtil.isEmpty(res)) {
            return res;
        }
        res = propMap.get(key);
        if (!StringUtil.isEmpty(res)) {
            return res;
        }
        res = System.getProperty(key);
        if (!StringUtil.isEmpty(res)) {
            return res;
        }
        return System.getenv(key);
    }

    private static String getFromSpringEnv(String key) {
        if (environment == null) {
            return null;
        }
        return environment.getProperty(key);
    }

    public static Integer getAsInteger(String key) {
        return getAsInteger(key, null);
    }

    public static Integer getAsInteger(String key, Integer defaultVal) {
        try {
            String val = get(key);
            return val == null ? defaultVal : StringUtil.toInteger(val, defaultVal);
        } catch (Exception e) {
            logger.error("getAsInteger error! key {}", key);
            return null;
        }
    }

    public static Short getAsShort(String key) {
        return getAsShort(key, null);
    }

    public static Short getAsShort(String key, Short defaultVal) {
        try {
            String val = get(key);
            return val == null ? defaultVal : StringUtil.toShort(val, defaultVal);
        } catch (Exception e) {
            logger.error("getAsShort error! key {}", key);
            return null;
        }
    }

    public static Byte getAsByte(String key) {
        return getAsByte(key, null);
    }

    public static Byte getAsByte(String key, Byte defaultVal) {
        try {
            String val = get(key);
            return val == null ? defaultVal : StringUtil.toByte(val, defaultVal);
        } catch (Exception e) {
            logger.error("getAsByte error! key {}", key);
            return null;
        }
    }

    public static Float getAsFloat(String key) {
        return getAsFloat(key, null);
    }

    public static Float getAsFloat(String key, Float defaultVal) {
        try {
            String val = get(key);
            return val == null ? defaultVal : StringUtil.toFloat(val, defaultVal);
        } catch (Exception e) {
            logger.error("getAsFloat error! key {}", key);
            return null;
        }
    }

    public static Long getAsLong(String key) {
        return getAsLong(key, null);
    }

    public static Long getAsLong(String key, Long defaultVal) {
        try {
            String val = get(key);
            return val == null ? defaultVal : StringUtil.toLong(val, defaultVal);
        } catch (Exception e) {
            logger.error("getAsLong error! key {}", key);
            return null;
        }
    }

    public static Boolean getAsBoolean(String key) {
        return getAsBoolean(key, null);
    }

    public static Boolean getAsBoolean(String key, Boolean defaultVal) {
        try {
            String val = get(key);
            return val == null ? defaultVal : StringUtil.toBoolean(val, defaultVal);
        } catch (Exception e) {
            logger.error("getAsBoolean error! key {}", key);
            return null;
        }
    }

    private static void setProxy() {
        Properties prop = System.getProperties();
        String httpProxyHost = get(AppContextKey.HTTP_PROXY_HOST);
        String httpProxyPort = get(AppContextKey.HTTP_PROXY_PORT);
        String httpNonProxyHosts = get(AppContextKey.HTTP_NON_PROXY_HOSTS);
        String httpsProxyHost = get(AppContextKey.HTTPS_PROXY_HOST);
        String httpsProxyPort = get(AppContextKey.HTTPS_PROXY_PORT);
        String httpsNonProxyHosts = get(AppContextKey.HTTPS_NON_PROXY_HOSTS);
        String ftpProxyHost = get(AppContextKey.FTP_PROXY_HOST);
        String ftpProxyPort = get(AppContextKey.FTP_PROXY_PORT);
        String ftpNonProxyHosts = get(AppContextKey.FTP_NON_PROXY_HOSTS);
        String socksProxyHost = get(AppContextKey.SOCKS_PROXY_HOST);
        String socksProxyPort = get(AppContextKey.SOCKS_PROXY_PORT);
        String socksNonProxyHosts = get(AppContextKey.SOCKS_NON_PROXY_HOSTS);

        if (!StringUtil.isEmpty(httpProxyHost)) {
            System.out.println("http.proxyHost:" + httpProxyHost);
            System.out.println("http.proxyPort:" + httpProxyPort);
            System.out.println("http.nonProxyHosts:" + httpNonProxyHosts);
            prop.setProperty("http.proxyHost", httpProxyHost);
            prop.setProperty("http.proxyPort", httpProxyPort);
            prop.setProperty("http.nonProxyHosts", httpNonProxyHosts);
        }
        if (!StringUtil.isEmpty(httpsProxyHost)) {
            System.out.println("https.proxyHost:" + httpsProxyHost);
            System.out.println("https.proxyPort:" + httpsProxyPort);
            System.out.println("https.nonProxyHosts:" + httpsNonProxyHosts);
            prop.setProperty("https.proxyHost", httpsProxyHost);
            prop.setProperty("https.proxyPort", httpsProxyPort);
            prop.setProperty("https.nonProxyHosts", httpsNonProxyHosts);
        }
        if (!StringUtil.isEmpty(ftpProxyHost)) {
            System.out.println("ftp.proxyHost:" + ftpProxyHost);
            System.out.println("ftp.proxyPort:" + ftpProxyPort);
            System.out.println("ftp.nonProxyHosts:" + ftpNonProxyHosts);
            prop.setProperty("ftp.proxyHost", ftpProxyHost);
            prop.setProperty("ftp.proxyPort", ftpProxyPort);
            prop.setProperty("ftp.nonProxyHosts", ftpNonProxyHosts);
        }
        if (!StringUtil.isEmpty(socksProxyHost)) {
            System.out.println("socks.proxyHost:" + socksProxyHost);
            System.out.println("socks.proxyPort:" + socksProxyPort);
            prop.setProperty("socksProxyHost", socksProxyHost);
            prop.setProperty("socksProxyPort", socksProxyPort);
            prop.setProperty("socksNonProxyHosts", socksNonProxyHosts);
        }
        String proxyUsername = get(AppContextKey.PROXY_USERNAME);
        String proxyPassword = get(AppContextKey.PROXY_PASSWORD);
        if (!StringUtil.isEmpty(proxyUsername)) {
            System.out.println("proxy.username:" + proxyUsername);
            final String username = proxyUsername;
            final String password = proxyPassword;
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password.toCharArray());
                }

            });
        }
    }

    public static void init() {
        String propFile = "project.properties";
        System.out.println("load:" + propFile);
        Properties properties = PropUtil.getProperties(propFile);
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            propMap.put((String) entry.getKey(), (String) entry.getValue());
        }
        propFile = "app.properties";
        System.out.println("load:" + propFile);
        properties = PropUtil.getProperties(propFile);
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            propMap.put((String) entry.getKey(), (String) entry.getValue());
        }
        System.out.println("props:" + StringUtil.toJson(propMap));
        setProxy();
    }

    public static String set(String key, String value) {
        System.getProperties().setProperty(key, value);
        return propMap.put(key, value);
    }

    public static void setEnvironment(Environment environment) {
        AppContext.environment = environment;
    }
}
