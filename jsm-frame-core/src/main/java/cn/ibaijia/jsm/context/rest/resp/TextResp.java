package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
/**
 * @author longzl
 */
public class TextResp implements JsmResp{
	@FieldAnn(comments = "text String")
	public String text;
}
