package cn.ibaijia.jsm.context.rest.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.cache.jedis.ShardedJedisCmd;
import cn.ibaijia.jsm.context.rest.order.BaseOrderLine;
import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.utils.TemplateUtil;
import redis.clients.jedis.ShardedJedis;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicLong;

public class FairLineService extends BaseOrderLine {

    @Resource
    private JedisService jedisService;

    private AtomicLong seed = new AtomicLong();

    @Override
    public Pair<String> order(String key, OrderAnn orderAnn) {

        if (getStatus().equals(BasePairConstants.ACTIVITY_NOT_STARTED) || getStatus().equals(BasePairConstants.ACTIVITY_OVER)) {
            return getStatus();
        }
        String member = TemplateUtil.formatWithContextVar(orderAnn.userKey());;
        Long idx = jedisService.exec(new ShardedJedisCmd<Long>() {
            @Override
            public Long run(ShardedJedis shardedJedis) throws Exception {
//                Redis Zrank 返回有序集中指定成员的排名。其中有序集成员按分数值递增(从小到大)顺序排列。
                Long idx = shardedJedis.zrank(key, member);
                if (idx == null) {//加入
                    shardedJedis.zadd(key, seed.incrementAndGet(), member);
                } else {//更新
                    shardedJedis.zincrby(key, -1, member);
                }
                return shardedJedis.zrank(key, member);
            }
        });
        if (idx == 0) {
            return BasePairConstants.ACTIVITY_TURN;
        } else {
            return BasePairConstants.ACTIVITY_QUEUEING;
        }
    }
}
