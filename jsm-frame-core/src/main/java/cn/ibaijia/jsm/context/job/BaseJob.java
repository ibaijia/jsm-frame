package cn.ibaijia.jsm.context.job;

import cn.ibaijia.jsm.utils.LogUtil;
import org.quartz.Job;
import org.slf4j.Logger;

public abstract class BaseJob implements Job {

    protected Logger logger = LogUtil.log(getClass());

    public void run() {
        try {
            execute(null);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

}
