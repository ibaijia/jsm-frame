package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import io.swagger.annotations.ApiParam;
import org.apache.http.Header;

import java.util.List;

/**
 * @author longzl
 */
public class FileResp implements JsmResp {
	@FieldAnn(comments = "自定义headers")
	public List<Header> headers;
	@FieldAnn(comments = "文件名")
	public String fileName;
	@ApiParam(hidden = true)
	@FieldAnn(comments = "输出流对象,String,Object,InputStream.")
	public Object outputObject;
}
