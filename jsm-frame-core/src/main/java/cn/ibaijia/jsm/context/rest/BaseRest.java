package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.context.interceptor.MyDateEditor;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.ExceptionUtil;
import cn.ibaijia.jsm.utils.JsmFrameUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public abstract class BaseRest {
    protected Logger logger = LogUtil.log(getClass());

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new MyDateEditor());
    }

    @ExceptionHandler(Exception.class)
    public RestResp globalExceptionHandler(Exception e) {
        logger.error("globalExceptionHandler error.", e);
        return JsmFrameUtil.dealException(e);
    }

    protected void failed(String errorMsg) {
        ExceptionUtil.failed(errorMsg);
    }

    protected void failed(Pair errorPair) {
        ExceptionUtil.failed(errorPair);
    }

    protected void failed(Pair errorPair, String errorMsg) {
        ExceptionUtil.failed(errorPair, errorMsg);
    }

    protected void notFound(String errorMsg) {
        ExceptionUtil.notFound(errorMsg);
    }

    protected void notFound(Pair errorPair) {
        ExceptionUtil.notFound(errorPair);
    }

    protected void notFound(Pair errorPair, String errorMsg) {
        ExceptionUtil.notFound(errorPair, errorMsg);
    }

    protected <T> RestResp<T> success(T result) {
        return success(result, "1001", "success");
    }

    protected <T> RestResp<T> success(T result, String code) {
        return success(result, code, "success");
    }

    protected <T> RestResp<T> success(T result, String code, String message) {
        RestResp<T> resp = new RestResp();
        resp.result = result;
        resp.code = code;
        resp.message = message;
        return resp;
    }

}
