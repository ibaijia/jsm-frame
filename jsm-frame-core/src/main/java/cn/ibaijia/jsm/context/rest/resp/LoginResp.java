package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
/**
 * @author longzl
 */
public class LoginResp implements ValidateModel {
    @FieldAnn(comments = "加密后的token")
    public String token;
    @FieldAnn(comments = "开发期间的token")
    public String dev_token;
    @FieldAnn(comments = "服务器时间")
    public Long serverTime;
}
