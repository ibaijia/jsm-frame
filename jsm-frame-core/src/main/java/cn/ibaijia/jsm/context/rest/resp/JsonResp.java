package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author longzl
 */
public class JsonResp implements JsmResp {
	@FieldAnn(comments = "json String")
	public String jsonStr;
}
