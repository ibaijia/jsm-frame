package cn.ibaijia.jsm.context.service;

import java.util.List;

public interface JsmSecurityService {
	public boolean hasRole(Long roleId);
	public boolean hasAnyRole(List<Long> roleIdList);
	public boolean hasPermission(String permissionCode);
	public boolean hasAnyPermission(List<String> permissionCodeList);
}
