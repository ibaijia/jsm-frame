package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * 输出自定义contentType,默认与HtmlResp一致
 * @author longzl
 */
public class FreeResp implements JsmResp {
	@FieldAnn(comments = "contentType")
	public String contentType = "text/html;charset=UTF-8";
	@FieldAnn(comments = "content String")
	public String content;
}
