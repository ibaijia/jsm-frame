package cn.ibaijia.jsm.context.listener;

import javax.servlet.ServletContextEvent;

public interface JsmAppListenerService {
	
	void init(ServletContextEvent sce);
	
	void close(ServletContextEvent sce);
	
}
