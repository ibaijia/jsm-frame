package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
/**
 * @author longzl
 */
public class ExcelResp implements JsmResp {
	@FieldAnn(comments = "文件名")
	public String fileName;
	@ApiParam(hidden = true)
	@FieldAnn(comments = "输出工作簿对象")
	public Workbook  workbook;
}
