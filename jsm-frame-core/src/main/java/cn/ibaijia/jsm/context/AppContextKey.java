package cn.ibaijia.jsm.context;

public class AppContextKey {

    public static final String DEV_MODEL = "jsm.dev.model";
    public static final String JEDIS_SESSION = "jsm.jedis.session";
    public static final String SESSION_EXPIRE_TIME = "jsm.session.expireTime";
    public static final String AT_EXPIRE_TIME = "jsm.at.expireTime";

    public static final String PROJECT_NAME = "jsm.projectName";
    public static final String CLUSTER_ID = "jsm.clusterId";
    public static final String OPT_LOG_RECORD_URL = "jsm.optlog.recordUrl";

    public static final String CACHE_SERVICE_CACHE_NAME = "cache.service.cache.name";
    public static final String CACHE_SERVICE_ASYNC_THREAD = "cache.service.async.thread";


    public static final String PROXY_USERNAME = "proxy.username";
    public static final String PROXY_PASSWORD = "proxy.password";

    public static final String HTTP_PROXY_HOST = "http.proxyHost";
    public static final String HTTP_PROXY_PORT = "http.proxyPort";
    public static final String HTTP_NON_PROXY_HOSTS = "http.nonProxyHosts";

    public static final String HTTPS_PROXY_HOST = "https.proxyHost";
    public static final String HTTPS_PROXY_PORT = "https.proxyPort";
    public static final String HTTPS_NON_PROXY_HOSTS = "https.nonProxyHosts";

    public static final String FTP_PROXY_HOST = "ftp.proxyHost";
    public static final String FTP_PROXY_PORT = "ftp.proxyPort";
    public static final String FTP_NON_PROXY_HOSTS = "ftp.nonProxyHosts";

    public static final String SOCKS_PROXY_HOST = "socks.proxyHost";
    public static final String SOCKS_PROXY_PORT = "socks.proxyPort";
    public static final String SOCKS_NON_PROXY_HOSTS = "socks.nonProxyHosts";


    public static final String CORS = "jsm.cors.open";
    public static final String CORS_ALLOW_ORIGIN = "jsm.cors.allowOrigin";
    public static final String CORS_ALLOW_METHODS = "jsm.cors.allowMethods";
    public static final String CORS_ALLOW_HEADERS = "jsm.cors.allowHeaders";
    public static final String CORS_ALLOW_CREDENTIALS = "jsm.cors.allowCredentials";
    public static final String CORS_MAX_AGE = "jsm.cors.maxAge";


    public static final String OAUTH_CLIENT_TOKEN_NAME = "jsm.oauth.client.tokenName";
    public static final String OAUTH_CLIENT_VERIFY_URL = "jsm.oauth.client.verifyUrl";
    public static final String OAUTH_CLIENT_EXPIRE_IN = "jsm.oauth.client.expireIn";
    public static final String OAUTH_CLIENT_APP_KEY = "jsm.oauth.client.appKey";
    public static final String OAUTH_CLIENT_APP_SECRET = "jsm.oauth.client.appSecret";
    public static final String OAUTH_CLIENT_TOKEN_URL = "jsm.oauth.client.getTokenUrl";
    public static final String OAUTH_CLIENT_REFRESH_TOKEN_URL = "jsm.oauth.client.refreshTokenUrl";


    public static final String OAUTH_PASSWORD_TOKEN_NAME = "jsm.oauth.password.tokenName";
    public static final String OAUTH_PASSWORD_VERIFY_URL = "jsm.oauth.password.verifyUrl";
    public static final String OAUTH_PASSWORD_EXPIRE_IN = "jsm.oauth.password.expireIn";

    public static final String ELASTIC_ADDRESS = "jsm.elastic.address";

    public static final String LOG_TYPE = "jsm.log.type";
    public static final String LOG_IDX = "jsm.log.idx";

    public static final String API_STAT_TYPE = "jsm.apiStat.type";
    public static final String API_STAT_IDX = "jsm.apiStat.idx";

    public static final String SYS_STAT_TYPE = "jsm.sysStat.type";
    public static final String SYS_STAT_IDX = "jsm.sysStat.idx";
    public static final String SYS_STAT_INTERVAL = "jsm.sysStat.interval";

    public static final String OPT_LOG_TYPE = "jsm.optLog.type";
    public static final String OPT_LOG_IDX = "jsm.optLog.idx";

    public static final String ALARM_TYPE = "jsm.alarm.type";
    public static final String ALARM_IDX = "jsm.alarm.idx";
    public static final String ALARM_MAX_COUNT = "jsm.alarm.max";

    public static final String SLOW_API_LIMIT = "jsm.slow.apiLimit";
    public static final String SLOW_SQL_LIMIT = "jsm.slow.sqlLimit";

    public static final String SWAGGER_ENABLE = "jsm.swagger.enable";
    public static final String SWAGGER_APIS_PACKAGE = "jsm.swagger.apis.package";
    public static final String SWAGGER_APIS_TITLE = "jsm.swagger.apis.title";
    public static final String SWAGGER_APIS_VERSION = "jsm.swagger.apis.version";
    public static final String SWAGGER_APIS_DESCRIPTION = "jsm.swagger.apis.description";
    public static final String SWAGGER_APIS_HEADERS = "jsm.swagger.apis.headers";

    public static final String GEN_PACKAGE_MODEL = "jsm.gen.package.model";
    public static final String GEN_PACKAGE_MAPPER = "jsm.gen.package.mapper";
    public static final String GEN_PACKAGE_SERVICE = "jsm.gen.package.service";
    public static final String GEN_PACKAGE_REST = "jsm.gen.package.rest";
    public static final String GEN_DIR_API = "jsm.gen.dir.api";


    public static final String REGEX_VALID_EMAIL = "regex.valid.email";
    public static final String REGEX_VALID_CHINESE = "regex.valid.chinese";
    public static final String REGEX_VALID_IPV4 = "regex.valid.ipv4";
    public static final String REGEX_VALID_DATE = "regex.valid.date";
    public static final String REGEX_VALID_DATETIME = "regex.valid.datetime";
    public static final String REGEX_VALID_URL = "regex.valid.url";
    public static final String REGEX_VALID_IDCARD = "regex.valid.idcard";
    public static final String REGEX_VALID_TELNO = "regex.valid.telno";
    public static final String REGEX_VALID_MOBNO = "regex.valid.mobno";
    public static final String REGEX_VALID_NUMBER = "regex.valid.number";
    public static final String REGEX_VALID_ENNUM = "regex.valid.ennum";
    public static final String REGEX_VALID_ACCOUNT = "regex.valid.account";
    public static final String REGEX_VALID_MONEY = "regex.valid.money";
    public static final String REGEX_VALID_BANKCARD = "regex.valid.bankcard";
    public static final String REGEX_EXTRACT_MONEY = "regex.extract.money";
    public static final String REGEX_EXTRACT_NUMBER = "regex.extract.number";

}
