package cn.ibaijia.jsm.context.dao.mapper;

import cn.ibaijia.jsm.context.dao.model.CreateTableInfo;
import cn.ibaijia.jsm.context.dao.model.FieldInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DbMapper {
	
	List<String> showTables();
	
	List<CreateTableInfo> showCreateTable(@Param("tableName")String tableName);

	List<String> showMssqlTables();

	List<FieldInfo> listMssqlTableInfo(@Param("tableName")String tableName);

}
