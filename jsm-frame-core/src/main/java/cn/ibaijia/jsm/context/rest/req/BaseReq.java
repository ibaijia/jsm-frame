package cn.ibaijia.jsm.context.rest.req;

import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;

public abstract class BaseReq implements ValidateModel {

    protected Logger logger = LogUtil.log(getClass());

    public String toJSONString() {
        return StringUtil.toJson(this);
    }
}
