package cn.ibaijia.jsm.elastic;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.http.HttpAsyncClient;
import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpResponse;
import org.apache.http.concurrent.FutureCallback;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;
import org.springframework.util.Assert;

import java.util.List;

public class ElasticClient {

    private Logger LOGGER = StatusLogger.getLogger();

    private String username;
    private String password;
    private String indexAddress;
    private String type = "_doc";
    private String postAddress;
    private String header;
    private String batchHeader;
    private final static String VAR_ID = "varId";

    private HttpClient httpClient = new HttpClient();
    private HttpAsyncClient httpAsyncClient = new HttpAsyncClient();

    public ElasticClient(String indexAddress) {
        httpClient.setWithExtra(false);
        this.indexAddress = indexAddress;
        this.postAddress = indexAddress + "/" + this.type;
        String idx = indexAddress.substring(indexAddress.lastIndexOf("/") + 1);
        this.header = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"}}", idx, type);
        this.batchHeader = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}", idx, type, VAR_ID);
    }

    public ElasticClient(String indexAddress, String type) {
        this.indexAddress = indexAddress;
        if (type != null) {
            this.type = type;
        }
        this.postAddress = indexAddress + "/" + this.type;
        String idx = indexAddress.substring(indexAddress.lastIndexOf("/") + 1);
        this.header = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"}}", idx, type);
        this.batchHeader = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}", idx, type, VAR_ID);
    }

    public ElasticClient(String indexAddress, String type, String username, String password) {
        this.indexAddress = indexAddress;
        if (type != null) {
            this.type = type;
        }
        this.postAddress = indexAddress + "/" + this.type;
        this.username = username;
        this.password = password;
        String idx = indexAddress.substring(indexAddress.lastIndexOf("/") + 1);
        this.header = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"}}", idx, type);
        this.batchHeader = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}", idx, type, VAR_ID);
    }


    /**
     * 检查Index是否存在，存在返回true
     *
     * @return
     */
    public boolean checkIndex() {
        LOGGER.debug("ElasticClient checkIndex:" + indexAddress);
        String res = httpClient.get(indexAddress);
        LOGGER.debug("ElasticClient checkIndex res:" + res);
        Assert.notNull(res, "ElasticClient checkIndex result null.");
        JsonNode jsonNode = JsonUtil.readTree(res);
        Assert.notNull(jsonNode, "ElasticClient readTree null");
        JsonNode statusNode = jsonNode.get("status");
        return statusNode == null ? true : false;
    }

    /**
     * 创建索引
     *
     * @param jsonOption
     * @return
     */
    public String createIndex(String jsonOption) {
        LOGGER.debug("ElasticClient createIndex:" + jsonOption);
        String res = httpClient.put(indexAddress, jsonOption);
        LOGGER.debug("ElasticClient createIndex res:" + res);
        return res;
    }

    public String deleteIndex() {
        LOGGER.debug("ElasticClient deleteIndex:" + this.indexAddress);
        String res = httpClient.delete(indexAddress);
        LOGGER.debug("ElasticClient deleteIndex res:" + res);
        return res;
    }

    public String recreateIndex(String jsonOption) {
        deleteIndex();
        return createIndex(jsonOption);
    }

    /**
     * 检查不存在就创建索引
     *
     * @param jsonOption
     * @return
     */
    public boolean createIndexIfNotExists(String jsonOption) {
        boolean exists = checkIndex();
        if (!exists) {
            String res = createIndex(jsonOption);
            JsonNode jsonNode = JsonUtil.readTree(res);
            Assert.notNull(jsonNode, "createIndex result null");
            JsonNode errorNode = jsonNode.get("error");
            Assert.isNull(errorNode, "createIndex failed:" + res);
            return true;
        } else {
            LOGGER.info("ElasticClient index exists:" + indexAddress);
        }
        return exists;
    }

    /**
     * 添加文档
     *
     * @param jsonData
     * @return
     */
    public void post(String jsonData) {
        LOGGER.debug("ElasticClient post:" + jsonData);
        httpAsyncClient.post(postAddress, jsonData, new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse result) {
                LOGGER.debug("httpAsyncClient post res:" + result.getEntity());
            }

            @Override
            public void failed(Exception ex) {
                LOGGER.debug("httpAsyncClient post failed!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post failed!"));
            }

            @Override
            public void cancelled() {
                LOGGER.debug("httpAsyncClient post cancelled!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post cancelled!"));
            }
        });
    }

    public void post(String id, String jsonData) {
        LOGGER.debug("ElasticClient post:" + jsonData);
        httpAsyncClient.post(postAddress + "/" + id, jsonData, new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse result) {
                LOGGER.debug("httpAsyncClient post res:" + result.getEntity());
            }

            @Override
            public void failed(Exception ex) {
                LOGGER.debug("httpAsyncClient post failed!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post failed!"));
            }

            @Override
            public void cancelled() {
                LOGGER.debug("httpAsyncClient post cancelled!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post cancelled!"));
            }
        });
    }

    public void postBatch(List<BatchObject> batchObjectList) {
        if (batchObjectList == null || batchObjectList.isEmpty()) {
            return;
        }
        LOGGER.debug("ElasticClient post batchObjectList size:" + batchObjectList.size());
        StringBuilder sb = new StringBuilder();
        for (BatchObject batchObject : batchObjectList) {
            sb.append(this.batchHeader.replace(VAR_ID, batchObject.id)).append("\n");
            sb.append(batchObject.body).append("\n");
        }
        postToElastic(sb);
    }

    private void postToElastic(StringBuilder sb) {
        httpAsyncClient.post(postAddress + "/_bulk", sb.toString(), new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse result) {
                LOGGER.debug("httpAsyncClient post res:" + result.getEntity());
            }

            @Override
            public void failed(Exception ex) {
                LOGGER.debug("httpAsyncClient post failed!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post failed!"));
            }

            @Override
            public void cancelled() {
                LOGGER.debug("httpAsyncClient post cancelled!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post cancelled!"));
            }
        });
    }

    public void postBatchStr(List<String> jsonDataList) {
        if (jsonDataList == null || jsonDataList.isEmpty()) {
            return;
        }
        LOGGER.debug("ElasticClient post jsonDataList size:" + jsonDataList.size());
        StringBuilder sb = new StringBuilder();
        for (String jsonData : jsonDataList) {
            sb.append(header).append("\n");
            sb.append(jsonData).append("\n");
        }
        postToElastic(sb);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIndexAddress() {
        return indexAddress;
    }

    public void setIndexAddress(String indexAddress) {
        this.indexAddress = indexAddress;
    }
}
