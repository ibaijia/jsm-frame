package cn.ibaijia.jsm.elastic;

public class BatchObject {

    public BatchObject(String id, String body) {
        this.id = id;
        this.body = body;
    }

    public String id;

    public String body;

}
