package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.context.dao.model.FieldInfo;

import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2021/11/4 15:10
 */
public interface DbGenerationStrategy {

    /**
     * 获取所有table名
     *
     * @return
     */
    List<String> getTables();

    /**
     * 通过表名 取到表详情
     *
     * @param tableName
     * @param contextData
     * @return
     */
    List<FieldInfo> getTableFieldInfo(String tableName, Map<String, Object> contextData);

    /**
     * db类型转java类型
     *
     * @param dbType
     * @return
     */
    public default String dbTypeToJavaType(String dbType) {
        if (dbType.startsWith("bigint")) {
            return "Long";
        }
        if (dbType.startsWith("tinyint")) {
            return "Byte";
        }
        if (dbType.startsWith("smallint")) {
            return "Short";
        }
        if (dbType.startsWith("int") || dbType.startsWith("integer")) {
            return "Integer";
        }
        if (dbType.startsWith("varchar") || dbType.contains("text")) {
            return "String";
        }
        if (dbType.startsWith("bit")) {
            return "Boolean";
        }
        if (dbType.startsWith("float")) {
            return "Float";
        }
        if (dbType.startsWith("double")) {
            return "Double";
        }
        if (dbType.startsWith("date") || dbType.startsWith("timestamp")) {
            return "Date";
        }
        return "byte[]";
    }

    /**
     * 表名转实体类名
     *
     * @param tableName
     * @return
     */
    public default String tableNameToModelName(String tableName) {
        String clazzName = tableName.replace("-", "_");
        if (clazzName.endsWith("_t")) {
            clazzName = clazzName.substring(0, clazzName.length() - 2);
        }
        StringBuilder sb = new StringBuilder();
        boolean toUpper = true;
        for (int i = 0; i < clazzName.length(); i++) {
            char c = clazzName.charAt(i);
            if (c == '_') {
                toUpper = true;
                continue;
            }
            sb.append(toUpper ? Character.toUpperCase(c) : c);
            if (toUpper) {
                toUpper = false;
            }
        }
        return sb.toString();
    }

    /**
     * 列名转字段名
     *
     * @param columnName
     * @return
     */
    public default String columnNameToFieldName(String columnName) {
        return columnName;
    }

}
