package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.dao.mapper.DbMapper;
import cn.ibaijia.jsm.context.dao.model.FieldInfo;

import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2021/11/4 15:19
 */
public class DefaultMsSqlGenerationStrategy implements DbGenerationStrategy {
    @Override
    public List<String> getTables() {
        DbMapper dbMapper = SpringContext.getBean(DbMapper.class);
        return dbMapper.showMssqlTables();
    }

    @Override
    public List<FieldInfo> getTableFieldInfo(String tableName, Map<String, Object> contextData) {
        DbMapper dbMapper = SpringContext.getBean(DbMapper.class);
        List<FieldInfo> fieldInfoList = dbMapper.listMssqlTableInfo(tableName);
        for (FieldInfo fieldInfo : fieldInfoList) {
            fieldInfo.fieldName = columnNameToFieldName(fieldInfo.fieldName);
            fieldInfo.fieldType = dbTypeToJavaType(fieldInfo.fieldType);
            if (fieldInfo.fieldType.equals("Date")) {
                contextData.put("hasDate", true);
            }
        }
        return fieldInfoList;
    }

}
