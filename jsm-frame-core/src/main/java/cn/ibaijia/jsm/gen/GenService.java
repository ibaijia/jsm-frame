package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.dao.model.ApiInfo;
import cn.ibaijia.jsm.context.dao.model.ControllerInfo;
import cn.ibaijia.jsm.context.dao.model.FieldInfo;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.utils.*;
import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import io.swagger.annotations.ApiOperation;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author longzl
 */
@Service
public class GenService extends BaseService {

    private String modelPackageName;
    private String mapperPackageName;
    private String servicePackageName;
    private String restPackageName;
    private String frontApiDir;

    private DbGenerationStrategy dbGenerationStrategy = new DefaultMySqlGenerationStrategy();

    public void setDbGenerationStrategy(DbGenerationStrategy dbGenerationStrategy) {
        this.dbGenerationStrategy = dbGenerationStrategy;
    }

    private void check() {
        if (modelPackageName == null) {
            modelPackageName = AppContext.get(AppContextKey.GEN_PACKAGE_MODEL);
            mapperPackageName = AppContext.get(AppContextKey.GEN_PACKAGE_MAPPER);
            servicePackageName = AppContext.get(AppContextKey.GEN_PACKAGE_SERVICE);
            restPackageName = AppContext.get(AppContextKey.GEN_PACKAGE_REST);
            frontApiDir = AppContext.get(AppContextKey.GEN_DIR_API);
        }
    }

    public void genAll() {
        List<String> tableNameList = dbGenerationStrategy.getTables();
        for (String tableName : tableNameList) {
            if (tableName.equals("opt_log_t")) {
                logger.debug("ignore jsm frame table. opt_log_t");
                continue;
            }
            gen(tableName, false, true);
        }
    }

    public void gen(String tableName) {
        gen(tableName, false, true);
    }

    public void gen(String tableName, boolean override) {
        gen(tableName, override, false);
    }

    public void gen(String tableName, boolean override, boolean genApi) {
        gen(tableName, override, true, genApi);
    }

    public void gen(String tableName, boolean override, boolean update, boolean genApi) {
        check();
        String baseJavaDir = null;
        String mapperXmlDirPath = null;
        try {
            baseJavaDir = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator;
            mapperXmlDirPath = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mappers" + File.separator;
        } catch (IOException e) {
            logger.error("", e);
        }

        String className = dbGenerationStrategy.tableNameToModelName(tableName);
        String categoryName = StringUtil.lowerCaseFirst(className);


        String restReqPackageName = restPackageName + ".req." + categoryName;
        String restVoPackageName = restPackageName + ".vo." + categoryName;
        logger.info("baseJavaDir:{}", baseJavaDir);
        logger.info("modelPackageName:{}", modelPackageName);
        logger.info("mapperPackageName:{}", mapperPackageName);
        logger.info("mapperXmlDirPath:{}", mapperXmlDirPath);
        logger.info("servicePackageName:{}", servicePackageName);
        logger.info("restReqPackageName:{}", restReqPackageName);
        logger.info("restVoPackageName:{}", restVoPackageName);

        String mapperFtl = "mapper.ftl";
        String mapperXmlFtl = "mapperXml.ftl";
        String modelFtl = "model.ftl";
        String serviceFtl = "service.ftl";
        String addReqFtl = "addReq.ftl";
        String updateReqFtl = "updateReq.ftl";
        String pageReqFtl = "pageReq.ftl";
        String voFtl = "vo.ftl";
        String apiFtl = "api.ftl";
        logger.info("mapperFtl:{}", mapperFtl);
        logger.info("mapperXmlFtl:{}", mapperXmlFtl);
        logger.info("modelFtl:{}", modelFtl);
        logger.info("serviceFtl:{}", serviceFtl);
        logger.info("addReqFtl:{}", addReqFtl);
        logger.info("updateReqFtl:{}", updateReqFtl);
        logger.info("pageReqFtl:{}", pageReqFtl);
        logger.info("voFtl:{}", voFtl);
        logger.info("apiFtl:{}", apiFtl);

        String modelDirPath = baseJavaDir + modelPackageName.replace(".", File.separator);
        String mapperDirPath = baseJavaDir + mapperPackageName.replace(".", File.separator);
        String serviceDirPath = baseJavaDir + servicePackageName.replace(".", File.separator);
        String restReqDirPath = baseJavaDir + restReqPackageName.replace(".", File.separator);
        String restVoDirPath = baseJavaDir + restVoPackageName.replace(".", File.separator);
        String restApiDirPath = baseJavaDir + restPackageName.replace(".", File.separator);

        makDirs(modelDirPath);
        makDirs(mapperDirPath);
        makDirs(mapperXmlDirPath);
        makDirs(serviceDirPath);
        makDirs(restReqDirPath);
        makDirs(restVoDirPath);
        makDirs(restApiDirPath);


        String modelFileName = modelDirPath + File.separator + className + ".java";
        String mapperFileName = mapperDirPath + File.separator + className + "Mapper.java";
        String mapperXmlFileName = mapperXmlDirPath + File.separator + className + "Mapper.xml";
        String serviceFileName = serviceDirPath + File.separator + className + "Service.java";
        String restAddReqFileName = restReqDirPath + File.separator + className + "AddReq.java";
        String restUpdateReqFileName = restReqDirPath + File.separator + className + "UpdateReq.java";
        String restPageReqFileName = restReqDirPath + File.separator + className + "PageReq.java";
        String restVoFileName = restVoDirPath + File.separator + className + "Vo.java";
        String restApiFileName = restApiDirPath + File.separator + className + "Api.java";

        Map<String, Object> datas = new HashMap<String, Object>();
        datas.put("modelPackageName", modelPackageName);
        datas.put("mapperPackageName", mapperPackageName);
        datas.put("servicePackageName", servicePackageName);
        datas.put("restReqPackageName", restReqPackageName);
        datas.put("restVoPackageName", restVoPackageName);
        datas.put("restPackageName", restPackageName);
        datas.put("tableName", tableName);
        datas.put("className", className);
        datas.put("apiName", StringUtil.camelToKebab(categoryName));
        datas.put("genApi", genApi);
        List<FieldInfo> fieldList = dbGenerationStrategy.getTableFieldInfo(tableName, datas);
        datas.put("fieldList", fieldList);

        //gen model main
        String modelStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, modelFtl);
        boolean modelRes = writeFile(modelStr, modelFileName, override);
        if (!modelRes && update) {//更新model
            updateModel(modelStr, modelFileName);
        }
        logger.info("createModel:{} res:{}", modelFileName, modelRes);

        //gen mapper main
        String mapperStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, mapperFtl);
        boolean mapperRes = writeFile(mapperStr, mapperFileName, override);
        logger.info("createMapper:{} res:{}", mapperFileName, mapperRes);
        if (!modelRes && update) {//更新 xml add addBatch update 内容
            updateMapper(mapperStr, mapperFileName);
            //去掉xml中 基础sql代码
//            updateMapperXml(mapperXmlFileName);
        }

        //gen mapperXml main
        buildFtlFile(datas, mapperXmlFtl, mapperXmlFileName, override);
        buildFtlFile(datas, serviceFtl, serviceFileName, override);
        if (genApi) {
            buildFtlFile(datas, addReqFtl, restAddReqFileName, override);
            buildFtlFile(datas, updateReqFtl, restUpdateReqFileName, override);
            buildFtlFile(datas, pageReqFtl, restPageReqFileName, override);
            buildFtlFile(datas, voFtl, restVoFileName, override);
            buildFtlFile(datas, apiFtl, restApiFileName, override);
        }

    }

    private boolean buildFtlFile(Map<String, Object> datas, String ftlName, String fileName, boolean override) {
        String str = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, ftlName);
        boolean res = writeFile(str, fileName, override);
        logger.info("create:{} res:{}", fileName, res);
        return res;
    }

    private void makDirs(String path) {
        File dir = new File(path);
        if (!dir.exists()) {
            logger.info("mkdir:{}", path);
            dir.mkdirs();
        }
    }

    private void updateMapperXml(String mapperXmlFileName) {
        String oldStr = FileUtil.readFileAsText(mapperXmlFileName);
        String addRegex = "(<insert id=\"add\"[\\s\\S]*?</insert>)";
        String addBatchRegex = "(<insert id=\"addBatch\"[\\s\\S]*?</insert>)";
        String updateRegex = "(<update id=\"update\"[\\s\\S]*?</update>)";
        String findByIdRegex = "(<select id=\"findById\"[\\s\\S]*?</select>)";
        String findByIdForUpdateRegex = "(<select id=\"findByIdForUpdate\"[\\s\\S]*?</select>)";
        String deleteByIdRegex = "(<delete id=\"deleteById\"[\\s\\S]*?</delete>)";
        String listAllRegex = "(<select id=\"listAll\"[\\s\\S]*?</select>)";
        //replace add
        oldStr = clearOldCode(oldStr, addRegex);
        //replace addBatch
        oldStr = clearOldCode(oldStr, addBatchRegex);
        //replace update
        oldStr = clearOldCode(oldStr, updateRegex);
        oldStr = clearOldCode(oldStr, findByIdRegex);
        oldStr = clearOldCode(oldStr, findByIdForUpdateRegex);
        oldStr = clearOldCode(oldStr, deleteByIdRegex);
        oldStr = clearOldCode(oldStr, listAllRegex);
        boolean res = writeFile(oldStr, mapperXmlFileName, true);
        logger.info("updateMapperXml:{} res:{}", mapperXmlFileName, res);
    }

    private String clearOldCode(String oldStr, String addRegex) {
        String oldAdd = findElement(oldStr, addRegex);
        if (oldAdd != null) {
            oldStr = oldStr.replace(oldAdd, "");
        }
        return oldStr;
    }

    private void updateMapper(String mapperStr, String mapperFileName) {
        String oldStr = FileUtil.readFileAsText(mapperFileName);

        String addReplace = "((\\s.*?)@Insert[^;]*?add\\([\\s\\S]*?\\);)";
        String addUpdate = "((\\s.*?)(int.*?)(add\\()(.*?)\\);)";
        String addBatchReplace = "((\\s.*?)@Insert[^;]*?addBatch\\([\\s\\S]*?\\);)";
        String addBatchUpdate = "((\\s.*?)(int.*?)(addBatch\\()(.*?)\\);)";
        String updateReplace = "((\\s.*?)@Update([^;]*?update\\()[\\s\\S]*?\\);)";
        String updateUpdate = "((\\s.*?)(int.*?)(update\\()(.*?)\\);)";
        String findByIdReplace = "((\\s.*?)@Select([^;]*?findById\\()[\\s\\S]*?\\);)";
        String findByIdUpdate = "((\\s.*?)(findById\\()(.*?)\\);)";
        String findByIdForUpdateReplace = "((\\s.*?)@Select([^;]*?findByIdForUpdate\\()[\\s\\S]*?\\);)";
        String findByIdForUpdateUpdate = "((\\s.*?)(findByIdForUpdate\\()(.*?)\\);)";
        String deleteByIdReplace = "((\\s.*?)@Delete([^;]*?deleteById\\()[\\s\\S]*?\\);)";
        String deleteByIdUpdate = "((\\s.*?)(deleteById\\()(.*?)\\);)";
        String listAllReplace = "((\\s.*?)@Select[^;]*?listAll\\([\\s\\S]*?\\);)";
        String listAllUpdate = "((\\s.*?)(listAll\\()(.*?)\\);)";

        String oldPackageName = "org.apache.ibatis.annotations.Param;";
        String newPackageName = "org.apache.ibatis.annotations.*;";

        StringBuilder newSb = new StringBuilder();

        //add method
        oldStr = replaceByRegex(mapperStr, oldStr, addReplace, addUpdate, newSb);
        //addBatch method
        oldStr = replaceByRegex(mapperStr, oldStr, addBatchReplace, addBatchUpdate, newSb);
        //update method
        oldStr = replaceByRegex(mapperStr, oldStr, updateReplace, updateUpdate, newSb);
        //findById method
        oldStr = replaceByRegex(mapperStr, oldStr, findByIdReplace, findByIdUpdate, newSb);
        //findByIdForUpdate method
        oldStr = replaceByRegex(mapperStr, oldStr, findByIdForUpdateReplace, findByIdForUpdateUpdate, newSb);
        //deleteById method
        oldStr = replaceByRegex(mapperStr, oldStr, deleteByIdReplace, deleteByIdUpdate, newSb);
        //listAll method
        oldStr = replaceByRegex(mapperStr, oldStr, listAllReplace, listAllUpdate, newSb);

        oldStr = oldStr.replace(oldPackageName, newPackageName);

        if (newSb.length() > 0) {//有新方法
            int idx = oldStr.indexOf("{");
            StringBuilder fileSb = new StringBuilder();
            fileSb.append(oldStr.substring(0, idx + 2));
            fileSb.append("\n\n\t").append(newSb);
            fileSb.append(oldStr.substring(idx + 2));
            oldStr = fileSb.toString();
        }

        boolean res = writeFile(oldStr, mapperFileName, true);
        logger.info("updateMapper:{} res:{}", mapperFileName, res);
    }

    private String replaceByRegex(String mapperStr, String oldStr, String addReplace, String addUpdate, StringBuilder newSb) {
        String addReplaceStrOld = findElement(oldStr, addReplace);
        String addReplaceStrNew = findElement(mapperStr, addReplace);
        if (addReplaceStrOld == null) {//原来没有生成
            //判断原来否有方法
            String addUpdateStrOld = findElement(oldStr, addUpdate);
            if (addUpdateStrOld == null) {
                newSb.append(addReplaceStrNew).append("\n");
            } else {//replace
                oldStr = oldStr.replace(addUpdateStrOld, addReplaceStrNew);
            }
        } else {
            oldStr = oldStr.replace(addReplaceStrOld, addReplaceStrNew);
        }
        return oldStr;
    }

    private String findElement(String content, String regex) {
        return findElement(content, regex, 1);
    }

    private String findElement(String content, String regex, int idx) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        while (m.find()) {
            if (idx <= m.groupCount()) {
                return m.group(idx);
            } else {
                logger.error("idx:{} > groupCount:{}", idx, m.groupCount());
            }
        }
        return null;
    }

    private List<String> findElements(String content, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        List<String> list = new ArrayList<String>();
        while (m.find()) {
            list.add(m.group());
        }
        return list;
    }

    private void updateModel(String modelStr, String modelFileName) {
        String oldStr = FileUtil.readFileAsText(modelFileName);
        String newStr = modelStr.substring(modelStr.indexOf("serialVersionUID"));
        String[] fieldArr = newStr.split(";");
        if (fieldArr.length < 1) {
            return;
        }
        StringBuilder newSb = new StringBuilder();
        for (int i = 1; i < fieldArr.length; i++) {
            String fieldStr = fieldArr[i];
            String[] arr = fieldStr.split("public ");
            if (arr.length != 2) {
                continue;
            }
            String filedDesc = "public " + arr[1];
            if (!oldStr.contains(filedDesc)) {
                newSb.append(fieldStr).append(";");
            }
        }
        if (newSb.length() > 0) {
            oldStr = oldStr.substring(0, oldStr.lastIndexOf('}'));
            oldStr = oldStr + newSb.toString() + "\n}";
            //save model
            boolean res = writeFile(oldStr, modelFileName, true);
            logger.info("updateModel:{} res:{}", modelFileName, res);
        }
    }

    private boolean writeXmlFile(Document doc, String mapperXmlFileName) {
        try {
            OutputFormat format = OutputFormat.createPrettyPrint();
            // 利用格式化类对编码进行设置
            format.setEncoding("UTF-8");
            FileOutputStream output = new FileOutputStream(new File(mapperXmlFileName));
            XMLWriter writer = new XMLWriter(output, format);
            writer.write(doc);
            writer.flush();
            writer.close();
            return true;
        } catch (Exception e) {
            logger.error("writeXmlFile error.", e);
            return false;
        }

    }

    private void updateOneElement(Element newRoot, Element oldRoot, String elementName, String attrName, String attrVal) {
        Element newAddElement = XmlUtil.find(newRoot, elementName, attrName, attrVal);
        Element oldAddElement = XmlUtil.find(oldRoot, elementName, attrName, attrVal);
        if (oldAddElement != null) {
            oldRoot.remove(oldAddElement);
            oldRoot.add(newAddElement.createCopy());
        } else {
            oldRoot.add(newAddElement.createCopy());
        }
    }

    public void genApiHtml(Class clazz) {
        check();
        List<ControllerInfo> controllerInfoList = new ArrayList<>();
        ControllerInfo controllerInfo = new ControllerInfo();
        String name = StringUtil.camelToKebab(clazz.getSimpleName());
        if (name.startsWith("-")) {
            name = name.substring(1);
        }
        name = "/" + name + "/";
        controllerInfo.name = name;
        controllerInfo.apiInfos = getApiInfoList(clazz);
        controllerInfoList.add(controllerInfo);
        genApiHtml(controllerInfoList);
    }

    public void genApiHtml() {
        check();
        Set<Class<?>> classSet = ClassUtil.loadClass(restPackageName, true);
        List<ControllerInfo> controllerInfoList = new ArrayList<>();
        for (Class clazz : classSet) {
            RestController restController = (RestController) clazz.getAnnotation(RestController.class);
            if (restController == null) {
                continue;
            }
            ControllerInfo controllerInfo = new ControllerInfo();
            String name = StringUtil.camelToKebab(clazz.getSimpleName());
            if (name.startsWith("-")) {
                name = name.substring(1);
            }
            name = "/" + name + "/";
            controllerInfo.name = name;
            controllerInfo.apiInfos = getApiInfoList(clazz);
            controllerInfoList.add(controllerInfo);
        }
        genApiHtml(controllerInfoList);
    }

    private void genApiHtml(List<ControllerInfo> controllerInfoList) {
        //gen file
        String apiHtml = "apiHtml.ftl";
        String apiHtmlPath = "";
        boolean isInSpringBoot = checkIsInSpringBoot();
        if (StringUtil.isEmpty(apiHtmlPath)) {
            StringBuilder sb = new StringBuilder();
            try {
                sb.append(new File("").getCanonicalPath()).append(File.separator);
            } catch (IOException e) {
                logger.error("", e);
            }
            sb.append("src").append(File.separator);
            sb.append("main").append(File.separator);
            if (isInSpringBoot) {
                sb.append("resources").append(File.separator);
            } else {
                sb.append("webapp").append(File.separator);
            }
            sb.append("static").append(File.separator);
            sb.append("api.html");
            apiHtmlPath = sb.toString();
        }
        makeDirs(apiHtmlPath.substring(0, apiHtmlPath.lastIndexOf(File.separator)));
        logger.info("apiHtmlPath:{}", apiHtmlPath);
        Map<String, Object> datas = new HashMap<String, Object>();
        datas.put("controllerInfoList", controllerInfoList);
        datas.put("controllerInfoJson", JsonUtil.toJsonString(controllerInfoList));

        //gen model main
        String modelStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, apiHtml);
        HtmlCompressor compressor = new HtmlCompressor();
        compressor.setCompressCss(true);
        compressor.setEnabled(true);
        compressor.setCompressCss(true);
        compressor.setYuiJsPreserveAllSemiColons(true);
        compressor.setYuiJsLineBreak(1);
        compressor.setPreserveLineBreaks(false);
        compressor.setRemoveIntertagSpaces(true);
        compressor.setRemoveComments(true);
        compressor.setRemoveMultiSpaces(true);
        modelStr = compressor.compress(modelStr);
        boolean modelRes = writeFile(modelStr, apiHtmlPath, true);
        logger.info("createApiHtml:{} res:{}", apiHtmlPath, modelRes);
    }

    private boolean checkIsInSpringBoot() {
        String className = "cn.ibaijia.jsm.spring.boot.autoconfigure.SpringBootVFS";
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private void makeDirs(String dirPath) {
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    private String getClazzUri(Class clazz) {
        RequestMapping baseRequestMapping = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
        if (baseRequestMapping != null && baseRequestMapping.value().length > 0) {
            return baseRequestMapping.value()[0];
        }
        PostMapping postMapping = (PostMapping) clazz.getAnnotation(PostMapping.class);
        if (postMapping != null && postMapping.value().length > 0) {
            return postMapping.value()[0];
        }
        PutMapping putMapping = (PutMapping) clazz.getAnnotation(PutMapping.class);
        if (putMapping != null && putMapping.value().length > 0) {
            return putMapping.value()[0];
        }
        GetMapping getMapping = (GetMapping) clazz.getAnnotation(GetMapping.class);
        if (getMapping != null && getMapping.value().length > 0) {
            return getMapping.value()[0];
        }
        DeleteMapping deleteMapping = (DeleteMapping) clazz.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null && deleteMapping.value().length > 0) {
            return deleteMapping.value()[0];
        }
        return "";
    }

    private String getMethodUri(Method method) {
        RequestMapping baseRequestMapping = method.getAnnotation(RequestMapping.class);
        if (baseRequestMapping != null && baseRequestMapping.value().length > 0) {
            return baseRequestMapping.value()[0];
        }
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        if (postMapping != null && postMapping.value().length > 0) {
            return postMapping.value()[0];
        }
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        if (putMapping != null && putMapping.value().length > 0) {
            return putMapping.value()[0];
        }
        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        if (getMapping != null && getMapping.value().length > 0) {
            return getMapping.value()[0];
        }
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null && deleteMapping.value().length > 0) {
            return deleteMapping.value()[0];
        }
        return "";
    }

    private String getHttpMethodName(Method method) {
        RequestMapping baseRequestMapping = method.getAnnotation(RequestMapping.class);
        if (baseRequestMapping != null) {
            return baseRequestMapping.method()[0].name().toLowerCase();
        }
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        if (postMapping != null) {
            return "post";
        }
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        if (putMapping != null) {
            return "put";
        }
        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        if (getMapping != null) {
            return "get";
        }
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null) {
            return "delete";
        }
        return "";
    }

    private List<ApiInfo> getApiInfoList(Class clazz) {
        String basePath = "";
        RestController restController = (RestController) clazz.getAnnotation(RestController.class);
        if (restController == null) {
            return null;
        }
        basePath = basePath + getClazzUri(clazz);
        List<ApiInfo> apiInfoList = new ArrayList<>();
        Method[] methods = clazz.getMethods();
        Arrays.sort(methods, new Comparator<Method>() {
            @Override
            public int compare(Method o1, Method o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (Method method : methods) {
            RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
            if (requestMapping == null || requestMapping.method().length == 0 || requestMapping.value().length == 0) {
                continue;
            }
            ApiInfo apiInfo = new ApiInfo();
            apiInfo.name = method.getName();
            apiInfo.comments = "";
            apiInfo.httpMethod = getHttpMethodName(method);
            apiInfo.url = basePath + getMethodUri(method);
            logger.info("getApiInfoList clazz:{}, methodName:{},url:{}", clazz.getSimpleName(), apiInfo.name, apiInfo.url);
            ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
            if (apiOperation != null) {
                apiInfo.comments = apiOperation.value() + " notes:" + apiOperation.notes();
            }
            for (Parameter parameter : method.getParameters()) {
                PathVariable pathVariable = parameter.getAnnotation(PathVariable.class);
                if (pathVariable != null) {
                    apiInfo.pathVarList = getSingleReqParamFields(parameter, pathVariable.value());
                    continue;
                }
                RequestParam requestParam = parameter.getAnnotation(RequestParam.class);
                if (requestParam != null) {
                    apiInfo.reqParamList = getSingleReqParamFields(parameter, requestParam.value());
                    continue;
                }
                RequestBody requestBody = parameter.getAnnotation(RequestBody.class);
                if (requestBody != null) {
                    apiInfo.bodyParamList = JsmFrameUtil.getFieldInfoList(parameter.getType(), null, null, 0, false);
                    continue;
                }
                //对象型的request param
                if (ValidateModel.class.isAssignableFrom(parameter.getType())) {
                    apiInfo.reqParamList = JsmFrameUtil.getFieldInfoList(parameter.getType(), JsmFrameUtil.getGenericNames(parameter.getParameterizedType()), null, 0, false);
                    if (Page.class.isAssignableFrom(parameter.getType())) {
                        for (FieldInfo fieldInfo : apiInfo.reqParamList) {
                            if (fieldInfo.fieldName.equals("list")) {
                                apiInfo.reqParamList.remove(fieldInfo);
                                break;
                            }
                        }
                    }
                    continue;
                }

            }
            //返回参数描述
            if (!method.getReturnType().getName().equals("void")) {
                apiInfo.respParamList = JsmFrameUtil.getFieldInfoList(method.getReturnType(), JsmFrameUtil.getGenericNames(method.getGenericReturnType()), null, 0, false);
            }
            apiInfoList.add(apiInfo);
        }
        return apiInfoList;
    }

    private List<FieldInfo> getSingleReqParamFields(Parameter parameter, String fieldName) {
        List<FieldInfo> reqParamList = new ArrayList<>();
        FieldInfo fieldInfo = new FieldInfo();
        fieldInfo.fieldName = parameter.getName();
        fieldInfo.fieldType = parameter.getType().getSimpleName();
        fieldInfo.required = false;
        FieldAnn fieldAnn = parameter.getAnnotation(FieldAnn.class);
        if (fieldAnn != null) {
            if (!StringUtil.isEmpty(fieldAnn.name())) {
                fieldInfo.fieldName = fieldAnn.name();
            }
            if (fieldAnn.maxLen() != -1) {
                fieldInfo.maxLength = fieldAnn.maxLen();
            }
            fieldInfo.required = fieldAnn.required();
            fieldInfo.comments = fieldAnn.comments();
        }
        if (!StringUtil.isEmpty(fieldName)) {
            fieldInfo.fieldName = fieldName;
        }
        reqParamList.add(fieldInfo);
        return reqParamList;
    }

    public void genFrontJsApi() {
        check();
        Set<Class<?>> classSet = ClassUtil.loadClass(restPackageName, false);
        List<ControllerInfo> controllerInfoList = new ArrayList<>();
        for (Class clazz : classSet) {
            ControllerInfo controllerInfo = new ControllerInfo();
            String name = clazz.getSimpleName();
            controllerInfo.name = name;
            controllerInfo.apiInfos = getApiInfoList(clazz);
            controllerInfoList.add(controllerInfo);
        }
        for (ControllerInfo controllerInfo : controllerInfoList) {
            genFrontJsApi(controllerInfo, true);
        }
    }

    public void genFrontJsApi(Class clazz, boolean override) {
        check();
        ControllerInfo controllerInfo = new ControllerInfo();
        String name = clazz.getSimpleName();
        controllerInfo.name = name;
        controllerInfo.apiInfos = getApiInfoList(clazz);

        genFrontJsApi(controllerInfo, true);
    }

    private void genFrontJsApi(ControllerInfo controllerInfo, boolean override) {

        //gen file
        String frontApi = "frontApi.ftl";
        String apiFileName = controllerInfo.name;
        String apiFilePath = frontApiDir + File.separator + apiFileName + ".js";

        Map<String, Object> datas = new HashMap<String, Object>();
        datas.put("methodInfoList", controllerInfo.apiInfos);

        String modelStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, frontApi);
        boolean modelRes = writeFile(modelStr, apiFilePath, override);
        logger.info("createModel:{} res:{}", apiFilePath, modelRes);
    }

    private boolean writeFile(String text, String filePath, boolean override) {

        File modelFile = new File(filePath);
        if (!override && modelFile.exists()) {
            logger.warn("file:{} exists, ignore! override:{}", filePath, override);
            return false;
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            fos.write(text.getBytes());
            return true;
        } catch (Exception e) {
            logger.error("", e);
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                logger.error("", e);
            }
        }
    }

}
