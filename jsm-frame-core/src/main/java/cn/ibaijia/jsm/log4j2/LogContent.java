package cn.ibaijia.jsm.log4j2;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.ThrowableProxy;

import java.util.Arrays;

/**
 * @author longzl
 */
public class LogContent {

    public String appName;
    public String env;
    public String gitHash;
    public String uid;
    public long time;
    public String traceId;
    public String level;
    public String thread;
    public String logName;
    public String methodName;
    public String logMsg;
    public String exMsg;
    public String exName;
    public String exTrace;

    public LogContent(LogEvent event, String traceId) {
        this.appName = AppContext.get("jsm.projectName", "unknownapp");
        this.env = AppContext.get("jsm.env", "unknownenv");
        this.gitHash = AppContext.get("git_hash", "unknownhash");
        this.time = event.getTimeMillis();
        this.logName = event.getLoggerName();
        this.traceId = traceId;
        this.level = event.getLevel().name();
        this.thread = event.getThreadName();
        this.uid = WebContext.currentUserId();
        if (event.getSource() != null) {
            this.methodName = event.getSource().getMethodName();
        }
        if (event.getMessage() != null) {
            this.logMsg = event.getMessage().getFormattedMessage();
        }
        ThrowableProxy thrownProxy = event.getThrownProxy();
        if (thrownProxy != null) {
            this.exMsg = thrownProxy.getMessage();
            this.exName = thrownProxy.getName();
            this.exTrace = parseException(thrownProxy.getStackTrace());
        }
    }


    private String parseException(StackTraceElement[] stackTrace) {
        StringBuffer sb = new StringBuffer();
        sb.append("\n");
        Arrays.stream(stackTrace).forEach((e) -> sb.append(e.getClassName()).append(".").append(e.getMethodName()).append("(").append(e.getFileName()).append(":").append(e.getLineNumber()).append(")").append("\n")
        );
        return sb.toString();
    }

}
