package cn.ibaijia.jsm.log4j2;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.JsonUtil;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author longzl
 */
@Plugin(name = "JsmAppender", category = "Core", elementType = "appender", printObject = true)
public class JsmAppender extends AbstractAppender {
    private static AtomicLong idAi = new AtomicLong(0);
    private static String clusterId;
    private static final int BATCH_SIZE = 500;
    private static LogStrategy logStrategy;


    protected JsmAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @Override
    public void append(LogEvent event) {
        String traceId = WebContext.getTraceId(event.getThreadName());
        if (traceId == null) {
            return;
        }
        if (logStrategy == null) {
            String logType = AppContext.get(AppContextKey.LOG_TYPE);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            logStrategy = getStrategyByType(logType);
            if (logStrategy == null) {
                System.out.println("logStrategy not found:" + AppContext.get(AppContextKey.LOG_TYPE));
                return;
            }
        }
        LogContent logContent = new LogContent(event, traceId);
        logStrategy.write(JsonUtil.toJsonString(logContent));
    }

    private LogStrategy getStrategyByType(String type) {
        if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(type)) {
            return SpringContext.getBean("consoleStrategy");
        } else {
            return SpringContext.getBean(type + "LogStrategy");
        }
    }

    /**
     * @param name
     * @param filter
     * @param layout
     * @param ignoreExceptions
     * @param address
     * @return
     */
    @PluginFactory
    public static JsmAppender createAppender(@PluginAttribute("name") String name,
                                             @PluginElement("Filter") final Filter filter,
                                             @PluginElement("Layout") Layout<? extends Serializable> layout,
                                             @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
                                             @PluginAttribute("address") String address
    ) {
        if (name == null) {
            name = "JsmAppender";
            LOGGER.warn("JsmAppender no name attribute,use default:" + name);
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new JsmAppender(name, filter, layout, ignoreExceptions);
    }

}
