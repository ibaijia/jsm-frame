package cn.ibaijia.jsm.log4j2;

import cn.ibaijia.jsm.context.WebContext;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;
import org.apache.logging.log4j.core.pattern.PatternConverter;

@Plugin(name = "JsmPatternConverter", category = PatternConverter.CATEGORY)
@ConverterKeys({"z"})
public class JsmPatternConverter extends LogEventPatternConverter {

    private static final JsmPatternConverter INSTANCE = new JsmPatternConverter();

    public static JsmPatternConverter newInstance(final String[] options) {
        return INSTANCE;
    }

    private JsmPatternConverter() {
        super("z", "z");
    }

    @Override
    public void format(LogEvent event, StringBuilder toAppendTo) {
        String traceId = WebContext.getTraceId(event.getThreadName());
        toAppendTo.append(traceId + "-" + WebContext.currentUserId());
    }
}
