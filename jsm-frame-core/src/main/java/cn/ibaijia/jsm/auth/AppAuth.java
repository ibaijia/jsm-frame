package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.exception.NoLoginException;
import cn.ibaijia.jsm.exception.NoPermissionException;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.utils.*;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AppAuth implements Auth {

    private Logger logger = LogUtil.log(AppAuth.class);

    @Override
    public String checkAuth(HttpServletRequest request, RestAnn resetAnn, boolean isTry) {
        String token = "";
        if (AppContext.isJedisSession()) {
            String at = RequestUtil.get(request, WebContext.JSM_AT);
            String ht = RequestUtil.get(request, WebContext.JSM_HT);
            logger.debug("checkSession, auth type {},at {}, ht {}", resetAnn.authType(), at, ht);
            token = decodeAppToken(at, ht, request);
            if (StringUtil.isEmpty(token)) {
                if (isTry) {
                    return token;
                } else {
                    throw new AuthFailException();
                }
            }
        }
        Session session = new Session(token);
        if (session.isExpire()) {
            if (isTry) {
                return token;
            } else {
                throw new NoLoginException();
            }
        } else {
            session.live();
            request.setAttribute(BasePairConstants.SESSION_ATTR_KEY, session);
        }
        //APP WEB 判断是否有系统权限
        if (!isTry && !StringUtil.isEmpty(resetAnn.permission())) {
            if (!WebUtil.hasPermissions(resetAnn.permission())) {
                throw new NoPermissionException();
            }
        }
        return token;
    }

    private String decodeAppToken(String at, String ht, HttpServletRequest request) {
        String token = null;
        try {
            if (!StringUtil.isEmpty(at) && !StringUtil.isEmpty(ht)) {
                String decAuth = BlowFishUtil.dec(at);
                if (decAuth == null || !decAuth.contains(",")) {
                    logger.error("illegal at: {}", at);
                    return null;
                }
                String[] arr = decAuth.split(",");
                token = arr[0];
                String timestamp = arr[1];
                Long cTime = Long.valueOf(timestamp);
                long sTime = DateUtil.currentTime();
                long res = Math.abs(sTime - cTime);
                int expireTime = AppContext.getAtExpireTime();
                if (res > expireTime) {
                    logger.error("sTime {} - cTime {} = {}", new Object[]{sTime, cTime, res});
                    return null;
                }

                String sAuth = BlowFishUtil.enc(token + "," + timestamp);
                if (!sAuth.equals(at)) {
                    logger.error("sAt {} != at {}", new Object[]{sAuth, at});
                    return null;
                }

                String sHash = EncryptUtil.md5(token + "," + timestamp);
                if (!sHash.equals(at)) {
                    logger.error("sHt {} != ht {}", new Object[]{sHash, at});
                    return null;
                }
                request.setAttribute("token", token);
            }
        } catch (Exception e) {
            logger.error("decodeAppToken error!", e);
            return null;
        }
        return token;
    }

}
