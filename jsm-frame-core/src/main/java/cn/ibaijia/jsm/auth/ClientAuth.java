package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.jsm.oauth.client.OauthContext;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.RequestUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author longzl
 */
@Component
public class ClientAuth implements Auth {

    private Logger logger = LogUtil.log(ClientAuth.class);

    @Resource
    private CacheService cacheService;

    private HttpClient httpClient = new HttpClient();

    private String tokenName;

    private String verifyUrl;

    private String getTokenName() {
        if (this.tokenName == null) {
            tokenName = AppContext.get(AppContextKey.OAUTH_CLIENT_TOKEN_NAME);
        }
        return tokenName;
    }

    private String getVerifyUrl() {
        if (this.verifyUrl == null) {
            verifyUrl = AppContext.get(AppContextKey.OAUTH_CLIENT_VERIFY_URL);
        }
        return verifyUrl;
    }

    @Override
    public String checkAuth(HttpServletRequest request, RestAnn resetAnn, boolean isTry) {
        String token = RequestUtil.get(request, getTokenName());
        String resourceCode = resetAnn.permission();
        boolean res = verifyOauthToken(token, resourceCode);
        if (!res) {
            throw new AuthFailException();
        }
        return token;
    }

    /**
     * 先取缓存 如果没有 再访问URL
     *
     * @param token
     * @param resourceCode
     * @return
     */
    private boolean verifyOauthToken(String token, String resourceCode) {
        try {
            String cacheKey = token + "_" + resourceCode;
            RestResp<String> verifyTokenResp = cacheService.get(cacheKey, new TypeReference<RestResp<String>>() {
            });
            if (verifyTokenResp == null) {
                verifyTokenResp = getVerifyOauthTokenResult(token, resourceCode);
                if (verifyTokenResp == null || !verifyTokenResp.isOK()) {
                    return false;
                }
                Integer expireTime = AppContext.getAsInteger(AppContextKey.OAUTH_CLIENT_EXPIRE_IN, 5);
                cacheService.set(cacheKey, expireTime, verifyTokenResp);
            }
            OauthContext.setAccessToken(token);
            logger.debug("setVerifyResult:{}", verifyTokenResp.result);
            OauthContext.setVerifyResult(verifyTokenResp.result);
            return verifyTokenResp.isOK();
        } catch (Exception e) {
            logger.error("verifyOauthToken error!", e);
            return false;
        }
    }

    /**
     * 再访问URL 验证Token
     *
     * @param accessToken
     * @param resourceCode
     * @return
     */
    private RestResp<String> getVerifyOauthTokenResult(String accessToken, String resourceCode) {
        try {
            if (StringUtil.isEmpty(getVerifyUrl())) {
                logger.error("oauth.client.verifyUrl not found, in AppContext!");
                return null;
            }
            List<Header> headerList = new ArrayList<Header>();
            headerList.add(new BasicHeader(getTokenName(), accessToken));
            String res = httpClient.get(headerList, getVerifyUrl());
            logger.debug("res:{}", res);
            if (StringUtil.isEmpty(res)) {
                logger.error("get verify token info empty error.");
                return null;
            } else {
                RestResp<String> resp = JsonUtil.parseObject(res, new TypeReference<RestResp<String>>() {
                });
                return resp;
            }
        } catch (Exception e) {
            logger.error("getVerifyOauthTokenResult error:" + getVerifyUrl(), e);
            return null;
        }
    }

}
