package cn.ibaijia.jsm.excel;

public enum ExcelFieldType {
	STRING(0,"string"), NUMBER(1,"number"), DATE(2,"date"), FLOAT(3,"float"),DATETTIME(4,"datetime");
	private int _v;
	private String _t;
	ExcelFieldType(int value,String t){
		_v = value;
		_t = t;
	}
	public int v(){
		return _v;
	}
	public static String findText(int v){
		ExcelFieldType[] values = ExcelFieldType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._v == v){
				return values[i]._t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		ExcelFieldType[] values = ExcelFieldType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._t.equals(t)){
				return values[i]._v;
			}
		}
		return 0;
	}
}
