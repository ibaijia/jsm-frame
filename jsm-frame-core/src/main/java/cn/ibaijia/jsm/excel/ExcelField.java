package cn.ibaijia.jsm.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.poi.ss.usermodel.CellType;
 
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE})
public @interface ExcelField {

    String name() default "";

    /**
     * inputCase1|replace1;inputCase2|replace2 是|1|;否|2;default|
     */
    String replaceRule() default "";

    /**
     * if date use: yyyy-MM-dd
     * if numeric use: #,#0.0
     */
    String format() default "";

    CellType cellType() default CellType.STRING;

    short cellStyle() default 0;

    boolean ignore() default false;

    int width() default -1;

    /**
     * -1 means not control
     */
    int height() default 14;

    TrimType trimType() default TrimType.NONE;

}