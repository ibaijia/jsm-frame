package cn.ibaijia.jsm.excel;

public enum TrimType {
	NONE(0,"none"), SPACE(1,"space"), CRLF(2,"crlf"), TAB(3,"tab"),ALL(4,"all");
	private int _v;
	private String _t;
	TrimType(int value, String t){
		_v = value;
		_t = t;
	}
	public int v(){
		return _v;
	}
	public static String findText(int v){
		TrimType[] values = TrimType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._v == v){
				return values[i]._t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		TrimType[] values = TrimType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._t.equals(t)){
				return values[i]._v;
			}
		}
		return 0;
	}
}
