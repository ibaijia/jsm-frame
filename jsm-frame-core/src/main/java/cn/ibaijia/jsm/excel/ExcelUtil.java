package cn.ibaijia.jsm.excel;

import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.utils.*;
import cn.ibaijia.jsm.utils.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

public class ExcelUtil {
    private static Logger logger = LogUtil.log(ExcelUtil.class);
    public static final String OFFICE_EXCEL_XLS = ".xls";
    public static final String OFFICE_EXCEL_XLSX = ".xlsx";
    public static final String DEFAULT_KEY = "default";

    private static String errorMessage = "第%s行，第%s列[%s]";

    public static Workbook getWorkbook(String templatePath) {
        try {
            return getWorkbook(new FileInputStream(templatePath));
        } catch (FileNotFoundException e) {
            logger.error("getWorkbook error.templatePath:" + templatePath, e);
            return null;
        }
    }

    public static boolean saveWorkbook(Workbook workbook, String path) {
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(path);
            workbook.write(outputStream);
            return true;
        } catch (IOException e) {
            logger.error("saveWorkbook error.path:" + path, e);
            return false;
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    logger.error("close workbook error.", e);
                } finally {
                    workbook = null;
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.error("close outputStream error.", e);
                } finally {
                    outputStream = null;
                }
            }
        }
    }

    public static Workbook getWorkbook(InputStream templateIs) {
        Workbook workBook = null;
        try {
            workBook = new XSSFWorkbook(templateIs);
            return workBook;
        } catch (Exception e) {
            logger.error("getWorkbook error!", e);
            return null;
        } finally {
            if (templateIs != null) {
                try {
                    templateIs.close();
                } catch (IOException e) {
                    logger.error("getWorkbook close templateIs error!", e);
                }
            }
        }
    }

    public static void setValue(Workbook workbook, String sheetName, int rowIdx, int cellIdx, String value) {
        Sheet sheet = workbook.getSheet(sheetName);
        if (sheet == null) {
            logger.error("setValue failed, sheetName {} not exists. ", sheetName);
        }
        setValue(sheet, rowIdx, cellIdx, value);

    }

    public static void setValue(Workbook workbook, int sheetIdx, int rowIdx, int cellIdx, String value) {
        Sheet sheet = workbook.getSheetAt(sheetIdx);
        if (sheet == null) {
            logger.error("setValue failed, sheetIdx {} not exists. ", sheetIdx);
        }
        setValue(sheet, rowIdx, cellIdx, value);

    }

    public static void setValue(Sheet sheet, int rowIdx, int cellIdx, String value) {
        Row row = sheet.getRow(rowIdx);
        if (row == null) {
//            row = sheet.createRow(rowIdx);
            logger.error("setValue failed, rowIdx {} not exists. ", rowIdx);
        }
        Cell cell = row.getCell(cellIdx);
        if (cell == null) {
//            cell = row.createCell(cellIdx);
            logger.error("setValue failed, cellIdx {} not exists. ", cellIdx);
        }
        CellStyle cellstyle = sheet.getWorkbook().createCellStyle();
        cellstyle.setWrapText(true);
        cell.setCellValue(value);
    }

    public static <T> Workbook writeToTemplate(InputStream templateIs, int sheetIdx, int beginRow, List<T> dataList) {
        Workbook workBook = null;
        try {
            workBook = new SXSSFWorkbook(new XSSFWorkbook(templateIs));
            return writeWorkbook(workBook, sheetIdx, beginRow, dataList);
        } catch (Exception e) {
            logger.error("writeToTemplate error!", e);
            return null;
        } finally {
            if (templateIs != null) {
                try {
                    templateIs.close();
                } catch (IOException e) {
                    logger.error("writeToTemplate close templateIs error!", e);
                }
            }
        }
    }

    public static <T> Workbook writeToNew(List<T> dataList) {
        Workbook workBook = null;
        try {
            workBook = new SXSSFWorkbook(new XSSFWorkbook());
            if (dataList == null || dataList.isEmpty()) {
                logger.warn("empty dataList!");
                return workBook;
            }
            Sheet sheet = workBook.createSheet();
            Row row = sheet.createRow(0);
            Field[] fieldList = dataList.get(0).getClass().getFields();
            int i = 0;
            Font font = workBook.createFont();
            font.setBold(true);
            CellStyle style = workBook.createCellStyle();
            style.setFont(font);
            for (Field field : fieldList) {
                Cell cell = row.createCell(i);
                cell.setCellStyle(style);
                ExcelField excelField = field.getAnnotation(ExcelField.class);
                if (excelField != null && excelField.ignore()) {
                    continue;
                }
                if (excelField == null || StringUtil.isEmpty(excelField.name())) {
                    cell.setCellValue(field.getName());
                } else {
                    cell.setCellValue(excelField.name());
                }
                if (excelField != null && excelField.width() > 0) {
                    sheet.setColumnWidth(i, excelField.width() * 256);
                }
                i++;
            }
            return writeWorkbook(workBook, 0, 1, dataList);
        } catch (Exception e) {
            logger.error("writeToNew error!", e);
            return null;
        }
    }

    private static <T> Workbook writeWorkbook(Workbook workBook, int sheetIdx, int beginRow, List<T> dataList) {
        Sheet sheet = workBook.getSheetAt(sheetIdx);
        Map<String, Integer> fieldMap = new HashMap<String, Integer>();
        Map<String, Map<String, String>> replaceMap = new HashMap<String, Map<String, String>>();
        if (beginRow == 1) {// set fieldIdx
            Row row = sheet.getRow(0);
            for (int i = 0; i < row.getLastCellNum(); i++) {
                fieldMap.put(row.getCell(i).getStringCellValue(), i);
            }
        }
        CellStyle cellstyle = workBook.createCellStyle();
        cellstyle.setWrapText(true);
        CellStyle numericCellstyle = workBook.createCellStyle();
        for (T data : dataList) {
            Row row = sheet.createRow(beginRow++);
            Field[] fieldList = data.getClass().getFields();
            int j = 0;
            for (Field field : fieldList) {

                ExcelField excelField = field.getAnnotation(ExcelField.class);
                if (excelField == null) {
                    excelField = ExcelUtil.class.getAnnotation(ExcelField.class);
                }
                if (excelField != null && excelField.ignore()) {
                    continue;
                }
                Integer idx = fieldMap.get(field.getName());
                if (excelField != null && !StringUtil.isEmpty(excelField.name())) {
                    idx = fieldMap.get(excelField.name());
                }
                if (idx == null) {
                    idx = j;
                }
                Cell cell = row.createCell(idx);
                Object val = BeanUtil.getFieldValue(field, data);
                if (val == null) {
                    continue;
                }
                if (val instanceof Date) {
                    if (StringUtil.isEmpty(excelField.format())) {
                        cell.setCellValue(DateUtil.format((Date) val, DateUtil.DATE_PATTERN));
                    } else {
                        cell.setCellValue(DateUtil.format((Date) val, excelField.format()));
                    }
                } else {
                    cell.setCellType(excelField.cellType());
                    logger.debug("excelField.name:{} excelField:cellType:{},excelField.format:{}", excelField.name(), excelField.cellType(), excelField.format());
                    if (excelField.cellType().equals(CellType.NUMERIC)) {
                        logger.debug("numeric val:{}", val);
                        if (val instanceof Float) {
                            Float fVal = (Float) val;
                            if (!StringUtil.isEmpty(excelField.format())) {
                                numericCellstyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(excelField.format()));
                            }
                            cell.setCellValue(StringUtil.toDouble(StringUtil.toString(fVal)));
                            cell.setCellStyle(numericCellstyle);
                        } else if (val instanceof Double) {
                            Double dVal = (Double) val;
                            if (!StringUtil.isEmpty(excelField.format())) {
                                numericCellstyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(excelField.format()));
                            }
                            cell.setCellValue(dVal);
                            cell.setCellStyle(numericCellstyle);
                        } else if (val instanceof Integer) {
                            cell.setCellValue((Integer) val);
                        } else if (val instanceof Long) {
                            cell.setCellValue((Long) val);
                        } else if (val instanceof Short) {
                            cell.setCellValue((Short) val);
                        } else if (val instanceof Byte) {
                            cell.setCellValue((Byte) val);
                        } else {
                            logger.error("unknown numeric type:{}", val.getClass().getName());
                        }
                    } else {
                        String valStr = val.toString();
                        if (excelField.trimType().equals(TrimType.SPACE)) {
                            valStr = StringUtil.trim(valStr);
                        }
                        if (excelField.trimType().equals(TrimType.CRLF)) {
                            valStr = StringUtil.trimCRLF(valStr);
                        }
                        if (excelField.trimType().equals(TrimType.TAB)) {
                            valStr = StringUtil.trimTab(valStr);
                        }
                        if (excelField.trimType().equals(TrimType.ALL)) {
                            valStr = StringUtil.trimAll(valStr);
                        }
                        if (!StringUtil.isEmpty(excelField.replaceRule()) && excelField.replaceRule().contains("|")) {
                            valStr = findReplaceValReverse(replaceMap, excelField.replaceRule(), field.getName(), valStr);
                        }
                        cell.setCellValue(valStr);
                        cell.setCellStyle(cellstyle);
                    }
                }
                if (excelField.height() > 0) {
                    row.setHeightInPoints(excelField.height());
                }
                j++;
            }
        }
        return workBook;
    }

    public static <T> void readExcel(MultipartFile multipartFile, int sheetIdx, Class<T> clazz, int beginRow, Page<T> page) {
        if (multipartFile.isEmpty()) {
            return;
        }
        String excelFile = multipartFile.getOriginalFilename().toLowerCase();
        InputStream fis = null;
        Workbook workBook = null;
        try {
            fis = multipartFile.getInputStream();
            if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLS)) {
                workBook = new HSSFWorkbook(fis);
            } else if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLSX)) {
                workBook = new XSSFWorkbook(fis);
            } else {
                logger.error("not excel file can't read.");
                return;
            }
            List<T> list = readWorkbook(workBook, sheetIdx, clazz, beginRow, page);
            page.setList(list);
        } catch (Exception e) {
            logger.error("readExcel error! file:" + excelFile, e);
            ExceptionUtil.failed(e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("readXlsx close fis error!", e);
                }
            }
        }
    }

    public static <T> List<T> readExcel(MultipartFile multipartFile, int sheetIdx, Class<T> clazz, int beginRow) {
        if (multipartFile.isEmpty()) {
            return null;
        }
        String excelFile = multipartFile.getOriginalFilename().toLowerCase();
        InputStream fis = null;
        Workbook workBook = null;
        try {
            fis = multipartFile.getInputStream();
            if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLS)) {
                workBook = new HSSFWorkbook(fis);
            } else if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLSX)) {
                workBook = new XSSFWorkbook(fis);
            } else {
                logger.error("not excel file can't read.");
                return null;
            }
            return readWorkbook(workBook, sheetIdx, clazz, beginRow, null);
        } catch (Exception e) {
            logger.error("readExcel error! file:" + excelFile, e);
            ExceptionUtil.failed(e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("readXlsx close fis error!", e);
                }
            }
        }
        return null;
    }

    public static <T> List<T> readExcel(String excelFile, int sheetIdx, Class<T> clazz, int beginRow) {
        File file = new File(excelFile);
        if (!file.exists()) {
            return null;
        }
        FileInputStream fis = null;
        Workbook workBook = null;
        try {
            fis = new FileInputStream(file);
            if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLS)) {
                workBook = new HSSFWorkbook(fis);
            } else if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLSX)) {
                workBook = new XSSFWorkbook(fis);
            } else {
                logger.error("not excel file can't read.");
                return null;
            }
            return readWorkbook(workBook, sheetIdx, clazz, beginRow, null);
        } catch (Exception e) {
            logger.error("readExcel error! file:" + excelFile, e);
            ExceptionUtil.failed(e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("readExcel close fis error!", e);
                }
            }
        }
        return null;
    }

    public static <T> Page<T> readExcel(String excelFile, int sheetIdx, Class<T> clazz, int beginRow, Page<T> page) {
        File file = new File(excelFile);
        if (!file.exists()) {
            return null;
        }
        FileInputStream fis = null;
        Workbook workBook = null;
        try {
            fis = new FileInputStream(file);
            if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLS)) {
                workBook = new HSSFWorkbook(fis);
            } else if (excelFile.toLowerCase().endsWith(OFFICE_EXCEL_XLSX)) {
                workBook = new XSSFWorkbook(fis);
            } else {
                logger.error("not excel file can't read.");
                return null;
            }
            List<T> list = readWorkbook(workBook, sheetIdx, clazz, beginRow, page);
            page.setList(list);
            return page;
        } catch (Exception e) {
            logger.error("readExcel error! file:" + excelFile, e);
            ExceptionUtil.failed(e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("readExcel close fis error!", e);
                }
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T> List<T> readWorkbook(Workbook workBook, int sheetIdx, Class<T> clazz, int beginRow, Page<T> page) {
        List<T> resultList = new ArrayList<T>();
        Map<String, Map<String, String>> replaceMap = new HashMap<String, Map<String, String>>();
        try {
            Sheet sheet = workBook.getSheetAt(sheetIdx);
            logger.info("read sheetIdx:{},row number:{}", sheetIdx, sheet.getLastRowNum());
            Map<String, Integer> fieldMap = new HashMap<String, Integer>();
            if (beginRow == 1) {// set fieldIdx
                Row row = sheet.getRow(0);
                for (int i = 0; i < row.getLastCellNum(); i++) {
                    fieldMap.put(row.getCell(i).getStringCellValue(), i);
                }
            }
            int totalRow = sheet.getLastRowNum() + 1;
            int startIdx = beginRow;
            int endIdx = totalRow;
            if (page != null) {
                page.setTotalCount(totalRow - beginRow);
                startIdx = beginRow + page.getBeginIndex();
                endIdx = startIdx + page.getPageSize();
//                if (page.getPageNo() > 1) {
//                    startIdx = startIdx + 1;
//                }
                if (endIdx > totalRow) {
                    endIdx = totalRow;
                }
                if (startIdx > totalRow) {
                    return null;
                }
            }
            Map<Integer, String> formatMap = new HashMap<>();

            for (int i = startIdx; i < endIdx; i++) {
                Object obj = clazz.newInstance();
                Field[] orgFields = obj.getClass().getFields();
                if (i == startIdx) {//兼容没有表头的情况，与字段顺序一一对应
                    int idx = 0;
                    for (Field field : orgFields) {
                        fieldMap.put(field.getName(), idx++);
                    }
                    for (Field field : orgFields) {
                        String excelFieldName = null;
                        ExcelField excelField = field.getAnnotation(ExcelField.class);
                        if (excelField != null) {
                            if(excelField.ignore()){
                                continue;
                            }
                            excelFieldName = excelField.name();
                        }
                        if (StringUtil.isEmpty(excelFieldName)) {
                            excelFieldName = field.getName();
                        }
                        Integer valIdx = fieldMap.get(excelFieldName);
                        formatMap.put(valIdx, excelField.format());
                    }
                }
                List<String> strList = readRowAsList(sheet.getRow(i), 0, formatMap);
                if (isEmptyRow(strList)) {
                    break;
                }
                for (Field field : orgFields) {
                    String excelFieldName = null;
                    ExcelField excelField = field.getAnnotation(ExcelField.class);
                    if (excelField != null) {
                        if(excelField.ignore()){
                            continue;
                        }
                        excelFieldName = excelField.name();
                    }
                    if (StringUtil.isEmpty(excelFieldName)) {
                        excelFieldName = field.getName();
                    }
                    Integer valIdx = fieldMap.get(excelFieldName);
                    if (valIdx == null || valIdx >= strList.size()) {
                        continue;
                    }
                    String val = strList.get(valIdx);
                    if (excelField != null) {
                        if (!StringUtil.isEmpty(excelField.replaceRule()) && excelField.replaceRule().contains("|")) {
                            val = findReplaceVal(replaceMap, excelField.replaceRule(), excelFieldName, val);
                        }
                    }
                    if (!StringUtil.isEmpty(val)) {
                        BeanUtil.setFieldValue(obj, field, val);
                    }
                }
                String errorMsg = ValidateUtil.validate(obj);
                if (!StringUtil.isEmpty(errorMsg)) {
                    String[] errorArr = errorMsg.split(",");
                    int fieldIdx = fieldMap.get(errorArr[0]);
                    String msg = String.format(errorMessage, i + 1, fieldIdx + 1, errorMsg);
                    ExceptionUtil.failed(msg);
                }
                resultList.add((T) obj);
            }

        } catch (Exception e) {
            logger.error("readWorkbook error!", e);
            ExceptionUtil.failed(e.getMessage());
        } finally {
            if (workBook != null) {
                try {
                    workBook.close();
                } catch (IOException e) {
                    logger.error("readWorkbook close workBook error!", e);
                }
            }
        }
        return resultList;
    }

    private static String findReplaceVal(Map<String, Map<String, String>> replaceMap, String replaceRule,
                                         String excelFieldName, String val) {
        if (!replaceMap.containsKey(excelFieldName)) {
            Map<String, String> map = new HashMap<String, String>();
            String[] ruleArr = replaceRule.split(";");
            for (String rule : ruleArr) {
                String[] arr = rule.split("\\|");
                if (arr.length > 1) {
                    map.put(arr[0], arr[1]);
                } else {
                    map.put(arr[0], null);
                }
            }
            replaceMap.put(excelFieldName, map);
        }
        String replaceVal = replaceMap.get(excelFieldName).get(val);
        //如果是空  判断是否有设置 DEFAULT_KEY,如果为null 并且有default key就使用default value
        if (replaceVal == null && replaceMap.get(excelFieldName).containsKey(DEFAULT_KEY)) {
            replaceVal = replaceMap.get(excelFieldName).get(DEFAULT_KEY);
        }
        return replaceVal == null ? val : replaceVal;
    }

    private static String findReplaceValReverse(Map<String, Map<String, String>> replaceMap, String replaceRule,
                                                String excelFieldName, String val) {
        if (!replaceMap.containsKey(excelFieldName)) {
            Map<String, String> map = new HashMap<String, String>();
            String[] ruleArr = replaceRule.split(";");
            for (String rule : ruleArr) {
                String[] arr = rule.split("\\|");
                if (arr.length > 1) {
                    map.put(arr[1], arr[0]);
                }
            }
            replaceMap.put(excelFieldName, map);
        }
        String replaceVal = replaceMap.get(excelFieldName).get(val);
        return replaceVal == null ? val : replaceVal;
    }

    private static boolean isEmptyRow(List<String> strList) {
        if(strList == null){
            return true;
        }
        for (String str : strList) {
            if (!StringUtil.isEmpty(str)) {
                return false;
            }
        }
        return true;
    }

    public static List<String> readRowAsList(Row row, int beginIdx, Map<Integer, String> formatMap) {
        if(row == null){
            return null;
        }
        List<String> list = new ArrayList<>();
        for (int i = beginIdx; i < row.getLastCellNum(); i++) {
            list.add(getCellValue(row.getCell(i), formatMap.get(i)));
        }
        return list;
    }

    @SuppressWarnings("deprecation")
    public static String getCellValue(Cell cell, String pattern) {
        if (cell == null)
            return "";
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            return cell.getStringCellValue();
        } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
            return String.valueOf(cell.getBooleanCellValue());
//        } else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA ) {
//            return cell.getCellFormula().;
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
            if (StringUtil.isEmpty(pattern)) {
                pattern = "0";
            }
            Double value = null;
            try {
                value = cell.getNumericCellValue();
            } catch (Exception e) {
                logger.error("", e);
            }
            return NumberUtil.formatString(value, pattern);
        }
        return cell.toString();
    }

    /**
     * 合并单元格处理,获取合并行
     *
     * @param sheet
     * @return List<CellRangeAddress>
     */
    public static List<CellRangeAddress> getCombineCell(Sheet sheet) {
        List<CellRangeAddress> list = new ArrayList<CellRangeAddress>();
        // 获得一个 sheet 中合并单元格的数量
        int sheetmergerCount = sheet.getNumMergedRegions();
        // 遍历所有的合并单元格
        for (int i = 0; i < sheetmergerCount; i++) {
            // 获得合并单元格保存进list中
            CellRangeAddress ca = sheet.getMergedRegion(i);
            list.add(ca);
        }
        return list;
    }

    /**
     * 判断单元格是否为合并单元格，是的话则将单元格的值返回
     *
     * @param listCombineCell 存放合并单元格的list
     * @param cell            需要判断的单元格
     * @param sheet           sheet
     * @return String
     */
    public static String isCombineCell(List<CellRangeAddress> listCombineCell, Cell cell, Sheet sheet) {
        int firstC = 0;
        int lastC = 0;
        int firstR = 0;
        int lastR = 0;
        String cellValue = null;
        for (CellRangeAddress ca : listCombineCell) {
            // 获得合并单元格的起始行, 结束行, 起始列, 结束列
            firstC = ca.getFirstColumn();
            lastC = ca.getLastColumn();
            firstR = ca.getFirstRow();
            lastR = ca.getLastRow();
            if (cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR) {
                if (cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC) {
                    Row fRow = sheet.getRow(firstR);
                    Cell fCell = fRow.getCell(firstC);
                    cellValue = getCellValue(fCell, null);
                    break;
                }
            } else {
                cellValue = "";
            }
        }
        return cellValue;
    }

    /**
     * 获取合并单元格的值
     *
     * @param sheet
     * @param row
     * @param column
     * @return String
     */
    public static String getMergedRegionValue(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();

        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();

            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    Row fRow = sheet.getRow(firstRow);
                    Cell fCell = fRow.getCell(firstColumn);
                    return getCellValue(fCell, null);
                }
            }
        }

        return null;
    }

    /**
     * 判断指定的单元格是否是合并单元格
     *
     * @param sheet
     * @param row    行下标
     * @param column 列下标
     * @return String
     */
    public static boolean isMergedRegion(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    return true;
                }
            }
        }
        return false;
    }

}

