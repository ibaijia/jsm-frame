package cn.ibaijia.jsm.cache.jedis;

public interface JedisCmdCallback<T> extends JedisCmd<T>, JedisCmdThrowable {
}
