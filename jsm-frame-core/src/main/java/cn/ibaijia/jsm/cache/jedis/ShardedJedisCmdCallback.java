package cn.ibaijia.jsm.cache.jedis;

public interface ShardedJedisCmdCallback<T> extends ShardedJedisCmd<T>, JedisCmdThrowable {
}
