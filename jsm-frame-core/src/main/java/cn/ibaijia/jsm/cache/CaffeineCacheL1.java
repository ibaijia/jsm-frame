package cn.ibaijia.jsm.cache;

import cn.ibaijia.jsm.utils.LogUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 10:55
 */
@Component
public class CaffeineCacheL1 implements CacheL1 {

    private Logger logger = LogUtil.log(this.getClass());

    Cache<String, Object> cache;

    public CaffeineCacheL1() {
        init();
    }

    private void init() {
        cache = Caffeine.newBuilder()
                .expireAfterWrite(10, TimeUnit.SECONDS)
                .initialCapacity(500)
                .maximumSize(2000)
                .build();
    }

    @Override
    public boolean put(String key, Object value) {
        try {
            cache.put(key, value);
            return true;
        } catch (Exception e) {
            logger.error("CaffeineCacheL1 put error. key:" + key, e);
            return false;
        }
    }

    @Override
    public <T> T get(String key) {
        try {
            return (T) cache.getIfPresent(key);
        } catch (Exception e) {
            logger.error("CaffeineCacheL1 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public boolean remove(String key) {
        try {
            cache.invalidate(key);
            return true;
        } catch (Exception e) {
            logger.error("CaffeineCacheL1 remove error. key:" + key, e);
            return false;
        }
    }
}
