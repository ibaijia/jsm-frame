package cn.ibaijia.jsm.cache.jedis;

import redis.clients.jedis.ShardedJedis;

public interface ShardedJedisCmd<T> {
   T run(ShardedJedis shardedJedis) throws Exception;
}
