package cn.ibaijia.jsm.cache.jedis;

public interface JedisCmdThrowable {
    void throwable(Throwable throwable);
}
