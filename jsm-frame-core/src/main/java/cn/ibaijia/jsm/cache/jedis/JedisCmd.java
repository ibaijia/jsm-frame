package cn.ibaijia.jsm.cache.jedis;

import redis.clients.jedis.Jedis;

public interface JedisCmd<T> {
   T run(Jedis jedis) throws Exception;
}
