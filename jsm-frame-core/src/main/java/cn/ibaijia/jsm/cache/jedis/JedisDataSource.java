package cn.ibaijia.jsm.cache.jedis;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import javax.annotation.Resource;

/**
 * @author longzl
 */
@Repository
public class JedisDataSource {
    private Logger logger = LoggerFactory.getLogger(JedisDataSource.class);

    @Resource
    private ShardedJedisPool shardedJedisPool;

    @Resource
    private JedisPool jedisPool;

    public ShardedJedis getSharedJedis() {
        ShardedJedis shardJedis = null;
        try {
            shardJedis = shardedJedisPool.getResource();
            return shardJedis;
        } catch (Exception e) {
            logger.error("getSharedJedis error:", e);
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JEDIS_SERVICE, "getSharedJedis error." + e.getMessage()));
            if (null != shardJedis) {
                shardJedis.close();
            }
        }
        return null;
    }

    public Jedis getJedis() {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis;
        } catch (Exception e) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JEDIS_SERVICE, "getJedis error." + e.getMessage()));
            logger.error("getSharedJedis error:", e);
            if (null != jedis) {
                jedis.close();
            }
        }
        return null;
    }

    public void returnResource(ShardedJedis shardedJedis) {
        shardedJedis.close();
    }

    public void returnResource(ShardedJedis shardedJedis, boolean broken) {
        shardedJedis.close();
    }

    public void returnResource(Jedis jedis) {
        jedis.close();
    }

    public void returnResource(Jedis jedis, boolean broken) {
        jedis.close();
    }

}