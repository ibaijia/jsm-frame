package cn.ibaijia.jsm.cache;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 10:48
 */
public interface CacheL1 {

    boolean put(String key, Object Object);

    <T> T  get(String key);

    boolean remove(String key);

}
