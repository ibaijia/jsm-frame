package cn.ibaijia.jsm.cache.jedis;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import com.mchange.v2.ser.SerializableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;

import javax.annotation.Resource;
import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Repository
public class JedisService {
    private Logger logger = LoggerFactory.getLogger(JedisService.class);

    @Resource
    private JedisDataSource redisDataSource;

    /**
     * 带有效期的设值（原子性）
     *
     * @param key
     * @param value
     */
    public String setex(String key, int seconds, String value) {
        String result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.setex(key, seconds, value);
            logger.debug("expireResult:" + result);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("setex", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 不存在,则设置
     *
     * @param key
     * @param seconds
     * @param value
     */
    public String setnx(String key, int seconds, String value) {
        String result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.set(key, value, "NX", "EX", seconds);
        } catch (Exception e) {
            logger.error("setnx error.", e);
            broken = true;
            addAlarm("setnx", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 不存在,则设置
     *
     * @param key
     * @param value
     */
    public Long setnx(String key, String value) {
        Long result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.setnx(key, value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("setnx", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 设置单个值
     *
     * @param key
     * @param value
     */
    public String set(String key, String value) {
        String result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.set(key, value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("set", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 设置map值
     *
     * @param key
     * @param map
     */
    public String hmset(String key, Map<String, String> map) {
        String result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.hmset(key, map);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("hmset", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 获取map值
     *
     * @param key
     * @param fields
     */
    public List<String> hmget(String key, String... fields) {
        List<String> result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.hmget(key, fields);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("hmget", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 设置单个值
     *
     * @param key
     * @param value
     */
    public Long hset(String key, String field, String value) {
        Long result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.hset(key, field, value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("hset", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 获取单个值
     *
     * @param key
     */
    public String hget(String key, String field) {
        String result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }

        boolean broken = false;
        try {
            result = shardedJedis.hget(key, field);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("hget", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 删除多个值
     *
     * @param key
     */
    public Long hdel(String key, String... fields) {
        Long result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }

        boolean broken = false;
        try {
            result = shardedJedis.hdel(key, fields);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("hdel", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 获取单个值
     *
     * @param key
     */
    public String get(String key) {
        String result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }

        boolean broken = false;
        try {
            result = shardedJedis.get(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("get", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public Boolean exists(String key) {
        Boolean result = false;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.exists(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("exists", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public String type(String key) {
        String result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.type(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("type", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 在某段时间后失效
     *
     * @param key
     * @param seconds
     */
    public Long expire(String key, int seconds) {
        logger.debug("key {} expire after {}", key, seconds);
        Long result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.expire(key, seconds);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("expire", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 在某个时间点失效
     *
     * @param key
     * @param unixTime
     */
    public Long expireAt(String key, long unixTime) {
        Long result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.expireAt(key, unixTime);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("expireAt", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public Long ttl(String key) {
        Long result = null;
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.ttl(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("ttl", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public long setRange(String key, long offset, String value) {
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        long result = 0;
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.setrange(key, offset, value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("setRange", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public String getRange(String key, long startOffset, long endOffset) {
        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        String result = null;
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.getrange(key, startOffset, endOffset);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("getRange", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public Long del(String key) {

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        Long result = null;
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.del(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("del", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }


    /**
     * 获取left弹出值
     *
     * @param key
     */
    public String lpop(String key) {
        String result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.lpop(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("lpop", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 获取right弹出值
     *
     * @param key
     */
    public String rpop(String key) {
        String result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.rpop(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("rpop", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 获取left弹出值
     *
     * @param key
     * @param values
     */
    public Long lpush(String key, String... values) {
        Long result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.lpush(key, values);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("lpush", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    /**
     * 获取left弹出值
     *
     * @param key
     * @param values
     */
    public Long rpush(String key, String... values) {
        Long result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.rpush(key, values);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("rpush", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }


    /**
     * 获取key len弹出值
     *
     * @param key
     */
    public Long llen(String key) {
        Long result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        boolean broken = false;
        try {
            result = shardedJedis.llen(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            broken = true;
            addAlarm("llen", e.getMessage());
        } finally {
            redisDataSource.returnResource(shardedJedis, broken);
        }
        return result;
    }

    public String setObject(String key, Serializable value) throws NotSerializableException {
        return setex(key.getBytes(), SerializableUtils.toByteArray(value));

    }

    public String setex(byte[] key, byte[] value) {
        return exec(new ShardedJedisCmd<String>() {
            @Override
            public String run(ShardedJedis shardedJedis) {
                return shardedJedis.set(key, value);
            }
        });
    }

    public String setexObject(String key, int seconds, Serializable value) throws NotSerializableException {
        return setex(key.getBytes(), seconds, SerializableUtils.toByteArray(value));

    }

    public String setex(byte[] key, int seconds, byte[] value) {
        return exec(new ShardedJedisCmd<String>() {
            @Override
            public String run(ShardedJedis shardedJedis) {
                return shardedJedis.setex(key, seconds, value);
            }
        });
    }

    public <T> T getObject(String key) throws Exception {
        return exec(new ShardedJedisCmd<T>() {
            @Override
            public T run(ShardedJedis shardedJedis) throws Exception {
                byte[] bytes = shardedJedis.get(key.getBytes());
                if (bytes != null && bytes.length > 0) {
                    return (T) SerializableUtils.fromByteArray(bytes);
                }
                return null;
            }
        });
    }

    /**
     * 获取执行任意 redis 命令值
     *
     * @param shardedJedisCmd
     */
    public <T> T exec(ShardedJedisCmd<T> shardedJedisCmd) {
        T result = null;

        ShardedJedis shardedJedis = redisDataSource.getSharedJedis();
        if (shardedJedis == null) {
            return result;
        }
        try {
            result = shardedJedisCmd.run(shardedJedis);
        } catch (Exception e) {
            logger.error("exec shardedJedisCmd error.", e);
            addAlarm("exec shardedJedisCmd", e.getMessage());
            if (shardedJedisCmd instanceof JedisCmdThrowable) {
                ((JedisCmdThrowable) shardedJedisCmd).throwable(e);
            }
        } finally {
            redisDataSource.returnResource(shardedJedis);
        }
        return result;
    }

    public <T> T exec(JedisCmd<T> jedisCmd) {
        T result = null;

        Jedis jedis = redisDataSource.getJedis();
        if (jedis == null) {
            return result;
        }
        try {
            result = jedisCmd.run(jedis);
        } catch (Exception e) {
            logger.error("exec jedisCmd error.", e);
            addAlarm("exec jedisCmd", e.getMessage());
            if (jedisCmd instanceof JedisCmdThrowable) {
                ((JedisCmdThrowable) jedisCmd).throwable(e);
            }
        } finally {
            redisDataSource.returnResource(jedis);
        }
        return result;
    }

    /**
     * @param lockKey 尝试获取 5分钟 最长能锁1小时
     */
    public boolean lock(String lockKey) {
        String sid = WebContext.currentSessionId();
        return lock(lockKey, StringUtil.isEmpty(sid) ? "-1" : sid, 3000, 3600);
    }

    public boolean lock(String lockKey, int retryTimes, int lockTimes) {
        String sid = WebContext.currentSessionId();
        return lock(lockKey, StringUtil.isEmpty(sid) ? "-1" : sid, retryTimes, lockTimes);
    }

    /**
     * 分布式 加锁
     *
     * @param lockKey
     * @param retryTimes
     */
    public synchronized boolean lock(String lockKey, String requestId, int retryTimes, int lockTimes) {
        String val = setnx(lockKey, lockTimes, requestId);
        boolean res = true;
        if (!"OK".equals(val)) {//拿不到锁，等待
            int i = 0;
            while (true) {
                val = setnx(lockKey, lockTimes, requestId);
                if ("OK".equals(val)) {
                    res = true;
                    break;
                }
                if (i == retryTimes) {
                    logger.info("wait lock time out retry times {}", retryTimes);
                    res = false;
                    break;
                }
                try {
                    this.wait(100);//0.1s
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
                i++;
            }
        }
        return res;
    }

    /**
     * 分页式 解锁
     *
     * @param lockKey
     */
    public synchronized void unlock(String lockKey) {
        del(lockKey);
    }


    private void addAlarm(String method, String errorMessage) {
        SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JEDIS_SERVICE, "" + method + "," + errorMessage));
    }
}