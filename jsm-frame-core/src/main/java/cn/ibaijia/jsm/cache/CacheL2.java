package cn.ibaijia.jsm.cache;

import com.fasterxml.jackson.core.type.TypeReference;

import java.lang.reflect.Type;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 10:48
 */
public interface CacheL2 {

    boolean put(String key, int seconds, Object Object);

    <T> T get(String key, Class<T> clazz);

    <T> T get(String key, TypeReference<T> typeReference);

    <T> T get(String key, Type type);

    String get(String key);

    boolean remove(String key);

}
