package cn.ibaijia.jsm.cache;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * EhCache + Redis(异步)
 *
 * @author longzl
 */
@Service
public class CacheService extends BaseService {

    @Resource
    private CacheL1 cacheL1;
    @Resource
    private CacheL2 cacheL2;

    private ThreadPoolExecutor executorService;

    public CacheService() {
        init();
    }

    private void init() {
        Integer threadNumber = AppContext.getAsInteger(AppContextKey.CACHE_SERVICE_ASYNC_THREAD, 1);
        logger.info("CacheService cacheL2 use async thread number:{}", threadNumber);
        executorService = new ThreadPoolExecutor(threadNumber, threadNumber, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
            byte index = 0;

            @Override
            public Thread newThread(Runnable runnable) {
                return new Thread(runnable, "cache-pool" + (++index));
            }
        });
    }

    public void close() {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void remove(String key) {
        cacheL1.remove(key);
        cacheL1.remove(key);
    }

    public <T> T get(String key, Class<T> clazz) {
        T result = getFromL1(key);
        if (result == null) {
            result = getFromL2(key, clazz);
            logger.debug("getFromL2");
        } else {
            logger.debug("getFromL1");
        }
        return result;
    }

    public <T> T get(String key, TypeReference<T> typeReference) {
        T result = getFromL1(key);
        if (result == null) {
            result = getFromL2(key, typeReference);
            logger.debug("getFromL2");
        } else {
            logger.debug("getFromL1");
        }
        return result;
    }

    public <T> T get(String key, Type type) {
        T result = getFromL1(key);
        if (result == null) {
            result = getFromL2(key, type);
            logger.debug("getFromL2");
        } else {
            logger.debug("getFromL1");
        }
        return result;
    }

    public void set(String key, int seconds, Serializable object) {
        //设置一级缓存
        cacheL1.put(key, object);
        //设置二级缓存
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                cacheL2.put(key, seconds, object);
            }
        });

    }

    private <T> T getFromL1(String key) {
        return cacheL1.get(key);
    }

    private <T> T getFromL2(String key, Class<T> clazz) {
        T value = cacheL2.get(key, clazz);
        if (value != null) {
            logger.debug("set to l1 key:{}", key);
            cacheL1.put(key, value);
        }
        return value;
    }

    private <T> T getFromL2(String key, TypeReference<T> typeReference) {
        T value = cacheL2.get(key, typeReference);
        if (value != null) {
            logger.debug("set to l1 key:{}", key);
            cacheL1.put(key, value);
        }
        return value;
    }

    private <T> T getFromL2(String key, Type type) {
        T value = cacheL2.get(key, type);
        if (value != null) {
            logger.debug("set to l1 key:{}", key);
            cacheL1.put(key, value);
        }
        return value;
    }

}
