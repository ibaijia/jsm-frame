package cn.ibaijia.jsm.cache;

import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Type;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 11:13
 */
@Component
public class JedisCacheL2 implements CacheL2 {

    private Logger logger = LogUtil.log(this.getClass());

    @Resource
    private JedisService jedisService;

    @Override
    public boolean put(String key, int seconds, Object value) {
        try {
            String val = null;
            if (value instanceof String) {
                val = (String) value;
            } else {
                val = JsonUtil.toJsonString(value);
            }
            jedisService.setex(key, seconds, val);
            return true;
        } catch (Exception e) {
            logger.error("JedisCacheL2 put error. key:" + key, e);
            return false;
        }
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        try {
            String res = jedisService.get(key);
            if (StringUtil.isEmpty(res)) {
                return null;
            }
            return JsonUtil.parseObject(res, clazz);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public <T> T get(String key, TypeReference<T> typeReference) {
        try {
            String res = jedisService.get(key);
            if (StringUtil.isEmpty(res)) {
                return null;
            }
            return JsonUtil.parseObject(res, typeReference);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public <T> T get(String key, Type type) {
        try {
            String res = jedisService.get(key);
            if (StringUtil.isEmpty(res)) {
                return null;
            }
            return JsonUtil.parseObject(res, type);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public String get(String key) {
        try {
            return jedisService.get(key);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public boolean remove(String key) {
        try {
            jedisService.del(key);
            return true;
        } catch (Exception e) {
            logger.error("JedisCacheL2 remove error. key:" + key, e);
            return false;
        }
    }

}
