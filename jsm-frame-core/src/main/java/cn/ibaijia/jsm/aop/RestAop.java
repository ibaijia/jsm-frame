package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.auth.AppAuth;
import cn.ibaijia.jsm.auth.Auth;
import cn.ibaijia.jsm.auth.WebAuth;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.dao.model.OptLog;
import cn.ibaijia.jsm.context.rest.RestStatusStrategy;
import cn.ibaijia.jsm.context.rest.order.BaseOrderLine;
import cn.ibaijia.jsm.context.rest.resp.*;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.context.session.SessionUser;
import cn.ibaijia.jsm.exception.AlarmException;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.exception.BaseException;
import cn.ibaijia.jsm.exception.FailedException;
import cn.ibaijia.jsm.license.LicenseService;
import cn.ibaijia.jsm.mybatis.datasource.DynamicDataSourceHolder;
import cn.ibaijia.jsm.stat.JsmOptLogService;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.*;
import org.apache.http.HttpStatus;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.lang.reflect.Method;

/**
 * Rest API基本控制 用到controller
 */
@Order(2)
@Aspect
@Component
public class RestAop {
    private static Logger logger = LogUtil.log(RestAop.class);
    private PlatformTransactionManager transactionManager;

    @Resource
    private JedisService jedisService;
    @Resource
    private WebAuth webAuth;
    @Resource
    private AppAuth appAuth;
    @Resource
    private LicenseService licenseService;
    @Resource
    private RestStatusStrategy restStatusStrategy;

    @Around("@annotation(restAnn)")
    public Object intercept(ProceedingJoinPoint jpt, RestAnn restAnn) throws Throwable {
        logger.debug("RestAop intercept");
        Object result = null;
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        //下单排队处理
        OrderAnn orderAnn = method.getAnnotation(OrderAnn.class);
        if (orderAnn != null) {
            Pair<String> resPair = orderCheck(jpt, orderAnn);
            if (!resPair.equals(BasePairConstants.ACTIVITY_TURN)) {
                throw new FailedException(resPair);
            }
        }
        boolean useMock = false;
        try {
            if (!AppContext.isDevModel() && licenseService != null) {
                if (!licenseService.check()) {
                    throw new AuthFailException(BasePairConstants.LICENSE_EXPIRED);
                }
            }
            //判断是否mock data
            String jsmMock = RequestUtil.getParameter(WebContext.getRequest(), WebContext.JSM_MOCK);
            if (AppContext.isDevModel() && !StringUtil.isEmpty(jsmMock)) {
                useMock = true;
                String res = JsmFrameUtil.genMockResult(method);
                logger.warn("mock resp:{}", res);
                return JsonUtil.parseObject(res, method.getGenericReturnType());
            }
            //验证内网
            if (restAnn.internal() && !IpUtil.isInternalIp(WebContext.getFirstForwardIp())) {
                throw new AuthFailException(BasePairConstants.NET_LIMIT);
            }
            //验证IP
            if (!StringUtil.isEmpty(restAnn.ipCheckKey()) && !IpUtil.checkRemoteIp(WebContext.getFirstForwardIp(), restAnn.ipCheckKey())) {
                throw new AuthFailException(BasePairConstants.IP_LIMIT);
            }

            //验证登录
            String authId = validateAuth(jpt, restAnn);
            //参数验证
            validateParams(jpt, restAnn);
            //验证重复提交
            repeatCheck(jpt, restAnn, authId);

            if (restAnn.sync()) {
                synchronized (method) {
                    result = process(jpt, restAnn);
                }
            } else {
                result = process(jpt, restAnn);
            }
        } catch (Exception e) {
            RestResp errorResp = JsmFrameUtil.dealException(e);
            return errorResp;
        } finally {
            //记录日志
            if (!useMock) {
                optLogRecord(restAnn, jpt);
            }
            // 输出结果
            boolean outputed = outputResult(result);
            if (outputed) {
                result = null;
            }
        }
        return result;
    }

    private Pair<String> orderCheck(ProceedingJoinPoint jpt, OrderAnn orderAnn) {
        String key = orderAnn.productKey();
        if (StringUtil.isEmpty(key)) {
            key = JsmFrameUtil.getKey(jpt);
        } else {
            key = TemplateUtil.formatWithContextVar(key);
        }
        BaseOrderLine orderLine = SpringContext.getBean(orderAnn.type().t());
        return orderLine.order(key, orderAnn);
    }

    private String validateAuth(ProceedingJoinPoint jpt, RestAnn resetAnn) {
        String authId = null;
        if (resetAnn.authType() != AuthType.NONE) {
            Auth auth = SpringContext.getBean(resetAnn.authType().t());
            if (auth == null) {
                logger.error("authBean not found.{}", resetAnn.authType().t());
            } else {
                logger.debug("authBean:{}", resetAnn.authType().t());
                authId = auth.checkAuth(WebContext.getRequest(), resetAnn, false);
            }
        } else {// if header has at ht ,try to use webAuth/appAuth
            if (AppContext.isJedisSession()) {
                String at = RequestUtil.get(WebContext.getRequest(), WebContext.JSM_AT);
                String ht = RequestUtil.get(WebContext.getRequest(), WebContext.JSM_HT);
                String token = RequestUtil.get(WebContext.getRequest(), WebContext.JSM_TOKEN);
                if ((StringUtil.isEmpty(at) && StringUtil.isEmpty(ht))
                        || (AppContext.isDevModel() && !StringUtil.isEmpty(token))) {
                    authId = webAuth.checkAuth(WebContext.getRequest(), resetAnn, true);
                    if (StringUtil.isEmpty(authId)) {
                        authId = appAuth.checkAuth(WebContext.getRequest(), resetAnn, true);
                    }
                }
            }
        }
        return authId;
    }

    private boolean outputResult(Object result) {
        boolean output = true;
        if (result instanceof RestResp) {
            WebContext.getRequest().setAttribute(WebContext.JSM_RESP_CODE, ((RestResp) result).code);
            if (WebContext.getResponse().getStatus() == HttpStatus.SC_OK) {
                WebContext.getResponse().setStatus(restStatusStrategy.httpStatusCode((RestResp) result));
            }
            logger.info("resp:" + StringUtil.toJson(result));
            output = false;
        } else {
            try {
                if (result instanceof FileResp) {
                    ResponseUtil.outputFile(WebContext.getResponse(), (FileResp) result);
                } else if (result instanceof TextResp) {
                    ResponseUtil.outputText(WebContext.getResponse(), (TextResp) result);
                } else if (result instanceof JsonResp) {
                    ResponseUtil.outputJson(WebContext.getResponse(), (JsonResp) result);
                } else if (result instanceof XmlResp) {
                    ResponseUtil.outputXml(WebContext.getResponse(), (XmlResp) result);
                } else if (result instanceof HtmlResp) {
                    ResponseUtil.outputHtml(WebContext.getResponse(), (HtmlResp) result);
                } else if (result instanceof ExcelResp) {
                    ResponseUtil.outputExcel(WebContext.getResponse(), (ExcelResp) result);
                } else if (result instanceof FreeResp) {
                    ResponseUtil.outputFreeResp(WebContext.getResponse(), (FreeResp) result);
                } else if (result instanceof String) {
                    String res = (String) result;
                    logger.info("resp: {}", res);
                    if (res.startsWith("redirect:")) {
                        WebContext.getResponse().sendRedirect(res.substring(9));
                    } else if (res.startsWith("forward:")) {
                        ServletRequest request = WebContext.getRequest();
                        ServletResponse response = WebContext.getResponse();
                        request.getRequestDispatcher(res.substring(8)).forward(request, response);
                    } else {
                        logger.warn("unknown Resp String!");
                        output = false;
                    }
                } else {
                    output = false;
                }
            } catch (Exception e) {
                logger.error("outputResult error.", e);
            }
        }
        return output;
    }

    private void repeatCheck(ProceedingJoinPoint jpt, RestAnn resetAnn, String authId) {
        if (WebContext.isRequestMethod("POST") || WebContext.isRequestMethod("PUT")) {
            if (resetAnn.repeatCheck()) {
                boolean isRepeat = isRepeatRequest(jpt, authId);
                if (isRepeat) {
                    throw new FailedException(BasePairConstants.REPEAT_REQ_ERROR);
                }
            }
        }
    }

    private void validateParams(ProceedingJoinPoint jpt, RestAnn resetAnn) {
        if (resetAnn.validate()) {
            String message = validate(jpt);
            if (!StringUtil.isEmpty(message)) {
                throw new FailedException(BasePairConstants.PARAMS_ERROR, message);
            }
        }
    }

    private void optLogRecord(RestAnn resetAnn, ProceedingJoinPoint jpt) {
        if (!StringUtil.isEmpty(resetAnn.log())) {
            OptLog optLog = new OptLog();
            optLog.clusterId = AppContext.getClusterId();
            optLog.traceId = WebContext.getTraceId();
            optLog.ip = WebContext.getRemoteIp();
            Session session = WebContext.currentSession();
            optLog.mac = session != null ? (String) session.get("mac") : "";
            optLog.funcName = resetAnn.logType();

            SessionUser sessionUser = WebContext.currentUser();
            if (sessionUser != null) {
                OptLogUtil.setLogUserIfNot((String) WebContext.currentSession().get(BaseConstants.SESSION_USERNAME_KEY));
                optLog.uid = sessionUser.getUid().toString();
            }
            OptLogUtil.setLogContentIfNot(jpt.getArgs());
            optLog.optDesc = TemplateUtil.formatWithContextVar(resetAnn.log());
            optLog.time = DateUtil.currentTime();
            JsmOptLogService logService = SpringContext.getBean(JsmOptLogService.class);
            logService.add(optLog);
        }
    }

    private boolean isRepeatRequest(ProceedingJoinPoint jpt, String authId) {
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        Object[] args = jpt.getArgs();
        if (args != null && args.length > 0) {
            for (Object arg : args) {
                if (arg == null) {
                    continue;
                }
                if (arg instanceof ValidateModel) {
                    String reqMd5 = EncryptUtil.md5(StringUtil.toJson(arg));
                    String res = jedisService.setnx(method.getName() + "_" + reqMd5 + "_" + authId, 5, "1");
                    if (!"OK".equals(res)) {//已经有相同的请求了
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Object process(ProceedingJoinPoint jpt, RestAnn resetAnn) throws Throwable {
        Object result = null;
        //事务处理
        TransactionStatus status = null;
        String clusterSyncLock = resetAnn.clusterSyncLock();
        try {
            clusterSyncLock = TemplateUtil.formatWithContextVar(clusterSyncLock);
            if (!StringUtil.isEmpty(clusterSyncLock) && jedisService != null) {
                jedisService.lock(clusterSyncLock);
            }
            DynamicDataSourceHolder.setDataSource(false);
            if (!resetAnn.transaction().equals(Transaction.NONE)) {
                DynamicDataSourceHolder.setDataSource(resetAnn.transaction().equals(Transaction.READ));
                this.transactionManager = ((PlatformTransactionManager) SpringContext.getBean(resetAnn.transManagerName()));
                status = this.transactionManager.getTransaction(createTransactionDefinition(resetAnn));
                TransactionUtil.setTransactionStatus(status);
            }
            result = jpt.proceed();
            if (status != null && !status.isCompleted()) {
                if (status.isRollbackOnly()) {
                    logger.warn("transaction rollback!");
                    this.transactionManager.rollback(status);
                } else {
                    this.transactionManager.commit(status);
                }
            }
            if (!StringUtil.isEmpty(resetAnn.log())) {
                OptLogUtil.setLogStatusIfNot("成功");
            }
        } catch (Exception e) {
            logger.error("process error.", e);
            if (status != null) {
                logger.error("error: transaction rollback!");
                this.transactionManager.rollback(status);
            }
            if (!StringUtil.isEmpty(resetAnn.log())) {
                OptLogUtil.setLogStatusIfNot("失败");
            }
            if (e instanceof AlarmException) {
                SystemUtil.addAlarm(new Alarm(((AlarmException) e).getName(), ((BaseException) e).getMsg()));
            } else if (e instanceof BaseException) {
                SystemUtil.addAlarm(new Alarm(e.getClass().getSimpleName(), ((BaseException) e).getMsg()));
            } else {
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_REST_AOP, "Exception," + e.getMessage()));
            }
            throw e;
        } finally {
            TransactionUtil.removeTransactionStatus();
            if (!StringUtil.isEmpty(clusterSyncLock) && jedisService != null) {
                jedisService.unlock(clusterSyncLock);
            }
        }
        return result;
    }

    private String validate(ProceedingJoinPoint jpt) {
        String message = null;
        Object[] args = jpt.getArgs();
        if (args != null && args.length > 0) {
            for (Object arg : args) {
                if (arg == null) {
                    continue;
                }
                if (arg instanceof ValidateModel) {
                    logger.info("request arg:{}", StringUtil.toJson(arg));
                    message = ValidateUtil.validate(arg);
                    if (!StringUtil.isEmpty(message)) {
                        break;
                    }
                } else {
                    logger.info("request arg type:{}", arg.getClass().getName());
                }
            }
        }
        return message;
    }

    private DefaultTransactionDefinition createTransactionDefinition(RestAnn restAnn) {
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setPropagationBehavior(restAnn.transPropagationBehavior());
        transactionDefinition.setIsolationLevel(restAnn.transIsolationLevel());
        transactionDefinition.setReadOnly(restAnn.transaction().equals(Transaction.READ));
        transactionDefinition.setTimeout(restAnn.transTimeout());//seconds
        return transactionDefinition;
    }

}
