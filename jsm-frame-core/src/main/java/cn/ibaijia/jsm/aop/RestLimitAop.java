package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.RestLimitAnn;
import cn.ibaijia.jsm.utils.JsmFrameUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.ReflectionUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

/**
 * Rest API限流控制 用到controller
 */
@Order(1)
@Aspect
@Component
public class RestLimitAop {
    private static Logger logger = LogUtil.log(RestLimitAop.class);

    private static Map<String, Semaphore> map = new ConcurrentHashMap<>();

    @Around("@annotation(restLimitAnn)")
    public Object intercept(ProceedingJoinPoint jpt, RestLimitAnn restLimitAnn) throws Throwable {
        logger.debug("RestLimitAop intercept");
        String key = JsmFrameUtil.getKey(jpt);
        Semaphore semaphore = getSemaphore(key, restLimitAnn.value());
        if (semaphore.tryAcquire()) {
            try {
                return jpt.proceed();
            } catch (Exception e) {
                throw e;
            } finally {
                semaphore.release();
            }
        } else {
            logger.debug("invoke: {}", restLimitAnn.fallbackMethod());
            return ReflectionUtil.invokeMethod(jpt, restLimitAnn.fallbackMethod());
        }
    }



    private synchronized Semaphore getSemaphore(String key, int permits) {
        Semaphore semaphore = map.get(key);
        if (semaphore == null) {
            semaphore = new Semaphore(permits);
            map.put(key, semaphore);
        }
        return semaphore;
    }

}
