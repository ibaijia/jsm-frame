package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.ClusterLockAnn;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.utils.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 分布式锁 切面用到service方法
 */
@Order(2)
@Aspect
@Component
public class ClusterLockAop {
    private static Logger logger = LogUtil.log(ClusterLockAop.class);

    @Resource
    private JedisService jedisService;

    //@Around("execution(* *.service.*.*(..))")
    @Around("@annotation(clusterLockAnn)")
    public Object intercept(ProceedingJoinPoint jpt, ClusterLockAnn clusterLockAnn) throws Throwable {
        Object result = null;
        if (TransactionUtil.getTransactionStatus() != null) {
            logger.warn("ClusterLockAnn not recommend run in transaction. it's may be make db lock.");
        }
        String key = clusterLockAnn.value();
        if (StringUtil.isEmpty(key)) {
            key = JsmFrameUtil.getLockKey(jpt);
        } else {
            key = TemplateUtil.formatWithContextVar(key);
        }
        try {
            logger.debug("clusterLockAnn lock:{}", key);
            boolean locked = jedisService.lock(key);
            if (!locked) {
                logger.error("clusterLockAnn lock:{} failed, return.", key);
                return result;
            }
            result = jpt.proceed();
        } catch (Exception e) {
            logger.error("clusterLockAnn error,lock key:" + key);
            throw e;
        } finally {
            jedisService.unlock(key);
            logger.debug("clusterLockAnn unlock:{}", key);
        }
        return result;
    }

}
