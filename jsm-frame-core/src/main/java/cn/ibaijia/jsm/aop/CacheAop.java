package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.CacheAnn;
import cn.ibaijia.jsm.annotation.CacheType;
import cn.ibaijia.jsm.cache.CacheL1;
import cn.ibaijia.jsm.cache.CacheL2;
import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.utils.JsmFrameUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.TemplateUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * 用于缓存 一般为service 方法
 */
@Order(1)
@Aspect
@Component
public class CacheAop {
    private static Logger logger = LogUtil.log(CacheAop.class);

    @Resource
    private CacheL1 cacheL1;
    @Resource
    private CacheL2 cacheL2;
    @Resource
    private CacheService cacheService;

    @Around("@annotation(cacheAnn)")
    public Object intercept(ProceedingJoinPoint jpt, CacheAnn cacheAnn) throws Throwable {
        Object result = null;
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        if (method.getReturnType().getName().equals("void")) {
            return jpt.proceed();
        }
        if (cacheAnn.cacheType().equals(CacheType.L1)) {
            result = getFromL1(jpt, cacheAnn);
        } else if (cacheAnn.cacheType().equals(CacheType.L2)) {
            result = getFromL2(jpt, cacheAnn);
        } else if (cacheAnn.cacheType().equals(CacheType.BOTH)) {
            result = getFromBoth(jpt, cacheAnn);
        }
        return result;
    }

    private Object getFromBoth(ProceedingJoinPoint jpt, CacheAnn cacheAnn) throws Throwable {
        Object result;
        String key = cacheAnn.cacheKey();
        if (StringUtil.isEmpty(key)) {
            key = JsmFrameUtil.getKey(jpt);
        } else {
            key = TemplateUtil.formatWithContextVar(key);
        }
        Type returnType = getReturnType(jpt);
        result = cacheService.get(key, returnType);
        if (result == null) {
            synchronized (jpt) {
                result = cacheService.get(key, returnType);
                if (result == null) {
                    result = jpt.proceed();
                    if (result != null || cacheAnn.cacheNull()) {
                        cacheService.set(key, cacheAnn.expireSeconds(), (Serializable) result);
                    }
                    logger.debug("update cacheService key: " + key);
                }
            }
        }
        return result;
    }

    private Object getFromL2(ProceedingJoinPoint jpt, CacheAnn cacheAnn) throws Throwable {
        String key = cacheAnn.cacheKey();
        if (StringUtil.isEmpty(key)) {
            key = JsmFrameUtil.getKey(jpt);
        } else {
            key = TemplateUtil.formatWithContextVar(key);
        }
        int expireTime = cacheAnn.expireSeconds();
        Type returnType = getReturnType(jpt);
        Object result = cacheL2.get(key, returnType);
        if (result == null) {
            synchronized (jpt) {
                result = cacheL2.get(key, returnType);
                if (result == null) {
                    result = jpt.proceed();
                    if (result != null || cacheAnn.cacheNull()) {
                        cacheL2.put(key, expireTime, result);
                    }
                    logger.debug("update cacheL2 key: " + key);
                }
            }
        }
        return result;
    }

    private Type getReturnType(ProceedingJoinPoint jpt) {
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        return method.getGenericReturnType();
    }

    private Object getFromL1(ProceedingJoinPoint jpt, CacheAnn cacheAnn) throws Throwable {
        String key = cacheAnn.cacheKey();
        if (StringUtil.isEmpty(key)) {
            key = JsmFrameUtil.getKey(jpt);
        } else {
            key = TemplateUtil.formatWithContextVar(key);
        }
        Object result = cacheL1.get(key);
        if (result == null) {
            synchronized (jpt) {
                result = cacheL1.get(key);
                if (result == null) {
                    result = jpt.proceed();
                    if (result != null || cacheAnn.cacheNull()) {
                        cacheL1.put(key, result);
                    }
                    logger.debug("update cacheL1 key: " + key);
                }
            }
        }
        return result;
    }

}
