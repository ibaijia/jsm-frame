package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.ClusterJobLockAnn;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.JsmFrameUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 分布式锁 切面 不到job方法
 */
@Order(1)
@Aspect
@Component
public class ClusterJobLockAop {
    private static Logger logger = LogUtil.log(ClusterJobLockAop.class);

    private AtomicInteger traceIdAi = new AtomicInteger(0);


    @Resource
    private JedisService jedisService;

    @Around("@annotation(clusterJobLockAnn)")
    public Object intercept(ProceedingJoinPoint jpt, ClusterJobLockAnn clusterJobLockAnn) throws Throwable {
        Object result = null;
        String key = clusterJobLockAnn.value();
        if (StringUtil.isEmpty(key)) {
            key = JsmFrameUtil.getLockKey(jpt);
        }
        try {
            logger.debug("clusterJobLockAnn lock:{}", key);
            boolean locked = jedisService.lock(key, 0, clusterJobLockAnn.freezeTime());
            if (!locked) {
                logger.warn("clusterJobLockAnn lock:{} failed, return.", key);
                return result;
            }
            //set traceId
            int intCount = traceIdAi.getAndIncrement();
            if (intCount > Integer.MAX_VALUE - 100) {
                traceIdAi.set(0);
            }
            WebContext.setTraceId(String.format("job-%s-%s-%s", AppContext.getClusterId(), key, intCount));
            result = jpt.proceed();
        } catch (Exception e) {
            logger.error("clusterJobLockAnn error,clusterJobLockAnn key:" + key, e);
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_CLUSTER_JOB_AOP, key + ":Exception," + e.getMessage()));
        } finally {
            WebContext.clearTraceId();
        }
        return result;
    }

}
