package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.RestFuseAnn;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.ReflectionUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Rest API熔断控制 用到controller
 */
@Order(3)
@Aspect
@Component
public class RestFuseAop {
    private static Logger logger = LogUtil.log(RestFuseAop.class);

    @Around("@annotation(restFuseAnn)")
    public Object intercept(ProceedingJoinPoint jpt, RestFuseAnn restFuseAnn) throws Throwable {
        logger.debug("RestFuseAop intercept");
        try {
            return jpt.proceed();
        } catch (Exception e) {
            try {
                //调用熔错方法
                logger.debug("invoke: {}", restFuseAnn.value());
                return ReflectionUtil.invokeMethod(jpt, restFuseAnn.value());
            } catch (Exception ex) {
                throw e;
            }
        }
    }

}
