package cn.ibaijia.jsm.annotation;

public enum Transaction {
	NONE(0,"无事务"), READ(1,"读事务"), WRITE(2,"写事务");
	private int _v;
	private String _t;
	Transaction(int value,String t){
		_v = value;
		_t = t;
	}
	public int v(){
		return _v;
	}
	public static String findText(int v){
		Transaction[] values = Transaction.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._v == v){
				return values[i]._t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		Transaction[] values = Transaction.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._t.equals(t)){
				return values[i]._v;
			}
		}
		return 0;
	}
}
