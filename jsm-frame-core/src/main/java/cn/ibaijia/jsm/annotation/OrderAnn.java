package cn.ibaijia.jsm.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author longzl / @createOn 2019-12-30
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface OrderAnn {

    /**
     * 非公平排队方案（快但不公平）公平排队方案（稍慢但公平）
     */
    public abstract LineType type() default LineType.UNFAIR;

    /**
     * 保证每个商品唯一  默认 className+methodName
     */
    public abstract String productKey() default "";

    /**
     * 每个用户唯一
     */
    public abstract String userKey() default "";

    /**
     * 持续时长
     */
    public abstract long duration() default 10 * 60;

}
