package cn.ibaijia.jsm.annotation;

import org.springframework.transaction.TransactionDefinition;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author longzl use in controller
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RestAnn {

    /**
     * 是登录客户端类型，默认为APP 移动端
     */
    public abstract AuthType authType() default AuthType.NONE;

    /**
     * 权限，默认值为"" PremissionCode or ResourceCode
     */
    public abstract String permission() default "";

    /**
     * 日志组
     */
    public abstract String logType() default "";

    /**
     * 日志支持变量
     */
    public abstract String log() default "";

    /**
     * 是否需要验证
     */
    public abstract boolean validate() default true;

    /**
     * synchronized,默认并行请求
     */
    public abstract boolean sync() default false;

    /**
     * 集群环境下的分布式同步锁,默认不开
     */
    public abstract String clusterSyncLock() default "";

    /**
     * 事务属性，默认为不在controller层开事务
     */
    public abstract Transaction transaction() default Transaction.NONE;

    /**
     * 事务超时时间
     */
    public abstract int transTimeout() default 600;

    /**
     * 传播行为
     */
    public abstract int transPropagationBehavior() default TransactionDefinition.PROPAGATION_REQUIRED;

    /*** 隔离级别*/
    public abstract int transIsolationLevel() default TransactionDefinition.ISOLATION_DEFAULT;

    /**
     * if Transaction.READ try read transactionManagerRead
     * 自定事务管理器
     */
    public abstract String transManagerName() default "transactionManager";

    /**
     * 是否启用IP检查 默认false AppContext.get(key),通常用于 三方调用IP检查
     */
    public abstract String ipCheckKey() default "";

    /**
     * 是否内网API 默认false
     */
    public abstract boolean internal() default false;

    /**
     * 请求ID，防止重复请求 只对POST PUT方法有效默认开起 默认requestBody md5
     */
    public abstract boolean repeatCheck() default true;

}
