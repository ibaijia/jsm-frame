package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * 用于解决集群中的job重复问题
 */
@Target( { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ClusterJobLockAnn {

    /** 默认 className+methodName */
    public abstract String value() default "";

    /**
     * 冻结时间 使其集群环境 其它实例不执行，默认10秒后
     */
    public abstract int freezeTime() default 10;


}
