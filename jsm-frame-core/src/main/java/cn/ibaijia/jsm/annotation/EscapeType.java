package cn.ibaijia.jsm.annotation;

public enum EscapeType {
	NONE(0,"none"), HTML(1,"html"), JS(2,"js"), CSS(3,"css"), ALL(4,"all");
	private int _v;
	private String _t;
	EscapeType(int value,String t){
		_v = value;
		_t = t;
	}
	public int v(){
		return _v;
	}
	public static String findText(int v){
		EscapeType[] values = EscapeType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._v == v){
				return values[i]._t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		EscapeType[] values = EscapeType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._t.equals(t)){
				return values[i]._v;
			}
		}
		return 0;
	}
}
