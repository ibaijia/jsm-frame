package cn.ibaijia.jsm.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( { java.lang.annotation.ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PermissionAnn {
	
	public String description() default "";
	
	public String groupName() default "";
	
}
