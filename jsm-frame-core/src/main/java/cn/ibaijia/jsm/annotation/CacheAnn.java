package cn.ibaijia.jsm.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author longzl / @createOn 2011-8-31
 * cache the method's return value
 */
@Target({ java.lang.annotation.ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface CacheAnn {
	/**
	 * 缓存方式
	 */
	public CacheType cacheType() default CacheType.BOTH;
	
	/**
	 * 过期时间秒
	 * only support CacheType.L2 and BOTH
	 */
	public int expireSeconds() default 300;

	/**
	 * 自定义cacheKey 实现可控 过期
	 */
	public String cacheKey() default "";

	/**
	 * 设置缓存空值 可以有效防止缓存击穿 避免攻击
	 */
	public boolean cacheNull() default true;
	
	
}
