package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface FieldAnn {

    public boolean required() default true;

    /**
     * 框架内置的几种 单个字段 类型，见FieldType
     * @return
     */
    public FieldType type() default FieldType.ALL;

    /**
     * 适用于 单个字段规则验证,如个性化的密码规则
     * @return 不匹配regex返回 message
     */
    public String regex() default "";

    /**
     * regex 或者 el不匹配时,返回的错误信息
     */
    public String message() default "";

    /**
     * 内置对象 vm(本身) session(JsmSession) request(HttpServletRequest)　#{elvar}
     * return 不匹配EL返回 message,效果同validateMethod,适用于请求对象内部多个字段的逻辑验证,如a字段为true时,b字段一定不能为空
     */
    public String el() default "";

    /**
     * 需要实现ValidateCallback接口的Spring BeanName或者BeanType(包名+类名),适用于查询DB的校验,如用户名验重
     */
    public String validateBean() default "";

    /**
     * 指定 ValidateModel中的验证方法 默认 fieldNameValidate,适用于请求对象内部多个字段的逻辑验证,如a字段为true时,b字段一定不能为空
     */
    public String validateMethod() default "default";

    public int minLen() default -1;

    public int maxLen() default -1;

    public EscapeType escape() default EscapeType.ALL;

    /**
     * 字段名字 non use
     */
    @Deprecated
    public String name() default "";

    /**
     * 字段说明
     */
    public String comments() default "";

    public String mockRule() default "";

    public String mockValue() default "";

}
