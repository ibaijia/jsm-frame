package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

@Target( { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ClusterLockAnn {

    /** 默认 className+methodName */
    public abstract String value() default "";

}
