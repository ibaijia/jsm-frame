package cn.ibaijia.jsm.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * API 熔断功能 api如错时回调处理
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RestFuseAnn {

    /**
     * 熔断（错误）时处理方法
     */
    public abstract String value();


}
