package cn.ibaijia.jsm.annotation;

public enum CacheType {
	L1(0,"L1 Caffeine"), L2(1,"L2 Redis"), BOTH(2,"BOTH L1+L2");
	private int _v;
	private String _t;
	CacheType(int value,String t){
		_v = value;
		_t = t;
	}
	public int v(){
		return _v;
	}
	public static String findText(int v){
		CacheType[] values = CacheType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._v == v){
				return values[i]._t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		CacheType[] values = CacheType.values();
		for (int i=0;i<values.length;i++){
			if (values[i]._t.equals(t)){
				return values[i]._v;
			}
		}
		return 0;
	}
}
