package cn.ibaijia.jsm.annotation;

public enum LineType {
    UNFAIR(0, "unfairLineService"), FAIR(1, "fairLineService");
    private int _v;
    private String _t;

    LineType(int value, String t) {
        _v = value;
        _t = t;
    }

    public int v() {
        return _v;
    }

    public String t() {
        return _t;
    }

    public static String findText(int v) {
        LineType[] values = LineType.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i]._v == v) {
                return values[i]._t;
            }
        }
        return null;
    }

    public static int findInt(String t) {
        LineType[] values = LineType.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i]._t.equals(t)) {
                return values[i]._v;
            }
        }
        return 0;
    }
}
