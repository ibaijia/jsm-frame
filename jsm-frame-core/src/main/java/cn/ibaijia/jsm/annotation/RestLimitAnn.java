package cn.ibaijia.jsm.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * API 限流功能 限制同时能处理多少请求
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RestLimitAnn {

    /**
     * 同时最大请求数
     */
    public abstract int value() default 100;

    /**
     * 限流后处理方法
     */
    public abstract String fallbackMethod();


}
