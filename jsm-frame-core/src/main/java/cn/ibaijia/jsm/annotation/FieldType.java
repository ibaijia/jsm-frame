package cn.ibaijia.jsm.annotation;

import cn.ibaijia.jsm.context.SpringContext;

public enum FieldType {
	ALL(-1, "all"), 
	NUMBER(0, "valid.number"), 
	EN_NUMBER(1, "valid.letterornumber"), 
	EMAIL(2, "valid.email"), 
	URL(3, "valid.url"), 
	ID_CARD(4, "valid.idcard"),
	TEL_NO(5, "valid.telephone"), 
	MOB_NO(6, "valid.mobilenumber"),
	IPV4(7, "valid.ipv4"),
	DATE(8, "valid.date"),
	DATE_TIME(9, "valid.datetime"),
	CHINESE(10, "valid.chinese"),
	MONEY(11, "valid.price"),
	BANK_CARD(12, "valid.bankcard");
	private int _v;
	private String _t;

	FieldType(int value, String t) {
		_v = value;
		_t = t;
	}

	public int v() {
		return _v;
	}
	
	public String t() {
		
		return SpringContext.getMessage(_t);
	}

	public static String findText(int v) {
		FieldType[] values = FieldType.values();
		for (int i = 0; i < values.length; i++) {
			if (values[i]._v == v) {
				return values[i]._t;
			}
		}
		return null;
	}

	public static int findInt(String t) {
		FieldType[] values = FieldType.values();
		for (int i = 0; i < values.length; i++) {
			if (values[i]._t.equals(t)) {
				return values[i]._v;
			}
		}
		return 0;
	}
}
