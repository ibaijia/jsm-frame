package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;

/**
 * @author longzl
 */
public class NoPermissionException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NoPermissionException() {
        super(BasePairConstants.NO_PERMISSION.getMessage());
        this.setErrorPair(BasePairConstants.NO_PERMISSION);
    }


}
