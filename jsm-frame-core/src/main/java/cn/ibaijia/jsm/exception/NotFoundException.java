package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author longzl
 */
public class NotFoundException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NotFoundException(String msg) {
        super(msg);
        this.setMsg(msg);
        this.setErrorPair(BasePairConstants.NOT_FOUND);
    }


    public NotFoundException(Pair errorPair) {
        super(errorPair.getMessage());
        this.errorPair = errorPair;
    }

    public NotFoundException(Pair errorPair, String msg) {
        super(msg);
        this.errorPair = errorPair;
        this.setMsg(msg);
    }
}
