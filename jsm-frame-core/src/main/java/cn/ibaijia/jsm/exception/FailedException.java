package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;

public class FailedException extends BaseException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public FailedException(String msg) {
		super(msg);
		this.setErrorPair(BasePairConstants.FAIL);
		this.setMsg(msg);
	}

	public FailedException(Pair errorPair) {
		super(errorPair.getMessage());
		this.errorPair = errorPair;
	}

	public FailedException(Pair errorPair, String msg) {
		super(msg);
		this.errorPair = errorPair;
		this.setMsg(msg);
	}
}
