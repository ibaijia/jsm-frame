package cn.ibaijia.jsm.oauth.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class RefreshTokenReq implements ValidateModel {

    /**
     * defaultVal:NULL
     * comments:AppKey
     */
    @FieldAnn(required = true, maxLen = 50, comments = "AppKey")
    public String appKey;
    /**
     * defaultVal:NULL
     * comments:accessToken
     */
    @FieldAnn(required = true, maxLen = 100, comments = "accessToken")
    public String accessToken;
    /**
     * defaultVal:NULL
     * comments:refreshToken
     */
    @FieldAnn(required = true, maxLen = 100, comments = "refreshToken")
    public String refreshToken;


}
