package cn.ibaijia.jsm.oauth.client;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.oauth.model.GetTokenReq;
import cn.ibaijia.jsm.oauth.model.RefreshTokenReq;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.HttpClientUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import org.slf4j.Logger;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * 适合OauthClient 如微信小程序OauthClient
 */
public class OauthClient {

    private Logger logger = LogUtil.log(OauthClient.class);
    private Date expireTime = new Date();
    private String appKey;
    private String appSecrete;
    private String getTokenUrl;
    private String refreshTokenUrl;

    /**
     * oauth.client.appKey
     */
    public OauthClient() {
        this.appKey = AppContext.get(AppContextKey.OAUTH_CLIENT_APP_KEY);
        this.appSecrete = AppContext.get(AppContextKey.OAUTH_CLIENT_APP_SECRET);
        this.getTokenUrl = AppContext.get(AppContextKey.OAUTH_CLIENT_TOKEN_URL);
        this.refreshTokenUrl = AppContext.get(AppContextKey.OAUTH_CLIENT_REFRESH_TOKEN_URL);
    }

    public OauthClient(String appKey, String appSecrete, String getTokenUrl, String refreshTokenUrl) {
        this.appKey = appKey;
        this.appSecrete = appSecrete;
        this.getTokenUrl = getTokenUrl;
        this.refreshTokenUrl = refreshTokenUrl;
    }

    public String getAccessToken() {
        GetTokenReq app = new GetTokenReq();
        app.appKey = this.appKey;
        app.appSecret = this.appSecrete;
        if (StringUtil.isEmpty(app.appKey)) {
            logger.error("oauth.client.appKey is not config in AppContext.");
            return null;
        }
        if (StringUtil.isEmpty(app.appSecret)) {
            logger.error("oauth.client.appSecret is not config in AppContext.");
            return null;
        }
        String getTokenUrl = this.getTokenUrl;
        if (StringUtil.isEmpty(getTokenUrl)) {
            logger.error("oauth.client.getToken.url is not config in AppContext.");
            return null;
        }
        String res = HttpClientUtil.post(getTokenUrl, StringUtil.toJson(app));
        logger.info("get oauth res:{}", res);
        return res;
    }

    public <T> T getAccessToken(Class<T> clazz) {
        String res = getAccessToken();
        if (!StringUtil.isEmpty(res)) {
            return StringUtil.parseObject(res, clazz);
        }
        return null;
    }

    public <T> T getAccessToken(JavaType type) {
        String res = getAccessToken();
        if (!StringUtil.isEmpty(res)) {
            return StringUtil.parseObject(res, type);
        }
        return null;
    }

    public <T> T getAccessToken(TypeReference<T> typeReference) {
        String res = getAccessToken();
        if (!StringUtil.isEmpty(res)) {
            return StringUtil.parseObject(res, typeReference);
        }
        return null;
    }

    public void setExpireIn(int expireIn) {
        this.expireTime = DateUtil.addSecond(DateUtil.currentDate(), expireIn);
    }

    public boolean isExpired() {
        return DateUtil.reduce(expireTime, DateUtil.currentDate(), DateUtil.MILLIS_PER_SECOND) < 300;
    }

    public String refreshToken(String refreshToken) {
        RefreshTokenReq refreshTokenReq = new RefreshTokenReq();
        refreshTokenReq.appKey = this.appKey;
        refreshTokenReq.refreshToken = refreshToken;
        String refreshTokenUrl = this.refreshTokenUrl;
        if (StringUtil.isEmpty(refreshTokenUrl)) {
            logger.error("oauth.client.refreshToken.url is not config in AppContext.");
            return null;
        }
        String res = HttpClientUtil.post(refreshTokenUrl, refreshTokenReq);
        logger.info("refresh oauth res:{}", res);
        return res;
    }

    public <T> T refreshToken(String refreshToken, Class<T> clazz) {
        String res = refreshToken(refreshToken);
        if (!StringUtil.isEmpty(res)) {
            return StringUtil.parseObject(res, clazz);
        }
        return null;
    }

    public <T> T refreshToken(String refreshToken, JavaType type) {
        String res = refreshToken(refreshToken);
        if (!StringUtil.isEmpty(res)) {
            return StringUtil.parseObject(res, type);
        }
        return null;
    }

    public <T> T refreshToken(String refreshToken, TypeReference<T> typeReference) {
        String res = refreshToken(refreshToken);
        if (!StringUtil.isEmpty(res)) {
            return StringUtil.parseObject(res, typeReference);
        }
        return null;
    }

}
