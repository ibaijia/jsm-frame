package cn.ibaijia.jsm.oauth.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class GetTokenReq implements ValidateModel {

    /**
     * defaultVal:NULL
     * comments:AppKey
     */
    @FieldAnn(required = true, maxLen = 50, comments = "AppKey")
    public String appKey;
    /**
     * defaultVal:NULL
     * comments:AppSecret
     */
    @FieldAnn(required = true, maxLen = 100, comments = "AppSecret")
    public String appSecret;


}
