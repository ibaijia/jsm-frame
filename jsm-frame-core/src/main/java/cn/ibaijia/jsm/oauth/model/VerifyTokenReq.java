package cn.ibaijia.jsm.oauth.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

public class VerifyTokenReq implements ValidateModel {

    @FieldAnn(comments = "客户端AT, from user client or app self")
    public String accessToken;

    @FieldAnn(comments = "授权类型, code,implicit,password,client")
    public String grantType;

    @FieldAnn(comments = "资源代码, resource code")
    public String resourceCode;

}
