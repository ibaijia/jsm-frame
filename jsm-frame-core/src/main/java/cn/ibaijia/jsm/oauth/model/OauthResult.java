package cn.ibaijia.jsm.oauth.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * 
 */
public class OauthResult implements ValidateModel {

    @FieldAnn(required = false, maxLen = 50, name = "appName", comments = "AppName")
    public String appName;

    @FieldAnn(required = true, maxLen = 50, comments = "AppKey")
    public String appKey;//用户appKey

    @FieldAnn(required = true, maxLen = 50, comments = "accessToken")
    public String accessToken;//随机生成的uuid

    @FieldAnn(required = true, maxLen = 50, comments = "refreshToken")
    public String refreshToken;//用于刷新token的凭证

    @FieldAnn(required = true, comments = "授权范围")
    public String scope;

    @FieldAnn(required = true, comments = "过期时间(秒),过期前可以使用rt来刷新token")
    public Integer expireIn;//accessToken过期时间

    @FieldAnn(required = true, comments = "authType")
    public Integer authType;


}
