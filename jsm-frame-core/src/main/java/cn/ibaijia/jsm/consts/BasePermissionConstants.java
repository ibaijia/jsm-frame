package cn.ibaijia.jsm.consts;

import cn.ibaijia.jsm.annotation.PermissionAnn;

/**
 * @author Administrator
 */
public class BasePermissionConstants {
	
//	private static Logger logger = LogUtil.log(BasePermissionConsts.class);
	
	@PermissionAnn(description="超级管理",groupName="系统")
	public static final String SUPER_ADMIN = "SUPER_ADMIN";
	
//	public static void main(String[] args) {
//		List<Permission> permissions = readPermissions();
//		for(Permission permission:permissions){
//			System.out.println(permission);
//		}
//	}
//
//	public static List<Permission> readPermissions(){
//		List<Permission> permissions = new ArrayList<Permission>();
//		try {
//			Field[] fieldList = BasePermissionConsts.class.getDeclaredFields();
//			for(Field field:fieldList){
//				PermissionAnn ann = field.getAnnotation(PermissionAnn.class);
//				if(ann == null){
//					continue;
//				}
//				Permission permission = new Permission();
//				permission.code = (String) field.get(BasePermissionConsts.class);
//				permission.name = field.getName();
//				permission.description = ann.description();
//				permission.groupName = ann.groupName();
//				permissions.add(permission);
//			}
//		} catch (Exception e) {
//			logger.error("readPermissions ann error!",e);
//		}
//		return permissions;
//	}
}
