package cn.ibaijia.jsm.consts;

/**
 * @author longzl
 */
public class BasePairConstants {

    public static final String SESSION_ATTR_KEY = "jsm:session:attrKey";

    public static final Pair<String> OK = new Pair<String>("1001", "成功!");
    public static final Pair<String> FAIL = new Pair<String>("1002", "失败!");
    public static final Pair<String> ERROR = new Pair<String>("1003", "出错啦!");
    public static final Pair<String> NOT_FOUND = new Pair<String>("1004", "找不到资源!");
    public static final Pair<String> LICENSE_EXPIRED = new Pair<String>("1005", "授权过期!");

    public static final Pair<String> ACTIVITY_NOT_STARTED = new Pair<String>("1020", "未开始！");
    public static final Pair<String> ACTIVITY_QUEUEING = new Pair<String>("1021", "排队中!");
    public static final Pair<String> ACTIVITY_OVER = new Pair<String>("1022", "抢购结束!");
    public static final Pair<String> ACTIVITY_LIMITED = new Pair<String>("1023", "不能参与!");
    public static final Pair<String> ACTIVITY_TURN = new Pair<String>("1024", "可下单!");

    public static final Pair<String> AUTH_FAIL = new Pair<String>("1101", "鉴权失败!");
    public static final Pair<String> PARAMS_ERROR = new Pair<String>("1102", "参数错误!");
    public static final Pair<String> REPEAT_REQ_ERROR = new Pair<String>("1103", "请勿重复提交!");
    public static final Pair<String> NET_LIMIT = new Pair<String>("1104", "网络受限!");
    public static final Pair<String> IP_LIMIT = new Pair<String>("1105", "IP受限!");

    public static final Pair<String> NO_LOGIN = new Pair<String>("1201", "需要登录");
    public static final Pair<String> NO_PERMISSION = new Pair<String>("1202", "无权限");

    public static final Pair<String> VERIFY_ERROR = new Pair<String>("9001", "AT校验失败");
    public static final Pair<String> REFRESH_ERROR = new Pair<String>("9000", "RT校验失败");

}
