package cn.ibaijia.jsm.consts;

/**
 * @author longzl
 */
public class BaseConstants {

    public static final String SYSTEM_ALARM_TYPE_REST_AOP = "RestAop";
    public static final String SYSTEM_ALARM_TYPE_SLOW_SQL = "SlowSql";
    public static final String SYSTEM_ALARM_TYPE_JEDIS_SERVICE = "JedisService";
    public static final String SYSTEM_ALARM_TYPE_CLUSTER_JOB_AOP = "ClusterJobLockAop";
    public static final String SYSTEM_ALARM_TYPE_HTTP_CLIENT = "HttpClient";
    public static final String SYSTEM_ALARM_TYPE_HTTP_ASYNC_CLIENT = "HttpAsyncClient";
    public static final String SYSTEM_ALARM_TYPE_ELASTIC_CLIENT = "ElasticClient";
    public static final String SYSTEM_ALARM_TYPE_ELASTIC_APPENDER = "ElasticAppender";
    public static final String SYSTEM_ALARM_TYPE_JSM_DISRUPTOR = "JsmDisruptor";
    public static final String SYSTEM_ALARM_TYPE_SLOW_API = "SlowApi";

    public static final String STRATEGY_TYPE_NONE = "none";
    public static final String STRATEGY_TYPE_MEMORY = "memory";
    public static final String STRATEGY_TYPE_CONSOLE = "console";
    public static final String STRATEGY_TYPE_ELASTIC = "elastic";
    public static final String STRATEGY_TYPE_KAFKA = "kafka";
    public static final String STRATEGY_TYPE_RABBITMQ = "rabbitmq";
    public static final String STRATEGY_TYPE_DB = "db";

    public static final int STATUS_ENABLE = 1;
    public static final int STATUS_DISABLE = 2;

    public static final int BOOLEAN_YES = 1;
    public static final int BOOLEAN_NO = 0;

    public static String SESSION_USERNAME_KEY = "username";

}
