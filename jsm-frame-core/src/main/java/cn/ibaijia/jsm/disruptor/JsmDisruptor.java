package cn.ibaijia.jsm.disruptor;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.SystemUtil;
import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class JsmDisruptor<T> {
    private static Logger logger = StatusLogger.getLogger();
    private EventTranslatorOneArg<MessageEvent<T>, T> translator = new MessageEventTranslator();
    private Disruptor<MessageEvent<T>> disruptor;
    private EventHandler<MessageEvent<T>> eventEventHandler;
    private int bufferSize = 1024;//必须是2的N次方
    //BlockingWaitStrategy 是最低效的策略，但其对CPU的消耗最小并且在各种不同部署环境中能提供更加一致的性能表现；
    //SleepingWaitStrategy 的性能表现跟 BlockingWaitStrategy 差不多，对 CPU 的消耗也类似，但其对生产者线程的影响最小，适合用于异步日志类似的场景；
    //YieldingWaitStrategy 的性能是最好的，适合用于低延迟的系统.在要求极高性能且事件处理线数小于 CPU 逻辑核心数的场景中，推荐使用此策略；例如，CPU开启超线程的特性。
    private WaitStrategy waitStrategy = new TimeoutBlockingWaitStrategy(10l, TimeUnit.MILLISECONDS);
    private EventFactory eventFactory = new MessageEventFactory();
    private MessageExceptionHandler exceptionHandler = new MessageExceptionHandler();
    private ProducerType producerType = ProducerType.MULTI;
    public String name = "jsm";


    public JsmDisruptor(EventHandler<MessageEvent<T>> eventEventHandler) {
        this.eventEventHandler = eventEventHandler;
        init();
    }

    public JsmDisruptor(EventHandler<MessageEvent<T>> eventEventHandler, int bufferSize) {
        this.eventEventHandler = eventEventHandler;
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }
        init();
    }

    public JsmDisruptor(EventHandler<MessageEvent<T>> eventEventHandler, int bufferSize, WaitStrategy waitStrategy) {
        this.eventEventHandler = eventEventHandler;
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }
        if (waitStrategy != null) {
            this.waitStrategy = waitStrategy;
        }
        init();
    }

    public JsmDisruptor(EventHandler<MessageEvent<T>> eventEventHandler, int bufferSize, WaitStrategy waitStrategy, ProducerType producerType) {
        this.eventEventHandler = eventEventHandler;
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }
        if (waitStrategy != null) {
            this.waitStrategy = waitStrategy;
        }
        if (producerType != null) {
            this.producerType = producerType;
        }
        init();
    }

    public class MessageEvent<T> {
        public T message;
    }

    public class MessageEventFactory implements EventFactory<MessageEvent> {
        @Override
        public MessageEvent newInstance() {
            return new MessageEvent();
        }
    }

    public class MessageEventTranslator implements EventTranslatorOneArg<MessageEvent<T>, T> {
        @Override
        public void translateTo(MessageEvent<T> messageEvent, long sequence, T message) {
            messageEvent.message = message;
        }
    }

    public class MessageThreadFactory implements ThreadFactory {
        int index = 0;

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "drp-" + name + "-" + (index < Integer.MAX_VALUE - 1 ? (++index) : 0));
        }
    }

    public class MessageExceptionHandler implements ExceptionHandler<MessageEvent<T>> {
        @Override
        public void handleEventException(Throwable ex, long sequence, MessageEvent<T> event) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JSM_DISRUPTOR, name + ",handleEventException:" + ex.getMessage()));
            ex.printStackTrace();
        }

        @Override
        public void handleOnStartException(Throwable ex) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JSM_DISRUPTOR, name + ",handleOnStartException:" + ex.getMessage()));
            ex.printStackTrace();
        }

        @Override
        public void handleOnShutdownException(Throwable ex) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JSM_DISRUPTOR, name + ",handleOnShutdownException:" + ex.getMessage()));
            ex.printStackTrace();
        }
    }

    private void init() {
        if (this.eventEventHandler == null) {
            throw new RuntimeException("eventEventHandler can't be null.");
        }
        disruptor = new Disruptor<>(eventFactory, bufferSize, new MessageThreadFactory(), producerType, waitStrategy);
        disruptor.handleEventsWith(eventEventHandler);
        disruptor.setDefaultExceptionHandler(exceptionHandler);
        disruptor.start();
    }

    public void shutdown() {
        if (disruptor != null) {
            disruptor.shutdown();
        }
    }

    public void pushMessage(T message) {
        pushMessage(message, true);
    }

    public void pushMessage(T message, boolean useTryPub) {
        if (disruptor != null) {
//            disruptor.publishEvent(translator, message);
            if (disruptor.getRingBuffer().remainingCapacity() < bufferSize * 0.01) {
                logger.warn(name + " ringBuffer remain: " + disruptor.getRingBuffer().remainingCapacity());
            }
            if (useTryPub) {
                disruptor.getRingBuffer().tryPublishEvent(translator, message);
            } else {
                disruptor.getRingBuffer().publishEvent(translator, message);
            }
        } else {
            logger.error("pushMessage error. ringBuffer is null!");
        }
    }
}
