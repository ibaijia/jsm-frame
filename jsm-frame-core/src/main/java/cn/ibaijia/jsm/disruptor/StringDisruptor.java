package cn.ibaijia.jsm.disruptor;

import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class StringDisruptor {
    private static Logger logger = StatusLogger.getLogger();
    private EventTranslatorOneArg<MessageEvent, String> translator = new MessageEventTranslator();
    private Disruptor<MessageEvent> disruptor;
    private Processor<MessageEvent> processor;
    private int bufferSize = 1024;//必须是2的N次方
    //BlockingWaitStrategy 是最低效的策略，但其对CPU的消耗最小并且在各种不同部署环境中能提供更加一致的性能表现；
    //SleepingWaitStrategy 的性能表现跟 BlockingWaitStrategy 差不多，对 CPU 的消耗也类似，但其对生产者线程的影响最小，适合用于异步日志类似的场景；
    //YieldingWaitStrategy 的性能是最好的，适合用于低延迟的系统.在要求极高性能且事件处理线数小于 CPU 逻辑核心数的场景中，推荐使用此策略；例如，CPU开启超线程的特性。
    private WaitStrategy waitStrategy = new TimeoutBlockingWaitStrategy(10l, TimeUnit.MILLISECONDS);;
    private ProducerType producerType = ProducerType.MULTI;

    public StringDisruptor(Processor<MessageEvent> processor) {
        this.processor = processor;
        init();
    }

    public StringDisruptor(Processor<MessageEvent> processor, int bufferSize) {
        this.processor = processor;
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }
        init();
    }

    public StringDisruptor(Processor<MessageEvent> processor, int bufferSize, WaitStrategy waitStrategy) {
        this.processor = processor;
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }
        if (waitStrategy != null) {
            this.waitStrategy = waitStrategy;
        }
        init();
    }

    public StringDisruptor(Processor<MessageEvent> processor, int bufferSize, WaitStrategy waitStrategy, ProducerType producerType) {
        this.processor = processor;
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }
        if (waitStrategy != null) {
            this.waitStrategy = waitStrategy;
        }
        if (producerType != null) {
            this.producerType = producerType;
        }
        init();
    }

    public class MessageEvent {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class MessageEventFactory implements EventFactory<MessageEvent> {
        @Override
        public MessageEvent newInstance() {
            return new MessageEvent();
        }
    }

    public class MessageEventTranslator implements EventTranslatorOneArg<MessageEvent, String> {
        @Override
        public void translateTo(MessageEvent messageEvent, long sequence, String message) {
            messageEvent.setMessage(message);
        }
    }

    public class MessageThreadFactory implements ThreadFactory {
        int index = 0;

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "jsm-str-drp-" + (index < Integer.MAX_VALUE - 1 ? (++index) : 0));
        }
    }

    public class MessageEventHandler implements EventHandler<MessageEvent> {
        @Override
        public void onEvent(MessageEvent messageEvent, long sequence, boolean endOfBatch) throws Exception {
            processor.process(messageEvent, sequence, endOfBatch);
        }
    }

    public class MessageExceptionHandler implements ExceptionHandler<MessageEvent> {
        @Override
        public void handleEventException(Throwable ex, long sequence, MessageEvent event) {
            ex.printStackTrace();
        }

        @Override
        public void handleOnStartException(Throwable ex) {
            ex.printStackTrace();
        }

        @Override
        public void handleOnShutdownException(Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void init() {
        if (this.processor == null) {
            throw new RuntimeException("processor can't be null.");
        }
        disruptor = new Disruptor<>(new MessageEventFactory(), bufferSize, new MessageThreadFactory(), producerType, waitStrategy);
        disruptor.handleEventsWith(new MessageEventHandler());
        disruptor.setDefaultExceptionHandler(new MessageExceptionHandler());
        disruptor.start();
    }

    public void shutdown() {
        if (disruptor != null) {
            disruptor.shutdown();
        }
    }

    public void pushMessage(String message) {
        if (disruptor != null) {
            disruptor.publishEvent(translator, message);
        } else {
            logger.error("pushMessage error. ringBuffer is null!");
        }
    }
}
