package cn.ibaijia.jsm.disruptor;

public interface Processor<T> {
    void process(T event, long sequence, boolean endOfBatch);
}
