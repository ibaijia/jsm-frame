package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.AppContext;
import org.slf4j.Logger;

import java.net.InetAddress;

/**
 * @author longzl
 */
public class IpUtil {

    private static Logger logger = LogUtil.log(IpUtil.class);

    private String ENABLE_CMD = "iptables -D INPUT -s %s -j DROP";
    private String DISABLE_CMD = "iptables -I INPUT -s %s -j DROP";

    private static String hostIp;
    private static String hostName;

    public static String getHostIp() {
        if (hostIp == null) {
            try {
                InetAddress addr = InetAddress.getLocalHost();
                hostIp = addr.getHostAddress();
            } catch (Exception e) {
                return null;
            }
        }
        return hostIp;
    }

    public static String getHostName() {
        if (hostName == null) {
            try {
                InetAddress addr = InetAddress.getLocalHost();
                hostName = addr.getHostName();
            } catch (Exception e) {
                return null;
            }
        }
        return hostName;
    }

    public static boolean checkRemoteIp(String remoteIp, String contextKey) {
        String ips = AppContext.get(contextKey);
        if (StringUtil.isEmpty(ips)) {
            return false;
        }
        return ips.contains(remoteIp);
    }

    /**
     * 禁IP
     *
     * @param ip
     */
    public boolean disableIp(String ip) {
        return checkAndExec(ip, DISABLE_CMD);
    }

    /**
     * 解禁IP
     *
     * @param ip
     */
    public boolean enableIp(String ip) {
        return checkAndExec(ip, ENABLE_CMD);
    }

    private boolean checkAndExec(String ip, String disable_cmd) {
        if (!SystemUtil.IS_OS_UNIX) {
            logger.error("not unix ,can't run shell.");
            return false;
        }
        if (!StringUtil.isIpv4(ip)) {
            logger.error("is not a ip:{}", ip);
            return false;
        }

        String cmd = String.format(disable_cmd, ip);
        ShellUtil.exec(cmd);
        return true;
    }


    public static boolean isInternalIp(String ip) {
        byte[] addr = textToNumericFormatV4(ip);
        return isInternalIp(addr);
    }

    public static byte[] textToNumericFormatV4(String ipStr) {
        byte[] var1 = new byte[4];
        long var2 = 0L;
        int var4 = 0;
        int var5 = ipStr.length();
        if (var5 != 0 && var5 <= 15) {
            for(int var6 = 0; var6 < var5; ++var6) {
                char var7 = ipStr.charAt(var6);
                if (var7 == '.') {
                    if (var2 < 0L || var2 > 255L || var4 == 3) {
                        return null;
                    }

                    var1[var4++] = (byte)((int)(var2 & 255L));
                    var2 = 0L;
                } else {
                    int var8 = Character.digit(var7, 10);
                    if (var8 < 0) {
                        return null;
                    }

                    var2 *= 10L;
                    var2 += (long)var8;
                }
            }

            if (var2 >= 0L && var2 < 1L << (4 - var4) * 8) {
                switch(var4) {
                    case 0:
                        var1[0] = (byte)((int)(var2 >> 24 & 255L));
                    case 1:
                        var1[1] = (byte)((int)(var2 >> 16 & 255L));
                    case 2:
                        var1[2] = (byte)((int)(var2 >> 8 & 255L));
                    case 3:
                        var1[3] = (byte)((int)(var2 >> 0 & 255L));
                    default:
                        return var1;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static boolean isInternalIp(byte[] addr) {
        final byte b0 = addr[0];
        final byte b1 = addr[1];
        //10.x.x.x/8
        final byte SECTION_1 = 0x0A;
        //172.16.x.x/12
        final byte SECTION_2 = (byte) 0xAC;
        final byte SECTION_3 = (byte) 0x10;
        final byte SECTION_4 = (byte) 0x1F;
        //192.168.x.x/16
        final byte SECTION_5 = (byte) 0xC0;
        final byte SECTION_6 = (byte) 0xA8;
        switch (b0) {
            case SECTION_1:
                return true;
            case SECTION_2:
                if (b1 >= SECTION_3 && b1 <= SECTION_4) {
                    return true;
                }
            case SECTION_5:
                switch (b1) {
                    case SECTION_6:
                        return true;
                }
            default:
                return false;

        }
    }

    public static Integer ipToInt(String ipStr) {
        String[] ip = ipStr.split("\\.");
        return (Integer.parseInt(ip[0]) << 24) + (Integer.parseInt(ip[1]) << 16) + (Integer.parseInt(ip[2]) << 8) + Integer.parseInt(ip[3]);
    }

    public static String intToIp(int intIp) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.valueOf(intIp >> 24) + ".");
        builder.append(String.valueOf((intIp & 0x00FFFFFF) >> 16) + ".");
        builder.append(String.valueOf((intIp & 0x0000FFFF) >> 8) + ".");
        builder.append(String.valueOf((intIp & 0x000000FF)));
        return builder.toString();
    }

    public static String ipToHex(String ipStr) {
        if (StringUtil.isEmpty(ipStr)) {
            return ipStr;
        }
        String[] ip = ipStr.split("\\.");
        if (ip.length != 4) {
            return ipStr;
        }
        StringBuilder sb = new StringBuilder();
        String res1 = Integer.toHexString(Integer.parseInt(ip[0]));
        String res2 = Integer.toHexString(Integer.parseInt(ip[1]));
        String res3 = Integer.toHexString(Integer.parseInt(ip[2]));
        String res4 = Integer.toHexString(Integer.parseInt(ip[3]));
        sb.append(res1.length() < 2 ? ("0" + res1) : res1);
        sb.append(res2.length() < 2 ? ("0" + res2) : res2);
        sb.append(res3.length() < 2 ? ("0" + res3) : res3);
        sb.append(res4.length() < 2 ? ("0" + res4) : res4);
        return sb.toString();
    }

    public static String hexToIp(String hexStr) {
        if (hexStr.length() != 8) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.parseInt(hexStr.substring(0, 2), 16)).append(".");
        sb.append(Integer.parseInt(hexStr.substring(2, 4), 16)).append(".");
        sb.append(Integer.parseInt(hexStr.substring(4, 6), 16)).append(".");
        sb.append(Integer.parseInt(hexStr.substring(6, 8), 16));
        return sb.toString();
    }


}
