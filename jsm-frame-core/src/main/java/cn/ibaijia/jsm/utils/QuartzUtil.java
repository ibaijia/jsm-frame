package cn.ibaijia.jsm.utils;

import org.quartz.*;
import org.slf4j.Logger;

public class QuartzUtil {
    private static Logger logger = LogUtil.log(QuartzUtil.class);
    private static SchedulerFactory schedulerFactory;

    public static void setSchedulerFactory(SchedulerFactory schedulerFactory) {
        QuartzUtil.schedulerFactory = schedulerFactory;
    }

    public static boolean addJob(Class<? extends Job> jobClass, String cron) {
        String jobName = jobClass.getSimpleName()+"_Job";
        String triggerName = jobClass.getSimpleName()+"_Trigger";
        return addJob(jobName, null, triggerName, null, jobClass, cron);
    }

    public static boolean addJob(String jobName, String triggerName, Class<? extends Job> jobClass, String cron) {
        return addJob(jobName, null, triggerName, null, jobClass, cron);
    }

    /**
     * 添加一个定时任务
     *
     * @param jobName          任务名
     * @param jobGroupName     任务组名
     * @param triggerName      触发器名
     * @param triggerGroupName 触发器组名
     * @param jobClass         任务
     * @param cron             时间设置，参考quartz说明文档
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static boolean addJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName, Class<? extends Job> jobClass, String cron) {
        boolean res = true;
        try {
            if(schedulerFactory == null){
                logger.error("schedulerFactory is null.");
                return false;
            }
            Scheduler sched = schedulerFactory.getScheduler();

            // 任务名，任务组，任务执行类
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, jobGroupName).build();

            if (sched.checkExists(jobDetail.getKey())) {
                logger.error("job key exists!");
                return false;
            }

            // 触发器  
            TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
            // 触发器名,触发器组  
            triggerBuilder.withIdentity(triggerName, triggerGroupName);
            triggerBuilder.startNow();
            // 触发器时间设定  
            triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));
            // 创建Trigger对象
            CronTrigger trigger = (CronTrigger) triggerBuilder.build();

            // 调度容器设置JobDetail和Trigger
            sched.scheduleJob(jobDetail, trigger);
            // 启动
            if (!sched.isShutdown()) {
                sched.start();
            }
        } catch (Exception e) {
            logger.error("addJob error!", e);
            res = false;
        }
        return res;
    }

    /**
     * 修改一个任务的触发时间
     *
     * @param jobName
     * @param jobGroupName
     * @param triggerName      触发器名
     * @param triggerGroupName 触发器组名
     * @param cron             时间设置，参考quartz说明文档
     */
    public static boolean modifyJobTime(String jobName, String jobGroupName, String triggerName, String triggerGroupName, String cron) {
        boolean res = true;
        try {
            if(schedulerFactory == null){
                logger.error("schedulerFactory is null.");
                return false;
            }
            Scheduler sched = schedulerFactory.getScheduler();
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
            CronTrigger trigger = (CronTrigger) sched.getTrigger(triggerKey);
            if (trigger == null) {
                return false;
            }

            String oldTime = trigger.getCronExpression();
            if (!oldTime.equalsIgnoreCase(cron)) {
                /** 方式一 ：调用 rescheduleJob 开始 */
                // 触发器  
                TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
                // 触发器名,触发器组  
                triggerBuilder.withIdentity(triggerName, triggerGroupName);
                triggerBuilder.startNow();
                // 触发器时间设定  
                triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));
                // 创建Trigger对象
                trigger = (CronTrigger) triggerBuilder.build();
                // 方式一 ：修改一个任务的触发时间
                sched.rescheduleJob(triggerKey, trigger);
                /** 方式一 ：调用 rescheduleJob 结束 */

                /** 方式二：先删除，然后在创建一个新的Job  */
                //JobDetail jobDetail = sched.getJobDetail(JobKey.jobKey(jobName, jobGroupName));  
                //Class<? extends Job> jobClass = jobDetail.getJobClass();  
                //removeJob(jobName, jobGroupName, triggerName, triggerGroupName);  
                //addJob(jobName, jobGroupName, triggerName, triggerGroupName, jobClass, cron); 
                /** 方式二 ：先删除，然后在创建一个新的Job */
            }
        } catch (Exception e) {
            logger.error("modifyJobTime error!", e);
            res = false;
        }
        return res;
    }

    /**
     * 移除一个任务
     *
     * @param jobName
     * @param jobGroupName
     * @param triggerName
     * @param triggerGroupName
     */
    public static boolean removeJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName) {
        boolean res = true;
        try {
            if(schedulerFactory == null){
                logger.error("schedulerFactory is null.");
                return false;
            }
            Scheduler sched = schedulerFactory.getScheduler();

            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);

            sched.pauseTrigger(triggerKey);// 停止触发器  
            sched.unscheduleJob(triggerKey);// 移除触发器  
            sched.deleteJob(JobKey.jobKey(jobName, jobGroupName));// 删除任务  
        } catch (Exception e) {
            logger.error("removeJob error!", e);
            res = false;
        }
        return res;
    }

    /**
     * 启动所有定时任务
     */
    public static boolean startJobs() {
        boolean res = true;
        try {
            if(schedulerFactory == null){
                logger.error("schedulerFactory is null.");
                return false;
            }
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
        } catch (Exception e) {
            logger.error("startJobs error!", e);
            res = false;
        }
        return res;
    }

    /**
     * 关闭所有定时任务
     */
    public static boolean shutdownJobs() {
        boolean res = true;
        try {
            if(schedulerFactory == null){
                logger.error("schedulerFactory is null.");
                return false;
            }
            Scheduler sched = schedulerFactory.getScheduler();
            if (!sched.isShutdown()) {
                sched.shutdown();
            }
        } catch (Exception e) {
            logger.error("shutdownJobs error!", e);
            res = false;
        }
        return res;
    }
}
