package cn.ibaijia.jsm.utils;

import org.apache.logging.log4j.status.StatusLogger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author longzl / @createOn 2011-8-29
 */
public class PropUtil {
    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();

    public static String get(String key, String propFile) {
        Properties prop = getProperties(propFile);
        return prop.getProperty(key);
    }

    public static Integer getAsInteger(String key, String propFile) {
        try {
            String val = get(key, propFile);
            if (!StringUtil.isEmpty(val)) {
                return Integer.valueOf(val);
            } else {
                logger.error("key is empty, key {} in propFile {}", key, propFile);
                return null;
            }
        } catch (Exception e) {
            logger.error("getAsInteger error! key {} in propFile {}", key, propFile);
            return null;
        }
    }

    public static Long getAsLong(String key, String propFile) {
        try {
            String val = get(key, propFile);
            if (!StringUtil.isEmpty(val)) {
                return Long.valueOf(val);
            } else {
                logger.error("key is empty, key {} in propFile {}", key, propFile);
                return null;
            }
        } catch (Exception e) {
            logger.error("getAsLong error! key {} in propFile {}", key, propFile);
            return null;
        }
    }


    public static Boolean getAsBoolean(String key, String propFile) {
        try {
            String val = get(key, propFile);
            if (!StringUtil.isEmpty(val)) {
                return Boolean.valueOf(val);
            } else {
                logger.error("key is empty, key {} in propFile {}", key, propFile);
                return null;
            }
        } catch (Exception e) {
            logger.error("getAsBoolean error! key {} in propFile {}", key, propFile);
            return null;
        }
    }

    public static Properties getProperties(String propFile) {
        Properties prop = new Properties();
        InputStream is = null;
        try {
            is = FileUtil.getResourceStream(propFile);
            prop.load(is);
            is.close();
        } catch (Exception e) {
            logger.error("load properties error! file {}", propFile);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error("close properties file error! file {}", propFile);
                }
            }
        }
        return prop;
    }
}
