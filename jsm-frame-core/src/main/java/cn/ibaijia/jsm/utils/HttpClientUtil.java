package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.http.HttpClient;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;

import java.util.List;


public class HttpClientUtil {

    private static HttpClient httpClient = new HttpClient();

    public static void setProxyHost(HttpHost proxyHost) {
        httpClient.setHttpProxy(proxyHost);
    }

    public static void setSocksProxy(String ip, int port) {
        httpClient.setSocksProxy(ip, port);
    }

    public static void setSocksProxy(boolean withExtra) {
        httpClient.setWithExtra(withExtra);
    }

    public static String get(String url) {
        return httpClient.get(url);
    }

    public static String get(List<Header> headers, String url) {
        return httpClient.get(headers, url);
    }

    public static String post(String url, List<NameValuePair> nvps) {
        return httpClient.post(url, nvps);
    }

    public static String post(List<Header> headers, String url, List<NameValuePair> nvps) {
        return httpClient.post(headers, url, nvps);
    }

    public static String post(String url, Object data) {
        return httpClient.post(url, data);
    }

    public static String post(String url, String strEntity) {
        return httpClient.post(url, strEntity);
    }

    public static String post(List<Header> headers, String url, Object data) {
        return httpClient.post(headers, url, data);
    }

    public static String post(List<Header> headers, String url, String strEntity) {
        return httpClient.post(headers, url, strEntity);
    }

    public static String put(String url, Object data) {
        return httpClient.put(url, data);
    }

    public static String put(List<Header> headers, String url, String strEntity) {
        return httpClient.put(headers, url, strEntity);
    }

    public static String delete(String url) {
        return httpClient.delete(url);
    }

    public static String delete(List<Header> headers, String url) {
        return httpClient.delete(headers, url);
    }

}