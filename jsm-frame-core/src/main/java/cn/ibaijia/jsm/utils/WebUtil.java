package cn.ibaijia.jsm.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import cn.ibaijia.jsm.consts.BasePermissionConstants;
import cn.ibaijia.jsm.context.WebContext;

public class WebUtil {
	private static Logger logger = LogUtil.log(WebUtil.class);
	
	public static String genToken(String username){
		return EncryptUtil.md5(username+Math.random()+System.nanoTime());
	}

	public static boolean hasPermissions(String permissionsStr) {
		List<String> permissionList = WebContext.currentPermissions() ;
		if(permissionList == null){
			logger.error("user have no permission!");
			return false;
		}
		if(permissionList.contains(BasePermissionConstants.SUPER_ADMIN)){
			return true;
		}
		Boolean resList = false;
		String elsStr = null;
		try {
			ExpressionParser parser = new SpelExpressionParser();
			EvaluationContext context = new StandardEvaluationContext();
			context.setVariable("permissions", permissionList);
			elsStr = createExpression(permissionsStr);
			resList = parser.parseExpression(elsStr).getValue(context,Boolean.class);
		} catch (Exception e) {
			logger.error("permissionsStr {},elsStr {} ", permissionsStr,elsStr);
//			logger.error("permissionVerify error!permissionsStr", e);
		}
		return resList;
	}
	
	private static String createExpression(String permissions) {
		//"#permissions?.contains(1) || #permissions?.contains('4')"
		String pattern = "([0-9a-zA-Z_-]+)";
		Pattern r = Pattern.compile(pattern);
		// 现在创建 matcher 对象
		Matcher m = r.matcher(permissions);
		String s = m.replaceAll("#permissions?.contains('$1')");
		return s;
	}
	
	public static void main(String[] args) {
		String str = "1002|BOOK_MANAGEMENT_BREAK|BOOK_MANAGEMENT_SHORT|BOOK_MANAGEMENT_SUPER_SHORT|BOOK_MANAGEMENT_SERVICE";
		String esStr = createExpression(str);
		System.out.println(esStr);
	
	}
}
