package cn.ibaijia.jsm.utils;

/**
 *@author longzl / @createOn 2010-10-29
 *@desc 
 */
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.slf4j.Logger;


/**
 * Utilities methods for image manipulation. It does not support writting of GIF
 * images, but it can read from. GIF images will be saved as PNG.
 * 
 * @author Rafael Steil
 * @version $Id: ImageUtils.main,v 1.22 2007/07/28 14:17:12 rafaelsteil Exp $
 */
public class ImageUtil {
	
	private static Logger logger = LogUtil.log(ImageUtil.class);

	public static final String IMAGE_JPEG = "jpg";

	public static final String IMAGE_PNG = "png";

	public static final String IMAGE_GIF = "gif";
	private static final String JPG_HEX = "ff";
	private static final String PNG_HEX = "89";
	

	/**
	 * Resizes an image
	 * 
	 * @param imgName
	 *            The image name to resize. Must be the complet path to the file
	 * @param ext
	 *            int
	 * @param maxWidth
	 *            The image's max width
	 * @param maxHeight
	 *            The image's max height
	 * @return A resized <code>BufferedImage</code>
	 * @throws IOException
	 */
	public static BufferedImage resizeImage(String imgName, String ext, int maxWidth,	int maxHeight) throws IOException {
		return resizeImage(ImageIO.read(new File(imgName)), ext, maxWidth, maxHeight);
	}

	/**
	 * Resizes an image.
	 * 
	 * @param image
	 *            The image to resize
	 * @param maxWidth
	 *            The image's max width
	 * @param maxHeight
	 *            The image's max height
	 * @return A resized <code>BufferedImage</code>
	 * @param ext
	 *            int
	 */
	public static BufferedImage resizeImage(BufferedImage image, String ext, int maxWidth, int maxHeight) {
		Dimension largestDimension = new Dimension(maxWidth, maxHeight);

		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);

		float aspectRation = (float) imageWidth / imageHeight;

		if (imageWidth > maxWidth || imageHeight > maxHeight) {
			if ((float) largestDimension.width / largestDimension.height > aspectRation) {
				largestDimension.width = (int) Math.ceil(largestDimension.height * aspectRation);
			} else {
				largestDimension.height = (int) Math.ceil(largestDimension.width / aspectRation);
			}

			imageWidth = largestDimension.width;
			imageHeight = largestDimension.height;
		}

		return createHeadlessSmoothBufferedImage(image, ext, imageWidth, imageHeight);
	}

	/**
	 * Saves an image to the disk.
	 * 
	 * @param image
	 *            The image to save
	 * @param toFileName
	 *            The filename to use
	 * @param ext
	 *            The image type. Use <code>ImageUtils.IMAGE_JPEG</code> to save
	 *            as JPEG images, or <code>ImageUtils.IMAGE_PNG</code> to save
	 *            as PNG.
	 * @return <code>false</code> if no appropriate writer is found
	 * @throws IOException
	 */
	public static boolean saveImage(BufferedImage image, String toFileName, String ext) throws IOException {
		return ImageIO.write(image, IMAGE_JPEG.equals(ext) ? "jpg" : "png", new File(toFileName));
	}

	/**
	 * Compress and save an image to the disk. Currently this method only
	 * supports JPEG images.
	 * 
	 * @param image
	 *            The image to save
	 * @param toFileName
	 *            The filename to use
	 * @param ext
	 *            The image type. Use <code>ImageUtils.IMAGE_JPEG</code> to save
	 *            as JPEG images, or <code>ImageUtils.IMAGE_PNG</code> to save
	 *            as PNG.
	 * @throws IOException
	 */
	public static void saveCompressedImage(BufferedImage image, String toFileName, String ext) throws IOException {
		if (IMAGE_PNG.equalsIgnoreCase(ext)) {
			throw new UnsupportedOperationException("PNG compression not implemented");
		}

		Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
		ImageWriter writer;
		writer = iter.next();

		ImageOutputStream ios = ImageIO.createImageOutputStream(new File(toFileName));
		writer.setOutput(ios);

		ImageWriteParam iwparam = new JPEGImageWriteParam(Locale.getDefault());

		iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwparam.setCompressionQuality(0.7F);

		writer.write(null, new IIOImage(image, null, null), iwparam);

		ios.flush();
		writer.dispose();
		ios.close();
	}
	
//	public static void compressImage(String imagePath,float quality) throws IOException {
//		if (IMAGE_PNG.equalsIgnoreCase(getSuffix(imagePath, null))) {
//			throw new UnsupportedOperationException("PNG compression not implemented");
//		}
//		BufferedImage srcImage = ImageIO.read(new File(imagePath));
//		Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(IMAGE_JPEG);
//		ImageWriter writer;
//		writer = iter.next();
//		
//		ImageOutputStream ios = ImageIO.createImageOutputStream(new File(imagePath));
//		writer.setOutput(ios);
//		
//		ImageWriteParam iwparam = new JPEGImageWriteParam(Locale.getDefault());
//		
//		iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//		iwparam.setCompressionQuality(quality);
//		
//		writer.write(null, new IIOImage(srcImage, null, null), iwparam);
//		
//		ios.flush();
//		writer.dispose();
//		ios.close();
//	}
	public static void compressImage(String imagePath,float quality) throws IOException {
		File file = new File(imagePath);
		BufferedImage srcImage = ImageIO.read(new File(imagePath));
		file.delete();
		Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(IMAGE_JPEG);
		ImageWriter writer;
		writer = iter.next();
		
		ImageOutputStream ios = ImageIO.createImageOutputStream(new File(imagePath));
		writer.setOutput(ios);
		
		ImageWriteParam iwparam = new JPEGImageWriteParam(Locale.getDefault());
		
		iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwparam.setCompressionQuality(quality);
		
		writer.write(null, new IIOImage(srcImage, null, null), iwparam);
		
		ios.flush();
		writer.dispose();
		ios.close();
	}
	
	public static String getSuffix(String fileName,String defaultExt){
		if (fileName.contains(".")) {
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		}
		return defaultExt;
	}

	/**
	 * Creates a <code>BufferedImage</code> from an <code>Image</code>. This
	 * method can function on a completely headless system. This especially
	 * includes Linux and Unix systems that do not have the X11 libraries
	 * installed, which are required for the AWT subsystem to operate. This
	 * method uses nearest neighbor approximation, so it's quite fast.
	 * Unfortunately, the result is nowhere near as nice looking as the
	 * createHeadlessSmoothBufferedImage method.
	 * 
	 * @param image
	 *            The image to convert
	 * @param width
	 *            The desired image width
	 * @param height
	 *            The desired image height
	 * @return The converted image
	 * @param ext
	 *            int
	 */
	public static BufferedImage createHeadlessBufferedImage(BufferedImage image, String ext,int width, int height) {
		int type = 0;
		if (IMAGE_PNG.equalsIgnoreCase(ext) && hasAlpha(image)) {
			type = BufferedImage.TYPE_INT_ARGB;
		} else {
			type = BufferedImage.TYPE_INT_RGB;
		}

		BufferedImage bi = new BufferedImage(width, height, type);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				bi.setRGB(x, y, image.getRGB(x * image.getWidth() / width, y
						* image.getHeight() / height));
			}
		}

		return bi;
	}

	/**
	 * Creates a <code>BufferedImage</code> from an <code>Image</code>. This
	 * method can function on a completely headless system. This especially
	 * includes Linux and Unix systems that do not have the X11 libraries
	 * installed, which are required for the AWT subsystem to operate. The
	 * resulting image will be smoothly scaled using bilinear filtering.
	 * 
	 * @param source
	 *            The image to convert
	 * @param width
	 *            The desired image width
	 * @param height
	 *            The desired image height
	 * @return The converted image
	 * @param ext
	 *            int
	 */
	public static BufferedImage createHeadlessSmoothBufferedImage(BufferedImage source,	String ext, int width, int height) {
		int type = 0;
		if (IMAGE_PNG.equalsIgnoreCase(ext) && hasAlpha(source)) {
			type = BufferedImage.TYPE_INT_ARGB;
		} else {
			type = BufferedImage.TYPE_INT_RGB;
		}

		BufferedImage dest = new BufferedImage(width, height, type);

		int sourcex;
		int sourcey;

		double scalex = (double) width / source.getWidth();
		double scaley = (double) height / source.getHeight();

		int x1;
		int y1;

		double xdiff;
		double ydiff;

		int rgb;
		int rgb1;
		int rgb2;

		for (int y = 0; y < height; y++) {
			sourcey = y * source.getHeight() / dest.getHeight();
			ydiff = scale(y, scaley) - sourcey;

			for (int x = 0; x < width; x++) {
				sourcex = x * source.getWidth() / dest.getWidth();
				xdiff = scale(x, scalex) - sourcex;

				x1 = Math.min(source.getWidth() - 1, sourcex + 1);
				y1 = Math.min(source.getHeight() - 1, sourcey + 1);

				rgb1 = getRGBInterpolation(source.getRGB(sourcex, sourcey), source.getRGB(x1,
						sourcey), xdiff);
				rgb2 = getRGBInterpolation(source.getRGB(sourcex, y1), source.getRGB(x1, y1),
						xdiff);

				rgb = getRGBInterpolation(rgb1, rgb2, ydiff);

				dest.setRGB(x, y, rgb);
			}
		}

		return dest;
	}

	private static double scale(int point, double scale) {
		return point / scale;
	}

	private static int getRGBInterpolation(int value1, int value2, double distance) {
		int alpha1 = (value1 & 0xFF000000) >>> 24;
		int red1 = (value1 & 0x00FF0000) >> 16;
		int green1 = (value1 & 0x0000FF00) >> 8;
		int blue1 = (value1 & 0x000000FF);

		int alpha2 = (value2 & 0xFF000000) >>> 24;
		int red2 = (value2 & 0x00FF0000) >> 16;
		int green2 = (value2 & 0x0000FF00) >> 8;
		int blue2 = (value2 & 0x000000FF);

		int rgb = ((int) (alpha1 * (1.0 - distance) + alpha2 * distance) << 24)
				| ((int) (red1 * (1.0 - distance) + red2 * distance) << 16)
				| ((int) (green1 * (1.0 - distance) + green2 * distance) << 8)
				| (int) (blue1 * (1.0 - distance) + blue2 * distance);

		return rgb;
	}

	/**
	 * Determines if the image has transparent pixels.
	 * 
	 * @param image
	 *            The image to check for transparent pixel.s
	 * @return <code>true</code> of <code>false</code>, according to the result
	 */
	public static boolean hasAlpha(Image image) {
		try {
			PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
			pg.grabPixels();

			return pg.getColorModel().hasAlpha();
		} catch (InterruptedException e) {
			return false;
		}
	}
	
    /**
     * 添加图片水印
     * @param targetImg 目标图片路径，如：C://myPictrue//1.jpg
     * @param waterImg  水印图片路径，如：C://myPictrue//logo.png
     * @param x 水印图片距离目标图片左侧的偏移量，如果x<0, 则在正中间
     * @param y 水印图片距离目标图片上侧的偏移量，如果y<0, 则在正中间
     * @param alpha 透明度(0.0 -- 1.0, 0.0为完全透明，1.0为完全不透明)
*/
    public final static void pressImage(String targetImg, String waterImg, int x, int y, float alpha) {
            try {
            	String ext = getSuffix(targetImg, IMAGE_JPEG);
                File file = new File(targetImg);
                Image image = ImageIO.read(file);
                int width = image.getWidth(null);
                int height = image.getHeight(null);
                BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = bufferedImage.createGraphics();
                g.drawImage(image, 0, 0, width, height, null);
            
                Image waterImage = ImageIO.read(new File(waterImg));    // 水印文件
                int width_1 = waterImage.getWidth(null);
                int height_1 = waterImage.getHeight(null);
                g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));
                
                int widthDiff = width - width_1;
                int heightDiff = height - height_1;
                if(x < 0){
                    x = widthDiff / 2;
                }else if(x > widthDiff){
                    x = widthDiff;
                }
                if(y < 0){
                    y = heightDiff / 2;
                }else if(y > heightDiff){
                    y = heightDiff;
                }
                g.drawImage(waterImage, x, y, width_1, height_1, null); // 水印文件结束
                g.dispose();
                ImageIO.write(bufferedImage, ext, file);
            } catch (IOException e) {
                logger.error("", e);
            }
    }

    /**
     * 添加文字水印
     * @param targetImg 目标图片路径，如：C://myPictrue//1.jpg
     * @param pressText 水印文字， 如：中国证券网
     * @param fontName 字体名称，    如：宋体
     * @param fontStyle 字体样式，如：粗体和斜体(Font.BOLD|Font.ITALIC)
     * @param fontSize 字体大小，单位为像素
     * @param color 字体颜色
     * @param x 水印文字距离目标图片左侧的偏移量，如果x<0, 则在正中间
     * @param y 水印文字距离目标图片上侧的偏移量，如果y<0, 则在正中间
     * @param alpha 透明度(0.0 -- 1.0, 0.0为完全透明，1.0为完全不透明)
*/
    public static void pressText(String targetImg, String pressText, String fontName, int fontStyle, int fontSize, Color color, int x, int y, float alpha) {
        try {
        	String ext = getSuffix(targetImg, IMAGE_JPEG);
            File file = new File(targetImg);
            Image image = ImageIO.read(file);
            int width = image.getWidth(null);
            int height = image.getHeight(null);
            BufferedImage bufferedImage = new BufferedImage(width-500, height-400, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImage.createGraphics();
            g.drawImage(image, -250, -200, width, height, null);
            g.setFont(new Font(fontName, fontStyle, fontSize));
            g.setColor(color);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));
            
            int width_1 = fontSize * getLength(pressText);
            int height_1 = fontSize;
            int widthDiff = width - width_1;
            int heightDiff = height - height_1;
            if(x < 0){
                x = widthDiff / 2;
            }else if(x > widthDiff){
                x = widthDiff;
            }
            if(y < 0){
                y = heightDiff / 2;
            }else if(y > heightDiff){
                y = heightDiff;
            }
            
            g.drawString(pressText, x, y + height_1);
            g.dispose();
            ImageIO.write(bufferedImage, ext, file);
        } catch (Exception e) {
        	 logger.error("", e);
        }
    }
    /**
     * 获取字符长度，一个汉字作为 1 个字符, 一个英文字母作为 0.5 个字符
     * @param text
     * @return 字符长度，如：text="中国",返回 2；text="test",返回 2；text="中国ABC",返回 4.
*/
    public static int getLength(String text) {
        int textLength = text.length();
        int length = textLength;
        for (int i = 0; i < textLength; i++) {
            if (String.valueOf(text.charAt(i)).getBytes().length > 1) {
                length++;
            }
        }
        return (length % 2 == 0) ? length / 2 : length / 2 + 1;
    }
    /**
     * 图片缩放
     * @param filePath 图片路径
     * @param height 高度
     * @param width 宽度
     * @param bb 比例不对时是否需要补白
*/
    public static void resize(String filePath, int height, int width, boolean bb) {
        try {
            double ratio = 0; //缩放比例    
            File f = new File(filePath);   
            BufferedImage bi = ImageIO.read(f);
            Image itemp = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);   
            //计算比例   
            if ((bi.getHeight() > height) || (bi.getWidth() > width)) {   
                if (bi.getHeight() > bi.getWidth()) {   
                    ratio = (new Integer(height)).doubleValue() / bi.getHeight();   
                } else {   
                    ratio = (new Integer(width)).doubleValue() / bi.getWidth();   
                }   
                AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(ratio, ratio), null);   
                itemp = op.filter(bi, null);   
            }   
            if (bb) {   
                BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);   
                Graphics2D g = image.createGraphics();   
                g.setColor(Color.white);   
                g.fillRect(0, 0, width, height);   
                if (width == itemp.getWidth(null))   
                    g.drawImage(itemp, 0, (height - itemp.getHeight(null)) / 2, itemp.getWidth(null), itemp.getHeight(null), Color.white, null);   
                else  
                    g.drawImage(itemp, (width - itemp.getWidth(null)) / 2, 0, itemp.getWidth(null), itemp.getHeight(null), Color.white, null);   
                g.dispose();   
                itemp = image;   
            }
            ImageIO.write((BufferedImage) itemp, IMAGE_JPEG, f);   
        } catch (IOException e) {
            logger.error("", e);
        }
    }
    
    public static void scaleByWidth(String filePath, int newWidth) {
        try {
            double ratio = 0; //缩放比例    
            File f = new File(filePath);   
            BufferedImage bi = ImageIO.read(f);
            int width = bi.getWidth(null);
            int height = bi.getHeight(null);
            Image itemp = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);   
            //计算比例   
            if ((bi.getWidth() > newWidth)) {   
                ratio = (new Integer(newWidth)).doubleValue() / width;
                AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(ratio, ratio), null);   
                itemp = op.filter(bi, null);   
            }   
            ImageIO.write((BufferedImage) itemp, IMAGE_JPEG, f);   
        } catch (IOException e) {
           logger.error("scaleByWidth error!", e);
        }
    }
    
    public static BufferedImage rotate(BufferedImage image, int degree, Color bgcolor) throws IOException {  
    	  
        int iw = image.getWidth();//原始图象的宽度   
        int ih = image.getHeight();//原始图象的高度  
        int w = 0;  
        int h = 0;  
        int x = 0;  
        int y = 0;  
        degree = degree % 360;  
        if (degree < 0)  
            degree = 360 + degree;//将角度转换到0-360度之间  
        double ang = Math.toRadians(degree);//将角度转为弧度  
  
        /** 
         *确定旋转后的图象的高度和宽度 
         */  
  
        if (degree == 180 || degree == 0 || degree == 360) {  
            w = iw;  
            h = ih;  
        } else if (degree == 90 || degree == 270) {  
            w = ih;  
            h = iw;  
        } else {  
            int d = iw + ih;  
            w = (int) (d * Math.abs(Math.cos(ang)));  
            h = (int) (d * Math.abs(Math.sin(ang)));  
        }  
  
        x = (w / 2) - (iw / 2);//确定原点坐标  
        y = (h / 2) - (ih / 2);  
        BufferedImage rotatedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);  
        Graphics2D gs = (Graphics2D)rotatedImage.getGraphics();  
        if(bgcolor==null){  
            rotatedImage  = gs.getDeviceConfiguration().createCompatibleImage(w, h, Transparency.OPAQUE);  
        }else{  
            gs.setColor(bgcolor);  
            gs.fillRect(0, 0, w, h);//以给定颜色绘制旋转后图片的背景  
        }  
          
        AffineTransform at = new AffineTransform();  
        at.rotate(ang, w / 2, h / 2);//旋转图象  
        at.translate(x, y);  
        AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BICUBIC);  
        op.filter(image, rotatedImage);  
        image = rotatedImage;  
          
        return image;  
    }  
    public static InputStream rotateImg(BufferedImage image, int degree, Color bgcolor) throws IOException {  
    	
    	int iw = image.getWidth();//原始图象的宽度   
    	int ih = image.getHeight();//原始图象的高度  
    	int w = 0;  
    	int h = 0;  
    	int x = 0;  
    	int y = 0;  
    	degree = degree % 360;  
    	if (degree < 0)  
    		degree = 360 + degree;//将角度转换到0-360度之间  
    	double ang = Math.toRadians(degree);//将角度转为弧度  
    	
    	/** 
    	 *确定旋转后的图象的高度和宽度 
    	 */  
    	
    	if (degree == 180 || degree == 0 || degree == 360) {  
    		w = iw;  
    		h = ih;  
    	} else if (degree == 90 || degree == 270) {  
    		w = ih;  
    		h = iw;  
    	} else {  
    		int d = iw + ih;  
    		w = (int) (d * Math.abs(Math.cos(ang)));  
    		h = (int) (d * Math.abs(Math.sin(ang)));  
    	}  
    	
    	x = (w / 2) - (iw / 2);//确定原点坐标  
    	y = (h / 2) - (ih / 2);  
    	BufferedImage rotatedImage = new BufferedImage(w, h, image.getType());  
    	Graphics2D gs = (Graphics2D)rotatedImage.getGraphics();  
    	if(bgcolor==null){  
    		rotatedImage  = gs.getDeviceConfiguration().createCompatibleImage(w, h, Transparency.TRANSLUCENT);  
    	}else{  
    		gs.setColor(bgcolor);  
    		gs.fillRect(0, 0, w, h);//以给定颜色绘制旋转后图片的背景  
    	}  
    	
    	AffineTransform at = new AffineTransform();  
    	at.rotate(ang, w / 2, h / 2);//旋转图象  
    	at.translate(x, y);  
    	AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BICUBIC);  
    	op.filter(image, rotatedImage);  
    	image = rotatedImage;  
    	
    	ByteArrayOutputStream  byteOut= new ByteArrayOutputStream();  
    	ImageOutputStream iamgeOut = ImageIO.createImageOutputStream(byteOut);  
    	
    	ImageIO.write(image, "png", iamgeOut);  
    	InputStream  inputStream = new ByteArrayInputStream(byteOut.toByteArray());  
    	
    	return inputStream;  
    }
    
    public static Graphics2D drawAL(int sx, int sy, int ex, int ey, Graphics2D g2){
    	logger.info("drawAL sx:"+sx + "  sy:"+sy+ "  ex:"+ex+ "  ey:"+ey);
		double H = 10; // 箭头高度
		double L = 4; // 底边的一半
		int x3 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		double awrad = Math.atan(L / H); // 箭头角度
		double arraow_len = Math.sqrt(L * L + H * H); // 箭头的长度
		double[] arrXY_1 = rotateVec(ex - sx, ey - sy, awrad, true, arraow_len);
		double[] arrXY_2 = rotateVec(ex - sx, ey - sy, -awrad, true, arraow_len);
		double x_3 = ex - arrXY_1[0]; // (x3,y3)是第一端点
		double y_3 = ey - arrXY_1[1];
		double x_4 = ex - arrXY_2[0]; // (x4,y4)是第二端点
		double y_4 = ey - arrXY_2[1];

		Double X3 = new Double(x_3);
		x3 = X3.intValue();
		Double Y3 = new Double(y_3);
		y3 = Y3.intValue();
		Double X4 = new Double(x_4);
		x4 = X4.intValue();
		Double Y4 = new Double(y_4);
		y4 = Y4.intValue();
		// 画线
		g2.drawLine(sx, sy, ex, ey);
		//
		GeneralPath triangle = new GeneralPath();
		triangle.moveTo(ex, ey);
		triangle.lineTo(x3, y3);
		triangle.lineTo(x4, y4);
		triangle.closePath();
		//实心箭头
		g2.fill(triangle);
		//非实心箭头
//		g2.draw(triangle);
		
		return g2;
	}

	// 计算
	private static double[] rotateVec(int px, int py, double ang,
			boolean isChLen, double newLen) {

		double mathstr[] = new double[2];
		// 矢量旋转函数，参数含义分别是x分量、y分量、旋转角、是否改变长度、新长度
		double vx = px * Math.cos(ang) - py * Math.sin(ang);
		double vy = px * Math.sin(ang) + py * Math.cos(ang);
		if (isChLen) {
			double d = Math.sqrt(vx * vx + vy * vy);
			vx = vx / d * newLen;
			vy = vy / d * newLen;
			mathstr[0] = vx;
			mathstr[1] = vy;
		}
		return mathstr;
	}
	
	public static void addText(String destImage,String text) throws IOException{
		
		File file = new File(destImage);
		Image image = ImageIO.read(file);
		int width = image.getWidth(null);
		int height = image.getHeight(null);
		
		BufferedImage bufferedImage = new BufferedImage(width, height + 20, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bufferedImage.createGraphics();
		g.drawImage(image, 0, 0, width, height, null);
		g.setColor(Color.white);
		g.fillRect(0, height, width, 20);
		g.setColor(Color.red);
		g.setFont(new Font("微软雅黑", Font.BOLD, 16));
		g.drawString(text, 0, height+16);
		g.dispose();
		ImageIO.write(bufferedImage, ImageUtil.IMAGE_JPEG, new File(destImage));
	}
	
	/**
	 * 将图片文件 Base64 String
	 * @param imgFilePath
	 */
	public static String toBase64(String imgFilePath) {
		byte[] data = null;
		// 读取图片字节数组
		InputStream is = null;
		try {
			is = new FileInputStream(imgFilePath);
			data = new byte[is.available()];
			is.read(data);
			is.close();
			return EncryptUtil.base64Encode(data);
		} catch (IOException e) {
			logger.error("", e);
			return null;
		}finally {
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					logger.error("", e);
				}
			}
		}
	}
	
	/**
	 * 字符串进行Base64解码并生成图片
	 * @param imgStr
	 * @param imgFilePath
	 */
	public static boolean fromBase64(String imgStr, String imgFilePath) {
		if (imgStr == null) // 图像数据为空
			return false;
		int headerIdx = imgStr.indexOf("base64,");
		if(headerIdx != -1){
			imgStr = imgStr.substring(headerIdx+7);
		}
		OutputStream out = null;
		try {
			// Base64解码
			byte[] bytes = EncryptUtil.base64Decode(imgStr, "UTF-8");
			for (int i = 0; i < bytes.length; ++i) {
				if (bytes[i] < 0) {// 调整异常数据
					bytes[i] += 256;
				}
			}
			// 生成jpeg图片
			out = new FileOutputStream(imgFilePath);
			out.flush();
			out.write(bytes);
			return true;
		} catch (Exception e) {
			return false;
		}finally {
			if(out != null){
				try {
					out.close();
				} catch (IOException e) {
					logger.error("", e);
				}
			}
		}
	}
	
	public static void cutJPG(InputStream input, OutputStream out, int x, int y, int width, int height){  
        ImageInputStream imageStream = null;  
        try {  
            Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("jpg");  
            ImageReader reader = readers.next();  
            imageStream = ImageIO.createImageInputStream(input);  
            reader.setInput(imageStream, true);  
            ImageReadParam param = reader.getDefaultReadParam();  
            Rectangle rect = new Rectangle(x, y, width, height);  
            param.setSourceRegion(rect);  
            BufferedImage bi = reader.read(0, param);  
            ImageIO.write(bi, "jpg", out); 
        } catch (IOException e) {
        	logger.error("cutJPG error!", e);
        } finally {  
            try {
				imageStream.close();
			} catch (IOException e) {
				logger.error("cutJPG error!", e);
			}  
        }  
    }  
      
    public static void cutPNG(InputStream input, OutputStream out, int x, int y, int width, int height){  
        ImageInputStream imageStream = null;  
        try {  
            Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("png");  
            ImageReader reader = readers.next();  
            imageStream = ImageIO.createImageInputStream(input);  
            reader.setInput(imageStream, true);  
            ImageReadParam param = reader.getDefaultReadParam();  
            Rectangle rect = new Rectangle(x, y, width, height);  
            param.setSourceRegion(rect);  
            BufferedImage bi = reader.read(0, param);  
            ImageIO.write(bi, "png", out);
        } catch (IOException e) {
        	logger.error("cutPNG error!", e);
        } finally {  
            try {
				imageStream.close();
			} catch (IOException e) {
				logger.error("cutPNG error!", e);
			}  
        }  
    }  
      
    public static void cutImage(InputStream input, OutputStream out,int x, int y, int width, int height,String imageType){  
        ImageInputStream imageStream = null;  
        try {
        	logger.debug("imageType:"+imageType);
        	Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName(imageType);  
            if (!readers.hasNext()) {
                return ;
            }
            ImageReader reader = readers.next();  
            imageStream = ImageIO.createImageInputStream(input);  
            reader.setInput(imageStream, true);  
            ImageReadParam param = reader.getDefaultReadParam();  
            Rectangle rect = new Rectangle(x, y, width, height);  
            param.setSourceRegion(rect);  
            BufferedImage bi = reader.read(0, param);  
            ImageIO.write(bi, imageType, out);
        } catch (IOException e) {
        	logger.error("cutImage error!", e);
        } finally {  
            try {
				imageStream.close();
			} catch (IOException e) {
				logger.error("cutImage error!", e);
			}  
        }  
    }
    
    public static String getImageExtension(String imagePath) {
        FileInputStream fis = null;
        String extension = imagePath.substring(imagePath.lastIndexOf('.')+1);
        try {
            fis = new FileInputStream(new File(imagePath));
            byte[] bs = new byte[1];
            fis.read(bs);
            String type = Integer.toHexString(bs[0]&0xFF);
            if(JPG_HEX.equals(type))
                extension = IMAGE_JPEG;
            if(PNG_HEX.equals(type))
                extension = IMAGE_PNG;
        } catch (Exception e) {
        	logger.error("getImageExtension error!", e);
        } finally {
            try {
                if(fis != null)
                    fis.close();
            } catch (IOException e) {
            	logger.error("getImageExtension error!", e);
            }
        }
        return extension;
    }
	
}
