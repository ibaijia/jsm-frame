package cn.ibaijia.jsm.utils;

import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;

import java.io.File;
import java.util.Iterator;

/**
 * @author Administrator
 */
public class XmlUtil {
	private static Logger log = LogUtil.log(XmlUtil.class);
	
	/**
	 * 
	 * @param xmlStr
	 * @return Document
	 */
	public static Document parseText(String xmlStr){
		try {
			return DocumentHelper.parseText(xmlStr);
		} catch (DocumentException e) {
			log.error("parse xml error:"+xmlStr, e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param document
	 * @return String
	 */
	public static String toXml(Document document){
		
		return document.asXML();
	}

	/**
	 * @return Document
	 */
	public static Document createNewDoc(){
		return createNewDoc("utf8");
	}

	/**
	 * @param encode
	 * @return Document
	 */
	public static Document createNewDoc(String encode){
		Document doc = DocumentHelper.createDocument();
		doc.setXMLEncoding(encode);
		return doc;
	}

	/**
	 * @param xmlPath
	 * @return Document
	 */
	public static Document loadXml(String xmlPath){
		SAXReader sax = new SAXReader();
		try {
			return sax.read(new File(xmlPath));
		} catch (DocumentException e) {
			log.error("loadXml erorr:"+xmlPath, e);
			return null;
		}
	}

	/**
	 * @param xmlPath
	 * @return Document
	 */
	public static Document loadClassPathXml(String xmlPath){
		SAXReader sax = new SAXReader();
		try {
//			return sax.read(FileUtil.getResource(xmlPath));
			return sax.read(ClassLoader.getSystemResourceAsStream(xmlPath));
		} catch (DocumentException e) {
			log.error("loadXml erorr:"+xmlPath, e);
			return null;
		}
	}

	public static Element find(Element root , String elementName, String attrName , String attrVal) {
		for (Iterator it = root.elements(elementName).iterator(); it.hasNext();) {
			Element element = (Element) it.next();
			Attribute name = element.attribute(attrName);
			if (name != null) {
				String value = name.getValue();
				if (value != null && attrVal.equals(value))
					return element;
				else
					find(element,elementName, attrName , attrVal);
			}
		}
		return null;
	}
	
	public static void main(String[] args){
		System.out.println(createNewDoc().asXML());
	}
}
