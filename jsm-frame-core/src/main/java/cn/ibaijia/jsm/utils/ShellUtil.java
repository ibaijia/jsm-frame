package cn.ibaijia.jsm.utils;

import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.slf4j.Logger;

import cn.ibaijia.jsm.utils.LogUtil;

public class ShellUtil {

	private static Logger logger = LogUtil.log(ShellUtil.class);
	
	public static String exec(String cmd) {
		logger.info("cmd:"+cmd);
		try {
			String[] cmdA = { "/bin/sh", "-c", cmd };
			Process process = Runtime.getRuntime().exec(cmdA);
			String line;
			StringBuffer sb = new StringBuffer();
			LineNumberReader ebr = new LineNumberReader(new InputStreamReader(process.getErrorStream()));
			while ((line = ebr.readLine()) != null) {
				logger.debug(line);
				sb.append(line).append("\n");
			}
			LineNumberReader br = new LineNumberReader(new InputStreamReader(process.getInputStream()));
			while ((line = br.readLine()) != null) {
				logger.debug(line);
				sb.append(line).append("\n");
			}
			process.waitFor();
			logger.debug("output:"+sb);
			return sb.toString();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}
	
}
