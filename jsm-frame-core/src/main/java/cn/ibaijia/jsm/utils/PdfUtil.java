package cn.ibaijia.jsm.utils;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import org.slf4j.Logger;

import java.io.FileOutputStream;
import java.util.Map;

public class PdfUtil {
    private static Logger logger = LogUtil.log(PdfUtil.class);

    public static boolean formatPdf(String templatePath,String outputPath,Map<String, Object> data){
        BaseFont font = null;
        try {
            font = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            logger.error("createFont STSong-Light,UniGB-UCS2-H error", e);
        }
        return formatPdf(templatePath,outputPath,data,font);
    }

    public static boolean formatPdf(String templatePath,String outputPath,Map<String, Object> data,BaseFont font){
        PdfReader reader = null;
        PdfStamper ps = null;
        FileOutputStream fos = null;
        try {
            reader = new PdfReader(templatePath);
            fos = new FileOutputStream(outputPath);

            /* 将要生成的目标PDF文件名称 */
            ps = new PdfStamper(reader, fos);
            AcroFields fields = ps.getAcroFields();
            ps.setFormFlattening(true);
            for (String key : data.keySet()) {
                String value = String.valueOf(data.get(key));
                if(font != null){
                    fields.setFieldProperty(key, "textfont", font, null);
                }
                fields.setField(key, value); // 为字段赋值,注意字段名称是区分大小写的
            }
            return true;
        } catch (Exception e) {
            logger.error("formatPdf error", e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (reader != null) {
                    reader.close();
                }
                if(fos != null){
                    fos.flush();
                    fos.close();
                }
            } catch (Exception e) {
                logger.error("formatPdf close error", e);
            }
        }
    }
}
