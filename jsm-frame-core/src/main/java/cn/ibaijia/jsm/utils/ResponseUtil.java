package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.rest.resp.*;
import org.apache.http.Header;
import org.slf4j.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class ResponseUtil {
    private static Logger logger = LogUtil.log(ResponseUtil.class);

    public static void outputExcel(HttpServletResponse response, ExcelResp excelResp) {
        logger.info("excelResp:{}", excelResp.fileName);
        response.setHeader("Content-Disposition", "attachment; filename=" + EncryptUtil.urlEncode(excelResp.fileName));
        response.setContentType("application/octet-stream;charset=UTF-8");
//		response.setContentLength(excelResp.workbook);
        if (excelResp.workbook != null) {
            try {
                excelResp.workbook.write(response.getOutputStream());
            } catch (IOException e) {
                logger.error("write workbook error! {}", excelResp.fileName);
            } finally {
                try {
                    excelResp.workbook.close();
                } catch (IOException e) {
                    logger.error("close workbook error! {}", excelResp.fileName);
                }
            }
        }
    }

    public static void outputXml(HttpServletResponse response, XmlResp xmlResp) {
        logger.info("xmlResp:{}", xmlResp.xml);
        PrintWriter pw = null;
        try {
            response.setContentType("text/xml;charset=UTF-8");
            pw = response.getWriter();
            pw.write(xmlResp.xml);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputXml error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputHtml(HttpServletResponse response, HtmlResp htmlResp) {
        logger.info("htmlResp:{}", htmlResp.html);
        PrintWriter pw = null;
        try {
            response.setContentType("text/html;charset=UTF-8");
            pw = response.getWriter();
            pw.write(htmlResp.html);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("htmlResp error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputFreeResp(HttpServletResponse response, FreeResp freeResp) {
        logger.info("freeResp: type:{} content:{}", freeResp.contentType, freeResp.content);
        PrintWriter pw = null;
        try {
            response.setContentType(freeResp.contentType);
            pw = response.getWriter();
            pw.write(freeResp.content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("freeResp error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputText(HttpServletResponse response, TextResp textResp) {
        logger.info("textResp:{}", textResp.text);
        PrintWriter pw = null;
        try {
            response.setContentType("text/plain;charset=UTF-8");
            pw = response.getWriter();
            pw.write(textResp.text);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputText error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputJson(HttpServletResponse response, JsonResp jsonResp) {
        logger.info("jsonResp:{}", jsonResp.jsonStr);
        PrintWriter pw = null;
        try {
            response.setContentType("application/json;charset=UTF-8");
            pw = response.getWriter();
            pw.write(jsonResp.jsonStr);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputJson error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputFile(HttpServletResponse response, FileResp fileResp) {
        logger.info("FileResp:{}", fileResp.fileName);
        response.setHeader("Content-Disposition", "attachment; filename=" + EncryptUtil.urlEncode(fileResp.fileName));
        response.setCharacterEncoding("UTF-8");
        if (fileResp.headers != null) {
            for (Header header : fileResp.headers) {
                response.setHeader(header.getName(), header.getValue());
            }
        }
        ServletOutputStream sos = null;
        InputStream is = null;
        PrintWriter pw = null;
        try {

            if (fileResp.outputObject instanceof String) {
                pw = response.getWriter();
                pw.print((String) fileResp.outputObject);
            } else if (fileResp.outputObject instanceof File) {

            } else if (fileResp.outputObject instanceof InputStream) {
                sos = response.getOutputStream();
                int offset = 0;
                byte[] bt = new byte[1024];
                is = (InputStream) fileResp.outputObject;
                while ((offset = is.read(bt)) != -1) {
                    sos.write(bt, 0, offset);
                }
            } else {//Other Object
                pw = response.getWriter();
                pw.print(StringUtil.toJson(fileResp.outputObject));
            }
        } catch (Exception e) {
            logger.error("outputFile error!" + fileResp.fileName, e);
        } finally {
            try {
                if (sos != null) {
                    sos.flush();
                    sos.close();
                }
                if (is != null) {
                    is.close();
                }
                if (pw != null) {
                    pw.close();
                }
            } catch (IOException e) {
                logger.error("outputFile error!" + fileResp.fileName, e);
            }
        }

    }

    public static void outputRestResp(HttpServletResponse response, RestResp restResp) {
        logger.info("restResp:{}", restResp);
        PrintWriter pw = null;
        try {
            response.setContentType("application/json;charset=UTF-8");
            pw = response.getWriter();
            pw.write(StringUtil.toJson(restResp));
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputRestResp error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }
}
