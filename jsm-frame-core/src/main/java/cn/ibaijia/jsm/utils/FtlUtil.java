package cn.ibaijia.jsm.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.exception.BaseException;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 */
public class FtlUtil {
	
	@SuppressWarnings("deprecation")
	public static String build(Class<?> clazz, String pathPrefix, Object data,String ftlPath) {
		Configuration config = new Configuration();
		config.setClassForTemplateLoading(clazz, pathPrefix);
		return build(data, ftlPath, config);
	}

	@SuppressWarnings("deprecation")
	private static String build(Object data, String ftlPath, Configuration config) {
		try {
			Template template = config.getTemplate(ftlPath);
			template.setEncoding("UTF-8");
			StringWriter writer = new StringWriter();
			template.process(data, writer);
			return writer.toString();
		} catch (Exception e) {
			throw new BaseException("解析ftl模板出错：" + ftlPath, e);
		}
	}

	/**
	 * @param clazz
	 *            :模板文件所在位置相对的类
	 * @param data
	 * @param ftlFilename
	 *            :模板文件名
	 * @param filename
	 *            :绝对文件名
	 */
	@SuppressWarnings("deprecation")
	public static void createFile(Class<?> clazz, String pathPrefix, Object data, String ftlFilename, String filename) {
		Configuration config = new Configuration();
		config.setClassForTemplateLoading(clazz, pathPrefix);
		try {
			config.setEncoding(Locale.getDefault(), "UTF-8");
			Template template = config.getTemplate(ftlFilename);
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FileUtil.createNewFile(filename)), "UTF-8"));
			template.process(data, out);
		} catch (Exception e) {
			throw new BaseException("解析ftl模板出错：" + ftlFilename, e);
		}
	}

	/**
	 * 通过物理路径加载模板
	 * 
	 * @param data
	 *            模板键值对
	 * @param ftlFilename
	 *            模板存放完整路径
	 */
	@SuppressWarnings("deprecation")
	public static String build(Object data, String ftlFilename) {
		Configuration config = new Configuration();
		config.setEncoding(Locale.getDefault(), "UTF-8");
		try {
			config.setTemplateLoader(new FileTemplateLoader(new File(ftlFilename).getParentFile()));
		} catch (IOException e) {
			throw new BaseException("解析ftl模板出错：" + ftlFilename, e);
		}
		return build(data, new File(ftlFilename).getName(), config);
	}
	public static String getTagFtl(String ftlName){
		return WebContext.getRealPath()+"WEB-INF"+File.separator+"tag"+File.separator+ftlName+".ftl";
		//return WebUtil.getContextPath()+"/static/tag"+ftlName+".ftl";
	}
	public static String getFormatInfoFtl(String ftlName){
		return WebContext.getRealPath()+"WEB-INF"+File.separator+"formatInfo"+File.separator+ftlName+".ftl";
		//return WebUtil.getContextPath()+"/static/tag"+ftlName+".ftl";
	}
	public static String getWeiXinFtl(String ftlName){
		return WebContext.getRealPath()+"WEB-INF"+File.separator+"weixin"+File.separator+ftlName+".ftl";
		//return WebUtil.getContextPath()+"/static/tag"+ftlName+".ftl";
	}
}
