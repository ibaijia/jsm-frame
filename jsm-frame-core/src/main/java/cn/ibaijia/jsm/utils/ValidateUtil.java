package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.annotation.EscapeType;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.annotation.FieldType;
import cn.ibaijia.jsm.context.rest.validate.ValidateCallback;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import org.slf4j.Logger;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtil {
    private static Logger logger = LogUtil.log(ValidateUtil.class);
    private static Map<String, ValidateCallback> cbCache = new HashMap<String, ValidateCallback>();

    @SuppressWarnings("unchecked")
    public static String validate(Object instance) {
        String message = null;
        if (!(instance instanceof ValidateModel)) {
            return null;
        }
        //check validate方法
        String res = checkValidateMethod("validate", (ValidateModel) instance);
        if (!StringUtil.isEmpty(res)) {
            return res;
        }
        //check validate bean
        String beanName = StringUtil.lowerCaseFirst(instance.getClass().getSimpleName() + "Validator");
        res = exeValidateBean(beanName, instance, (ValidateModel) instance);
        if (!StringUtil.isEmpty(res)) {
            return res;
        }

        Field[] fields = instance.getClass().getFields();
        for (Field field : fields) {
            String name = JsonUtil.getStrategyName(field.getName());
            FieldAnn fieldAnn = field.getAnnotation(FieldAnn.class);
            if (fieldAnn == null) {
                continue;
            }
            Object value = getFieldValue(field, instance);
            if (value == null && !fieldAnn.required()) {
                continue;
            }

            if (ValidateModel.class.isAssignableFrom(field.getType())) {
                if (fieldAnn.required() && value == null) {
                    message = SpringContext.getMessage("valid.required", name);
                    return message;
                }
                message = checkMore(fieldAnn, instance, field);
                if (!StringUtil.isEmpty(message)) {
                    return message;
                }
                if (value != null) {
                    String msg = ValidateUtil.validate(value);
                    if (!StringUtil.isEmpty(msg)) {
                        msg = name + "." + msg;
                        return msg;
                    }
                }
            } else if (List.class.isAssignableFrom(field.getType())) {
                List<Object> reqList = (List<Object>) getFieldValue(field, instance);
                if (fieldAnn.required() && (reqList == null || reqList.isEmpty())) {
                    message = SpringContext.getMessage("valid.required", name);
                    return message;
                }
                message = checkMore(fieldAnn, instance, field);
                if (!StringUtil.isEmpty(message)) {
                    return message;
                }
                if (reqList != null) {
                    for (Object req : reqList) {
                        if (req != null && ValidateModel.class.isAssignableFrom(req.getClass())) {
                            String msg = ValidateUtil.validate(req);
                            if (!StringUtil.isEmpty(msg)) {
                                msg = name + "." + msg;
                                return msg;
                            }
                        }
                    }
                }
            } else {
                if (fieldAnn.required() && StringUtil.isEmpty(value)) {
                    message = SpringContext.getMessage("valid.required", name);
                    return message;
                }
                FieldType type = fieldAnn.type();
                if (!StringUtil.isEmpty(value) && !type.equals(FieldType.ALL)) {//判断类型
                    if (FieldType.NUMBER.equals(type) && !StringUtil.isNumber(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.EN_NUMBER.equals(type) && !StringUtil.isEnNum(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.CHINESE.equals(type) && !StringUtil.isChinese(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.DATE.equals(type) && !StringUtil.isDate(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.DATE_TIME.equals(type) && !StringUtil.isDateTime(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.EMAIL.equals(type) && !StringUtil.isEmail(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.ID_CARD.equals(type) && !StringUtil.isIdcard(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.IPV4.equals(type) && !StringUtil.isIpv4(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.MOB_NO.equals(type) && !StringUtil.isMobNo(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.TEL_NO.equals(type) && !StringUtil.isTelNo(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.MONEY.equals(type) && !StringUtil.isMoney(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.URL.equals(type) && !StringUtil.isUrl(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                    if (FieldType.BANK_CARD.equals(type) && !StringUtil.isBankCard(value.toString())) {
                        message = SpringContext.getMessage("valid.mustbe", name, type.t());
                        return message;
                    }
                }

                String regex = fieldAnn.regex();
                if (!StringUtil.isEmpty(regex) && (value instanceof String) && !((String) value).matches(regex)) {
                    String msg = fieldAnn.message();
                    message = SpringContext.getMessage("valid.regexLimit", name, msg);
                    return message;
                }

                int minLen = fieldAnn.minLen();
                if (minLen != -1 && (value instanceof String) && ((String) value).length() < minLen) {
                    message = SpringContext.getMessage("valid.minLenLimit", name, minLen);
                    return message;
                }

                int maxLen = fieldAnn.maxLen();
                if (maxLen != -1 && (value instanceof String) && ((String) value).length() > maxLen) {
                    message = SpringContext.getMessage("valid.maxLenLimit", name, maxLen);
                    return message;
                }

                message = checkMore(fieldAnn, instance, field);
                if (!StringUtil.isEmpty(message)) {
                    return message;
                }

                //escape
                if (value instanceof String) {
                    String valueStr = (String) value;
                    EscapeType escapeType = fieldAnn.escape();
                    if (EscapeType.ALL.equals(escapeType)) {
                        setFieldValue(field, instance, HtmlUtil.filter(valueStr));
                    } else if (EscapeType.CSS.equals(escapeType)) {
                        setFieldValue(field, instance, HtmlUtil.cssFilter(valueStr));
                    } else if (EscapeType.JS.equals(escapeType)) {
                        setFieldValue(field, instance, HtmlUtil.jsFilter(valueStr));
                    } else if (EscapeType.HTML.equals(escapeType)) {
                        setFieldValue(field, instance, HtmlUtil.htmlFilter(valueStr));
                    }
                }
            }
        }
        return message;
    }

    private static String checkMore(FieldAnn fieldAnn, Object instance, Field field) {
        String message = checkEl(fieldAnn, instance, field);
        if (!StringUtil.isEmpty(message)) {
            return message;
        }

        message = checkValidateBean(fieldAnn, instance, field);
        if (!StringUtil.isEmpty(message)) {
            return message;
        }

        message = checkValidateMethod(fieldAnn, instance, field);
        if (!StringUtil.isEmpty(message)) {
            return message;
        }
        return null;
    }

    private static String checkValidateMethod(FieldAnn fieldAnn, Object instance, Field field) {
        String validateMethod = fieldAnn.validateMethod();
        String name = field.getName();
        if ("default".equals(validateMethod)) {
            validateMethod = name + "Validate";
        }
        if (!StringUtil.isEmpty(validateMethod)) {
            String res = checkValidateMethod(validateMethod, (ValidateModel) instance);
            if (!StringUtil.isEmpty(res)) {
                return SpringContext.getMessage("valid.regexLimit", name, res);
            }
        }
        return null;
    }

    private static String checkValidateBean(FieldAnn fieldAnn, Object instance, Field field) {
        String cb = fieldAnn.validateBean();
        if (!StringUtil.isEmpty(cb)) {
            String res = exeValidateBean(cb, BeanUtil.getFieldValue(field, instance), (ValidateModel) instance);
            if (!StringUtil.isEmpty(res)) {
                return SpringContext.getMessage("valid.regexLimit", field.getName(), res);
            }
        }
        return null;
    }

    private static String checkEl(FieldAnn fieldAnn, Object instance, Field field) {
        String el = fieldAnn.el();
        if (!StringUtil.isEmpty(el)) {
            boolean res = exeEl(el, instance);
            if (!res) {
                String msg = fieldAnn.message();
                return SpringContext.getMessage("valid.regexLimit", field.getName(), msg);
            }
        }
        return null;
    }


    private static String checkValidateMethod(String validateMethod, ValidateModel validateModel) {
        try {
            Method method = validateModel.getClass().getMethod(validateMethod);
            if (method == null) {
                return null;
            }
            return (String) method.invoke(validateModel);
        } catch (Exception e) {
            logger.debug("checkValidateMethod :" + e.getMessage());
            return null;
        }
    }

    private static String exeValidateBean(String cb, Object fieldVal, ValidateModel vm) {
        try {
            ValidateCallback vc = null;
            try {
                vc = SpringContext.getBean(cb);
            } catch (Exception e) {
                logger.debug("try get bean failed by name.{}", cb);
            }
            if (vc != null) {
                return vc.exe(fieldVal, vm);
            }
        } catch (Exception e) {
            return "验证器异常:" + cb;
        }
        return null;
    }

    public static boolean exeEl(String el, Object vm) {
        ExpressionParser parser = new SpelExpressionParser();
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("vm", vm);
        context.setVariable("session", WebContext.currentSession());
        context.setVariable("request", WebContext.getRequest());
        String elsStr = createExpression(el, vm);
        return parser.parseExpression(elsStr).getValue(context, Boolean.class);
    }

    private static String createExpression(String el, Object vm) {
        Pattern p = Pattern.compile("#\\{(.*?)\\}");
        Matcher m = p.matcher(el);
        Set<String> vars = new HashSet<String>();
        while (m.find()) {
            vars.add(m.group(1));
        }
        for (String var : vars) {
            String val = (String) BeanUtil.getFieldValue(var, vm);
            if (!StringUtil.isEmpty(val)) {
                el = el.replaceAll("#\\{" + var + "\\}", val);
            }
        }
        return el;
    }

    private static Object getFieldValue(Field field, Object instance) {
        try {
            //ReflectionUtil.getFieldValue(this, name)
            return field.get(instance);
        } catch (Exception e) {
            logger.error("getFieldValue error!", e);
            return null;
        }
    }

    private static void setFieldValue(Field field, Object instance, Object value) {
        try {
            //ReflectionUtil.getFieldValue(this, name)
            field.set(instance, value);
        } catch (Exception e) {
            logger.error("getFieldValue error!", e);
        }
    }
}
