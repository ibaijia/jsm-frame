package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.exception.FailedException;
import cn.ibaijia.jsm.exception.NotFoundException;
import org.slf4j.Logger;

public class ExceptionUtil {

    private static Logger logger = LogUtil.log(ExceptionUtil.class);

    public static void failed(String errorMsg) {
        logger.error(errorMsg);
        throw new FailedException(errorMsg);
    }

    public static void failed(Pair errorPair) {
        logger.error(errorPair.getMessage());
        throw new FailedException(errorPair);
    }

    public static void failed(Pair errorPair, String errorMsg) {
        logger.error(errorMsg);
        throw new FailedException(errorPair, errorMsg);
    }

    public static void notFound(String errorMsg) {
        logger.error(errorMsg);
        throw new NotFoundException(errorMsg);
    }

    public static void notFound(Pair errorPair) {
        logger.error(errorPair.getMessage());
        throw new NotFoundException(errorPair);
    }

    public static void notFound(Pair errorPair, String errorMsg) {
        logger.error(errorMsg);
        throw new NotFoundException(errorPair, errorMsg);
    }

}
