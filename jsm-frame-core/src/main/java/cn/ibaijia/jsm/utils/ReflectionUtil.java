package cn.ibaijia.jsm.utils;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.util.Assert;

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 反射工具类
 */
public class ReflectionUtil {
    private static Logger logger = LogUtil.log(ReflectionUtil.class);


    public static boolean isInt(Class clazz) {
        return (clazz == int.class || clazz == Integer.class);
    }

    public static boolean isLong(Class clazz) {
        return (clazz == long.class || clazz == Long.class);
    }

    public static boolean isShort(Class clazz) {
        return (clazz == short.class || clazz == Short.class);
    }

    public static boolean isByte(Class clazz) {
        return (clazz == byte.class || clazz == Byte.class);
    }

    public static boolean isDouble(Class clazz) {
        return (clazz == double.class || clazz == Double.class);
    }

    public static boolean isFloat(Class clazz) {
        return (clazz == float.class || clazz == Float.class);
    }

    public static boolean isString(Class clazz) {
        return (clazz == String.class);
    }

    public static boolean isBoolean(Class clazz) {
        return (clazz == boolean.class || clazz == Boolean.class);
    }

    public static boolean isCharacter(Class clazz) {
        return (clazz == char.class || clazz == Character.class);
    }

    public static boolean isBigDecimal(Class clazz) {
        return (clazz == BigDecimal.class);
    }

    public static boolean isDate(Class clazz) {
        return (clazz == Date.class);
    }

    public static boolean isBaseType(Class clazz) {
        return (isFloat(clazz) || isDouble(clazz) || isLong(clazz) || isInt(clazz) || isShort(clazz) || isByte(clazz) || isBoolean(clazz) || isCharacter(clazz) || isString(clazz));
    }

    public static boolean isNumberType(Class clazz) {
        return (isFloat(clazz) || isDouble(clazz) || isLong(clazz) || isInt(clazz) || isShort(clazz) || isByte(clazz) || isBigDecimal(clazz));
    }

    public static boolean isPlanType(Class clazz) {
        return (isBaseType(clazz) || isDate(clazz) || isBigDecimal(clazz));
    }

    public static boolean isObjectType(Class clazz) {
        return (clazz == Object.class);
    }

    public static boolean isInt(String clazz) {
        return (clazz.equals("Integer") || clazz.equals("int"));
    }

    public static boolean isLong(String clazz) {
        return (clazz.equals("Long") || clazz.equals("long"));
    }

    public static boolean isShort(String clazz) {
        return (clazz.equals("Short") || clazz.equals("short"));
    }

    public static boolean isByte(String clazz) {
        return (clazz.equals("Byte") || clazz.equals("byte"));
    }

    public static boolean isDouble(String clazz) {
        return (clazz.equals("Double") || clazz.equals("double"));
    }

    public static boolean isFloat(String clazz) {
        return (clazz.equals("Float") || clazz.equals("float"));
    }

    public static boolean isString(String clazz) {
        return (clazz.equals("String"));
    }

    public static boolean isBoolean(String clazz) {
        return (clazz.equals("Boolean") || clazz.equals("boolean"));
    }

    public static boolean isCharacter(String clazz) {
        return (clazz.equals("Character") || clazz.equals("Char"));
    }

    public static boolean isBigDecimal(String clazz) {
        return (clazz.equals("BigDecimal"));
    }

    public static boolean isDate(String clazz) {
        return clazz.equals("Date");
    }

    public static boolean isBaseType(String clazz) {
        return (isFloat(clazz) || isDouble(clazz) || isLong(clazz) || isInt(clazz) || isShort(clazz) || isByte(clazz) || isBoolean(clazz) || isCharacter(clazz) || isString(clazz));
    }

    public static boolean isNumberType(String clazz) {
        return (isFloat(clazz) || isDouble(clazz) || isLong(clazz) || isInt(clazz) || isShort(clazz) || isByte(clazz) || isBigDecimal(clazz));
    }

    public static boolean isPlanType(String clazz) {
        return (isBaseType(clazz) || isDate(clazz) || isBigDecimal(clazz));
    }

    public static boolean isObjectType(String clazz) {
        return clazz.equals("Object");
    }

    public static boolean isListType(String clazz) {
        return clazz.startsWith("List") || clazz.startsWith("Set");
    }


    /**
     * 循环向上转型, 获取对象的DeclaredField.
     *
     * @param object
     * @param fieldName
     * @return 如向上转型到Object仍无法找到, 返回null.
     */
    private static Field getDeclaredField(Object object, String fieldName) {
        Assert.notNull(object, "object不能为空");
        Assert.hasText(fieldName, "fieldName");
        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz
                .getSuperclass()) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                // Field不在当前类定义,继续向上转型
            }
        }
        return null;
    }

    /**
     * 循环向上转型, 获取对象的DeclaredMethod.
     *
     * @param object
     * @param methodName
     * @param parameterTypes
     * @return 如向上转型到Object仍无法找到, 返回null.
     */
    private static Method getDeclaredMethod(Object object, String methodName,
                                            Class<?>[] parameterTypes) {
        Assert.notNull(object, "object不能为空");
        Assert.hasText(methodName, "methodName");

        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz
                .getSuperclass()) {
            try {
                return clazz.getDeclaredMethod(methodName, parameterTypes);
            } catch (NoSuchMethodException e) {
                // Method不在当前类定义,继续向上转型
            }
        }
        return null;
    }

    /**
     * 直接读取对象属性值, 无视private/protected修饰符, 不经过getter函数.
     *
     * @param object
     * @param fieldName
     */
    public static Object getFieldValue(Object object, String fieldName) {
        Field field = getDeclaredField(object, fieldName);

        if (field == null) {
            throw new IllegalArgumentException("Could not find field ["
                    + fieldName + "] on target [" + object + "]");
        }

        makeAccessible(field);

        Object result = null;
        try {
            result = field.get(object);
        } catch (IllegalAccessException e) {
            logger.error("不可能抛出的异常{}", e.getMessage());
        }
        return result;
    }

    /**
     * 直接设置对象属性值, 无视private/protected修饰符, 不经过setter函数.
     *
     * @param object
     * @param fieldName
     * @param fieldValue
     */
    public static void setFieldValue(Object object, String fieldName,
                                     Object fieldValue) {
        Field field = getDeclaredField(object, fieldName);

        if (field == null) {
            throw new IllegalArgumentException("Could not find field ["
                    + fieldName + "] on target [" + object + "]");
        }

        makeAccessible(field);

        try {
            field.set(object, fieldValue);
        } catch (IllegalAccessException e) {
            logger.error("不可能抛出的异常:{}", e.getMessage());
        }
    }

    /**
     * 直接调用对象方法, 无视private/protected修饰符.
     *
     * @param object
     * @param methodName
     * @param parameterTypes
     * @param parameterValues
     */
    public static Object invokeMethod(Object object, String methodName,
                                      Class<?>[] parameterTypes, Object[] parameterValues) {
        Method method = getDeclaredMethod(object, methodName, parameterTypes);

        if (method == null) {
            throw new IllegalArgumentException("Could not find method ["
                    + methodName + "] on target [" + object + "]");
        }

        method.setAccessible(true);

        try {
            return method.invoke(object, parameterValues);
        } catch (Exception e) {
            throw convertReflectionExceptionToUnchecked(e);
        }
    }

    public static Object invokeStaticMethod(Class<?> clazz, String methodName,
                                            Class<?>[] parameterTypes, Object[] parameterValues) {
        Method method = getClazzMethod(clazz, methodName, parameterTypes);

        if (method == null) {
            throw new IllegalArgumentException("Could not find static method ["
                    + methodName + "] on target clazz [" + clazz + "]");
        }

        method.setAccessible(true);

        try {
            return method.invoke(null, parameterValues);
        } catch (Exception e) {
            throw convertReflectionExceptionToUnchecked(e);
        }
    }

    private static Method getClazzMethod(Class<?> clazz, String methodName, Class<?>[] parameterTypes) {
        try {
            return clazz.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    /**
     * 调用Getter方法.
     *
     * @param object
     * @param fieldName
     */
    public static Object invokeGetterMethod(Object object, String fieldName) {
        String getterMethodName = "get" + StringUtils.capitalize(fieldName);
        return invokeMethod(object, getterMethodName, new Class[]{},
                new Object[]{});
    }

    /**
     * 调用Setter方法.
     *
     * @param target
     * @param fieldName
     * @param parameterValue
     * @param parameterType  用于查找Setter方法,为空时使用value的Class替代.
     */
    public static void invokeSetterMethod(Object target, String fieldName,
                                          Class<?> parameterType, Object parameterValue) {
        String setterMethodName = "set" + StringUtils.capitalize(fieldName);
        Class<?> type = (parameterType != null ? parameterType : parameterValue
                .getClass());
        invokeMethod(target, setterMethodName, new Class[]{type},
                new Object[]{parameterValue});
    }

    /**
     * 调用Setter方法.使用value的Class来查找Setter方法.
     *
     * @param target
     * @param propertyName
     * @param parameterValue
     */
    public static void invokeSetterMethod(Object target, String propertyName,
                                          Object parameterValue) {
        invokeSetterMethod(target, propertyName, null, parameterValue);
    }

    /**
     * 强行设置Field可访问.
     */
    private static void makeAccessible(Field field) {
        if (!Modifier.isPublic(field.getModifiers())
                || !Modifier.isPublic(field.getDeclaringClass().getModifiers())) {
            field.setAccessible(true);
        }
    }

    /**
     * 通过反射, 获得Class定义中声明的父类的泛型参数的类型. 如无法找到, 返回Object.class.
     *
     * @param <T>
     * @param clazz
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getSuperClassGenricType(Class<T> clazz) {
        return (Class<T>) getSuperClassGenricType(clazz, 0);
    }

    /**
     * 通过反射, 获得定义Class时声明的父类的泛型参数的类型. 如无法找到, 返回Object.class.
     *
     * @param clazz
     * @param index
     */
    public static Class<?> getSuperClassGenricType(Class<?> clazz, int index) {

        Type genType = clazz.getGenericSuperclass();

        if (!(genType instanceof ParameterizedType)) {
            logger.warn(clazz.getSimpleName()
                    + "'s superclass not ParameterizedType");
            return Object.class;
        } else {
            Type[] params = ((ParameterizedType) genType)
                    .getActualTypeArguments();

            if (index >= params.length || index < 0) {
                logger.warn("Index: " + index + ", Size of "
                        + clazz.getSimpleName() + "'s Parameterized Type: "
                        + params.length);
                return Object.class;
            }
            if (!(params[index] instanceof Class)) {
                logger
                        .warn(clazz.getSimpleName()
                                + " not set the actual class on superclass generic parameter");
                return Object.class;
            }

            return (Class<?>) params[index];
        }
    }

    /**
     * 将反射时的checked exception转换为unchecked exception.
     */
    public static RuntimeException convertReflectionExceptionToUnchecked(
            Exception e) {
        if (e instanceof IllegalAccessException
                || e instanceof IllegalArgumentException
                || e instanceof NoSuchMethodException) {
            return new IllegalArgumentException("Reflection Exception.", e);
        } else if (e instanceof InvocationTargetException) {
            return new RuntimeException("Reflection Exception.",
                    ((InvocationTargetException) e).getTargetException());
        } else if (e instanceof RuntimeException) {
            return (RuntimeException) e;
        }
        return new RuntimeException("Unexpected Checked Exception.", e);
    }

    public static Object invokeMethod(ProceedingJoinPoint jpt, String methodName) {
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        Class<?>[] argTypes = method.getParameterTypes();
        return invokeMethod(jpt.getTarget(), methodName, argTypes, jpt.getArgs());
    }
}
