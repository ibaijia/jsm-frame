package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.WebContext;

public class OptLogUtil {

    private static String LOG_USER = "logUser";
    private static String LOG_CONTENT = "logContent";
    private static String LOG_STATUS = "logStatus";

    public static void setParam(String name, String value) {
        WebContext.getRequest().setAttribute(name, value);
    }

    public static void setContentAndStatus(String logContent, String logStatus) {
        WebContext.getRequest().setAttribute(LOG_CONTENT, logContent);
        WebContext.getRequest().setAttribute(LOG_STATUS, logStatus);
    }

    public static void setLogContent(String value) {
        WebContext.getRequest().setAttribute(LOG_CONTENT, value);
    }

    public static void setLogContent(Object value) {
        if (value instanceof String) {
            WebContext.getRequest().setAttribute(LOG_CONTENT, value);
        } else {
            WebContext.getRequest().setAttribute(LOG_CONTENT, StringUtil.toJson(value));
        }
    }

    public static void setLogContentIfNot(Object value) {
        if (WebContext.getRequest().getAttribute(LOG_CONTENT) == null) {
            setLogContent(value);
        }
    }

    public static void setLogStatusIfNot(String value) {
        if (WebContext.getRequest().getAttribute(LOG_STATUS) == null) {
            WebContext.getRequest().setAttribute(LOG_STATUS, value);
        }
    }

    public static void setLogStatus(String value) {
        WebContext.getRequest().setAttribute(LOG_STATUS, value);
    }

    public static void setLogUser(String value) {
        WebContext.getRequest().setAttribute(LOG_USER, value);
    }

    public static void setLogUserIfNot(String value) {
        if (WebContext.getRequest().getAttribute(LOG_USER) == null) {
            WebContext.getRequest().setAttribute(LOG_USER, value);
        }
    }

}
