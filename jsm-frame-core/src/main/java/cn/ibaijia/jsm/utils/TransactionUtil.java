package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

import cn.ibaijia.jsm.context.SpringContext;

public class TransactionUtil {

    private static Logger logger = LogUtil.log(TransactionUtil.class);

    public static PlatformTransactionManager getTransactionManager() {
        return ((PlatformTransactionManager) SpringContext.getBean("transactionManager"));
    }

    public static PlatformTransactionManager getTransactionManager(String transManagerName) {
        return ((PlatformTransactionManager) SpringContext.getBean(transManagerName));
    }

    public static void setTransactionStatus(TransactionStatus status) {
        ThreadLocalUtil.transTL.set(status);
    }

    public static TransactionStatus getTransactionStatus() {
        return ThreadLocalUtil.transTL.get();
    }

    public static void removeTransactionStatus() {
        ThreadLocalUtil.transTL.remove();
    }

    public static void setRollbackOnly() {
        TransactionStatus transactionStatus = ThreadLocalUtil.transTL.get();
        if (transactionStatus == null) {
            logger.error("setRollbackOnly transactionStatus is null.");
            return;
        }
        if (transactionStatus.isCompleted()) {
            logger.error("setRollbackOnly transactionStatus is completed.");
            return;
        }
        transactionStatus.setRollbackOnly();
    }

    public static void commit() {
        PlatformTransactionManager tm = getTransactionManager();
        TransactionStatus transactionStatus = ThreadLocalUtil.transTL.get();
        if (transactionStatus == null) {
            logger.error("commit transactionStatus is null.");
            return;
        }
        if (transactionStatus.isCompleted()) {
            logger.error("commit transactionStatus is completed.");
            return;
        }
        tm.commit(transactionStatus);
    }

}
