package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author longzl / @createOn 2010-10-21
 *
 */
public class LogUtil {
    public static Logger log(Class<?> clazz) {
        Logger logger = LoggerFactory.getLogger(clazz);
        return logger;

    }
}
