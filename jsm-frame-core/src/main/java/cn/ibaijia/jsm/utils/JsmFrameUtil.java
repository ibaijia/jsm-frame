package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.dao.model.FieldInfo;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.exception.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.*;

/**
 * @author longzl
 */
public class JsmFrameUtil {

    private static Logger logger = LogUtil.log(JsmFrameUtil.class);

    private static ScriptEngineManager sem;
    private static ScriptEngine se;

    public static String genMockResult(Method method) {
        String mockJsPath = "META-INF/js/mock.js";
        try {
            InputStream inputStream = JsmFrameUtil.class.getClassLoader().getResourceAsStream(mockJsPath);
            String mockJs = FileUtil.readFileAsText(inputStream);
            if (sem == null) {
                sem = new ScriptEngineManager();
                se = sem.getEngineByName("javascript");
                se.eval(mockJs);
            }
            String jsonStr = JsmFrameUtil.getMockJsonStr(method);
            logger.debug("mockJsStr:{}", jsonStr);
            //处理'@ * '
            jsonStr = jsonStr.replaceAll("\\\"@(.*?)\\\"", "Mock.Random.$1");
            se.eval("var res = Mock.mock(" + jsonStr + ");");
            se.eval("res = JSON.stringify(res);");
            String res = (String) se.get("res");
            return res;
        } catch (Exception e) {
            logger.error("genMockResult error.", e);
            return null;
        }
    }

    public static RestResp dealException(Exception e) {
        RestResp baseResp = new RestResp();
        if (e instanceof BaseException) {
            baseResp.setPair(((BaseException) e).getErrorPair());
            baseResp.message = ((BaseException) e).getMsg();
            if (e instanceof NotFoundException) {
                WebContext.getResponse().setStatus(404);
            } else if ((e instanceof NoLoginException) || (e instanceof AuthFailException)) {
                //Unauthorized
                WebContext.getResponse().setStatus(401);
            } else if (e instanceof NoPermissionException) {
                //Forbidden
                WebContext.getResponse().setStatus(403);
            } else {
                //Bad Request
                WebContext.getResponse().setStatus(400);
            }
        } else {
            WebContext.getResponse().setStatus(500);
            logger.error("other exception", e);
            baseResp.setPair(BasePairConstants.ERROR);
            if (AppContext.isDevModel()) {
                baseResp.message = baseResp.message + "[" + e.getMessage() + "]";
            }
        }
        logger.info("resp:" + StringUtil.toJson(baseResp));
        return baseResp;
    }

    public static String getMockJsonStr(Method method) {
        Map<String, Object> mapObject = new TreeMap<>();
        List<FieldInfo> fieldInfoList = JsmFrameUtil.getFieldInfoList(method.getReturnType(), JsmFrameUtil.getGenericNames(method.getGenericReturnType()), null, 0, true);
        for (FieldInfo fieldInfo : fieldInfoList) {
            String key = fieldInfo.fieldName;
            key = JsonUtil.getStrategyName(key);
            if (!StringUtil.isEmpty(fieldInfo.fieldAnn.mockRule())) {
                key = key + "|" + fieldInfo.fieldAnn.mockRule();
            }
            Object value = fieldInfo.fieldAnn.mockValue();
            if (StringUtil.isEmpty(value)) {
                value = getMockJson(fieldInfo);
            }
            mapObject.put(key, value);
        }
        return StringUtil.toJson(mapObject);
    }

    private static Object getMockJson(FieldInfo fieldInfo) {
        Object value = fieldInfo.fieldAnn.mockValue();
        if (StringUtil.isEmpty(value)) {
            int minLength = fieldInfo.fieldAnn.minLen() == -1 ? 0 : fieldInfo.fieldAnn.minLen();
            int maxLength = fieldInfo.fieldAnn.maxLen() == -1 ? 250 : fieldInfo.fieldAnn.maxLen();
            if (ReflectionUtil.isPlanType(fieldInfo.fieldType)) {
                value = getPlanTypeVal(fieldInfo.fieldType, minLength, maxLength);
            } else {
                if (fieldInfo.fieldInfoList == null) {
                    logger.error("field name:{} field type:{} maybe not implement ValidateModel.", fieldInfo.fieldName, fieldInfo.fieldType);
                    return value;
                }
                Map<String, Object> mapObject = new TreeMap<>();
                for (FieldInfo fieldInfo1 : fieldInfo.fieldInfoList) {
                    String key = fieldInfo1.fieldName;
                    key = JsonUtil.getStrategyName(key);
                    if (!StringUtil.isEmpty(fieldInfo1.fieldAnn.mockRule())) {
                        key = key + "|" + fieldInfo1.fieldAnn.mockRule();
                    }
                    mapObject.put(key, getMockJson(fieldInfo1));
                }
                if (ReflectionUtil.isListType(fieldInfo.fieldType)) {
                    //TODO seems have some problem
                    int len = fieldInfo.fieldType.split("\\|").length;
                    List lastList = null;
                    for (int i = 0; i < len - 1; i++) {
                        List list = new ArrayList();
                        lastList = list;
                        if (i == 0) {
                            value = list;
                        } else {
                            ((List) value).add(list);
                        }
                    }
                    lastList.add(mapObject);
                } else {
                    value = mapObject;
                }
            }
        }
        return value;
    }

    private static Class createClazz(String typeName) {
        try {
            return Class.forName(typeName.split("<")[0]);
        } catch (ClassNotFoundException e) {
            logger.error("", e);
        }
        return null;
    }

    private static Object getPlanTypeVal(String fieldType, int minLength, int maxLength) {
        String value = null;
        if (ReflectionUtil.isString(fieldType)) {
            value = String.format("@string(%s, %s)", minLength, maxLength);
        } else if (ReflectionUtil.isNumberType(fieldType)) {
            value = String.format("@integer(0,7)");
        } else if (ReflectionUtil.isDate(fieldType)) {
            value = String.format("@datetime()");
        } else if (ReflectionUtil.isBoolean(fieldType)) {
            value = String.format("@boolean");
        }
//        if (value != null) {
//            value = value.replace("@", "Mock.Random.");
//        }
        return value;
    }

    public static Type[] getGenericTypes(Type type) {
        if (type instanceof ParameterizedType) {
            return ((ParameterizedType) type).getActualTypeArguments();
        }
        return null;
    }

    public static String getGenericNames(Type type) {
        Type[] genericTypes = getGenericTypes(type);
        if (genericTypes != null && genericTypes.length > 0) {
            return genericTypes[0].getTypeName();
        }
        return null;
    }

    public static List<FieldInfo> getFieldInfoList(Class<?> clazz, String genericTypeName, List<String> parentClazzList, int level, boolean withFieldAnn) {
        if (clazz == null) {
            return null;
        }
        List<FieldInfo> list = new ArrayList<>();
        if (parentClazzList == null) {
            parentClazzList = new ArrayList<>();
        }
        if (parentClazzList.contains(clazz.getName())) {
            logger.warn("cycle class:{}", clazz.getName());
            return null;
        }
        parentClazzList.add(clazz.getName());
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            FieldAnn fieldAnn = field.getAnnotation(FieldAnn.class);
            if (fieldAnn == null) {
                continue;
            }
            FieldInfo fieldInfo = new FieldInfo();
            if (withFieldAnn) {
                fieldInfo.fieldAnn = fieldAnn;
            }
            fieldInfo.fieldType = field.getType().getSimpleName();
            fieldInfo.fieldName = field.getName();
            fieldInfo.required = fieldAnn.required();
            fieldInfo.comments = fieldAnn.comments();
            fieldInfo.level = level;
            if (!ReflectionUtil.isPlanType(field.getType())) {
                if ((Collection.class.isAssignableFrom(field.getType()))) {
                    Type type = field.getGenericType();
                    int levelList = level + 1;
                    while (type != null) {
                        Type[] genericClazzList1 = getGenericTypes(type);
                        if (genericClazzList1 == null) {
                            break;
                        }
                        type = genericClazzList1[0];
//                        if (type instanceof TypeVariableImpl && genericTypeName != null) {
                        if (type instanceof TypeVariable && genericTypeName != null) {
                            type = createClazz(genericTypeName);
                            break;
                        } else if (type instanceof ParameterizedType) {
                            String typeName = ((ParameterizedType) type).getRawType().getTypeName();
                            fieldInfo.fieldType = fieldInfo.fieldType + "|" + typeName.substring(typeName.lastIndexOf(".") + 1);
                            levelList++;
                        } else {
                            break;
                        }
                    }
                    if (type != null) {
                        Class<?> clazz1 = createClazz(type.getTypeName());
                        if (clazz1 != null) {
                            fieldInfo.fieldType = fieldInfo.fieldType + "|" + clazz1.getSimpleName();
                            fieldInfo.fieldInfoList = getFieldInfoList(clazz1, parentClazzList, levelList, withFieldAnn);
                        }
                    }

                } else if (ValidateModel.class.isAssignableFrom(field.getType())) {//list set
                    fieldInfo.fieldInfoList = getFieldInfoList(field.getGenericType(), parentClazzList, level + 1, withFieldAnn);
                } else {
                    Type type = field.getGenericType();
                    if (type instanceof TypeVariable) {
                        if (genericTypeName != null) {
                            Class clazz1 = createClazz(genericTypeName);
                            fieldInfo.fieldType = clazz1.getSimpleName();
                            fieldInfo.fieldInfoList = getFieldInfoList(clazz1, createRemainGeneric(genericTypeName), parentClazzList, level + 1, withFieldAnn);
                            genericTypeName = null;//TODO
                        }
                    } else {
                        logger.warn("ignore field info:{}", StringUtil.toJson(fieldInfo.fieldName));
                    }
                }
            }

            if (genericTypeName == null) {
                Type[] genericTypes = getGenericTypes(field.getDeclaringClass().getGenericSuperclass());
                if (genericTypes != null && genericTypes.length > 0) {
                    genericTypeName = genericTypes[0].getTypeName();
                }
            }
            list.add(fieldInfo);
        }
        logger.debug("remove:{}", clazz.getName());
        parentClazzList.remove(clazz.getName());
        return list;
    }

    private static String createRemainGeneric(String typeName) {
        int beginIdx = typeName.indexOf("<");
        int endIdx = typeName.indexOf(">");
        if (beginIdx == -1) {
            return null;
        }
        return typeName.substring(beginIdx + 1, endIdx);
    }

    public static List<FieldInfo> getFieldInfoList(Type type, List<String> parentClazzList, int level, boolean withFieldAnn) {
        if (parentClazzList == null) {
            parentClazzList = new ArrayList<>();
        }
        if (type instanceof ParameterizedType) {
            Class clazz = createClazz(((ParameterizedType) type).getRawType().getTypeName());
            String genericName = null;
            Type[] genericTypes = ((ParameterizedType) type).getActualTypeArguments();
            if (genericTypes != null && genericTypes.length > 0) {
                genericName = genericTypes[0].getTypeName();
            }
            return getFieldInfoList(clazz, genericName, parentClazzList, level, withFieldAnn);
        } else {
            Class clazz = createClazz(type.getTypeName());
            return getFieldInfoList(clazz, null, parentClazzList, level, withFieldAnn);
        }
    }

    public static String getKey(ProceedingJoinPoint jpt) {
        StringBuilder sb = new StringBuilder();
        sb.append("jsm:cache:");
        sb.append(jpt.toString());
        for (Object o : jpt.getArgs()) {
            if (o == null) {
                o = "";
            }
            sb.append(":").append(o.hashCode());
        }
        return sb.toString();
    }

    public static String getLockKey(ProceedingJoinPoint jpt) {
        StringBuilder sb = new StringBuilder();
        sb.append("jsm:lock:");
        sb.append(jpt.toString());
        for (Object o : jpt.getArgs()) {
            if (o == null) {
                o = "";
            }
            sb.append(":").append(o.hashCode());
        }
        return sb.toString();
    }

}