package cn.ibaijia.jsm.utils;

import org.springframework.transaction.TransactionStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ThreadLocalUtil {

    private static List<ThreadLocal> threadLocalList = new ArrayList<>();

    public static final ThreadLocal<HttpServletRequest> requestTL = new ThreadLocal<HttpServletRequest>();
    public static final ThreadLocal<HttpServletResponse> responseTL = new ThreadLocal<HttpServletResponse>();
    public static final ThreadLocal<String> jspPathTL = new ThreadLocal<String>();
    public static final ThreadLocal<String> oauthAccessTokenTL = new ThreadLocal<>();
    public static final ThreadLocal<String> oauthVerifyResultTL = new ThreadLocal<>();
    public static final ThreadLocal<SimpleDateFormat> sdfTL = new ThreadLocal<SimpleDateFormat>();
    public static final ThreadLocal<TransactionStatus> transTL = new ThreadLocal<TransactionStatus>();
    public static final ThreadLocal<String> dataSourceTL = new ThreadLocal<String>();

    public static boolean add(ThreadLocal threadLocal) {
        return threadLocalList.add(threadLocal);
    }

    public static void cleanAll() {
        for (ThreadLocal threadLocal : threadLocalList) {
            threadLocal.remove();
        }
        requestTL.remove();
        responseTL.remove();
        jspPathTL.remove();
        oauthAccessTokenTL.remove();
        oauthVerifyResultTL.remove();
        sdfTL.remove();
        transTL.remove();
        dataSourceTL.remove();
    }

    public static void clean(ThreadLocal threadLocal) {
        threadLocal.remove();
    }

}
