package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.WebContext;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TemplateUtil {

    private static String ATTRIBUTE_PREFIX = "attribute.";
    private static String REQUEST_PREFIX = "request.";
    private static String SESSION_PREFIX = "session.";

    public static String format(String tplStr, Map<String, String> data) {
        Pattern p = Pattern.compile("#\\{(.*?)\\}");
        Matcher m = p.matcher(tplStr);
        Set<String> vars = new HashSet<String>();
        while (m.find()) {
            vars.add(m.group(1));
        }
        for (Map.Entry<String, String> entry : data.entrySet()) {
            tplStr = tplStr.replaceAll("#\\{" + entry.getKey() + "\\}", entry.getValue());
        }
        return tplStr;
    }

    public static String formatWithContextVar(String str) {
        Pattern p = Pattern.compile("#\\{(.*?)\\}");
        Matcher m = p.matcher(str);
        Set<String> vars = new HashSet<String>();
        while (m.find()) {
            vars.add(m.group(1));
        }
        for (String var : vars) {
            String val = getContextVarValue(var);
            if (!StringUtil.isEmpty(val)) {
                str = str.replaceAll("#\\{" + var + "\\}", val);
            }
        }
        return str;
    }

    private static String getContextVarValue(String var) {
        if (var.startsWith(REQUEST_PREFIX)) {
            return StringUtil.toString(WebContext.getRequest().getParameter(var.replace(REQUEST_PREFIX, "")));
        } else if (var.startsWith(ATTRIBUTE_PREFIX)) {
            return StringUtil.toString(WebContext.getRequest().getAttribute(var.replace(ATTRIBUTE_PREFIX, "")));
        } else if (var.startsWith(SESSION_PREFIX)) {
            if (WebContext.currentSession() != null) {
                return StringUtil.toString(WebContext.currentSession().get(var.replace(SESSION_PREFIX, "")));
            }
        } else {
            String value = StringUtil.toString(WebContext.getRequest().getParameter(var));
            if (StringUtil.isEmpty(value)) {
                value = StringUtil.toString(WebContext.getRequest().getAttribute(var));
                if (StringUtil.isEmpty(value)) {
                    if (WebContext.currentSession() != null) {
                        value = StringUtil.toString(WebContext.currentSession().get(var));
                    }
                }
            }
            return value;
        }
        return null;
    }

}
