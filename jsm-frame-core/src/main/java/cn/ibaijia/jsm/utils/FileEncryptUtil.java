package cn.ibaijia.jsm.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.slf4j.Logger;

public class FileEncryptUtil { 

  private static Logger logger = LogUtil.log(FileEncryptUtil.class);
  /** 
  * 根据参数生成KEY 
  */ 
  public static Key getKey(String strKey) { 
    try { 
        KeyGenerator keyGenerator = KeyGenerator.getInstance("DES"); 
        keyGenerator.init(new SecureRandom(strKey.getBytes())); 
        SecretKey key = keyGenerator.generateKey();
        keyGenerator = null;
        return key;
    } catch (Exception e) { 
        logger.error("getKey error!", e);
        return null;
    } 
  } 

  /** 
  * 文件file进行加密并保存目标文件destFile中 
  * 
  * @param file   要加密的文件 如c:/test/srcFile.txt 
  * @param destFile 加密后存放的文件名 如c:/加密后文件.txt 
  */ 
  public static void encrypt(String file, String destFile,String strKey){ 
    // cipher.init(Cipher.ENCRYPT_MODE, getKey());
    InputStream is = null; 
    OutputStream out = null; 
    CipherInputStream cis = null; 
    try {
    	Key key = getKey(strKey);
    	Cipher cipher = Cipher.getInstance("DES"); 
    	cipher.init(Cipher.ENCRYPT_MODE, key); 
		is = new FileInputStream(file); 
		out = new FileOutputStream(destFile); 
		cis = new CipherInputStream(is, cipher); 
		byte[] buffer = new byte[1024]; 
		int r; 
		while ((r = cis.read(buffer)) > 0) { 
		    out.write(buffer, 0, r); 
		} 
		
	} catch (Exception e) {
		logger.error("encrypt error!", e);
	} finally {
		try {
			if(cis != null){
				cis.close(); 
			}
			if(is != null){
				is.close(); 
			}
			if(out != null){
				out.close();
			}
		} catch (IOException e1) {
			logger.error("encrypt error!", e1);
		}
	} 
  } 
  /** 
  * 文件采用DES算法解密文件 
  * 
  * @param file 已加密的文件 如c:/加密后文件.txt 
  *         * @param destFile 
  *         解密后存放的文件名 如c:/ test/解密后文件.txt 
  */ 
  public static void decrypt(String file, String dest,String strKey){ 
    InputStream is = null;
	OutputStream out = null;
	CipherOutputStream cos = null;
	try {
		Cipher cipher = Cipher.getInstance("DES");
		Key key = getKey(strKey);
		cipher.init(Cipher.DECRYPT_MODE, key); 
		is = new FileInputStream(file); 
		out = new FileOutputStream(dest); 
		cos = new CipherOutputStream(out, cipher); 
		byte[] buffer = new byte[1024]; 
		int r; 
		while ((r = is.read(buffer)) >= 0) { 
		    cos.write(buffer, 0, r); 
		}
	} catch (Exception e) {
		logger.error("decrypt error!", e);
	} finally {
		try {
			if(cos != null){
				cos.close(); 
			}
			if(is != null){
				is.close(); 
			}
			if(out != null){
				out.close();
			}
		} catch (Exception e1) {
			logger.error("decrypt error!", e1);
		}
	}  
  } 
  public static void main(String[] args) throws Exception { 
//	String fileName = "e:/test/1.rar";
//	String encFileName = "e:/test/enc-1.rar";
//	String decFileName = "e:/test/dec-1.rar";
//	String key = "aaa";
//	long start = System.nanoTime();
//	FileEncryptUtil.encrypt(fileName, encFileName,key); //加密 
//	long encEnd = System.nanoTime();
//	System.out.println("enc time:"+(encEnd-start));
//	FileEncryptUtil.decrypt(encFileName, decFileName,key); //解密 
//	long decEnd = System.nanoTime();
//	System.out.println("dec time:"+(decEnd-encEnd));
  } 
}
