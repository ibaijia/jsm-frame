package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;

import java.util.*;

public class SensitiveWordUtil {
    private static Logger logger = LogUtil.log(SensitiveWordUtil.class);
    private static Map sensitiveWordMap = new HashMap();
    public static final int MATCH_TYPE_MIN = 1;      //最小匹配
    public static final int MATCH_TYPE_MAX = 2;      //最大匹配
    private static String END_TRUE = "1";
    private static String END_FALSE = "0";
    private static String END_FLAG = "end";

    public static void init() {
        String[] wordsArr = loadSensitiveDic();
        createSensitiveWordMap(wordsArr);
    }

    //load dic
    private static String[] loadSensitiveDic() {
        String filename = "sensitive-words";
        try {
            String txt = FileUtil.readFileAsText(SensitiveWordUtil.class.getClassLoader().getResourceAsStream(filename));
            return txt.split(";");
        } catch (Exception e) {
            logger.error("loadSensitiveDic error.", e);
            return null;
        }
    }

    /**
     * 创建DFA 结构
     * DFA即Deterministic Finite Automaton，也就是确定有穷自动机
     *
     * @param words
     */
    private static void createSensitiveWordMap(String[] words) {
        sensitiveWordMap = new HashMap(words.length);     //初始化敏感词容器，减少扩容操作
        //迭代keyWordSet
        for (String word : words) {
            Map parentMap = sensitiveWordMap;
            for (int i = 0; i < word.length(); i++) {
                char keyChar = word.charAt(i);       //转换成char型
                Object childMap = parentMap.get(keyChar);       //获取
                if (childMap != null) {        //如果存在该key，直接赋值
                    parentMap = (Map) childMap;
                } else {     //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                    Map<String, String> newWordMap = new HashMap<>();
                    newWordMap.put(END_FLAG, END_FALSE);     //不是最后一个
                    parentMap.put(keyChar, newWordMap);
                    parentMap = newWordMap;
                }
                if (i == word.length() - 1) {
                    parentMap.put(END_FLAG, END_TRUE);    //最后一个
                }
            }
        }
    }

    private static String createReplaceString(char replaceChar, int length) {
        char[] resultReplace = new char[length];
        for (int i = 0; i < length; i++) {
            resultReplace[i] = replaceChar;
        }
        return String.valueOf(resultReplace);
    }

    /**
     * 查找并替换敏感词
     *
     * @param txt
     * @param matchType
     * @param replaceChar
     * @return
     */
    public static String findAndReplaceSensitiveWord(String txt, int matchType, char replaceChar) {
        String resultTxt = txt;
        Set<String> set = getSensitiveWord(txt, matchType);     //获取所有的敏感词
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String word = iterator.next();
            String replaceString = createReplaceString(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }
        return resultTxt;
    }

    /**
     * 从当前位置检查 敏感词长度 返回length大于0 则表示存在敏感词
     *
     * @param txt
     * @param beginIndex
     * @param matchType
     * @return
     */
    private static int checkSensitiveWordLength(String txt, int beginIndex, int matchType) {
        boolean flag = false;    //敏感词结束标识位：用于敏感词只有1位的情况
        int matchFlag = 0;     //匹配标识数默认为0
        char word = 0;
        Map currentMap = sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++) {
            word = txt.charAt(i);
            currentMap = (Map) currentMap.get(word);     //获取指定key
            if (currentMap != null) {     //存在，则判断是否为最后一个
                matchFlag++;     //找到相应key，匹配标识+1
                if (END_TRUE.equals(currentMap.get(END_FLAG))) {       //如果为最后一个匹配规则,结束循环，返回匹配标识数
                    flag = true;       //结束标志位为true
                    if (MATCH_TYPE_MIN == matchType) {    //最小规则，直接返回,最大规则还需继续查找
                        break;
                    }
                }
            } else {     //不存在，直接返回
                break;
            }
        }
        if (matchFlag < 2 || !flag) {        //长度必须大于等于1，为词
            matchFlag = 0;
        }
        return matchFlag;
    }

    /**
     * 获取所有 敏感词
     *
     * @param txt
     * @param matchType
     * @return
     */
    public static Set<String> getSensitiveWord(String txt, int matchType) {
        Set<String> sensitiveWordList = new HashSet<>();
        for (int i = 0; i < txt.length(); i++) {
            int length = checkSensitiveWordLength(txt, i, matchType);    //判断是否包含敏感字符
            if (length > 0) {    //存在,加入list中
                sensitiveWordList.add(txt.substring(i, i + length));
                i = i + length - 1;    //减1的原因，是因为for会自增
            }
        }
        return sensitiveWordList;
    }

}
