
package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.status.StatusLogger;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StringUtil {

    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();

    private static final String CONSTS_STR = "QWERTYUPASDFGHJKZXCVBNM1234567890qwertyupasdfghjkzxcvbnm";
    private static final String CONSTS_NUM = "1234567890";

    public static String substr(String str, int beginIdx, int length) {
        if (isEmpty(str)) {
            return str;
        }
        if (str.length() <= beginIdx) {
            return null;
        }
        int endIdx = beginIdx + length;
        if (endIdx > str.length()) {
            endIdx = str.length();
        }
        return str.substring(beginIdx, endIdx);
    }

    public static String extract(String regex, String str) {
        return extract(regex, str, 0);
    }

    public static String extract(String regex, String str, int groupIdx) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        if (m.find()) {
            if (groupIdx > m.groupCount()) {
                return null;
            }
            return m.group(groupIdx);
        }
        return null;
    }

    public static String extractNumber(String str) {
        return extract(AppContext.get(AppContextKey.REGEX_EXTRACT_NUMBER), str);
    }

    public static Integer extractInteger(String str) {
        String res = extractNumber(str);
        if (!StringUtil.isEmpty(res)) {
            return Integer.valueOf(res);
        }
        return null;
    }

    public static Float extractFloat(String str) {
        String res = extractNumber(str);
        if (!StringUtil.isEmpty(res)) {
            return Float.valueOf(res);
        }
        return null;
    }

    public static Double extractDouble(String str) {
        String res = extractNumber(str);
        if (!StringUtil.isEmpty(res)) {
            return Double.valueOf(res);
        }
        return null;
    }


    /**
     * iso8859-1转化为utf-8编码
     *
     * @param str
     * @return String
     */
    public static String iso2utf(String str) {
        String result = StringUtils.stripToEmpty(str);
        try {
            result = new String(result.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("iso2utf error!", e);
        }
        return result;
    }

    public static String trim(String str) {
        if (str == null) {
            return "";
        }
        return str.trim();
    }

    /**
     * @return 将java对象转化为json字符串
     */
    public static String toJson(Object data) {
        return JsonUtil.toJsonString(data);
    }

    public static <T> T parseObject(String text, Class<T> clazz) {
        return JsonUtil.parseObject(text, clazz);
    }

    public static <T> T parseObject(String text, TypeReference<T> type) {
        return JsonUtil.parseObject(text, type);
    }

    public static <T> T parseObject(String text, JavaType type) {
        return JsonUtil.parseObject(text, type);
    }

    public static <T> T parseObject(String text, Type type) {
        return JsonUtil.parseObject(text, type);
    }

    public static String toUtf8String(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 0 && c <= 255) {
                sb.append(c);
            } else {
                byte[] b;
                try {
                    b = Character.toString(c).getBytes("utf-8");
                } catch (Exception ex) {
                    System.out.println(ex);
                    b = new byte[0];
                }
                for (int j = 0; j < b.length; j++) {
                    int k = b[j];
                    if (k < 0)
                        k += 256;
                    sb.append("%" + Integer.toHexString(k).toUpperCase());
                }
            }
        }
        return sb.toString();
    }

    public static boolean isEmpty(Object object) {
        if (object == null) {
            return true;
        } else {
            return isEmpty(object.toString());
        }
    }

    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        } else {
            return "".equals(str = str.trim()) ? true : false;
        }
    }

    public static Integer toInteger(Object obj) {
        return toInteger(toString(obj), 0);
    }

    public static Integer toInteger(String str) {
        return toInteger(str, 0);
    }

    public static Integer toInteger(String val, Integer defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Integer.valueOf(trim(val));
        } catch (Exception e) {
            logger.error("toInteger error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static Short toShort(Object obj) {
        return toShort(toString(obj), (short) 0);
    }

    public static Short toShort(String str) {
        return toShort(str, (short) 0);
    }

    public static Short toShort(String val, Short defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Short.valueOf(trim(val));
        } catch (Exception e) {
            logger.error("toShort error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static Byte toByte(Object obj) {
        return toByte(toString(obj), (byte) 0);
    }

    public static Byte toByte(String str) {
        return toByte(str, (byte) 0);
    }

    public static Byte toByte(String val, Byte defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Byte.valueOf(trim(val));
        } catch (Exception e) {
            logger.error("toByte error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static Boolean toBoolean(Object obj) {
        return toBoolean(toString(obj), false);
    }

    public static Boolean toBoolean(String str) {
        return toBoolean(str, false);
    }

    public static Boolean toBoolean(String val, Boolean defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Boolean.valueOf(trim(val));
        } catch (Exception e) {
            logger.error("toBoolean error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static Float toFloat(String str) {
        return toFloat(str, 0f);
    }

    public static Float toFloat(String val, Float defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Float.valueOf(trim(val));
        } catch (Exception e) {
            logger.error("toFloat error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static Long toLong(String str) {
        return toLong(str, 0L);
    }

    public static Long toLong(String val, Long defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Long.valueOf(val);
        } catch (Exception e) {
            logger.error("toLong error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static Double toDouble(String str) {
        return toDouble(str, 0d);
    }

    public static Double toDouble(String val, Double defaultVal) {
        try {
            if (StringUtil.isEmpty(val)) {
                return defaultVal;
            }
            return Double.valueOf(val);
        } catch (Exception e) {
            logger.error("toDouble error, use defaultVal:{}", defaultVal);
            return defaultVal;
        }
    }

    public static String toString(Object object) {
        return object == null ? "" : object.toString().trim();
    }

    /**
     * 判断2个不为空并且要相等
     *
     * @param source
     * @param target
     * @return boolean
     */
    public static boolean notNullAndEquls(Object source, Object target) {
        if (source == null || target == null)
            return false;
        return source.toString().equals(target.toString());
    }

    public static boolean equals(Object source, Object target) {
//        logger.debug("equals source:{},target:{}", source, target);
        if (source != null) {
            return source.equals(target);
        }
        if (target != null) {
            return target.equals(source);
        }
        return true;//都为null
    }

    public static boolean notEquals(Object source, Object target) {

        return !equals(source, target);
    }

    public static boolean isEmail(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_EMAIL));
        }
        return false;
    }

    public static boolean isUrl(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_URL));
        }
        return false;
    }

    public static boolean isIdcard(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_IDCARD));
        }
        return false;
    }

    public static boolean isTelNo(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_TELNO));
        }
        return false;
    }

    public static boolean isMobNo(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_MOBNO));
        }
        return false;
    }

    public static boolean isIpv4(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_IPV4));
        }
        return false;
    }

    public static boolean isDate(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_DATE));
        }
        return false;
    }

    public static boolean isDateTime(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_DATETIME));
        }
        return false;
    }

    public static boolean isChinese(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_CHINESE));
        }
        return false;
    }

    public static boolean isNumber(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_NUMBER));
        }
        return false;
    }

    public static boolean isMoney(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_MONEY));
        }
        return false;
    }

    public static boolean isBankCard(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_BANKCARD));
        }
        return false;
    }

    public static boolean isEnNum(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_ENNUM));
        }
        return false;
    }

    public static boolean isAccount(String str) {
        if (str != null) {
            return str.matches(AppContext.get(AppContextKey.REGEX_VALID_ACCOUNT));
        }
        return false;
    }

    public static boolean isEquals(String orginStr, String destStr) {
        if (orginStr == null) {
            return destStr == null;
        } else {
            return orginStr.equals(destStr);
        }
    }

    public static Long yuanToFen(Double money) {
        Double res = NumberUtil.multiply(money, 100d);
        res = NumberUtil.formatHalfUp(res, 0);
        return res.longValue();
    }

    public static Double fenToYuan(Long money) {
        Double res = NumberUtil.divide(money, 100d);
        res = NumberUtil.formatHalfUp(res, 2);
        return res;
    }

    public static Long yuanToFen(Float money) {
        Float res = NumberUtil.multiply(money, 100f);
        res = NumberUtil.formatHalfUp(res, 0);
        return res.longValue();
    }

    public static Float fenToYuanFloat(Long money) {
        Float res = NumberUtil.divide(money, 100f);
        res = NumberUtil.formatHalfUp(res, 2);
        return res;
    }

    public static String fill(Object data, int len) {
        String val = data.toString();
        if (val.length() >= len) {
            return val;
        }
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len - val.length(); i++) {
            sb.append("0");
        }
        sb.append(val);
        return sb.toString();
    }

    public static int getByteLength(String str) {
        if (str == null) {
            return 0;
        }
        return str.getBytes().length;
    }

    public static String normalizeSQL(String line) {
        if (line == null) {
            return line;
        }
        return line.trim().replaceAll("(\\s+|\\u00A0)", " ");
    }

    public static String normalize(String line) {
        if (line == null) {
            return line;
        }
        return line.trim().replaceAll("(\\s+|\\u00A0)", " ");
    }

    public static String trimCRLF(String line) {
        if (line == null) {
            return line;
        }
        return line.replaceAll("(\r|\n|\r\n)", "");
    }

    public static String trimTab(String line) {
        if (line == null) {
            return line;
        }
        return line.replaceAll("(\t+)", "");
    }

    public static String trimAll(String line) {
        if (line == null) {
            return line;
        }
        return line.replaceAll("(\r|\n|\r\n|\t|\\s)", "");
    }

    public static String genRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(CONSTS_STR.charAt(random.nextInt(CONSTS_STR.length())));
        }
        return sb.toString();
    }

    public static String genRandomNum(int length) {
        StringBuilder sb = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(CONSTS_NUM.charAt(random.nextInt(CONSTS_NUM.length())));
        }
        return sb.toString();
    }

    public static String genRandomStr(String source, int length) {
        if (isEmpty(source)) {
            return null;
        }
        StringBuilder sb = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(source.charAt(random.nextInt(source.length())));
        }
        return sb.toString();
    }

    public static String camelToKebab(String str) {
        if (isEmpty(str)) {
            return null;
        }
        return str.replaceAll("[A-Z]", "-$0").toLowerCase();
    }

    public static String camelToSnake(String str) {
        if (isEmpty(str)) {
            return null;
        }
        return str.replaceAll("[A-Z]", "_$0").toLowerCase();
    }

    private static Pattern snakePattern = Pattern.compile("_(\\w)");

    public static String snakeToCamel(String str) {
        return replaceByPattern(str, snakePattern);
    }

    private static String replaceByPattern(String str, Pattern pattern) {
        if (isEmpty(str)) {
            return null;
        }
        str = str.toLowerCase();
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private static Pattern kebabPattern = Pattern.compile("-(\\w)");

    public static String kebabToCamel(String str) {
        return replaceByPattern(str, kebabPattern);
    }

    public static String kebabToSnake(String str) {
        return str.replaceAll("-", "_");
    }

    public static String snakeToKebab(String str) {
        return str.replaceAll("_", "-");
    }

    public static String upperCaseFirst(String str) {
        char[] chars = str.toCharArray();
        if (chars[0] >= 'a' && chars[0] <= 'z') {
            chars[0] = (char) (chars[0] - 32);
        }
        return new String(chars);
    }

    public static String lowerCaseFirst(String str) {
        char[] chars = str.toCharArray();
        if (chars[0] >= 'A' && chars[0] <= 'Z') {
            chars[0] = (char) (chars[0] + 32);
        }
        return new String(chars);
    }

    public static String trim(String str, String symbol) {
        if (isEmpty(str)) {
            return null;
        }
        if (str.startsWith(symbol)) {
            str = str.substring(symbol.length());
        }
        if (str.endsWith(symbol)) {
            str = str.substring(0, str.length() - symbol.length());
        }
        return str.trim();
    }

    public static String uuid() {
        return uuid(true);
    }

    public static String uuid(boolean removeStrike) {
        if (removeStrike) {
            return UUID.randomUUID().toString().replace("-", "");
        } else {
            return UUID.randomUUID().toString();
        }
    }

    public static String listJoin(List list, char separator) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Object str : list) {
            if (i > 0) {
                sb.append(separator);
            }
            sb.append(toString(str));
            i++;
        }
        return sb.toString();
    }

    public static String listToStr(List list, char separator) {
        return listJoin(list, separator);
    }

    public static List<String> strToList(String str, char separator) {
        if (isEmpty(str)) {
            return null;
        }
        String[] arr = str.split(String.valueOf(separator));
        return new ArrayList<>(Arrays.asList(arr));
    }

    public static String removeAsList(String str, char separator, String element) {
        if (isEmpty(str)) {
            return str;
        }
        List<String> list = strToList(str, separator);
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String el = it.next();
            if (element.equals(el)) {
                it.remove();
            }
        }
        return listToStr(list, separator);
    }

    public static void main(String[] args) {
//        System.out.println(genRandomString(5));
//        System.out.println(humpToMiddleLine("UserApi"));
//        System.out.println(StringUtil.class.getSimpleName());
        System.out.println(IpUtil.ipToInt("192.168.200.253"));
    }

}
