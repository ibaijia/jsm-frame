package cn.ibaijia.jsm.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

    public static String get(HttpServletRequest request, String key) {
        String val = getHeader(request, key);
        if (StringUtil.isEmpty(val)) {
            val = getParameter(request, key);
        }
        if (StringUtil.isEmpty(val)) {
            val = getCookie(request, key);
        }
        return val;
    }

    public static String getHeader(HttpServletRequest request, String key) {
        return request.getHeader(key);
    }

    public static String getParameter(HttpServletRequest request, String key) {
        return request.getParameter(key);
    }

    public static String getCookie(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (key.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

}
