<#setting number_format="#"/>
package ${restReqPackageName};
<#if hasDate?exists>
import java.util.Date;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibaijia.jsm.utils.DateUtil;
</#if>
import cn.ibaijia.jsm.base.ValidateModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

public class ${className}UpdateReq implements ValidateModel {
    private static final long serialVersionUID = 1L;

    @FieldAnn(required = true, comments = "记录ID")
    public Long id;

	<#list fieldList as item>
	/**
	 * defaultVal:${item.defaultVal?default("")}
	 * comments:${item.comments?default("")}
	 */
    <#if item.fieldType == "String">
    @FieldAnn(required = ${item.required?string('true', 'false')}, maxLen = ${item.maxLength?default("1")}, comments = "${item.comments?default("")}")
    <#else>
    @FieldAnn(required = ${item.required?string('true', 'false')},  comments = "${item.comments?default("")?replace("\r\n","")}")
    </#if>
    <#if item.fieldType == "Date">
    @JSONField(format = DateUtil.DATETIME_PATTERN)
    </#if>
	public ${item.fieldType?default("")} ${item.fieldName?default("")};
	</#list>
}