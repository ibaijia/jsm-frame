<#setting number_format="#"/>
import ajax from "../../common/util/HttpUtil";

export default {
<#list methodInfoList as item>
    /*!
     *${item.comments}
     */
    ${item.name}(${item.fullVarName?default("")}successCb, errorCb) {
        this.${item.name}_param_async(${item.fullVarName?default("")}'', successCb, errorCb, true);
    },
    ${item.name}_param(${item.fullVarName?default("")}paramStr, successCb, errorCb) {//可传url参数
        this.${item.name}_param_async(${item.fullVarName?default("")}paramStr, successCb, errorCb, true);
    },
    ${item.name}_param_async(${item.fullVarName?default("")}paramStr, successCb, errorCb, async) {//可传url参数和设置是否同步，默认true异步
        var url = '${item.url?replace("{","'+")?replace("}","+'")}';
        if(paramStr){
            url = url + '?' + paramStr;
        }
        ajax.${item.httpMethod}(url, ${item.fullVarName?default("")}successCb, errorCb, async)
    },
</#list>
}
