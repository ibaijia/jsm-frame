<#setting number_format="#"/>
package ${restPackageName};

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.rest.resp.RestResp;
import ${servicePackageName}.${className}Service;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import javax.annotation.Resource;
import ${restReqPackageName}.*;
import ${restVoPackageName}.*;
import ${modelPackageName}.${className};

@RestController
@RequestMapping("/v1/${apiName}")
public class ${className}Api extends BaseRest {

    @Resource
    private ${className}Service ${className?uncap_first}Service;

    @ApiOperation(value = "${className}分页列表", notes = "")
    @RequestMapping(value = "/page-list", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<${className}PageReq<${className}Vo>> pageList(${className}PageReq<${className}Vo> req) {
        RestResp<${className}PageReq<${className}Vo>> resp = new RestResp<>();
        ${className?uncap_first}Service.pageList(req);
        resp.result = req;
        return resp;
    }

    @ApiOperation(value = "${className}新增", notes = "")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> add(@RequestBody ${className}AddReq req) {
        RestResp<Integer> resp = new RestResp<>();
        resp.result = ${className?uncap_first}Service.add(req);
        return resp;
    }

    @ApiOperation(value = "${className}详情", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<${className}> findById(@PathVariable("id") Long id) {
        RestResp<${className}> resp = new RestResp<>();
        resp.result = ${className?uncap_first}Service.findById(id);
        return resp;
    }

    @ApiOperation(value = "${className}修改", notes = "")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> update(@RequestBody ${className}UpdateReq req) {
        RestResp<Integer> resp = new RestResp<>();
        resp.result = ${className?uncap_first}Service.update(req);
        return resp;
    }

    @ApiOperation(value = "${className}删除", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Integer> deleteById(@PathVariable("id") Long id) {
        RestResp<Integer> resp = new RestResp<>();
        resp.result = ${className?uncap_first}Service.deleteById(id);
        return resp;
    }

}