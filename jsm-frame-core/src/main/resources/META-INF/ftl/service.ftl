<#setting number_format="#"/>
package ${servicePackageName};

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import ${modelPackageName}.${className};
import ${mapperPackageName}.${className}Mapper;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.utils.BeanUtil;
<#if genApi?exists && genApi>
import ${restReqPackageName}.*;
import ${restVoPackageName}.*;
</#if>
@Service
public class ${className}Service extends BaseService {

    @Resource
    private ${className}Mapper ${className?uncap_first}Mapper;
    <#if genApi?exists && genApi>
    public void pageList(${className}PageReq<${className}Vo> req) {
        //example,只使用了db model,mapper xml 也可使用vo对象
        ${className?uncap_first}Mapper.pageListVo(req);
    }

    public int add(${className}AddReq req) {
        ${className} ${className?uncap_first} = new ${className}();
        BeanUtil.copy(req, ${className?uncap_first});
        return ${className?uncap_first}Mapper.add(${className?uncap_first});
    }

    public int update(${className}UpdateReq req) {
        ${className} db${className} = ${className?uncap_first}Mapper.findByIdForUpdate(req.id);
        if ( db${className} == null ) {
            notFound("id not found:" + req.id);
        }
        BeanUtil.copy(req, db${className});
        return ${className?uncap_first}Mapper.update(db${className});
    }
    </#if>
    public int deleteById(Long id) {
        return ${className?uncap_first}Mapper.deleteById(id);
    }

    public ${className} findById(Long id) {
        ${className} db${className} = ${className?uncap_first}Mapper.findById(id);
        if ( db${className} == null ) {
            notFound("id not found:" + id);
        }
        return db${className};
    }

}