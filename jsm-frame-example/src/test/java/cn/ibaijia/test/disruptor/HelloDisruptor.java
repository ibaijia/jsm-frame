package cn.ibaijia.test.disruptor;

public class HelloDisruptor {

    public static void main(String[] args) {
        DisruptorService disruptorService = new DisruptorService();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    disruptorService.pushMessage("message-" + i);
                }
            }
        }).start();
    }
}
