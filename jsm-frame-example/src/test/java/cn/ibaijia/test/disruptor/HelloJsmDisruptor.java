package cn.ibaijia.test.disruptor;

import cn.ibaijia.jsm.disruptor.JsmDisruptor;
import cn.ibaijia.jsm.http.HttpAsyncClient;
import cn.ibaijia.jsm.utils.StringUtil;
import com.lmax.disruptor.EventHandler;
import org.apache.http.HttpResponse;
import org.apache.http.concurrent.FutureCallback;

public class HelloJsmDisruptor {


    public static void main(String[] args) {
//        JsmDisruptor<String> jsmDisruptor = new JsmDisruptor<String>(new EventHandler<JsmDisruptor<main.lang.String>.MessageEvent<String>>() {
//            @Override
//            public void onEvent(JsmDisruptor<String>.MessageEvent<String> stringMessageEvent, long l, boolean b) throws Exception {
//                System.out.println("onEvent:"+stringMessageEvent.message);
//            }
//        });
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 100; i++) {
//                    jsmDisruptor.pushMessage("message-" + i);
//                }
//            }
//        }).start();

//        JsmDisruptor<Integer> jsmDisruptor = new JsmDisruptor<Integer>(new EventHandler<JsmDisruptor<main.lang.Integer>.MessageEvent<Integer>>() {
//            @Override
//            public void onEvent(JsmDisruptor<Integer>.MessageEvent<Integer> stringMessageEvent, long l, boolean b) throws Exception {
//                System.out.println("onEvent:" + stringMessageEvent.message);
//            }
//        });
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 100; i++) {
//                    jsmDisruptor.pushMessage(i);
//                }
//            }
//        }).start();
        HttpAsyncClient asyncClient = new HttpAsyncClient();
        JsmDisruptor<Student> jsmDisruptor = new JsmDisruptor<Student>(new EventHandler<JsmDisruptor<Student>.MessageEvent<Student>>() {
            @Override
            public void onEvent(JsmDisruptor<Student>.MessageEvent<Student> stringMessageEvent, long l, boolean b) throws Exception {
                System.out.println(String.format("onEvent: %s ,%s ", l, b));
                System.out.println(Thread.currentThread().getId());
//                Thread.sleep(1000);
                asyncClient.get("https://www.baidu.com", new FutureCallback<HttpResponse>() {

                    @Override
                    public void completed(HttpResponse result) {
//                        System.out.println("completed:" + result.getStatusLine());
                    }

                    @Override
                    public void failed(Exception ex) {
                        System.out.println("failed!");
                    }

                    @Override
                    public void cancelled() {
                        System.out.println("cancelled!");
                    }
                });
            }
        }, 32);
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getId());
                for (int i = 0; i < 100; i++) {
                    Student student = new Student();
                    student.name = "同学" + i;
                    student.age = i;
                    jsmDisruptor.pushMessage(student);
                    System.out.println("push " + i);
                }
            }
        }).start();


    }
}

class Student {
    public String name;
    public int age;
}
