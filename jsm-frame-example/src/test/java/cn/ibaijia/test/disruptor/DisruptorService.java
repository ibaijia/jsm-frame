package cn.ibaijia.test.disruptor;

import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;

import java.util.concurrent.ThreadFactory;

public class DisruptorService {
    private static Logger logger = StatusLogger.getLogger();
    private EventTranslatorOneArg<MessageEvent, String> translator = new MessageEventTranslator();
    private RingBuffer<MessageEvent> ringBuffer;

    public class MessageEvent {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class MessageEventFactory implements EventFactory<MessageEvent> {
        @Override
        public MessageEvent newInstance() {
            return new MessageEvent();
        }
    }

    public class MessageEventTranslator implements EventTranslatorOneArg<MessageEvent, String> {
        @Override
        public void translateTo(MessageEvent messageEvent, long sequence, String message) {
            messageEvent.setMessage(message);
        }
    }

    public class MessageThreadFactory implements ThreadFactory {
        int index = 0;

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "jsm-drp-" + (index < Integer.MAX_VALUE - 1 ? (++index) : 0));
        }
    }

    public class MessageEventHandler implements EventHandler<MessageEvent> {
        @Override
        public void onEvent(MessageEvent messageEvent, long sequence, boolean endOfBatch) throws Exception {
            System.out.println(messageEvent.getMessage() + "endOfBatch:" + endOfBatch);
        }
    }

    public class MessageExceptionHandler implements ExceptionHandler<MessageEvent> {
        @Override
        public void handleEventException(Throwable ex, long sequence, MessageEvent event) {
            ex.printStackTrace();
        }

        @Override
        public void handleOnStartException(Throwable ex) {
            ex.printStackTrace();

        }

        @Override
        public void handleOnShutdownException(Throwable ex) {
            ex.printStackTrace();

        }
    }

    private void init() {
        int ringBufferSize = 1024;//必须是2的N次方
        Disruptor<MessageEvent> disruptor = new Disruptor<MessageEvent>(new MessageEventFactory(), ringBufferSize, new MessageThreadFactory(), ProducerType.MULTI, new BlockingWaitStrategy());
        disruptor.handleEventsWith(new MessageEventHandler());
        disruptor.setDefaultExceptionHandler(new MessageExceptionHandler());
        ringBuffer = disruptor.start();
    }

    public void pushMessage(String message) {
        if (ringBuffer == null) {
            synchronized (translator) {
                if (ringBuffer == null) {
                    init();
                }
            }
        }
        if (ringBuffer != null) {
            ringBuffer.publishEvent(translator, message);
        } else {
            logger.error("pushMessage error. ringBuffer is null!");
        }
    }
}
