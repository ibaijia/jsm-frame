package cn.ibaijia.test.free;

import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.jsm.utils.HtmlUtil;
import cn.ibaijia.jsm.utils.IpUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.junit.Test;

import java.util.Arrays;

public class TestFree {

    @Test
    public void testHttpClient() {
        String url = "https://webstagecn-aws.tni.com.cn/store/customer/account/create?s=4335511&ct=3&ln=%E9%AB%98*";
        HttpClient httpClient = new HttpClient();
//        HttpHost proxyHost = new HttpHost("192.168.0.106",1080,"socks5");
//        httpClient.setHttpProxy(proxyHost);
        httpClient.setSocksProxy("192.168.0.106", 1080);
        String res = httpClient.get(url);
        System.out.println(res);
    }

    @Test
    public void testStat() {
//        System.out.println(StringUtil.toJson(SystemUtil.getSystemStat())); c0a80c8
        System.out.println(Integer.MAX_VALUE);
        System.out.println(IpUtil.ipToHex("192.168.0.200"));
        System.out.println(IpUtil.hexToIp("c0a800c8"));
    }

    @Test
    public void testSort(){
        int[] arr = {3,2,6,7};
        Arrays.sort(arr);
        System.out.println(StringUtil.toJson(arr));
    }


    @Test
    public void testHtmlUtil(){
        String str = "${abc}";
        System.out.println(HtmlUtil.elFilter(str));
    }

}
