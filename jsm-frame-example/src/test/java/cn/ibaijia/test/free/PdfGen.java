package cn.ibaijia.test.free;

import cn.ibaijia.jsm.utils.PdfUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class PdfGen {

    @Test
    public void genPdf() {
        String tplPath = "D:\\test\\contract_tpl1.pdf";
        String outPath = "D:\\test\\contract_tpl_out.pdf";
        Map<String,Object> data = new HashMap<>();
        data.put("contractNo","123");
//        data.put("租客姓名","隆治利");
//        data.put("租客证件类型","学生证");
//        data.put("房源地址","重庆");
        PdfUtil.formatPdf(tplPath,outPath,data);
    }

}
