package cn.ibaijia.test.free;

import com.alibaba.fastjson.JSON;
import cn.ibaijia.jsm.http.HttpClient;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.junit.Test;
import org.wltea.analyzer.cfg.Configuration;
import org.wltea.analyzer.cfg.DefaultConfig;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import org.wltea.analyzer.dic.Dictionary;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BaiduAudioTest {


    @Test
    public void getAudio() {
        String text = "吃过晚饭了吗？";
        String url = "https://fanyi.baidu.com/gettts?lan=zh&text=%s&spd=5&source=web";

        HttpClient httpClient = new HttpClient();
        httpClient.getFile(String.format(url, text), "D:/2.mp3");

    }

    @Test
    public void mergeAudio() {
        File audio1 = new File("D:/1.mp3");
        File audio2 = new File("D:/2.mp3");
        File audio3 = new File("D:/3.mp3");
//        appendFile(audio1,audio3);
//        appendFile(audio2,audio3);
        List<File> list = new ArrayList<>();
        list.add(audio1);
        list.add(audio2);
        appendFiles(list, audio3);

    }

    @Test
    public void getWord() throws IOException {
        String dir = "D:/test/1/";
        List<String> wordsList = segText("my name is longzhili",true);
        System.out.println(JSON.toJSONString(wordsList));
        String url = "https://fanyi.baidu.com/gettts?lan=zh&text=%s&spd=5&source=web";
        HttpClient httpClient = new HttpClient();
        int idx = 0;
        List<File> fileList = new ArrayList<>();
        for (String words : wordsList) {
            String fileName = dir + idx + ".mp3";
            httpClient.getFile(String.format(url, words), fileName);
            fileList.add(new File(fileName));
            idx++;
        }
        String outFile = dir + idx + "out.mp3";
        appendFiles(fileList,new File(outFile));
    }

    @Test
    public void getSeg() throws IOException {
        List<String> wordsList = splition("隆丰临饿了没有？",true);
        System.out.println(JSON.toJSONString(wordsList));
    }

    @Test
    public void testik() throws Exception {
        String text = "张北京儿童医院几年解决痘痘";
        // 创建分词对象
        Analyzer analyzer = new IKAnalyzer(false); // true　用智能分词，false细粒度
        Configuration cfg = DefaultConfig.getInstance();
        Dictionary.initial(cfg);
        StringReader reader = new StringReader(text);
        // 分词
        TokenStream tokenStream = analyzer.tokenStream("", reader);
        CharTermAttribute charTerm = tokenStream.getAttribute(CharTermAttribute.class);
        // 遍历分词数据
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
            System.out.print(charTerm.toString() + "|");
        }
        tokenStream.close();
        reader.close();
        System.out.println();
    }

    private List<String> splition(String sentence,boolean useSmart) throws IOException {
        Analyzer analyzer = new IKAnalyzer(useSmart);
        Configuration cfg = DefaultConfig.getInstance();
        Dictionary.initial(cfg);
        StringReader reader = new StringReader(sentence);
        TokenStream tokenStream = analyzer.tokenStream("", reader);
        CharTermAttribute charTerm = tokenStream.getAttribute(CharTermAttribute.class);
        tokenStream.reset();
        Set<String> words = new HashSet<>();
        words.add(sentence);
        while (tokenStream.incrementToken()) {
            String word = charTerm.toString();
            words.add(word);
        }
        tokenStream.close();
        reader.close();
        return new ArrayList<>(words);
    }

    private List<String> segText(String text, boolean useSmart) {
        Configuration cfg = DefaultConfig.getInstance();
        Dictionary.initial(cfg);
        List<String> list = new ArrayList<>();
        IKSegmenter ik = new IKSegmenter(new StringReader(text), useSmart);
        try {
            Lexeme word = null;
            while((word=ik.next())!=null) {
                list.add(word.getLexemeText());
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return list;
    }

    public boolean appendFile(File source, File dest) {
        FileOutputStream fos = null;
        FileInputStream fis = null;
        try {
            fos = new FileOutputStream(dest);
            byte[] buff = new byte[1024];
            fis = new FileInputStream(source);
            int len = 0;
            while (len != -1) {
                fos.write(buff, 0, len);
                len = fis.read(buff);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public boolean appendFiles(List<File> sourceList, File dest) {
        FileOutputStream fos = null;
        FileInputStream fis = null;
        try {
            fos = new FileOutputStream(dest);
            byte[] buff = new byte[1024];
            for (File source : sourceList) {
                fis = new FileInputStream(source);
                int len = 0;
                while (len != -1) {
                    fos.write(buff, 0, len);
                    len = fis.read(buff);
                }
                fis.close();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

}

