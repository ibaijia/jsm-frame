package cn.ibaijia.test.free;

import cn.ibaijia.jsm.utils.StringUtil;

import java.util.Arrays;

public class TestSearch {

    public static void main(String[] args) {
        int arr[] = {5, 11, 7, 9, 2, 3, 12, 8, 6, 1,9, 4, 10};
//        int idx = halfSearch(arr, 3);
//        System.out.println(idx);
        Arrays.sort(arr);
        System.out.println(StringUtil.toJson(getFibonacci()));
        System.out.println(halfSearch(arr,9));
    }

    /**
     * 顺序查找
     *
     * 顺序查找也称为线形查找，属于无序查找算法。从数据结构线形表的一端开始，顺序扫描，依次将扫描到的结点关键字与给定值k相比较，若相等则表示查找成功；若扫描结束仍没有找到关键字等于k的结点，表示查找失败。
     * 顺序查找的时间复杂度为O(n)。
     *
     */
    public static int orderSearch(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == target) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 常规二分查找
     *
     * 也称为是折半查找，属于有序查找算法。用给定值k先与中间结点的关键字比较，中间结点把线形表分成两个子表，若相等则查找成功；若不相等，再根据k与该中间结点关键字的比较结果确定下一步查找哪个子表，这样递归进行，直到查找到或查找结束发现表中没有这样的结点。
     * 最坏情况下，关键词比较次数为log2(n+1)，且期望时间复杂度为O(log2n)；
     *
     */
    public static int halfSearch(int[] arr, int target) {
        //使数组有序
        Arrays.sort(arr);
        System.out.println(String.format("原数组%s中查找【%s】", Arrays.toString(arr), target));
        int left = 0;
        int right = arr.length - 1;
        while (left <= right) {
            int mid = ((right - left) >> 1) + left;
            if (target > arr[mid]) {
                left = mid + 1;
            } else if (target < arr[mid]) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    /**
     * 递归二分查找
     * @param high len-1
     */
    public static int halfByRecurseSearch(int[] arr, int target, int low, int high) {
        int mid = ((high - low) >> 1) + low;
        if (low == high) {
            return -1;
        }
        if (target == arr[mid]) {
            return mid;
        }
        if (target > arr[mid]) {
            return halfByRecurseSearch(arr, target, mid + 1, high);
        }
        if (target < arr[mid]) {
            return halfByRecurseSearch(arr, target, low, mid - 1);
        }
        return -1;
    }

    /**
     * 插值查找
     *
     * 基于二分查找算法，将查找点的选择改进为自适应选择，可以提高查找效率。当然，差值查找也属于有序查找
     * 代码基本上和二分查找一样，有修改的地方就是mid的获取
     *
     * 在二分查找中$mid=$$low+high \over {2}$ 可改写成 $mid =low + $ $ {high-low} \over {2}$
     *
     * 也就是说我们的mid每次都是折中的取，但是对于一些均匀分布的有序表，这样做感觉有些费时，比如找字典的时候，找a这个字母，我们肯定不会从中间开始，而是偏向于字典前面一些开始。
     *
     * 插值查找就是基于这样的思想
     * 我们对1/2进行改进:
     *
     * $mid=low+$$key-a[low] \over a[high]=a[low]$$(high-low)$
     *
     * key就是要查找的值，数组a是有序表
     *
     * 简单的理解就是计算出key所占比，然后更好地找到key所在的区间范围
     * 但是对于极端分布的数组，插值查找的效率就大打折扣了
     *
     */
    public static int insertSearch(int[] arr, int target) {
        Arrays.sort(arr);
        int left = 0;
        int right = arr.length - 1;
        while (left <= right) {
            int mid = left + (right - left) * ((target - arr[left]) / (arr[right] - arr[left]));
            if (target > arr[mid]) {
                left = mid + 1;
            } else if (target < arr[mid]) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    /**
     * 递归插值查找
     *
     * @param high len-1
     */
    public static int insertByRecurseSearch(int[] arr, int target, int low, int high) {
        int mid = low + (high - low) * ((target - arr[low]) / (arr[high] - arr[low]));
        if (low <= high) {
            if (target == arr[mid]) {
                return mid;
            } else if (target > arr[mid]) {
                return insertByRecurseSearch(arr, target, mid + 1, high);
            } else {
                return insertByRecurseSearch(arr, target, low, mid - 1);
            }
        } else {
            return -1;
        }
    }


    // 因为后面我们mid=low+F(k-1)-1，需要使用到斐波那契数列，因此我们需要先获取到一个斐波那契数列
    // 非递归方法得到一个斐波那契数列
    private static int[] getFibonacci() {
        int[] fibonacci = new int[20];
        fibonacci[0] = 1;
        fibonacci[1] = 1;
        for (int i = 2; i < fibonacci.length; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }
        return fibonacci;
    }

    // 编写斐波那契查找算法
    // 使用非递归的方式编写算法
    /**
     *
     * 斐波那契查找原理与前两种相似，仅仅改变了中间结点(mid)的位置，mid不 .
     * 再是中间或插值得到，而是位于黄金分割点附近，即mid=low+F(k-1)-1
     *
     * @param arr       数组
     * @param findValue 我们需要查找的关键码(值)
     * @return 返回对应的下标，如果没有-1
     */
    public static int fibonacciSearch(int[] arr, int findValue) {
        int low = 0;
        int high = arr.length - 1;
        int k = 0;// 表示斐波那契分割数值的下标
        int mid = 0;// 存放mid值
        int fibonacci[] = getFibonacci();// 获取到斐波那契数列
        // 获取到斐波那契分割数值的下标
        while (high > fibonacci[k] - 1)
            k++;
        // 因为 fibonacci[k] 值 可能大于 arr 的 长度，因此我们需要使用Arrays类，构造一个新的数组，并指向temp[]
        // 不足的部分会使用0填充
        int[] temp = Arrays.copyOf(arr, fibonacci[k]);
        // 实际上需求使用arr数组最后的数填充 temp
        for (int i = high + 1; i < temp.length; i++) {
            temp[i] = arr[high];
        }
        // 使用while来循环处理，找到我们的数 findValue
        while (low <= high) {// 只要这个条件满足，就可以找
            mid = low + fibonacci[k] - 1;
            if (findValue < temp[mid]) {// 我们应该继续向数组的前面查找(左边)
                high = mid - 1;
                // 为甚是 k--
                // 说明
                // 1. 全部元素 = 前面的元素 + 后边元素
                // 2. f[k] = f[k-1] + f[k-2]
                // 因为 前面有 f[k-1]个元素,所以可以继续拆分 f[k-1] = f[k-2] + f[k-3]
                // 即 在 f[k-1] 的前面继续查找 k--
                // 即下次循环 mid = f[k-1-1]-1
                k--;
            } else if (findValue > temp[mid]) {
                low = mid + 1;
                // 为什么是k -=2
                // 说明
                // 1. 全部元素 = 前面的元素 + 后边元素
                // 2. f[k] = f[k-1] + f[k-2]
                // 3. 因为后面我们有f[k-2] 所以可以继续拆分 f[k-2] = f[k-3] + f[k-4]
                // 4. 即在f[k-2] 的前面进行查找 k -=2
                // 5. 即下次循环 mid = f[k - 1 - 2] - 1
                k++;
            } else {// 找到
                // 需要确定，返回的是哪个下标
                if (mid <= high)
                    return mid;
                else
                    return high;
            }
        }
        return -1;

    }

}
