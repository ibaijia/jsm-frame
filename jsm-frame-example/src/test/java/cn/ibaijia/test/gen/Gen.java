package cn.ibaijia.test.gen;

import cn.ibaijia.jsm.gen.GenService;
import cn.ibaijia.jsm.utils.LogUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springContext-core.xml"})
public class Gen {

    private Logger logger = LogUtil.log(Gen.class);

    @Resource
    private GenService genService;

    @Test
    public void gen() {
        genService.gen("area_t", false);
    }

}
