<a href="#get_hello">GET /hello</a>


<a name="get_hello"></a>GET /hello

|  请求参数  |            |   |
|:----------|:-------------|:------|
| result | 必填 | 响应结果 |
| list | 选填 | 当前页数据列表 |
			| pageNo | 必填 | 当前页数 |
			| pageSize | 必填 | 每页条数 |
			| totalCount | 选填 | 总条数 |
			| queryString | 选填 | 查询条件 |
		| code | 必填 | 1001表示成功，其它参照错误代码表 |
		| message | 必填 | 响应码对应描述 |
		

|  响应参数  |            |   |
|:----------|:-------------|:------|
| result | 必填 | 响应结果 |
| list | 选填 | 当前页数据列表 |
			| pageNo | 必填 | 当前页数 |
			| pageSize | 必填 | 每页条数 |
			| totalCount | 选填 | 总条数 |
			| queryString | 选填 | 查询条件 |
		| code | 必填 | 1001表示成功，其它参照错误代码表 |
		| message | 必填 | 响应码对应描述 |