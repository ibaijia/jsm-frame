package ${daoMapperPkg};

import cn.ibaijia.jsm.dao.model.OptLog;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;

public interface OptLogMapper {
	
	Long add(OptLog optLog);
	
	List<OptLog> pageList(Page<OptLog> page);

}
