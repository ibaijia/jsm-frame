<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xsi:schemaLocation="
		http://www.springframework.org/schema/beans 
		http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
		http://www.springframework.org/schema/context 
		http://www.springframework.org/schema/context/spring-context-4.1.xsd
		http://www.springframework.org/schema/tx 
		http://www.springframework.org/schema/tx/spring-tx-4.1.xsd">
		
	<!-- 使用annotation注解方式配置事务 -->
	<tx:annotation-driven transaction-manager="transactionManager" />
	
	<!-- 使用JDBC事务 -->  
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">  
        <property name="dataSource" ref="dynamicDataSource" />
    </bean>
	
    <#noparse>
    <!-- 默认数据源 或者 Master数据源（Write） -->
    <bean id="masterDataSource" class="com.alibaba.druid.pool.DruidDataSource" init-method="init" destroy-method="close">
        <property name="driverClassName" value="${mysql.master.driverClassName}"/>
        <property name="url" value="${mysql.master.url}"/>
        <property name="username" value="${mysql.master.username}"/>
        <property name="password" value="${mysql.master.password}"/>
        <property name="maxActive" value="${mysql.master.maxActive}"/>
        <property name="minIdle" value="${mysql.master.minIdle}"/>
        <property name="maxWait" value="60000" /><!-- 配置获取连接等待超时的时间 -->
        <property name="timeBetweenEvictionRunsMillis" value="60000"/><!-- 每隔多少时间检测一次，比如每半小时检测一次，总不能总是检测，这会对性能产生影响 -->
        <property name="minEvictableIdleTimeMillis" value="3000000"/><!-- 一个数据库连接连接多少时间之外，我们认为其应该不再适用了(可能下一次就会失效了)，应该移除并重新建立连接了 -->
        <property name="validationQuery" value="SELECT 1"/>
        <property name="testWhileIdle" value="true"/><!-- 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。 -->
        <property name="testOnBorrow" value="true"/> <!-- 这里建议配置为TRUE，防止取到的连接不可用,但会影响性能 -->
        <property name="testOnReturn" value="false" /><!-- 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能-->
        <property name="poolPreparedStatements" value="true" /><!-- 是否缓存preparedStatement，也就是PSCache。PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql5.5以下的版本中没有PSCache功能，建议关闭掉。5.5及以上版本有PSCache，建议开启。 -->
        <property name="maxOpenPreparedStatements" value="100" />
        <property name="removeAbandoned" value="true"/> <!--是否在数据库连接请求量大的时候，如总数50，当前已请求了49个，所剩不多了，检测那些执行时间久的连接，将其从池中移除掉(移除之后怎么作，由实现决定，如直接断开，或者任其继续执行等) -->
        <property name="removeAbandonedTimeout" value="300000"/> <!--一次数据库操作执行时间超过多少秒的连接被认为是需要移除的 -->
        <property name="connectionInitSqls">
            <list>
                <value>set names utf8mb4</value>
            </list>
        </property>
        <property name="filters" value="stat,wall" /><!-- 配置监控统计拦截的filters，去掉后监控界面sql无法统计   开启web监控、开启sql防火墙 -->
        <property name="proxyFilters">
            <list>
                <ref bean="logFilter" />
                <ref bean="statFilter" />
            </list>
        </property>
    </bean>
    <!-- 慢SQL记录 -->
    <bean id="statFilter" class="com.alibaba.druid.filter.stat.StatFilter">
        <!-- 慢sql时间设置,即执行时间大于10秒的都是慢sql -->
        <property name="slowSqlMillis" value="10000"/>
        <property name="logSlowSql" value="true"/>
    </bean>
    <bean id="logFilter" class="com.alibaba.druid.filter.logging.Slf4jLogFilter">
        <property name="statementExecutableSqlLogEnable" value="false" />
    </bean>

    <!--  Slave数据源(Read) -->
    <bean id="slaveDataSource" class="com.alibaba.druid.pool.DruidDataSource" init-method="init" destroy-method="close">
        <property name="driverClassName" value="${mysql.slave.driverClassName}"/>
        <property name="url" value="${mysql.slave.url}"/>
        <property name="username" value="${mysql.slave.username}"/>
        <property name="password" value="${mysql.slave.password}"/>
        <property name="maxActive" value="${mysql.slave.maxActive}"/>
        <property name="minIdle" value="${mysql.slave.minIdle}"/>
        <property name="maxWait" value="60000" /><!-- 配置获取连接等待超时的时间 -->
        <property name="timeBetweenEvictionRunsMillis" value="60000"/><!-- 每隔多少时间检测一次，比如每半小时检测一次，总不能总是检测，这会对性能产生影响 -->
        <property name="minEvictableIdleTimeMillis" value="3000000"/><!-- 一个数据库连接连接多少时间之外，我们认为其应该不再适用了(可能下一次就会失效了)，应该移除并重新建立连接了 -->
        <property name="validationQuery" value="SELECT 1"/>
        <property name="testWhileIdle" value="true"/><!-- 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。 -->
        <property name="testOnBorrow" value="true"/> <!-- 这里建议配置为TRUE，防止取到的连接不可用,但会影响性能 -->
        <property name="testOnReturn" value="false" /><!-- 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能-->
        <property name="removeAbandoned" value="true"/> <!--是否在数据库连接请求量大的时候，如总数50，当前已请求了49个，所剩不多了，检测那些执行时间久的连接，将其从池中移除掉(移除之后怎么作，由实现决定，如直接断开，或者任其继续执行等) -->
        <property name="removeAbandonedTimeout" value="300000"/> <!--一次数据库操作执行时间超过多少秒的连接被认为是需要移除的 -->
        <property name="connectionInitSqls">
            <list>
                <value>set names utf8mb4</value>
            </list>
        </property>
        <property name="filters" value="stat,wall" /><!-- 配置监控统计拦截的filters，去掉后监控界面sql无法统计   开启web监控、开启sql防火墙 -->
        <property name="proxyFilters">
            <list>
                <ref bean="logFilter" />
                <ref bean="statFilter" />
            </list>
        </property>
    </bean>
    <!-- 动态数据源 -->
	<bean id="dynamicDataSource" class="cn.ibaijia.jsm.db.DynamicDataSource">
        <property name="targetDataSources">
            <map key-type="java.lang.String">
                <entry key="master" value-ref="masterDataSource"/>
                <entry key="slave" value-ref="slaveDataSource"/>
            </map>
        </property>
    </bean>

    <!-- 配置SqlSessionFactoryBean --></#noparse>
	<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
		<property name="dataSource" ref="dynamicDataSource"/>
		<property name="typeAliasesPackage" value="cn.ibaijia.jsm.dao.model,${daoModelPkg},${restVoPkg}"></property>
		 <!-- 自动扫描需要定义类别名的包，将包内的JAVA类的类名作为类别名 -->
		<property name="mapperLocations">
			<list>
				<value>classpath*:/META-INF/mappers/DbMapper.xml</value>
				<value>classpath*:mappers/*Mapper.xml</value>
			</list>
		</property>
		<property name="plugins">
			 <list>
			 	<bean class="cn.ibaijia.jsm.interceptor.PageInterceptor">
				 	<property name="properties">
	                    <value>
	                        dbDialect=mysql
	                    </value>
	                </property>
			 	</bean>
			 </list>
		</property>
	</bean>
	
	<!-- 自动扫描所有的Mapper接口与文件 -->
     <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
         <property name="basePackage" value="cn.ibaijia.jsm.dao.mapper,${daoMapperPkg}"></property>
         <!-- <property name="sqlSessionFactory" ref="sqlSessionFactory"></property>  --> 
         <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"></property>  
     </bean>
	
</beans>