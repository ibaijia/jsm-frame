# master ,default
mysql.master.driverClassName=com.mysql.jdbc.Driver
mysql.master.url=jdbc:mysql://${dbHost}/${dbName}?useUnicode=true&autoReconnect=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai
mysql.master.username=${dbUser}
mysql.master.password=${dbPassword}
mysql.master.initialSize=1
mysql.master.maxActive=10
mysql.master.maxIdle=8
mysql.master.minIdle=1

# slave
mysql.slave.driverClassName=com.mysql.jdbc.Driver
mysql.slave.url=jdbc:mysql://${dbHost}/${dbName}?useUnicode=true&autoReconnect=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai
mysql.slave.username=${dbUser}
mysql.slave.password=${dbPassword}
mysql.slave.initialSize=1
mysql.slave.maxActive=10
mysql.slave.maxIdle=8
mysql.slave.minIdle=1