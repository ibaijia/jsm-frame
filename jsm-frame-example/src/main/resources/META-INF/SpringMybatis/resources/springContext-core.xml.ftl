<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xsi:schemaLocation="
		http://www.springframework.org/schema/beans 
		http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
		http://www.springframework.org/schema/context 
		http://www.springframework.org/schema/context/spring-context-4.1.xsd
		http://www.springframework.org/schema/aop 
        http://www.springframework.org/schema/aop/spring-aop-4.1.xsd
		http://www.springframework.org/schema/tx 
		http://www.springframework.org/schema/tx/spring-tx-4.1.xsd">
	
	     <!-- 引入配置文件 -->
    <context:property-placeholder location="classpath*:springContext-*.properties"/>
         
	<context:component-scan base-package="${servicePkg}" />
	<context:component-scan base-package="${jobPkg}" />
	
	<import resource="classpath:springContext-mysql.xml"/>
	<import resource="classpath:springContext-ehcache.xml"/>
	<import resource="classpath:springContext-jedis.xml"/>
	<!--
	<import resources="classpath:springContext-mongo.xml"/>
	-->
	<import resource="classpath:springContext-schedule.xml"/>
	
	
	<!-- 用于持有ApplicationContext,可以使用SpringContext.getBean('xxxx')的静态方法得到spring bean对象 -->
	<bean class="cn.ibaijia.jsm.context.SpringContext" lazy-init="false" />
	
	<!-- 配置国际化资源文件路径 -->
	<bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
        <property name="basename" value="messages/message"/>
        <property name="useCodeAsDefaultMessage" value="true"/>
    </bean>

    <!-- jsm service -->
    <context:component-scan base-package="cn.ibaijia.jsm.service,cn.ibaijia.jsm.aop,cn.ibaijia.jsm.auth,cn.ibaijia.jsm.context" />

    <aop:aspectj-autoproxy  proxy-target-class="true">
    </aop:aspectj-autoproxy>


</beans>