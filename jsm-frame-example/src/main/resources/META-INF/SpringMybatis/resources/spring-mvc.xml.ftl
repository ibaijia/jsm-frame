<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" 
	   xmlns:aop="http://www.springframework.org/schema/aop" 
	   xmlns:context="http://www.springframework.org/schema/context"
	   xmlns:mvc="http://www.springframework.org/schema/mvc"
	   xmlns:p="http://www.springframework.org/schema/p"
	   xmlns:tx="http://www.springframework.org/schema/tx" 
	   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xsi:schemaLocation="http://www.springframework.org/schema/aop 
		http://www.springframework.org/schema/aop/spring-aop-4.1.xsd 
		http://www.springframework.org/schema/beans 
		http://www.springframework.org/schema/beans/spring-beans-4.1.xsd 
		http://www.springframework.org/schema/context 
		http://www.springframework.org/schema/context/spring-context-4.1.xsd 
		http://www.springframework.org/schema/mvc 
		http://www.springframework.org/schema/mvc/spring-mvc-4.1.xsd 
		http://www.springframework.org/schema/tx 
		http://www.springframework.org/schema/tx/spring-tx-4.1.xsd">

	<mvc:annotation-driven >
		<mvc:message-converters register-defaults="true">
			<!-- 使用 fastjson替换默认jackson -->
			<bean class="com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter">
				<property name="supportedMediaTypes">
					<list>
					    <!-- 
						<value>text/html;charset=UTF-8</value>
						<value>text/json;charset=UTF-8</value>
					     -->
						<value>application/json;charset=UTF-8</value>
					</list>
				</property>
				<property name="features">
					<list>
                        <value>QuoteFieldNames</value><!-- 输出key时是否使用双引号,默认为true -->
                        <value>WriteMapNullValue</value> <!--是否输出值为null的字段,默认为false -->
                        <!--<value>WriteNullNumberAsZero</value> 数值字段如果为null,输出为0,而非null -->
                        <!--<value>WriteNullBooleanAsFalse</value> Boolean字段如果为null,输出为false,而非null -->
                        <value>WriteNullStringAsEmpty</value> <!--字符类型字段如果为null,输出为"",而非null -->
                        <value>WriteNullListAsEmpty</value> <!--List字段如果为null,输出为[],而非null -->
                        <value>DisableCircularReferenceDetect</value><!-- 禁止循环引用,如$.ref之类 -->
					</list>
				</property>
			</bean>
		</mvc:message-converters>
	</mvc:annotation-driven> 
    
	<context:component-scan base-package="${restPkg}" />

	<!-- swagger 配置 -->
	<context:component-scan base-package="cn.ibaijia.jsm.swagger" />
	<mvc:resources location="classpath:/META-INF/swagger/" mapping="/swagger/**"  cache-period="31556926" />
		
	<!--注解说明 ,配置了包扫描之后，<context annotation-config />就可以移除了
    <context:annotation-config />--> 
    
    <!-- 国际化资源 -->
    <bean id="localeChangeInterceptor" class="org.springframework.web.servlet.i18n.LocaleChangeInterceptor" />
    <bean id="cookieLocaleResolver" class="org.springframework.web.servlet.i18n.CookieLocaleResolver">
    	 <property name="defaultLocale" value="zh_CN"/>
    </bean>
    <bean id="localeResolver" class="org.springframework.web.servlet.i18n.SessionLocaleResolver">
        <property name="defaultLocale" value="zh_CN"/>
    </bean>
	
	<mvc:resources mapping="/static/**" location="/static/" cache-period="31556926"/>
	<mvc:resources mapping="/resource/**" location="/resource/" cache-period="31556926"/>

	<mvc:interceptors>
		 <!--国际化资源切换（根据请求参数中的locale参数自动切换）-->
        <mvc:interceptor>
            <mvc:mapping path="/**"/>
            <mvc:exclude-mapping path="/resource/**"/>
            <mvc:exclude-mapping path="/static/**"/>
            <ref bean="localeChangeInterceptor"/>
        </mvc:interceptor>
		<mvc:interceptor>
			<mvc:mapping path="/v*/**"/>
			<mvc:exclude-mapping path="/resource/**"/>
			<mvc:exclude-mapping path="/static/**"/>
			<bean class="cn.ibaijia.jsm.interceptor.JsmRestInterceptor"></bean>
		</mvc:interceptor>
	</mvc:interceptors>  
	
	<!-- 配置视图解析 -->
	<bean
		class="org.springframework.web.servlet.view.InternalResourceViewResolver">
		<property name="prefix" value="/WEB-INF/jsp/" />
		<property name="suffix" value=".jsp" />
	</bean>
	
	<!-- 配置文件上传  5M-->
	<bean id="multipartResolver"
		class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
		<property name="maxUploadSize" value="20971520000" /><!-- 总大小20G -->
	</bean>

    <!-- jsm service -->
    <context:component-scan base-package="cn.ibaijia.jsm.aop" />

    <aop:aspectj-autoproxy proxy-target-class="true">
        <aop:include name="restAop"></aop:include>
    </aop:aspectj-autoproxy>
</beans>