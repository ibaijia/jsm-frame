# reset api no auth check , user no permission check, config hot load
dev.model=false
# session storage , true,false
jedis.session=true
# 30*60s*8
session.timeout=14400
# at expire time
at.expire.time=300000

#validator.package
validator.package=${restPkg}.validator

#swagger
swagger.enable=true
swagger.apis.package=${restPkg}
swagger.apis.title=${projectName} RESTful APIs
swagger.apis.description=${projectName} APIs
swagger.apis.version=1.0
swagger.apis.headers=${r'jsm-token:{dev_token}'}

# api stat override by app.properties
api.stat=false
#api.stat.name=${projectName}
#api.stat.url=http://localhost:8082/jsm-admin/v1/api-stat/
#elastic.address=http://192.168.0.154:9200

#alarm.api.time = 30000
#alarm.max.count = 500

# api referer
default.referer=
<#noparse>
#regex valid
regex.valid.email=[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?
regex.valid.chinese= [\\u4E00-\\u9FA5]
regex.valid.ipv4=^\\d+\\.\\d+\\.\\d+\\.\\d+$
regex.valid.date=^\\d{4}-\\d{1,2}-\\d{1,2}$
regex.valid.datetime=^\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}$
regex.valid.url=[a-zA-z]+://[^\\s]*
regex.valid.idcard=^(\\d{6})(\\d{4})(\\d{2})(\\d{2})(\\d{3})([0-9]|X)$
regex.valid.telno=^(\\(\\d{3,4}-)|\\d{3.4}-)?\\d{7,8}$
regex.valid.mobno=^(13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\\d{8}$
regex.valid.number=^[0-9]*$
regex.valid.ennum=^[A-Za-z0-9]+$
regex.valid.account=^[a-zA-Z][a-zA-Z0-9_]+$
regex.valid.money=^(([1-9]\\d{0,9})|0)(\\.\\d{1,2})?$
regex.valid.bankcard=^(\\d{16}|\\d{19})$
regex.extract.money=(([1-9]\\d{0,9})|0)(\\.\\d{1,2})?
regex.extract.number=\\d+(\\.\\d+)?
</#noparse>

project.name=${projectName}
gen.model.package.name=${daoModelPkg}
gen.mapper.package.name=${daoMapperPkg}
gen.service.package.name=${servicePkg}
gen.rest.package.name=${restPkg}
gen.front.api.dir=