package ${servicePkg};

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import ${daoMapperPkg}.SeqMapper;
import ${daoModelPkg}.Seq;
import cn.ibaijia.jsm.annotation.ClusterLockAnn;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SeqService extends BaseService {

    @Resource
    private SeqMapper seqMapper;

    private Seq oauthAppId = new Seq("oauth-app-id", "", 1L);

    @ClusterLockAnn
    @Transactional(propagation = Propagation.NEVER)
    public String getNextAppId() {
        return getDailyBeginCode(oauthAppId, 4);
    }

    private String getDailyBeginCode(Seq seq, Integer length) {
        try {
            String date = DateUtil.format(DateUtil.currentDate(), DateUtil.COMPACT_DATE_PATTERN);
            Seq dbSeq = seqMapper.findByName(seq.name);
            if (dbSeq == null) {
                dbSeq = new Seq();
                BeanUtil.copy(seq, dbSeq);
                dbSeq.prefix = date;
                seqMapper.add(dbSeq);
            } else {
                if (!dbSeq.prefix.endsWith(date)) {
                    dbSeq.prefix = date;
                    dbSeq.seqVal = 1L;
                } else {
                    dbSeq.seqVal++;
                }
                seqMapper.update(dbSeq);
            }
            if (StringUtil.isEmpty(length)) {
                length = 5;
            }
            logger.debug("seq:{}", StringUtil.toJson(seq));
            String code = seq.prefix + dbSeq.prefix + StringUtil.fill(dbSeq.seqVal, length);
            return code;
        } catch (Exception e) {
            logger.error("getDailyBeginCode error!", e);
            return null;
        }
    }

    private String getYearlyBeginCode(Seq seq, Integer length) {
        try {
            String date = DateUtil.format(DateUtil.currentDate(), "yyyy");
            Seq dbSeq = seqMapper.findByName(seq.name);
            if (dbSeq == null) {
                dbSeq = new Seq();
                BeanUtil.copy(seq, dbSeq);
                dbSeq.prefix = date;
                seqMapper.add(dbSeq);
            } else {
                if (!dbSeq.prefix.startsWith(date)) {//每年seqVal重置
                    dbSeq.prefix = date;
                    dbSeq.seqVal = 1L;
                } else {
                    dbSeq.seqVal++;
                }
                seqMapper.update(dbSeq);
            }
            if (StringUtil.isEmpty(length)) {
                length = 5;
            }
            String code = seq.prefix + dbSeq.prefix + StringUtil.fill(dbSeq.seqVal, length);
            return code;
        } catch (Exception e) {
            logger.error("getYearlyBeginCode error!", e);
            return null;
        }
    }

}