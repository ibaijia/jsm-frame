<#setting number_format="#"/>
spring:
  profiles:
    active: env
  freemarker:
    checkTemplateLocation: false
  messages:
    basename: messages/message

server:
  port: 8080
  servlet:
    context-path: /${projectName}

logging:
  config: classpath:log4j2.xml

jsm:
  projectName: ${projectName}
  clusterId: ${projectName}
  dev.model: true
  jedis.session: true
  kafka.logTopic: jsmLog
  web:
    maxUploadSize: 209715200
    encoding: UTF-8
    staticDir: /static/
  mybatis:
    typeAliasesPackage: ${daoModelPkg},${restVoPkg}
    mapperLocations:
     - classpath*:mappers/*Mapper.xml
    pageDbDialect: mysql
  ehcache:
    configLocation: classpath:ehcache.xml
  swagger:
    enable: true
    restPackage: '${restPkg}'
    title: '${projectName} RESTful APIs'
    description: '${projectName} APIs'
    version: '1.0'
    headers: '${r'jsm-token:{dev_token}'}'
  gen:
    package:
      model: ${daoModelPkg}
      mapper: ${daoMapperPkg}
      service: ${servicePkg}
      rest: ${restPkg}
