<?xml version="1.0" encoding="UTF-8"?>
<ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.ehcache.org/ehcache.xsd">
	<diskStore path="java.io.tmpdir" />
	<defaultCache maxElementsInMemory="10000" 
				  eternal="false" 
				  overflowToDisk="true" 
				  timeToIdleSeconds="120"
				  timeToLiveSeconds="120" 
				  diskPersistent="false" 
				  diskExpiryThreadIntervalSeconds="120"
				  />
	<!--maxElementsInMemory:缓存可以存储的总记录量-->
	<!--eternal:缓存是否永远不销毁 -->
	<!--overflowToDisk:当缓存中的数据达到最大值时，是否把缓存数据写入磁盘-->
	<!--timeToIdleSeconds:当缓存闲置时间超过该值，则缓存自动销毁-->
	<!--timeToLiveSeconds:缓存创建之后，到达该缓存自动销毁-->
	<cache name="queryCache"
		   maxElementsInMemory="10000" 
		   eternal="false"
		   timeToIdleSeconds="120"
		   timeToLiveSeconds="120" 
		   overflowToDisk="true" />
	<cache name="queryCacheEternal"
		   maxElementsInMemory="10000" 
		   eternal="true"
		   overflowToDisk="true" />
</ehcache>
