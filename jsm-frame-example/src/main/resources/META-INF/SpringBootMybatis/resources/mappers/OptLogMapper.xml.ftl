<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://www.mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${daoMapperPkg}.OptLogMapper">

    <insert id="add" parameterType="OptLog" useGeneratedKeys="true" keyProperty="id">
        insert into opt_log_t (ip,mac,uid,funcName,optDesc,time) values (${r'#{ip}'},${r'#{mac}'},${r'#{uid}'},${r'#{funcName}'},${r'#{optDesc}'},${r'#{time}'})
    </insert>

    <select id="pageList" resultType="OptLog" parameterType="Page">
    	select t.* from opt_log_t t ,user_t u where t.uid=u.id
		<if test="queryMap != null and queryMap.userName != null">
    	 and u.username like concat(concat('%',${r'#{queryMap.userName}'}),'%')
    	</if>
    	<!-- 操作类型查询 -->
    	<if test="queryMap != null and queryMap.funcName != null">
    	 and t.funcName  like concat(concat('%',${r'#{queryMap.funcName}'}),'%')
    	</if>
    	<!-- 操作时间查询 -->
    	<if test="queryMap != null  and queryMap.startTime != null">
    	<![CDATA[
				AND time >=${r'#{queryMap.startTime}'}
               ]]>
    	</if>
    	<if test="queryMap != null  and queryMap.endTime != null">
    	<![CDATA[
				AND time <= ${r'#{queryMap.endTime}'}
               ]]>
    	</if>
    	 order by t.id desc
    </select>
  
</mapper>