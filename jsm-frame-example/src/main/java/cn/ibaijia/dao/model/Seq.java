package cn.ibaijia.dao.model;


public class Seq extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Seq() {
		super();
	}
	public Seq(String name, String prefix, Long seqVal) {
		super();
		this.name = name;
		this.prefix = prefix;
		this.seqVal = seqVal;
	}
	
	public String name;
	public String prefix;
	public Long seqVal;
}
