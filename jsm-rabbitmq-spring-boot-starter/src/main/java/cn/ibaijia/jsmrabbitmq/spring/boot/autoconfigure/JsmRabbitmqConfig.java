package cn.ibaijia.jsmrabbitmq.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author longzl
 */
@Configuration
@EnableRabbit
@ComponentScan(basePackages = {"cn.ibaijia.jsm.stat.strategy"})
public class JsmRabbitmqConfig {

    @Bean
    @ConditionalOnProperty(name = AppContextKey.LOG_IDX)
    public Queue createLogQueue() {
        return new Queue(AppContext.get(AppContextKey.LOG_IDX));
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.API_STAT_IDX)
    public Queue createApiStatQueue() {
        return new Queue(AppContext.get(AppContextKey.API_STAT_IDX));
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.SYS_STAT_IDX)
    public Queue createSysStatQueue() {
        return new Queue(AppContext.get(AppContextKey.SYS_STAT_IDX));
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.ALARM_IDX)
    public Queue createAlarmQueue() {
        return new Queue(AppContext.get(AppContextKey.ALARM_IDX));
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.OPT_LOG_IDX)
    public Queue createOptLogQueue() {
        return new Queue(AppContext.get(AppContextKey.OPT_LOG_IDX));
    }

}