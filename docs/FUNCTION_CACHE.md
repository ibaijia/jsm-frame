
### 1. 缓存注解[CacheAnn]
+  @CacheAnn 一般使用在 service层做查询缓存，支持缓存到caffeine和redis以二级缓存
+ 示例：
``` java
    // CacheType.L1 一级缓存， CacheType.L2 二级缓存， CacheType.L1L2 一二级缓存同时用
    @CacheAnn(cacheType = CacheType.L1L2, expireSeconds = 100)
    public String getNameByUserId(Long userId) {
        UserInfo userInfo = userMapper.findUserInfoById(userId);
        if (userInfo != null) {
            return userInfo.name;
        }
        return null;
    }
```

### 2. Redis封装[JedisService]
+ JedisService 提供一些常用的 redis操作，以及可以执行指定命令(实现JedisCmd接口)
+ 示例：
``` java
    // 保存key value
    String key = "efsStatus";
    jedisService.setex(key,8600,StringUtil.toJson(resModel));
    // 执行指定命令
    jedisService.exec(new JedisCmd<Long>() {
        @Override
        public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) {
            if (cluster) {
                return jedisCluster.zadd(key,1,uri);
            } else {
                return jedis.zadd(key,1,uri);
            }
        }
    });
```

### 3. 二级缓存[CacheService]
+ Caffeine+Redis 二级缓存 封装类
+ 相关配置
``` yaml
jsm:
  cache:
    l1:
      live: 10
      init: 500
      max: 2000
    l2:
      removeOnUpdate: true
      clusterChannel: jsm-l2-cluster-channel
```