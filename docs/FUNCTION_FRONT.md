### 插件介绍
+ jsm-spring-boot-starter 2.2.0 开始提供了前端插件jsm-tool, 用于生成前端api对应的TS或者JS代码。<br>
+ 解决问题:
  1. 少写代码，基于后台自动生成代码，特别是TS可以少很多types对象定义
  2. 字段名直接映射到前端，减少前后台对接字段时，眼花导致的调试工作量
  3. 字段注释直接映射到前端，开发工具可清楚看到每个API和以及出入参数的说明

### 安装
npm安装
``` shell
npm install jsm-tool -D
```
yarn安装
``` shell
yarn add jsm-tool -D
```
### 配置package.json
安装成功后在package.json中 scripts中添加如下代码
``` json lines
    "jsm:init": "jsm init --with-ajax-lib", //初始化里加ajaxlib 加上--with-ajax-lib ,默认不生成
    "jsm:genAjaxLib": "jsm genAjaxLib", //已有项目中使用 生成的ajax-lib
    "jsm:gen": "jsm gen"
```
>注：jsm:genAjaxLib 和 jsm:gen 只需要执行一次。为了安全，重复执行不会覆盖已生成的文件。ajax-lib需要安装axios

### 初始化配置
配置好package.json后，执行如下命令完成初始化配置
``` shell
npm run jsm:init
```
执行完成后会在项目目录下生成jsm.config.json,内容如下
``` json lines
{
    "apiDir": "src/api",   //生成的API目录，可自行修改
    "apiModelDir": "./model",  //生成的ApiModel目录，可自行修改（TS）
    "ajaxLibPath": "../../axios/index", //本项目中的ajax库路径，通常是需要修改的
    "ajaxLibName": "axios", //本项目中的ajax库别名，可自行修改
    "override": true, //如果文件已经存在，是否覆盖
    "lang": "ts",  //生成的ts脚本或者js脚本
    "apis": [   //多个api服务配置在这个里面
        {
            "api": "http://localhost:8080/base-api", //远程api的服务名，通常是需要修改的
            "dir": "base-api", //对应本地目录名，建议与服务名一致
            "baseUri": "base-api" //API的基础URI,支持字符串"base-api"或者代码(import.meta.env.VITE_BASE_URL), 通常是需要修改的
        }
    ]
}
```
### 生成代码
配置好jsm.config.json后，执行如下命令完成API到前端代码的自动生成,
>注: 需要对应API服务,启用swagger, jsm.swagger.enable = true
``` shell
npm run jsm:gen
```
生成后的目录结构如下
```
src
├── App.vue
├── api
│   ├── base-api
│   │   ├── AreaApi.ts
│   │   ├── HelloApi.ts
│   │   ├── PermissionApi.ts
│   │   ├── RoleApi.ts
│   │   ├── UserApi.ts
│   │   ├── UserInfoApi.ts
│   │   └── model
│   │       ├── AreaApiModel.ts
│   │       ├── HelloApiModel.ts
│   │       ├── PermissionApiModel.ts
│   │       ├── RoleApiModel.ts
│   │       ├── UserApiModel.ts
│   │       └── UserInfoApiModel.ts
│   └── oauth-api
│       ├── HelloApi.ts
│       ├── OauthApi.ts
│       ├── WxMpApi.ts
│       ├── WxSnsApi.ts
│       ├── WxSnsMenuApi.ts
│       └── model
│           ├── HelloApiModel.ts
│           ├── OauthApiModel.ts
│           ├── WxMpApiModel.ts
│           ├── WxSnsApiModel.ts
│           └── WxSnsMenuApiModel.ts
```
生成后的 API样例
``` java
/**
 * @description User登录获取图形验证码API notes:根据filename生成验证码
 * @Path参数
 * @param key,String,否,,
 * @响应Body UserApiModel.CaptchaResp
 * @param result,FileVo,否,响应结果,
 * @param code,String,是,1001表示成功，其它参照错误代码表,
 * @param message,String,是,响应码对应描述,
 * @return UserApiModel.CaptchaResp
 */
export const captcha = (key: string) => {
    return axios.get(`${baseUrl}/v1/user/captcha/${key}`)
}
/**
 * @description User修改密码
 * @请求Body UserApiModel.ChangePwdReqBody
 * @param password,String,是,密码,
 * @param newPassword,String,是,新密码,
 * @响应Body UserApiModel.ChangePwdResp
 * @param result,Integer,否,响应结果,
 * @param code,String,是,1001表示成功，其它参照错误代码表,
 * @param message,String,是,响应码对应描述,
 * @return UserApiModel.ChangePwdResp
 */
export const changePwd = (reqBody: model.ChangePwdReqBody) => {
    return axios.put(`${baseUrl}/v1/user/change-pwd`, reqBody)
}
```

生成后的 API Model样例
``` java
/**
 * @description ChangePwdReqBody
 * @param password,string,是,密码,
 * @param newPassword,string,是,新密码,
 */
export type ChangePwdReqBody = {
    password: string
    newPassword: string
}
/**
 * @description ChangePwdResp
 * @param result,number,否,响应结果,
 * @param code,string,是,1001表示成功，其它参照错误代码表,
 * @param message,string,是,响应码对应描述,
 */
export type ChangePwdResp = {
    result?: number
    code: string
    message: string
}
```

### 额外说明
1. 前端在使用过程中如有什么问题或建议可以提出来讨论
2. 框架只是解决大部分的问题，如有特殊情况，可自己写代码处理，并不会限制到你写代码的方式