### 一. 使用工具 [jsm-creator] 生成脚手架

#### 生成[spring-boot]脚手架
参照如下步骤
![步骤](gen_springboot.jpg)

#### 生成[spring-cloud]脚手架
第一步选择SpringCloudMybatisCreator，其它步骤操作一样。

### 二. 脚手架目录介绍
```
app-api #项目名
├── Dockerfile #Dockerfile
├── pom.xml # pom包配置文件
└── src
    ├── main
    │ ├── java
    │ │ └── com
    │ │     └── xxx
    │ │         └── app
    │ │             ├── StartApp.java #启动类
    │ │             ├── consts #常量配置目录
    │ │             │ ├── AppConstants.java
    │ │             │ ├── AppPairs.java
    │ │             │ └── Permissions.java
    │ │             ├── dao   #数据库 dao层
    │ │             │ ├── mapper #表一一对应的javaMapper
    │ │             │ │ └── SeqMapper.java
    │ │             │ └── model #与表一一对应的javaModel
    │ │             │     └── Seq.java
    │ │             ├── rest  #控制层 restApi
    │ │             │ └── HelloApi.java #保留API HEALTHCHECK
    │ │             └── service #业务层
    │ │                 └── SeqService.java
    │ └── resources
    │     ├── application-env.yml #环境配置文件（打包里会被指定环境的配置文件覆盖）
    │     ├── application.yml #基础配置文件
    │     ├── log4j2.xml #日志配置文件
    │     ├── mappers #mybatis配置目录
    │     │ ├── OptLogMapper.xml
    │     │ └── SeqMapper.xml #与javaMapper一一对应的xml配置文件
    │     └── messages #国际化配置
    │         ├── message_en_US.properties
    │         └── message_zh_CN.properties
    └── test
        └── java
            └── com
                └── xxx
                    └── app
                        └── test
                            ├── free
                            │ └── JunitTest.java #不需要spring容器的单元测试
                            └── gen 
                                ├── Gen.java #根据表名生成代码的
                                └── GenApiDoc.java #生成API文档的

```

### 三. 启动服务

启动类 com.xxx.app.StartApp.java

### 四. 访问Swagger
+ 启动后访问：http://{host}:{port}/{appName}/static/swagger/index.html

+ 框架集成swagger，配置如下。
``` yaml
jsm:
  swagger:
    enable: true
    restPackage: 'com.xxx.app.rest'
    title: 'app-api Example RESTful APIs'
    description: '内部 APIs'
    version: '1.0'
    headers: '' #自定义header,格式 headerName:headerValue
```

### 五. JSM框架介绍
![步骤](jsm_arch.jpg)