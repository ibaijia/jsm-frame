### 轻量级熔断功能[RestFuseAnn]
``` java
public @interface RestFuseAnn {

    /**
     * 错误 错误次数
     */
    int value() default 3;

    /**
     * 熔断 统计时长
     */
    int duration() default 60;

    /**
     * 熔断 时长
     */
    int fuseDuration() default 120;

    /**
     * 熔断（错误）时处理方法
     */
    String fallbackMethod();

    /**
     * 熔断 自定义Key
     */
    String key() default "";
    
}
```
+ 示例
``` java
@RestController
@RequestMapping("/v1/test-fuse/")
public class TestFuseApi extends BaseRest {

    /**
     * 异常里调用 helloError
     * @param name
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
    @RestFuseAnn(fallbackMethod = "helloError")
    public RestResp<String> hello(@RequestParam("name") String name) {
        throw new RuntimeException("my error.");
//        return success("hello");
    }

    public RestResp<String> helloError(@RequestParam("name") String name) {
        return success("helloError");
    }

}
```

### 轻量级限流功能[RestLimitAnn]
``` java
public @interface RestLimitAnn {

    /**
     * 同时最大请求数
     */
    int value() default 100;

    /**
     * 限流后处理方法
     */
    String fallbackMethod();

    /**
     * 熔断 自定义Key
     */
    String key() default "";

}

```

``` java
@RestController
@RequestMapping("/v1/test-limit/")
public class TestLimitApi extends BaseRest {

    /**
     * 超过2个并发 调用 helloLimit
     * @param name
     * @return
     */
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @RestLimitAnn(value = 2, fallbackMethod = "helloLimit")
    public RestResp<String> hello(@RequestParam("name") String name) {
        return success("hello");
    }

    public RestResp<String> helloLimit(@RequestParam("name") String name) {
        return success("helloLimit");
    }

}
```