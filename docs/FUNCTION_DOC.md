
### 代码生成
+ com.xxx.app.test.gen.GenService 根据指定的数据库生成*Mapper.xml,*Mapper.java,(model)*.java,*Service.java,*Api.java,默认如下配置.
``` yaml
jsm:
    gen:
        package:
           model: com.xxx.app.dao.model
           mapper: com.xxx.app.dao.mapper
           service: com.xxx.app.service
           rest: com.xxx.app.rest
```
+ 常用示例：
``` java
	//生成对应的 Task.java TaskMapper.java TaskMapper.xml 不强制覆盖
	genService.gen("task_t");
	//生成对应的 Task.java TaskMapper.java TaskMapper.xml 且强制覆盖(谨用)
	genService.gen("task_t",true);
	//生成对应的 Task.java TaskMapper.java TaskMapper.xml TaskService.java TaskApi.java 不强制覆盖
	genService.gen("task_t",false, true);
```

### 参数验证
+ 请求Model实现 ValidateModel 接口，
+ 请求Model 中需要各个字段的验证规则使用  @FieldAnn注解进行配置,实现应用中的验证分三类验证。 格式验证，如电话，身份证，数字等 。比较验证，需要用到el 如:model属性比较。高级验证:比如需要查询数据库，
+ 示例（验证顺序 validate(对象方法) --> <validateModel>Validator(spring容器对象) -->  required(字段) --> type(字段) --> regex(字段) --> minLen(字段) --> maxLen(字段) --> el(字段) --> validateMethod(字段)）:

``` java
   public class Student implements ValidateModel {
	
	//简单格式验证　适用于　单个字段 类型，见FieldType
	@FieldAnn(required = false,type = FieldType.MOB_NO)
	public String mobile;
	//自定义正则　适用于 单个字段规则验证,如个性化的密码规则
	@FieldAnn(required = false,regex = " [\\u4e00-\\u9fa5]",message = "中文名必须是中文")
	public String cnName;
	//是否必填
	@FieldAnn(required = true)
	public Integer score1;
	//el验证 vm为 Model自身　不匹配EL返回 message,效果同validateMethod,适用于请求对象内部多个字段的逻辑验证,如a字段为true时,b字段一定不能为空
	@FieldAnn(required = false,el="#vm.score2 > #vm.score1" ,message = "score2 应该大于 score1")
	public Integer score2;
	//高级验证，需要实现ValidateCallback接口的Spring BeanName或者BeanType(包名+类名),适用于查询DB的校验,如用户名验重
	@FieldAnn(required = false,validateBean = "studentValidator")
	public String name;
	//el中使用到session
	@FieldAnn(required = false,el = "#session.get('123') == #vm.ss",message = "session没有key123")
	public String ss;
	//指定 ValidateModel中的验证方法 默认 fieldNameValidate,适用于请求对象内部多个字段的逻辑验证,如a字段为true时,b字段一定不能为空
	@FieldAnn(required = false,validateMethod="scoreValidate")
	public String score;
	
	//默认mobile字段 会尝试调用 mobileValidate方法
	public String scoreValidate(){
	}
	//全局验证方法
	public String validate(){
      return null;
    }
}

//全局验证器 sample
@Component
public class UpdateCnProductReqValidator implements ValidateCallback {
    @Override
    public String exe(Object o, ValidateModel validateModel) {
        return null;
    }
}
   
```


### 权限配置
+ Controller 方法上增加 @RestAnn ，默认无权限拦截,authType=AuthType.NONE无效，多个用 支持 | 和 &
+ 示例：
``` java
	@RestAnn(authType = AuthType.JSM_WEB, permission = "SYSTEM_MANAGEMENT")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public RestResp<Integer> delete(@PathVariable("ids") String ids) {
	}
```
+ AuthType 枚举
```java
public enum AuthType {
    NONE(0, "none"),
    OAUTH_CODE(1, "codeAuth"),//oauth_code
    OAUTH_IMPLICIT(2, "implicitAuth"),//oauth_implicit
    OAUTH_PASSWORD(3, "passwordAuth"),//oauth_password
    OAUTH_CLIENT(4, "clientAuth"),//oauth_client
    JSM_APP(5, "jsmAppAuth"),//blowfish at=bf(at+time) ht=md5(at+time)
    JSM_WEB(6, "jsmWebAuth"),//base64 at=base64(at+time) ht=md5(at+time)

    JSM_TOKEN(7, "jsmTokenAuth"),//JSM_APP中的 md5(at+time)+bf(at+time)=token
    JSM_GATEWAY(8, "jsmGatewayAuth"),//运行在GATEWAY 后面，直接绑定jsm-gw-{key}的数据到Oauth对象
    JSM_DIRECT(9, "jsmDirectAuth")//直接解析token拿到uid 拿用户信息 配置userService 拿permissions配合permission
}
```

### 事务配置
+ Controller 方法上增加 @RestAnn 默认不开启事务,transaction = Transaction.NONE
+ 注:默认数据库读写分离规则 @RestAnn(transaction = Transaction.READ) 连从数据库，否则连主数据库
+ 示例：
``` java
	@RestAnn(transaction = Transaction.WRITE)
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public RestResp<Integer> delete(@PathVariable("ids") String ids) {
	}
```

### 日志配置
+ Controller 方法上增加 @RestAnn (只支持AuthType.JSM_*),logType日志模块 log日志描述，支持变量
+ 示例：
``` java
	@RestAnn(authType = AuthType.JSM_WEB, logType = AppConsts.LOG_TPYE_USER, log = "[#{logUser}]重置密码[#{logContent}][#{logStatus}]")
	@PutMapping(value = "/reset-password")
	public RestResp<Integer> resetPassword(@RequestBody UserResetPwdReq req) {
		OptLogUtil.setLogUser((String)WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
		Long userId = WebContext.currentUser().getUid();
		OptLogUtil.setLogContent(userId+"重置用户"+req.id);
		RestResp<Integer> resp = new RestResp<Integer>();
		resp.result = userService.resetPassword(req.id);
		OptLogUtil.setLogStatus("成功");
		return resp;
	}
```

### 分页
+ 直接使用 Controller 方法参数使用 cn.ibaijia.jsm.dao.model.Page, page参数直接传到mapper中作为参数查询
+ 示例：
``` java
	@RestAnn(authType = AuthType.JSM_WEB, transaction = Transaction.READ)
	@GetMapping(value = "/page")
	public RestResp<Page<UserInfo>> pageListUser(Page<UserInfo> page) {
	}
```

### 会话上下文
+ jsm封装了redis做分布session和servlet自身session,可通过如下配置.
``` yaml
jsm:
  jsm.session.type: default #L2 reids L1L2 cacheService
```
+ 获取Session(只支持AuthType.JSM_*)，示例：
``` java
	WebContext.currentSession() 可获取当前session
```
+ 获取OauthContext(只支持AuthType.OAUTH_*),示例
``` java
	OauthContext.getVerifyResult() 可获取当前解Token得到的消息
```

### 分布式锁
有以下三种方式
+ 注解@ClusterLockAnn
``` java
    @ClusterLockAnn
    public String getNextAppId() {
        return getDailyBeginCode(oauthAppId, 4);
    }
```
+ 代码块 try-catch-finally 配合 jedisService.lock(key) 和 jedisService.unlock(key) (不推荐)
+ LockService
 ``` java
    @Autowired
    private LockService lockService;
```

### DRUID配置
+ 配置
``` yaml
jsm:
  druid:
    enableMonitor: true
    uri: '/static/db-monitor/*'
    loginUsername: admin
    loginPassword: pwd
    allow: ''
    deny: ''
    filters: stat,wall,slf4j
    logSlowSql: true
    slowSqlMillis: 10
    mergeSql: true
```
+ 访问控制台
http://{host}:{port}/{appName}/<jsm.druid.uri>/index.html

### 常用配置
+ web配置
``` yaml
jsm:
  web:
    maxUploadSize: 209715200
    encoding: UTF-8
    staticDir: /static/
    staticPath: /public/
```
+ JSON序列化配置
``` yaml
jsm:
  json:
    showNull: true
    booleanNullFalse: true
    numberNullZero: true
    stringNullEmpty: true
    arrayNullEmpty: true
    namingStrategy: lowerCamelCase # lowerCamelCase snakeCase upperCamelCase lowerCase kebabCase
```
+ Mybatis配置
``` yaml
jsm:
  mybatis:
    typeAliasesPackage: com.xxx.app.dao.model,com.xxx.app.rest.vo
    mapperLocations:
     - classpath*:mappers/*Mapper.xml
    pageDbDialect: mysql
```
+ 日志统计配置
``` yaml
jsm:
  elastic:
    address:
  log:
    type: redis #  none console(consoleStrategy) elastic(elasticLogStrategy) redis(redisLogStrategy) kafka(kafkaLogStrategy) rabbitmq(rabbitmqLogStrategy) custom(自定义的customLogStrategy)
    idx: jsm_log_idx
  apiStat:
    type: redis #  none console elastic kafka rabbitmq custom
    idx:  jsm_api_stat_idx
  sysStat:
    type: redis
    idx: jsm_sys_stat_idx
    interval: 300
  alarm:
    type: redis
    idx: jsm_alarm_idx
  optLog:
    type: redis
    idx: jsm_opt_log_idx
```

### 几个通用上下文
+ AppContext 用于读取环境配置 优先级 Spring Environment > AppContext propMap > System property  > System env
+ WebContext 用于读取web运行时上下文(只支持AuthType.JSM_*)
+ OauthContext 用于读取Oauth解token的结果(只支持AuthType.OAUTH_*)
+ SpringContext 用于读取spring容器对象

### 常用工具类
+ StringUtil
+ JsonUtil
+ DateUtil
+ LogUtil
+ ExcelUtil
+ HttpClientUtil
+ HttpBrowser
+ EncryptUtil
+ BeanUtil
+ 更多见 cn.ibaijia.jsm.utils 包。