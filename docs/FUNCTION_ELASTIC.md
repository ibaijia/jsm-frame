### 查询构建器 Builder

### 查询
    - Bool 布尔条件 相当于括号容器
        BoolItem 有如下几种情况
            Must  相当于 and 统计相关度
            MustNot  相当于 not exists
            Should  相当于 or
            Filter  相当于 and 但不统计相关度

###  BoolItem中的条件        
    - MatchCondition 匹配条件
    - TermCondition 等于条件
    - RangeCondition 范围条件
        RangeOperator 范围操作符
        RangeItem 
    - RangeCondition
    - SortCondition

### 例子
+ env=xxx and appName=xxx and name=xxx and time > xx and time < xx
``` java
    Builder builder = new Builder();
        builder.addSort(new SortCondition("time", false));

        builder.bool()
                .must()
                .term("env", elasticConfig.getEnv())
                .term("appName", appName)
                .term("name", name)
                .range("time",RangeOperator.GT, startTime.getTime())
                .range("time", RangeOperator.LT, endTime.getTime());

        String queryJson = builder.build();
        logger.info("queryJson:{}", queryJson);
        List<ElasticObject<AmsAlarmInfo>> list = elasticService.query(queryJson, new TypeReference<List<ElasticObject<AmsAlarmInfo>>>() {
        });
```
+ env=xxx and appName=xxx and time > xx and time < xx
    if req.alarmName != null then
      alarmName = req.alarmName
    end 
``` java
        Builder builder = new Builder();
        builder.from(req.pageNo * req.pageSize).size(req.pageSize).addSort(new SortCondition("time", false));

        builder.bool()
                .must()
                .term("env", elasticConfig.getEnv())
                .term("appName", req.appName)
                .range("time", RangeOperator.GT, req.startTime.getTime())
                .range("time", RangeOperator.LT, req.endTime.getTime())
                .termIfTrue(req.alarmName != null,"alarmName", req.alarmName);

        String queryJson = builder.build();
        logger.info("queryJson:{}", queryJson);
        List<ElasticObject<AmsAlarmInfoVo>> esList = elasticService.query(queryJson, new TypeReference<List<ElasticObject<AmsAlarmInfoVo>>>() {
        });
```
+ env=xxx and appName=xxx and time > xx and time < xx
    if !StringUtil.isEmpty(fieldName) && minVal != null && maxVal == null then
       fieldName > minVal
    end
    if !StringUtil.isEmpty(fieldName) && minVal == null && maxVal != null then
        fieldName < maxVal
    end
    if !StringUtil.isEmpty(fieldName) && minVal != null && maxVal != null then
        fieldName > minVal and fieldName < maxVal
    end
``` java
        Builder builder = new Builder();
        builder.addSort(new SortCondition("time", false));
        builder.bool()
                .must()
                        .term("env", elasticConfig.getEnv())
                        .term("appName", appName)
                        .range("time", RangeOperator.GT, startTime.getTime())
                        .range("time", RangeOperator.LT, endTime.getTime())
                        .rangeIfTrue(!StringUtil.isEmpty(fieldName) && minVal != null && maxVal == null,
                                fieldName, RangeOperator.LT, minVal)
                        .rangeIfTrue(!StringUtil.isEmpty(fieldName) && minVal == null && maxVal != null,
                                fieldName, RangeOperator.GT, maxVal)
                        .boolIfTrue(!StringUtil.isEmpty(fieldName) && minVal != null && maxVal != null,
                                new Bool(new Should()
                                        .range(fieldName, RangeOperator.LT, minVal)
                                        .range(fieldName, RangeOperator.GT, maxVal)
                                    )
                                );

        String queryJson = builder.build();
        logger.info("queryJson:{}", queryJson);
        List<ElasticObject<AmsSystemStat>> list = elasticService.query(queryJson, new TypeReference<List<ElasticObject<AmsSystemStat>>>() {
        });

```

### ElasticService 可创建和查询 功能比较全

### ElasticQueryService 只查询的Service,更量级不需要传入表的Mappings

### ElasticObject 查询返回的Es对象
