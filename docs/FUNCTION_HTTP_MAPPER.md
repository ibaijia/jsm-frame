### 远程API调用
+ 注解 @HttpMapperScan @HttpMapper  @HttpParam @HttpPathVariable  @HttpHeader @HttpMapping @HttpGetMapping @HttpPostMapping @HttpPutMapping @HttpDeleteMapping 支持 三方API调用
+ 示例 StartApp配置注解 @HttpMapperScan
``` java
    @HttpMapperScan(basePackages = {"com.xxx.app.httpmapper"})
    @MapperScan(basePackages={"com.xxx.app.dao.mapper"})
    @SpringBootApplication(exclude = {QuartzAutoConfiguration.class},scanBasePackages = "com.xxx.app")
    public class StartApp {
        private static Logger logger = LogUtil.log(StartApp.class);
        public static void main(String[] args) {
            SpringApplication.run(StartApp.class, args);
            logger.info("start ok.");
        }
    }
```
+ 指定包下面新建接口,支持环境变量${},和参数占位#{}
``` java
@HttpMapper(baseUri = "${service.baseUrl}")
public interface TestHttpMapper {

    @HttpMapping(method = RequestMethod.GET, uri = "/v1/test-get")
    public RestResp<String> testGet();

    /**
     * 访问url /v1/test-get/xxx?a=xxx
     * @param name1
     * @param name2
     * @return
     */
    @HttpMapping(method = RequestMethod.GET, uri = "/v1/test-get/#{name1}?a=#{name2}")
    public RestResp<String> testGetVariable(@HttpPathVariable("name1") String name1, @HttpPathVariable("name2") String name2);

    /**
     * 访问url /v1/test-get?name1=xxx&name2=xxx
     * @param name1
     * @param name2
     * @return
     */
    @HttpMapping(method = RequestMethod.GET, uri = "/v1/test-get")
    public RestResp<String> testGetParam(@HttpParam("name1") String name1, @HttpParam("name2") String name2);

    /**
     * Post请求
     * @param req
     * @return
     */
    @HttpMapping(method = RequestMethod.POST, uri = "/v1/test-post")
    public RestResp<String> testPost(AreaAddReq req);

    /**
     * /v1/test-post/xxx
     * @param name2
     * @param req
     * @return
     */
    @HttpPostMapping(uri = "/v1/test-post/#{name}")
    public RestResp<String> testPostVariable(@HttpPathVariable("name") String name2, AreaAddReq req);

    /**
     * header
     * name:xxx
     *
     * HttpHeader 支持List<Header> 或者 Set<Header>
     *
     * @param name
     * @param req
     * @return
     */
    @HttpPostMapping(uri = "/v1/test-post")
    public RestResp<String> testPostHeader(@HttpHeader("name") String name, AreaAddReq req);

}

```
+ 在Service中注入调用
``` java
    @Resource
    private TestHttpMapper testHttpMapper;
```