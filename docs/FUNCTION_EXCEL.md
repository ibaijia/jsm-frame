
### Excel 导出
+ 示例,创建导入Model UserExportVo
``` java
public class UserExportVo {

    @ExcelField(name = "记录ID", width = 20)
    public Long id;
    /**
     * defaultVal:NULL
     * comments:用户名
     */
    @ExcelField(name = "用户名", width = 20)
    public String username;
    /**
     * defaultVal:NULL
     * comments:密码
     */
    @ExcelField(ignore = true)
    public String password;
    /**
     * defaultVal:NULL
     * comments:手势密码
     */
    @ExcelField(ignore = true)
    public String gesturePwd;
    /**
     * defaultVal:NULL
     * comments:状态 -1 删除 1可用 2 禁用
     */
    @ExcelField(name = "状态", replaceRule = "删除|-1;可用|1;禁用|2")
    public Integer status;
}
```
``` java
    @ApiOperation(value = "User导出", notes = "")
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<ExcelVo> exportUser() {
        List<UserExportVo> list = new ArrayList<>();
        List<User> allUser = userService.findAll();
        for (User user : allUser) {
            UserExportVo userExportVo = new UserExportVo();
            BeanUtil.copy(user, userExportVo);
            list.add(userExportVo);
        }
        ExcelVo excelVo = new ExcelVo();
        //将List写入到workbook
        excelVo.workbook = ExcelUtil.writeToNew(list);
        excelVo.fileName = "所有用户信息.xlsx";
        return success(excelVo);
    }
   ```
+ 内置ExcelVo对象，同理还有其它一些内置Vo对象，如FileVo,XmlVo等。

### Excel导入
+ 示例，创建一个Model,使用@ExcelField注解
``` java
public class UserImportModel {

    @ExcelField(name = "记录ID", width = 20)
    public Long id;

    /**
     * defaultVal:NULL
     * comments:用户名
     */
    @ExcelField(name = "用户名", width = 20)
    public String username;
    /**
     * defaultVal:NULL
     * comments:密码
     */
    @ExcelField(ignore = true)
    public String password;
    /**
     * defaultVal:NULL
     * comments:手势密码
     */
    @ExcelField(ignore = true)
    public String gesturePwd;
    /**
     * defaultVal:NULL
     * comments:状态 UserImportModel-1 删除 1可用 2 禁用
     */
    @ExcelField(name = "状态", replaceRule = "删除|-1;可用|1;禁用|2")
    public Integer status;
}
```
+ 使用ExcelUtil.readExcel读并绑定到 UserImportModel
``` java
List<UserImportModel> importList = ExcelUtil.readExcel(file, 0, UserImportModel.class, 1);
```

### Excel分页读取
+ 传入page对象，根据page对象的pageNo和pageSize返回 指定的区间的数据
``` java
        String excelFile = "D:/test/test1.xlsx";
        Page<StudentImportModel1> page = ExcelUtil.readExcel(excelFile,0,StudentImportModel1.class,1,new Page(10));
```