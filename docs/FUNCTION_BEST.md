### 日志收集[Elastic]
![](log_arch.jpg)
+ jsm框架扩展了自己定义了JsmAppender，实现了日志异步收集处理。<br>

![](best_log.jpg)
+ 日志写入ES，配全AMS查询

### 持续集成[Jenkins]
![](cicd.jpg)
+ 配合Jenkins+Pipeline(Groovy)，发布到Docker集群
+ 持续集成是一种软件开发实践，即团队开发成员经常集成他们的工作，通常每个成员每天至少集成一次，也就意味着每天可能会发生多次集成。每次集成都通过自动化的构建（包括编译，发布，自动化测试）来验证，从而尽早地发现集成错误。
### 缓存集群[Redis]
![](best_cache.jpg)
+ N>=6,且为偶数, 能被(cluster-replicas+1)整除

### 数据集群[MySql]
![](best_db.jpg)
+ 注：配置jsm框架，读写分离

### 技术蓝图
![](biz_arch.jpg)