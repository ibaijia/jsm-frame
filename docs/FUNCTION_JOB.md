### 分布式Job锁
+ 注解@ClusterJobLockAnn
+ 示例
``` java
    @ClusterJobLockAnn
    public void doJob(){
        logger.info("TestJsmJob2:: begin.");
        logger.info("TestJsmJob2:: running.");
        logger.info("TestJsmJob2:: end.");
    }
```

### 自定义Job,继承BaseJsmJob抽象类或者实现JsmJob接口
+ 注:继承BaseJsmJob和实现JsmJob接口，则为普通原生job.
+ JsmJob接口及说明
``` java
public interface JsmJob {
     /**
      * 执行Job的初始操作，每次启动执行一次
      */
     void onLoad();
     /**
      * 动态获取可以来自数据库，也可以来自配置中心 每个周期都会调用
      * @return
      */
     String getCron();
     /**
      * 传入false 不执行 doJob ，实现Job动态开关 每个周期都会调用
      * @return
      */
     boolean isOpen();
     /**
      * 执行Job的业务内容
      */
     void doJob();
}
```
``` java
public abstract class BaseJsmJob implements JsmJob {

    protected Logger logger = LogUtil.log(getClass());

    @Override
    public void onLoad() {
        //Do Nothing
    }

    @Override
    public boolean isOpen() {
        return true;
    }
}
```

``` java
@Component
public class TestJsmJob1 extends BaseJsmJob {

    /**
     * 这个不会动态刷新 配置写到配置Bean 加@RefreshScope 也没用
     */
    @Value("${jobValue:1}")
    private String jobValue;
    @Resource
    private JobConfig jobConfig;

    @Override
    public void onLoad() {
        //TODO 这里可以做一些配置，比如检查并把Job写入配置中心或者DB
    }

    @Override
    public String getCron() {
        return jobConfig.getJob1Cron();
    }

    @Override
    public boolean isOpen() {
        return jobConfig.isJob1Open();
    }

    @ClusterJobLockAnn
    @Override
    public void doJob() {
        logger.info("TestJsmJob1:: begin.");
        logger.info("TestJsmJob1:: running getJob1Content:{} AppContext:{} jobValue:{}", jobConfig.getJob1Content(), AppContext.get("job1Content"), jobValue);
        logger.info("TestJsmJob1:: end.");
    }
}
```
### BaseQuartzJob 自定义QuartzJob【不推荐】