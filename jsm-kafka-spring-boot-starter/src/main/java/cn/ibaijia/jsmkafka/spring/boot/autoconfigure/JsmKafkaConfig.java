package cn.ibaijia.jsmkafka.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

/**
 * @author longzl
 */
@Configuration
@EnableKafka
@ComponentScan(basePackages = {"cn.ibaijia.jsm.stat.strategy"})
public class JsmKafkaConfig {

    @Bean
    @ConditionalOnProperty(name = AppContextKey.LOG_IDX)
    public NewTopic createLogTopic() {
        return new NewTopic(AppContext.get(AppContextKey.LOG_IDX), 1, (short)1);
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.API_STAT_IDX)
    public NewTopic createApiStatTopic() {
        return new NewTopic(AppContext.get(AppContextKey.API_STAT_IDX), 1, (short)1);
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.SYS_STAT_IDX)
    public NewTopic createSysStatTopic() {
        return new NewTopic(AppContext.get(AppContextKey.SYS_STAT_IDX), 1, (short)1);
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.ALARM_IDX)
    public NewTopic createAlarmTopic() {
        return new NewTopic(AppContext.get(AppContextKey.ALARM_IDX), 1, (short)1);
    }

    @Bean
    @ConditionalOnProperty(name = AppContextKey.OPT_LOG_IDX)
    public NewTopic createOptLogTopic() {
        return new NewTopic(AppContext.get(AppContextKey.OPT_LOG_IDX), 1, (short)1);
    }

}