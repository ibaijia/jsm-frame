package cn.ibaijia.jsm.stat.strategy;

import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * @Author: LongZL
 * @Date: 2021/11/19 10:38
 */
@Service("kafkaOptLogStrategy")
public class KafkaOptLogStrategy implements LogStrategy {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${" + AppContextKey.OPT_LOG_IDX + ":jsm_opt_log_idx}")
    private String idx;

    @Override
    public void write(Serializable object) {
        String text = null;
        if (object instanceof String) {
            text = (String) object;
        } else {
            text = JsonUtil.toJsonString(object);
        }
        kafkaTemplate.send(idx, text);
    }
}
