### 2.2.0

#### new
- 升级到jdk11
- spring-boot-starters 升级到 2.2.2.RELEASE以及其它一些jar包的升级
- 新增JsmJob接口和BaseJsmJob支持job开关及job cron表达式动态配置。
- 新增LockService提供默认基于Redis现实的分布式锁
- 优化生成器
- httpMapper支持HttpClientProcessor，实现自定义处理结果
- 优化swaggerUI
- 支持生成前端TS代码
- httpMapper支持 List<Header> 传自定义headers
- 日志分析 添加clientId字段，jsmcid 

#### fix
- 修复jsm.sessionType default下登录问题
- fix Job中的traceId不正常问题
- 修改CacheAnn 默认key BUG
- JsonUtil中showNull不生效问题
- 修复findForUpdate时数组转换错误问题

### 2.1.1

#### new

- 重构包名
- 新增DicAnn可实现字段自动字典映射
- json config support , jsm.json.namingStrategy
  ,jsm.json.showNull,jsm.json.booleanNullFalse,jsm.json.numberNullZero,jsm.json.stringNullEmpty,jsm.json.arrayNullEmpty
- jedis 去除 sharedJedis,ShardedJedisCmdCallback,JedisCmdCallback 支持
- jedis 升级到 4.2.1 , 支持多nodes集群，多个node用 逗号,分开
- change SessionUser uid to id
- session support cacheService config by jsm.session.type (default,L2,L1L2) ,remove jsm.jedis.session
- 新增CacheableObject增加用于判断 对象是否来于缓存，以提升某些情况下的性能。
- 新增 @HttpClientServiceScan @HttpClientService @HttpClientMapping 支持 三方API调用
- 添加DelaySet 和 DelayMap 并且默认 Session 使用 DelaySet 延时
- FieldAnn 新增 urlDecode 配置，默认false
- 添加jcraft 支持ssh,并提供JsmPool,PoolObject,PoolFactory支持 轻松定义连接池
- 新增genService.genPermissions() 动态生成Permissions.java
- FieldAnn 新增 escapeSensitiveWord 配置过滤敏感词, 默认false
- jsm 支持 RestTemplate，header传递
- 支持elasticsearch 查询
- ResAnn OauthType支持GatewayAuth类型，支持前置Gateway解析token
- 支持统计 单独配置 jsm.stat.redis.* 和 jsm.stat.elastic.* 
- elasticClient,HttpClient，HttpAsyncClient支持 username pass 认证

#### fix

- 修复jackson默认时区问题
- 修复mock List | String问题，优化mock返回逻辑

### 2.1.0

#### new

- integrate jsm-frame-core to jsm-spring-boot-starter and rename some package name
- add JsmAppender support for log. jsm.log.type=none console(consoleStrategy) elastic(elasticLogStrategy) redis(redisLogStrategy) kafka(
  kafkaLogStrategy) rabbitmq(rabbitmqLogStrategy) custom(自定义的customLogStrategy)
- add jsm.apiStat.type jsm.apiStat.idx
- add jsm.sysStat.type jsm.sysStat.idx jsm.sysStat.interval
- add jsm.alarm.type
- add jsm.optLog.type
- add jsm-kafka-spring-boot-starter
- add jsm-rabbitmq-spring-boot-starter
- add jsm.json.namingStrategy: lowerCamelCase # lowerCamelCase snakeCase upperCamelCase lowerCase kebabCase
- add RestStatusStrategy for custom BaseRest.code to httpStatus
- add DbGenerationStrategy for db field to model filed.
- 新增框架全局配置JsmConfigurer

### 2.0.0

#### new:

- change package to cn.ibaijia.jsm
- db support encPassword for blow fish
- change reqId to traceId
- jdk min version 1.8
- traceId req add hex ip string
- add ScheduleTaskService 可动态加载job
- genService 反射table时 找不到类型默认为byte[]
- ExcelUtil 支持读取 公式
- 新增熔断功能RestFuseAnn
- 新增限流功能RestLimitAnn
- traceId add userId
- 添加 SlowSql 预警
- 移出 app.properties and project.properties
- 添加 jsm.cors.open 跨域开关

#### fix:

- fix AppContext logger bug.
- fix BeanUtil.copy null问题
- 优化ElasticAppender 批量插入
- fix jedisPool不能识别 uri bug

### 1.3.25

#### new:

- ES api stat URL类型不指定默认为text
- genService 支持生成 service CURD方法和 api CURD API

#### fix:

- fix FileAnn El Validate message bug.
- fix fast json converter on effect bug.
- fix FileUtil zip bug

### 1.3.24

#### new:

- 推荐HttpClient 更灵活，支持更多方式
- support spring boot stater
- add SensitiveWordUtil sensitive-words file in classpath
- ehcahe 单机两个 自动 和 手动 两个docker待测试
- 增强jedisService提供jedis和shardedJedis 两种回调
- log42j增加ElasticAppender
- 新增DefaultApiStatService
- 移出ProjectContext
- RestAnn 增加 internal 和 ipCheckKey ,实现 内网和IP检查
- disruptor 最低 3.4.2
- job 设置reqId
- 新增JsmDisruptor 支持泛型
- SystemUtil SystemStat 和 Alarm 系统及系统预警信息统计
- support config alarm.api.time = 30000 alarm.max.count = 500
- 新增JedisCmdThrowable 回调redis错误
- 新增HttpAsyncClient
- 发送ElasticAppender采用HttpAsyncClient,默认缓存4096
- ValidateModel FieldAnn 新增validateMethod(), 默认调用 filedName+Validate方法验证
- JsmDisruptor 默认使用 tryPublishEvent方法，保存elasticAppender在大量写日志的时候，不耽误主线任务。
- ValidateModel 新增调用全局validate方法 和全局Component modelName+Validator
- JsmFilter 支持 exclude 参数 默认排除 /static/;/swagger/

#### fix:

- fix ClusterLockAnn exception bug.

### 1.3.23

#### new:

- 新增log4j2,日志ID jri支持。
- 自动生成Model时，String默认按数据库字长度生成@FieldAnn。
- 新增加自动成功前端Api的方法genService.genFrontApi。
- clusterSyncLock in RestAnn 支持上下文变量 request > attribute > session ，如 clusterSyncLock=“userCountLock_#{userId}”
- swagger api新增 参数说明文档
- 支持 unique-req-id 防重复提交 deleted
- 支持 api.stat.url
- 自动生成的daoModel 日期带 JSONField注解
- 支持快照（createSnapshot）更新
- 新增ClusterLockAnn 分布式锁注解 支持上下文变量 request > attribute > session ，如 clusterSyncLock=“userCountLock_#{userId}”
- 支持GenService mapper 和 model更新
- 新增findByIdForUpdate 方法
- 生成的基础方法(除pageList外)移入到mapper.java中，使mapper.xml中更整洁
- 分页查询支持自定义_COUNT语句
- 支持jsm_mock参数获取MOCK数据
- 新增ClusterJobLockAnn解决多实例模式下JOB高可用
- 无授权模式下，尝试解析at ht
- 自动开启POST PUT重复提交,可通过repeatCheck=false关闭
- CacheAnn支持多级缓存CacheType.EHCACHE_REDIS, 并且默认使用此策略
- 新增CacheService 支持二级缓存 L1 ehcache L2 Redis
- 新增数据库读写分离配置
- excelField 导入数字支持format
- 新增ehcache.RMICacheManagerPeerProviderFactory 解决ehcache在docker容器下RMI集群手动模式Host缺陷

#### fix:

- 由于nginx默认不传播带下划线‘_’的header，修改日志传播header为jsm-req-id，修改开发token为jsm-token。
- NumberUtil 使用BigDecimal 精度问题
- 分页查询 字段中有select from 不能使用的问题
- 接口开发模式下支持mock数据,如加上 jsm_mock=1
- fix mock value不支持@表达式

### 1.3.22

#### new:

- GenService，自动生成批量新增
- 支持log4j2。
- 分页参数按类型获取。

#### fix:

- .

### 1.3.21

#### new:

- GenService，支持tinyint smallint生成为 byte short
- 新增JsmSessionListener 监听器。

#### fix:

- 配置为使用HttpSession 的一些bug。

### 1.3.20

#### new:

- GenService，支持覆盖生成,并且自动生成Service

#### fix:

- job中使用 HttpClientUtil token报错

### 1.3.19

#### new:

- HttpClientUtil授权传播

#### fix:

- 日志id传播不了

### 1.3.18

#### new:

- api统计 api.stat

#### fix:

- StringUtil.fenToYuan 不精确issue