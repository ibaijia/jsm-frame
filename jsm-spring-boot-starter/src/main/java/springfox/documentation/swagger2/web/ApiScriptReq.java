package springfox.documentation.swagger2.web;

/**
 * @author longzl
 */
public class ApiScriptReq {

    /**
     * ts ,js
     */
    private String lang;
    /**
     * apiClass 如: UserApi
     */
    private String apiName;
    /**
     * api , model
     */
    private String fileType;
    /**
     * 如: ../../axios/index
     */
    private String ajaxLibPath;
    /**
     * 如:axios
     */
    private String ajaxLibName;
    /**
     * ts model的 目录用 api文件中引用
     * 如 ./
     */
    private String apiModelDir;
    /**
     * baseUri 环境变量  api文件中引用
     * 如: import.meta.env.VITE_APP_BASE_URL or "base-api"
     */
    private String baseUri;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getAjaxLibPath() {
        return ajaxLibPath;
    }

    public void setAjaxLibPath(String ajaxLibPath) {
        this.ajaxLibPath = ajaxLibPath;
    }

    public String getAjaxLibName() {
        return ajaxLibName;
    }

    public void setAjaxLibName(String ajaxLibName) {
        this.ajaxLibName = ajaxLibName;
    }

    public String getApiModelDir() {
        return apiModelDir;
    }

    public void setApiModelDir(String apiModelDir) {
        this.apiModelDir = apiModelDir;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public void setBaseUri(String baseUri) {
        this.baseUri = baseUri;
    }
}
