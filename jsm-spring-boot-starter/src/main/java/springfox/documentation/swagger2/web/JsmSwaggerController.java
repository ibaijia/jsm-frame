package springfox.documentation.swagger2.web;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.gen.GenService;
import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.SwaggerProperties;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import io.swagger.models.Swagger;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.service.Documentation;
import springfox.documentation.spring.web.DocumentationCache;
import springfox.documentation.spring.web.json.Json;
import springfox.documentation.spring.web.json.JsonSerializer;
import springfox.documentation.swagger2.mappers.ServiceModelToSwagger2Mapper;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author longzl
 */
@ApiIgnore
@Controller
public class JsmSwaggerController {
    private Logger logger = LogUtil.log(JsmSwaggerController.class);
    @Resource
    private SwaggerProperties swaggerProperties;
    @Resource
    private DocumentationCache documentationCache;
    @Resource
    private ServiceModelToSwagger2Mapper mapper;
    @Resource
    private JsonSerializer jsonSerializer;
    @Resource
    private GenService genService;

    @ApiIgnore
    @GetMapping(
            value = {"${jsm.swagger.docPath:/v1/swagger-api-docs}"},
            produces = {"application/json", "application/hal+json"}
    )
    @ResponseBody
    public ResponseEntity<Json> getDocumentation(@RequestParam(value = "group", required = false) String swaggerGroup, HttpServletRequest servletRequest) {
        if (!this.swaggerProperties.enable) {
            return new ResponseEntity(BaseConstants.SYSTEM_EMPTY_STRING, HttpStatus.OK);
        }
        String groupName = (String) Optional.fromNullable(swaggerGroup).or("default");
        Documentation documentation = this.documentationCache.documentationByGroup(groupName);
        if (documentation == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            Swagger swagger = this.mapper.mapDocumentation(documentation);
            if (Strings.isNullOrEmpty(swagger.getHost())) {
                UriComponents uriComponents = HostNameProvider.componentsFrom(servletRequest);
                swagger.basePath(Strings.isNullOrEmpty(uriComponents.getPath()) ? "/" : uriComponents.getPath());
                swagger.host(this.hostName(uriComponents));
            }
            return new ResponseEntity(jsonSerializer.toJson(swagger), HttpStatus.OK);
        }
    }

    @ApiIgnore
    @PostMapping(value = {"${jsm.swagger.detailPath:/v1/swagger-api-detail}"})
    public ResponseEntity<String> getApiDetail(HttpServletRequest servletRequest) {
        if (!this.swaggerProperties.enable) {
            return new ResponseEntity(BaseConstants.SYSTEM_EMPTY_STRING, HttpStatus.OK);
        }
        String path = servletRequest.getParameter("path");
        logger.info("path:{}", path);
        path = path.split("_")[0];
        String[] arr = path.split("/");
        String clazzName = StringUtil.kebabToCamel(arr[1]);
        clazzName = StringUtil.upperCaseFirst(clazzName);
        String methodName = arr[2];
        methodName = methodName.split("Using")[0];
        logger.info("clazzName:{} methodName:{}", clazzName, methodName);
        String html = genService.getControllerInfoHtml(clazzName, methodName);
        return new ResponseEntity(html, HttpStatus.OK);
    }

    @GetMapping(value = {"${jsm.swagger.detailPath:/v1/swagger-api-names}"})
    @ResponseBody
    public ResponseEntity<List<String>> getApiNames() {
        if (!this.swaggerProperties.enable) {
            return new ResponseEntity(BaseConstants.SYSTEM_EMPTY_STRING, HttpStatus.OK);
        }
        List<String> result = genService.getApiNames();
        return new ResponseEntity(JsonUtil.toJsonString(result), HttpStatus.OK);
    }

    @PostMapping(value = {"${jsm.swagger.detailPath:/v1/swagger-api-script}"})
    @ResponseBody
    public ResponseEntity<String> getApiScript(@RequestBody ApiScriptReq req) {
        if (!this.swaggerProperties.enable) {
            return new ResponseEntity(BaseConstants.SYSTEM_EMPTY_STRING, HttpStatus.OK);
        }
        String script = genService.genScript(req);
        return new ResponseEntity(script, HttpStatus.OK);
    }

    private String hostName(UriComponents uriComponents) {
        if (!StringUtil.isEmpty(swaggerProperties.getHost())) {
            String host = uriComponents.getHost();
            int port = uriComponents.getPort();
            return port > -1 ? String.format("%s:%d", host, port) : host;
        } else {
            return BaseConstants.SYSTEM_EMPTY_STRING;
        }
    }
}
