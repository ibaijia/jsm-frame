package cn.ibaijia.jsm.oauth.client;

import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.ThreadLocalUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;

/**
 * 中间 资源API server 或者 三方应用server
 * @author longzl
 */
public class OauthContext {

    public static void setAccessToken(String token) {
        ThreadLocalUtil.OAUTH_ACCESS_TOKEN_TL.set(token);
    }

    public static void setVerifyResult(String result) {
        ThreadLocalUtil.OAUTH_VERIFY_RESULT_TL.set(result);
    }

    public static String getVerifyResult() {
        return ThreadLocalUtil.OAUTH_VERIFY_RESULT_TL.get();
    }

    public static <T> T getVerifyResult(Class<T> clazz) {
        String verifyResult = getVerifyResult();
        if (!StringUtil.isEmpty(verifyResult)) {
            return JsonUtil.parseObject(verifyResult, clazz);
        }
        return null;
    }

    public static <T> T getVerifyResult(JavaType type) {
        String verifyResult = getVerifyResult();
        if (!StringUtil.isEmpty(verifyResult)) {
            return JsonUtil.parseObject(verifyResult, type);
        }
        return null;
    }

    public static <T> T getVerifyResult(TypeReference<T> typeReference) {
        String verifyResult = getVerifyResult();
        if (!StringUtil.isEmpty(verifyResult)) {
            return JsonUtil.parseObject(verifyResult, typeReference);
        }
        return null;
    }

}
