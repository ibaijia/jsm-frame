package cn.ibaijia.jsm.oauth.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
/**
 * @author longzl
 */
public class RefreshTokenReq implements ValidateModel {
    /**
     * defaultVal:NULL
     * comments:accessToken
     */
    @FieldAnn(required = true, maxLen = 100, comments = "accessToken")
    public String accessToken;
    /**
     * defaultVal:NULL
     * comments:refreshToken
     */
    @FieldAnn(required = true, maxLen = 100, comments = "refreshToken")
    public String refreshToken;

}
