package cn.ibaijia.jsm.oauth.client;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.oauth.model.RefreshTokenReq;
import cn.ibaijia.jsm.utils.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 适合OauthClient 如微信小程序OauthClient
 *
 * @author longzl
 */
public class OauthClient {

    private Logger logger = LogUtil.log(OauthClient.class);
    private Date expireTime = new Date();
    private String appKey;
    private String appSecret;
    private String getTokenUrl;
    private String refreshTokenUrl;
    private List<Header> headers = new ArrayList<>();

    /**
     * oauth.client.appKey
     */
    public OauthClient() {
        appKey = AppContext.get(AppContextKey.OAUTH_CLIENT_APP_KEY);
        appSecret = AppContext.get(AppContextKey.OAUTH_CLIENT_APP_SECRET);
        getTokenUrl = AppContext.get(AppContextKey.OAUTH_CLIENT_TOKEN_URL);
        refreshTokenUrl = AppContext.get(AppContextKey.OAUTH_CLIENT_REFRESH_TOKEN_URL);
        headers.add(new BasicHeader(BaseConstants.APP_KEY, appKey));
        headers.add(new BasicHeader(BaseConstants.APP_SECRETE, appSecret));
    }

    public String getAccessToken() {
        if (StringUtil.isEmpty(appKey)) {
            logger.error("缺少配置oauth.client.appKey.");
            return null;
        }
        if (StringUtil.isEmpty(appSecret)) {
            logger.error("缺少配置oauth.client.appSecret.");
            return null;
        }
        if (StringUtil.isEmpty(getTokenUrl)) {
            logger.error("缺少配置oauth.client.getToken.url.");
            return null;
        }
        String res = HttpClientUtil.post(headers, getTokenUrl, "");
        logger.info("get oauth res:{}", res);
        return res;
    }

    public <T> T getAccessToken(Class<T> clazz) {
        String res = getAccessToken();
        if (!StringUtil.isEmpty(res)) {
            return JsonUtil.parseObject(res, clazz);
        }
        return null;
    }

    public <T> T getAccessToken(JavaType type) {
        String res = getAccessToken();
        if (!StringUtil.isEmpty(res)) {
            return JsonUtil.parseObject(res, type);
        }
        return null;
    }

    public <T> T getAccessToken(TypeReference<T> typeReference) {
        String res = getAccessToken();
        if (!StringUtil.isEmpty(res)) {
            return JsonUtil.parseObject(res, typeReference);
        }
        return null;
    }

    public void setExpireIn(int expireIn) {
        expireTime = DateUtil.addSecond(DateUtil.currentDate(), expireIn);
    }

    public boolean isExpired() {
        return DateUtil.reduce(expireTime, DateUtil.currentDate(), DateUtil.MILLIS_PER_SECOND) < 300;
    }

    public String refreshToken(String accessToken, String refreshToken) {
        RefreshTokenReq refreshTokenReq = new RefreshTokenReq();
        refreshTokenReq.refreshToken = refreshToken;
        refreshTokenReq.accessToken = accessToken;
        if (StringUtil.isEmpty(refreshTokenUrl)) {
            logger.error("缺少配置oauth.client.refreshToken.url");
            return null;
        }
        String res = HttpClientUtil.put(headers, refreshTokenUrl, JsonUtil.toJsonString(refreshTokenReq));
        logger.info("refresh oauth res:{}", res);
        return res;
    }

    public <T> T refreshToken(String accessToken, String refreshToken, Class<T> clazz) {
        String res = refreshToken(accessToken, refreshToken);
        if (!StringUtil.isEmpty(res)) {
            return JsonUtil.parseObject(res, clazz);
        }
        return null;
    }

    public <T> T refreshToken(String accessToken, String refreshToken, JavaType type) {
        String res = refreshToken(accessToken, refreshToken);
        if (!StringUtil.isEmpty(res)) {
            return JsonUtil.parseObject(res, type);
        }
        return null;
    }

    public <T> T refreshToken(String accessToken, String refreshToken, TypeReference<T> typeReference) {
        String res = refreshToken(accessToken, refreshToken);
        if (!StringUtil.isEmpty(res)) {
            return JsonUtil.parseObject(res, typeReference);
        }
        return null;
    }

}
