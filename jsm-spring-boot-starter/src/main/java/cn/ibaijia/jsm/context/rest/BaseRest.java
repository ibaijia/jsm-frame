package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.interceptor.MyDateEditor;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author longzl
 */
@RestController
public abstract class BaseRest {
    protected Logger logger = LogUtil.log(getClass());

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        logger.debug("initBinder");
        binder.registerCustomEditor(Date.class, new MyDateEditor());
        String[] disallowedFields = new String[]{"class.*", "Class.*", "*.class.*", "*.Class.*"};
        binder.setDisallowedFields(disallowedFields);
    }

    protected <T> RestResp<T> success(T result) {
        return success(result,  BasePairConstants.OK.getCode(), BasePairConstants.OK.getMessage());
    }

    protected <T> RestResp<T> success(T result, String code) {
        return success(result, code, BasePairConstants.OK.getMessage());
    }

    protected <T> RestResp<T> success(T result, String code, String message) {
        RestResp<T> resp = new RestResp();
        resp.result = result;
        resp.code = code;
        resp.message = message;
        return resp;
    }

}
