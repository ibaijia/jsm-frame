package cn.ibaijia.jsm.context.rest.resp.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/6/2 18:36
 */
public class FreeVo implements ValidateModel, OutputVo {

    @FieldAnn(comments = "contentType")
    public String contentType = "text/html;charset=UTF-8";

    @FieldAnn(comments = "")
    public String content;

    public FreeVo() {
    }

    public FreeVo(String contentType, String content) {
        this.contentType = contentType;
        this.content = content;
    }
}

