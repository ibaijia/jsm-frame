package cn.ibaijia.jsm.context.service;

import java.util.List;

/**
 * @author longzl
 */
public interface JsmSecurityService {
	/**
	 * 返回当前用户是有这个角色ID
	 * @param roleId
	 * @return
	 */
	public boolean hasRole(String roleId);
	/**
	 * 返回当前用户是有这个角色ID之1
	 * @param roleIdList
	 * @return
	 */
	public boolean hasAnyRole(List<String> roleIdList);
	/**
	 * 返回当前用户是有这个权限permissionCode
	 * @param permissionCode
	 * @return
	 */
	public boolean hasPermission(String permissionCode);
	/**
	 * 返回当前用户是有这个权限permissionCodeList之1
	 * @param permissionCodeList
	 * @return
	 */
	public boolean hasAnyPermission(List<String> permissionCodeList);
}
