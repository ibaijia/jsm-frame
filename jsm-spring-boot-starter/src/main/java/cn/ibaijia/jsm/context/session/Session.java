package cn.ibaijia.jsm.context.session;

import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.SessionContext;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @author longzl
 */
public class Session {
    private static final Logger logger = LoggerFactory.getLogger(Session.class);
    private String token;
    private String sessionKey;
    private JedisService jedisService;
    private CacheService cacheService;
    private SessionUser sessionUser;
    private HttpSession session;
    private boolean invalid = false;

    /**
     * session中的 userInfo的key
     */
    public final static String SESSION_PREFIX = "jsm:session:";
    /**
     * session中的 userInfo的key
     */
    private final static String SESSION_USER_ATTR_KEY = "jsm:sessionUser";
    /**
     * 每个用户的userId 对应的 token
     */
    public final static String USER_TOKEN_PREFIX = "jsm:user:token:";

    public static Session create(String token) {
        Session session = new Session(token);
        session.set(SESSION_PREFIX, token);
        session.needLive();
        WebContext.getRequest().setAttribute(BaseConstants.SESSION_ATTR_KEY, session);
        return session;
    }

    public Session(String token) {
        super();
        this.token = token;
        this.sessionKey = SESSION_PREFIX + token;
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            this.jedisService = SpringContext.getBean(JedisService.class);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            this.cacheService = SpringContext.getBean(CacheService.class);
        } else {
            session = WebContext.getRequest().getSession();
        }
    }

    public void set(String key, String value) {
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            jedisService.hset(sessionKey, key, value);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            cacheService.hset(sessionKey, key, value);
        } else {
            session.setAttribute(key, value);
        }
    }

    /**
     * @param key
     * @return if jedis will string
     */
    public Object get(String key) {
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            return jedisService.hget(sessionKey, key);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            return cacheService.hget(sessionKey, key);
        } else {
            return session.getAttribute(key);
        }
    }

    public void del(String key) {
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            jedisService.hdel(sessionKey, key);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            cacheService.hdel(sessionKey, key);
        } else {
            session.removeAttribute(key);
        }
    }

    /**
     * jedis only
     */
    public void needLive() {
        logger.debug("needLive!");
        SessionContext.LIVE_SET.add(this);
    }

    public void live() {
        logger.debug("live check!");
        if (!this.invalid) {
            logger.debug("live");
            int liveTime = AppContext.getSessionExpireTime();
            if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
                jedisService.expire(sessionKey, liveTime);
                if (this.sessionUser != null) {
                    String userTokenKey = USER_TOKEN_PREFIX + this.sessionUser.getId();
                    jedisService.expire(userTokenKey, liveTime);
                }
            } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
                cacheService.expire(sessionKey, liveTime);
                if (this.sessionUser != null) {
                    String userTokenKey = USER_TOKEN_PREFIX + this.sessionUser.getId();
                    cacheService.expire(userTokenKey, liveTime);
                }
            }
        }
    }

    public String getToken() {
        return token;
    }

    public SessionUser getSessionUser() {
        if (this.sessionUser == null) {
            if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2) || AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
                String sessionUserStr = (String) get(SESSION_USER_ATTR_KEY);
                if (!StringUtil.isEmpty(sessionUserStr)) {
                    this.sessionUser = JsonUtil.parseObject(sessionUserStr, SessionUser.class);
                }
            } else {
                this.sessionUser = (SessionUser) session.getAttribute(SESSION_USER_ATTR_KEY);
            }
        }
        return sessionUser;
    }

    public void setSessionUser(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2) || AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            set(SESSION_USER_ATTR_KEY, JsonUtil.toJsonString(sessionUser));
            setUserToken();
            needLive();
        } else {
            session.setAttribute(SESSION_USER_ATTR_KEY, sessionUser);
            SessionContext.LOGIN_SESSION.put(sessionUser.getId(), session);
        }
    }

    private void setUserToken() {
        String userTokenKey = USER_TOKEN_PREFIX + sessionUser.getId();
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            jedisService.set(userTokenKey, token);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            cacheService.set(userTokenKey, token);
        }
        logger.debug("setSessionUser key:{} , token:{}", userTokenKey, token);
    }

    private void remove(String key) {
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            jedisService.del(key);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            cacheService.remove(key);
            jedisService.del(key);
        }
    }

    public boolean isExpire() {
        if (this.invalid) {
            return true;
        }
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2)) {
            return !jedisService.exists(sessionKey);
        } else if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            return !cacheService.exists(sessionKey);
        } else {
            if (this.session != null) {
                return this.session == null;
            }
        }
        return false;
    }

    public void flush() {
        if (!this.invalid) {
            setSessionUser(sessionUser);
        }
    }

    /**
     * 使当前session过期
     */
    public void invalidate() {
        logger.debug("session invalidate.");
        this.invalid = true;
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2) || AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            if (this.getSessionUser() != null) {
                String userTokenKey = USER_TOKEN_PREFIX + this.getSessionUser().getId();
                remove(userTokenKey);
                logger.debug("invalidate key:{}", userTokenKey);
            }
            remove(sessionKey);
            logger.debug("invalidate token:{}", token);
        } else {
            if (this.session != null) {
                this.session.invalidate();
            }
            if (this.sessionUser != null) {
                SessionContext.LOGIN_SESSION.remove(this.sessionUser.getId());
            }
        }
    }


    public static boolean expire(String uid) {
        return SessionContext.expire(uid);
    }

    public static boolean isLogon(String uid) {
        return SessionContext.isLogon(uid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Session session = (Session) o;
        return Objects.equals(token, session.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }
}
