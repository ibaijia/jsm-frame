package cn.ibaijia.jsm.context.rest.validate;

import cn.ibaijia.jsm.utils.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;

/**
 * 可验证码实体，需要jsm框架验证的实体需要实现这个接口
 *
 * @author longzl
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(JsonUtil.JsmStrategy.class)
public interface ValidateModel extends Serializable {

}
