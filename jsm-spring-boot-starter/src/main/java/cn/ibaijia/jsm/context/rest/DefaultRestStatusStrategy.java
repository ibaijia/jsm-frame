package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.context.rest.resp.RestResp;

/**
 * 业务结果对应响应 httpStatus 转换策略
 * @author longzl
 */
public class DefaultRestStatusStrategy implements RestStatusStrategy {

    @Override
    public int httpStatusCode(RestResp restResp) {
        return restResp.getHttpStatusCode();
    }

}
