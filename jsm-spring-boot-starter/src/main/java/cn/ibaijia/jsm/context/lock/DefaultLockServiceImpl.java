package cn.ibaijia.jsm.context.lock;

import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author longzl
 */
public class DefaultLockServiceImpl implements LockService {

    private static final int TIMEOUT = 30*1000;

    private static final int LOCK_SECONDS = 60;

    private static final int TIMER_DELAY = 55;

    private static final int LOCK_CONTINUE_SECONDS = 65;

    private Logger logger = LogUtil.log(this.getClass());
    private ScheduledExecutorService scheduler;

    private JedisService jedisService;

    private Set<String> lockKeys = new CopyOnWriteArraySet<>();

    public DefaultLockServiceImpl(JedisService jedisService) {
        this.jedisService = jedisService;
        this.init();
    }

    private void init(){
        scheduler = new ScheduledThreadPoolExecutor(1, runnable -> new Thread(runnable, "jsm-lock-service-scheduler"));
        scheduler.scheduleAtFixedRate(()->{
            Iterator<String> iterator = lockKeys.iterator();
            while (iterator.hasNext()) {
                try {
                    String key = iterator.next();
                    if(!StringUtil.isEmpty(key)){
                        jedisService.expire(key,LOCK_CONTINUE_SECONDS);
                    }
                } catch (Exception e) {
                    logger.error("lock continue error:{}",e.getMessage());
                }
            }
        },TIMER_DELAY*1000,LOCK_SECONDS*1000, TimeUnit.SECONDS);

    }
    @Override
    public boolean tryLock(String lockKey) {
        return this.lock(lockKey,0);
    }
    @Override
    public boolean lock(String lockKey) {
        return this.lock(lockKey,TIMEOUT);
    }

    @Override
    public boolean lock(String lockKey, int timeout) {
        return this.lock(lockKey,timeout,LOCK_SECONDS);
    }

    @Override
    public boolean lock(String lockKey, int timeout, int seconds) {
        if(jedisService == null){
            throw new RuntimeException("jedisService not found.");
        }
        int retryTimes = 0;
        if(timeout > BaseConstants.LOCK_SPIN_MILLIS){
            retryTimes = timeout/BaseConstants.LOCK_SPIN_MILLIS;
        }
        boolean res = jedisService.lock(lockKey,retryTimes,seconds);
        if(res){
            lockKeys.add(lockKey);
        }
        return res;
    }

    @Override
    public boolean unlock(String key) {
        jedisService.unlock(key);
        return lockKeys.remove(key);
    }

}
