package cn.ibaijia.jsm.context.listener;

import javax.servlet.ServletContextEvent;

/**
 * @author longzl
 */
public interface JsmAppListener {

	/**
	 * 初始化时调用
	 * @param sce
	 */
	void init(ServletContextEvent sce);

	/**
	 * 关闭时调用
	 * @param sce
	 */
	void close(ServletContextEvent sce);
	
}
