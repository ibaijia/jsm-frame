package cn.ibaijia.jsm.context.rest.resp.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/6/2 18:36
 */
public class FollowVo implements ValidateModel, OutputVo {

    @FieldAnn(comments = "")
    public String uri;

    public FollowVo() {
    }

    public FollowVo(String uri) {
        this.uri = uri;
    }
}

