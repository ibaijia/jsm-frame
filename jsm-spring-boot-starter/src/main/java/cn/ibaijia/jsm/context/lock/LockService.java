package cn.ibaijia.jsm.context.lock;

/**
 * @author longzl
 */
public interface LockService {

    /**
     * 尝试锁，拿不到立即返回
     * @param lockKey
     * @return
     */
    boolean tryLock(String lockKey);

    /**
     * 尝试锁，默认尝试30s
     * @param lockKey
     * @return
     */
    boolean lock(String lockKey);

    /**
     * 尝试锁，默认尝试30s
     * @param lockKey
     * @param timeout
     * @return
     */
    boolean lock(String lockKey, int timeout);

    /**
     * 获取锁 指定锁定时间
     * @param lockKey
     * @param timeout
     * @param seconds
     * @return
     */
    boolean lock(String lockKey, int timeout, int seconds);

    /**
     * 解锁
     * @param key
     * @return
     */
    boolean unlock(String key);

}
