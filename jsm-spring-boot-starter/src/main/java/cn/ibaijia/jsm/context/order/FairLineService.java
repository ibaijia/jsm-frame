package cn.ibaijia.jsm.context.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.collection.DelaySet;
import cn.ibaijia.jsm.collection.DelaySetProcessor;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.TemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author longzl
 */
@Component
public class FairLineService extends BaseOrderLine {

    @Autowired(required = false)
    private JedisService jedisService;

    private DelaySet<String> delaySet;

    @Override
    public Pair<String> order(String key, OrderAnn orderAnn) {

        if (!BasePairConstants.ACTIVITY_STARTED.equals(getStatus())) {
            return getStatus();
        }
        String member = TemplateUtil.formatWithContextVar(orderAnn.userKey());
        Long idx = jedisService.exec(jedis -> {
            delaySet.add(key);
            // Redis ZRank 返回有序集中指定成员的排名。其中有序集成员按分数值递增(从小到大)顺序排列。
            Long idx1 = jedis.zrank(key, member);
            if (idx1 == null) {
                jedis.zadd(key, 100000, member);//插入到最后
            } else {
                jedis.zincrby(key, -1, member);
            }
            idx1 = jedis.zrank(key, member);
            if(idx1 == 0){
                jedis.zrem(key, member);//删除成员，下次加到最后
            }
            return idx1;
        });
        if (idx == 0) {
            return BasePairConstants.ACTIVITY_TURN;
        } else {
            return BasePairConstants.ACTIVITY_QUEUEING;
        }
    }

    @Override
    public void setStatus(Pair<String> status) {
        super.setStatus(status);
        if(BasePairConstants.ACTIVITY_STARTED.equals(status)){
            delaySet = new DelaySet<>("fireLineServiceDelaySet", 1 * DateUtil.MILLIS_PER_MINUTE, new DelaySetProcessor<String>() {
                @Override
                public void process(Set<String> set) {
                    for(String key:set){
                        jedisService.expire(key,3600);
                    }
                }
                @Override
                public void throwable(Throwable throwable) {
                    logger.error("fireLineServiceDelaySet process error.",throwable);
                }
            });
        }
        if(BasePairConstants.ACTIVITY_OVER.equals(status)){
            delaySet.stop();
            delaySet = null;
        }
    }
}
