package cn.ibaijia.jsm.context;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.context.session.SessionUser;
import cn.ibaijia.jsm.utils.*;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.logging.log4j.status.StatusLogger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * web工具类
 *
 * @author longzl
 */
public class WebContext {
    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();
    public static final String JSM_TRACE_ID = "jsm-trace-id";
    public static final String JSM_CLIENT_ID = "jsm-client-id";
    private static final int JSM_CLIENT_ID_MAX_AGE = 10 * 365 * 24 * 60 * 60;
    public static final String JSM_AT = "at";
    public static final String JSM_HT = "ht";
    public static final String JSM_HAT = "hat";

    public static final String JSM_DEBUG_TOKEN = "jsm-debug-token";
    public static String JSM_AUTHORIZATION = "Authorization";
    public static final String JSM_RESP_CODE = "jsm-resp-code";
    public static final String JSM_MOCK = "jsm-mock";
    private static final String UNKNOWN = "unknown";
    public static final int MAX_ID = Integer.MAX_VALUE - 100;
    private static AtomicInteger traceIdAi = new AtomicInteger(0);

    private static String tracePrefix;

    private static String getTracePrefix() {
        if (tracePrefix == null && AppContext.getClusterId() != null) {
            tracePrefix = "trc-" + AppContext.getClusterId() + "-";
        }
        return tracePrefix;
    }

    public static void setRequest(HttpServletRequest request) {
        ThreadLocalUtil.REQUEST_TL.set(request);
    }

    public static HttpServletRequest getRequest() {
        if (ThreadLocalUtil.REQUEST_TL.get() != null) {
            return ThreadLocalUtil.REQUEST_TL.get();
        } else {
            return getJspRequest();
        }
    }

    public static boolean isRequestMethod(String method) {
        if (getRequest() == null) {
            return false;
        }
        return getRequest().getMethod().equals(method);
    }

    public static void clearTraceId() {
        ThreadLocalUtil.TRACE_ID_TL.remove();
    }

    public static void setTraceId(String traceId) {
        ThreadLocalUtil.TRACE_ID_TL.set(traceId);
    }

    public static String getTraceId() {
        //如果是job ThreadTL中直接拿到
        String traceId = ThreadLocalUtil.TRACE_ID_TL.get();
        if (traceId == null) {
            HttpServletRequest request = getRequest();
            if (request != null) {
                traceId = request.getHeader(WebContext.JSM_TRACE_ID);
                if (traceId == null) {
                    traceId = request.getParameter(WebContext.JSM_TRACE_ID);
                }
                if (traceId == null) {
                    traceId = (String) request.getAttribute(WebContext.JSM_TRACE_ID);
                }
                if (traceId == null) {
                    StringBuilder traceSb = new StringBuilder();
                    String hexIp = IpUtil.ipToHex(getRemoteIp());
                    int intCount = traceIdAi.getAndIncrement();
                    if (intCount > MAX_ID) {
                        traceIdAi.set(0);
                    }
                    traceSb.append(getTracePrefix()).append(hexIp).append("-").append(intCount);
                    traceId = traceSb.toString();
                    request.setAttribute(WebContext.JSM_TRACE_ID, traceId);
                }
            } else {//内部线程
                traceId = getTracePrefix() + Thread.currentThread().getName();
            }
        }
        return traceId;
    }

    public static String getRequestAt() {
        HttpServletRequest request = getRequest();
        if (request != null) {
            return getHeaderOrParamOrCookie(WebContext.JSM_AT);
        }
        return null;
    }

    public static String getRequestHt() {
        HttpServletRequest request = getRequest();
        if (request != null) {
            return getHeaderOrParamOrCookie(WebContext.JSM_HT);
        }
        return null;
    }

    public static String getRequestHat() {
        HttpServletRequest request = getRequest();
        if (request != null) {
            return getHeaderOrParamOrCookie(WebContext.JSM_HAT);
        }
        return null;
    }

    public static String getRequestDebugToken() {
        HttpServletRequest request = getRequest();
        if (request != null) {
            return getHeaderOrParam(WebContext.JSM_DEBUG_TOKEN);
        }
        return null;
    }

    public static String getRequestAuthorization() {
        HttpServletRequest request = getRequest();
        if (request != null) {
            return getHeaderOrParam(WebContext.JSM_AUTHORIZATION);
        }
        return null;
    }

    public static HttpHeaders checkAndSetHeaders(HttpHeaders headers) {
        String requestId = WebContext.getTraceId();
        if (!StringUtil.isEmpty(requestId)) {
            headers.add(WebContext.JSM_TRACE_ID, requestId);
        }

        String clientId = WebContext.getClientId();
        if (!StringUtil.isEmpty(clientId)) {
            headers.add(WebContext.JSM_CLIENT_ID, clientId);
        }

        String at = WebContext.getRequestAt();
        if (!StringUtil.isEmpty(at)) {
            headers.add(WebContext.JSM_AT, at);
        }

        String ht = WebContext.getRequestHt();
        if (!StringUtil.isEmpty(ht)) {
            headers.add(WebContext.JSM_HT, ht);
        }

        String hat = WebContext.getRequestHat();
        if (!StringUtil.isEmpty(hat)) {
            headers.add(WebContext.JSM_HAT, hat);
        }

        String debugToken = WebContext.getRequestDebugToken();
        if (!StringUtil.isEmpty(debugToken)) {
            headers.add(WebContext.JSM_DEBUG_TOKEN, debugToken);
        }

        String authorization = WebContext.getRequestAuthorization();
        if (!StringUtil.isEmpty(authorization)) {
            headers.add(WebContext.JSM_AUTHORIZATION, authorization);
        }
        return headers;
    }

    public static HttpRequestBase checkAndSetHeaders(HttpRequestBase httpEntity) {
        String requestId = WebContext.getTraceId();
        if (!StringUtil.isEmpty(requestId)) {
            httpEntity.setHeader(WebContext.JSM_TRACE_ID, requestId);
        }

        String clientId = WebContext.getClientId();
        if (!StringUtil.isEmpty(clientId)) {
            httpEntity.setHeader(WebContext.JSM_CLIENT_ID, clientId);
        }

        String at = WebContext.getRequestAt();
        if (!StringUtil.isEmpty(at)) {
            httpEntity.setHeader(WebContext.JSM_AT, at);
        }

        String ht = WebContext.getRequestHt();
        if (!StringUtil.isEmpty(ht)) {
            httpEntity.setHeader(WebContext.JSM_HT, ht);
        }

        String hat = WebContext.getRequestHat();
        if (!StringUtil.isEmpty(hat)) {
            httpEntity.setHeader(WebContext.JSM_HAT, hat);
        }

        String token = WebContext.getRequestDebugToken();
        if (!StringUtil.isEmpty(token)) {
            httpEntity.setHeader(WebContext.JSM_DEBUG_TOKEN, token);
        }

        String authorization = WebContext.getRequestAuthorization();
        if (!StringUtil.isEmpty(authorization)) {
            httpEntity.setHeader(WebContext.JSM_AUTHORIZATION, authorization);
        }
        return httpEntity;
    }

    public static String getHeaderOrParamOrCookie(String key) {
        String value = getHeader(key);
        if (value == null) {
            value = getParam(key);
        }
        if (value == null) {
            value = getCookie(key);
        }
        return value;
    }

    public static String getHeaderOrCookie(String key) {
        String value = getHeader(key);
        if (value == null) {
            value = getCookie(key);
        }
        return value;
    }

    public static String getHeader(String key) {
        HttpServletRequest request = getRequest();
        if (request == null) {
            return null;
        }
        return request.getHeader(key);
    }

    public static String getParam(String key) {
        HttpServletRequest request = getRequest();
        if (request == null) {
            return null;
        }
        return request.getParameter(key);
    }

    public static String getCookie(String key) {
        HttpServletRequest request = getRequest();
        if (request == null) {
            return null;
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(key)) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void setCookie(String name, String value) {
        setCookie(name, value, null);
    }

    public static void setCookie(String name, String value, Integer maxAge) {
        HttpServletResponse response = getResponse();
        if (response == null) {
            return;
        }
        Cookie cookie = new Cookie(name, value);
        cookie.setPath(BaseConstants.SYSTEM_SYMBOL_PATH);
        if (maxAge != null) {
            cookie.setMaxAge(maxAge);
        }
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }

    public static String getHeaderOrParam(String key) {
        String value = getHeader(key);
        if (value == null) {
            value = getParam(key);
        }
        return value;
    }

    public static String getClientId() {
        if (getRequest() == null) {
            return null;
        }
        String clientId = ThreadLocalUtil.CLIENT_ID_TL.get();
        if (clientId != null) {
            return clientId;
        }
        clientId = getHeaderOrCookie(JSM_CLIENT_ID);
        if (StringUtil.isEmpty(clientId)) {
            clientId = StringUtil.genRandomString(6) + DateUtil.currentDateStr(DateUtil.COMPACT_PATTERN);
            WebContext.setCookie(JSM_CLIENT_ID, clientId, JSM_CLIENT_ID_MAX_AGE);
        }
        ThreadLocalUtil.CLIENT_ID_TL.set(clientId);
        return clientId;
    }

    private static HttpServletRequest getJspRequest() {
        ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        return servletRequestAttributes == null ? null : servletRequestAttributes.getRequest();
    }

    public static String currentSessionId() {
        Session session = currentSession();
        return session == null ? null : session.getToken();
    }

    public static Session currentSession() {
        HttpServletRequest request = getRequest();
        return (Session) (request == null ? null : request.getAttribute(BaseConstants.SESSION_ATTR_KEY));
    }

    public static void setResponse(HttpServletResponse response) {
        ThreadLocalUtil.RESPONSE_TL.set(response);
    }

    public static HttpServletResponse getResponse() {
        if (ThreadLocalUtil.RESPONSE_TL.get() != null) {
            return ThreadLocalUtil.RESPONSE_TL.get();
        } else {
            return getJspResponse();
        }
    }

    public static HttpServletResponse getJspResponse() {
        ServletWebRequest servletWebRequest = ((ServletWebRequest) RequestContextHolder.getRequestAttributes());
        return servletWebRequest == null ? null : servletWebRequest.getResponse();
    }

    public static void redirect(String action) throws IOException {
        getResponse().sendRedirect(getContextPath() + action);
    }

    public static boolean isRest() {
        String uri = getRequest().getRequestURI().toLowerCase();
        return !uri.contains(".");
    }

    private static void sendAjaxError(String msg) {
        RestResp<String> restResp = new RestResp<String>();
        restResp.setPair(BasePairConstants.ERROR);
        restResp.result = msg;
        try {
            getResponse().addHeader("Content-Type", "text/json;charset=UTF-8");
            getResponse().addHeader("Content-Type", "application/json;charset=UTF-8");
            getResponse().getWriter().write(JsonUtil.toJsonString(restResp));
            getResponse().getWriter().flush();
            getResponse().getWriter().close();
        } catch (Exception e) {
            logger.error("sendAjaxError error!", e);
        }
    }

    private static String contextPath;

    private static String realPath;

    public static String getRealPath() {
        return realPath;
    }

    public static void setRealPath(String realPath) {
        WebContext.realPath = realPath;
    }

    public static String getContextPath() {
        return contextPath;
    }

    public static void setContextPath(String contextPath) {
        WebContext.contextPath = contextPath;
    }


    /**
     * @param ext 返回一个随机产生指定扩展名的文件名
     */
    public static String getRandomFileName(String type, String ext) {
        StringBuilder sb = new StringBuilder();
        sb.append(type).append("_").append(DateUtil.format(new Date(), "yyyyMMddHHmmss"));
        sb.append("_").append(new Random().nextInt(1000)).append(".").append(ext);
        return sb.toString();
    }

    /**
     * 保存excel对象并返回保存后的文件名
     */
    public static String saveExcel(HSSFWorkbook workbook) throws IOException {
        String fileName = getRandomFileName("temp", "xls");
        OutputStream os = new FileOutputStream(fileName);
        workbook.write(os);
        os.close();
        return fileName.substring(fileName.lastIndexOf("\\") + 1);
    }

    /**
     * 取得当前用户
     */
    public static SessionUser currentUser() {
        Session session = currentSession();
        if (session == null) {
            return null;
        } else {
            return session.getSessionUser();
        }
    }

    public static Long currentUserIdLong() {
        return StringUtil.toLong(currentUserId(), null);
    }

    public static String currentUserId() {
        SessionUser currentUser = currentUser();
        if (currentUser == null) {
            return null;
        } else {
            return currentUser.getId();
        }
    }

    /**
     * 取得当前用户权限
     */
    public static List<String> currentPermissions() {
        SessionUser sessionUser = currentUser();
        if (sessionUser == null) {
            return null;
        }
        return sessionUser.getPermissions();
    }

    /**
     * 取得当前用户角色
     */
    public static List<String> currentRoles() {
        SessionUser sessionUser = currentUser();
        if (sessionUser == null) {
            return null;
        }
        return sessionUser.getRoles();
    }

    /**
     * 有些问题
     */
    public static boolean isAjax() {
        String type = getRequest().getHeader("X-Requested-With");
        return "XMLHttpRequest".equals(type);
    }

    public static String getFirstForwardIp() {
        String[] ips = getForwardedIps();
        if (ips == null) {
            return null;
        }
        return ips[0];
    }

    public static String getLastForwardIp() {
        String[] ips = getForwardedIps();
        if (ips == null) {
            return null;
        }
        return ips[ips.length - 1];
    }

    public static String[] getForwardedIps() {
        HttpServletRequest request = getRequest();
        String ip = getRemoteAddr(request);
        if (ip != null && ip.contains(BaseConstants.SYSTEM_SYMBOL_COMMA)) {
            ip = ip.replaceAll(" ", "");
        }
        return ip == null ? null : ip.split(BaseConstants.SYSTEM_SYMBOL_COMMA);
    }

    private static String getRemoteAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    public static String getRemoteIp() {
        HttpServletRequest request = getRequest();
        if (request == null) {
            return null;
        }
        return getRemoteIp(request);
    }

    public static String getRemoteIp(HttpServletRequest request) {
        String ip = getRemoteAddr(request);
        if (ip != null && ip.contains(BaseConstants.SYSTEM_SYMBOL_COMMA)) {
            ip = ip.split(BaseConstants.SYSTEM_SYMBOL_COMMA)[0];
        }
        return ip;
    }

    public static void setJspPath(String path) {
        ThreadLocalUtil.JSP_PATH_TL.set(path);
    }

    public static String getJspPath() {
        String path = ThreadLocalUtil.JSP_PATH_TL.get();
        ThreadLocalUtil.JSP_PATH_TL.remove();
        return path;
    }

    public static String getBackUrl() {
        if (HttpMethod.GET.name().equals(WebContext.getRequest().getMethod())) {
            return getActionUri();
        } else {
            String url = WebContext.getRequest().getHeader("REFERER");
            return url.substring(url.indexOf("=") + 1);
        }
    }

    public static byte[] getRequestBodyBytes(HttpServletRequest request) {
        try {
            int contentLength = request.getContentLength();
            if (contentLength < 0) {
                return null;
            }
            byte[] buffer = new byte[contentLength];
            for (int i = 0; i < contentLength; ) {
                int readLen = request.getInputStream().read(buffer, i, contentLength - i);
                if (readLen == -1) {
                    break;
                }
                i += readLen;
            }
            return buffer;
        } catch (IOException e) {
            logger.error("getRequestBodyBytes error!", e);
            return null;
        }
    }

    public static String getRequestBody(HttpServletRequest request) {
        try {
            byte[] buffer = getRequestBodyBytes(request);
            String charEncoding = request.getCharacterEncoding();
            if (charEncoding == null) {
                charEncoding = "UTF-8";
            }
            if (buffer != null) {
                return new String(buffer, charEncoding);
            }
        } catch (Exception e) {
            logger.error("getRequestBody error!", e);
        }
        return null;
    }

    public static String getRequestBody() {
        return getRequestBody(WebContext.getRequest());
    }

    /**
     * 取得相对于当前系统actionURI
     */
    public static String getActionUri() {
        return WebContext.getRequest().getRequestURI().substring(WebContext.getContextPath().length());
    }
}
