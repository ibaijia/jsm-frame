package cn.ibaijia.jsm.context;

import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.collection.DelaySet;
import cn.ibaijia.jsm.collection.DelaySetProcessor;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author: LongZL
 * @Date: 2022/8/10 15:51
 */
public class SessionContext {

    private static final Logger logger = LogUtil.log(SessionContext.class);

    public static final Map<String, HttpSession> LOGIN_SESSION = new HashMap<>();

    public static final DelaySet<Session> LIVE_SET = new DelaySet<>("session-live-set", 60 * 1000, new DelaySetProcessor<Session>() {
        @Override
        public void process(Set<Session> set) {
            for (Session session : set) {
                session.live();
            }
        }

        @Override
        public void throwable(Throwable throwable) {
            logger.error("session-live-set error:", throwable);
        }
    });

    public static boolean expire(String uid) {
        String userTokenKey = Session.USER_TOKEN_PREFIX + uid;
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2) || AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            JedisService jedisService = SpringContext.getBean(JedisService.class);
            String token = jedisService.get(userTokenKey);
            Session session = new Session(token);
            session.invalidate();
            logger.debug("expire userTokenKey:{} , token:{}", userTokenKey, token);
        } else {
            HttpSession session = LOGIN_SESSION.get(uid);
            if (session != null) {
                session.invalidate();
            }
            LOGIN_SESSION.remove(uid);
        }
        return true;
    }

    public static boolean isLogon(String uid) {
        String userTokenKey = Session.USER_TOKEN_PREFIX + uid;
        boolean res;
        if (AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L2) || AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_L1L2)) {
            JedisService jedisService = SpringContext.getBean(JedisService.class);
            res = jedisService.exists(userTokenKey);
        } else {
            res = LOGIN_SESSION.get(uid) != null;
        }
        logger.debug("uid:{}, isLogon:{}", uid, res);
        return res;
    }
}
