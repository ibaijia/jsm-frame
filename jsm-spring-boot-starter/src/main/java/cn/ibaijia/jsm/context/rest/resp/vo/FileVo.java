package cn.ibaijia.jsm.context.rest.resp.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import org.apache.http.Header;

import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/6/2 18:36
 */
public class FileVo implements ValidateModel, OutputVo {

    @FieldAnn(comments = "自定义headers")
    public List<Header> headers;

    @FieldAnn(comments = "文件名")
    public String fileName = "unknown_file_name.jpg";

    @JSONField(serialize = false)
    @JsonIgnore
    @ApiParam(hidden = true)
    @FieldAnn(comments = "输出流对象,String,InputStream,File,Object.")
    public Object content;

    public FileVo() {
    }

    public FileVo(Object content) {
        this.content = content;
    }

    public FileVo(String fileName, Object content) {
        this.fileName = fileName;
        this.content = content;
    }

    public FileVo(List<Header> headers, String fileName, Object content) {
        this.headers = headers;
        this.fileName = fileName;
        this.content = content;
    }
}

