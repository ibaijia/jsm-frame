package cn.ibaijia.jsm.context.listener;

import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.context.WebContext;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author longzl
 */
public class JsmHttpSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        JsmConfigurer.getJsmSessionListener().sessionCreated(httpSessionEvent);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        if (WebContext.currentSession() != null) {
            WebContext.currentSession().invalidate();
        }
        JsmConfigurer.getJsmSessionListener().sessionDestroyed(httpSessionEvent);
    }

}
