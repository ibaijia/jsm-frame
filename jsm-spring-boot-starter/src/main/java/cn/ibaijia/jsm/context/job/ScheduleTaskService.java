package cn.ibaijia.jsm.context.job;

import org.springframework.beans.factory.DisposableBean;

/**
 * @author longzl
 */
public interface ScheduleTaskService extends DisposableBean {
    /**
     * 这种动态job不支持 @ClusterJobLockAnn 因为不被Spring管理，不支持AOP
     */
    void loadTasks();
}
