package cn.ibaijia.jsm.context.listener;

import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author longzl / @createOn 2010-8-28
 * 启动服务时 初始工作 这里先于configure 初始 销毁 请实现 InitializingBean 和 DisposableBean
 */
public class JsmServletContextListener implements ServletContextListener {
    private static Logger logger = LogUtil.log(JsmServletContextListener.class);

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        JsmConfigurer.getJsmAppListenerService().close(sce);
        logger.info("app close complete!");
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebContext.setContextPath(sce.getServletContext().getContextPath());
        WebContext.setRealPath(sce.getServletContext().getRealPath("/"));
        System.setProperty("file.encoding", "utf-8");
        JsmConfigurer.getJsmAppListenerService().init(sce);
        logger.info("jsm-frame init complete!");
    }

}
