package cn.ibaijia.jsm.context.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.dao.BaseModel;

/**
 * @author zhili
 * createOn 2016年11月30日
 */
public class OptLog extends BaseModel {
    private static final long serialVersionUID = -969033916740549421L;
    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId = AppContext.get(AppContextKey.CLUSTER_ID, "");
    @FieldAnn(required = false, comments = "应用名")
    public String appName = AppContext.get(AppContextKey.APP_NAME, "");
    @FieldAnn(required = false, comments = "环境")
    public String env = AppContext.get(AppContextKey.ENV, "");
    @FieldAnn(required = false, comments = "GIT_COMMIT_ID")
    public String gitHash = AppContext.get(AppContextKey.GIT_HASH, "");
    @FieldAnn(required = false, maxLen = 50, comments = "客户端ID")
    public String clientId;
    @FieldAnn(required = false, comments = "操作时间")
    public Long time;
    @FieldAnn(comments = "操作人ip")
    public String ip;
    @FieldAnn(comments = "操作请求ID")
    public String traceId;
    @FieldAnn(comments = "操作人MAC地址")
    public String mac;
    @FieldAnn(comments = "操作人用户ID")
    public String uid;
    @FieldAnn(comments = "模块名称")
    public String funcName;
    @FieldAnn(comments = "操作内容")
    public String optDesc;

    public OptLog() {

    }

    public OptLog(String ip, String mac, String uid, String funcName, String optDesc) {
        super();
        this.ip = ip;
        this.mac = mac;
        this.uid = uid;
        this.funcName = funcName;
        this.optDesc = optDesc;
        this.clientId = WebContext.getClientId();
    }
}
