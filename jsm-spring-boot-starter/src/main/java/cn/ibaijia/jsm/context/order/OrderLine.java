package cn.ibaijia.jsm.context.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author longzl
 */
public interface OrderLine {

    /**
     * 获取下单状态
     * @param key
     * @param orderAnn
     * @return BasePairConsts.ACTIVITY_TURN 表示可以下单 否则表示不可以下单
     */
    Pair<String> order(String key, OrderAnn orderAnn);


}
