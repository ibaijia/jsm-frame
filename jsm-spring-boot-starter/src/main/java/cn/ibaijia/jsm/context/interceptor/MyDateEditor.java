package cn.ibaijia.jsm.context.interceptor;

import cn.ibaijia.jsm.utils.DateUtil;

import java.beans.PropertyEditorSupport;
import java.util.Date;

/**
 * @author longzl
 */
public class MyDateEditor extends PropertyEditorSupport {
	 @Override
     public void setAsText(String text) {
         setValue(DateUtil.parse(text));
     }

     @Override
     public String getAsText() {
         Date date = (Date) getValue();
         return DateUtil.format(date, DateUtil.DATETIME_PATTERN);
     }
}
