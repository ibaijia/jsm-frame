package cn.ibaijia.jsm.context.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author longzl
 */
public abstract class BaseOrderLine extends BaseService implements OrderLine {

    protected Pair<String> status = BasePairConstants.ACTIVITY_NOT_STARTED;

    /**
     *  下单实现方法
     * @param key
     * @param orderAnn
     * @return
     */
    @Override
    public abstract Pair<String> order(String key,OrderAnn orderAnn);

    public Pair<String> getStatus() {
        return status;
    }

    public void setStatus(Pair<String> status) {
        this.status = status;
    }
}
