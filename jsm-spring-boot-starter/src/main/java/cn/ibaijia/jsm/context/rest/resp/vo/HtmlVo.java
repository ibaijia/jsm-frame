package cn.ibaijia.jsm.context.rest.resp.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/6/2 18:36
 */
public class HtmlVo implements ValidateModel, OutputVo {

    @FieldAnn(comments = "")
    public String content;

    public HtmlVo() {
    }

    public HtmlVo(String content) {
        this.content = content;
    }
}

