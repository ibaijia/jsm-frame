package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.JsmFrameUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: LongZL
 * @Date: 2022/6/17 17:26
 */
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    private Logger logger = LogUtil.log(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public RestResp defaultExceptionHandler(Exception e) throws Exception {
        logger.error("defaultExceptionHandler error.");
        return JsmFrameUtil.dealException(e);
    }

}
