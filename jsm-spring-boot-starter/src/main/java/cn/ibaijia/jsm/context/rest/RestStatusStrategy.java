package cn.ibaijia.jsm.context.rest;

import cn.ibaijia.jsm.context.rest.resp.RestResp;

/**
 * @author longzl
 */
public interface RestStatusStrategy {

    /**
     * 返回转换的状态
     * @param restResp
     * @return
     */
    int httpStatusCode(RestResp restResp);

}
