package cn.ibaijia.jsm.context.job;

/**
 * @author longzl
 */
public interface JsmJob {
    /**
     * 执行Job的初始操作，每次启动执行一次
     */
    void onLoad();

    /**
     * 动态获取可以来自数据库，也可以来自配置中心 每个周期都会调用
     *
     * @return
     */
    String getCron();

    /**
     * 传入false 不执行 doJob ，实现Job动态开关 每个周期都会调用
     *
     * @return
     */
    boolean isOpen();

    /**
     * 执行Job的业务内容
     */
    void doJob();

}
