package cn.ibaijia.jsm.context.interceptor;

import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.StringUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * 使RestTemplate可以传header
 *
 * @Author: LongZL
 * @Date: 2022/9/2 19:03
 */
public class JsmHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = request.getHeaders();
        WebContext.checkAndSetHeaders(headers);
        return execution.execute(request, body);
    }
}
