package cn.ibaijia.jsm.context.order;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author longzl
 */
@Component
public class UnfairLineService extends BaseOrderLine {

    @Autowired(required = false)
    private JedisService jedisService;

    @Override
    public Pair<String> order(String key, OrderAnn orderAnn) {

        if (!BasePairConstants.ACTIVITY_STARTED.equals(getStatus())) {
            return getStatus();
        }
        String requestId = "1";
        try {
            boolean res = jedisService.lock(key, requestId, 0, 30);
            if (res) {
                return BasePairConstants.ACTIVITY_TURN;
            } else {
                return BasePairConstants.ACTIVITY_QUEUEING;
            }
        } catch (Exception e){
            logger.error("UnfairLineService lock error.",e);
            return BasePairConstants.ACTIVITY_QUEUEING;
        } finally {
            jedisService.unlock(key);
        }
    }
}
