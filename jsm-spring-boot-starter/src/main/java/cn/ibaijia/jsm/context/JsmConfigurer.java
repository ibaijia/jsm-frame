package cn.ibaijia.jsm.context;

import cn.ibaijia.jsm.context.listener.DefaultJsmAppListener;
import cn.ibaijia.jsm.context.listener.DefaultJsmSessionListener;
import cn.ibaijia.jsm.context.listener.JsmAppListener;
import cn.ibaijia.jsm.context.listener.JsmSessionListener;
import cn.ibaijia.jsm.context.rest.DefaultRestStatusStrategy;
import cn.ibaijia.jsm.context.rest.RestStatusStrategy;
import cn.ibaijia.jsm.context.service.JsmSecurityService;
import cn.ibaijia.jsm.context.service.DefaultJsmSecurityServiceImpl;
import cn.ibaijia.jsm.gen.DbGenerationStrategy;
import cn.ibaijia.jsm.gen.DefaultMySqlGenerationStrategy;
import cn.ibaijia.jsm.http.HttpMapperProcessor;
import cn.ibaijia.jsm.http.DefaultHttpMapperProcessorImpl;

/**
 * @Author: LongZL
 * @Date: 2022/1/25 14:21
 */
public class JsmConfigurer {

    private static JsmAppListener jsmAppListenerService = new DefaultJsmAppListener();

    private static JsmSessionListener jsmSessionListener = new DefaultJsmSessionListener();

    private static RestStatusStrategy restStatusStrategy = new DefaultRestStatusStrategy();

    private static JsmSecurityService  webJsmSecurityService = new DefaultJsmSecurityServiceImpl();

    private static DbGenerationStrategy dbGenerationStrategy = new DefaultMySqlGenerationStrategy();

    private static HttpMapperProcessor httpMapperProcessor = new DefaultHttpMapperProcessorImpl();

    public static HttpMapperProcessor getHttpMapperProcessor() {
        return httpMapperProcessor;
    }

    public static void setHttpMapperProcessor(HttpMapperProcessor httpMapperProcessor) {
        JsmConfigurer.httpMapperProcessor = httpMapperProcessor;
    }

    public static DbGenerationStrategy getDbGenerationStrategy() {
        return dbGenerationStrategy;
    }

    public static void setDbGenerationStrategy(DbGenerationStrategy dbGenerationStrategy) {
        JsmConfigurer.dbGenerationStrategy = dbGenerationStrategy;
    }

    public static JsmSecurityService getWebJsmSecurityService() {
        return webJsmSecurityService;
    }

    public static void setWebJsmSecurityService(JsmSecurityService webJsmSecurityService) {
        JsmConfigurer.webJsmSecurityService = webJsmSecurityService;
    }

    public static RestStatusStrategy getRestStatusStrategy() {
        return restStatusStrategy;
    }

    public static void setRestStatusStrategy(RestStatusStrategy restStatusStrategy) {
        JsmConfigurer.restStatusStrategy = restStatusStrategy;
    }

    public static JsmSessionListener getJsmSessionListener() {
        return jsmSessionListener;
    }

    public static void setJsmSessionListener(JsmSessionListener jsmSessionListener) {
        JsmConfigurer.jsmSessionListener = jsmSessionListener;
    }

    public static JsmAppListener getJsmAppListenerService() {
        return jsmAppListenerService;
    }

    public static void setJsmAppListenerService(JsmAppListener jsmAppListenerService) {
        JsmConfigurer.jsmAppListenerService = jsmAppListenerService;
    }
}
