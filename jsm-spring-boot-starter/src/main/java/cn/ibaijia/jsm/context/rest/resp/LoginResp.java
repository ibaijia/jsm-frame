package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @author longzl
 */
public class LoginResp implements ValidateModel {
    @FieldAnn(comments = "加密后的token")
    public String token;
    @FieldAnn(comments = "token明文, 仅供调试使用，生产环境不会返回这个字段。")
    public String devToken;
    @FieldAnn(comments = "服务器时间")
    public Long serverTime;
}
