package cn.ibaijia.jsm.context.job;

import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

/**
 * @author longzl
 */
public abstract class BaseJsmJob implements JsmJob {

    protected Logger logger = LogUtil.log(getClass());

    @Override
    public void onLoad() {
        //Do Nothing
    }

    @Override
    public boolean isOpen() {
        return true;
    }
}
