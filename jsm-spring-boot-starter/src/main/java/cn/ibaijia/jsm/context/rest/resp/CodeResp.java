package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

import java.io.Serializable;

/**
 * @author longzl
 */
public class CodeResp<C> implements JsmResp, ValidateModel, Serializable {
    @FieldAnn(comments = "1001表示成功，其它参照错误代码表", mockValue = "1001")
    public C code;
    @FieldAnn(comments = "响应码对应描述", mockValue = "success")
    public String message;
}
