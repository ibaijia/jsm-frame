package cn.ibaijia.jsm.context.interceptor;

import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.async.CallableProcessingInterceptor;

import java.util.concurrent.Callable;

/**
 * @Author: LongZL
 * @Date: 2022/8/1 15:41
 */
public class JsmTimeoutCallableProcessingInterceptor implements CallableProcessingInterceptor {

    private static Logger logger = LogUtil.log(JsmRestInterceptor.class);

    @Override
    public <T> Object handleTimeout(NativeWebRequest request, Callable<T> task) throws Exception {
        logger.warn("handleTimeout timeout!");
        return CallableProcessingInterceptor.super.handleTimeout(request, task);
    }
}
