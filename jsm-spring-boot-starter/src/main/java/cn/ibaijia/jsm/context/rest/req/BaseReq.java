package cn.ibaijia.jsm.context.rest.req;

import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

/**
 * @author longzl
 */
public abstract class BaseReq implements ValidateModel {

    protected Logger logger = LogUtil.log(getClass());

    public String toJsonString() {
        return JsonUtil.toJsonString(this);
    }
}
