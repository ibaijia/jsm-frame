package cn.ibaijia.jsm.context.service;

import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.ExceptionUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

/**
 * @author longzl
 */
public abstract class BaseService {
    protected Logger logger = LogUtil.log(getClass());

    protected void failed(String errorMsg) {
        ExceptionUtil.failed(errorMsg);
    }

    protected void failed(Pair errorPair) {
        ExceptionUtil.failed(errorPair);
    }

    protected void failed(Pair errorPair, String errorMsg) {
        ExceptionUtil.failed(errorPair, errorMsg);
    }

    protected void failed(RestResp restResp) {
        ExceptionUtil.failed(restResp);
    }

    protected void notFound(String errorMsg) {
        ExceptionUtil.notFound(errorMsg);
    }

    protected void notFound(Pair errorPair) {
        ExceptionUtil.notFound(errorPair);
    }

    protected void notFound(Pair errorPair, String errorMsg) {
        ExceptionUtil.notFound(errorPair, errorMsg);
    }
}
