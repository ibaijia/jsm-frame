package cn.ibaijia.jsm.context.service;

import cn.ibaijia.jsm.consts.BasePermissionConstants;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: LongZL
 * @Date: 2022/1/25 14:58
 */
public class DefaultJsmSecurityServiceImpl implements JsmSecurityService {

    private Logger logger = LogUtil.log(this.getClass());

    @Override
    public boolean hasRole(String role) {
        List<String> roleList = WebContext.currentRoles();
        if (roleList == null) {
            logger.error("user have no roleId!");
            return false;
        }
        boolean resList = false;
        String elsStr = null;
        try {
            ExpressionParser parser = new SpelExpressionParser();
            EvaluationContext context = new StandardEvaluationContext();
            context.setVariable("list", roleList);
            elsStr = createExpression(role);
            resList = parser.parseExpression(elsStr).getValue(context, Boolean.class);
        } catch (Exception e) {
            logger.error("role {},elsStr {} ", role, elsStr);
        }
        return resList;
    }

    @Override
    public boolean hasAnyRole(List<String> roleIdList) {
        for (String roleId : roleIdList) {
            if (hasRole(roleId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasPermission(String permissionCode) {
        List<String> permissionList = WebContext.currentPermissions();
        if (permissionList == null) {
            logger.error("user have no permission!");
            return false;
        }
        if (permissionList.contains(BasePermissionConstants.SUPER_ADMIN)) {
            return true;
        }
        boolean resList = false;
        String elsStr = null;
        try {
            ExpressionParser parser = new SpelExpressionParser();
            EvaluationContext context = new StandardEvaluationContext();
            context.setVariable("list", permissionList);
            elsStr = createExpression(permissionCode);
            resList = parser.parseExpression(elsStr).getValue(context, Boolean.class);
        } catch (Exception e) {
            logger.error("permissionCode {},elsStr {} ", permissionCode, elsStr);
        }
        return resList;
    }

    @Override
    public boolean hasAnyPermission(List<String> permissionCodeList) {
        for (String permissionCode : permissionCodeList) {
            if (hasPermission(permissionCode)) {
                return true;
            }
        }
        return false;
    }

    private String createExpression(String permission) {
        //"#permissions?.contains(1) || #permissions?.contains('4')"
        String pattern = "([0-9a-zA-Z_-]+)";
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(permission);
        String s = m.replaceAll("#list?.contains('$1')");
        return s;
    }

}
