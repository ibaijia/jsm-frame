package cn.ibaijia.jsm.context.rest.resp.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @Author: LongZL
 * @Date: 2022/6/2 18:36
 */
public class ExcelVo implements ValidateModel, OutputVo {

    @FieldAnn(comments = "文件名")
    public String fileName = "unknown_file_name.xls";

    @JSONField(serialize = false)
    @JsonIgnore
    @ApiParam(hidden = true)
    @FieldAnn(comments = "Workbook")
    public Workbook workbook;

    public ExcelVo() {
    }

    public ExcelVo(Workbook workbook) {
        this.workbook = workbook;
    }

    public ExcelVo(String fileName, Workbook workbook) {
        this.fileName = fileName;
        this.workbook = workbook;
    }
}

