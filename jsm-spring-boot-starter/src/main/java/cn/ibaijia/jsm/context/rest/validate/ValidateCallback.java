package cn.ibaijia.jsm.context.rest.validate;

/**
 * @author longzl
 */
public interface ValidateCallback {

	/**
	 * 验证逻辑
	 * @param filedVal
	 * @param vm
	 * @return 错误信息
	 */
	String exe(Object filedVal,ValidateModel vm);
	
}
