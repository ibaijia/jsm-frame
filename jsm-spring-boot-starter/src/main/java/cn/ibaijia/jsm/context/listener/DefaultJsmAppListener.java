package cn.ibaijia.jsm.context.listener;

import javax.servlet.ServletContextEvent;

/**
 * @Author: LongZL
 * @Date: 2022/1/25 14:27
 */
public class DefaultJsmAppListener implements JsmAppListener {

    @Override
    public void init(ServletContextEvent sce) {

    }

    @Override
    public void close(ServletContextEvent sce) {

    }
}
