package cn.ibaijia.jsm.context;

import cn.ibaijia.jsm.utils.StringUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.Locale;
import java.util.Map;

/**
 * @author longzl
 */
@Component
public class SpringContext implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext){
        SpringContext.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        checkApplicationContext();
        return applicationContext;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name) {
        checkApplicationContext();
        return (T) applicationContext.getBean(name);
    }

    /**
     *  从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
     *  从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
     *  如果有多个Bean符合Class, 取出第一个.
     * @param clazz
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> clazz) {
        checkApplicationContext();
        @SuppressWarnings("rawtypes")
        Map beanMaps = applicationContext.getBeansOfType(clazz);
        if (beanMaps != null && !beanMaps.isEmpty()) {
            return (T) beanMaps.values().iterator().next();
        } else {
            return null;
        }
    }

    /**
     *  获取继承指定 class的 bean
     * @param clazz
     * @return
     * @param <T>
     */
    public static <T> Map<String,T> getBeansOfType(Class<T> clazz){
        checkApplicationContext();
        Map<String,T> beanMaps = applicationContext.getBeansOfType(clazz);
        return beanMaps;
    }

    public static <T> T getBean(Class<T> clazz, WebApplicationContext applicationContext) {
        try {
            return applicationContext.getBean(clazz);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getMessage(String key, Object... args) {
        return getMessage(Locale.CHINA, key, args);
    }

    public static String getMessage(Locale locale, String key, Object... args) {
        if (applicationContext == null) {
            return key;
        }
        return applicationContext.getMessage(key, args, locale);
    }

    private static void checkApplicationContext() {
        if (applicationContext == null) {
            throw new IllegalStateException("applicaitonContext未注入,请在applicationContext.xml中定义SpringContextHolder");
        }
    }

    /**
     * 添加bean -- beanName默认 类名首字母小写
     */
    public void addBean(Class<?> beanClass) {
        String beanName = StringUtil.lowerCaseFirst(beanClass.getSimpleName());
        addBean(beanName,beanClass);
    }

    /**
     * 添加bean
     */
    public void addBean(String beanName, Class<?> beanClass) {
        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) applicationContext.getAutowireCapableBeanFactory();
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(beanClass);
        BeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
        if (!beanDefinitionRegistry.containsBeanDefinition(beanName)) {
            beanDefinitionRegistry.registerBeanDefinition(beanName, beanDefinition);
        }
    }

    /**
     *  移除bean
     */
    public void removeBean(String beanName) {
        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) applicationContext.getAutowireCapableBeanFactory();
        beanDefinitionRegistry.getBeanDefinition(beanName);
        beanDefinitionRegistry.removeBeanDefinition(beanName);
    }

}
