package cn.ibaijia.jsm.context.rest.resp.vo;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/6/2 18:36
 */
public class XmlVo implements ValidateModel, OutputVo {

    @FieldAnn(comments = "")
    public String content;

    public XmlVo() {
    }

    public XmlVo(String content) {
        this.content = content;
    }
}

