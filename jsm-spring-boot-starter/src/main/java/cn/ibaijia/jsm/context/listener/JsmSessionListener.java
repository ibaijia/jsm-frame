package cn.ibaijia.jsm.context.listener;

import javax.servlet.http.HttpSessionEvent;

/**
 * @author longzl
 */
public interface JsmSessionListener {

    /**
     * 初始化时调用
	 * @param httpSessionEvent
	 */
	void sessionCreated(HttpSessionEvent httpSessionEvent);

	/**
	 * 关闭时调用
	 * @param httpSessionEvent
	 */
	void sessionDestroyed(HttpSessionEvent httpSessionEvent);
	
}
