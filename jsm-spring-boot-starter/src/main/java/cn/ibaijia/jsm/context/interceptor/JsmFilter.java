package cn.ibaijia.jsm.context.interceptor;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.stat.JsmApiStatService;
import cn.ibaijia.jsm.stat.model.ApiStat;
import cn.ibaijia.jsm.utils.*;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author longzl / @createOn 2010-6-25
 */
public class JsmFilter extends OncePerRequestFilter {
    private Logger logger = LogUtil.log(this.getClass());
    private String encoding = "UTF-8";
    private String[] excludeArr = null;
    private boolean cors = AppContext.getAsBoolean(AppContextKey.CORS, false);
    private String corsAllowOrigin = AppContext.get(AppContextKey.CORS_ALLOW_ORIGIN, "*");
    private String corsAllowMethods = AppContext.get(AppContextKey.CORS_ALLOW_METHODS, "POST, GET, OPTIONS, DELETE, HEAD");
    private String corsAllowHeaders = AppContext.get(AppContextKey.CORS_ALLOW_HEADERS, "DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,access-control-allow-origin,Authorization,access_token,at,ht");
    private String corsAllowCredentials = AppContext.get(AppContextKey.CORS_ALLOW_CREDENTIALS, "true");
    private String corsMaxAge = AppContext.get(AppContextKey.CORS_MAX_AGE, "3600");

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    @Override
    protected void initFilterBean() throws ServletException {
        super.initFilterBean();
        String exclude = getFilterConfig().getInitParameter("exclude");
        if (!StringUtil.isEmpty(exclude)) {
            excludeArr = exclude.split(";");
        }
    }

    private boolean isExclude(HttpServletRequest request) {
        if (excludeArr != null) {
            String servletPath = request.getServletPath();
            for (String pattern : excludeArr) {
                if (servletPath.startsWith(pattern)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain fc) {
        String traceId = WebContext.getTraceId();
        response.setHeader(WebContext.JSM_TRACE_ID, traceId);
        if (cors) {
            response.setHeader("Access-Control-Allow-Origin", corsAllowOrigin);
            response.setHeader("Access-Control-Allow-Methods", corsAllowMethods);
            response.setHeader("Access-Control-Allow-Headers", corsAllowHeaders);
            response.setHeader("Access-Control-Allow-Credentials", corsAllowCredentials);
            response.setHeader("Access-Control-Max-Age", corsMaxAge);
        }
        if (isExclude(request)) {
            try {
                fc.doFilter(request, response);
            } catch (Exception e) {
                logger.error("", e);
            }
            return;
        }
        WebContext.setRequest(request);
        WebContext.setResponse(response);
        String reqMethod = request.getMethod();
        String uri = request.getRequestURI();
        String uriInfo = String.format("%s %s", reqMethod, uri);
        logger.info(uriInfo);
        long startTime = System.currentTimeMillis();
        response.setCharacterEncoding(this.encoding);
        try {
            checkAndSetClientId();
            if (reqMethod.equals(RequestMethod.OPTIONS.name())) {
                String error = "request method not support.";
                logger.warn("request method not support {}", reqMethod);
                throw new RuntimeException(error);
            } else {
                logger.debug("before fc.doFilter");
                fc.doFilter(request, response);
                logger.debug("after fc.doFilter");
            }
        } catch (Exception e) {
            String error = "request error url:" + request.getRequestURL().toString();
            logger.error(error, e);
            RestResp<String> errorResp = JsmFrameUtil.dealException(e);
            ResponseUtil.outputRestResp(response, errorResp);
        } finally {
            statApi(request, response, uriInfo, startTime, traceId);
            ThreadLocalUtil.cleanAll();
        }
    }

    private void checkAndSetClientId() {
        String clientId = WebContext.getClientId();
        logger.debug("{}={}", WebContext.JSM_CLIENT_ID,clientId);
    }


    private void statApi(HttpServletRequest request, HttpServletResponse response, String uriInfo, long startTime, String traceId) {
        JsmApiStatService apiStatService = SpringContext.getBean(JsmApiStatService.class);
        long endTime = System.currentTimeMillis();
        long spendTime = endTime - startTime;
        ApiStat apiStat = new ApiStat();
        apiStat.clientId = WebContext.getClientId();
        apiStat.traceId = traceId;
        apiStat.clusterId = AppContext.getClusterId();
        apiStat.url = uriInfo;
        apiStat.beginTime = startTime;
        apiStat.endTime = endTime;
        apiStat.spendTime = spendTime;
        apiStat.httpStatus = response.getStatus();
        apiStat.respCode = (String) request.getAttribute(WebContext.JSM_RESP_CODE);
        apiStat.time = DateUtil.currentTime();
        apiStatService.add(apiStat);
    }

}
