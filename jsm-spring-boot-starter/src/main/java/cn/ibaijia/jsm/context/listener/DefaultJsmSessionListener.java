package cn.ibaijia.jsm.context.listener;

import javax.servlet.http.HttpSessionEvent;

/**
 * @Author: LongZL
 * @Date: 2022/1/25 14:43
 */
public class DefaultJsmSessionListener implements JsmSessionListener{
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
