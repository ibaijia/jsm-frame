package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.utils.JsonUtil;

/**
 * @author longzl
 */
public class IntegerResp<T> extends CodeResp<Integer> {
    @FieldAnn(required = false, comments = "响应结果")
    public T result;

    public IntegerResp() {
        super();
    }

    @Override
    public String toString() {
        return JsonUtil.toJsonString(this);
    }

}
