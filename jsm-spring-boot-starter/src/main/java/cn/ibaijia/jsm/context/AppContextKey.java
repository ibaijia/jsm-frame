package cn.ibaijia.jsm.context;

/**
 * @author longzl
 */
public class AppContextKey {

    public static final String DEV_MODEL = "jsm.devModel";
    public static final String SESSION_TYPE = "jsm.session.type";
    public static final String SESSION_EXPIRE_TIME = "jsm.session.expireTime";
    public static final String AT_EXPIRE_TIME = "jsm.at.expireTime";

    public static final String ENV = "JSM_ENV";
    public static final String GIT_HASH = "JSM_GIT_HASH";
    public static final String APP_NAME = "jsm.appName";
    public static final String CLUSTER_ID = "jsm.clusterId";
    public static final String OPT_LOG_RECORD_URL = "jsm.optlog.recordUrl";

    public static final String CACHE_SERVICE_ASYNC_THREAD = "jsm.cache.async.thread";

    public static final String JSON_NAMING_STRATEGY = "jsm.json.namingStrategy";
    public static final String JSON_SHOW_NULL = "jsm.json.showNull";
    public static final String JSON_BOOLEAN_NULL_EMPTY = "jsm.json.booleanNullFalse";
    public static final String JSON_NUMBER_NULL_EMPTY = "jsm.json.numberNullZero";
    public static final String JSON_STRING_NULL_EMPTY = "jsm.json.stringNullEmpty";
    public static final String JSON_ARRAY_NULL_EMPTY = "jsm.json.arrayNullEmpty";


    public static final String PROXY_USERNAME = "proxy.username";
    public static final String PROXY_PASSWORD = "proxy.password";

    public static final String HTTP_PROXY_HOST = "http.proxyHost";
    public static final String HTTP_PROXY_PORT = "http.proxyPort";
    public static final String HTTP_NON_PROXY_HOSTS = "http.nonProxyHosts";

    public static final String HTTPS_PROXY_HOST = "https.proxyHost";
    public static final String HTTPS_PROXY_PORT = "https.proxyPort";
    public static final String HTTPS_NON_PROXY_HOSTS = "https.nonProxyHosts";

    public static final String FTP_PROXY_HOST = "ftp.proxyHost";
    public static final String FTP_PROXY_PORT = "ftp.proxyPort";
    public static final String FTP_NON_PROXY_HOSTS = "ftp.nonProxyHosts";

    public static final String SOCKS_PROXY_HOST = "socks.proxyHost";
    public static final String SOCKS_PROXY_PORT = "socks.proxyPort";
    public static final String SOCKS_NON_PROXY_HOSTS = "socks.nonProxyHosts";


    public static final String CORS = "jsm.cors.open";
    public static final String CORS_ALLOW_ORIGIN = "jsm.cors.allowOrigin";
    public static final String CORS_ALLOW_METHODS = "jsm.cors.allowMethods";
    public static final String CORS_ALLOW_HEADERS = "jsm.cors.allowHeaders";
    public static final String CORS_ALLOW_CREDENTIALS = "jsm.cors.allowCredentials";
    public static final String CORS_MAX_AGE = "jsm.cors.maxAge";

    public static final String OAUTH_CLIENT_APP_KEY = "jsm.oauth.client.appKey";
    public static final String OAUTH_CLIENT_APP_SECRET = "jsm.oauth.client.appSecret";
    public static final String OAUTH_CLIENT_TOKEN_NAME = "jsm.oauth.client.tokenName";
    public static final String OAUTH_CLIENT_BASE_URL = "jsm.oauth.client.baseUrl";
    public static final String OAUTH_CLIENT_VERIFY_URL = "jsm.oauth.client.verifyUrl";
    public static final String OAUTH_CLIENT_EXPIRE_IN = "jsm.oauth.client.expireIn";
    public static final String OAUTH_CLIENT_TOKEN_URL = "jsm.oauth.client.getTokenUrl";
    public static final String OAUTH_CLIENT_REFRESH_TOKEN_URL = "jsm.oauth.client.refreshTokenUrl";

    public static final String OAUTH_PASSWORD_APP_KEY = "jsm.oauth.password.appKey";
    public static final String OAUTH_PASSWORD_APP_SECRET = "jsm.oauth.password.appSecret";
    public static final String OAUTH_PASSWORD_TOKEN_NAME = "jsm.oauth.password.tokenName";
    public static final String OAUTH_PASSWORD_BASE_URL = "jsm.oauth.password.baseUrl";
    public static final String OAUTH_PASSWORD_VERIFY_URL = "jsm.oauth.password.verifyUrl";
    public static final String OAUTH_PASSWORD_EXPIRE_IN = "jsm.oauth.password.expireIn";

    public static final String OAUTH_GATEWAY_TOKEN_NAME = "jsm.oauth.gateway.tokenName";
    public static final String OAUTH_GATEWAY_PREFIX = "jsm.oauth.gateway.prefix";
    /**
     * 多个用 分号;隔开
     */
    public static final String OAUTH_GATEWAY_HEADERS = "jsm.oauth.gateway.headers";

    public static final String ELASTIC_ADDRESS = "jsm.elastic.address";
    public static final String ELASTIC_USER = "jsm.elastic.user";
    public static final String ELASTIC_PASS = "jsm.elastic.pass";

    public static final String STAT_ELASTIC_ADDRESS = "jsm.stat.elastic.address";
    public static final String STAT_ELASTIC_USER = "jsm.stat.elastic.user";
    public static final String STAT_ELASTIC_PASS = "jsm.stat.elastic.pass";

    public static final String SCHEDULE_DYNA_JOB_ENABLE = "jsm.schedule.dynaJobEnable";
    public static final String SCHEDULE_DYNA_POOL_SIZE = "jsm.schedule.dynaPoolSize";
    public static final String SCHEDULE_QUARTZ_POOL_SIZE = "jsm.schedule.quartzPoolSize";

    public static final String LOG_TYPE = "jsm.log.type";
    public static final String LOG_IDX = "jsm.log.idx";

    public static final String API_STAT_TYPE = "jsm.apiStat.type";
    public static final String API_STAT_IDX = "jsm.apiStat.idx";

    public static final String SYS_STAT_TYPE = "jsm.sysStat.type";
    public static final String SYS_STAT_IDX = "jsm.sysStat.idx";
    public static final String SYS_STAT_INTERVAL = "jsm.sysStat.interval";

    public static final String OPT_LOG_TYPE = "jsm.optLog.type";
    public static final String OPT_LOG_IDX = "jsm.optLog.idx";

    public static final String ALARM_TYPE = "jsm.alarm.type";
    public static final String ALARM_IDX = "jsm.alarm.idx";
    public static final String ALARM_MAX_COUNT = "jsm.alarm.max";

    public static final String AMS_BOSS_HOST = "jsm.amsBoss.host";
    public static final String AMS_BOSS_APP_KEY = "jsm.amsBoss.appKey";
    public static final String AMS_BOSS_APP_SECRET = "jsm.amsBoss.appSecret";

    public static final String CONTAINER_STAT_IDX = "jsm.containerStat.idx";

    public static final String SLOW_API_LIMIT = "jsm.slow.apiLimit";
    public static final String SLOW_SQL_LIMIT = "jsm.slow.sqlLimit";

    public static final String GEN_PACKAGE_MODEL = "jsm.gen.package.model";
    public static final String GEN_PACKAGE_MAPPER = "jsm.gen.package.mapper";
    public static final String GEN_PACKAGE_SERVICE = "jsm.gen.package.service";
    public static final String GEN_PACKAGE_REST = "jsm.gen.package.rest";
    public static final String GEN_DIR_API = "jsm.gen.dir.api";

    public static final String SWAGGER_ENABLE = "jsm.swagger.enable";
    public static final String SWAGGER_PACKAGE = "jsm.swagger.restPackage";
    public static final String SWAGGER_TITLE = "jsm.swagger.title";
    public static final String SWAGGER_DESCRIPTION = "jsm.swagger.description";
    public static final String SWAGGER_VERSION = "jsm.swagger.version";
    public static final String SWAGGER_HEADERS = "jsm.swagger.headers";


    public static final String REGEX_VALID_EMAIL = "regex.valid.email";
    public static final String REGEX_VALID_CHINESE = "regex.valid.chinese";
    public static final String REGEX_VALID_IPV4 = "regex.valid.ipv4";
    public static final String REGEX_VALID_DATE = "regex.valid.date";
    public static final String REGEX_VALID_DATETIME = "regex.valid.datetime";
    public static final String REGEX_VALID_URL = "regex.valid.url";
    public static final String REGEX_VALID_IDCARD = "regex.valid.idcard";
    public static final String REGEX_VALID_TELNO = "regex.valid.telno";
    public static final String REGEX_VALID_MOBNO = "regex.valid.mobno";
    public static final String REGEX_VALID_NUMBER = "regex.valid.number";
    public static final String REGEX_VALID_ENNUM = "regex.valid.ennum";
    public static final String REGEX_VALID_ACCOUNT = "regex.valid.account";
    public static final String REGEX_VALID_MONEY = "regex.valid.money";
    public static final String REGEX_VALID_BANKCARD = "regex.valid.bankcard";
    public static final String REGEX_EXTRACT_MONEY = "regex.extract.money";
    public static final String REGEX_EXTRACT_NUMBER = "regex.extract.number";

}
