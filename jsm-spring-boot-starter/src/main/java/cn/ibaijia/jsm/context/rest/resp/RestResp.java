package cn.ibaijia.jsm.context.rest.resp;

import cn.ibaijia.jsm.consts.BasePairConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author longzl
 */
public class RestResp<T> extends StringResp<T> {

    @JsonIgnore
    public boolean isOk() {
        return BasePairConstants.OK.getCode().equals(this.code);
    }

    @JsonIgnore
    public int getHttpStatusCode() {
        if (isOk()) {
            return 200;
        } else if (BasePairConstants.NOT_FOUND.getCode().equals(code)) {
            return 404;
        } else if (BasePairConstants.NO_LOGIN.getCode().equals(code) || BasePairConstants.AUTH_FAIL.getCode().equals(code)
                || BasePairConstants.AT_INVALID.getCode().equals(code) || BasePairConstants.RT_INVALID.getCode().equals(code)) {
            return 401;
        } else if (BasePairConstants.NO_PERMISSION.getCode().equals(code) || BasePairConstants.IP_LIMIT.getCode().equals(code)
                || BasePairConstants.NET_LIMIT.getCode().equals(code) || BasePairConstants.LICENSE_EXPIRED.getCode().equals(code)) {
            return 403;
        }
        return 400;
    }

}
