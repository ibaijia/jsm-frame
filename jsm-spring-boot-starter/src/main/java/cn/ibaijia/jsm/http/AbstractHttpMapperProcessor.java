package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.http.ann.*;
import cn.ibaijia.jsm.utils.*;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * @author longzl
 */
public abstract class AbstractHttpMapperProcessor implements HttpMapperProcessor {
    protected Logger logger = LogUtil.log(this.getClass());

    @Override
    public <T> Object process(FactoryBean bean) {
        Class<T> interfaceClass = bean.getObjectType();
        Object mapper = Proxy.newProxyInstance(bean.getClass().getClassLoader(), new Class<?>[]{interfaceClass}, (proxy, method, args) -> {
            HttpMapper httpMapper = interfaceClass.getAnnotation(HttpMapper.class);
            HttpMapping httpMapping = AnnotatedElementUtils.getMergedAnnotation(method, HttpMapping.class);
            if (httpMapping != null) {
                Object result = process(httpMapper, httpMapping, method, args);
                if ((result instanceof RestResp) && !((RestResp<?>) result).isOk()) {
                    ExceptionUtil.failed(((RestResp<?>) result));
                }
                return result;
            } else {
                throw new RuntimeException("No HttpMapping on method:" + method.getName());
            }
        });
        return mapper;
    }

    /**
     * 处理逻辑
     *
     * @param httpMapper
     * @param httpMapping
     * @param method
     * @param args
     * @return
     */
    abstract Object process(HttpMapper httpMapper, HttpMapping httpMapping, Method method, Object[] args);

    protected void processByHttpClient(HttpMapping mapping, boolean withExtra, boolean allowAllSsl, List<Header> headerList, String url, Object requestData, HttpClientProcessor processor) {
        HttpClient httpClient = configHttpClient(withExtra, allowAllSsl);
        if (mapping.method().equals(RequestMethod.GET)) {
            url = buildGetUrl(url, requestData, httpClient);
            httpClient.get(headerList, url, processor);
        } else if (mapping.method().equals(RequestMethod.POST)) {
            httpClient.post(headerList, url, requestData, processor);
        } else if (mapping.method().equals(RequestMethod.PUT)) {
            httpClient.put(headerList, url, requestData, processor);
        } else if (mapping.method().equals(RequestMethod.DELETE)) {
            httpClient.delete(headerList, url, processor);
        }
    }

    protected String getResultByHttpClient(HttpMapping mapping, boolean withExtra, boolean allowAllSsl, List<Header> headerList, String url, Object requestData) {
        HttpClient httpClient = configHttpClient(withExtra, allowAllSsl);
        String res = null;
        if (mapping.method().equals(RequestMethod.GET)) {
            url = buildGetUrl(url, requestData, httpClient);
            res = httpClient.get(headerList, url);
        } else if (mapping.method().equals(RequestMethod.POST)) {
            res = httpClient.post(headerList, url, requestData);
        } else if (mapping.method().equals(RequestMethod.PUT)) {
            res = httpClient.put(headerList, url, requestData);
        } else if (mapping.method().equals(RequestMethod.DELETE)) {
            res = httpClient.delete(headerList, url);
        }
        return res;
    }

    protected HttpClient configHttpClient(boolean withExtra, boolean allowAllSsl) {
        HttpClient httpClient = new HttpClient(false);
        httpClient.setWithExtra(withExtra);
        httpClient.setAllowAllSsl(allowAllSsl);
        return httpClient;
    }

    protected String buildGetUrl(String url, Object requestData, HttpClient httpClient) {
        if (requestData != null) {
            if (requestData instanceof List) {
                List<?> paramList = (List<?>) requestData;
                if (paramList.isEmpty()) {
                    if (paramList.get(0) instanceof NameValuePair) {
                        url = httpClient.buildUrl(url, (List<NameValuePair>) paramList);
                    } else {
                        logger.warn("get method ignore non List<NameValuePair> requestData:{}", JsonUtil.toJsonString(requestData));
                    }
                }
            } else if (requestData instanceof ValidateModel) {
                url = httpClient.buildUrl(url, (ValidateModel) requestData);
            } else {
                logger.warn("get method ignore non List requestData:{}", JsonUtil.toJsonString(requestData));
            }
        }
        return url;
    }

    protected String formatUrl(String mappingUri, String mappingBaseUri, String baseUri, Method method, Object[] args) {
        String url = mappingUri;
        if (!StringUtil.isEmpty(mappingBaseUri)) {
            url = mappingBaseUri + url;
        }
        if (!url.startsWith(BaseConstants.PREFIX_HTTP) && !url.startsWith(BaseConstants.PREFIX_HTTPS)) {
            url = baseUri + url;
        }
        url = TemplateUtil.formatWithEnvVar(url);
        StringBuilder paramsSb = new StringBuilder();
        if (args != null && args.length > 0) {
            Map<String, Object> data = new HashMap<>(8);
            int i = 0;
            for (Parameter parameter : method.getParameters()) {
                HttpPathVariable httpPathVariable = parameter.getAnnotation(HttpPathVariable.class);
                String name = parameter.getName();
                if (httpPathVariable != null) {
                    name = httpPathVariable.value();
                    data.put(name, args[i]);
                }
                HttpParam httpParam = parameter.getAnnotation(HttpParam.class);
                if (httpParam != null) {
                    if (paramsSb.length() > 0) {
                        paramsSb.append(BaseConstants.SYSTEM_SYMBOL_AND);
                    }
                    paramsSb.append(httpParam.value()).append(BaseConstants.SYSTEM_SYMBOL_EQUAL).append(args[i]);
                }
                i++;
            }
            url = TemplateUtil.format(url, data);
        }
        if (paramsSb.length() > 0) {
            if (url.contains(BaseConstants.SYSTEM_SYMBOL_QUERY)) {
                url = url + BaseConstants.SYSTEM_SYMBOL_AND + paramsSb;
            } else {
                url = url + BaseConstants.SYSTEM_SYMBOL_QUERY + paramsSb;
            }
        }
        return TemplateUtil.formatWithContextVar(url);
    }

    /**
     * 最后一个没有 HttpParam 的参数 且不是HttpClientProcessor，则为RequestBodyData
     *
     * @param method
     * @param args
     * @return
     */
    protected Object getRequestData(Method method, Object[] args) {
        Object dataParam = null;
        if (args != null && args.length > 0) {
            int i = 0;
            for (Parameter parameter : method.getParameters()) {
                HttpParam httpParam = parameter.getAnnotation(HttpParam.class);
                if (httpParam == null && !(args[i] instanceof HttpClientProcessor)) {
                    dataParam = args[i];
                }
                i++;
            }
        }
        return dataParam;
    }

    protected List<Header> getHeaderList(Method method, Object[] args) {
        List<Header> list = new ArrayList<>();
        if (args != null && args.length > 0) {
            int i = 0;
            for (Parameter parameter : method.getParameters()) {
                HttpHeader httpHeader = parameter.getAnnotation(HttpHeader.class);
                if (httpHeader != null && args[i] != null) {
                    if ((args[i] instanceof List) && (list.get(0) instanceof Header)) {
                        list.addAll((List) args[i]);
                    } else if ((args[i] instanceof Set) && (list.get(0) instanceof Header)) {
                        list.addAll((Set) args[i]);
                    } else {
                        list.add(new BasicHeader(httpHeader.value(), StringUtil.toString(args[i])));
                    }
                }
                i++;
            }
        }
        return list.isEmpty() ? null : list;
    }

    protected HttpClientProcessor findHttpClientProcessor(Object[] args) {
        if (args != null && args.length > 0) {
            int i = 0;
            for (Object arg : args) {
                if (arg instanceof HttpClientProcessor) {
                    return (HttpClientProcessor) arg;
                }
            }
        }
        return null;
    }
}
