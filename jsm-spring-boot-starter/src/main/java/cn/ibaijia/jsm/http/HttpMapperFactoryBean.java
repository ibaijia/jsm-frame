package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.utils.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.FactoryBean;

/**
 * @Author: LongZL
 * @Date: 2022/6/21 16:52
 */
public class HttpMapperFactoryBean<T> implements FactoryBean {

    private static Logger logger = LogUtil.log(HttpMapperFactoryBean.class);

    private Class<T> interfaceClass;

    public HttpMapperFactoryBean(Class<T> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    @Override
    public Object getObject() throws Exception {
        return getProxyMapper();
    }

    @Override
    public Class<?> getObjectType() {
        return interfaceClass;
    }

    @Override
    public boolean isSingleton() {
        return org.springframework.beans.factory.FactoryBean.super.isSingleton();
    }

    private Object getProxyMapper() {
        HttpMapperProcessor processor = JsmConfigurer.getHttpMapperProcessor();
        return processor.process(this);
    }

}
