package cn.ibaijia.jsm.http.ann;

import cn.ibaijia.jsm.http.HttpMapperBeanRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author longzl
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({HttpMapperBeanRegister.class})
public @interface HttpMapperScan {

	String[] basePackages();

}
