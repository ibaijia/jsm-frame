package cn.ibaijia.jsm.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import cn.ibaijia.jsm.utils.LogUtil;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

/**
 * @author longzl
 */
public class HttpBrowser {
    private Logger logger = LogUtil.log(HttpBrowser.class);
    private List<Header> headers;
    private HttpClientContext context = null;
    private CookieStore cookieStore = null;
    private HttpHost proxyHost = null;

    private static final int MIN_REDIRECT_CODE = 301;
    private static final int MAX_REDIRECT_CODE = 308;


    private HttpBrowser(List<Header> headers, HttpHost proxyHost) {
        if (headers != null) {
            this.headers = headers;
        } else {
            this.headers = getDefaultHeader();
        }
        context = HttpClientContext.create();
        cookieStore = new BasicCookieStore();
        this.proxyHost = proxyHost;
        // 配置超时时间（连接服务端超时1秒，请求数据返回超时2秒）  
//        requestConfig = RequestConfig.custom().setConnectTimeout(120000).setSocketTimeout(60000).setConnectionRequestTimeout(60000).build(); 
        // 设置默认跳转以及存储cookie  
    }

    private CloseableHttpClient createClient() {
        SSLConnectionSocketFactory sslcsf = null;
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            }).build();
            // AllowAllHostnameVerifier: 这种方式不对主机名进行验证，验证功能被关闭，是个空操作(域名验证)
            sslcsf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
        } catch (Exception e) {
            logger.error("create ssl socket factory error!", e);
        }

        HttpClientBuilder hcb = HttpClientBuilder.create()
//        		.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())  
                .setRedirectStrategy(new DefaultRedirectStrategy())
//	            .setDefaultRequestConfig(requestConfig)  
                .setDefaultCookieStore(cookieStore)
                .setDefaultHeaders(this.headers);
        if (proxyHost != null) {
            hcb.setProxy(proxyHost);
        }
        if (sslcsf != null) {
            hcb.setSSLSocketFactory(sslcsf);
        }
        return hcb.build();
    }

    private HttpGet proxyHttpGet(HttpGet httpGet) {
        if (this.proxyHost != null) {
            RequestConfig config = RequestConfig.custom().setProxy(this.proxyHost).build();
            httpGet.setConfig(config);
        }
        return httpGet;
    }

    private HttpPost proxyHttpPost(HttpPost httpPost) {
        if (this.proxyHost != null) {
            RequestConfig config = RequestConfig.custom().setProxy(this.proxyHost).build();
            httpPost.setConfig(config);
        }
        return httpPost;
    }

    private HttpPut proxyHttpPut(HttpPut httpPut) {
        if (this.proxyHost != null) {
            RequestConfig config = RequestConfig.custom().setProxy(this.proxyHost).build();
            httpPut.setConfig(config);
        }
        return httpPut;
    }

    private HttpDelete proxyHttpDelete(HttpDelete httpDelete) {
        if (this.proxyHost != null) {
            RequestConfig config = RequestConfig.custom().setProxy(this.proxyHost).build();
            httpDelete.setConfig(config);
        }
        return httpDelete;
    }

    public static HttpBrowser open() {
        return open(getDefaultHeader());
    }

    public static HttpBrowser open(List<Header> headers) {
        HttpBrowser httpBrowser = new HttpBrowser(headers, null);
        return httpBrowser;
    }

    public static HttpBrowser open(List<Header> headers, HttpHost proxyHost) {
        HttpBrowser httpBrowser = new HttpBrowser(headers, proxyHost);
        return httpBrowser;
    }

    public static HttpBrowser open(HttpHost proxyHost) {
        HttpBrowser httpBrowser = new HttpBrowser(null, proxyHost);
        return httpBrowser;
    }

    public String get(String url) {
        return get(url, "UTF-8");
    }

    public String get(String url, String charset) {
        logger.info("get url:" + url);
        String res = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = createClient();
            HttpGet httpGet = new HttpGet(url);
            httpGet = proxyHttpGet(httpGet);
            CloseableHttpResponse response = httpClient.execute(httpGet, context);
            try {
                logger.info("get url status:" + response.getStatusLine());
                res = EntityUtils.toString(response.getEntity(), charset);
            } catch (Exception e) {
                logger.error("http get read error:" + url, e);
            } finally {
                response.close();
            }
        } catch (Exception e) {
            logger.error("http get error:" + url, e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    logger.error("close httpClient error:" + url, e);
                }
            }
        }
        return res;
    }

    /**
     * List <NameValuePair> nvps = new ArrayList <NameValuePair>();
     * nvps.add(new BasicNameValuePair("username", "vip"));
     * nvps.add(new BasicNameValuePair("password", "secret"));
     *
     * @param url
     * @param nvps
     */
    public String post(String url, List<NameValuePair> nvps) {
        return post(url, nvps, "UTF-8");
    }

    public String post(String url, List<NameValuePair> nvps, String charset) {
        logger.info("post url:" + url);
        String res = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = createClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost = proxyHttpPost(httpPost);
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, charset));
            CloseableHttpResponse response = httpClient.execute(httpPost, context);
            try {
                logger.info(response.getStatusLine().toString());
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode >= MIN_REDIRECT_CODE && statusCode <= MAX_REDIRECT_CODE) {
                    Header locationHeader = response.getFirstHeader("location");
                    if (locationHeader != null) {
                        res = locationHeader.toString();
                    }
                } else {
                    res = EntityUtils.toString(response.getEntity(), charset);
                }
            } catch (Exception e) {
                logger.error("http get read error:" + url, e);
            } finally {
                response.close();
            }
        } catch (Exception e) {
            logger.error("http get error:" + url, e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    logger.error("close httpClient error:" + url, e);
                }
            }
        }

        return res;
    }

    public String post(String url, String strEntity) {
        return post(url, strEntity, "UTF-8");
    }

    public String post(String url, String strEntity, String charset) {
        logger.info("post url:" + url);
        String res = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = createClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost = proxyHttpPost(httpPost);
            StringEntity entity = new StringEntity(strEntity, charset);
            entity.setContentEncoding(charset);
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            CloseableHttpResponse response = httpClient.execute(httpPost, context);

            try {
                logger.info(response.getStatusLine().toString());
                res = EntityUtils.toString(response.getEntity(), charset);
            } catch (Exception e) {
                logger.error("http get read error:" + url, e);
            } finally {
                response.close();
            }
        } catch (Exception e) {
            logger.error("http post error:" + url, e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    logger.error("close httpClient error:" + url, e);
                }
            }
        }
        return res;
    }

    public String put(String url, String strEntity) {
        return put(url, strEntity, "UTF-8");
    }

    public String put(String url, String strEntity, String charset) {
        logger.info("put url:" + url);
        String res = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = createClient();
            HttpPut httpPut = new HttpPut(url);
            httpPut = proxyHttpPut(httpPut);
            StringEntity entity = new StringEntity(strEntity, charset);
            entity.setContentEncoding(charset);
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
            CloseableHttpResponse response = httpClient.execute(httpPut, context);

            try {
                logger.info(response.getStatusLine().toString());
                res = EntityUtils.toString(response.getEntity(), charset);
            } catch (Exception e) {
                logger.error("http put read error:" + url, e);
            } finally {
                response.close();
            }
        } catch (Exception e) {
            logger.error("http put error:" + url, e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    logger.error("close httpClient error:" + url, e);
                }
            }
        }
        return res;
    }

    public String delete(String url) {
        return delete(url, "UTF-8");
    }

    public String delete(String url, String charset) {
        logger.info("delete url:" + url);
        String res = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = createClient();
            HttpDelete httpDelete = new HttpDelete(url);
            httpDelete = proxyHttpDelete(httpDelete);
            CloseableHttpResponse response = httpClient.execute(httpDelete, context);
            try {
                logger.info("delete url status:" + response.getStatusLine());
                res = EntityUtils.toString(response.getEntity(), charset);
            } catch (Exception e) {
                logger.error("http delete read error:" + url, e);
            } finally {
                response.close();
            }
        } catch (Exception e) {
            logger.error("http delete error:" + url, e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    logger.error("close httpClient error:" + url, e);
                }
            }
        }
        return res;
    }


    public String getImage(String url, String savePath) {
        logger.info("getImage url:" + url);
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        FileOutputStream output = null;
        InputStream fis = null;
        try {
            httpClient = createClient();
            HttpGet httpGet = new HttpGet(url);
            httpGet = proxyHttpGet(httpGet);
            response = httpClient.execute(httpGet, context);
            logger.info("get image url:" + response.getStatusLine());
            output = new FileOutputStream(savePath);
            //得到网络资源的字节数组,并写入文件  
            fis = response.getEntity().getContent();
            byte[] b = new byte[1024];
            int j = 0;
            while ((j = fis.read(b)) != -1) {
                output.write(b, 0, j);
            }
            return savePath;

        } catch (Exception e) {
            logger.error("http get error:" + url, e);
            return null;
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (output != null) {
                    output.flush();
                    output.close();
                }
                if (response != null) {
                    response.close();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {
                logger.error("httpClient getImage close error!", e);
            }
        }
    }

    public String postImage(String url, String imagePath) {
        return postImage(url, imagePath, "UTF-8");
    }

    public String postImage(String url, String imagePath, String charset) {
        logger.info("postImage url:" + url);
        String res = null;
        try {
            CloseableHttpClient httpClient = createClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost = proxyHttpPost(httpPost);
            FileBody bin = new FileBody(new File(imagePath));
            long len = bin.getContentLength();
            logger.info("postImage image size:" + len / 1024 + "KB");
            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    .addPart("media", bin)
                    .build();
            httpPost.setEntity(reqEntity);
            CloseableHttpResponse response = httpClient.execute(httpPost, context);

            try {
                logger.info(response.getStatusLine().toString());
                res = EntityUtils.toString(response.getEntity(), charset);
                logger.info("postImage res:" + res);
            } catch (Exception e) {
                logger.error("http get read error:" + url, e);
            } finally {
                response.close();
                httpClient.close();
            }
        } catch (Exception e) {
            logger.error("http get error:" + url, e);
        }
        return res;
    }

    /**
     * 手动增加cookie
     *
     * @param name
     * @param value
     * @param domain
     * @param path
     */
    public void addCookie(String name, String value, String domain, String path) {
        BasicClientCookie cookie = new BasicClientCookie(name, value);
        cookie.setDomain(domain);
        cookie.setPath(path);
        cookieStore.addCookie(cookie);
    }

    private static List<Header> getDefaultHeader() {
        List<Header> headers = new ArrayList<Header>();
        headers.add(new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"));
        headers.add(new BasicHeader("Accept-Encoding", "gzip, deflate, sdch"));
        headers.add(new BasicHeader("Accept-Language", "zh-CN,zh;q=0.8"));
        headers.add(new BasicHeader("Cache-Control", "max-age=0"));
        headers.add(new BasicHeader("Connection", "keep-alive"));
        headers.add(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36"));
        return headers;

    }

}
