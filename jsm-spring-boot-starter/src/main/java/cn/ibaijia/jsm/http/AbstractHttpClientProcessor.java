package cn.ibaijia.jsm.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * @author longzl
 */
public abstract class AbstractHttpClientProcessor implements HttpClientProcessor {
    @Override
    public void before(HttpRequestBase httpRequestBase) {
    }

    /**
     * 完成后调用
     * @param response
     */
    @Override
    public abstract void complete(HttpResponse response);

    /**
     * 异常后调用
     * @param e
     */
    @Override
    public abstract void exception(Exception e);

    @Override
    public void after() {
    }
}
