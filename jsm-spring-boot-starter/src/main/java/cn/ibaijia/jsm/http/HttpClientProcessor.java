package cn.ibaijia.jsm.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
/**
 * @author longzl
 */
public interface HttpClientProcessor {

    /**调用前
     * @param httpRequestBase
     */
    void before(HttpRequestBase httpRequestBase);

    /**调用完成
     * @param response
     */
    void complete(HttpResponse response);

    /**调用异常
     * @param e
     */
    void exception(Exception e);

    /**
     * 调用后
     */
    void after();

}
