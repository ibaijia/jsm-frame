package cn.ibaijia.jsm.http.ann;

import java.lang.annotation.*;

/**
 * @author longzl
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HttpHeader {

    String value();

}
