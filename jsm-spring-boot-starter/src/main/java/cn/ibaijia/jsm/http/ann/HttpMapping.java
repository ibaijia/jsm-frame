package cn.ibaijia.jsm.http.ann;

import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.*;

/**
 * @author longzl
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HttpMapping {
    /**
     * API请求方法
     */
    RequestMethod method();

    /**
     * API BASE URI
     */
    String baseUri() default "";

    /**
     * API URI
     */
    String uri();

    /**
     * 超时时间 秒
     */
    int timeout() default 60;

    /**
     * 是否传送额外头信息 如比如 access_token
     */
    boolean withExtra() default true;

    /**
     * 是否允许所有的SSL
     */
    boolean allowAllSsl() default false;

}
