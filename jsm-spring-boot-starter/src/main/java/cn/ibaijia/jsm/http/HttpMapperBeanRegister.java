package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.http.ann.HttpMapperScan;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Author: LongZL
 * @Date: 2022/6/21 10:53
 */
public class HttpMapperBeanRegister implements ImportBeanDefinitionRegistrar {

    private static Logger logger = LogUtil.log(HttpMapperBeanRegister.class);

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        HttpMapperScanner scanner = new HttpMapperScanner(registry);
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(importingClassMetadata.getAnnotationAttributes(HttpMapperScan.class.getName()));
        if (attributes != null) {
            String[] stringArray = attributes.getStringArray("basePackages");
            scanner.doScan(stringArray);
        }
    }

}
