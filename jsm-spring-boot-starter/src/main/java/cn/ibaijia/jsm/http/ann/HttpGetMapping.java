package cn.ibaijia.jsm.http.ann;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author longzl
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@HttpMapping(method = RequestMethod.GET, uri = "")
public @interface HttpGetMapping {
    /**
     * API BASE URI
     */
    @AliasFor(annotation = HttpMapping.class)
    String baseUri() default "";

    /**
     * API URI
     */
    @AliasFor(annotation = HttpMapping.class)
    String uri();

    /**
     * 超时时间 秒
     */
    @AliasFor(annotation = HttpMapping.class)
    int timeout() default 60;

    /**
     * 是否传送额外头信息 如比如 access_token
     */
    @AliasFor(annotation = HttpMapping.class)
    boolean withExtra() default true;

    /**
     * 是否允许所有的SSL
     */
    @AliasFor(annotation = HttpMapping.class)
    boolean allowAllSsl() default false;

}
