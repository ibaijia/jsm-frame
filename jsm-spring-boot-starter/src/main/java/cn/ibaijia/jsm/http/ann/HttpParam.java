package cn.ibaijia.jsm.http.ann;

import java.lang.annotation.*;

/**
 * Url参数值
 *
 * @author longzl
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HttpParam {

    String value();

}
