package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.http.ann.HttpMapper;
import cn.ibaijia.jsm.http.ann.HttpMapping;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.apache.http.Header;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author longzl
 */
public class DefaultHttpMapperProcessorImpl extends AbstractHttpMapperProcessor {

    @Override
    public Object process(HttpMapper httpMapper, HttpMapping mapping, Method method, Object[] args) {
        boolean withExtra = mapping.withExtra() || httpMapper.withExtra();
        boolean allowAllSsl = mapping.allowAllSsl() || httpMapper.allowAllSsl();

        List<Header> headerList = getHeaderList(method, args);
        String url = formatUrl(mapping.uri(), mapping.baseUri(), httpMapper.baseUri(), method, args);
        Object requestData = getRequestData(method, args);
        HttpClientProcessor httpClientProcessor = findHttpClientProcessor(args);
        if (httpClientProcessor != null) {
            processByHttpClient(mapping, withExtra, allowAllSsl, headerList, url, requestData, httpClientProcessor);
            return null;
        } else {
            String res = getResultByHttpClient(mapping, withExtra, allowAllSsl, headerList, url, requestData);
            logger.debug("res:{}", res);
            Object resultObj = null;
            if (!StringUtil.isEmpty(res) && !method.getReturnType().equals(String.class)) {
                logger.debug("parse to:{}", method.getGenericReturnType());
                resultObj = JsonUtil.parseObject(res, method.getGenericReturnType());
            } else {
                resultObj = res;
            }
            return resultObj;
        }
    }

}
