package cn.ibaijia.jsm.http;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author longzl
 */
public interface HttpMapperProcessor {

    /**
     * process 处理逻辑
     * @param bean
     * @param <T>
     * @return
     */
    <T> Object process(FactoryBean bean);

}
