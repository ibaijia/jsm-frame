package cn.ibaijia.jsm.http;

import cn.ibaijia.jsm.context.WebContext;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * spring boot 和 spring cloud 的一些差异化配置
 *
 * @author longzl
 */
public class HttpClientStrategy {

    public static void setExtraHeaders(HttpRequestBase httpEntity) {
        WebContext.checkAndSetHeaders(httpEntity);
    }

}
