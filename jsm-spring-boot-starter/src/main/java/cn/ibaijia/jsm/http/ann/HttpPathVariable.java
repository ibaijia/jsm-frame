package cn.ibaijia.jsm.http.ann;

import java.lang.annotation.*;

/**
 * Url路径参数值，替换占位字符#{var}
 *
 * @author longzl
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HttpPathVariable {

    String value();

}
