package cn.ibaijia.jsm.http.ann;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author longzl
 */
@Target({java.lang.annotation.ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HttpMapper {

	/**
	 * API BASE URI
	 */
	String baseUri();
	/**
	 * 超时时间 秒
	 */
	int timeout() default 60;
	/**
	 * 是否传送额外头信息 如比如 access_token
	 */
	boolean withExtra() default true;
	/**
	 * useCookie true 一个代理只有一个 httpClient
	 */
	boolean useCookie() default false;
	/**
	 * 是否允许所有的SSL
	 */
	boolean allowAllSsl() default false;

}
