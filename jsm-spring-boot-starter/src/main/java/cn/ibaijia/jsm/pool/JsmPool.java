package cn.ibaijia.jsm.pool;

import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

import java.util.Queue;
import java.util.concurrent.*;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 14:31
 */
public class JsmPool<K,T extends PoolObject> {

    private Logger logger = LogUtil.log(this.getClass());

    private static int TIME_UINT = 10;
    private Queue<T> waitingList = new ConcurrentLinkedQueue<>();
    private Object getLock = new Object();
    private volatile int size;

    private PoolConfig poolConfig;
    private PoolObjectFactory<T> poolObjectFactory;

    public JsmPool(PoolConfig poolConfig, PoolObjectFactory<T> poolObjectFactory) {
        this.poolConfig = poolConfig;
        this.poolObjectFactory = poolObjectFactory;
        this.init();
    }

    private void init() {
        long currentTime = DateUtil.currentTime();
        for (int i = 0; i < poolConfig.getMinSize(); i++) {
            T t = poolObjectFactory.create(poolConfig.getConnProperties());
            t.setTimestamp(currentTime);
            waitingList.add(t);
        }
        this.size = poolConfig.getMinSize();
        ExecutorService executor = new ThreadPoolExecutor(1, 1, 0L,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), r -> new Thread(r,"jsm-pool-cleaner"));
        executor.execute(() -> {
            while (true) {
                synchronized (getLock) {
                    long currentTime1 = DateUtil.currentTime();
                    if (size > poolConfig.getMinSize() && waitingList.size() > poolConfig.getMinSize()) {
                        T t = waitingList.peek();
                        while (t != null && (currentTime1 - t.getTimestamp() > poolConfig.getMaxIdle())) {
                            waitingList.poll();
                            size--;
                            if (size <= poolConfig.getMinSize() || waitingList.size() <= poolConfig.getMinSize()) {
                                break;
                            }
                            t = waitingList.peek();
                        }
                    }
                }
                try {
                    status();
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public K exec(PoolExec<K,T> poolExec) {
        T t = null;
        try {
            t = get();
            return poolExec.exec(t);
        } catch (Exception e) {
            poolExec.throwable(e);
            return null;
        } finally {
            this.release(t);
        }
    }

    public T get() {
        return get(Long.valueOf(poolConfig.getMaxWait() / TIME_UINT).intValue());
    }

    public T get(int tryTime) {
        synchronized (getLock) {
            T poolObject = waitingList.poll();
            if (poolObject == null) {
                if (size < poolConfig.getMaxSize()) {
                    poolObject = poolObjectFactory.create(poolConfig.getConnProperties());
                    size++;
                    logger.debug("get from create");
                    return poolObject;
                } else {
                    if (tryTime > 0) {
                        try {
                            getLock.wait(TIME_UINT);
                            return get(tryTime - 1);
                        } catch (InterruptedException e) {
                            logger.error("get wait error.", e);
                            throw new RuntimeException("get wait error", e);
                        }
                    } else {
                        throw new RuntimeException("get time out");
                    }
                }
            } else {
                if (!poolObject.test()) {
                    logger.debug("get from pool test failed.");
                    return get();
                }
                logger.debug("get from pool");
                return poolObject;
            }
        }
    }

    public boolean release(T poolObject) {
        if (poolObject == null) {
            return false;
        }
        synchronized (getLock) {
            poolObject.setTimestamp(DateUtil.currentTime());
            if (this.waitingList.size() < poolConfig.getMaxSize()) {
                logger.debug("release add");
                waitingList.add(poolObject);
                return true;
            } else {
                logger.debug("release give up");
                this.size--;
            }
            return false;
        }
    }

    public void status() {
        logger.debug(String.format("size:%s queueSize:%s ", this.size, this.waitingList.size()));
    }

}
