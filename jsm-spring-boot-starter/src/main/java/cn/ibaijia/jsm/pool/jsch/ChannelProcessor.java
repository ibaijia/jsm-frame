package cn.ibaijia.jsm.pool.jsch;

import com.jcraft.jsch.Channel;

/**
 * @Author: LongZL
 * @Date: 2022/8/16 10:52
 */
public interface ChannelProcessor<T> {

    /**
     * 处理方法
     * @param channel
     * @return 处理返回的结果
     */
    T process(Channel channel);

}
