package cn.ibaijia.jsm.pool.jsch;

import cn.ibaijia.jsm.pool.PoolObjectFactory;
import cn.ibaijia.jsm.utils.StringUtil;

import java.util.Properties;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 16:52
 */
public class JschClientFactory implements PoolObjectFactory {

    @Override
    public JschClient create(Properties properties) {
        String ip = properties.getProperty("ip");
        Integer port = StringUtil.toInteger(properties.getProperty("port"), 22);
        String username = properties.getProperty("username");
        String password = properties.getProperty("password");
        String privateKey = properties.getProperty("privateKey");
        return new JschClient(ip, port, username, password, privateKey);
    }

}
