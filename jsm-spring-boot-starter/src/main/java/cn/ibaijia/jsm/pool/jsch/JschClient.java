package cn.ibaijia.jsm.pool.jsch;

import cn.ibaijia.jsm.exception.BaseException;
import cn.ibaijia.jsm.pool.AbstractPoolObject;
import cn.ibaijia.jsm.utils.StringUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 16:50
 */
public class JschClient extends AbstractPoolObject {

    private String ip;
    private Integer port;
    private String username;
    private String password;
    private String privateKey;

    private Session session;
    private JSch jsch;

    public JschClient(String ip, Integer port, String username, String password, String privateKey) {
        this.ip = ip;
        this.port = port;
        this.username = username;
        this.password = password;
        this.privateKey = privateKey;
        this.init();
    }

    @Override
    public boolean init() {
        try {
            jsch = new JSch();
            this.connect();
            return true;
        } catch (Exception e) {
            logger.error("JschClient init error.", e);
            return false;
        }
    }

    @Override
    public void connect() throws Exception {
        if (!StringUtil.isEmpty(privateKey)) {
            logger.debug("connect: use private key!");
            jsch.addIdentity(username, privateKey.getBytes(StandardCharsets.UTF_8), null, null);
        }
        session = jsch.getSession(username, ip, port);
        if (session == null) {
            throw new RuntimeException("session is null");
        }
        session.setConfig("StrictHostKeyChecking", "no");
        if (!StringUtil.isEmpty(password)) {
            logger.info("connect: use password!");
            session.setPassword(password);
        }
        session.connect();
    }

    @Override
    public void disconnect() {
        session.disconnect();
        session = null;
    }

    @Override
    public void reconnect() throws Exception {
        if (session == null) {
            connect();
        } else {
            disconnect();
            Thread.sleep(1000);
            connect();
        }
    }

    @Override
    public boolean isConnect() {
        return session != null && session.isConnected();
    }

    @Override
    public void setConnect(boolean connect) {
        // DO NOTHING
    }

    @Override
    public boolean test() {
        // DO NOTHING
        return true;
    }

    public String execCmd(String cmd, String charset) {
        logger.info("execCmd: {}", cmd);
        StringBuilder sb = new StringBuilder();
        ChannelExec channel = null;
        try {
            if (!session.isConnected()) {
                session.connect();
            }
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(cmd);
            channel.connect();

            InputStream is = channel.getInputStream();
            InputStream eis = channel.getErrStream();

            byte[] isBuf = new byte[1024];
            byte[] eisBuf = new byte[1024];
            while (true) {
                while (is.available() > 0) {
                    int i = is.read(isBuf, 0, 1024);
                    if (i < 0) {
                        break;
                    }
                    sb.append(new String(isBuf, 0, i, charset));
                }
                while (eis.available() > 0) {
                    int i = eis.read(eisBuf, 0, 1024);
                    if (i < 0) {
                        break;
                    }
                    sb.append(new String(eisBuf, 0, i, charset));
                }
                if (channel.isClosed()) {
                    if (is.available() > 0) {
                        continue;
                    }
                    logger.info("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(10);
                } catch (Exception ee) {
                }
            }

        } catch (Exception e) {
            String errorMsg = String.format("execCmd error:%s", cmd);
            logger.error(errorMsg, e);
            throw new BaseException(errorMsg);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
        String result = sb.toString();
        logger.info("exe cmd res: {}", result);
        return result;
    }

    /**
     * @param channelType session shell exec x11 auth-agent@openssh.com direct-tcpip forwarded-tcpip sftp subsystem
     * @param channelProcessor
     * @param <T>
     * @return
     */
    public <T> T openChannelAndProcess(String channelType, ChannelProcessor<T> channelProcessor) {
        Channel channel = null;
        try {
            channel = session.openChannel(channelType);
            if (channel == null) {
                logger.error("open channel null. type:{}", channelType);
                return null;
            }
            return channelProcessor.process(channel);
        } catch (Exception e) {
            logger.error("openChannelAndProcess error.", e);
            return null;
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

}
