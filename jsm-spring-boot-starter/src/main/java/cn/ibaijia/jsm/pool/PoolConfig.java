package cn.ibaijia.jsm.pool;

import java.util.Properties;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 14:35
 */
public class PoolConfig {

    /**
     * 最小Size,初始Size
     */
    private int minSize;

    /**
     * 最大Size
     */
    private int maxSize;

    /**
     * 最大空闲时间 毫秒，超过initSize的部分对象释放后 最大存活时间
     */
    private int maxIdle;

    /**
     * 获取对象最大等待时间 毫秒
     */
    private int maxWait;

    /**
     * 自定义连接属性
     */
    private Properties connProperties;

    public int getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(int maxWait) {
        this.maxWait = maxWait;
    }

    public Properties getConnProperties() {
        return connProperties;
    }

    public void setConnProperties(Properties connProperties) {
        this.connProperties = connProperties;
    }
}
