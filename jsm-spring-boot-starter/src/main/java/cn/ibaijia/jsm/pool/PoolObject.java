package cn.ibaijia.jsm.pool;

/**
 * @Author: LongZL
 * @Date: 2022/8/16 9:49
 */
public interface PoolObject {
    /**
     * 对象初始化
     *
     * @return 是否被动成功
     */
    boolean init();

    /**
     * 创建连接
     *
     * @throws Exception
     */
    void connect() throws Exception;

    /**
     * 断开连接
     */
    void disconnect();

    /**
     * 重新连接
     *
     * @throws Exception
     */
    void reconnect() throws Exception;

    /**
     * 是否已连接 连接池调用
     *
     * @return
     */
    boolean isConnect();

    /**
     * 设置是否连接 连接池调用
     *
     * @param connect
     */
    void setConnect(boolean connect);

    /**
     * 测试是否连接 连接池调用
     *
     * @return
     */
    boolean test();

    /**
     * 获取时间戳 判断生命周期
     * @return
     */
    long getTimestamp();

    /**
     * 设置时间戳
     * @param timestamp
     */
    void setTimestamp(long timestamp);
}
