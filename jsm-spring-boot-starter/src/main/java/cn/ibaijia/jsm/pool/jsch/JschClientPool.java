package cn.ibaijia.jsm.pool.jsch;

import cn.ibaijia.jsm.pool.JsmPool;
import cn.ibaijia.jsm.pool.PoolConfig;
import cn.ibaijia.jsm.pool.PoolExec;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 16:58
 */
public class JschClientPool {

    private JsmPool<String,JschClient> pool;

    public JschClientPool(PoolConfig poolConfig) {
        this.pool = new JsmPool<>(poolConfig, new JschClientFactory());
    }

    public String exec(PoolExec<String,JschClient> poolExec) {
        return pool.exec(poolExec);
    }
}
