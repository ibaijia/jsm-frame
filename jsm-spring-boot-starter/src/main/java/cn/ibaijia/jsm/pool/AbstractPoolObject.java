package cn.ibaijia.jsm.pool;

import cn.ibaijia.jsm.utils.LogUtil;
import org.slf4j.Logger;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 14:25
 */
public abstract class AbstractPoolObject implements PoolObject {

    protected Logger logger = LogUtil.log(this.getClass());

    private long timestamp;

    /**
     * 对象初始化
     *
     * @return 是否被动成功
     */
    @Override
    public boolean init() {
        try {
            this.connect();
            return true;
        } catch (Exception e) {
            logger.error("PoolObject init error.", e);
            return false;
        }
    }

    /**
     * 重新连接
     *
     * @throws Exception
     */
    @Override
    public void reconnect() throws Exception {
        disconnect();
        Thread.sleep(1000);
        connect();
    }

    /**
     * 设置是否连接 连接池调用
     *
     * @param connect
     */
    @Override
    public void setConnect(boolean connect) {
    }

    /**
     * 测试是否连接 连接池调用
     *
     * @return
     */
    @Override
    public boolean test() {
        return true;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
