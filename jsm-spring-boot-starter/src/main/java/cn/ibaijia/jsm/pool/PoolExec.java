package cn.ibaijia.jsm.pool;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 16:06
 */
public interface PoolExec<K,T extends PoolObject> {

    /**
     * 执行 体
     * @param poolObject
     * @return 执行结果
     */
    K exec(T poolObject);

    /**
     * 执行 抛出的异常
     * @param throwable
     */
    void throwable(Throwable throwable);

}
