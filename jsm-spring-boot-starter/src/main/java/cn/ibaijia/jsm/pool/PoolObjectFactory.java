package cn.ibaijia.jsm.pool;

import java.util.Properties;

/**
 * @Author: LongZL
 * @Date: 2022/8/15 15:34
 */
public interface PoolObjectFactory<T extends PoolObject> {



    /**
     * 创建 对象
     * @param properties
     * @return T
     */
   T create(Properties properties);

}
