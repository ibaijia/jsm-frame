package cn.ibaijia.jsm.mybatis;
/**
 * @author longzl
 */
public class OracleDialect extends AbstractDialect {

	@Override
	public String concatPageSql(String sql, int startRow, int pageSize) {
		StringBuilder sqlBuilder = new StringBuilder(sql.length() + 120);
        sqlBuilder.append("SELECT * FROM ( ");
        sqlBuilder.append(" SELECT TMP_PAGE.*, ROWNUM ROW_ID FROM ( ");
        sqlBuilder.append(sql);
        sqlBuilder.append(" ) TMP_PAGE WHERE ROWNUM <= ");
        sqlBuilder.append(startRow);
        sqlBuilder.append(" ) WHERE ROW_ID > ");
        sqlBuilder.append(startRow + pageSize);
        return sqlBuilder.toString();
	}

}
