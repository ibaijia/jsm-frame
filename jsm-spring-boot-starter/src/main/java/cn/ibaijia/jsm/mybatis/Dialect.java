package cn.ibaijia.jsm.mybatis;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import java.util.Properties;

/**
 * @author longzl
 */
public interface Dialect {

	/**
	 * 设置属性
	 * @param properties
	 */
	void setProperties(Properties properties);

	/**
	 * createNewMappedStatement
	 * @param ms
	 * @param newSqlSource
	 * @return
	 */
	MappedStatement createNewMappedStatement(MappedStatement ms,SqlSource newSqlSource);

	/**
	 * createNewMappedStatement
	 * @param ms
	 * @param boundSql
	 * @param sql
	 * @return
	 */
	MappedStatement createNewMappedStatement(MappedStatement ms, BoundSql boundSql, String sql);

	/**
	 * createNewBoundSql
	 * @param ms
	 * @param boundSql
	 * @param sql
	 * @return
	 */
	BoundSql createNewBoundSql(MappedStatement ms, BoundSql boundSql, String sql);

	/**
	 * concatCountSql
	 * @param sql
	 * @return
	 */
	String concatCountSql(String sql);

	/**
	 * concatPageSql
	 * @param sql
	 * @param startRow
	 * @param pageSize
	 * @return
	 */
	String concatPageSql(String sql,int startRow,int pageSize);

	/**
	 * getSqlSource
	 * @param boundSql
	 * @return
	 */
	SqlSource getSqlSource(BoundSql boundSql);

	/**
	 * getSqlBody
	 * @param sql
	 * @return
	 */
	String getSqlBody(String sql);
	
}
