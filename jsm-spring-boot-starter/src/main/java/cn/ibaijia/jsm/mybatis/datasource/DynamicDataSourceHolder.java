package cn.ibaijia.jsm.mybatis.datasource;

import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.ThreadLocalUtil;
import org.slf4j.Logger;

/**
 * @author longzl
 */
public class DynamicDataSourceHolder {
    private static Logger logger = LogUtil.log(DynamicDataSourceHolder.class);

    public static void setDataSource(String name) {
        ThreadLocalUtil.DATA_SOURCE_TL.set(name);
    }

    public static String getDataSource() {
        String key = ThreadLocalUtil.DATA_SOURCE_TL.get();
        if (StringUtil.isEmpty(key)) {
            key = "master";
        }
        logger.debug("get data source:{}", key);
        return key;
    }

    public static void setDataSource(boolean readOnly) {
        String key = readOnly ? "slave" : "master";
        logger.debug("set data source:{}", key);
        setDataSource(key);
    }
}
