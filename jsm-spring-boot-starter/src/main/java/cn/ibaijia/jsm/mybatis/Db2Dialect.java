package cn.ibaijia.jsm.mybatis;

/**
 * @author longzl
 */
public class Db2Dialect extends AbstractDialect {

	@Override
	public String concatPageSql(String sql, int startRow, int pageSize) {
		StringBuilder sb = new StringBuilder(sql.length() + 140);
		sb.append("SELECT * FROM (SELECT TMP_PAGE.*,ROWNUMBER() OVER() AS ROW_ID FROM ( ");
		sb.append(sql);
		sb.append(" ) AS TMP_PAGE) TMP_PAGE WHERE ROW_ID BETWEEN ");
		sb.append(startRow);
		sb.append(" AND ");
		sb.append(startRow + pageSize);
		return sb.toString();
	}

}
