package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * API 熔断功能 api如错时回调处理
 *
 * @author longzl
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RestFuseAnn {

    /**
     * 错误 错误次数
     */
    int value() default 3;

    /**
     * 熔断 统计时长
     */
    int duration() default 60;

    /**
     * 熔断 时长
     */
    int fuseDuration() default 120;

    /**
     * 熔断（错误）时处理方法
     */
    String fallbackMethod();

    /**
     * 熔断 自定义Key
     */
    String key() default "";

}
