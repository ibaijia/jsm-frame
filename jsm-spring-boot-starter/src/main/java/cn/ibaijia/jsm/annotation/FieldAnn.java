package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * @author longzl
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldAnn {

    boolean required() default true;

    /**
     * 是否需要 URL Decode
     *
     * @return
     */
    boolean urlDecode() default false;

    /**
     * 框架内置的几种 单个字段 类型，见FieldType
     *
     * @return
     */
    FieldType type() default FieldType.ALL;

    /**
     * 适用于 单个字段规则验证,如个性化的密码规则
     *
     * @return 不匹配regex返回 message
     */
    String regex() default "";

    /**
     * regex 或者 el不匹配时,返回的错误信息
     */
    String message() default "";

    /**
     * 内置对象 vm(本身) session(JsmSession) request(HttpServletRequest)　#{elvar}
     * return 不匹配EL返回 message,效果同validateMethod,适用于请求对象内部多个字段的逻辑验证,如a字段为true时,b字段一定不能为空
     */
    String el() default "";

    /**
     * 需要实现ValidateCallback接口的Spring BeanName或者BeanType(包名+类名),适用于查询DB的校验,如用户名验重
     */
    String validateBean() default "";

    /**
     * 指定 ValidateModel中的验证方法 默认 fieldNameValidate,适用于请求对象内部多个字段的逻辑验证,如a字段为true时,b字段一定不能为空
     */
    String validateMethod() default "default";

    int minLen() default -1;

    int maxLen() default -1;

    EscapeType escape() default EscapeType.ALL;

    boolean escapeSensitiveWord() default false;

    /**
     * 字段说明
     */
    String comments() default "";

    /**
     * 一般是 range 如:1-7,+1,2,123.3(表示123后面3位数)，具体参考: http://mockjs.com/examples.html
     */
    String mockRule() default "";

    /**
     * 任意值
     */
    String mockValue() default "";

}
