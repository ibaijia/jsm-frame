package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * @author longzl / @createOn 2019-12-30
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OrderAnn {

    /**
     * 非公平排队方案（快但不公平）公平排队方案（稍慢但公平）
     */
    abstract LineType type() default LineType.UNFAIR;

    /**
     * 保证每个商品唯一  默认 className+methodName
     */
    abstract String productKey();

    /**
     * 每个用户唯一
     */
    abstract String userKey();

    /**
     * 持续时长
     */
    abstract long duration() default 10 * 60;

}
