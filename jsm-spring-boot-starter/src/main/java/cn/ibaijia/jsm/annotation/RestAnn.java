package cn.ibaijia.jsm.annotation;

import org.springframework.transaction.TransactionDefinition;

import java.lang.annotation.*;

/**
 * @author longzl use in controller
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RestAnn {

    /**
     * 是登录客户端类型，默认为APP 移动端
     */
    AuthType authType() default AuthType.NONE;

    /**
     * 权限，默认值为"" PermissionCode or ResourceCode
     */
    String permission() default "";

    /**
     * 日志组
     */
    String logType() default "";

    /**
     * 日志支持变量
     */
    String log() default "";

    /**
     * 是否需要验证
     */
    boolean validate() default true;

    /**
     * synchronized,默认并行请求
     */
    boolean sync() default false;

    /**
     * 集群环境下的分布式同步锁,默认不开
     */
    String clusterSyncLock() default "";

    /**
     * 事务属性，默认为不在controller层开事务
     */
    Transaction transaction() default Transaction.NONE;

    /**
     * 事务超时时间
     */
    int transTimeout() default 10;

    /**
     * 传播行为
     */
    int transPropagationBehavior() default TransactionDefinition.PROPAGATION_REQUIRED;

    /*** 隔离级别*/
    int transIsolationLevel() default TransactionDefinition.ISOLATION_DEFAULT;

    /**
     * if Transaction.READ try read transactionManagerRead
     * 自定事务管理器
     */
    String transManagerName() default "transactionManager";

    /**
     * 是否启用IP检查 默认false AppContext.get(key),通常用于 三方调用IP检查
     */
    String ipCheckKey() default "";

    /**
     * 是否内网API 默认false
     */
    boolean internal() default false;

    /**
     * 请求ID，防止重复请求 只对POST PUT方法有效默认开起 默认requestBody md5
     */
    boolean repeatCheck() default true;

    /**
     * 用于个别 API 设置，比如上传的API 本来就慢，可以根据业务单独设置一个合理的值
     * 自定义慢API预警值，默认使用系统配置的 jsm.slow.apiLimit
     */
    int apiLimit() default -1;

}
