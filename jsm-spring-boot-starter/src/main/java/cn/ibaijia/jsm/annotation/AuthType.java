package cn.ibaijia.jsm.annotation;
/**
 * @author longzl
 */
public enum AuthType {
	/**
	 *
	 */
	NONE(0,"none"),
	/**
	 * oauth_code
	 */
	OAUTH_CODE(1,"codeAuth"),
	/**
	 *oauth_implicit
	 */
	OAUTH_IMPLICIT(2,"implicitAuth"),
	/**
	 *oauth_password
	 */
	OAUTH_PASSWORD(3,"passwordAuth"),
	/**
	 * oauth_client
	 */
	OAUTH_CLIENT(4,"clientAuth"),
	/**
	 * blowfish
	 */
	JSM_APP(5,"jsmAppAuth"),
	/**
	 * base64
	 */
	JSM_WEB(6,"jsmWebAuth"),
	/**
	 * JSM_APP中的 md5(at+time)+bf(at+time)=token
	 */
	JSM_TOKEN(7,"jsmTokenAuth"),
	/**
	 * 运行在GATEWAY 后面，直接绑定jsm-gw-{key}的数据到Oauth对象
	 */
	JSM_GATEWAY(8,"jsmGatewayAuth"),
	/**
	 * 直接解析token拿到uid 拿用户信息 配置userService 拿permissions配合permission
	 */
	JSM_DIRECT(9,"jsmDirectAuth");


	private int v;
	private String t;
	AuthType(int value,String t){
		this.v = value;
		this.t = t;
	}
	public int v(){
		return v;
	}
	public String t(){
		return t;
	}
	public static String findText(int v){
		AuthType[] values = AuthType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].v == v){
				return values[i].t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		AuthType[] values = AuthType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].t.equals(t)){
				return values[i].v;
			}
		}
		return 0;
	}
}
