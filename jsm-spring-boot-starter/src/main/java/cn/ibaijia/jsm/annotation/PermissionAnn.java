package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * @author longzl
 */
@Target( { java.lang.annotation.ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PermissionAnn {
	
	String description() default "";
	
	String groupName() default "";
	
}
