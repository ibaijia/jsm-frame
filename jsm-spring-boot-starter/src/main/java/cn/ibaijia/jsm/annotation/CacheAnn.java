package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * @author longzl / @createOn 2011-8-31
 * cache the method's return value
 */
@Target({ java.lang.annotation.ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheAnn {
	/**
	 * 缓存方式
	 */
	CacheType cacheType() default CacheType.L1L2;
	
	/**
	 * 过期时间秒
	 * only support CacheType.L2 and BOTH
	 */
	int expireSeconds() default 300;

	/**
	 * 自定义cacheKey 实现可控 过期
	 */
	String cacheKey() default "";

	/**
	 * 设置缓存空值 可以有效防止缓存击穿 避免攻击
	 */
	boolean cacheNull() default true;
	
	
}
