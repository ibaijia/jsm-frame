package cn.ibaijia.jsm.annotation;
/**
 * @author longzl
 */
public enum EscapeType {
	/**
	 *
	 */
	NONE(0,"none"),
	/**
	 *
	 */
	HTML(1,"html"),
	/**
	 *
	 */
	JS(2,"js"),
	/**
	 *
	 */
	CSS(3,"css"),
	/**
	 *
	 */
	ALL(4,"all");
	private int v;
	private String t;
	EscapeType(int value,String t){
		this.v = value;
		this.t = t;
	}
	public int v(){
		return v;
	}
	public static String findText(int v){
		EscapeType[] values = EscapeType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].v == v){
				return values[i].t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		EscapeType[] values = EscapeType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].t.equals(t)){
				return values[i].v;
			}
		}
		return 0;
	}
}
