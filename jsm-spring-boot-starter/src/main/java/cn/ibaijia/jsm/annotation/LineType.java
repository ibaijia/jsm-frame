package cn.ibaijia.jsm.annotation;
/**
 * @author longzl
 */
public enum LineType {
    /**
     *
     */
    UNFAIR(0, "unfairLineService"),
    /**
     *
     */
    FAIR(1, "fairLineService"),
    /**
     *
     */
    CUSTOM(2,"customLineService");
    private int v;
    private String t;

    LineType(int value, String t) {
        this.v = value;
        this.t = t;
    }

    public int v() {
        return v;
    }

    public String t() {
        return t;
    }

    public static String findText(int v) {
        LineType[] values = LineType.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].v == v) {
                return values[i].t;
            }
        }
        return null;
    }

    public static int findInt(String t) {
        LineType[] values = LineType.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].t.equals(t)) {
                return values[i].v;
            }
        }
        return 0;
    }
}
