package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * 用于解决集群中的job重复问题
 * @author longzl
 */
@Target( { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ClusterJobLockAnn {

    /**
     * 默认 className+methodName
     * */
    String value() default "";

}
