package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * API 限流功能 限制同时能处理多少请求
 *
 * @author longzl
 */
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RestLimitAnn {

    /**
     * 同时最大请求数
     */
    int value() default 100;

    /**
     * 限流后处理方法
     */
    String fallbackMethod();

    /**
     * 熔断 自定义Key
     */
    String key() default "";

}
