package cn.ibaijia.jsm.annotation;
/**
 * @author longzl
 */
public enum CacheType {
	/**
	 * caffeine
	 */
	L1(0,"L1"),
	/**
	 * redis
	 */
	L2(1,"L2"),
	/**
	 * caffeine+redis
	 */
	L1L2(2,"L1L2");
	private int v;
	private String t;
	CacheType(int value,String t){
		this.v = value;
		this.t = t;
	}
	public int v(){
		return v;
	}
	public static String findText(int v){
		CacheType[] values = CacheType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].v == v){
				return values[i].t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		CacheType[] values = CacheType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].t.equals(t)){
				return values[i].v;
			}
		}
		return 0;
	}
}
