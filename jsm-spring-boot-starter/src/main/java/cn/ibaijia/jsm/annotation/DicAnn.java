package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * @Author: LongZL
 * @Date: 2022/4/8 16:05
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DicAnn {

    /**
     * 字典名
     */
    String name();

    /**
     * 通常用于 新起字段为解析 指定字典字段
     * 解析为字典的字段名
     */
    String fieldName() default "";

    /**
     * 缓存方式
     */
    CacheType cacheType() default CacheType.L1L2;

    /**
     * 是否转化为字典对象 默认false,如果有需要反序列化的情况，需要设置为true
     */
    boolean toDicKV() default false;

    /**
     * Dic对象的Key字段名称
     */
    String dicKeyName() default "id";

    /**
     * Dic对象的Value字段名称
     */
    String dicValueName() default "name";
    /**
     * Dic对象的Key字段名称说明 字段描述，无实际意义
     */
    String dicKeyNameComment() default "";

    /**
     * Dic对象的Value字段名称说明 字段描述，无实际意义
     */
    String dicValueNameComment() default "";

}
