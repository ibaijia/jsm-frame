package cn.ibaijia.jsm.annotation;
/**
 * @author longzl
 */
public enum Transaction {
	/**
	 *
	 */
	NONE(0,"无事务"),
	/**
	 *
	 */
	READ(1,"读事务"),
	/**
	 *
	 */
	WRITE(2,"写事务");
	private int v;
	private String t;
	Transaction(int value,String t){
		this.v = value;
		this.t = t;
	}
	public int v(){
		return v;
	}
	public static String findText(int v){
		Transaction[] values = Transaction.values();
		for (int i=0;i<values.length;i++){
			if (values[i].v == v){
				return values[i].t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		Transaction[] values = Transaction.values();
		for (int i=0;i<values.length;i++){
			if (values[i].t.equals(t)){
				return values[i].v;
			}
		}
		return 0;
	}
}
