package cn.ibaijia.jsm.annotation;

import java.lang.annotation.*;

/**
 * @author longzl
 */
@Target( { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ClusterLockAnn {

    /** 默认 className+methodName */
    String value() default "";

    /** 默认获取锁超时时间，默认30s */
    int timeout() default 30;

}
