package cn.ibaijia.jsm.disruptor;

/**
 * @author longzl
 */
public interface Processor<T> {
    /**
     * 处理
     * @param event
     * @param sequence
     * @param endOfBatch
     */
    void process(JsmDisruptor.MessageEvent event, long sequence, boolean endOfBatch);
}
