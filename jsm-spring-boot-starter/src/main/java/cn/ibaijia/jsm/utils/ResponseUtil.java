package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.rest.resp.vo.*;
import cn.ibaijia.jsm.exception.FailedException;
import org.slf4j.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @author longzl
 */
public class ResponseUtil {
    private static Logger logger = LogUtil.log(ResponseUtil.class);

    public static void outputExcel(HttpServletResponse response, ExcelVo excelVo) {
        logger.debug("excelVo:{}", excelVo.fileName);
        response.setHeader("Content-Disposition", "attachment; filename=" + EncryptUtil.urlEncode(excelVo.fileName));
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        if (excelVo.workbook != null) {
            try {
                excelVo.workbook.write(response.getOutputStream());
            } catch (IOException e) {
                logger.error("write workbook error! {}", excelVo.fileName);
            } finally {
                try {
                    excelVo.workbook.close();
                } catch (IOException e) {
                    logger.error("close workbook error! {}", excelVo.fileName);
                }
            }
        }
    }

    public static void outputXml(HttpServletResponse response, XmlVo xmlVo) {
        logger.debug("xmlVo:{}", xmlVo.content);
        if(xmlVo.content == null){
            throw new FailedException("xmlVo content is null.");
        }
        PrintWriter pw = null;
        try {
            response.setContentType("application/xml;charset=UTF-8");
            pw = response.getWriter();
            pw.write(xmlVo.content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputXml error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputHtml(HttpServletResponse response, HtmlVo htmlVo) {
        logger.debug("htmlVo:{}", htmlVo.content);
        if(htmlVo.content == null){
            throw new FailedException("htmlVo content is null.");
        }
        PrintWriter pw = null;
        try {
            response.setContentType("text/html;charset=UTF-8");
            pw = response.getWriter();
            pw.write(htmlVo.content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("htmlResp error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputFreeResp(HttpServletResponse response, FreeVo freeVo) {
        logger.debug("freeVo: type:{} content:{}", freeVo.contentType, freeVo.content);
        if(freeVo.content == null){
            throw new FailedException("freeVo content is null.");
        }
        PrintWriter pw = null;
        try {
            response.setContentType(freeVo.contentType);
            pw = response.getWriter();
            pw.write(freeVo.content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("freeResp error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputText(HttpServletResponse response, TextVo textVo) {
        logger.debug("textVo:{}", textVo.content);
        if(textVo.content == null){
            throw new FailedException("textVo content is null.");
        }
        PrintWriter pw = null;
        try {
            response.setContentType("text/plain;charset=UTF-8");
            pw = response.getWriter();
            pw.write(textVo.content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputText error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputJson(HttpServletResponse response, JsonVo jsonVo) {
        logger.debug("jsonVo:{}", jsonVo.content);
        if(jsonVo.content == null){
            throw new FailedException("jsonVo content is null.");
        }
        PrintWriter pw = null;
        try {
            response.setContentType("application/json;charset=UTF-8");
            pw = response.getWriter();
            pw.write(jsonVo.content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputJson error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void outputFile(HttpServletResponse response, FileVo fileVo) {
        logger.debug("fileVo:{}", fileVo.fileName);
        if(fileVo.content == null){
            throw new FailedException("fileVo content is null.");
        }
        String contentDisposition = "attachment; filename=" + EncryptUtil.urlEncode(fileVo.fileName);
        if (!StringUtil.isEmpty(WebContext.getRequest().getParameter(BaseConstants.CONTENT_DISPLAY_INLINE))) {
            contentDisposition = "inline";
        }
        response.setHeader("Content-Disposition", contentDisposition);
        response.setCharacterEncoding("UTF-8");
        if (fileVo.headers != null) {
            fileVo.headers.stream().forEach(header -> {
                response.setHeader(header.getName(), header.getValue());
            });
        } else {
            String suffix = fileVo.fileName.substring(fileVo.fileName.lastIndexOf("."));
            String contentType = MimeTypeUtil.getContentType(suffix);
            if (contentType != null) {
                response.setContentType(contentType);
            }
        }
        ServletOutputStream sos = null;
        InputStream is = null;
        PrintWriter pw = null;
        try {
            if (fileVo.content instanceof String) {
                pw = response.getWriter();
                pw.print((String) fileVo.content);
            } else if (fileVo.content instanceof File) {
                sos = response.getOutputStream();
                int offset = 0;
                byte[] bt = new byte[1024];
                is = new FileInputStream((File) fileVo.content);
                while ((offset = is.read(bt)) != -1) {
                    sos.write(bt, 0, offset);
                }
            } else if (fileVo.content instanceof InputStream) {
                sos = response.getOutputStream();
                int offset = 0;
                byte[] bt = new byte[1024];
                is = (InputStream) fileVo.content;
                while ((offset = is.read(bt)) != -1) {
                    sos.write(bt, 0, offset);
                }
            } else {//Other Object
                pw = response.getWriter();
                pw.print(JsonUtil.toJsonString(fileVo.content));
            }
        } catch (Exception e) {
            logger.error("outputFile error!" + fileVo.fileName, e);
        } finally {
            try {
                if (sos != null) {
                    sos.flush();
                    sos.close();
                }
                if (is != null) {
                    is.close();
                }
                if (pw != null) {
                    pw.close();
                }
            } catch (IOException e) {
                logger.error("outputFile error!" + fileVo.fileName, e);
            }
        }
    }

    public static void outputRestResp(HttpServletResponse response, RestResp restResp) {
        logger.info("restResp:{}", restResp);
        PrintWriter pw = null;
        try {
            response.setContentType("application/json;charset=UTF-8");
            pw = response.getWriter();
            pw.write(JsonUtil.toJsonString(restResp));
            pw.flush();
            pw.close();
        } catch (IOException e) {
            logger.error("outputRestResp error!", e);
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }
}
