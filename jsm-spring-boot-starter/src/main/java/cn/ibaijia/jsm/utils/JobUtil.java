package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.job.JsmJob;
import org.slf4j.Logger;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.config.Task;

import java.util.Map;

/**
 * @author longzl
 */
public class JobUtil {

    private static Logger logger = LogUtil.log(JobUtil.class);

    private static ScheduledTaskRegistrar scheduledTaskRegistrar;

    public static void setScheduledTaskRegistrar(ScheduledTaskRegistrar str) {
        scheduledTaskRegistrar = str;
    }

    public static boolean freshJsmJob(String name){
        if(scheduledTaskRegistrar == null){
            return false;
        }
        Map<String, JsmJob> jsmJobMap = SpringContext.getBeansOfType(JsmJob.class);
        scheduledTaskRegistrar.getTriggerTaskList().forEach(v->{

        });
        return false;
    }

}
