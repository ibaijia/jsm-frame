package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;

import java.util.*;

/**
 * @author longzl
 */
public class SensitiveWordUtil {
    private static Logger logger = LogUtil.log(SensitiveWordUtil.class);
    private static Map<Character, Word> sensitiveWordMap = new HashMap<>();
    public static final int MATCH_TYPE_MIN = 1;
    public static final int MATCH_TYPE_MAX = 2;

    private static void init() {
        if (sensitiveWordMap.isEmpty()) {
            String[] wordsArr = loadSensitiveDic();
            if (wordsArr != null && wordsArr.length > 0) {
                createSensitiveWordMap(wordsArr);
            }
        }
    }

    private static String[] loadSensitiveDic() {
        String filename = "sensitive-words";
        try {
            String txt = FileUtil.readFileAsText(SensitiveWordUtil.class.getClassLoader().getResourceAsStream(filename));
            return txt.split(";");
        } catch (Exception e) {
            logger.error("loadSensitiveDic error.", e);
            return null;
        }
    }

    /**
     * 创建DFA 结构
     * DFA即Deterministic Finite Automaton，也就是确定有穷自动机
     *
     * @param words
     */
    private static void createSensitiveWordMap(String[] words) {
        for (String word : words) {
            Map<Character, Word> parentMap = sensitiveWordMap;
            for (int i = 0; i < word.length(); i++) {
                char keyChar = word.charAt(i);
                Word childWord = parentMap.get(keyChar);
                if (childWord == null) {
                    childWord = new Word(false);
                    parentMap.put(keyChar, childWord);
                }
                parentMap = childWord.next;
                if (i == word.length() - 1) {
                    childWord.end = true;
                }
            }
        }
    }

    private static String createReplaceString(char replaceChar, int length) {
        char[] resultReplace = new char[length];
        for (int i = 0; i < length; i++) {
            resultReplace[i] = replaceChar;
        }
        return String.valueOf(resultReplace);
    }

    public static String escapeSensitiveWord(String txt) {
        return escapeSensitiveWord(txt, MATCH_TYPE_MAX, '*');
    }

    /**
     * 查找并替换敏感词
     *
     * @param txt
     * @param matchType
     * @param replaceChar
     * @return
     */
    public static String escapeSensitiveWord(String txt, int matchType, char replaceChar) {
        init();
        String resultTxt = txt;
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String word = iterator.next();
            String replaceString = createReplaceString(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }
        return resultTxt;
    }

    /**
     * 从当前位置检查 敏感词长度 返回length大于0 则表示存在敏感词
     *
     * @param txt
     * @param beginIndex
     * @param matchType
     * @return
     */
    private static int checkSensitiveWordLength(String txt, int beginIndex, int matchType) {
        boolean matchEndWords = false;
        int length = 0;
        Map<Character, Word> currentMap = sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++) {
            char word = txt.charAt(i);
            Word currentWord = currentMap.get(word);
            if (currentWord != null) {
                length++;
                currentMap = currentWord.next;
                if (currentWord.end) {
                    matchEndWords = true;
                    if (MATCH_TYPE_MIN == matchType) {
                        break;
                    }
                }
            } else {
                break;
            }
        }
        if (!matchEndWords) {
            length = 0;
        }
        return length;
    }

    /**
     * 获取所有 敏感词
     *
     * @param txt
     * @param matchType
     * @return
     */
    public static Set<String> getSensitiveWord(String txt, int matchType) {
        init();
        Set<String> sensitiveWordList = new HashSet<>();
        for (int i = 0; i < txt.length(); i++) {
            int length = checkSensitiveWordLength(txt, i, matchType);
            if (length > 0) {
                sensitiveWordList.add(txt.substring(i, i + length));
                i = i + length - 1;
            }
        }
        return sensitiveWordList;
    }

}

class Word {
    boolean end;
    Map<Character, Word> next;

    public Word() {

    }

    public Word(boolean end) {
        this.end = end;
        this.next = new HashMap<>();
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }
}
