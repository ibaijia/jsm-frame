package cn.ibaijia.jsm.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author longzl
 */
public class HtmlUtil {
    private static Logger logger = LogUtil.log(HtmlUtil.class);

    private static String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>";
    private static String regEx_css = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>";
    private static String regEx_html = "<[^>]+>";
    private static String regEx_el = "[${]";

    /**
     * js html css filter
     */
    public static String filter(String str) {
        if (str == null) {
            return "";
        }
        str = jsFilter(str);
        str = cssFilter(str);
        str = htmlFilter(str);
        str = elFilter(str);
        return str;
    }

    /**
     * el filter
     */
    public static String elFilter(String str) {
        if (str == null) {
            return "";
        }
        Pattern pattern = Pattern.compile(regEx_el, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        str = matcher.replaceAll("");
        return str;
    }

    /**
     * js filter
     */
    public static String jsFilter(String str) {
        if (str == null) {
            return "";
        }
        Pattern pattern = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        str = matcher.replaceAll("");
        return str;
    }

    /**
     * html filter
     */
    public static String htmlFilter(String str) {
        if (str == null) {
            return "";
        }
        Pattern pattern = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        str = matcher.replaceAll("");
        return str;
    }

    /**
     * css filter
     */
    public static String cssFilter(String str) {
        if (str == null) {
            return "";
        }
        Pattern pattern = Pattern.compile(regEx_css, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        str = matcher.replaceAll("");
        return str;
    }


    public static Document getDocument(String url) {
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            logger.error("getDocument error url:" + url, e);
        }
        return document;
    }

    public static Document parse(String html) {
        Document document = null;
        try {
            document = Jsoup.parse(html);
        } catch (Exception e) {
            logger.error("parse error html:" + html, e);
        }
        return document;
    }
}
