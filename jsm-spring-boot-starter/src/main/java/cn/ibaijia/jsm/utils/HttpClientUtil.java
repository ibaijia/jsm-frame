package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.http.HttpClient;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;

import java.util.List;

/**
 * @author longzl
 */
public class HttpClientUtil {

    private static HttpClient httpClient = new HttpClient();

    public static void setHttpProxy(HttpHost proxyHost) {
        httpClient.setHttpProxy(proxyHost);
    }

    public static void setSocksProxy(String ip, int port) {
        httpClient.setSocksProxy(ip, port);
    }

    public static void setWithExtra(boolean withExtra) {
        httpClient.setWithExtra(withExtra);
    }

    public static <T> T get(String url, Class<T> clazz) {
        return httpClient.get(url, clazz);
    }

    public static <T> T get(List<Header> headers, String url, Class<T> clazz) {
        return httpClient.get(headers, url, clazz);
    }

    public static <T> T get(String url, TypeReference<T> typeReference) {
        return httpClient.get(url, typeReference);
    }

    public static <T> T get(List<Header> headers, String url, TypeReference<T> typeReference) {
        return httpClient.get(headers, url, typeReference);
    }

    public static String get(String url) {
        return httpClient.get(url);
    }

    public static String get(List<Header> headers, String url) {
        return httpClient.get(headers, url);
    }


    public static <T> T post(String url, List<NameValuePair> nvps, Class<T> clazz) {
        return httpClient.post(url, nvps, clazz);
    }

    public static <T> T post(List<Header> headers, String url, List<NameValuePair> nvps, Class<T> clazz) {
        return httpClient.post(headers, url, nvps, clazz);
    }

    public static <T> T post(String url, List<NameValuePair> nvps, TypeReference<T> typeReference) {
        return httpClient.post(url, nvps, typeReference);
    }

    public static <T> T post(List<Header> headers, String url, List<NameValuePair> nvps, TypeReference<T> typeReference) {
        return httpClient.post(headers, url, nvps, typeReference);
    }

    public static String post(String url, List<NameValuePair> nvps) {
        return httpClient.post(url, nvps);
    }

    public static String post(List<Header> headers, String url, List<NameValuePair> nvps) {
        return httpClient.post(headers, url, nvps);
    }


    public static <T> T post(String url, Object data, Class<T> clazz) {
        return httpClient.post(url, data, clazz);
    }

    public static <T> T post(List<Header> headers, String url, Object data, Class<T> clazz) {
        return httpClient.post(headers, url, data, clazz);
    }

    public static <T> T post(String url, Object data, TypeReference<T> typeReference) {
        return httpClient.post(url, data, typeReference);
    }

    public static <T> T post(List<Header> headers, String url, Object data, TypeReference<T> typeReference) {
        return httpClient.post(headers, url, data, typeReference);
    }

    public static String post(String url, Object data) {
        return httpClient.post(url, data);
    }

    public static String post(String url, String strEntity) {
        return httpClient.post(url, strEntity);
    }

    public static String post(List<Header> headers, String url, Object data) {
        return httpClient.post(headers, url, data);
    }

    public static String post(List<Header> headers, String url, String strEntity) {
        return httpClient.post(headers, url, strEntity);
    }

    public static <T> T put(String url, Object data, Class<T> clazz) {
        return httpClient.put(url, data, clazz);
    }

    public static <T> T put(List<Header> headers, String url, Object data, Class<T> clazz) {
        return httpClient.put(headers, url, data, clazz);
    }

    public static <T> T put(String url, Object data, TypeReference<T> typeReference) {
        return httpClient.put(url, data, typeReference);
    }

    public static <T> T put(List<Header> headers, String url, Object data, TypeReference<T> typeReference) {
        return httpClient.put(headers, url, data, typeReference);
    }

    public static String put(String url, Object data) {
        return httpClient.put(url, data);
    }

    public static String put(List<Header> headers, String url, String strEntity) {
        return httpClient.put(headers, url, strEntity);
    }

    public static <T> T delete(String url, Class<T> clazz) {
        return httpClient.delete(url, clazz);
    }

    public static <T> T delete(List<Header> headers, String url, Class<T> clazz) {
        return httpClient.delete(headers, url, clazz);
    }

    public static <T> T delete(String url, TypeReference<T> typeReference) {
        return httpClient.delete(url, typeReference);
    }

    public static <T> T delete(List<Header> headers, String url, TypeReference<T> typeReference) {
        return httpClient.delete(headers, url, typeReference);
    }

    public static String delete(String url) {
        return httpClient.delete(url);
    }

    public static String delete(List<Header> headers, String url) {
        return httpClient.delete(headers, url);
    }

}