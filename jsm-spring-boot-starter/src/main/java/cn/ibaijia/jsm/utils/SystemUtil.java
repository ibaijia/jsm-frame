package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.stat.JsmAlarmService;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.stat.model.DiskStat;
import cn.ibaijia.jsm.stat.model.SystemHealth;
import cn.ibaijia.jsm.stat.model.SystemStat;
import com.sun.management.OperatingSystemMXBean;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * @author longzl
 */
public class SystemUtil {

    public static final String JAVA_VERSION = getSystemProperty("java.version");

    public static final String OS_NAME = getSystemProperty("os.name");

    public static final String OS_VERSION = getSystemProperty("os.version");

    public static final boolean IS_OS_AIX = getOsMatches("AIX");

    public static final boolean IS_OS_HP_UX = getOsMatches("HP-UX");

    public static final boolean IS_OS_IRIX = getOsMatches("Irix");

    public static final boolean IS_OS_LINUX = (getOsMatches("Linux")) || (getOsMatches("LINUX"));

    public static final boolean IS_OS_MAC = getOsMatches("Mac");

    public static final boolean IS_OS_MAC_OSX = getOsMatches("Mac OS X");

    public static final boolean IS_OS_OS2 = getOsMatches("OS/2");

    public static final boolean IS_OS_SOLARIS = getOsMatches("Solaris");

    public static final boolean IS_OS_SUN_OS = getOsMatches("SunOS");

    public static final boolean IS_OS_UNIX = (IS_OS_AIX) || (IS_OS_HP_UX)
            || (IS_OS_IRIX) || (IS_OS_LINUX) || (IS_OS_MAC_OSX)
            || (IS_OS_SOLARIS) || (IS_OS_SUN_OS);

    public static final boolean IS_OS_WINDOWS = getOsMatches("Windows");

    public static final boolean IS_OS_WINDOWS_2000 = getOsMatches("Windows", "5.0");

    public static final boolean IS_OS_WINDOWS_95 = getOsMatches("Windows 9", "4.0");

    public static final boolean IS_OS_WINDOWS_98 = getOsMatches("Windows 9", "4.1");

    public static final boolean IS_OS_WINDOWS_ME = getOsMatches("Windows", "4.9");

    public static final boolean IS_OS_WINDOWS_NT = getOsMatches("Windows NT");

    public static final boolean IS_OS_WINDOWS_XP = getOsMatches("Windows", "5.1");

    private static boolean getOsMatches(String osNamePrefix) {
        if (OS_NAME == null) {
            return false;
        }
        return OS_NAME.startsWith(osNamePrefix);
    }

    private static boolean getOsMatches(String osNamePrefix, String osVersionPrefix) {
        if ((OS_NAME == null) || (OS_VERSION == null)) {
            return false;
        }
        return ((OS_NAME.startsWith(osNamePrefix)) && (OS_VERSION.startsWith(osVersionPrefix)));
    }

    private static String getSystemProperty(String property) {
        try {
            return System.getProperty(property);
        } catch (SecurityException localSecurityException) {
            System.err.println("Caught a SecurityException reading the system property '"
                    + property
                    + "'; the SystemUtils property value will default to null.");
        }
        return null;
    }

    public static SystemStat getSystemStat() {
        SystemStat systemStat = new SystemStat();
        systemStat.osName = System.getProperty("os.name");
        systemStat.osVersion = System.getProperty("os.version");
        systemStat.osArch = System.getProperty("os.arch");
        systemStat.userName = System.getProperty("user.name");
        systemStat.userHome = System.getProperty("user.home");
        systemStat.userDir = System.getProperty("user.dir");
        systemStat.userTimezone = System.getProperty("user.timezone");
        systemStat.userLanguage = System.getProperty("user.language");
        systemStat.tmpDir = System.getProperty("java.io.tmpdir");
        systemStat.runtimeName = System.getProperty("java.runtime.name");
        systemStat.runtimeVersion = System.getProperty("java.version");
        systemStat.jvmName = System.getProperty("java.vm.name");
        systemStat.jvmVersion = System.getProperty("java.vm.version");
        systemStat.javaHome = System.getProperty("java.home");
        systemStat.javaVersion = System.getProperty("java.version");
        systemStat.fileEncoding = System.getProperty("file.encoding");
        systemStat.catalinaHome = System.getProperty("catalina.home");
        systemStat.totalMemory = Runtime.getRuntime().totalMemory();
        systemStat.freeMemory = Runtime.getRuntime().freeMemory();
        systemStat.useMemory = systemStat.totalMemory - systemStat.freeMemory;
        systemStat.memRatio = Float.valueOf(NumberUtil.divide(systemStat.useMemory * 100, systemStat.totalMemory)).intValue();
        OperatingSystemMXBean osmxb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        if (osmxb != null) {
            double cpuLoad = osmxb.getProcessCpuLoad();
            systemStat.cpuRatio = Double.valueOf(cpuLoad * 100).intValue();
        }
        // 获得线程总数
        ThreadGroup parentThread;
        for (parentThread = Thread.currentThread().getThreadGroup(); parentThread
                .getParent() != null; parentThread = parentThread.getParent()) {
            ;
        }
        systemStat.threadCount = parentThread.activeCount();
        systemStat.clusterId = AppContext.getClusterId();
        systemStat.diskUsed = getMaxDiskUsePercent();
        systemStat.time = DateUtil.currentTime();
        return systemStat;
    }

    public static void addAlarm(Alarm alarm) {
        JsmAlarmService alarmService = SpringContext.getBean(JsmAlarmService.class);
        alarmService.add(alarm);
    }

    public static void clearAlarms() {
        JsmAlarmService alarmService = SpringContext.getBean(JsmAlarmService.class);
        alarmService.clear();
    }

    public static List<Alarm> pullAlarms() {
        JsmAlarmService alarmService = SpringContext.getBean(JsmAlarmService.class);
        return alarmService.pull();
    }

    public static List<DiskStat> getDiskStat() {
        File[] roots = File.listRoots();
        List<DiskStat> diskStatList = new ArrayList<>();
        for (File file : roots) {
            DiskStat diskStat = new DiskStat();
            diskStat.path = file.getPath();
            diskStat.total = file.getTotalSpace();
            diskStat.free = file.getFreeSpace();
            diskStatList.add(diskStat);
        }
        return diskStatList;
    }

    public static Integer getMaxDiskUsePercent() {
        if (!IS_OS_UNIX) {
            return 0;
        }
        String res = ShellUtil.exec("df");
        String[] arr = StringUtil.normalize(res).split(" ");
        int max = 0;
        for (int i = 1; i < arr.length; i++) {
            if (!arr[i].endsWith("%")) {
                continue;
            }
            Integer used = StringUtil.extractInteger(arr[i]);
            if (used != null && used > max) {
                max = used;
            }
        }
        return max;
    }

    public static SystemHealth getSystemHealth() {
        SystemHealth systemHealth = new SystemHealth();
        systemHealth.systemStat = getSystemStat();
        systemHealth.alarmList = pullAlarms();
        return systemHealth;
    }

}
