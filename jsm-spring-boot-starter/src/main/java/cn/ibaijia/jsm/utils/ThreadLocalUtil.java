package cn.ibaijia.jsm.utils;

import org.springframework.transaction.TransactionStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author longzl
 */
public class ThreadLocalUtil {

    private static List<ThreadLocal> appThreadLocalList = new ArrayList<>();

    public static final ThreadLocal<HttpServletRequest> REQUEST_TL = new ThreadLocal<>();
    public static final ThreadLocal<HttpServletResponse> RESPONSE_TL = new ThreadLocal<>();
    public static final ThreadLocal<String> JSP_PATH_TL = new ThreadLocal<>();
    public static final ThreadLocal<String> OAUTH_ACCESS_TOKEN_TL = new ThreadLocal<>();
    public static final ThreadLocal<String> OAUTH_VERIFY_RESULT_TL = new ThreadLocal<>();
    public static final ThreadLocal<SimpleDateFormat> SDF_TL = new ThreadLocal<>();
    public static final ThreadLocal<TransactionStatus> TRANS_TL = new ThreadLocal<>();
    public static final ThreadLocal<String> DATA_SOURCE_TL = new ThreadLocal<>();
    public static final ThreadLocal<String> TRACE_ID_TL = new ThreadLocal<>();
    public static final ThreadLocal<String> CLIENT_ID_TL = new ThreadLocal<>();
    public static final ThreadLocal<Integer> SLOW_API_LIMIT_TL = new ThreadLocal<>();

    public static boolean add(ThreadLocal threadLocal) {
        return appThreadLocalList.add(threadLocal);
    }

    public static void cleanAll() {
        for (ThreadLocal threadLocal : appThreadLocalList) {
            threadLocal.remove();
        }
        REQUEST_TL.remove();
        RESPONSE_TL.remove();
        JSP_PATH_TL.remove();
        OAUTH_ACCESS_TOKEN_TL.remove();
        OAUTH_VERIFY_RESULT_TL.remove();
        SDF_TL.remove();
        TRANS_TL.remove();
        DATA_SOURCE_TL.remove();
        TRACE_ID_TL.remove();
        CLIENT_ID_TL.remove();
        SLOW_API_LIMIT_TL.remove();
    }

    public static void clean(ThreadLocal threadLocal) {
        threadLocal.remove();
    }

}
