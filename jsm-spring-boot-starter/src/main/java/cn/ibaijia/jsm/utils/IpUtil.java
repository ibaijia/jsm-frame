package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import org.slf4j.Logger;

import java.net.InetAddress;

/**
 * @author longzl
 */
public class IpUtil {

    private static Logger logger = LogUtil.log(IpUtil.class);

    private static final String ENABLE_CMD = "iptables -D INPUT -s %s -j DROP";
    private static final String DISABLE_CMD = "iptables -I INPUT -s %s -j DROP";

    private static final String LOCAL_IP = "127.0.0.1";
    private static final int STRING_IP_LENGTH = 4;
    private static final int HEX_IP_LENGTH = 8;
    private static String hostIp;
    private static String hostName;

    public static String getHostIp() {
        if (hostIp == null) {
            try {
                InetAddress addr = InetAddress.getLocalHost();
                hostIp = addr.getHostAddress();
            } catch (Exception e) {
                return null;
            }
        }
        return hostIp;
    }

    public static String getHostName() {
        if (hostName == null) {
            try {
                InetAddress addr = InetAddress.getLocalHost();
                hostName = addr.getHostName();
            } catch (Exception e) {
                return null;
            }
        }
        return hostName;
    }

    public static boolean checkRemoteIp(String remoteIp, String contextKey) {
        String ips = AppContext.get(contextKey);
        if (StringUtil.isEmpty(ips)) {
            return false;
        }
        return ips.contains(remoteIp);
    }

    /**
     * 禁IP
     *
     * @param ip
     */
    public boolean disableIp(String ip) {
        return checkAndExec(ip, DISABLE_CMD);
    }

    /**
     * 解禁IP
     *
     * @param ip
     */
    public boolean enableIp(String ip) {
        return checkAndExec(ip, ENABLE_CMD);
    }

    private boolean checkAndExec(String ip, String disableCmd) {
        if (!SystemUtil.IS_OS_UNIX) {
            logger.error("not unix ,can't run shell.");
            return false;
        }
        if (!StringUtil.isIpv4(ip)) {
            logger.error("is not a ip:{}", ip);
            return false;
        }

        String cmd = String.format(disableCmd, ip);
        ShellUtil.exec(cmd);
        return true;
    }


    public static boolean isInternalIp(String ip) {
        if (!StringUtil.isIpv4(ip)) {
            return false;
        }
        if (LOCAL_IP.equals(ip)) {
            return true;
        }
        byte[] address = intToByte(ipToInt(ip));
        return isInternalIp(address);
    }


    public static boolean isInternalIp(byte[] addr) {
        final byte b0 = addr[0];
        final byte b1 = addr[1];
        //10.x.x.x/8
        final byte section1 = 0x0A;
        //172.16.x.x/12
        final byte section2 = (byte) 0xAC;
        final byte section3 = (byte) 0x10;
        final byte section4 = (byte) 0x1F;
        //192.168.x.x/16
        final byte section5 = (byte) 0xC0;
        final byte section6 = (byte) 0xA8;
        switch (b0) {
            case section1:
                return true;
            case section2:
                if (b1 >= section3 && b1 <= section4) {
                    return true;
                }
            case section5:
                if (b1 == section6) {
                    return true;
                }
            default:
                return false;

        }
    }
    public static byte[] intToByte(int i) {
        byte[] targets = new byte[4];
        targets[3] = (byte) (i & 0xFF);
        targets[2] = (byte) (i >> 8 & 0xFF);
        targets[1] = (byte) (i >> 16 & 0xFF);
        targets[0] = (byte) (i >> 24 & 0xFF);
        return targets;
    }

    public static Integer ipToInt(String ipStr) {
        String[] ip = ipStr.split("\\.");
        return (Integer.parseInt(ip[0]) << 24) + (Integer.parseInt(ip[1]) << 16) + (Integer.parseInt(ip[2]) << 8) + Integer.parseInt(ip[3]);
    }

    public static String intToIp(int intIp) {
        StringBuilder builder = new StringBuilder();
        int first = (intIp & 0xFFFFFFFF) >> 24;
        if (first < 0) {
            first += 256;
        }
        builder.append(first + ".");
        builder.append(((intIp & 0x00FFFFFF) >> 16) + ".");
        builder.append(((intIp & 0x0000FFFF) >> 8) + ".");
        builder.append(intIp & 0x000000FF);
        return builder.toString();
    }

    public static String ipToHex(String ipStr) {
        if (StringUtil.isEmpty(ipStr)) {
            return ipStr;
        }
        String[] ip = ipStr.split("\\.");
        if (ip.length != STRING_IP_LENGTH) {
            return ipStr;
        }
        StringBuilder sb = new StringBuilder();
        String res1 = Integer.toHexString(Integer.parseInt(ip[0]));
        String res2 = Integer.toHexString(Integer.parseInt(ip[1]));
        String res3 = Integer.toHexString(Integer.parseInt(ip[2]));
        String res4 = Integer.toHexString(Integer.parseInt(ip[3]));
        sb.append(res1.length() < 2 ? ("0" + res1) : res1);
        sb.append(res2.length() < 2 ? ("0" + res2) : res2);
        sb.append(res3.length() < 2 ? ("0" + res3) : res3);
        sb.append(res4.length() < 2 ? ("0" + res4) : res4);
        return sb.toString();
    }

    public static String hexToIp(String hexStr) {
        if (hexStr.length() != HEX_IP_LENGTH) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.parseInt(hexStr.substring(0, 2), 16)).append(".");
        sb.append(Integer.parseInt(hexStr.substring(2, 4), 16)).append(".");
        sb.append(Integer.parseInt(hexStr.substring(4, 6), 16)).append(".");
        sb.append(Integer.parseInt(hexStr.substring(6, 8), 16));
        return sb.toString();
    }


    public static boolean isInRange(String ip, String cidr) {
        if (!cidr.contains(BaseConstants.SYSTEM_SYMBOL_PATH)) {
            return StringUtil.equals(ip, cidr);
        }
        String[] ips = ip.split("\\.");
        int ipAddr = (Integer.parseInt(ips[0]) << 24)
                | (Integer.parseInt(ips[1]) << 16)
                | (Integer.parseInt(ips[2]) << 8) | Integer.parseInt(ips[3]);
        int type = Integer.parseInt(cidr.replaceAll(".*/", ""));
        int mask = 0xFFFFFFFF << (32 - type);
        String cidrIp = cidr.replaceAll("/.*", "");
        String[] cidrIps = cidrIp.split("\\.");
        int cidrIpAddr = (Integer.parseInt(cidrIps[0]) << 24)
                | (Integer.parseInt(cidrIps[1]) << 16)
                | (Integer.parseInt(cidrIps[2]) << 8)
                | Integer.parseInt(cidrIps[3]);

        return (ipAddr & mask) == (cidrIpAddr & mask);
    }

}
