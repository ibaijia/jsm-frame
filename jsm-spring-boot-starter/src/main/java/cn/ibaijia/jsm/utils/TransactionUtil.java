package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.SpringContext;
import org.slf4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

/**
 * @author longzl
 */
public class TransactionUtil {

    private static Logger logger = LogUtil.log(TransactionUtil.class);

    public static PlatformTransactionManager getTransactionManager() {
        return ((PlatformTransactionManager) SpringContext.getBean("transactionManager"));
    }

    public static PlatformTransactionManager getTransactionManager(String transManagerName) {
        return ((PlatformTransactionManager) SpringContext.getBean(transManagerName));
    }

    public static void setTransactionStatus(TransactionStatus status) {
        ThreadLocalUtil.TRANS_TL.set(status);
    }

    public static TransactionStatus getTransactionStatus() {
        return ThreadLocalUtil.TRANS_TL.get();
    }

    public static void removeTransactionStatus() {
        ThreadLocalUtil.TRANS_TL.remove();
    }

    public static void setRollbackOnly() {
        TransactionStatus transactionStatus = ThreadLocalUtil.TRANS_TL.get();
        if (transactionStatus == null) {
            logger.error("setRollbackOnly transactionStatus is null.");
            return;
        }
        if (transactionStatus.isCompleted()) {
            logger.error("setRollbackOnly transactionStatus is completed.");
            return;
        }
        transactionStatus.setRollbackOnly();
    }

    public static void commit() {
        PlatformTransactionManager tm = getTransactionManager();
        TransactionStatus transactionStatus = ThreadLocalUtil.TRANS_TL.get();
        if (transactionStatus == null) {
            logger.error("commit transactionStatus is null.");
            return;
        }
        if (transactionStatus.isCompleted()) {
            logger.error("commit transactionStatus is completed.");
            return;
        }
        tm.commit(transactionStatus);
    }

}
