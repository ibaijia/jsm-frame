package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * @author longzl
 */
public class ShellUtil {

    private static Logger logger = LogUtil.log(ShellUtil.class);

    public static String exec(String cmd) {
        return exec(cmd, "UTF-8");
    }

    public static String exec(String cmd, String charset) {
        logger.debug("cmd:" + cmd);
        if (!SystemUtil.IS_OS_UNIX) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        BufferedReader inputStreamReader = null;
        BufferedReader errInputStreamReader = null;
        try {
            String[] cmdA = {"/bin/sh", "-c", cmd};
            Process process = Runtime.getRuntime().exec(cmdA);
            inputStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream(), charset));
            errInputStreamReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), charset));
            String line = null;
            while ((line = inputStreamReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            String errLine = null;
            while ((errLine = errInputStreamReader.readLine()) != null) {
                sb.append(errLine).append("\n");
            }
            process.waitFor(60000, TimeUnit.SECONDS);
            logger.debug("output:" + sb);
            return sb.toString();
        } catch (Exception e) {
            logger.error("exec cmd error:" + cmd, e);
        } finally {
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    logger.error("close inputStreamReader error.", e);
                }
            }
            if (errInputStreamReader != null) {
                try {
                    errInputStreamReader.close();
                } catch (IOException e) {
                    logger.error("close errInputStreamReader error.", e);
                }
            }
        }
        return null;
    }

}
