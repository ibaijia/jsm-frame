package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;

import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * @author longzl
 */
public class ValidCodeUtil {
    private static Logger log = LogUtil.log(ValidCodeUtil.class);

    private static final int MAX_COLOR = 255;

    private Random random = new Random();
    /**
     * 随机产生的字符串
     */
    private String randAll = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    /**
     * 随机产生的字符串
     */
    private String randNumber = "0123456789";
    /**
     * 放到session中的key
     */
    private String name = "validCode";
    /**
     * 图片宽
     */
    private int width = 80;
    /**
     * 图片高
     */
    private int height = 26;
    /**
     * 干扰线数量
     */
    private int lineSize = 40;
    /**
     * 随机产生字符数量
     */
    private int num = 4;
    private boolean numberOnly = true;

    private StringBuilder validCode = new StringBuilder();

    public String getValidCode() {
        return validCode.toString();
    }

    public ValidCodeUtil(String name, int width, int height, int num) {
        super();
        if (!StringUtil.isEmpty(name)) {
            this.name = name;
        }
        if (width != 0) {
            this.width = width;
        }
        if (height != 0) {
            this.height = height;
        }
        if (num != 0) {
            this.num = num;
        }
    }

    public ValidCodeUtil(String name, int width, int height, int num, boolean numberOnly) {
        this(name, width, height, num);
        this.numberOnly = numberOnly;
    }

    /**
     * 获得字体
     */
    private Font getFont() {
        return new Font("Fixedsys", Font.CENTER_BASELINE, 18);
    }

    /**
     * 获得颜色
     */
    private Color getRandColor(int fc, int bc) {
        if (fc > MAX_COLOR) {
            fc = MAX_COLOR;
        }
        if (bc > MAX_COLOR) {
            bc = MAX_COLOR;
        }
        int r = fc + random.nextInt(bc - fc - 16);
        int g = fc + random.nextInt(bc - fc - 14);
        int b = fc + random.nextInt(bc - fc - 18);
        return new Color(r, g, b);
    }

    /**
     * 生成随机图片
     */
    public BufferedImage getImage(HttpSession session) {
        //BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
        BufferedImage image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_BGR);
        //产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
        Graphics g = image.getGraphics();
        g.fillRect(0, 0, this.width, this.height);
        g.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 18));
        g.setColor(getRandColor(110, 133));
        //绘制干扰线
        for (int i = 0; i <= lineSize; i++) {
            drowLine(g);
        }
        //绘制随机字符
        for (int i = 1; i <= this.num; i++) {
            drowString(g, i);
        }
        session.removeAttribute(this.name);
        session.setAttribute(this.name, this.getValidCode());
        log.debug("validCode:" + this.getValidCode());
        g.dispose();
        return image;
    }

    public BufferedImage getImage() {
        //BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
        BufferedImage image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_BGR);
        /* 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作 */
        Graphics g = image.getGraphics();
        g.fillRect(0, 0, this.width, this.height);
        g.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 18));
        g.setColor(getRandColor(110, 133));
        //绘制干扰线
        for (int i = 0; i <= lineSize; i++) {
            drowLine(g);
        }
        //绘制随机字符
        for (int i = 1; i <= this.num; i++) {
            drowString(g, i);
        }
        log.debug("validCode:" + this.getValidCode());
        g.dispose();
        return image;
    }

    /**
     * 绘制字符串
     */
    private void drowString(Graphics g, int i) {
        g.setFont(getFont());
        g.setColor(new Color(random.nextInt(101), random.nextInt(111), random.nextInt(121)));
        String rand = getRandomChar();
        this.validCode.append(rand);
        g.translate(random.nextInt(3), random.nextInt(3));
        g.drawString(rand, 13 * i, 16);
    }

    /**
     * 绘制干扰线
     */
    private void drowLine(Graphics g) {
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        int xl = random.nextInt(13);
        int yl = random.nextInt(15);
        g.drawLine(x, y, x + xl, y + yl);
    }

    /**
     * 获取随机的字符
     */
    private String getRandomChar() {
        String str = this.numberOnly ? randNumber : randAll;
        int num = random.nextInt(str.length());
        return String.valueOf(str.charAt(num));
    }

}