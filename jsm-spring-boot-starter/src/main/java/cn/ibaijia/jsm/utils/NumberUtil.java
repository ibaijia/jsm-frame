package cn.ibaijia.jsm.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author longzl
 */
public class NumberUtil {

    private static DecimalFormat format = new DecimalFormat();

    public static synchronized String formatString(Object number, String pattern) {
        format.applyPattern(pattern);
        return format.format(number);
    }

    public static float format(float number, int newScale, RoundingMode roundingMode) {
        BigDecimal bg = new BigDecimal(String.valueOf(number)).setScale(newScale, roundingMode);
        return bg.floatValue();
    }

    public static float formatHalfUp(float number, int newScale) {
        return format(number, newScale, RoundingMode.HALF_UP);
    }

    public static float formatUp(float number, int newScale) {
        return format(number, newScale, RoundingMode.UP);
    }

    public static float formatDown(float number, int newScale) {
        return format(number, newScale, RoundingMode.DOWN);
    }

    public static double format(double number, int newScale, RoundingMode roundingMode) {
        BigDecimal bg = new BigDecimal(String.valueOf(number)).setScale(newScale, roundingMode);
        return bg.doubleValue();
    }

    public static double formatHalfUp(double number, int newScale) {
        return format(number, newScale, RoundingMode.HALF_UP);
    }

    public static double formatUp(double number, int newScale) {
        return format(number, newScale, RoundingMode.UP);
    }

    public static double formatDown(double number, int newScale) {
        return format(number, newScale, RoundingMode.DOWN);
    }

    public static float add(float a, float b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.add(bigB).floatValue();
    }

    public static float subtract(float a, float b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.subtract(bigB).floatValue();
    }

    public static float multiply(float a, float b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.multiply(bigB).floatValue();
    }

    public static float divide(float a, float b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.divide(bigB, 6, RoundingMode.HALF_UP).floatValue();
    }

    public static float divide(float a, float b, int scale, RoundingMode mode) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.divide(bigB, scale, mode).floatValue();
    }

    public static double add(double a, double b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.add(bigB).doubleValue();
    }

    public static double subtract(double a, double b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.subtract(bigB).doubleValue();
    }

    public static double multiply(double a, double b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.multiply(bigB).doubleValue();
    }

    public static double divide(double a, double b) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.divide(bigB, 6, RoundingMode.HALF_UP).doubleValue();
    }


    public static double divide(double a, double b, int scale, RoundingMode mode) {
        BigDecimal bigA = new BigDecimal(String.valueOf(a));
        BigDecimal bigB = new BigDecimal(String.valueOf(b));
        return bigA.divide(bigB, scale, mode).doubleValue();
    }
}
