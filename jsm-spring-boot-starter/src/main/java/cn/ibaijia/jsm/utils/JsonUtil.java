package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.json.DicAnnIntrospector;
import cn.ibaijia.jsm.json.JsmBeanSerializerModifier;
import cn.ibaijia.jsm.json.JsmJsonConfig;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.logging.log4j.status.StatusLogger;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.TimeZone;

/**
 * @author longzl
 */
public class JsonUtil {

    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();

    private static final String SNAKE_CASE = "snakeCase";
    private static final String LOWER_CAMEL_CASE = "lowerCamelCase";
    private static final String UPPER_CAMEL_CASE = "upperCamelCase";
    private static final String LOWER_CASE = "lowerCase";
    private static final String KEBAB_CASE = "kebabCase";

    private static JsmJsonConfig jsmJsonConfig = new JsmJsonConfig();

    /**
     * @return 将java对象转化为json字符串
     */
    public static String toJsonString(Object data) {
        try {
            return getObjectMapper().writeValueAsString(data);
        } catch (Exception e) {
            logger.error("toJson error!", e);
            throw new RuntimeException(e);
        }
    }

    public static boolean isJsonString(String jsonStr) {
        try {
            JsonNode node = getObjectMapper().readTree(jsonStr);
            return node.isArray() || node.isObject();
        } catch (IOException e) {
            return false;
        }
    }

    public static ObjectMapper getObjectMapper() {
        try {
            return SpringContext.getBean(ObjectMapper.class);
        } catch (Exception e) {
            return createObjectMapper();
        }
    }

    public static ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        if (!Boolean.TRUE.equals(jsmJsonConfig.isShowNull())) {
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }
        objectMapper.setTimeZone(TimeZone.getDefault());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setPropertyNamingStrategy(new JsmStrategy());
        objectMapper.setSerializerFactory(objectMapper.getSerializerFactory().withSerializerModifier(new JsmBeanSerializerModifier()));
        AnnotationIntrospector sis = objectMapper.getSerializationConfig().getAnnotationIntrospector();
        AnnotationIntrospector dis = objectMapper.getDeserializationConfig().getAnnotationIntrospector();
        DicAnnIntrospector dicAnnIntrospector = new DicAnnIntrospector();
        AnnotationIntrospector newSis = AnnotationIntrospectorPair.pair(sis, dicAnnIntrospector);
        AnnotationIntrospector newDis = AnnotationIntrospectorPair.pair(dis, dicAnnIntrospector);
        objectMapper.setAnnotationIntrospectors(newSis, newDis);
        return objectMapper;
    }

    public static JsonNode readTree(String jsonStr) {
        try {
            return getObjectMapper().readTree(jsonStr);
        } catch (IOException e) {
            logger.error("readTree error!", e);
            throw new RuntimeException(e);
        }
    }

    public static String getStrategyName(String name) {
        String namingStrategy = AppContext.get(AppContextKey.JSON_NAMING_STRATEGY);
        if (StringUtil.isEmpty(namingStrategy)) {
            return name;
        } else if (SNAKE_CASE.equalsIgnoreCase(namingStrategy)) {
            return getStrategyName(name, PropertyNamingStrategy.SNAKE_CASE);
        } else if (LOWER_CAMEL_CASE.equalsIgnoreCase(namingStrategy)) {
            return StringUtil.lowerCaseFirst(getStrategyName(name, PropertyNamingStrategy.UPPER_CAMEL_CASE));
        } else if (UPPER_CAMEL_CASE.equalsIgnoreCase(namingStrategy)) {
            return getStrategyName(name, PropertyNamingStrategy.UPPER_CAMEL_CASE);
        } else if (KEBAB_CASE.equalsIgnoreCase(namingStrategy)) {
            return getStrategyName(name, PropertyNamingStrategy.KEBAB_CASE);
        } else if (LOWER_CASE.equalsIgnoreCase(namingStrategy)) {
            return getStrategyName(name, PropertyNamingStrategy.LOWER_CASE);
        }
        return name;
    }

    public static String getStrategyName(String name, PropertyNamingStrategy namingStrategy) {
        if (namingStrategy instanceof PropertyNamingStrategy.SnakeCaseStrategy) {
            return ((PropertyNamingStrategy.SnakeCaseStrategy) namingStrategy).translate(name);
        } else if (namingStrategy instanceof PropertyNamingStrategy.UpperCamelCaseStrategy) {
            return ((PropertyNamingStrategy.UpperCamelCaseStrategy) namingStrategy).translate(name);
        } else if (namingStrategy instanceof PropertyNamingStrategy.KebabCaseStrategy) {
            return ((PropertyNamingStrategy.KebabCaseStrategy) namingStrategy).translate(name);
        } else if (namingStrategy instanceof PropertyNamingStrategy.LowerCaseStrategy) {
            return ((PropertyNamingStrategy.LowerCaseStrategy) namingStrategy).translate(name);
        }
        return name;
    }

    public static class JsmStrategy extends PropertyNamingStrategy.PropertyNamingStrategyBase {
        public JsmStrategy() {
        }

        @Override
        public String translate(String input) {
            return getStrategyName(input);
        }
    }

    public static <T> T parseObject(String text, Class<T> clazz) {
        try {
            return getObjectMapper().readValue(text, clazz);
        } catch (Exception e) {
            logger.error("parseObject error!" + text, e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T parseObject(String text, TypeReference<T> type) {
        try {
            return getObjectMapper().readValue(text,type);
        } catch (Exception e) {
            logger.error("parseObject error!" + text, e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T parseObject(String text, JavaType type) {
        try {
            return getObjectMapper().readValue(text, type);
        } catch (Exception e) {
            logger.error("parseObject error!" + text, e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T parseObject(String text, Type type) {
        try {
            return getObjectMapper().readValue(text, getJavaType(type));
        } catch (Exception e) {
            logger.error("parseObject error!", e);
            throw new RuntimeException(e);
        }
    }

    public static JavaType getJavaType(Type type) {
        //判断是否带有泛型
        if (type instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
            //获取泛型类型
            Class rowClass = (Class) ((ParameterizedType) type).getRawType();

            JavaType[] javaTypes = new JavaType[actualTypeArguments.length];

            for (int i = 0; i < actualTypeArguments.length; i++) {
                //泛型也可能带有泛型，递归获取
                javaTypes[i] = getJavaType(actualTypeArguments[i]);
            }
            return TypeFactory.defaultInstance().constructParametricType(rowClass, javaTypes);
        } else {
            //简单类型直接用该类构建JavaType
            Class cla = (Class) type;
            return TypeFactory.defaultInstance().constructParametricType(cla, new JavaType[0]);
        }
    }

    public static JsmJsonConfig getJsmJsonConfig() {
        return jsmJsonConfig;
    }
}
