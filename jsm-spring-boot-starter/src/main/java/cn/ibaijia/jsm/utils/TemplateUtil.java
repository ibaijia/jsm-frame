package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author longzl
 */
public class TemplateUtil {

    private static String HEADER_PREFIX = "header.";
    private static String REQUEST_PREFIX = "request.";
    private static String ATTRIBUTE_PREFIX = "attribute.";
    private static String SESSION_PREFIX = "session.";
    private static String REG_VAR = "#\\{(.*?)\\}";
    private static String ENV_VAR = "\\$\\{(.*?)\\}";

    public static String format(String tplStr, Map<String, Object> data) {
        if (!tplStr.contains(BaseConstants.SYSTEM_SYMBOL_HASH)) {
            return tplStr;
        }
        Pattern pattern = Pattern.compile(REG_VAR);
        Matcher matcher = pattern.matcher(tplStr);
        Set<String> vars = new HashSet<String>();
        while (matcher.find()) {
            vars.add(matcher.group(1));
        }
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            tplStr = tplStr.replaceAll("#\\{" + entry.getKey() + "\\}", StringUtil.toString(entry.getValue()));
        }
        return tplStr;
    }

    public static String formatWithEnvVar(String tplStr) {
        if (!tplStr.contains(BaseConstants.SYSTEM_SYMBOL_DOLLAR)) {
            return tplStr;
        }
        Pattern pattern = Pattern.compile(ENV_VAR);
        Matcher matcher = pattern.matcher(tplStr);
        Set<String> vars = new HashSet<>();
        while (matcher.find()) {
            vars.add(matcher.group(1));
        }
        for (String key : vars) {
            tplStr = tplStr.replaceAll("\\$\\{" + key + "\\}", AppContext.get(key));
        }
        return tplStr;
    }

    public static String formatWithContextVar(String str) {
        if (!str.contains(BaseConstants.SYSTEM_SYMBOL_HASH)) {
            return str;
        }
        Pattern pattern = Pattern.compile(REG_VAR);
        Matcher matcher = pattern.matcher(str);
        Set<String> vars = new HashSet<String>();
        while (matcher.find()) {
            vars.add(matcher.group(1));
        }
        for (String var : vars) {
            String val = getContextVarValue(var);
            if (!StringUtil.isEmpty(val)) {
                str = str.replaceAll("#\\{" + var + "\\}", val);
            }
        }
        return str;
    }

    private static String getContextVarValue(String var) {
        if (var.startsWith(REQUEST_PREFIX)) {
            return StringUtil.toString(WebContext.getRequest().getParameter(var.replace(REQUEST_PREFIX, "")));
        } else if (var.startsWith(HEADER_PREFIX)) {
            return StringUtil.toString(WebContext.getRequest().getHeader(var.replace(HEADER_PREFIX, "")));
        } else if (var.startsWith(ATTRIBUTE_PREFIX)) {
            return StringUtil.toString(WebContext.getRequest().getAttribute(var.replace(ATTRIBUTE_PREFIX, "")));
        } else if (var.startsWith(SESSION_PREFIX)) {
            if (WebContext.currentSession() != null) {
                return StringUtil.toString(WebContext.currentSession().get(var.replace(SESSION_PREFIX, "")));
            }
        } else {
            String value = StringUtil.toString(WebContext.getRequest().getAttribute(var));
            if (StringUtil.isEmpty(value)) {
                value = StringUtil.toString(WebContext.getRequest().getParameter(var));
                if (StringUtil.isEmpty(value)) {
                    value = StringUtil.toString(WebContext.getRequest().getHeader(var));
                    if (StringUtil.isEmpty(value)) {
                        if (WebContext.currentSession() != null) {
                            value = StringUtil.toString(WebContext.currentSession().get(var));
                        }
                    }
                }
            }
            return value;
        }
        return null;
    }

}
