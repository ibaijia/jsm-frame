package cn.ibaijia.jsm.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * @author longzl
 */
public class EncryptUtil {

    private static Logger logger = LogUtil.log(PropUtil.class);

    /**
     * 用MD5算法进行加密
     *
     * @param str 需要加密的字符串
     * @return 加密后的字符串
     */
    public static String md5(String str) {
        return encode(str, "MD5").toUpperCase();
    }

    /**
     * 用SHA算法进行加密
     *
     * @param str 需要加密的字符串
     * @return 加密后的字符串
     */
    public static String sha(String str) {
        return encode(str, "SHA");
    }

    public static String sha1(String str) {
        return encode(str, "SHA-1");
    }

    /**
     * 用base64算法进行加密
     *
     * @param str 需要加密的字符串
     * @return 加密后的字符串
     */
    public static String base64Encode(String str) {
        String charset = "UTF-8";
        return base64Encode(str.getBytes(), charset);
    }

    public static String base64Encode(byte[] bytes) {
        String charset = "UTF-8";
        return base64Encode(bytes, charset);
    }

    public static String base64Encode(byte[] bytes, String charset) {
        try {
            return new String(Base64.encodeBase64(bytes), charset);
        } catch (Exception e) {
            logger.error("base64Encode error!", e);
            return null;
        }
    }

    /**
     * 用base64算法进行解密
     *
     * @param str 需要加密的字符串
     * @return 加密后的字符串
     * @throws IOException
     */
    public static String base64Decode(String str) {
        String charset = "UTF-8";
        return new String(base64Decode(str, charset));
    }

    public static byte[] base64Decode(String str, String charset) {
        try {
            return Base64.decodeBase64(str.getBytes(charset));
        } catch (Exception e) {
            logger.error("base64Decode error!", e);
            return null;
        }
    }

    /**
     * 加密算法
     *
     * @param str    需要加密的字符串
     * @param method 加密方法
     * @return 加密后的字符串
     */
    private static String encode(String str, String method) {
        MessageDigest md = null;
        StringBuilder dstr = new StringBuilder(32);
        try {

            md = MessageDigest.getInstance(method);
            // 进行加密运算
            byte[] bytes = md.digest(str.getBytes());
            for (int i = 0; i < bytes.length; i++) {
                // 将整数转换成十六进制形式的字符串 这里与0xff进行与运算的原因是保证转换结果为32位
                String astr = Integer.toHexString(bytes[i] & 0xFF);
                if (astr.length() == 1) {
                    dstr.append("0");
                }
                dstr.append(astr);
            }
            return dstr.toString();
        } catch (NoSuchAlgorithmException e) {
            logger.error("encode error!", e);
        }
        return dstr.toString();
    }

    public static String byteToHex(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) throws IOException {
        System.out.println("lappmd5加密:" + md5("hc123").toLowerCase());
    }

    public static String soeEncode(String str) {
        byte[] arr = str.getBytes();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (byte) ((arr[i] - 10) ^ 10);
        }
        Base64 base64 = new Base64();
        ArrayUtils.reverse(arr);
        return new String(base64.encode(arr));
    }

    public static String soeDecode(String str) {
        Base64 base64 = new Base64();
        byte[] arr = base64.decode(str.getBytes());
        ArrayUtils.reverse(arr);
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (byte) ((arr[i] ^ 10) + 10);
        }
        return new String(arr);
    }

    public static String lappMd5(String str) {
        return md5(base64Encode(str + "@lapp") + str + base64Encode(str));
    }

    public static String lappSha(String str) {
        return sha(base64Encode(str + "@lapp") + str + base64Encode(str));
    }

    public static String urlDecode(String str) {
        return urlDecode(str, "UTF-8");
    }

    public static String urlEncode(String str) {
        return urlEncode(str, "UTF-8");
    }

    public static String urlDecode(String str, String enc) {
        try {
            return URLDecoder.decode(str, enc);
        } catch (UnsupportedEncodingException e) {
            logger.error("urlDecode error!", e);
            return null;
        }
    }

    public static String urlEncode(String str, String enc) {
        try {
            return URLEncoder.encode(str, enc);
        } catch (UnsupportedEncodingException e) {
            logger.error("urlEncode error!", e);
            return null;
        }
    }
}
