package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.consts.BaseConstants;
import org.slf4j.Logger;

import java.lang.reflect.Field;
import java.util.Date;

/**
 * @author longzl
 */
public class BeanUtil {
    private static Logger logger = LogUtil.log(BeanUtil.class);

    public static void copy(Object org, Object dest, String... ignoreProperties) {
        Field[] orgFields = org.getClass().getFields();
        for (Field field : orgFields) {
            String name = field.getName();
            if (!ignore(name, ignoreProperties)) {
//				setValue(dest,dest);
                Field destField = getField(dest, name);
                if (destField != null) {
                    copyField(org, dest, field, destField);
                }
            }
        }
    }

    public static void setFieldValue(Object obj, Field field, Object value) {
        if (value == null) {
            return;
        }
        try {
            Class<?> fieldType = field.getType();
            if (fieldType.isAssignableFrom(value.getClass())) {
                field.set(obj, value);
            } else {
                String valueStr = value.toString();
                logger.debug("field type:" + fieldType);
                logger.debug("valueStr:" + valueStr);
                if (fieldType.isAssignableFrom(Integer.class) || fieldType.isAssignableFrom(int.class)) {
                    value = Float.valueOf(valueStr).intValue();
                } else if (fieldType.isAssignableFrom(Date.class)) {
                    if (value instanceof Long) {
                        value = DateUtil.parse((Long) value);
                    } else if (value instanceof String) {
                        value = DateUtil.parse(valueStr);
                    }
                } else if (fieldType.isAssignableFrom(Long.class) || fieldType.isAssignableFrom(long.class)) {
                    value = Long.valueOf(valueStr);
                } else if (fieldType.isAssignableFrom(Float.class) || fieldType.isAssignableFrom(float.class)) {
                    value = Float.valueOf(valueStr);
                } else if (fieldType.isAssignableFrom(Double.class) || fieldType.isAssignableFrom(double.class)) {
                    value = Double.valueOf(valueStr);
                } else if (fieldType.isAssignableFrom(Short.class) || fieldType.isAssignableFrom(short.class)) {
                    value = Short.valueOf(valueStr);
                } else if (fieldType.isAssignableFrom(Byte.class) || fieldType.isAssignableFrom(byte.class)) {
                    value = Byte.valueOf(valueStr);
                } else if (fieldType.isAssignableFrom(Boolean.class) || fieldType.isAssignableFrom(boolean.class)) {
                    value = Boolean.valueOf(valueStr);
                } else if (fieldType.isAssignableFrom(String.class)) {
                    value = valueStr;
                }
                field.set(obj, value);
            }
        } catch (Exception e) {
            logger.error("setFieldValue error!", e);
        }
    }

    private static void copyField(Object org, Object dest, Field field, Field destField) {
        try {
            Class<?> destType = destField.getType();
            Class<?> orgType = field.getType();
            Object orgValue = getFieldValue(field, org);
            if (orgValue == null) {
                destField.set(dest, orgValue);
                return;
            }
            if (destType.isAssignableFrom(orgType)) {
                destField.set(dest, orgValue);
            } else {
                String orgValueStr = orgValue.toString();
                logger.debug("dest type:" + destType);
                logger.debug("orgValueStr:" + orgValueStr);
                if (destType.isAssignableFrom(Integer.class) || destType.isAssignableFrom(int.class)) {
                    orgValue = Float.valueOf(orgValueStr).intValue();
                } else if (destType.isAssignableFrom(Date.class)) {
                    if (orgValue instanceof Long) {
                        orgValue = DateUtil.parse((Long) orgValue);
                    } else if (orgValue instanceof String) {
                        orgValue = DateUtil.parse(orgValueStr);
                    }
                } else if (destType.isAssignableFrom(Long.class) || destType.isAssignableFrom(long.class)) {
                    orgValue = Long.valueOf(orgValueStr);
                } else if (destType.isAssignableFrom(Float.class) || destType.isAssignableFrom(float.class)) {
                    orgValue = Float.valueOf(orgValueStr);
                } else if (destType.isAssignableFrom(Double.class) || destType.isAssignableFrom(double.class)) {
                    orgValue = Double.valueOf(orgValueStr);
                } else if (destType.isAssignableFrom(Short.class) || destType.isAssignableFrom(short.class)) {
                    orgValue = Short.valueOf(orgValueStr);
                } else if (destType.isAssignableFrom(Byte.class) || destType.isAssignableFrom(byte.class)) {
                    orgValue = Byte.valueOf(orgValueStr);
                } else if (destType.isAssignableFrom(Boolean.class) || destType.isAssignableFrom(boolean.class)) {
                    orgValue = Boolean.valueOf(orgValueStr);
                } else if (destType.isAssignableFrom(String.class)) {
                    orgValue = orgValueStr;
                }
                destField.set(dest, orgValue);
            }
        } catch (Exception e) {
            logger.error("copyField error:" + field.getName(), e);
        }
    }

    private static Field getField(Object dest, String name) {
        try {
            return dest.getClass().getField(name);
        } catch (Exception e) {
            logger.debug("getField {} error!", name);
            return null;
        }
    }


    private static boolean ignore(String name, String[] ignoreProperties) {
        if (ignoreProperties == null) {
            return false;
        }
        for (String prop : ignoreProperties) {
            if (prop.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static Object getFieldValue(Field field, Object obj) {
        try {
            //ReflectionUtil.getFieldValue(this, name)
            return field.get(obj);
        } catch (Exception e) {
            logger.error("getFieldValue error!", e);
            return null;
        }
    }

    public static Object getFieldValue(String fieldName, Object obj) {
        try {
            if (!fieldName.contains(BaseConstants.SYSTEM_SYMBOL_POINT)) {
                Field field = getDeclaredField(obj, fieldName);
                return field.get(obj);
            } else {
                int firstIdx = fieldName.indexOf(BaseConstants.SYSTEM_SYMBOL_POINT);
                String firstField = fieldName.substring(0, firstIdx);
                Field field = getDeclaredField(obj, firstField);
                return getFieldValue(fieldName.substring(firstIdx + 1), field.get(obj));
            }
        } catch (Exception e) {
            logger.error("getFieldValue error!", e);
            return null;
        }
    }

    private static Field getDeclaredField(Object object, String fieldName) {
        if (object == null) {
            logger.error("object is null ,can't get fieldName.{}", fieldName);
            return null;
        }
        if (StringUtil.isEmpty(fieldName)) {
            logger.error("fieldName is empty ,can't get fieldName.");
            return null;
        }
        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz
                .getSuperclass()) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                // Field不在当前类定义,继续向上转型
            }
        }
        return null;
    }

}
