package cn.ibaijia.jsm.utils;

import java.util.HashMap;

/**
 * @Author: LongZL
 * @Date: 2022/1/26 10:26
 */
public class MimeTypeUtil {

    private static HashMap<String, String> map = new HashMap<>();

    static {
        map.put(".jpg", "image/jpeg");
        map.put(".jpeg", "image/jpeg");
        map.put(".jpe", "image/jpeg");
        map.put(".png", "image/png");
        map.put(".gif", "image/gif");
        map.put(".bmp", "image/bmp");
        map.put(".book", "application/vnd.framemaker");
        map.put(".pdf", "application/pdf");
        map.put(".bz", "application/x-bzip");
        map.put(".bz2", "application/x-bzip2");
        map.put(".gz", "application/x-gzip");
        map.put(".zip", "application/zip");
        map.put(".7z", "application/x-7z-compressed");
        map.put(".rar", "application/x-rar-compressed");
        map.put(".swf", "application/x-shockwave-flash");
        map.put(".svg", "image/svg+xml");
        map.put(".svgz", "image/svg+xml");
        map.put(".doc", "application/msword");
        map.put(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        map.put(".xls", "application/vnd.ms-excel");
        map.put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        map.put(".ppt", "application/vnd.ms-powerpoint");
        map.put(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        map.put(".txt", "text/plain");
        map.put(".htm", "text/html");
        map.put(".html", "text/html");
        map.put(".flv", "video/x-flv");
        map.put(".f4v", "video/x-f4v");
        map.put(".jpgm", "video/jpm");
        map.put(".jpgv", "video/jpeg");
        map.put(".csv", "text/csv");
        map.put(".css", "text/css");
        map.put(".js", "application/javascript");
        map.put(".json", "application/json");
        map.put(".jsonml", "application/jsonml+json");
        map.put(".lnk", "application/x-ms-shortcut");
        map.put(".log", "text/plain");
        map.put(".m13", "application/x-msmediaview");
        map.put(".m14", "application/x-msmediaview");
        map.put(".m1v", "video/mpeg");
        map.put(".m21", "application/mp21");
        map.put(".3gp", "video/3gpp");
        map.put(".3g2", "video/3gpp2");
        map.put(".m2a", "audio/mpeg");
        map.put(".m2v", "video/mpeg");
        map.put(".m3a", "audio/mpeg");
        map.put(".m3u", "audio/x-mpegurl");
        map.put(".m3u8", "application/vnd.apple.mpegurl");
        map.put(".m4a", "audio/mp4");
        map.put(".m4b", "audio/mp4");
        map.put(".m4v", "audio/mp4");
        map.put(".m4r", "audio/mp4");
        map.put(".m4u", "video/vnd.mpegurl");
        map.put(".mov", "video/quicktime");
        map.put(".movie", "video/x-sgi-movie");
        map.put(".mp1", "audio/mpeg");
        map.put(".mp2", "audio/mpeg");
        map.put(".mp21", "application/mp21");
        map.put(".mp2a", "audio/mpeg");
        map.put(".mp3", "audio/mpeg");
        map.put(".mp4", "audio/mpeg");
        map.put(".mp4a", "audio/mpeg");
        map.put(".mp4s", "application/mp4");
        map.put(".mp4v", "audio/mpeg");
        map.put(".mpa", "audio/mpeg");
        map.put(".mpe", "video/mpeg");
        map.put(".mpeg", "video/mpeg");
        map.put(".mpg", "video/mpeg");
        map.put(".mpg4", "video/mp4");
    }

    public static String getContentType(String suffix) {
        return map.get(suffix);
    }

}
