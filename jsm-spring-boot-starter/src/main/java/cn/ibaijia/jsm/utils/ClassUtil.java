package cn.ibaijia.jsm.utils;

import org.slf4j.Logger;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author longzl / @createOn 2011-10-27
 */
public class ClassUtil {
    private static Logger logger = LogUtil.log(ClassUtil.class);

    public static boolean hasClass(String clazzName) {
        try {
            return null != Class.forName(clazzName);
        } catch (Exception e) {
            return false;
        }
    }

    public static Set<Class<?>> loadClass(String packagePath) {
        return loadClass(packagePath, true);
    }

    public static Set<Class<?>> loadClass(String packagePath, boolean recursive) {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        String packageDir = packagePath.replace('.', '/');
        try {
            Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(packageDir);
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                if ("file".equals(url.getProtocol())) {
                    String filePath = URLDecoder.decode(url.getFile(), "utf-8");
                    logger.info("find class in dir: {}", filePath);
                    findClassesInFolder(filePath, packagePath, recursive, classes);
                } else if ("jar".equals(url.getProtocol())) {
                    JarURLConnection jarUrl = (JarURLConnection) url.openConnection();
                    JarFile jar = jarUrl.getJarFile();
                    logger.info("find class in dir: {}", jar.getName());
                    findClassesInJar(jar, packageDir, recursive, classes);
                }
            }
        } catch (IOException e) {
            logger.error("scan package: {} error!", packagePath, e);
        }
        return classes;
    }

    private static void findClassesInJar(JarFile jar, String packageDir, boolean recursive, Set<Class<?>> classes) {
        Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            String name = entry.getName();
            if (name.charAt(0) == '/') {
                name = name.substring(1);
            }
            if (name.startsWith(packageDir)) {
                if (recursive || !name.substring(packageDir.length() + 1).contains("/")) {
                    if (name.endsWith(".class") && !entry.isDirectory()) {
                        String fullClassName = name.substring(0, name.length() - 6).replace('/', '.');
                        try {
                            classes.add(Thread.currentThread().getContextClassLoader().loadClass(fullClassName));
                        } catch (Exception e) {
                            logger.error("class not found {}", fullClassName, e);
                        }
                    }
                }
            }
        }
    }

    /**
     * 以文件的形式来获取包下的所有Class
     */
    private static void findClassesInFolder(String packageDir,
                                            String packagePath, final boolean recursive, Set<Class<?>> classes) {
        File dir = new File(packageDir);
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        File[] dirfiles = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        for (File file : dirfiles) {
            // 如果是目录 则继续扫描
            if (file.isDirectory()) {
                findClassesInFolder(packageDir + "/" + file.getName(), packagePath + '.' + file.getName(), recursive, classes);
            } else {
                // 如果是java类文件 去掉后面的.class 只留下类名
                String className = file.getName().substring(0, file.getName().length() - 6);
                String fullClassName = packagePath + '.' + className;
                try {
                    classes.add(Thread.currentThread().getContextClassLoader().loadClass(fullClassName));
                } catch (ClassNotFoundException e) {
                    logger.error("class not found {}", fullClassName, e);
                }
            }
        }
    }

    public static void main(String[] args) {
        String pakg = "org.apache.commons.beanutils";
        for (Class<?> clazz : ClassUtil.loadClass(pakg, false)) {
            System.out.println(clazz.getName());
        }
    }
}
