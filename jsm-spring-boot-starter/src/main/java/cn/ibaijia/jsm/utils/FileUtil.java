package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.exception.BaseException;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.status.StatusLogger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author longzl / @createOn 2010-9-3
 */
public class FileUtil {
    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();

    public static InputStream getResourceStream(String filename) {
        try {
            ClassPathResource resource = new ClassPathResource(filename);
            return resource.getInputStream();
        } catch (Exception e) {
            logger.warn("file {} not found", filename);
            throw new BaseException("file not found：" + filename, e);
        }
    }

    public static File getResource(String filename) {
        try {
            return ResourceUtils.getFile("classpath:" + filename);
        } catch (FileNotFoundException e) {
            logger.warn("file {} not found", filename);
            throw new BaseException("file not found：" + filename, e);
        }
    }

    public static File createNewFile(String filePath) {
        filePath = filePath.replace('\\', '/').trim();
        new File(filePath.substring(0, filePath.lastIndexOf('/'))).mkdirs();
        return new File(filePath);
    }

    public static File getFile(String filename) {
        return new File(filename);
    }

    public static boolean zip(String filePath, String zipFileName) {
        logger.info("zip dir:{} to {}", filePath, zipFileName);
        //创建zip输出流
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipFileName));
            File sourceFile = new File(filePath);
            compress(out, sourceFile, null);
        } catch (Exception e) {
            logger.error("zip error!", e);
            return false;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    logger.error("zip close out error!", e);
                }
            }
        }
        return true;
    }

    private static void compress(ZipOutputStream out, File sourceFile, String pathInZip) throws Exception {
        String fileName = sourceFile.getName();
        if (!StringUtil.isEmpty(pathInZip)) {
            fileName = pathInZip + "/" + sourceFile.getName();
        }
        if (sourceFile.isDirectory()) {
            File[] flist = sourceFile.listFiles();
            if (flist.length == 0) {
                out.putNextEntry(new ZipEntry(fileName + "/"));
            } else {
                for (File file : flist) {
                    compress(out, file, fileName);
                }
            }
        } else {
            out.putNextEntry(new ZipEntry(fileName));
            FileInputStream fos = null;
            BufferedInputStream bis = null;
            try {
                fos = new FileInputStream(sourceFile);
                bis = new BufferedInputStream(fos);
                int len;
                byte[] buffer = new byte[1024];
                while ((len = bis.read(buffer)) != -1) {
                    out.write(buffer, 0, len);
                    out.flush();
                }
            } catch (Exception e) {
                logger.error("compress error!", e);
            } finally {
                if (bis != null) {
                    bis.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }

    public static String readFileAsText(String filePath) {
        return readFileAsText(new File(filePath));
    }

    public static String readFileAsText(File file) {
        StringBuilder sb = new StringBuilder();
        try {
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\r\n");
            }
        } catch (IOException e) {
            logger.error("readFileAsText error.", e);
        }
        return sb.toString();
    }

    public static String readFileAsText(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        try {
            IOUtils.readLines(inputStream, Charset.defaultCharset()).forEach(line -> sb.append(line));
        } catch (IOException e) {
            logger.error("readFileAsText error.", e);
        }
        return sb.toString();
    }
}
