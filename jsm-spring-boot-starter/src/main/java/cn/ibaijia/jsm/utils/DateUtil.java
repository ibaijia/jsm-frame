package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.exception.BaseException;
import org.apache.logging.log4j.status.StatusLogger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author longzl / @createOn 2010-8-25
 */
public class DateUtil {
    private static org.apache.logging.log4j.Logger logger = StatusLogger.getLogger();

    public static long TIME_DIFF = 0;

    /**
     * 1000
     */
    public static final long MILLIS_PER_SECOND = 1000;

    /**
     * 60×1000
     */
    public static final long MILLIS_PER_MINUTE = 60 * MILLIS_PER_SECOND;

    /**
     * 60×60×1000
     */
    public static final long MILLIS_PER_HOUR = 60 * MILLIS_PER_MINUTE;

    /**
     * 24×60×60×1000
     */
    public static final long MILLIS_PER_DAY = 24 * MILLIS_PER_HOUR;

    public static final String SLASH_DATE_PATTERN = "yyyy/MM/dd";

    public static final String SLASH_TRIM_SECOND_PATTERN = "yyyy/MM/dd HH:mm";

    public static final String SLASH_DATETIME_PATTERN = "yyyy/MM/dd HH:mm:ss";

    public static final String COMPACT_DATE_PATTERN = "yyyyMMdd";

    public static final String COMPACT_TRIM_SECOND_PATTERN = "yyyyMMdd HH:mm";

    public static final String COMPACT_DATETIME_PATTERN = "yyyyMMdd HH:mm:ss";

    public static final String COMPACT_PATTERN = "yyyyMMddHHmmss";

    public static final String DATE_PATTERN = "yyyy-MM-dd";

    public static final String TRIM_SECOND_PATTERN = "yyyy-MM-dd HH:mm";

    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private static final String EL = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";

    private static final long SECONDS_LENGTH = 10;
    private static final long MILLIS_LENGTH = 13;
    private static final int THREE = 3;
    private static final int TWO = 2;
    private static final int ONE = 1;

    private static SimpleDateFormat getAdf() {
        SimpleDateFormat sdf = ThreadLocalUtil.SDF_TL.get();
        if (sdf == null) {
            sdf = new SimpleDateFormat();
            ThreadLocalUtil.SDF_TL.set(sdf);
        }
        return sdf;
    }

    public static Date currentDate() {
        Date date = new Date();
        if (TIME_DIFF == 0) {
            return date;
        }
        return new Date(date.getTime() + TIME_DIFF);
    }

    public static String currentDateStr() {
        return currentDateStr(DateUtil.COMPACT_DATETIME_PATTERN);
    }

    public static String currentDateStr(String pattern) {
        Date date = currentDate();
        return format(date, pattern);
    }

    public static Long currentTime() {
        return currentDate().getTime();
    }

    public static Long currentTimeSecond() {
        return currentTime() / MILLIS_PER_SECOND;
    }

    public static Long toTime(Date date) {
        return date.getTime();
    }

    public static Long toTimeSecond(Date date) {
        return date.getTime() / MILLIS_PER_SECOND;
    }

    public static Date fromTime(String millisOrSeconds) {
        if (StringUtil.isEmpty(millisOrSeconds) || !StringUtil.isNumber(millisOrSeconds)) {
            logger.error("empty or non-number string:{}", millisOrSeconds);
            return null;
        }
        //秒
        if (millisOrSeconds.length() == SECONDS_LENGTH) {
            return fromTimeSecond(StringUtil.toLong(millisOrSeconds));
        }
        //毫秒
        if (millisOrSeconds.length() == MILLIS_LENGTH) {
            return fromTime(StringUtil.toLong(millisOrSeconds));
        }
        return null;
    }

    public static Date fromTime(Long millis) {
        return new Date(millis);
    }

    public static Date fromTimeSecond(Long second) {
        return new Date(second * MILLIS_PER_SECOND);
    }


    public static String format(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        getAdf().applyPattern(pattern);
        return getAdf().format(date);
    }

    public static Date parse(String strDate, String pattern) {
        getAdf().applyPattern(pattern);
        try {
            return getAdf().parse(strDate);
        } catch (ParseException e) {
            logger.error("parse error!", e);
        }
        return null;
    }

    public static Date parse(String strDate) {
        if (StringUtil.isEmpty(strDate)) {
            return null;
        }
        return parse(strDate, getPattern(strDate));
    }

    /**
     * @param strDate
     */
    public static boolean isDate(String strDate) {
        return Pattern.compile(EL).matcher(strDate).matches();
    }

    public static String getPattern(String strDate) {
        String[] arr = strDate.split(" ");
        if (arr.length == ONE) {
            if (arr[0].split(BaseConstants.SYSTEM_SYMBOL_MIDDLE_LINE).length == THREE) {
                return DateUtil.DATE_PATTERN;
            } else {
                return DateUtil.COMPACT_DATE_PATTERN;
            }
        } else if (arr.length == TWO) {
            if (arr[0].split(BaseConstants.SYSTEM_SYMBOL_MIDDLE_LINE).length == THREE) {
                if (arr[1].split(BaseConstants.SYSTEM_SYMBOL_COLON).length == TWO) {
                    return DateUtil.TRIM_SECOND_PATTERN;
                } else {
                    return DateUtil.DATETIME_PATTERN;
                }
            } else {
                if (arr[1].split(BaseConstants.SYSTEM_SYMBOL_COLON).length == TWO) {
                    return DateUtil.COMPACT_TRIM_SECOND_PATTERN;
                } else {
                    return DateUtil.COMPACT_DATETIME_PATTERN;
                }
            }

        } else {
            throw new BaseException("非法日期格式！");
        }
    }

    public static boolean oneDayAgo(Date date) {
        if (DateUtil.currentTime() - date.getTime() > MILLIS_PER_DAY) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean oneHourAgo(Date date) {
        if (DateUtil.currentTime() - date.getTime() > MILLIS_PER_HOUR) {
            return true;
        } else {
            return false;
        }
    }

    public static Calendar parseCalendar(String dateStr, String pattern) {
        Calendar c = Calendar.getInstance();
        c.setTime(parse(dateStr, pattern));
        return c;
    }

    public static Date parse(Long date) {
        if (date == null) {
            return null;
        }
        return new Date(date);
    }

    public static String format(Calendar c, String pattern) {
        if (c == null) {
            return null;
        }
        return new SimpleDateFormat(pattern).format(c.getTime());
    }

    public static String format(Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(DATE_PATTERN).format(date);
    }

    /**
     * @param date
     * @return 1 to 7, sunday is 1
     */
    public static int dayInWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * @param date
     * @return 1 to 7 MONDAY is 1
     */
    public static int dayInWeekForUse(Date date) {
        int res = dayInWeek(date) - 1;
        return res == 0 ? 7 : res;
    }

    public static int dayInMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static Date addDay(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, amount);
        return c.getTime();
    }

    public static Date addMinute(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, amount);
        return c.getTime();
    }

    public static Date addSecond(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.SECOND, amount);
        return c.getTime();
    }

    public static Date addMilliSecond(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MILLISECOND, amount);
        return c.getTime();
    }

    public static boolean isSameDay(Date date, Date other) {
        return format(date).equals(format(other));
    }

    public static long reduce(Date startDate, Date endDate, long timeType) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(startDate);
        c2.setTime(endDate);
        long result = c1.getTimeInMillis() - c2.getTimeInMillis();
        return result / timeType;
    }

    public static Date dayOfEnd(Date date) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        c1.set(Calendar.HOUR_OF_DAY, 23);
        c1.set(Calendar.MINUTE, 59);
        c1.set(Calendar.SECOND, 59);
        c1.set(Calendar.MILLISECOND, 0);
        return c1.getTime();
    }

    public static Date dayOfBegin(Date date) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        c1.set(Calendar.HOUR_OF_DAY, 0);
        c1.set(Calendar.MINUTE, 0);
        c1.set(Calendar.SECOND, 0);
        c1.set(Calendar.MILLISECOND, 0);
        return c1.getTime();
    }

    public static Date addMonth(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, amount);
        return c.getTime();
    }

    public static Date setDay(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.DAY_OF_MONTH, amount);
        return c.getTime();
    }

    public static Date setMonth(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MONTH, amount);
        return c.getTime();
    }

    public static Date setYear(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.YEAR, amount);
        return c.getTime();
    }

    public static int getYear(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.YEAR);
    }

    public static int getMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MONTH);
    }

}
