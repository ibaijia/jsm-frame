package cn.ibaijia.jsm.utils;

import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.gen.model.FieldInfo;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.exception.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.*;

/**
 * @author longzl
 */
public class JsmFrameUtil {

    private static Logger logger = LogUtil.log(JsmFrameUtil.class);

    private static ScriptEngineManager sem;
    private static ScriptEngine se;

    public static String genMockResult(Method method) {
        String mockJsPath = "META-INF/js/mock.js";
        try {
            InputStream inputStream = JsmFrameUtil.class.getClassLoader().getResourceAsStream(mockJsPath);
            String mockJs = FileUtil.readFileAsText(inputStream);
            if (sem == null) {
                sem = new ScriptEngineManager();
                se = sem.getEngineByName("javascript");
                se.eval(mockJs);
            }
            String jsonStr = JsmFrameUtil.getMockJsonStr(method);
            logger.debug("mockJsStr:{}", jsonStr);
            //处理'@ * '
            jsonStr = jsonStr.replaceAll("\\\"@(.*?)\\\"", "Mock.Random.$1");
            se.eval("var res = Mock.mock(" + jsonStr + ");");
            se.eval("res = JSON.stringify(res);");
            String res = (String) se.get("res");
            return res;
        } catch (Exception e) {
            logger.error("genMockResult error.", e);
            return null;
        }
    }

    public static RestResp dealException(Exception e) {
        RestResp baseResp = new RestResp();
        if (e instanceof BaseException) {
            baseResp.setPair(((BaseException) e).getErrorPair());
            baseResp.message = ((BaseException) e).getMsg();
            if (e instanceof NotFoundException) {
                WebContext.getResponse().setStatus(404);
            } else if ((e instanceof NoLoginException) || (e instanceof AuthFailException)) {
                //Unauthorized
                WebContext.getResponse().setStatus(401);
            } else if (e instanceof NoPermissionException) {
                //Forbidden
                WebContext.getResponse().setStatus(403);
            } else if (e instanceof InfoException) {
                //提示信息类异常 返回200
                WebContext.getResponse().setStatus(200);
            } else {
                //Bad Request
                WebContext.getResponse().setStatus(400);
            }
        } else if (e instanceof NoHandlerFoundException) {
            logger.error("NoHandlerFoundException error.");
            baseResp.result = e.getMessage();
            baseResp.setPair(BasePairConstants.NOT_FOUND);
            WebContext.getResponse().setStatus(404);
        } else if (e instanceof HttpRequestMethodNotSupportedException) {
            logger.error("HttpRequestMethodNotSupportedException error.");
            baseResp.result = e.getMessage();
            baseResp.setPair(BasePairConstants.NOT_FOUND);
            WebContext.getResponse().setStatus(404);
        } else {
            WebContext.getResponse().setStatus(500);
            logger.error("unknown exception.", e);
            baseResp.setPair(BasePairConstants.ERROR);
            if (AppContext.isDevModel()) {
                baseResp.message = baseResp.message + "[" + e.getMessage() + "]";
            }
        }
        WebContext.getRequest().setAttribute(WebContext.JSM_RESP_CODE, baseResp.code);
        logger.info("resp:" + JsonUtil.toJsonString(baseResp));
        return baseResp;
    }

    public static String getMockJsonStr(Method method) {
        Map<String, Object> mapObject = new TreeMap<>();
        List<FieldInfo> fieldInfoList = JsmFrameUtil.getFieldInfoList(method.getReturnType(), JsmFrameUtil.getGenericNames(method.getGenericReturnType()), null, 0, true);
        for (FieldInfo fieldInfo : fieldInfoList) {
            String key = fieldInfo.fieldName;
            key = JsonUtil.getStrategyName(key);
            String mockRule = fieldInfo.fieldAnn.mockRule();
            if (!StringUtil.isEmpty(mockRule)) {
                key = key + "|" + mockRule;
            }
            Object value = fieldInfo.fieldAnn.mockValue();
            if (StringUtil.isEmpty(value)) {
                value = getMockJson(fieldInfo);
            } else {
                if (!ReflectionUtil.isPlanType(fieldInfo.fieldType) || fieldInfo.dicAnn != null || !StringUtil.isEmpty(mockRule)) {
                    if (JsonUtil.isJsonString((String) value)) {
                        value = JsonUtil.readTree((String) value);
                    }
                }
                if (ReflectionUtil.isNumberType(fieldInfo.fieldType)) {
                    value = ReflectionUtil.parseByFieldType(value.toString(),fieldInfo.fieldType);
                }
            }
            mapObject.put(key, value);
        }
        return JsonUtil.toJsonString(mapObject);
    }

    private static Object getMockJson(FieldInfo fieldInfo) {
        Object value = fieldInfo.fieldAnn.mockValue();
        if (fieldInfo.dicAnn != null) {
            if (fieldInfo.fieldInfoList != null) {
                return getMockObject(fieldInfo.fieldInfoList);
            } else {
                return "1";
            }
        }
        int minLength = fieldInfo.fieldAnn.minLen() == -1 ? 0 : fieldInfo.fieldAnn.minLen();
        int maxLength = fieldInfo.fieldAnn.maxLen() == -1 ? 250 : fieldInfo.fieldAnn.maxLen();
        if (ReflectionUtil.isPlanType(fieldInfo.fieldType)) {
            value = getPlanTypeVal(fieldInfo.fieldType, minLength, maxLength);
        } else {
            if (fieldInfo.fieldInfoList == null) {
                logger.error("field name:{} field type:{} maybe not implement ValidateModel.", fieldInfo.fieldName, fieldInfo.fieldType);
                return value;
            }
            if (ReflectionUtil.isListType(fieldInfo.fieldType)) {
                String[] fieldTypeArr = fieldInfo.fieldType.split("\\|");
                //普通类型list 或者 List|String
                if (fieldInfo.fieldInfoList.isEmpty()) {
                    List list = new ArrayList();
                    String planType = "Integer";
                    if (fieldTypeArr.length > 1) {
                        planType = fieldTypeArr[1];
                    }
                    list.add(getPlanTypeVal(planType, minLength, maxLength));
                    value = list;
                } else {
                    List lastList = null;
                    for (int i = 0; i < fieldTypeArr.length - 1; i++) {
                        List list = new ArrayList();
                        lastList = list;
                        if (i == 0) {
                            value = list;
                        } else {
                            ((List) value).add(list);
                        }
                    }
                    lastList.add(getMockObject(fieldInfo.fieldInfoList));
                }
            } else {
                value = getMockObject(fieldInfo.fieldInfoList);
            }
        }
        return value;
    }

    private static Map<String, Object> getMockObject(List<FieldInfo> fieldInfoList) {
        Map<String, Object> mapObject = new TreeMap<>();
        for (FieldInfo fieldInfo : fieldInfoList) {
            String key = fieldInfo.fieldName;
            key = JsonUtil.getStrategyName(key);
            //Dic toKV时 kv是没有fieldAnn
            if (fieldInfo.fieldAnn == null) {
                mapObject.put(key, getPlanTypeVal(fieldInfo.fieldType, 1, 4));
            } else {
                String mockRule = fieldInfo.fieldAnn.mockRule();
                if (!StringUtil.isEmpty(mockRule)) {
                    key = key + "|" + mockRule;
                }
                Object value = fieldInfo.fieldAnn.mockValue();
                if (StringUtil.isEmpty(value)) {
                    value = getMockJson(fieldInfo);
                } else {
                    if (!ReflectionUtil.isPlanType(fieldInfo.fieldType) || fieldInfo.dicAnn != null || !StringUtil.isEmpty(mockRule)) {
                        if (JsonUtil.isJsonString((String) value)) {
                            value = JsonUtil.readTree((String) value);
                        }
                    }
                    if (ReflectionUtil.isNumberType(fieldInfo.fieldType)) {
                        value = ReflectionUtil.parseByFieldType(value.toString(),fieldInfo.fieldType);
                    }
                }
                mapObject.put(key, value);
            }
        }
        return mapObject;
    }

    private static Class createClazz(String typeName) {
        if (typeName == null || isGenericTypeName(typeName)) {
            return null;
        }
        try {
            return Class.forName(typeName.split("<")[0].replace("[]", ""));
        } catch (ClassNotFoundException e) {
            logger.error("", e);
        }
        return null;
    }

    private static boolean isGenericTypeName(String typeName) {
        return typeName.length() == 1;
    }

    private static String getPlanTypeVal(String fieldType, int minLength, int maxLength) {
        String value = null;
        if (ReflectionUtil.isString(fieldType)) {
            value = String.format("@string(%s, %s)", minLength, maxLength);
        } else if (ReflectionUtil.isNumberType(fieldType)) {
            value = String.format("@integer(0,7)");
        } else if (ReflectionUtil.isDate(fieldType)) {
            value = String.format("@datetime()");
        } else if (ReflectionUtil.isBoolean(fieldType)) {
            value = String.format("@boolean");
        }
        return value;
    }

    public static Type[] getGenericTypes(Type type) {
        if (type instanceof ParameterizedType) {
            return ((ParameterizedType) type).getActualTypeArguments();
        }
        return null;
    }

    public static String getGenericNames(Type type) {
        Type[] genericTypes = getGenericTypes(type);
        if (genericTypes != null && genericTypes.length > 0) {
            return genericTypes[0].getTypeName();
        }
        return null;
    }

    /**
     * @param clazz
     * @param genericTypeName
     * @param parentClazzList
     * @param level
     * @param withFieldAnn    是否返回 字段的FieldAnn对象,MockJs会返回 其它场景不需要
     * @return
     */
    public static List<FieldInfo> getFieldInfoList(Class<?> clazz, String genericTypeName, List<String> parentClazzList, int level, boolean withFieldAnn) {
        if (clazz == null) {
            return null;
        }
        if (parentClazzList == null) {
            parentClazzList = new ArrayList<>();
        }
        if (parentClazzList.contains(clazz.getName()) && ValidateModel.class.isAssignableFrom(clazz)) {
            logger.warn("cycle class:{}", clazz.getName());
            return null;
        }
        try {
            parentClazzList.add(clazz.getName());
            Field[] fields = clazz.getFields();
            if (fields.length == 0 && genericTypeName != null) {
                return getFieldInfoList(createClazz(genericTypeName), createRemainGeneric(genericTypeName), parentClazzList, level, withFieldAnn);
            } else {
                return getFieldInfoList(genericTypeName, parentClazzList, level, withFieldAnn, fields);
            }
        } catch (Exception e) {
            logger.error("getFieldInfoList error.", e);
            return null;
        } finally {
            logger.debug("remove:{}", clazz.getName());
            parentClazzList.remove(clazz.getName());
        }
    }

    private static List<FieldInfo> getFieldInfoList(String genericTypeName, List<String> parentClazzList, int level, boolean withFieldAnn, Field[] fields) {
        List<FieldInfo> list = new ArrayList<>();
        for (Field field : fields) {
            FieldAnn fieldAnn = field.getAnnotation(FieldAnn.class);
            DicAnn dicAnn = field.getAnnotation(DicAnn.class);
            if (fieldAnn == null) {
                continue;
            }
            FieldInfo fieldInfo = initFieldInfo(level, withFieldAnn, field, fieldAnn, dicAnn);
            if (!ReflectionUtil.isPlanType(field.getType())) {
                if ((Collection.class.isAssignableFrom(field.getType())) || field.getType().isArray()) {
                    Type type = field.getGenericType();
                    int levelList = level + 1;
                    while (type != null) {
                        Type[] genericClazzList1 = getGenericTypes(type);
                        if (genericClazzList1 == null) {
                            break;
                        }
                        type = genericClazzList1[0];
                        if (type instanceof TypeVariable && genericTypeName != null) {
                            type = createClazz(genericTypeName);
                            break;
                        } else if (type instanceof ParameterizedType) {
                            String typeName = ((ParameterizedType) type).getRawType().getTypeName();
                            fieldInfo.fieldType = String.format("%s|%s", fieldInfo.fieldType, typeName.substring(typeName.lastIndexOf(".") + 1));
                            levelList++;
                        } else {
                            break;
                        }
                    }
                    if (type != null) {
                        Class<?> clazz1 = createClazz(type.getTypeName());
                        if (clazz1 != null) {
                            fieldInfo.fieldType = field.getType().getSimpleName() + "|" + clazz1.getSimpleName();
                            if (dicAnn != null) {
                                if (dicAnn.toDicKV()) {
                                    fieldInfo.fieldType = field.getType().getSimpleName() + "|" + DicAnn.class.getSimpleName();
                                    fieldInfo.fieldInfoList = createDicFieldList(dicAnn);
                                }
                            } else {
                                fieldInfo.fieldInfoList = getFieldInfoList(clazz1, parentClazzList, levelList, withFieldAnn);
                            }
                        }
                    }
                } else if (ValidateModel.class.isAssignableFrom(field.getType())) {
                    fieldInfo.fieldInfoList = getFieldInfoList(field.getGenericType(), parentClazzList, level + 1, withFieldAnn);
                } else {
                    Type type = field.getGenericType();
                    if (type instanceof TypeVariable) {
                        if (genericTypeName != null) {
                            Class clazz1 = createClazz(genericTypeName);
                            fieldInfo.fieldType = getGenericFieldType(genericTypeName);
                            fieldInfo.fieldInfoList = getFieldInfoList(clazz1, createRemainGeneric(genericTypeName), parentClazzList, level + 1, withFieldAnn);
                            genericTypeName = null;
                        }
                    } else {
                        logger.warn("ignore field info:{}", JsonUtil.toJsonString(fieldInfo.fieldName));
                    }
                }
            }
            if (genericTypeName == null) {
                Type[] genericTypes = getGenericTypes(field.getDeclaringClass().getGenericSuperclass());
                if (genericTypes != null && genericTypes.length > 0) {
                    genericTypeName = genericTypes[0].getTypeName();
                }
            }
            if (notExists(fieldInfo, list)) {
                list.add(fieldInfo);
            }
        }
        return list;
    }

    private static boolean notExists(FieldInfo fieldInfo, List<FieldInfo> list) {
        for (FieldInfo f : list) {
            if (f.fieldName.equals(fieldInfo.fieldName)) {
                return false;
            }
        }
        return true;
    }

    private static FieldInfo initFieldInfo(int level, boolean withFieldAnn, Field field, FieldAnn fieldAnn, DicAnn dicAnn) {
        FieldInfo fieldInfo = new FieldInfo();
        fieldInfo.fieldAnn = withFieldAnn ? fieldAnn : null;
        fieldInfo.fieldType = field.getType().getSimpleName();
        fieldInfo.fieldName = field.getName();
        fieldInfo.required = fieldAnn.required();
        fieldInfo.comments = fieldAnn.comments();
        if (dicAnn != null) {
            fieldInfo.dicAnn = dicAnn;
            fieldInfo.fieldType = String.class.getSimpleName();
            fieldInfo.comments = "[字典值]" + fieldInfo.comments;
            if (dicAnn.toDicKV()) {
                fieldInfo.fieldType = DicAnn.class.getSimpleName();
                fieldInfo.fieldInfoList = createDicFieldList(dicAnn);
            }
        }
        fieldInfo.level = level;
        return fieldInfo;
    }

    private static String getGenericFieldType(String genericTypeName) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            Class clazz1 = createClazz(genericTypeName);
            sb.append(clazz1.getSimpleName());
            genericTypeName = createRemainGeneric(genericTypeName);
            if (genericTypeName == null) {
                break;
            } else {
                sb.append("|");
            }
        }
        return sb.toString();
    }

    private static List<FieldInfo> createDicFieldList(DicAnn dicAnn) {
        List<FieldInfo> list = new ArrayList<>();
        FieldInfo keyField = new FieldInfo();
        keyField.fieldType = String.class.getSimpleName();
        keyField.fieldName = dicAnn.dicKeyName();
        keyField.comments = dicAnn.dicKeyNameComment();
        list.add(keyField);
        FieldInfo valueField = new FieldInfo();
        valueField.fieldType = String.class.getSimpleName();
        valueField.fieldName = dicAnn.dicValueName();
        valueField.comments = dicAnn.dicValueNameComment();
        list.add(valueField);
        return list;
    }

    private static String createRemainGeneric(String typeName) {
        int beginIdx = typeName.indexOf("<");
        int endIdx = typeName.lastIndexOf(">");
        if (beginIdx == -1) {
            return null;
        }
        return typeName.substring(beginIdx + 1, endIdx);
    }

    public static List<FieldInfo> getFieldInfoList(Type type, List<String> parentClazzList, int level, boolean withFieldAnn) {
        if (parentClazzList == null) {
            parentClazzList = new ArrayList<>();
        }
        if (type instanceof ParameterizedType) {
            Class clazz = createClazz(((ParameterizedType) type).getRawType().getTypeName());
            String genericName = null;
            Type[] genericTypes = ((ParameterizedType) type).getActualTypeArguments();
            if (genericTypes != null && genericTypes.length > 0) {
                genericName = genericTypes[0].getTypeName();
            }
            return getFieldInfoList(clazz, genericName, parentClazzList, level, withFieldAnn);
        } else {
            Class clazz = createClazz(type.getTypeName());
            return getFieldInfoList(clazz, null, parentClazzList, level, withFieldAnn);
        }
    }

    public static String getKey(String prefix, String key, ProceedingJoinPoint jpt) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        if (StringUtil.isEmpty(key)) {
            sb.append(jpt.toString());
            for (Object o : jpt.getArgs()) {
                if (o == null) {
                    o = "";
                }
                sb.append(":").append(o.hashCode());
            }
        } else {
            sb.append(key);
        }
        return sb.toString();
    }

    public static String getKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:cache:", key, jpt);
    }

    public static String getOrderCheckKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:order:check:", key, jpt);
    }

    public static String getJobLockKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:job:lock:", key, jpt);
    }

    public static String getLockKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:lock:", key, jpt);
    }

    public static String getFuseCountKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:fuse:count:", key, jpt);
    }

    public static String getFuseKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:fuse:", key, jpt);
    }

    public static String getLimitKey(String key, ProceedingJoinPoint jpt) {
        return getKey("jsm:limit:", key, jpt);
    }

}