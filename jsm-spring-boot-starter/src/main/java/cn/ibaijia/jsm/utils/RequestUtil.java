package cn.ibaijia.jsm.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author longzl
 */
public class RequestUtil {

    public static String get(HttpServletRequest request, String key) {
        String val = getHeader(request, key);
        if (StringUtil.isEmpty(val)) {
            val = getParameter(request, key);
        }
        if (StringUtil.isEmpty(val)) {
            val = getCookie(request, key);
        }
        return val;
    }

    public static String getHeader(HttpServletRequest request, String key) {
        return request.getHeader(key);
    }


    public static Map<String,String> getHeadersByPrefix(HttpServletRequest request,String prefix) {
        Map<String,String> map = new HashMap<>(16);
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if(!StringUtil.isEmpty(prefix)){
                if(headerName.startsWith(prefix)){
                    String newName = headerName.replaceFirst(prefix,"");
                    map.put(newName,request.getHeader(headerName));
                }
            } else {
                map.put(headerName,request.getHeader(headerName));
            }
        }
        return map;
    }


    public static String getParameter(HttpServletRequest request, String key) {
        return request.getParameter(key);
    }

    public static String getCookie(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (key.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

}
