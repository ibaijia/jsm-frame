package cn.ibaijia.jsm.gen.model;

import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.gen.GenUtil;

/**
 * @author longzl
 */
public class TsFieldInfo {

    public String fieldName;
    public String fieldType;
    public String defaultVal;
    public String comments;
    public Boolean required = false;

    public Integer maxLength = 250;
    /**
     * 字段的 注解
     */
    public FieldAnn fieldAnn;
    /**
     * 字段的 字典 注解
     */
    public DicAnn dicAnn;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public String getTsFieldType() {
        return GenUtil.getTsFieldType(this.fieldType, this.dicAnn);
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getDefaultVal() {
        return defaultVal;
    }

    public void setDefaultVal(String defaultVal) {
        this.defaultVal = defaultVal;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }
}
