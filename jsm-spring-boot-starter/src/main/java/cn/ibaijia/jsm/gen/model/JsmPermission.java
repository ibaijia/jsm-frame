package cn.ibaijia.jsm.gen.model;

import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @Author: LongZL
 * @Date: 2022/8/20 12:07
 */
public class JsmPermission {

    @FieldAnn(required = false, maxLen = 50, comments = "名称")
    public String name;

    @FieldAnn(required = false, maxLen = 50, comments = "代码")
    public String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
