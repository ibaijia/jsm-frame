package cn.ibaijia.jsm.gen.model;
/**
 * @author longzl
 */
public class CreateTableInfo {

	public CreateTableInfo() {
	}

	public CreateTableInfo(String table, String ddl) {
		this.table = table;
		this.ddl = ddl;
	}

	public String table;
	public String ddl;
	
}
