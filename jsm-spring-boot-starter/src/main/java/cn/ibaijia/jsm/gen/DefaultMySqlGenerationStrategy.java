package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.gen.model.CreateTableInfo;
import cn.ibaijia.jsm.gen.model.FieldInfo;
import cn.ibaijia.jsm.gen.model.JsmPermission;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2021/11/4 15:19
 */
public class DefaultMySqlGenerationStrategy implements DbGenerationStrategy {

    private static Logger logger = LogUtil.log(DefaultMySqlGenerationStrategy.class);

    @Override
    public List<String> getTables() {
        return DbUtil.showTables();
    }

    @Override
    public List<FieldInfo> getTableFieldInfo(String tableName, Map<String, Object> contextData) {
        logger.info("getTableFieldInfo:{}", tableName);
        List<FieldInfo> list = new ArrayList<FieldInfo>();
        List<CreateTableInfo> res = DbUtil.showCreateTable(tableName);
        if (res.size() > 0) {
            String createInfo = res.get(0).ddl;
            logger.info(createInfo);
            createInfo = createInfo.substring(createInfo.indexOf("(") + 2, createInfo.lastIndexOf(")") - 1);
            logger.info(createInfo);
            String[] lineArr = createInfo.split("\\n");
            for (int i = 1; i < lineArr.length; i++) {
                String[] arr = lineArr[i].trim().replaceAll("\\s+", " ")
                        .replace("NOT NULL", "NOT_NULL").split(" ");
                if (!arr[0].startsWith("`")) {
                    logger.warn("ignore line:{}", lineArr[i]);
                    continue;
                }
                FieldInfo fieldInfo = new FieldInfo();
                fieldInfo.fieldName = arr[0].replace("`", "");
                fieldInfo.fieldName = columnNameToFieldName(fieldInfo.fieldName);
                if ("`id`".equals(fieldInfo.fieldName)) {
                    logger.warn("id line ignore:{}", lineArr[i]);
                    continue;
                }
                fieldInfo.fieldType = dbTypeToJavaType(arr[1]);
                if (lineArr[i].contains(" DEFAULT ") && arr.length >= 4) {
                    fieldInfo.defaultVal = arr[3];
                }
                if (lineArr[i].contains(" COMMENT ")) {
                    fieldInfo.comments = lineArr[i].substring(lineArr[i].indexOf("'") + 1);
                    fieldInfo.comments = fieldInfo.comments.substring(0, fieldInfo.comments.length() - 2);
                }
                if (lineArr[i].contains("NOT NULL")) {
                    fieldInfo.required = true;
                }
                if (arr[1].startsWith("varchar(")) {
                    fieldInfo.maxLength = StringUtil.extractInteger(arr[1]);
                }
                if ("Date".equals(fieldInfo.fieldType)) {
                    contextData.put("hasDate", true);
                }
                list.add(fieldInfo);
            }
        }
        return list;
    }

    @Override
    public List<JsmPermission> listPermissions() {
        return DbUtil.listPermissions();
    }

}
