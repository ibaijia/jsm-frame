package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.gen.model.*;
import cn.ibaijia.jsm.utils.*;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author longzl
 */
public class GenUtil {

    private static final Logger logger = LogUtil.log(GenUtil.class);


    public static boolean writeFile(String text, String filePath, boolean override) {
        File modelFile = new File(filePath);
        if (!override && modelFile.exists()) {
            logger.warn("file:{} exists, ignore! override:{}", filePath, override);
            return false;
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            fos.write(text.getBytes());
            return true;
        } catch (Exception e) {
            logger.error("", e);
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                logger.error("", e);
            }
        }
    }

    public static boolean checkIsInSpringBoot() {
        return true;
    }

    public static void makeDirs(String dirPath) {
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static String getClazzUri(Class clazz) {
        RequestMapping baseRequestMapping = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
        if (baseRequestMapping != null && baseRequestMapping.value().length > 0) {
            return baseRequestMapping.value()[0];
        }
        PostMapping postMapping = (PostMapping) clazz.getAnnotation(PostMapping.class);
        if (postMapping != null && postMapping.value().length > 0) {
            return postMapping.value()[0];
        }
        PutMapping putMapping = (PutMapping) clazz.getAnnotation(PutMapping.class);
        if (putMapping != null && putMapping.value().length > 0) {
            return putMapping.value()[0];
        }
        GetMapping getMapping = (GetMapping) clazz.getAnnotation(GetMapping.class);
        if (getMapping != null && getMapping.value().length > 0) {
            return getMapping.value()[0];
        }
        DeleteMapping deleteMapping = (DeleteMapping) clazz.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null && deleteMapping.value().length > 0) {
            return deleteMapping.value()[0];
        }
        return "";
    }

    public static String getMethodUri(Method method) {
        RequestMapping baseRequestMapping = method.getAnnotation(RequestMapping.class);
        if (baseRequestMapping != null && baseRequestMapping.value().length > 0) {
            return baseRequestMapping.value()[0];
        }
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        if (postMapping != null && postMapping.value().length > 0) {
            return postMapping.value()[0];
        }
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        if (putMapping != null && putMapping.value().length > 0) {
            return putMapping.value()[0];
        }
        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        if (getMapping != null && getMapping.value().length > 0) {
            return getMapping.value()[0];
        }
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null && deleteMapping.value().length > 0) {
            return deleteMapping.value()[0];
        }
        return "";
    }

    public static String getHttpMethodName(Method method) {
        RequestMapping baseRequestMapping = method.getAnnotation(RequestMapping.class);
        if (baseRequestMapping != null) {
            return baseRequestMapping.method()[0].name().toLowerCase();
        }
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        if (postMapping != null) {
            return "post";
        }
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        if (putMapping != null) {
            return "put";
        }
        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        if (getMapping != null) {
            return "get";
        }
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null) {
            return "delete";
        }
        return "";
    }

    public static boolean buildFtlFile(Map<String, Object> datas, String ftlName, String fileName, boolean override) {
        GenService genService = SpringContext.getBean(GenService.class);
        String str = FtlUtil.build(genService.getClass(), "/META-INF/ftl/", datas, ftlName);
        boolean res = writeFile(str, fileName, override);
        logger.info("create:{} res:{}", fileName, res);
        return res;
    }

    public static void makDirs(String path) {
        File dir = new File(path);
        if (!dir.exists()) {
            logger.info("mkdir:{}", path);
            dir.mkdirs();
        }
    }


    public static void updateMapper(String mapperStr, String mapperFileName) {
        String oldStr = FileUtil.readFileAsText(mapperFileName);

        String addReplace = "((\\s.*?)@Insert[^;]*?add\\([\\s\\S]*?\\);)";
        String addUpdate = "((\\s.*?)(int.*?)(add\\()(.*?)\\);)";
        String addBatchReplace = "((\\s.*?)@Insert[^;]*?addBatch\\([\\s\\S]*?\\);)";
        String addBatchUpdate = "((\\s.*?)(int.*?)(addBatch\\()(.*?)\\);)";
        String updateReplace = "((\\s.*?)@Update([^;]*?update\\()[\\s\\S]*?\\);)";
        String updateUpdate = "((\\s.*?)(int.*?)(update\\()(.*?)\\);)";
        String findByIdReplace = "((\\s.*?)@Select([^;]*?findById\\()[\\s\\S]*?\\);)";
        String findByIdUpdate = "((\\s.*?)(findById\\()(.*?)\\);)";
        String findByIdForUpdateReplace = "((\\s.*?)@Select([^;]*?findByIdForUpdate\\()[\\s\\S]*?\\);)";
        String findByIdForUpdateUpdate = "((\\s.*?)(findByIdForUpdate\\()(.*?)\\);)";
        String deleteByIdReplace = "((\\s.*?)@Delete([^;]*?deleteById\\()[\\s\\S]*?\\);)";
        String deleteByIdUpdate = "((\\s.*?)(deleteById\\()(.*?)\\);)";
        String listAllReplace = "((\\s.*?)@Select[^;]*?listAll\\([\\s\\S]*?\\);)";
        String listAllUpdate = "((\\s.*?)(listAll\\()(.*?)\\);)";

        String oldPackageName = "org.apache.ibatis.annotations.Param;";
        String newPackageName = "org.apache.ibatis.annotations.*;";

        StringBuilder newSb = new StringBuilder();

        //add method
        oldStr = replaceByRegex(mapperStr, oldStr, addReplace, addUpdate, newSb);
        //addBatch method
        oldStr = replaceByRegex(mapperStr, oldStr, addBatchReplace, addBatchUpdate, newSb);
        //update method
        oldStr = replaceByRegex(mapperStr, oldStr, updateReplace, updateUpdate, newSb);
        //findById method
        oldStr = replaceByRegex(mapperStr, oldStr, findByIdReplace, findByIdUpdate, newSb);
        //findByIdForUpdate method
        oldStr = replaceByRegex(mapperStr, oldStr, findByIdForUpdateReplace, findByIdForUpdateUpdate, newSb);
        //deleteById method
        oldStr = replaceByRegex(mapperStr, oldStr, deleteByIdReplace, deleteByIdUpdate, newSb);
        //listAll method
        oldStr = replaceByRegex(mapperStr, oldStr, listAllReplace, listAllUpdate, newSb);

        oldStr = oldStr.replace(oldPackageName, newPackageName);

        //有新方法
        if (newSb.length() > 0) {
            int idx = oldStr.indexOf("{");
            StringBuilder fileSb = new StringBuilder();
            fileSb.append(oldStr.substring(0, idx + 2));
            fileSb.append("\n\n\t").append(newSb);
            fileSb.append(oldStr.substring(idx + 2));
            oldStr = fileSb.toString();
        }

        boolean res = GenUtil.writeFile(oldStr, mapperFileName, true);
        logger.info("updateMapper:{} res:{}", mapperFileName, res);
    }

    public static String replaceByRegex(String mapperStr, String oldStr, String addReplace, String addUpdate, StringBuilder newSb) {
        String addReplaceStrOld = findElement(oldStr, addReplace);
        String addReplaceStrNew = findElement(mapperStr, addReplace);
        //原来没有生成
        if (addReplaceStrOld == null) {
            //判断原来否有方法
            String addUpdateStrOld = findElement(oldStr, addUpdate);
            if (addUpdateStrOld == null) {
                newSb.append(addReplaceStrNew).append("\n");
            } else {//replace
                oldStr = oldStr.replace(addUpdateStrOld, addReplaceStrNew);
            }
        } else {
            oldStr = oldStr.replace(addReplaceStrOld, addReplaceStrNew);
        }
        return oldStr;
    }

    public static String findElement(String content, String regex) {
        return findElement(content, regex, 1);
    }

    public static String findElement(String content, String regex, int idx) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        while (m.find()) {
            if (idx <= m.groupCount()) {
                return m.group(idx);
            } else {
                logger.error("idx:{} > groupCount:{}", idx, m.groupCount());
            }
        }
        return null;
    }

    public static void updateModel(String modelStr, String modelFileName) {
        String oldStr = FileUtil.readFileAsText(modelFileName);
        String newStr = modelStr.substring(modelStr.indexOf("serialVersionUID"));
        String[] fieldArr = newStr.split(";");
        if (fieldArr.length < 1) {
            return;
        }
        StringBuilder newSb = new StringBuilder();
        for (int i = 1; i < fieldArr.length; i++) {
            String fieldStr = fieldArr[i];
            String[] arr = fieldStr.split("public ");
            if (arr.length != 2) {
                continue;
            }
            String filedDesc = "public " + arr[1] + ";";
            if (!oldStr.contains(filedDesc)) {
                newSb.append(fieldStr).append(";");
            }
        }
        if (newSb.length() > 0) {
            oldStr = oldStr.substring(0, oldStr.lastIndexOf('}'));
            oldStr = oldStr + newSb.toString() + "\n}";
            //save model
            boolean res = GenUtil.writeFile(oldStr, modelFileName, true);
            logger.info("updateModel:{} res:{}", modelFileName, res);
        }
    }

    public static List<TsTypeInfo> getTsTypeInfoList(ControllerInfo controllerInfo) {
        List<TsTypeInfo> list = new ArrayList<>();
        for (ApiInfo apiInfo : controllerInfo.apiInfos) {

            if (apiInfo.reqParamList != null) {
                TsTypeInfo reqParamTypeInfo = new TsTypeInfo();
                reqParamTypeInfo.typeName = getReqParamTsTypeName(apiInfo.name);
                reqParamTypeInfo.fieldInfoList = new ArrayList<>();
                for (FieldInfo fieldInfo : apiInfo.reqParamList) {
                    if (ReflectionUtil.isMultipartFile(fieldInfo.fieldType) || ReflectionUtil.isMultipartFileArray(fieldInfo.fieldType)) {
                        //排除文件上传时的字段
                        continue;
                    }
                    TsFieldInfo tsFieldInfo = new TsFieldInfo();
                    BeanUtil.copy(fieldInfo, tsFieldInfo);
                    reqParamTypeInfo.fieldInfoList.add(tsFieldInfo);
                    if (!ReflectionUtil.isPlanType(fieldInfo.fieldType)) {
                        TsTypeInfo subTypeInfo = getSubTsTypeInfo(fieldInfo, list);
                        addToList(subTypeInfo, list);
                    }
                }
                list.add(reqParamTypeInfo);
            }

            if (apiInfo.bodyParamList != null) {
                TsTypeInfo reqBodyTypeInfo = new TsTypeInfo();
                reqBodyTypeInfo.typeName = getReqBodyTsTypeName(apiInfo.name);
                reqBodyTypeInfo.fieldInfoList = new ArrayList<>();
                for (FieldInfo fieldInfo : apiInfo.bodyParamList) {
                    TsFieldInfo tsFieldInfo = new TsFieldInfo();
                    BeanUtil.copy(fieldInfo, tsFieldInfo);
                    reqBodyTypeInfo.fieldInfoList.add(tsFieldInfo);
                    if (!ReflectionUtil.isPlanType(fieldInfo.fieldType)) {
                        TsTypeInfo subTypeInfo = getSubTsTypeInfo(fieldInfo, list);
                        addToList(subTypeInfo, list);
                    }
                }
                list.add(reqBodyTypeInfo);
            }

            if (apiInfo.respParamList != null) {
                TsTypeInfo respBodyTypeInfo = new TsTypeInfo();
                respBodyTypeInfo.typeName = getRespBodyTsTypeName(apiInfo.name);
                respBodyTypeInfo.fieldInfoList = new ArrayList<>();
                for (FieldInfo fieldInfo : apiInfo.respParamList) {
                    TsFieldInfo tsFieldInfo = new TsFieldInfo();
                    BeanUtil.copy(fieldInfo, tsFieldInfo);
                    respBodyTypeInfo.fieldInfoList.add(tsFieldInfo);
                    if (!ReflectionUtil.isPlanType(fieldInfo.fieldType)) {
                        TsTypeInfo subTypeInfo = getSubTsTypeInfo(fieldInfo, list);
                        addToList(subTypeInfo, list);
                    }
                }
                list.add(respBodyTypeInfo);
            }
        }
        return list;
    }

    private static void addToList(TsTypeInfo subTypeInfo, List<TsTypeInfo> list) {
        if (subTypeInfo == null || subTypeInfo.fieldInfoList == null) {
            return;
        }
        for (TsTypeInfo tsTypeInfo : list) {
            if (tsTypeInfo.typeName.equals(subTypeInfo.typeName)) {
                return;
            }
        }
        list.add(subTypeInfo);
    }

    private static TsTypeInfo getSubTsTypeInfo(FieldInfo fieldInfo, List<TsTypeInfo> list) {
        TsTypeInfo typeInfo = new TsTypeInfo();
        String typeName = fieldInfo.fieldType;
        String[] arr = typeName.split(BaseConstants.SYSTEM_SYMBOL_ESCAPE_VERTICAL_LINE);
        if (ReflectionUtil.isListType(typeName)) {
            int idx = 0;
            String name = BaseConstants.SYSTEM_STRING_OBJECT;
            for (String elementName : arr) {
                if (ReflectionUtil.isListType(elementName)) {
                    idx++;
                } else {
                    name = elementName;
                }
            }
            typeInfo.typeName = name;
        } else {
            typeInfo.typeName = arr[0];
        }
        if (fieldInfo.dicAnn != null) {
            if (fieldInfo.dicAnn.toDicKV()) {
                String name = fieldInfo.dicAnn.name().replace('.', '_');
                name = StringUtil.snakeToCamel(name);
                typeInfo.typeName = StringUtil.upperCaseFirst(name);
            }
        }
        if (fieldInfo.fieldInfoList != null) {
            typeInfo.fieldInfoList = new ArrayList<>();
            for (FieldInfo field : fieldInfo.fieldInfoList) {
                TsFieldInfo tsFieldInfo = new TsFieldInfo();
                BeanUtil.copy(field, tsFieldInfo);
                typeInfo.fieldInfoList.add(tsFieldInfo);
                if (ReflectionUtil.isMultiType(field.fieldType)) {
                    TsTypeInfo subTypeInfo = getSubTsTypeInfo(field, list);
                    addToList(subTypeInfo, list);
                }
            }
        } else {
            logger.error("fieldInfo.fieldInfoList != null, fieldType:{}", fieldInfo.fieldType);
        }
        return typeInfo;
    }

    public static String getTsFieldType(String fieldType, DicAnn dicAnn) {
        String type = "any";
        if (ReflectionUtil.isNumberType(fieldType)) {
            type = "number";
        } else if (ReflectionUtil.isString(fieldType) || ReflectionUtil.isDate(fieldType)) {
            type = "string";
        } else if (ReflectionUtil.isBoolean(fieldType)) {
            type = "boolean";
        } else if (ReflectionUtil.isMultipartFile(fieldType)) {
            type = "File";
        } else if (ReflectionUtil.isListType(fieldType)) {
            String[] arr = fieldType.split(BaseConstants.SYSTEM_SYMBOL_ESCAPE_VERTICAL_LINE);
            StringBuilder sb = new StringBuilder();
            for (String elem : arr) {
                if (ReflectionUtil.isListType(elem)) {
                    sb.append(BaseConstants.SYSTEM_SYMBOL_ARRAY);
                } else {
                    type = getTsMainType(elem) + sb;
                }
            }
        } else if (!ReflectionUtil.isPlanType(fieldType)) {
            type = fieldType.split(BaseConstants.SYSTEM_SYMBOL_ESCAPE_VERTICAL_LINE)[0];
            if (BaseConstants.SYSTEM_STRING_WORKBOOK.equals(type) || BaseConstants.SYSTEM_STRING_OBJECT.equals(type)) {
                return BaseConstants.SYSTEM_STRING_ANY;
            }
        }
        if (dicAnn != null) {
            if (dicAnn.toDicKV()) {
                String name = dicAnn.name().replace('.', '_');
                name = StringUtil.snakeToCamel(name);
                type = StringUtil.upperCaseFirst(name);
            } else {
                type = "string";
            }
        }
        return type;
    }

    public static String getTsMainType(String type) {
        if (ReflectionUtil.isNumberType(type)) {
            type = "number";
        } else if (ReflectionUtil.isString(type) || ReflectionUtil.isDate(type)) {
            type = "string";
        } else if (ReflectionUtil.isBoolean(type)) {
            type = "boolean";
        } else if (ReflectionUtil.isMultipartFile(type)) {
            type = "File";
        }
        return type;
    }

    public static String getReqParamTsTypeName(String name) {
        return StringUtil.upperCaseFirst(name) + "Req";
    }

    public static String getReqBodyTsTypeName(String name) {
        return StringUtil.upperCaseFirst(name) + "ReqBody";
    }

    public static String getRespBodyTsTypeName(String name) {
        return StringUtil.upperCaseFirst(name) + "Resp";
    }

    public static boolean existsNonFileReqParam(List<FieldInfo> reqParamList) {
        boolean exist = false;
        for (FieldInfo fieldInfo : reqParamList) {
            if (!ReflectionUtil.isMultipartFile(fieldInfo.fieldType) && !ReflectionUtil.isMultipartFileArray(fieldInfo.fieldType)) {
                exist = true;
                break;
            }
        }
        return exist;
    }
}
