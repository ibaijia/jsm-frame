package cn.ibaijia.jsm.gen.model;

import java.util.List;

/**
 * @author longzl
 */
public class TsTypeInfo {

    public String typeName;
    /**
     * 数组 List set类型时 保存元素的类型
     */
    public String arrayElementName;

    public List<TsFieldInfo> fieldInfoList;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<TsFieldInfo> getFieldInfoList() {
        return fieldInfoList;
    }

    public void setFieldInfoList(List<TsFieldInfo> fieldInfoList) {
        this.fieldInfoList = fieldInfoList;
    }

    public String getArrayElementName() {
        return arrayElementName;
    }

    public void setArrayElementName(String arrayElementName) {
        this.arrayElementName = arrayElementName;
    }
}
