package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.gen.model.*;
import cn.ibaijia.jsm.utils.*;
import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.web.ApiScriptReq;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

/**
 * @author longzl
 */
@Service
public class GenService extends BaseService {

    private static final String LANG_TS = "ts";
    private static final String LANG_JS = "js";
    private static final String FILE_TYPE_API = "api";
    private static final String FILE_TYPE_MODEL = "model";
    @Value(value = "${" + AppContextKey.GEN_PACKAGE_MODEL + ":}")
    private String modelPackageName;
    @Value(value = "${" + AppContextKey.GEN_PACKAGE_MAPPER + ":}")
    private String mapperPackageName;
    @Value(value = "${" + AppContextKey.GEN_PACKAGE_SERVICE + ":}")
    private String servicePackageName;
    @Value(value = "${" + AppContextKey.GEN_PACKAGE_REST + ":}")
    private String restPackageName;
    @Value(value = "${" + AppContextKey.SWAGGER_PACKAGE + ":}")
    private String swaggerPackage;

    @PostConstruct
    public void check() {
        if (StringUtil.isEmpty(modelPackageName) && !StringUtil.isEmpty(swaggerPackage)) {
            String basePackage = swaggerPackage.substring(0, swaggerPackage.lastIndexOf('.'));
            modelPackageName = basePackage + ".dao.model";
            mapperPackageName = basePackage + ".dao.mapper";
            servicePackageName = basePackage + ".service";
            restPackageName = basePackage + ".rest";
        }
    }

    public void genAll() {
        List<String> tableNameList = JsmConfigurer.getDbGenerationStrategy().getTables();
        for (String tableName : tableNameList) {
            if ("opt_log_t".equals(tableName)) {
                logger.debug("ignore jsm frame table. opt_log_t");
                continue;
            }
            gen(tableName, false, true);
        }
    }

    public void gen(String tableName) {
        gen(tableName, false, true);
    }

    public void gen(String tableName, boolean override) {
        gen(tableName, override, false);
    }

    public void gen(String tableName, boolean override, boolean genApi) {
        gen(tableName, override, true, genApi);
    }

    public void gen(String tableName, boolean override, boolean update, boolean genApi) {
        String baseJavaDir = null;
        String mapperXmlDirPath = null;
        try {
            baseJavaDir = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator;
            mapperXmlDirPath = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mappers" + File.separator;
        } catch (IOException e) {
            logger.error("", e);
        }

        String className = JsmConfigurer.getDbGenerationStrategy().tableNameToModelName(tableName);
        String categoryName = StringUtil.lowerCaseFirst(className);

        String restReqPackageName = restPackageName + ".req." + categoryName.toLowerCase();
        String restVoPackageName = restPackageName + ".vo." + categoryName.toLowerCase();
        logger.info("baseJavaDir:{}", baseJavaDir);
        logger.info("modelPackageName:{}", modelPackageName);
        logger.info("mapperPackageName:{}", mapperPackageName);
        logger.info("mapperXmlDirPath:{}", mapperXmlDirPath);
        logger.info("servicePackageName:{}", servicePackageName);
        logger.info("restReqPackageName:{}", restReqPackageName);
        logger.info("restVoPackageName:{}", restVoPackageName);

        Map<String, Object> datas = new HashMap<>(16);
        datas.put("modelPackageName", modelPackageName);
        datas.put("mapperPackageName", mapperPackageName);
        datas.put("servicePackageName", servicePackageName);
        datas.put("restReqPackageName", restReqPackageName);
        datas.put("restVoPackageName", restVoPackageName);
        datas.put("restPackageName", restPackageName);
        datas.put("tableName", tableName);
        datas.put("className", className);
        datas.put("apiName", StringUtil.camelToKebab(categoryName));
        datas.put("genApi", genApi);
        List<FieldInfo> fieldList = JsmConfigurer.getDbGenerationStrategy().getTableFieldInfo(tableName, datas);
        datas.put("fieldList", fieldList);

        genModelFiles(true, update, baseJavaDir, datas);

        genMapperFiles(override, update, baseJavaDir, datas);

        genMapperXmlFiles(override, mapperXmlDirPath, datas);

        genServiceFiles(override, baseJavaDir, datas);

        if (genApi) {
            genApiFiles(override, baseJavaDir, datas);
        }
    }

    private void genServiceFiles(boolean override, String baseJavaDir, Map<String, Object> datas) {
        String serviceFtl = "service.ftl";
        logger.info("serviceFtl:{}", serviceFtl);
        String className = (String) datas.get("className");
        String serviceDirPath = baseJavaDir + servicePackageName.replace(".", File.separator);
        GenUtil.makDirs(serviceDirPath);
        String serviceFileName = serviceDirPath + File.separator + className + "Service.java";
        GenUtil.buildFtlFile(datas, serviceFtl, serviceFileName, override);
    }

    private void genMapperXmlFiles(boolean override, String mapperXmlDirPath, Map<String, Object> datas) {
        String mapperXmlFtl = "mapperXml.ftl";
        logger.info("mapperXmlFtl:{}", mapperXmlFtl);
        String className = (String) datas.get("className");
        String mapperXmlFileName = mapperXmlDirPath + File.separator + className + "Mapper.xml";
        GenUtil.makDirs(mapperXmlDirPath);
        //gen mapperXml main
        GenUtil.buildFtlFile(datas, mapperXmlFtl, mapperXmlFileName, override);
    }

    private void genMapperFiles(boolean override, boolean update, String baseJavaDir, Map<String, Object> datas) {
        String mapperFtl = "mapper.ftl";
        logger.info("mapperFtl:{}", mapperFtl);
        String className = (String) datas.get("className");
        String mapperDirPath = baseJavaDir + mapperPackageName.replace(".", File.separator);
        String mapperFileName = mapperDirPath + File.separator + className + "Mapper.java";
        GenUtil.makDirs(mapperDirPath);
        //gen mapper main
        String mapperStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, mapperFtl);
        boolean mapperRes = GenUtil.writeFile(mapperStr, mapperFileName, override);
        logger.info("createMapper:{} res:{}", mapperFileName, mapperRes);
        //更新 xml add addBatch update 内容
        if (!mapperRes && update) {
            GenUtil.updateMapper(mapperStr, mapperFileName);
        }
    }

    private boolean genModelFiles(boolean override, boolean update, String baseJavaDir, Map<String, Object> datas) {
        String modelFtl = "model.ftl";
        logger.info("modelFtl:{}", modelFtl);
        String className = (String) datas.get("className");
        String modelDirPath = baseJavaDir + modelPackageName.replace(".", File.separator);
        String modelFileName = modelDirPath + File.separator + className + ".java";
        GenUtil.makDirs(modelDirPath);
        //gen model
        String modelStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, modelFtl);
        boolean modelRes = GenUtil.writeFile(modelStr, modelFileName, override);
        //update model
        if (!modelRes && update) {
            GenUtil.updateModel(modelStr, modelFileName);
        }
        logger.info("createModel:{} res:{}", modelFileName, modelRes);
        return modelRes;
    }

    private void genApiFiles(boolean override, String baseJavaDir, Map<String, Object> datas) {
        String addReqFtl = "addReq.ftl";
        String updateReqFtl = "updateReq.ftl";
        String pageReqFtl = "pageReq.ftl";
        String voFtl = "vo.ftl";
        String apiFtl = "api.ftl";
        logger.info("addReqFtl:{}", addReqFtl);
        logger.info("updateReqFtl:{}", updateReqFtl);
        logger.info("pageReqFtl:{}", pageReqFtl);
        logger.info("voFtl:{}", voFtl);
        logger.info("apiFtl:{}", apiFtl);

        String className = (String) datas.get("className");
        String restReqPackageName = (String) datas.get("restReqPackageName");
        String restVoPackageName = (String) datas.get("restVoPackageName");

        String restReqDirPath = baseJavaDir + restReqPackageName.replace(".", File.separator);
        String restVoDirPath = baseJavaDir + restVoPackageName.replace(".", File.separator);
        String restApiDirPath = baseJavaDir + restPackageName.replace(".", File.separator);

        String restAddReqFileName = restReqDirPath + File.separator + className + "AddReq.java";
        String restUpdateReqFileName = restReqDirPath + File.separator + className + "UpdateReq.java";
        String restPageReqFileName = restReqDirPath + File.separator + className + "PageReq.java";
        String restVoFileName = restVoDirPath + File.separator + className + "Vo.java";
        String restApiFileName = restApiDirPath + File.separator + className + "Api.java";

        GenUtil.makDirs(restReqDirPath);
        GenUtil.makDirs(restVoDirPath);
        GenUtil.makDirs(restApiDirPath);

        GenUtil.buildFtlFile(datas, addReqFtl, restAddReqFileName, override);
        GenUtil.buildFtlFile(datas, updateReqFtl, restUpdateReqFileName, override);
        GenUtil.buildFtlFile(datas, pageReqFtl, restPageReqFileName, override);
        GenUtil.buildFtlFile(datas, voFtl, restVoFileName, override);
        GenUtil.buildFtlFile(datas, apiFtl, restApiFileName, override);
    }

    public void genPermissions() {
        String baseJavaDir = null;
        try {
            baseJavaDir = new File("").getCanonicalPath() + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator;
        } catch (IOException e) {
            logger.error("", e);
        }
        String constPackageName = servicePackageName.substring(0, servicePackageName.length() - 7) + "consts";
        String constDirPath = baseJavaDir + constPackageName.replace(".", File.separator);
        List<JsmPermission> permissionList = JsmConfigurer.getDbGenerationStrategy().listPermissions();
        Map<String, Object> datas = new HashMap<>(16);
        datas.put("constPackageName", constPackageName);
        datas.put("permissionList", permissionList);
        String ftl = "permissions.ftl";
        String permissionsFileName = constDirPath + File.separator + "Permissions.java";
        GenUtil.buildFtlFile(datas, ftl, permissionsFileName, true);
    }

    public void genApiHtml() {
        Set<Class<?>> classSet = ClassUtil.loadClass(restPackageName, true);
        List<ControllerInfo> controllerInfoList = new ArrayList<>();
        for (Class clazz : classSet) {
            RestController restController = (RestController) clazz.getAnnotation(RestController.class);
            if (restController == null) {
                continue;
            }
            ControllerInfo controllerInfo = new ControllerInfo();
            String name = StringUtil.camelToKebab(clazz.getSimpleName());
            if (name.startsWith("-")) {
                name = name.substring(1);
            }
            name = "/" + name + "/";
            controllerInfo.name = name;
            controllerInfo.apiInfos = getApiInfoList(clazz);
            controllerInfoList.add(controllerInfo);
        }
        genApiHtml(controllerInfoList);
    }

    private void genApiHtml(List<ControllerInfo> controllerInfoList) {
        String apiHtml = "apiHtml.ftl";
        String apiHtmlPath = "";
        boolean isInSpringBoot = GenUtil.checkIsInSpringBoot();
        if (StringUtil.isEmpty(apiHtmlPath)) {
            StringBuilder sb = new StringBuilder();
            try {
                sb.append(new File("").getCanonicalPath()).append(File.separator);
            } catch (IOException e) {
                logger.error("", e);
            }
            sb.append("src").append(File.separator);
            sb.append("main").append(File.separator);
            if (isInSpringBoot) {
                sb.append("resources").append(File.separator);
            } else {
                sb.append("webapp").append(File.separator);
            }
            sb.append("static").append(File.separator);
            sb.append("api.html");
            apiHtmlPath = sb.toString();
        }
        GenUtil.makeDirs(apiHtmlPath.substring(0, apiHtmlPath.lastIndexOf(File.separator)));
        logger.info("apiHtmlPath:{}", apiHtmlPath);
        Map<String, Object> datas = new HashMap<>(16);
        datas.put("controllerInfoList", controllerInfoList);
        datas.put("controllerInfoJson", JsonUtil.toJsonString(controllerInfoList));

        //gen model main
        String modelStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", datas, apiHtml);
        HtmlCompressor compressor = new HtmlCompressor();
        compressor.setCompressCss(true);
        compressor.setEnabled(true);
        compressor.setCompressCss(true);
        compressor.setYuiJsPreserveAllSemiColons(true);
        compressor.setYuiJsLineBreak(1);
        compressor.setPreserveLineBreaks(false);
        compressor.setRemoveIntertagSpaces(true);
        compressor.setRemoveComments(true);
        compressor.setRemoveMultiSpaces(true);
        modelStr = compressor.compress(modelStr);
        boolean modelRes = GenUtil.writeFile(modelStr, apiHtmlPath, true);
        logger.info("createApiHtml:{} res:{}", apiHtmlPath, modelRes);
    }

    private List<ApiInfo> getApiInfoList(Class clazz) {
        return this.getApiInfoList(clazz, null);
    }

    private List<ApiInfo> getApiInfoList(Class clazz, String methodName) {
        String basePath = "";
        RestController restController = (RestController) clazz.getAnnotation(RestController.class);
        if (restController == null) {
            return null;
        }
        basePath = basePath + GenUtil.getClazzUri(clazz);
        List<ApiInfo> apiInfoList = new ArrayList<>();
        Method[] methods = clazz.getMethods();
        Arrays.sort(methods, Comparator.comparing(Method::getName));
        for (Method method : methods) {
            if (methodName != null && !methodName.equals(method.getName())) {
                continue;
            }
            RequestMapping requestMapping = AnnotatedElementUtils.getMergedAnnotation(method, RequestMapping.class);
            if (requestMapping == null || requestMapping.method().length == 0 || requestMapping.value().length == 0) {
                continue;
            }
            ApiInfo apiInfo = new ApiInfo();
            apiInfo.name = method.getName();
            apiInfo.comments = "";
            apiInfo.httpMethod = GenUtil.getHttpMethodName(method);
            apiInfo.url = basePath + GenUtil.getMethodUri(method);
            logger.info("getApiInfoList clazz:{}, methodName:{},url:{}", clazz.getSimpleName(), apiInfo.name, apiInfo.url);
            ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
            if (apiOperation != null) {
                apiInfo.comments = apiOperation.value();
                if (!StringUtil.isEmpty(apiOperation.notes())) {
                    apiInfo.comments = apiInfo.comments + " notes:" + apiOperation.notes();
                }
            }
            for (Parameter parameter : method.getParameters()) {
                PathVariable pathVariable = parameter.getAnnotation(PathVariable.class);
                if (pathVariable != null) {
                    apiInfo.pathVarList = getSingleReqParamFields(parameter, pathVariable.value());
                    continue;
                }
                RequestParam requestParam = parameter.getAnnotation(RequestParam.class);
                if (requestParam != null) {
                    apiInfo.reqParamList = getSingleReqParamFields(parameter, requestParam.value());
                    continue;
                }
                RequestBody requestBody = parameter.getAnnotation(RequestBody.class);
                if (requestBody != null) {
                    apiInfo.bodyParamList = JsmFrameUtil.getFieldInfoList(parameter.getType(), null, null, 0, false);
                    continue;
                }
                //对象型的request param
                if (ValidateModel.class.isAssignableFrom(parameter.getType())) {
                    apiInfo.reqParamList = JsmFrameUtil.getFieldInfoList(parameter.getType(), JsmFrameUtil.getGenericNames(parameter.getParameterizedType()), null, 0, false);
                    if (Page.class.isAssignableFrom(parameter.getType())) {
                        Iterator<FieldInfo> it = apiInfo.reqParamList.iterator();
                        while (it.hasNext()) {
                            FieldInfo fieldInfo = it.next();
                            if ("list".equals(fieldInfo.fieldName) || "totalCount".equals(fieldInfo.fieldName)) {
                                it.remove();
                            }
                        }
                    }
                }
            }
            //返回参数描述
            if (!"void".equals(method.getReturnType().getName())) {
                apiInfo.respParamList = JsmFrameUtil.getFieldInfoList(method.getReturnType(), JsmFrameUtil.getGenericNames(method.getGenericReturnType()), null, 0, false);
            }
            apiInfoList.add(apiInfo);
        }
        return apiInfoList;
    }

    public String getControllerInfoHtml(String clazzName, String methodName) {
        ControllerInfo controllerInfo = getControllerInfo(clazzName, methodName);
        if (controllerInfo != null) {
            String ftlPath = "apiDetail.ftl";
            Map<String, Object> data = new HashMap<>(1);
            data.put("controllerInfo", controllerInfo);
            data.put("controllerInfoJson", JsonUtil.toJsonString(controllerInfo));
            String modelStr = FtlUtil.build(this.getClass(), "/META-INF/ftl/", data, ftlPath);
            return modelStr;
        }
        return null;
    }

    private ControllerInfo getControllerInfo(String className, String methodName) {
        Set<Class<?>> classSet = ClassUtil.loadClass(restPackageName, true);
        for (Class clazz : classSet) {
            RestController restController = (RestController) clazz.getAnnotation(RestController.class);
            if (restController == null) {
                continue;
            }
            if (!className.equals(clazz.getSimpleName())) {
                continue;
            }
            ControllerInfo controllerInfo = new ControllerInfo();
            String name = StringUtil.camelToKebab(clazz.getSimpleName());
            if (name.startsWith("-")) {
                name = name.substring(1);
            }
            name = "/" + name + "/";
            controllerInfo.name = name;
            controllerInfo.apiInfos = getApiInfoList(clazz, methodName);
            return controllerInfo;
        }
        return null;
    }

    private List<FieldInfo> getSingleReqParamFields(Parameter parameter, String annFieldName) {
        List<FieldInfo> reqParamList = new ArrayList<>();
        FieldInfo fieldInfo = new FieldInfo();
        fieldInfo.fieldName = parameter.getName();
        fieldInfo.fieldType = parameter.getType().getSimpleName();
        fieldInfo.required = false;
        FieldAnn fieldAnn = parameter.getAnnotation(FieldAnn.class);
        if (fieldAnn != null) {
            if (fieldAnn.maxLen() != -1) {
                fieldInfo.maxLength = fieldAnn.maxLen();
            }
            fieldInfo.required = fieldAnn.required();
            fieldInfo.comments = fieldAnn.comments();
        }
        if (!StringUtil.isEmpty(annFieldName)) {
            fieldInfo.fieldName = annFieldName;
        }
        reqParamList.add(fieldInfo);
        return reqParamList;
    }

    public List<String> getApiNames() {
        logger.info("tmpdir:{}", System.getProperty("java.io.tmpdir"));
        Set<Class<?>> classSet = ClassUtil.loadClass(restPackageName, false);
        List<String> list = new ArrayList<>();
        for (Class clazz : classSet) {
            String name = clazz.getSimpleName();
            list.add(name);
        }
        return list;
    }

    public String genScript(ApiScriptReq req) {
        ControllerInfo controllerInfo = getControllerInfo(req.getApiName(), null);
        Map<String, Object> data = new HashMap<>(2);
        data.put("req", req);
        data.put("controllerInfo", controllerInfo);
        String result = null;
        if (LANG_TS.equals(req.getLang())) {
            if (FILE_TYPE_MODEL.equals(req.getFileType())) {
                List<TsTypeInfo> tsTypeInfoList = GenUtil.getTsTypeInfoList(controllerInfo);
                data.put("tsTypeInfoList", tsTypeInfoList);
                result = genStr("frontTsModel.ftl", data);
            }
            if (FILE_TYPE_API.equals(req.getFileType())) {
                result = genStr("frontTsApi.ftl", data);
            }
        }
        if (LANG_JS.equals(req.getLang())) {
            if (FILE_TYPE_API.equals(req.getFileType())) {
                result = genStr("frontJsApi.ftl", data);
            }
        }
        return result;
    }

    private String genStr(String tplPath, Map<String, Object> data) {
        try {
            String str = FtlUtil.build(this.getClass(), "/META-INF/ftl/", data, tplPath);
            return str;
        } catch (Exception e) {
            logger.error("",e);
        }
        return null;
    }

}
