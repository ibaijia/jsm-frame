package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.gen.model.CreateTableInfo;
import cn.ibaijia.jsm.gen.model.FieldInfo;
import cn.ibaijia.jsm.gen.model.JsmPermission;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * @author longzl
 */
public class DbUtil {

    /**
     * showTables
     *
     * @return 表名
     */
    public static List<String> showTables() {
        String sql = "show tables;";
        JdbcTemplate jdbcTemplate = SpringContext.getBean(JdbcTemplate.class);
        if (jdbcTemplate == null) {
            throw new AssertionError();
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(String.class));
    }

    /**
     * showCreateTable
     *
     * @param tableName
     * @return
     */
    public static List<CreateTableInfo> showCreateTable(String tableName) {
        String sql = "show create table %s;";
        JdbcTemplate jdbcTemplate = SpringContext.getBean(JdbcTemplate.class);
        if (jdbcTemplate == null) {
            throw new AssertionError();
        }
        return jdbcTemplate.query(String.format(sql, tableName), (rs, rowNum) -> {
            return new CreateTableInfo(rs.getString(1), rs.getString(2));
        });
    }

    /**
     * showMssqlTables
     *
     * @return
     */
    public static List<String> showMssqlTables() {
        String sql = "select name from sys.tables where is_ms_shipped=0;";
        JdbcTemplate jdbcTemplate = SpringContext.getBean(JdbcTemplate.class);
        if (jdbcTemplate == null) {
            throw new AssertionError();
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(String.class));
    }

    /**
     * listMssqlTableInfo
     *
     * @param tableName
     * @return
     */
    public static List<FieldInfo> listMssqlTableInfo(String tableName) {
        String sql = "SELECT col.name AS fieldName , " +
                " t.name AS fieldType , " +
                " ISNULL(ep.[value], '') AS comments , " +
                " col.length AS maxLength , " +
                " ISNULL(COLUMNPROPERTY(col.id, col.name, 'Scale'), 0) AS scale , " +
                " COLUMNPROPERTY(col.id, col.name, 'IsIdentity') AS isIdentity , " +
                " col.isnullable AS required , " +
                " ISNULL(comm.text, '') AS defaultVal " +
                " FROM dbo.syscolumns col " +
                " LEFT JOIN dbo.systypes t ON col.xtype = t.xusertype " +
                " inner JOIN dbo.sysobjects obj ON col.id = obj.id AND obj.xtype = 'U' AND obj.status >= 0 " +
                " LEFT JOIN dbo.syscomments comm ON col.cdefault = comm.id " +
                " LEFT JOIN sys.extended_properties ep ON col.id = ep.major_id AND col.colid = ep.minor_id AND ep.name = 'MS_Description' " +
                " WHERE obj.name = '%s' and col.name != 'id' " +
                " ORDER BY col.colorder ;";
        JdbcTemplate jdbcTemplate = SpringContext.getBean(JdbcTemplate.class);
        if (jdbcTemplate == null) {
            throw new AssertionError();
        }
        return jdbcTemplate.query(String.format(sql, tableName), new BeanPropertyRowMapper<>(FieldInfo.class));
    }

    /**
     * listPermissions
     *
     * @return
     */
    public static List<JsmPermission> listPermissions(){
        String sql = "SELECT name,code  FROM permission_t where code is not null and code != '';";
        JdbcTemplate jdbcTemplate = SpringContext.getBean(JdbcTemplate.class);
        if (jdbcTemplate == null) {
            throw new AssertionError();
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(JsmPermission.class));
    }

}
