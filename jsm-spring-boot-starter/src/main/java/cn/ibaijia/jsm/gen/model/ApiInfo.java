package cn.ibaijia.jsm.gen.model;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.gen.GenUtil;
import cn.ibaijia.jsm.utils.ReflectionUtil;

import java.io.Serializable;
import java.util.List;

/**
 * @author longzl
 */
public class ApiInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    public String name;
    public String httpMethod;
    public String url;
    public List<FieldInfo> pathVarList;
    public List<FieldInfo> reqParamList;
    public List<FieldInfo> bodyParamList;
    public List<FieldInfo> respParamList;
    public String comments;

    public boolean pathVarListShow = true;
    public boolean reqParamListShow = true;
    public boolean bodyParamListShow = true;
    public boolean respParamListShow = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUrl() {
        return url;
    }

    public String getFrontUrl() {
        if (url.contains("{")) {
            return url.replace("{", "${");
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<FieldInfo> getPathVarList() {
        return pathVarList;
    }

    public void setPathVarList(List<FieldInfo> pathVarList) {
        this.pathVarList = pathVarList;
    }

    public List<FieldInfo> getReqParamList() {
        return reqParamList;
    }

    public void setReqParamList(List<FieldInfo> reqParamList) {
        this.reqParamList = reqParamList;
    }

    public List<FieldInfo> getBodyParamList() {
        return bodyParamList;
    }

    public void setBodyParamList(List<FieldInfo> bodyParamList) {
        this.bodyParamList = bodyParamList;
    }

    public List<FieldInfo> getRespParamList() {
        return respParamList;
    }

    public void setRespParamList(List<FieldInfo> respParamList) {
        this.respParamList = respParamList;
    }

    public String getTsReqVar() {
        StringBuilder sb = new StringBuilder();
        if (this.pathVarList != null) {
            for (FieldInfo fieldInfo : this.pathVarList) {
                sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                        .append(fieldInfo.fieldName).append(": ").append(fieldInfo.getTsFieldType());
            }
        }
        if (this.reqParamList != null) {
            if (GenUtil.existsNonFileReqParam(this.reqParamList)) {
                sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                        .append("reqParams: model.").append(GenUtil.getReqParamTsTypeName(this.name));
            }
            //文件上传
            for (FieldInfo fieldInfo : this.reqParamList) {
                if (ReflectionUtil.isMultipartFile(fieldInfo.fieldType) || ReflectionUtil.isMultipartFileArray(fieldInfo.fieldType)) {
                    sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                            .append(fieldInfo.fieldName).append(": ").append(BaseConstants.SYSTEM_STRING_FILE);
                }
                if (ReflectionUtil.isMultipartFileArray(fieldInfo.fieldType)) {
                    sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                            .append(BaseConstants.SYSTEM_SYMBOL_ARRAY);
                }
            }
        }
        if (this.bodyParamList != null) {
            sb.append(sb.length() > 0 ? "," : "").append("reqBody: model.").append(GenUtil.getReqBodyTsTypeName(this.name));
            return sb.toString();
        }
        return sb.toString();
    }

    public String getJsReqVar() {
        StringBuilder sb = new StringBuilder();
        if (this.pathVarList != null) {
            for (FieldInfo fieldInfo : this.pathVarList) {
                sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                        .append(fieldInfo.fieldName);
            }
        }
        if (this.reqParamList != null) {
            if (GenUtil.existsNonFileReqParam(this.reqParamList)) {
                sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                        .append("reqParams");
            }
            //文件上传
            for (FieldInfo fieldInfo : this.reqParamList) {
                if (ReflectionUtil.isMultipartFile(fieldInfo.fieldType) || ReflectionUtil.isMultipartFileArray(fieldInfo.fieldType)) {
                    sb.append(sb.length() > 0 ? BaseConstants.SYSTEM_SYMBOL_COMMA : BaseConstants.SYSTEM_EMPTY_STRING)
                            .append(fieldInfo.fieldName);
                }
            }
        }
        if (this.bodyParamList != null) {
            sb.append(sb.length() > 0 ? "," : "").append("reqBody");
            return sb.toString();
        }
        return sb.toString();
    }

    public String getFrontReqVarName() {
        StringBuilder sb = new StringBuilder();
        if (this.reqParamList != null) {
            if (GenUtil.existsNonFileReqParam(this.reqParamList)) {
                sb.append(" + parseQueryString(reqParams)");
            }
            //文件上传
            for (FieldInfo fieldInfo : this.reqParamList) {
                if (ReflectionUtil.isMultipartFile(fieldInfo.fieldType) || ReflectionUtil.isMultipartFileArray(fieldInfo.fieldType)) {
                    sb.append(", ").append(fieldInfo.fieldName);
                }
            }
        }
        if (this.bodyParamList != null) {
            sb.append(", reqBody");
        }
        return sb.toString();
    }

    public String getTsReqParamType() {
        if (this.reqParamList != null) {
            return GenUtil.getReqParamTsTypeName(this.name);
        }
        return null;
    }

    public String getTsReqBodyType() {
        if (this.bodyParamList != null) {
            return GenUtil.getReqBodyTsTypeName(this.name);
        }
        return null;
    }

    public String getTsRespBodyType() {
        if (this.respParamList != null) {
            return GenUtil.getRespBodyTsTypeName(this.name);
        }
        return null;
    }


}
