package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.gen.model.FieldInfo;
import cn.ibaijia.jsm.gen.model.JsmPermission;

import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2021/11/4 15:19
 */
public class DefaultMsSqlGenerationStrategy implements DbGenerationStrategy {
    @Override
    public List<String> getTables() {
        return DbUtil.showMssqlTables();
    }

    @Override
    public List<FieldInfo> getTableFieldInfo(String tableName, Map<String, Object> contextData) {
        List<FieldInfo> fieldInfoList = DbUtil.listMssqlTableInfo(tableName);
        for (FieldInfo fieldInfo : fieldInfoList) {
            fieldInfo.fieldName = columnNameToFieldName(fieldInfo.fieldName);
            fieldInfo.fieldType = dbTypeToJavaType(fieldInfo.fieldType);
            if ("Date".equals(fieldInfo.fieldType)) {
                contextData.put("hasDate", true);
            }
        }
        return fieldInfoList;
    }

    @Override
    public List<JsmPermission> listPermissions() {
        return DbUtil.listPermissions();
    }

}
