package cn.ibaijia.jsm.gen.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author longzl
 */
public class ControllerInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    public String name;
    public List<ApiInfo> apiInfos;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ApiInfo> getApiInfos() {
        return apiInfos;
    }

    public void setApiInfos(List<ApiInfo> apiInfos) {
        this.apiInfos = apiInfos;
    }
}
