package cn.ibaijia.jsm.gen.model;

import java.io.Serializable;
import java.util.List;
/**
 * @author longzl
 */
public class MethodInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    public String name;
    public String httpMethod;
    public String url;
    public List<String> pathVarList;
    public List<String> reqParamList;
    public List<String> bodyParamList;
    public String dataVarName;
    public String fullVarName;
    public String comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getPathVarList() {
        return pathVarList;
    }

    public void setPathVarList(List<String> pathVarList) {
        this.pathVarList = pathVarList;
    }

    public String getDataVarName() {
        return dataVarName;
    }

    public void setDataVarName(String dataVarName) {
        this.dataVarName = dataVarName;
    }

    public String getFullVarName() {
        return fullVarName;
    }

    public void setFullVarName(String fullVarName) {
        this.fullVarName = fullVarName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<String> getReqParamList() {
        return reqParamList;
    }

    public void setReqParamList(List<String> reqParamList) {
        this.reqParamList = reqParamList;
    }

    public List<String> getBodyParamList() {
        return bodyParamList;
    }

    public void setBodyParamList(List<String> bodyParamList) {
        this.bodyParamList = bodyParamList;
    }
}
