package cn.ibaijia.jsm.gen;

import cn.ibaijia.jsm.gen.model.FieldInfo;
import cn.ibaijia.jsm.gen.model.JsmPermission;

import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2021/11/4 15:10
 */
public interface DbGenerationStrategy {

    /**
     * 获取所有table名
     *
     * @return
     */
    List<String> getTables();

    /**
     * 通过表名 取到表详情
     *
     * @param tableName
     * @param contextData
     * @return
     */
    List<FieldInfo> getTableFieldInfo(String tableName, Map<String, Object> contextData);

    /**
     * 返回所有权限
     * @return
     */
    List<JsmPermission> listPermissions();

    static final String DB_BIGINT = "bigint";
    static final String DB_TINYINT = "tinyint";
    static final String DB_SMALLINT = "smallint";
    static final String DB_INT = "int";
    static final String DB_INTEGER = "integer";
    static final String DB_VARCHAR = "varchar";
    static final String DB_TEXT = "text";
    static final String DB_BIT = "bit";
    static final String DB_FLOAT = "float";
    static final String DB_DOUBLE = "double";
    static final String DB_DATE = "date";
    static final String DB_TIMESTAMP = "timestamp";
    static final String DB_TABLE_SUFFIX = "_t";
    /**
     * db类型转java类型
     *
     * @param dbType
     * @return
     */
    public default String dbTypeToJavaType(String dbType) {
        if (dbType.startsWith(DB_BIGINT)) {
            return "Long";
        }
        if (dbType.startsWith(DB_TINYINT)) {
            return "Byte";
        }
        if (dbType.startsWith(DB_SMALLINT)) {
            return "Short";
        }
        if (dbType.startsWith(DB_INT) || dbType.startsWith(DB_INTEGER)) {
            return "Integer";
        }
        if (dbType.startsWith(DB_VARCHAR) || dbType.contains(DB_TEXT)) {
            return "String";
        }
        if (dbType.startsWith(DB_BIT)) {
            return "Boolean";
        }
        if (dbType.startsWith(DB_FLOAT)) {
            return "Float";
        }
        if (dbType.startsWith(DB_DOUBLE)) {
            return "Double";
        }
        if (dbType.startsWith(DB_DATE) || dbType.startsWith(DB_TIMESTAMP)) {
            return "Date";
        }
        return "byte[]";
    }

    /**
     * 表名转实体类名
     *
     * @param tableName
     * @return
     */
    public default String tableNameToModelName(String tableName) {
        String clazzName = tableName.replace("-", "_");
        if (clazzName.endsWith(DB_TABLE_SUFFIX)) {
            clazzName = clazzName.substring(0, clazzName.length() - 2);
        }
        StringBuilder sb = new StringBuilder();
        boolean toUpper = true;
        for (int i = 0; i < clazzName.length(); i++) {
            char c = clazzName.charAt(i);
            if (c == '_') {
                toUpper = true;
                continue;
            }
            sb.append(toUpper ? Character.toUpperCase(c) : c);
            if (toUpper) {
                toUpper = false;
            }
        }
        return sb.toString();
    }

    /**
     * 列名转字段名
     *
     * @param columnName
     * @return
     */
    public default String columnNameToFieldName(String columnName) {
        return columnName;
    }

}
