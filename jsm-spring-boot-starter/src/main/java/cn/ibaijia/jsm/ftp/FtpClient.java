package cn.ibaijia.jsm.ftp;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.utils.LogUtil;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;

import java.io.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * @author longzl
 */
public class FtpClient {
    private static Logger logger = LogUtil.log(FtpClient.class);
    private FTPClient ftpClient;
    private String defaultEncoding = "UTF-8";

    public FtpClient() {
        ftpClient = new FTPClient();
        //解决上传文件时文件名乱码
        ftpClient.setControlEncoding(defaultEncoding);
    }

    public void setTimeOut(int defaultTimeoutSecond, int connectTimeoutSecond, int dataTimeoutSecond) {
        try {
            ftpClient.setDefaultTimeout(defaultTimeoutSecond * 1000);
            //ftp.setConnectTimeout(connectTimeoutSecond * 1000); //commons-net-3.5.jar
            //commons-net-1.4.1.jar 连接后才能设置
            ftpClient.setSoTimeout(connectTimeoutSecond * 1000);
            ftpClient.setDataTimeout(Duration.ofMillis(dataTimeoutSecond * 1000));
        } catch (Exception e) {
            logger.error("setTimeout Exception:", e);
        }
    }

    public FTPClient getFtpClient() {
        return ftpClient;
    }

    public void setControlEncoding(String charset) {
        ftpClient.setControlEncoding(charset);
    }

    public boolean setFileType(int fileType) {
        try {
            ftpClient.setFileType(fileType);
            return true;
        } catch (Exception e) {
            logger.error("setFileType failed.", e);
            return false;
        }
    }

    public boolean connect(String host, int port, String user, String password) {
        if (ftpClient.isConnected()) {
            logger.error("has connected.");
            return false;
        }
        try {
            ftpClient.connect(host, port);
            // Check rsponse after connection attempt.
            int reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                disconnect();
                return false;
            }
            if ("".equals(user)) {
                user = "anonymous";
            }
            // Login.
            if (!ftpClient.login(user, password)) {
                disconnect();
                logger.error("login failed." + host);
                return false;
            }
            // Set data transfer mode. FTP.ASCII_FILE_TYPE
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            //  Use passive mode to pass firewalls.
            ftpClient.enterLocalPassiveMode();
            return true;
        } catch (Exception e) {
            logger.error("connect failed.", e);
            return false;
        }
    }

    public boolean isConnected() {
        return ftpClient.isConnected();
    }

    public boolean disconnect() {
        if (ftpClient.isConnected()) {
            try {
                ftpClient.logout();
                ftpClient.disconnect();
            } catch (IOException e) {
                logger.error("disconnect error!");
                return false;
            }
        }
        return true;
    }

    public boolean downloadFile(String ftpFilePath, OutputStream out) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            // Get file info.
            FTPFile[] fileInfoArray = ftpClient.listFiles(ftpFilePath);
            if (fileInfoArray == null || fileInfoArray.length == 0) {
                logger.error("remote file:" + ftpFilePath + "'not found.");
                return false;
            }
            // Check file size.
            FTPFile ftpFile = fileInfoArray[0];
            long size = ftpFile.getSize();
            logger.info("downloadFile size:{}", size);
            // Download file.
            boolean res = ftpClient.retrieveFile(ftpFilePath, out);
            logger.info("download file:{} res:{}", ftpFilePath, res);
            out.flush();
            return true;
        } catch (Exception e) {
            logger.error("downloadFile error:" + ftpFilePath, e);
            return false;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    logger.error("close out error", e);
                }
            }
        }
    }

    public boolean uploadFile(String ftpFilePath, InputStream in) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        // path check
        FileInfo fileInfo = null;
        try {
            fileInfo = makeDirAndGetFileInfo(ftpFilePath);
            boolean res = ftpClient.storeFile(fileInfo.fileName, in);
            logger.info("upload file:{} res:{}", ftpFilePath, res);
            return res;
        } catch (Exception e) {
            logger.error("uploadFile failed." + ftpFilePath, e);
            return false;
        } finally {
            try {
                in.close();
                if (fileInfo != null) {
                    checkAndReturn(fileInfo.dir);
                }
            } catch (IOException e) {
                logger.error("close in error.", e);
            }
        }
    }

    public boolean rename(String from, String to) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            return ftpClient.rename(from, to);
        } catch (Exception e) {
            logger.error("rename failed.", e);
            return false;
        }
    }

    public boolean deleteFile(String ftpFileName) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            logger.info("delete ftp file:{}", ftpFileName);
            return ftpClient.deleteFile(ftpFileName);
        } catch (Exception e) {
            logger.error("rename failed.", e);
            return false;
        }
    }

    public boolean deleteDir(String remotePath) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            logger.info("delete ftp dir:{}", remotePath);
            FTPFile[] ftpFiles = ftpClient.listFiles(remotePath);
            for (int i = 0; ftpFiles != null && i < ftpFiles.length; i++) {
                FTPFile ftpFile = ftpFiles[i];
                if (isDirectory(ftpFile)) {
                    deleteDir(remotePath + "/" + ftpFile.getName());
                } else {
                    deleteFile(remotePath + "/" + ftpFile.getName());
                }
            }
            return true;
        } catch (Exception e) {
            logger.error("deleteDir error!" + remotePath, e);
            return false;
        }
    }

    public boolean uploadFile(String ftpFilePath, File localFile) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            // File check.
            if (!localFile.exists()) {
                logger.error("localFile not exists:" + localFile.getAbsolutePath());
                return false;
            }
            // Upload.
            boolean res = uploadFile(ftpFilePath, new BufferedInputStream(new FileInputStream(localFile)));
            return res;
        } catch (Exception e) {
            logger.error("", e);
            return false;
        }
    }

    public boolean uploadDir(String ftpFilePath, String localPath) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            File file = new File(localPath);
            if (!file.exists()) {
                logger.error("file not exists:" + localPath);
                return false;
            }
            makeDirAndReturn(ftpFilePath);
            File[] files = file.listFiles();
            for (File f : files) {
                if (isDirectory(f)) {
                    uploadDir(ftpFilePath + "/" + f.getName(), f.getPath());
                } else if (f.isFile()) {
                    uploadFile(ftpFilePath + "/" + f.getName(), f);
                }
            }
            return true;
        } catch (Exception e) {
            logger.error("uploadDir error." + localPath, e);
            return false;
        }
    }

    private boolean isDirectory(File file){
        return file.isDirectory() && !".".equals(file.getName()) && !"..".equals(file.getName());
    }
    private boolean isDirectory(FTPFile file){
        return file.isDirectory() && !".".equals(file.getName()) && !"..".equals(file.getName());
    }

    public boolean downloadFile(String ftpFileName, File localFile) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            return downloadFile(ftpFileName, new BufferedOutputStream(new FileOutputStream(localFile)));
        } catch (Exception e) {
            logger.error("downloadFile error." + ftpFileName, e);
            return false;
        }
    }

    public boolean downloadDir(String remotePath, String localPath) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            File file = new File(localPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            FTPFile[] ftpFiles = ftpClient.listFiles(remotePath);
            for (int i = 0; ftpFiles != null && i < ftpFiles.length; i++) {
                FTPFile ftpFile = ftpFiles[i];
                if (isDirectory(ftpFile)) {
                    downloadDir(remotePath + "/" + ftpFile.getName(), localPath + "/" + ftpFile.getName());
                } else {
                    downloadFile(remotePath + "/" + ftpFile.getName(), new File(localPath + "/" + ftpFile.getName()));
                }
            }
            return true;
        } catch (Exception e) {
            logger.error("downloadDir error!" + remotePath, e);
            return false;
        }
    }

    public List<String> listFileNames(String filePath) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return null;
        }
//        changeWorkingDirectory(filePath);
        List<String> fileList = new ArrayList<String>();
        try {
            FTPFile[] ftpFiles = ftpClient.listFiles(filePath);
            for (int i = 0; ftpFiles != null && i < ftpFiles.length; i++) {
                FTPFile ftpFile = ftpFiles[i];
                if (ftpFile.isFile()) {
                    fileList.add(ftpFile.getName());
                }
            }
        } catch (Exception e) {
            logger.error("listFileNames error!", e);
        }
//        checkAndReturn(filePath);
        return fileList;
    }

    private FileInfo makeDirAndGetFileInfo(String filePath) {
        if (filePath.endsWith(BaseConstants.SYSTEM_SYMBOL_PATH)) {
            filePath = filePath.substring(0, filePath.length() - 1);
        }
        if (filePath.startsWith(BaseConstants.SYSTEM_SYMBOL_PATH)) {
            filePath = filePath.substring(1, filePath.length());
        }
        //mkdirs
        FileInfo fileInfo = new FileInfo();
        try {
            if (filePath.contains(BaseConstants.SYSTEM_SYMBOL_PATH)) {
                int lastSp = filePath.lastIndexOf(BaseConstants.SYSTEM_SYMBOL_PATH);
                String dir = filePath.substring(0, lastSp);
                fileInfo.dir = dir;
                if (!ftpClient.changeWorkingDirectory(dir)) {
                    //创建成功返回true，失败（已存在）返回false
                    boolean mkres = makeDirAndGoto(dir);
                    logger.info("change:{} false, mkres:{},current dir:{}", dir, mkres, printWorkingDirectory());
                }
                fileInfo.fileName = filePath.substring(lastSp + 1, filePath.length());
            } else {
                fileInfo.fileName = filePath;
            }
            return fileInfo;
        } catch (Exception e) {
            logger.error("makeDirAndGetFileInfo error." + filePath, e);
            return null;
        }
    }

    private boolean checkAndReturn(String filePath) {
        if (filePath.endsWith(BaseConstants.SYSTEM_SYMBOL_PATH)) {
            filePath = filePath.substring(0, filePath.length() - 1);
        }
        if (filePath.startsWith(BaseConstants.SYSTEM_SYMBOL_PATH)) {
            filePath = filePath.substring(1, filePath.length());
        }
        //mkdirs
        try {
            if (filePath.contains(BaseConstants.SYSTEM_SYMBOL_PATH)) {
                StringBuilder dir = new StringBuilder();
                String[] dirs = filePath.split("\\/");
                for (int i = 0; i < dirs.length; i++) {
                    dir.append("../");
                }
                boolean res = ftpClient.changeWorkingDirectory(dir.toString());
                logger.info("return: cd {} {}, current dir:{} ", dir, res, printWorkingDirectory());
            }
            return true;
        } catch (Exception e) {
            logger.error("checkAndReturnFtpFileName error." + filePath, e);
            return false;
        }
    }

    public List<FTPFile> listFiles(String filePath) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return null;
        }
        List<FTPFile> fileList = null;
        try {
            FTPFile[] ftpFiles = ftpClient.listFiles(filePath);
            for (int i = 0; ftpFiles != null && i < ftpFiles.length; i++) {
                FTPFile ftpFile = ftpFiles[i];
                fileList.add(ftpFile);
            }
        } catch (Exception e) {
            logger.error("listFiles error!", e);
        }
        return fileList;
    }

    public boolean sendSiteCommand(String args) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }

        try {
            return ftpClient.sendSiteCommand(args);
        } catch (IOException e) {
            logger.error("sendSiteCommand error!", e);
            return false;
        }
    }

    public String printWorkingDirectory() {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return "";
        }
        try {
            return ftpClient.printWorkingDirectory();
        } catch (Exception e) {
            logger.error("printWorkingDirectory error!", e);
            return "";
        }
    }

    public boolean changeWorkingDirectory(String dir) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            return ftpClient.changeWorkingDirectory(dir);
        } catch (Exception e) {
            logger.error("changeWorkingDirectory error!", e);
        }
        return false;
    }

    public boolean changeToParentDirectory() {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        try {
            return ftpClient.changeToParentDirectory();
        } catch (Exception e) {
            logger.error("changeToParentDirectory error!", e);
            return false;
        }
    }

    public String printParentDirectory() {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return "";
        }
        String w = printWorkingDirectory();
        changeToParentDirectory();
        String p = printWorkingDirectory();
        changeWorkingDirectory(w);
        return p;
    }

    public boolean makeDirAndGoto(String ftpFilePath) {
        if (!ftpClient.isConnected()) {
            logger.error("not connected.");
            return false;
        }
        if (ftpFilePath.contains(BaseConstants.SYSTEM_SYMBOL_PATH)) {
            String[] dirs = ftpFilePath.split("\\/");
            boolean res = false;
            for (String dir : dirs) {
                res = makeDirAndGoto(dir);
            }
            return res;
        } else {
            try {
                ftpClient.makeDirectory(ftpFilePath);
                return ftpClient.changeWorkingDirectory(ftpFilePath);
            } catch (IOException e) {
                logger.error("makeDirectory error!" + ftpFilePath, e);
                return false;
            }
        }
    }

    public boolean makeDirAndReturn(String ftpFilePath) {
        makeDirAndGoto(ftpFilePath);
        return checkAndReturn(ftpFilePath);
    }

    class FileInfo {
        public String dir = ".";
        public String fileName;
    }

}