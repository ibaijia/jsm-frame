package cn.ibaijia.jsm.cache.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

/**
 * @author longzl
 */
public interface JedisCmd<T> {
    /**
     * 执行方法
     *
     * @param cluster      是否集群模式
     * @param jedis        集群模式时 值为 null
     * @param jedisCluster 单机模式时 值为 null
     * @return
     * @throws Exception 异常
     */
    T run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception;

    /**
     * 异常
     *
     * @param throwable
     */
    default void throwable(Throwable throwable) {
        // do nothing
    }
}
