package cn.ibaijia.jsm.cache.jedis;

import redis.clients.jedis.commands.JedisCommands;

/**
 * @author longzl
 */
public interface JedisSimpleCmd<T> {
    /**
     * 执行方法
     *
     * @param jedis 可能是Jedis也可能是JedisCluster 实例
     * @return
     */
    T run(JedisCommands jedis);

    /**
     * 异常
     *
     * @param throwable
     */
    default void throwable(Throwable throwable) {
        // do nothing
    }
}
