package cn.ibaijia.jsm.cache;

import com.fasterxml.jackson.core.type.TypeReference;

import java.lang.reflect.Type;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 10:48
 */
public interface CacheL2 extends CacheServiceCmd {

    /**
     * 获取 缓存
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T get(String key, Class<T> clazz);

    /**
     * 获取 缓存
     * @param key
     * @param typeReference
     * @param <T>
     * @return
     */
    <T> T get(String key, TypeReference<T> typeReference);

    /**
     * 获取 缓存
     * @param key
     * @param type
     * @param <T>
     * @return
     */
    <T> T get(String key, Type type);

    /**
     * 启动监听器
     * @param cacheL1
     */
    void startListener(CacheL1 cacheL1);

}
