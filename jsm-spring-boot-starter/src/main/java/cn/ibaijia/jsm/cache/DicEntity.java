package cn.ibaijia.jsm.cache;

/**
 * @Author: LongZL
 * @Date: 2022/4/20 16:56
 */
public class DicEntity {

    public static final int ACTION_HDEL = 0;
    public static final int ACTION_HSET = 1;

    public DicEntity() {
    }

    public DicEntity(int action, String key, String field, String value) {
        this.action = action;
        this.key = key;
        this.field = field;
        this.value = value;
    }

    public DicEntity(int action, String key, String field) {
        this.action = action;
        this.key = key;
        this.field = field;
    }

    public int action = ACTION_HSET;
    public String key;
    public String field;
    public String value;

}
