package cn.ibaijia.jsm.cache;

import cn.ibaijia.jsm.utils.LogUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 10:55
 */
public class CaffeineCacheL1 implements CacheL1 {

    private Logger logger = LogUtil.log(this.getClass());

    Cache<String, Object> cache;

    private Long duration;

    private Integer init;

    private Long max;

    public CaffeineCacheL1(Long duration, Integer init, Long max) {
        this.duration = duration;
        this.init = init;
        this.max = max;
        init();
    }

    private void init() {
        cache = Caffeine.newBuilder()
                .expireAfterWrite(duration, TimeUnit.SECONDS)
                .initialCapacity(init)
                .maximumSize(max)
                .build();
    }

    @Override
    public boolean set(String key, int seconds, Object object) {
        //不支持 do nothing
        return true;
    }

    @Override
    public boolean set(String key, Object value) {
        try {
            cache.put(key, value);
            return true;
        } catch (Exception e) {
            logger.error("CaffeineCacheL1 put error. key:" + key, e);
            return false;
        }
    }

    @Override
    public <T> T get(String key) {
        try {
            return (T) cache.getIfPresent(key);
        } catch (Exception e) {
            logger.error("CaffeineCacheL1 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public boolean remove(String key) {
        try {
            logger.debug("remove key:{}", key);
            cache.invalidate(key);
            return true;
        } catch (Exception e) {
            logger.error("CaffeineCacheL1 remove error. key:" + key, e);
            return false;
        }
    }

    @Override
    public String hget(String key, String field) {
        Map<String, String> map = get(key);
        if (map != null) {
            return map.get(field);
        }
        return null;
    }

    @Override
    public boolean hset(String key, String field, String value) {
        try {
            Map<String, String> map = get(key);
            if (map == null) {
                map = new HashMap<>(16);
            }
            map.put(field, value);
            cache.put(key, map);
            return true;
        } catch (Exception e) {
            logger.error("hset error!", e);
            return false;
        }
    }

    @Override
    public boolean hdel(String key, String field) {
        try {
            Map<String, String> map = get(key);
            if (map != null) {
                map.remove(field);
            }
        } catch (Exception e) {
            logger.error("hdel error!", e);
            return false;
        }
        return true;
    }

    @Override
    public Long expire(String key, int seconds) {
        return null;
    }

    @Override
    public Boolean exists(String key) {
        return cache.getIfPresent(key) != null;
    }
}
