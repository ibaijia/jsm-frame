package cn.ibaijia.jsm.cache.jedis;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import com.mchange.v2.ser.SerializableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.params.SetParams;

import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author longzl
 */
public class JedisService implements DisposableBean {
    private final Logger logger = LoggerFactory.getLogger(JedisService.class);

    private static final String OK = "OK";

    private JedisDataSource redisDataSource;

    private JedisCluster jedisCluster;

    private boolean cluster;

    public JedisService(JedisDataSource redisDataSource, JedisCluster jedisCluster, boolean cluster) {
        this.redisDataSource = redisDataSource;
        this.jedisCluster = jedisCluster;
        this.cluster = cluster;
    }

    /**
     * 带有效期的设值（原子性）
     *
     * @param key
     * @param seconds
     * @param value
     */
    public String setex(String key, int seconds, String value) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.setex(key, seconds, value);
                } else {
                    return jedis.setex(key, seconds, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec setex", throwable.getMessage());
            }
        });
    }

    /**
     * 不存在,则设置
     *
     * @param key
     * @param seconds
     * @param value
     */
    public String setnx(String key, int seconds, String value) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                SetParams setParams = new SetParams().ex(seconds).nx();
                if (cluster) {
                    return jedisCluster.set(key, value, setParams);
                } else {
                    return jedis.set(key, value, setParams);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec setnx", throwable.getMessage());
            }
        });
    }

    /**
     * 不存在,则设置
     *
     * @param key
     * @param value
     */
    public Long setnx(String key, String value) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.setnx(key, value);
                } else {
                    return jedis.setnx(key, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec setnx", throwable.getMessage());
            }
        });
    }

    /**
     * 设置单个值
     *
     * @param key
     * @param value
     */
    public String set(String key, String value) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.set(key, value);
                } else {
                    return jedis.set(key, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec set", throwable.getMessage());
            }
        });
    }

    /**
     * 设置map值
     *
     * @param key
     * @param map
     */
    public String hmset(String key, Map<String, String> map) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.hmset(key, map);
                } else {
                    return jedis.hmset(key, map);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec hmset", throwable.getMessage());
            }
        });
    }

    /**
     * 获取map值
     *
     * @param key
     * @param fields
     */
    public List<String> hmget(String key, String... fields) {
        return exec(new JedisCmd<List<String>>() {
            @Override
            public List<String> run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.hmget(key, fields);
                } else {
                    return jedis.hmget(key, fields);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec hmget", throwable.getMessage());
            }
        });
    }

    /**
     * 设置单个值
     *
     * @param key
     * @param value
     */
    public Long hset(String key, String field, String value) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.hset(key, field, value);
                } else {
                    return jedis.hset(key, field, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec hset", throwable.getMessage());
            }
        });
    }

    /**
     * 获取单个值
     *
     * @param key
     */
    public String hget(String key, String field) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.hget(key, field);
                } else {
                    return jedis.hget(key, field);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec hget", throwable.getMessage());
            }
        });
    }

    /**
     * 删除多个值
     *
     * @param key
     */
    public Long hdel(String key, String... fields) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.hdel(key, fields);
                } else {
                    return jedis.hdel(key, fields);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec hdel", throwable.getMessage());
            }
        });
    }

    /**
     * 获取单个值
     *
     * @param key
     */
    public String get(String key) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.get(key);
                } else {
                    return jedis.get(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec get", throwable.getMessage());
            }
        });
    }

    public Boolean exists(String key) {
        return exec(new JedisCmd<Boolean>() {
            @Override
            public Boolean run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.exists(key);
                } else {
                    return jedis.exists(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec exists", throwable.getMessage());
            }
        });
    }

    public String type(String key) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.type(key);
                } else {
                    return jedis.type(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec type", throwable.getMessage());
            }
        });
    }

    /**
     * 在某段时间后失效
     *
     * @param key
     * @param seconds
     */
    public Long expire(String key, int seconds) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.expire(key, seconds);
                } else {
                    return jedis.expire(key, seconds);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec expire", throwable.getMessage());
            }
        });
    }

    /**
     * 在某个时间点失效
     *
     * @param key
     * @param unixTime
     */
    public Long expireAt(String key, long unixTime) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.expireAt(key, unixTime);
                } else {
                    return jedis.expireAt(key, unixTime);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec expireAt", throwable.getMessage());
            }
        });
    }

    public Long ttl(String key) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.ttl(key);
                } else {
                    return jedis.ttl(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec ttl", throwable.getMessage());
            }
        });
    }

    public long setRange(String key, long offset, String value) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.setrange(key, offset, value);
                } else {
                    return jedis.setrange(key, offset, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec setrange", throwable.getMessage());
            }
        });
    }

    public String getRange(String key, long startOffset, long endOffset) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.getrange(key, startOffset, endOffset);
                } else {
                    return jedis.getrange(key, startOffset, endOffset);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec getrange", throwable.getMessage());
            }
        });
    }

    public Long del(String key) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.del(key);
                } else {
                    return jedis.del(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec del", throwable.getMessage());
            }
        });
    }


    /**
     * 获取left弹出值
     *
     * @param key
     */
    public String lpop(String key) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.lpop(key);
                } else {
                    return jedis.lpop(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec lpop", throwable.getMessage());
            }
        });
    }

    /**
     * 获取right弹出值
     *
     * @param key
     */
    public String rpop(String key) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.rpop(key);
                } else {
                    return jedis.rpop(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec rpop", throwable.getMessage());
            }
        });
    }

    /**
     * 获取left弹出值
     *
     * @param key
     * @param values
     */
    public Long lpush(String key, String... values) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.lpush(key, values);
                } else {
                    return jedis.lpush(key, values);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec lpush", throwable.getMessage());
            }
        });
    }

    /**
     * 获取left弹出值
     *
     * @param key
     * @param values
     */
    public Long rpush(String key, String... values) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.rpush(key, values);
                } else {
                    return jedis.rpush(key, values);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec rpush", throwable.getMessage());
            }
        });
    }


    /**
     * 获取key len弹出值
     *
     * @param key
     */
    public Long llen(String key) {
        return exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.llen(key);
                } else {
                    return jedis.llen(key);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec llen", throwable.getMessage());
            }
        });
    }

    public String setObject(String key, Serializable value) throws NotSerializableException {
        return set(key.getBytes(), SerializableUtils.toByteArray(value));
    }

    public String set(byte[] key, byte[] value) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.set(key, value);
                } else {
                    return jedis.set(key, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec set", throwable.getMessage());
            }
        });
    }

    public String setexObject(String key, int seconds, Serializable value) throws NotSerializableException {
        return setex(key.getBytes(), seconds, SerializableUtils.toByteArray(value));

    }

    public String setex(byte[] key, int seconds, byte[] value) {
        return exec(new JedisCmd<String>() {
            @Override
            public String run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.setex(key, seconds, value);
                } else {
                    return jedis.setex(key, seconds, value);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec setex", throwable.getMessage());
            }
        });
    }

    public <T> T getObject(String key) {
        byte[] result = exec(new JedisCmd<byte[]>() {
            @Override
            public byte[] run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.get(key.getBytes());
                } else {
                    return jedis.get(key.getBytes());
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                addAlarm("exec get", throwable.getMessage());
            }
        });

        if (result != null) {
            try {
                return (T) SerializableUtils.fromByteArray(result);
            } catch (Exception e) {
                logger.error("getObject SerializableUtils.fromByteArray error.",e);
            }
        }
        return null;
    }


    public <T> T exec(JedisCmd<T> jedisCmd) {
        Jedis jedis = null;
        T result = null;
        if (!cluster) {
            jedis = redisDataSource.getJedis();
        }
        try {
            result = jedisCmd.run(cluster, jedis, jedisCluster);
        } catch (Exception e) {
            logger.error("exec jedisCmd error.", e);
            addAlarm("exec jedisCmd", e.getMessage());
            jedisCmd.throwable(e);
        } finally {
            if (!cluster) {
                redisDataSource.returnResource(jedis);
            }
        }
        return result;
    }

    public <T> T exec(JedisSimpleCmd<T> jedisCmd) {
        Jedis jedis = null;
        T result = null;
        if (!cluster) {
            jedis = redisDataSource.getJedis();
        }
        try {
            result = jedisCmd.run(cluster ? jedisCluster : jedis);
        } catch (Exception e) {
            logger.error("exec jedisCmd error.", e);
            addAlarm("exec jedisSimpleCmd", e.getMessage());
            jedisCmd.throwable(e);
        } finally {
            if (!cluster) {
                redisDataSource.returnResource(jedis);
            }
        }
        return result;
    }

    /**
     * @param lockKey 尝试获取 5分钟 最长能锁1小时
     */
    public boolean lock(String lockKey) {
        String sid = WebContext.currentSessionId();
        return lock(lockKey, StringUtil.isEmpty(sid) ? "-1" : sid, 30000, 3600);
    }

    public boolean lock(String lockKey, int retryTimes, int lockTimes) {
        String sid = WebContext.currentSessionId();
        return lock(lockKey, StringUtil.isEmpty(sid) ? "-1" : sid, retryTimes, lockTimes);
    }

    /**
     * 分布式 加锁
     *
     * @param lockKey
     * @param retryTimes
     */
    public synchronized boolean lock(String lockKey, String requestId, int retryTimes, int lockTimes) {
        String val = setnx(lockKey, lockTimes, requestId);
        boolean res = true;
        if (!OK.equals(val)) {
            int i = 0;
            while (true) {
                val = setnx(lockKey, lockTimes, requestId);
                if ("OK".equals(val)) {
                    break;
                }
                if (i == retryTimes) {
                    logger.info("wait lock time out retry times {}", retryTimes);
                    res = false;
                    break;
                }
                try {
                    this.wait(BaseConstants.LOCK_SPIN_MILLIS);
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
                i++;
            }
        }
        return res;
    }

    /**
     * 分页式 解锁
     *
     * @param lockKey
     */
    public synchronized void unlock(String lockKey) {
        del(lockKey);
    }


    public void addAlarm(String method, String errorMessage) {
        String logType = AppContext.get(AppContextKey.ALARM_TYPE, BaseConstants.STRATEGY_TYPE_NONE);
        if(BaseConstants.STRATEGY_TYPE_REDIS.equals(logType) || BaseConstants.STRATEGY_TYPE_NONE.equals(logType)){
            System.out.println(method + "," + errorMessage);
            return;
        }
        SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JEDIS_SERVICE, method + "," + errorMessage));
    }

    @Override
    public void destroy() throws Exception {
//        if (redisDataSource != null && redisDataSource.getJedisPool() != null) {
//            try {
//                redisDataSource.getJedisPool().close();
//            } catch (Exception e) {
//                logger.error("redisDataSource.getJedisPool().close() error.", e);
//            }
//        }
//        if (jedisCluster != null) {
//            try {
//                jedisCluster.close();
//            } catch (Exception e) {
//                logger.error("jedisCluster.close() error.", e);
//            }
//        }
    }
}