package cn.ibaijia.jsm.cache;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;

/**
 * @Author: LongZL
 * @Date: 2022/6/1 10:24
 */
public interface CacheableObject {


    /**
     * 是否 来至缓存的对象
     * @return
     */
    @JSONField(serialize = false)
    @JsonIgnore
    @ApiParam(hidden = true)
    boolean isCacheObject();

    /**
     * 标识为 是 来至缓存的对象
     * @param cacheObject
     */
    void setCacheObject(boolean cacheObject);

}
