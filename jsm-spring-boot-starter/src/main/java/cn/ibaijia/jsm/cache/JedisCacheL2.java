package cn.ibaijia.jsm.cache;

import cn.ibaijia.jsm.cache.jedis.JedisCmd;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPubSub;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 11:13
 */
public class JedisCacheL2 implements CacheL2 {

    private Logger logger = LogUtil.log(this.getClass());

    private JedisService jedisService;

    private Integer dbIdx;

    private boolean removeOnUpdate;

    private String clusterChannel;

    public JedisCacheL2(JedisService jedisService, Integer dbIdx, boolean removeOnUpdate, String clusterChannel) {
        this.jedisService = jedisService;
        this.dbIdx = dbIdx;
        this.removeOnUpdate = removeOnUpdate;
        this.clusterChannel = clusterChannel;
    }

    @Override
    public void startListener(CacheL1 cacheL1) {
        listenKeyExpiredAndSet(cacheL1);
        listenOthers(cacheL1);
    }

    private void listenKeyExpiredAndSet(CacheL1 cacheL1) {
        ExecutorService executor = new ThreadPoolExecutor(1, 1, 0L,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), r -> new Thread(r,"jedis-l2-listen1"));
        executor.execute(() -> jedisService.exec(new JedisCmd<>() {
            @Override
            public void throwable(Throwable throwable) {
                logger.error("JedisCacheL2 Listener error.", throwable);
            }

            @Override
            public Object run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                String expiredPattern = String.format("__keyevent@%s__:expired", dbIdx);
                String setPattern = String.format("__keyevent@%s__:set", dbIdx);
                List<String> patterns = new ArrayList<>();
                patterns.add(expiredPattern);
                if (removeOnUpdate) {
                    patterns.add(setPattern);
                }
                JedisPubSub jedisPubSub = new JedisPubSub() {
                    @Override
                    public void onPSubscribe(String pattern, int subscribedChannels) {
                        super.onPSubscribe(pattern, subscribedChannels);
                        logger.debug("JedisCacheL2 listener onPSubscribe:" + pattern + "|" + subscribedChannels);
                    }

                    @Override
                    public void onPMessage(String pattern, String channel, String message) {
                        super.onPMessage(pattern, channel, message);
                        logger.debug("JedisCacheL2 listener onPMessage:" + pattern + "|" + channel + "|" + message);
                        cacheL1.remove(message);
                    }
                };

                if (cluster) {
                    jedisCluster.psubscribe(jedisPubSub, patterns.toArray(new String[patterns.size()]));
                } else {
                    jedis.psubscribe(jedisPubSub, patterns.toArray(new String[patterns.size()]));
                }
                return null;
            }
        }));
    }

    /**
     * 监听CacheService 目前支持了 hset hdel
     **/
    private void listenOthers(CacheL1 cacheL1) {
        ExecutorService executor = new ThreadPoolExecutor(1, 1, 0L,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), r -> new Thread(r,"jedis-l2-listen2"));
        executor.execute(() -> jedisService.exec(new JedisCmd<>() {
            @Override
            public void throwable(Throwable throwable) {
                logger.error("JedisCacheL2 Listener error.", throwable);
            }

            @Override
            public Object run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                JedisPubSub jedisPubSub = new JedisPubSub() {
                    @Override
                    public void onSubscribe(String channel, int subscribedChannels) {
                        logger.debug("JedisCacheL2 listener common onSubscribe:{},{}", channel, subscribedChannels);
                    }

                    @Override
                    public void onMessage(String channel, String message) {
                        logger.debug("JedisCacheL2 listener common onMessage:{},{}", channel, message);
                        DicEntity dicEntity = JsonUtil.parseObject(message, DicEntity.class);
                        if (dicEntity != null) {
                            if (dicEntity.action == DicEntity.ACTION_HDEL) {
                                cacheL1.hdel(dicEntity.key, dicEntity.field);
                            } else if (dicEntity.action == DicEntity.ACTION_HSET) {
                                cacheL1.hset(dicEntity.key, dicEntity.field, dicEntity.value);
                            } else {
                                logger.error("unkown action.");
                            }
                        }
                    }
                };
                if (cluster) {
                    jedisCluster.subscribe(jedisPubSub, clusterChannel);
                } else {
                    jedis.subscribe(jedisPubSub, clusterChannel);
                }
                return null;
            }
        }));
    }

    @Override
    public String hget(String key, String field) {
        return jedisService.hget(key, field);
    }

    @Override
    public boolean hset(String key, String field, String value) {
        jedisService.hset(key, field, value);
        jedisService.exec(new JedisCmd<Object>() {
            @Override
            public Object run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                DicEntity dicEntity = new DicEntity(DicEntity.ACTION_HSET, key, field, value);
                if (cluster) {
                    jedisCluster.publish(clusterChannel, JsonUtil.toJsonString(dicEntity));
                } else {
                    jedis.publish(clusterChannel, JsonUtil.toJsonString(dicEntity));
                }
                return null;
            }
        });
        return true;
    }

    @Override
    public boolean hdel(String key, String field) {
        jedisService.hdel(key, field);
        jedisService.exec(new JedisCmd<Object>() {
            @Override
            public Object run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                DicEntity dicEntity = new DicEntity(DicEntity.ACTION_HDEL, key, field);
                if (cluster) {
                    jedisCluster.publish(clusterChannel, JsonUtil.toJsonString(dicEntity));
                } else {
                    jedis.publish(clusterChannel, JsonUtil.toJsonString(dicEntity));
                }
                return null;
            }
        });
        return true;
    }

    @Override
    public Long expire(String key, int seconds) {
        return jedisService.expire(key, seconds);
    }

    @Override
    public Boolean exists(String key) {
        return jedisService.exists(key);
    }

    @Override
    public boolean set(String key, Object value) {
        try {
            String val = null;
            if (value instanceof String) {
                val = (String) value;
            } else {
                val = JsonUtil.toJsonString(value);
            }
            jedisService.set(key, val);
            return true;
        } catch (Exception e) {
            logger.error("JedisCacheL2 put error. key:" + key, e);
            return false;
        }
    }

    @Override
    public boolean set(String key, int seconds, Object value) {
        try {
            String val = null;
            if (value instanceof String) {
                val = (String) value;
            } else {
                val = JsonUtil.toJsonString(value);
            }
            jedisService.setex(key, seconds, val);
            return true;
        } catch (Exception e) {
            logger.error("JedisCacheL2 put error. key:" + key, e);
            return false;
        }
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        try {
            String res = jedisService.get(key);
            if (StringUtil.isEmpty(res)) {
                return null;
            }
            return JsonUtil.parseObject(res, clazz);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public <T> T get(String key, TypeReference<T> typeReference) {
        try {
            String res = jedisService.get(key);
            if (StringUtil.isEmpty(res)) {
                return null;
            }
            return JsonUtil.parseObject(res, typeReference);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public <T> T get(String key, Type type) {
        try {
            String res = jedisService.get(key);
            if (StringUtil.isEmpty(res)) {
                return null;
            }
            return JsonUtil.parseObject(res, type);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public String get(String key) {
        try {
            return jedisService.get(key);
        } catch (Exception e) {
            logger.error("JedisCacheL2 get error. key:" + key, e);
            return null;
        }
    }

    @Override
    public boolean remove(String key) {
        try {
            jedisService.del(key);
            return true;
        } catch (Exception e) {
            logger.error("JedisCacheL2 remove error. key:" + key, e);
            return false;
        }
    }

}
