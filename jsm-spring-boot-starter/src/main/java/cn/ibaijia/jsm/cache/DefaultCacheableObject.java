package cn.ibaijia.jsm.cache;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;

/**
 * @Author: LongZL
 * @Date: 2022/6/1 10:24
 */
public class DefaultCacheableObject implements CacheableObject  {

    private boolean cache;

    /**
     * 是否 来至缓存的对象
     *
     * @return
     */
    @Override
    @JSONField(serialize = false)
    @JsonIgnore
    @ApiParam(hidden = true)
    public boolean isCacheObject() {
        return cache;
    }

    /**
     * 标识为 是 来至缓存的对象
     *
     * @param isCache
     */
    @Override
    public void setCacheObject(boolean isCache) {
        this.cache = isCache;
    }

}
