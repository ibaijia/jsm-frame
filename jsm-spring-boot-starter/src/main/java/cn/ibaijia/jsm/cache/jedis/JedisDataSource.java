package cn.ibaijia.jsm.cache.jedis;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author longzl
 */
public class JedisDataSource {
    private Logger logger = LoggerFactory.getLogger(JedisDataSource.class);

    private JedisPool jedisPool;

    public Jedis getJedis() {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis;
        } catch (Exception e) {
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_JEDIS_SERVICE, "getJedis error." + e.getMessage()));
            logger.error("getJedis error:", e);
            if (null != jedis) {
                jedis.close();
            }
        }
        return null;
    }

    public void returnResource(Jedis jedis) {
        jedis.close();
    }

    public void returnResource(Jedis jedis, boolean broken) {
        jedis.close();
    }

    public JedisPool getJedisPool() {
        return jedisPool;
    }

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }
}