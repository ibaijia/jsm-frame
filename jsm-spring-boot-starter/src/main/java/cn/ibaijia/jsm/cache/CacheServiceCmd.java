package cn.ibaijia.jsm.cache;

/**
 * @Author: LongZL
 * @Date: 2022/1/21 10:48
 */
public interface CacheServiceCmd {

    /**
     * 设置指定key value和过期时间
     * @param key
     * @param seconds
     * @param value
     * @return
     */
    boolean set(String key, int seconds, Object value);

    /**
     * 设置key value
     * @param key
     * @param value
     * @return
     */
    boolean set(String key, Object value);

    /**
     * 获取指定key
     * @param key
     * @param <T>
     * @return
     */
    <T> T get(String key);

    /**
     * 删除指定key
     * @param key
     * @return
     */
    boolean remove(String key);

    /**
     * map中获取一个字段
     * @param key
     * @param field
     * @return
     */
    String hget(String key, String field);

    /**
     * map中设置一个字段
     * @param key
     * @param field
     * @param value
     * @return
     */
    boolean hset(String key, String field, String value);

    /**
     * map中删除一个字段
     * @param key
     * @param field
     * @return
     */
    boolean hdel(String key, String field);

    /**
     * 设置过期时间
     * @param key
     * @param seconds
     * @return
     */
    Long expire(String key, int seconds);

    /**
     * 是否存在key
     * @param key
     * @return
     */
    Boolean exists(String key);

}
