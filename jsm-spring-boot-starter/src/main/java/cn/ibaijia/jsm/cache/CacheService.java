package cn.ibaijia.jsm.cache;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.exception.NotSupportException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.DisposableBean;

import java.lang.reflect.Type;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Caffeine + Redis(异步)
 * @author longzl
 */
public class CacheService extends BaseService implements CacheServiceCmd, DisposableBean {

    private CacheL1 cacheL1;

    private CacheL2 cacheL2;

    private ThreadPoolExecutor syncThreadPool;

    public CacheService(CacheL1 cacheL1, CacheL2 cacheL2) {
        this.cacheL1 = cacheL1;
        this.cacheL2 = cacheL2;
        this.init();
    }

    private void init() {
        Integer threadNumber = AppContext.getAsInteger(AppContextKey.CACHE_SERVICE_ASYNC_THREAD, 1);
        logger.info("CacheService cacheL2 use async thread number:{}", threadNumber);
        syncThreadPool = new ThreadPoolExecutor(threadNumber, threadNumber, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
            byte index = 0;

            @Override
            public Thread newThread(Runnable runnable) {
                return new Thread(runnable, "cache-pool" + (++index));
            }
        });
        cacheL2.startListener(cacheL1);
    }

    private void close() {
        if (syncThreadPool != null) {
            syncThreadPool.shutdown();
        }
    }

    @Override
    public boolean set(String key, Object value) {
        cacheL1.set(key, value);
        syncThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                cacheL2.set(key, value);
            }
        });
        return false;
    }

    @Override
    public boolean set(String key, int seconds, Object value) {
        syncThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                cacheL2.set(key, seconds, value);
            }
        });
        return true;
    }

    @Override
    public <T> T get(String key) {
        throw new NotSupportException("cache service not support get(String key).");
    }

    @Override
    public boolean remove(String key) {
        cacheL1.remove(key);
        return cacheL2.remove(key);
    }

    public <T> T get(String key, Class<T> clazz) {
        T result = getFromL1(key);
        if (result == null) {
            result = getFromL2(key, clazz);
            logger.debug("getFromL2");
        } else {
            logger.debug("getFromL1");
        }
        return result;
    }

    public <T> T get(String key, TypeReference<T> typeReference) {
        T result = getFromL1(key);
        if (result == null) {
            result = getFromL2(key, typeReference);
            logger.debug("getFromL2");
        } else {
            logger.debug("getFromL1");
        }
        return result;
    }

    public <T> T get(String key, Type type) {
        T result = getFromL1(key);
        if (result == null) {
            result = getFromL2(key, type);
            logger.debug("getFromL2");
        } else {
            logger.debug("getFromL1");
        }
        return result;
    }

    @Override
    public String hget(String key, String field) {
        String res = cacheL1.hget(key, field);
        if (res == null) {
            res = cacheL2.hget(key, field);
            if (res != null) {
                cacheL1.hset(key, field, res);
            }
        }
        return res;
    }

    @Override
    public boolean hdel(String key, String field) {
        boolean res = cacheL1.hdel(key, field);
        res = cacheL2.hdel(key, field);
        return res;
    }

    @Override
    public Long expire(String key, int seconds) {
        return cacheL2.expire(key, seconds);
    }

    @Override
    public Boolean exists(String key) {
        return cacheL2.exists(key);
    }

    @Override
    public boolean hset(String key, String field, String value) {
        boolean res = cacheL1.hset(key, field, value);
        res = cacheL2.hset(key, field, value);
        return res;
    }

    private <T> T getFromL1(String key) {
        return cacheL1.get(key);
    }

    private <T> T getFromL2(String key, Class<T> clazz) {
        T value = cacheL2.get(key, clazz);
        if (value != null) {
            logger.debug("set to l1 key:{}", key);
            cacheL1.set(key, value);
        }
        return value;
    }

    private <T> T getFromL2(String key, TypeReference<T> typeReference) {
        T value = cacheL2.get(key, typeReference);
        if (value != null) {
            logger.debug("set to l1 key:{}", key);
            cacheL1.set(key, value);
        }
        return value;
    }

    private <T> T getFromL2(String key, Type type) {
        T value = cacheL2.get(key, type);
        if (value != null) {
            logger.debug("set to l1 key:{}", key);
            cacheL1.set(key, value);
        }
        return value;
    }

    @Override
    public void destroy() throws Exception {
        close();
    }
}
