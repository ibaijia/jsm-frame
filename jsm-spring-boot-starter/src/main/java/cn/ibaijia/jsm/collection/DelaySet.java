package cn.ibaijia.jsm.collection;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: LongZL
 * @Date: 2022/8/10 14:28
 */
public class DelaySet<T> {

    private Object lock = new Object();
    private Set<T> set = new HashSet<>();

    private String name = "delay-set";
    private long delay = 1000;
    private DelaySetProcessor<T> processor;
    private boolean needClose = false;

    public DelaySet(DelaySetProcessor<T> processor) {
        this.processor = processor;
        this.start();
    }

    public DelaySet(String name, long delay, DelaySetProcessor processor) {
        this.name = name;
        this.delay = delay;
        this.processor = processor;
        this.start();
    }

    private void start() {
        ExecutorService executor = new ThreadPoolExecutor(1, 1, 0L,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), r -> new Thread(r,name));
        executor.execute(() -> {
            while (true) {
                synchronized (lock) {
                    try {
                        lock.wait(delay);
                        if (needClose) {
                            break;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Set<T> data = getData();
                    processor.process(data);
                } catch (Throwable throwable) {
                    processor.throwable(throwable);
                }
            }
        });
    }

    public void stop() {
        this.needClose = true;
        synchronized (lock) {
            lock.notify();
        }
    }

    private Set<T> getData() {
        synchronized (lock) {
            Set<T> data = set;
            this.set = new HashSet<>();
            return data;
        }
    }

    public boolean add(T element) {
        synchronized (lock) {
            return set.add(element);
        }
    }

}

