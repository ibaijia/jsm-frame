package cn.ibaijia.jsm.collection;

import java.util.Map;

/**
 * @author longzl
 */
public interface DelayMapProcessor<K,V> {
    /**
     * 处理
     * @param map
     */
    void process(Map<K,V> map);

    /**
     * 异常
     * @param throwable
     */
    void throwable(Throwable throwable);
}
