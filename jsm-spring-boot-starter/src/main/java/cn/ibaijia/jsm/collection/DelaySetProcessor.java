package cn.ibaijia.jsm.collection;

import java.util.Set;

/**
 * @author longzl
 */
public interface DelaySetProcessor<T> {
    /**
     * 处理
     * @param set
     */
    void process(Set<T> set);

    /**
     * 异常
     * @param throwable
     */
    void throwable(Throwable throwable);
}
