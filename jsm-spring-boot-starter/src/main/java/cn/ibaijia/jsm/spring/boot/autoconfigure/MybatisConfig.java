package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.mybatis.PageInterceptor;
import cn.ibaijia.jsm.mybatis.datasource.DynamicDataSource;
import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.*;
import cn.ibaijia.jsm.utils.BlowFishUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.plugin.Interceptor;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * @author longzl
 */
@Configuration
@EnableConfigurationProperties({MybatisConfigProperties.class, DbMasterProperties.class, DbSlaveProperties.class, DruidProperties.class})
public class MybatisConfig implements InitializingBean {

    private static Logger logger = LogUtil.log(MybatisConfig.class);

    private MybatisConfigProperties mybatisConfigProperties;

    private DbMasterProperties dbMasterProperties;

    private DbSlaveProperties dbSlaveProperties;

    private DruidProperties druidProperties;

    public MybatisConfig(MybatisConfigProperties mybatisConfigProperties, DbMasterProperties dbMasterProperties, DbSlaveProperties dbSlaveProperties, DruidProperties druidProperties) {
        this.mybatisConfigProperties = mybatisConfigProperties;
        this.dbMasterProperties = dbMasterProperties;
        this.dbSlaveProperties = dbSlaveProperties;
        this.druidProperties = druidProperties;
    }

    @Bean(name = {"transactionManager"})
    @ConditionalOnMissingBean(DataSourceTransactionManager.class)
    public DataSourceTransactionManager transactionManager() {
        logger.debug("jsmDataSourceTransactionManager()");
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dynamicDataSource());
        return dataSourceTransactionManager;
    }

    @Bean(name = {"sqlSessionFactory"})
    @ConditionalOnMissingBean(SqlSessionFactoryBean.class)
    public SqlSessionFactoryBean sqlSessionFactory() {
        logger.debug("jsmSqlSessionFactoryBean()");
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setVfs(SpringBootVfs.class);
        if (!StringUtil.isEmpty(mybatisConfigProperties.typeAliasesPackage)) {
            mybatisConfigProperties.typeAliasesPackage = "cn.ibaijia.jsm.context.dao.model," + mybatisConfigProperties.typeAliasesPackage;
        }
        sqlSessionFactoryBean.setTypeAliasesPackage(mybatisConfigProperties.typeAliasesPackage);
        sqlSessionFactoryBean.setDataSource(dynamicDataSource());
        sqlSessionFactoryBean.setMapperLocations(resolveMapperLocations());
        sqlSessionFactoryBean.setPlugins(resolvePlugins());
        return sqlSessionFactoryBean;
    }

    @Bean(name = {"dynamicDataSource"})
    @ConditionalOnMissingBean(DynamicDataSource.class)
    public DynamicDataSource dynamicDataSource() {
        logger.info("createDynamicDataSource");
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        Map<Object, Object> targetDataSources = new HashMap<>(2);
        DruidDataSource master = createMasterBasicDataSource();
        DruidDataSource slave = createSlaveBasicDataSource();
        targetDataSources.put("master", master);
        targetDataSources.put("slave", slave);
        dynamicDataSource.setTargetDataSources(targetDataSources);
        dynamicDataSource.setDefaultTargetDataSource(master);
        return dynamicDataSource;
    }

    private DruidDataSource createMasterBasicDataSource() {
        if (dbSlaveProperties == null || dbMasterProperties.getUrl() == null) {
            logger.warn("db.master not config.");
            return null;
        }
        logger.debug("createMasterBasicDataSource masterUrl:{}", dbMasterProperties.getUrl());
        return createBasicDataSource(dbMasterProperties);
    }

    private DruidDataSource createBasicDataSource(DbProperties dbProperties) {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(dbProperties.getDriverClassName());
        dataSource.setUrl(dbMasterProperties.getUrl());
        dataSource.setUsername(dbProperties.getUsername());
        if (StringUtil.isEmpty(dbProperties.getEncPassword())) {
            dataSource.setPassword(dbProperties.getPassword());
        } else {
            dataSource.setPassword(BlowFishUtil.dec(dbProperties.getEncPassword()));
        }
        dataSource.setMaxActive(dbProperties.getMaxActive());
        dataSource.setMinIdle(dbProperties.getMinIdle());
        dataSource.setValidationQuery(dbProperties.getValidationQuery());
        dataSource.setTestOnBorrow(dbProperties.getTestOnBorrow());
        if(Boolean.TRUE.equals(druidProperties.getEnableMonitor())){
            try {
                dataSource.setFilters(druidProperties.getFilters());
            } catch (SQLException e) {
                logger.error("dataSource.setFilters error:" + druidProperties.getFilters(), e);
            }
            dataSource.setConnectProperties(druidProperties.getConnectionProperties());
        }
        return dataSource;
    }

    private DruidDataSource createSlaveBasicDataSource() {
        if (dbSlaveProperties == null || dbSlaveProperties.getUrl() == null) {
            logger.warn("db.slave not config.");
            return null;
        }
        logger.debug("createSlaveBasicDataSource slaveUrl:{}", dbSlaveProperties.getUrl());
        return createBasicDataSource(dbSlaveProperties);
    }

    public Resource[] resolveMapperLocations() {
        logger.info("resolveMapperLocations :{}", mybatisConfigProperties.mapperLocations);
        ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
        List<String> mapperLocations = new ArrayList<String>();
        mapperLocations.addAll(Arrays.asList(mybatisConfigProperties.mapperLocations));
        List<Resource> resources = new ArrayList<Resource>();
        for (String mapperLocation : mapperLocations) {
            try {
                Resource[] mappers = resourceResolver.getResources(mapperLocation);
                resources.addAll(Arrays.asList(mappers));
            } catch (IOException e) {
                logger.error("resolveMapperLocations error." + mapperLocation, e);
            }
        }
        return resources.toArray(new Resource[resources.size()]);
    }

    public Interceptor[] resolvePlugins() {
        logger.info("resolvePlugins dbDialect:{}", mybatisConfigProperties.pageDbDialect);
        List<Interceptor> list = new ArrayList<>();
        PageInterceptor pageInterceptor = new PageInterceptor();
        Properties properties = new Properties();
        properties.put("dbDialect", mybatisConfigProperties.pageDbDialect);
        pageInterceptor.setProperties(properties);
        list.add(pageInterceptor);
        return list.toArray(new Interceptor[list.size()]);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.setProperty("druid.mysql.usePingMethod","false");
        logger.debug("MybatisConfig afterPropertiesSet");
    }
}