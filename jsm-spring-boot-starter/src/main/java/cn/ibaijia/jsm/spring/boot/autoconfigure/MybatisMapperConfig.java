package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.MybatisConfigProperties;
import cn.ibaijia.jsm.utils.LogUtil;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author longzl
 */
@Configuration
public class MybatisMapperConfig implements InitializingBean {

    private static Logger logger = LogUtil.log(MybatisMapperConfig.class);

    @Bean
    @ConditionalOnMissingBean(MapperScannerConfigurer.class)
    @ConditionalOnProperty(prefix = MybatisConfigProperties.PREFIX, name = "basePackage", matchIfMissing = true)
    public MapperScannerConfigurer mapperScannerConfigurer() {
        logger.debug("mapperScannerConfigurer()");
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("cn.ibaijia.jsm.context.dao.mapper");
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        return mapperScannerConfigurer;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.debug("MybatisMapperConfig afterPropertiesSet");
    }
}