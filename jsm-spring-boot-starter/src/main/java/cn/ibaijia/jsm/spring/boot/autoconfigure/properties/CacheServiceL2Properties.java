package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: LongZL
 * @Date: 2022/6/6 14:56
 */
@ConfigurationProperties(prefix = CacheServiceL2Properties.PREFIX)
public class CacheServiceL2Properties {

    public final static String PREFIX = "jsm.cache.l2";

    public boolean removeOnUpdate = true;

    public String clusterChannel = "jsm-l2-cluster-channel";

    public boolean isRemoveOnUpdate() {
        return removeOnUpdate;
    }

    public void setRemoveOnUpdate(boolean removeOnUpdate) {
        this.removeOnUpdate = removeOnUpdate;
    }

    public String getClusterChannel() {
        return clusterChannel;
    }

    public void setClusterChannel(String clusterChannel) {
        this.clusterChannel = clusterChannel;
    }
}
