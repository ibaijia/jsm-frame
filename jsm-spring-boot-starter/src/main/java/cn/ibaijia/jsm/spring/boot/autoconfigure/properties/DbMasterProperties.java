package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author longzl
 */
@ConfigurationProperties(prefix = "jsm.db.master")
public class DbMasterProperties extends DbProperties{
}