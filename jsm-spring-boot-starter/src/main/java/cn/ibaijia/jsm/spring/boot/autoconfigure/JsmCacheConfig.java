package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.cache.*;
import cn.ibaijia.jsm.cache.jedis.JedisDataSource;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.context.lock.DefaultLockServiceImpl;
import cn.ibaijia.jsm.context.lock.LockService;
import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.*;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.*;

import java.util.HashSet;
import java.util.Set;

/**
 * @author longzl
 */
@Configuration
@EnableConfigurationProperties({JedisProperties.class, JedisPoolProperties.class, CacheServiceL1Properties.class, CacheServiceL2Properties.class,StatJedisProperties.class,StatJedisPoolProperties.class})
public class JsmCacheConfig {

    private Logger logger = LogUtil.log(getClass());

    private JedisProperties jedisProperties;

    private JedisPoolProperties jedisPoolProperties;

    private CacheServiceL1Properties cacheServiceL1Properties;

    private CacheServiceL2Properties cacheServiceL2Properties;

    private StatJedisProperties statJedisProperties;

    private StatJedisPoolProperties statJedisPoolProperties;

    private CacheL1 cacheL1;

    private CacheL2 cacheL2;

    @Autowired
    public JsmCacheConfig(JedisProperties jedisProperties, JedisPoolProperties jedisPoolProperties, CacheServiceL1Properties cacheServiceL1Properties, CacheServiceL2Properties cacheServiceL2Properties,
                          StatJedisProperties statJedisProperties, StatJedisPoolProperties statJedisPoolProperties) {
        this.jedisProperties = jedisProperties;
        this.jedisPoolProperties = jedisPoolProperties;
        this.cacheServiceL1Properties = cacheServiceL1Properties;
        this.cacheServiceL2Properties = cacheServiceL2Properties;
        this.statJedisProperties = statJedisProperties;
        this.statJedisPoolProperties = statJedisPoolProperties;
    }

    @Bean(name = {"cacheService"})
    @ConditionalOnProperty(prefix = JedisProperties.PREFIX, name = "nodes", matchIfMissing = false)
    public CacheService cacheService() {
        CacheService cacheService = new CacheService(cacheL1(),cacheL2());
        return cacheService;
    }

    @Bean(name = {"cacheL1"})
    @ConditionalOnMissingBean(CacheL1.class)
    public CacheL1 cacheL1() {
        if (cacheL1 == null) {
            cacheL1 = new CaffeineCacheL1(cacheServiceL1Properties.live, cacheServiceL1Properties.init, cacheServiceL1Properties.max);
        }
        return cacheL1;
    }

    @Bean(name = {"cacheL2"})
    @ConditionalOnProperty(prefix = JedisProperties.PREFIX, name = "nodes", matchIfMissing = false)
    public CacheL2 cacheL2() {
        if (cacheL2 == null) {
            cacheL2 = new JedisCacheL2(jedisService(), jedisProperties.dbIdx, cacheServiceL2Properties.removeOnUpdate, cacheServiceL2Properties.clusterChannel);
        }
        return cacheL2;
    }


    @Bean(name = {"jedisService"})
    @ConditionalOnProperty(prefix = JedisProperties.PREFIX, name = "nodes", matchIfMissing = false)
    public JedisService jedisService() {
        logger.debug("jedisService()");
        boolean cluster = jedisProperties.nodes.contains(",");
        if (cluster) {
            logger.info("redis is cluster model.");
        }
        JedisService jedisService = null;
        if (cluster) {
            jedisService = new JedisService(null, jedisCluster(), true);
        } else {
            jedisService = new JedisService(jedisDataSource(), null, false);
        }
        return jedisService;
    }


    @Bean(name = {"statJedisService"})
    @ConditionalOnProperty(prefix = StatJedisProperties.PREFIX, name = "nodes", matchIfMissing = false)
    public JedisService statJedisService() {
        logger.debug("statJedisService()");
        boolean cluster = statJedisProperties.nodes.contains(",");
        if (cluster) {
            logger.info("redis is cluster model.");
        }
        JedisService jedisService = null;
        if (cluster) {
            jedisService = new JedisService(null, statJedisCluster(), true);
        } else {
            jedisService = new JedisService(statJedisDataSource(), null, false);
        }
        return jedisService;
    }

    @Bean(name = {"lockService"})
    public LockService lockService() {
        logger.debug("lockService");
        return new DefaultLockServiceImpl(jedisService());
    }

    /**
     * 单机情况下的请求查重缓存
     * @return
     */
    @Bean(name = {"repeatCache"})
    public CaffeineCacheL1 repeatCache() {
        if (!StringUtil.isEmpty(jedisProperties.nodes)) {
            return null;
        }
        return new CaffeineCacheL1(5L, 10, 10000L);
    }

    private JedisCluster jedisCluster() {
        logger.debug("jedisCluster()");
        GenericObjectPoolConfig<Connection> genericObjectPool = createGenericObjectPool();
        Set<HostAndPort> nodeList = createJedisClusterInfo(jedisProperties.nodes);
        logger.debug("jedisCluster(): node:{} prop:{}", JsonUtil.toJsonString(nodeList), JsonUtil.toJsonString(jedisProperties));
        JedisCluster jedisCluster = new JedisCluster(nodeList, jedisProperties.connectionTimeout, jedisProperties.soTimeout, jedisProperties.maxAttempts,
                jedisProperties.password, jedisProperties.clientName, genericObjectPool);
        return jedisCluster;
    }

    private JedisCluster statJedisCluster() {
        logger.debug("statJedisCluster()");
        GenericObjectPoolConfig<Connection> genericObjectPool = createStatGenericObjectPool();
        Set<HostAndPort> nodeList = createJedisClusterInfo(jedisProperties.nodes);
        logger.debug("statJedisCluster(): node:{} prop:{}", JsonUtil.toJsonString(nodeList), JsonUtil.toJsonString(statJedisProperties));
        JedisCluster jedisCluster = new JedisCluster(nodeList, statJedisProperties.connectionTimeout, statJedisProperties.soTimeout, statJedisProperties.maxAttempts,
                statJedisProperties.password, statJedisProperties.clientName, genericObjectPool);
        return jedisCluster;
    }

    private JedisPool jedisPool() {
        String oneUri = jedisProperties.nodes.split(",")[0];
        logger.debug("jedisPool() redis.node:{} ", oneUri);
        GenericObjectPoolConfig<Jedis> poolConfig = this.createJedisPoolConfig();
        HostAndPort hostAndPort = parseHostAndPort(oneUri);
        JedisPool jedisPool = new JedisPool(poolConfig, hostAndPort.getHost(), hostAndPort.getPort(),
                jedisProperties.connectionTimeout, jedisProperties.soTimeout, jedisProperties.password, jedisProperties.dbIdx, jedisProperties.clientName);
        return jedisPool;
    }

    private JedisPool statJedisPool() {
        String oneUri = statJedisProperties.nodes.split(",")[0];
        logger.debug("statJedisPool() redis.node:{} ", oneUri);
        GenericObjectPoolConfig<Jedis> poolConfig = this.createStatJedisPoolConfig();
        HostAndPort hostAndPort = parseHostAndPort(oneUri);
        JedisPool jedisPool = new JedisPool(poolConfig, hostAndPort.getHost(), hostAndPort.getPort(),
                statJedisProperties.connectionTimeout, statJedisProperties.soTimeout, statJedisProperties.password, statJedisProperties.dbIdx, statJedisProperties.clientName);
        return jedisPool;
    }

    private HostAndPort parseHostAndPort(String node) {
        String[] arr = node.split(":");
        String host = arr[0];
        String port = arr.length > 1 ? arr[1] : "6379";
        HostAndPort hostAndPort = new HostAndPort(host, StringUtil.toInteger(port));
        return hostAndPort;
    }

    private JedisDataSource jedisDataSource() {
        JedisDataSource jedisDataSource = new JedisDataSource();
        jedisDataSource.setJedisPool(jedisPool());
        return jedisDataSource;
    }

    private JedisDataSource statJedisDataSource() {
        JedisDataSource jedisDataSource = new JedisDataSource();
        jedisDataSource.setJedisPool(statJedisPool());
        return jedisDataSource;
    }

    private GenericObjectPoolConfig<Connection> createGenericObjectPool() {
        GenericObjectPoolConfig<Connection> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(jedisPoolProperties.maxTotal);
        poolConfig.setMaxIdle(jedisPoolProperties.maxIdle);
        poolConfig.setMinIdle(jedisPoolProperties.minIdle);
        poolConfig.setMaxWaitMillis(jedisPoolProperties.maxWaitMillis);
        poolConfig.setTestOnBorrow(jedisPoolProperties.testOnBorrow);
        poolConfig.setTestOnReturn(jedisPoolProperties.testOnReturn);
        poolConfig.setTestOnCreate(jedisPoolProperties.testOnCreate);
        poolConfig.setTestWhileIdle(jedisPoolProperties.testWhileIdle);
        return poolConfig;
    }

    private GenericObjectPoolConfig<Connection> createStatGenericObjectPool() {
        GenericObjectPoolConfig<Connection> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(statJedisPoolProperties.maxTotal);
        poolConfig.setMaxIdle(statJedisPoolProperties.maxIdle);
        poolConfig.setMinIdle(statJedisPoolProperties.minIdle);
        poolConfig.setMaxWaitMillis(statJedisPoolProperties.maxWaitMillis);
        poolConfig.setTestOnBorrow(statJedisPoolProperties.testOnBorrow);
        poolConfig.setTestOnReturn(statJedisPoolProperties.testOnReturn);
        poolConfig.setTestOnCreate(statJedisPoolProperties.testOnCreate);
        poolConfig.setTestWhileIdle(statJedisPoolProperties.testWhileIdle);
        return poolConfig;
    }

    private GenericObjectPoolConfig<Jedis> createJedisPoolConfig() {
        GenericObjectPoolConfig<Jedis> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(jedisPoolProperties.maxTotal);
        poolConfig.setMaxIdle(jedisPoolProperties.maxIdle);
        poolConfig.setMinIdle(jedisPoolProperties.minIdle);
        poolConfig.setTestOnBorrow(jedisPoolProperties.testOnBorrow);
        poolConfig.setTestOnReturn(jedisPoolProperties.testOnReturn);
        poolConfig.setMaxWaitMillis(jedisPoolProperties.maxWaitMillis);
        return poolConfig;
    }

    private GenericObjectPoolConfig<Jedis> createStatJedisPoolConfig() {
        GenericObjectPoolConfig<Jedis> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(statJedisPoolProperties.maxTotal);
        poolConfig.setMaxIdle(statJedisPoolProperties.maxIdle);
        poolConfig.setMinIdle(statJedisPoolProperties.minIdle);
        poolConfig.setTestOnBorrow(statJedisPoolProperties.testOnBorrow);
        poolConfig.setTestOnReturn(statJedisPoolProperties.testOnReturn);
        poolConfig.setMaxWaitMillis(statJedisPoolProperties.maxWaitMillis);
        return poolConfig;
    }

    private Set<HostAndPort> createJedisClusterInfo(String nodes) {
        String[] nodeArr = nodes.split(",");
        Set<HostAndPort> set = new HashSet<>();
        for (String oneUri : nodeArr) {
            HostAndPort hostAndPort = parseHostAndPort(oneUri);
            set.add(hostAndPort);
        }
        return set;
    }


}