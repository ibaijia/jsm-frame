package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: LongZL
 * @Date: 2022/6/6 14:56
 */
@ConfigurationProperties(prefix = StatJedisProperties.PREFIX)
public class StatJedisProperties {

    public final static String PREFIX = "jsm.stat.redis";

    public String nodes;

    public int dbIdx = 0;

    public int connectionTimeout = 6000;

    public int soTimeout = 6000;

    public int maxAttempts = 3;

    public String password;

    public String clientName;


    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public int getDbIdx() {
        return dbIdx;
    }

    public void setDbIdx(int dbIdx) {
        this.dbIdx = dbIdx;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public void setSoTimeout(int soTimeout) {
        this.soTimeout = soTimeout;
    }

    public int getMaxAttempts() {
        return maxAttempts;
    }

    public void setMaxAttempts(int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
