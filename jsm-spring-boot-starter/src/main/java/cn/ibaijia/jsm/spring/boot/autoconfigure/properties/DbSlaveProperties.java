package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author longzl
 */
@ConfigurationProperties(prefix = "jsm.db.slave")
public class DbSlaveProperties extends DbProperties {

}