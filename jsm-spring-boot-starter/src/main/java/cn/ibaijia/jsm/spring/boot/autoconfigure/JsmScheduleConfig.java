package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.job.JsmJob;
import cn.ibaijia.jsm.context.job.ScheduleTaskService;
import cn.ibaijia.jsm.utils.JobUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.QuartzUtil;
import org.quartz.CronExpression;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

/**
 * @author longzl
 */
@Configuration
public class JsmScheduleConfig implements SchedulingConfigurer {

    protected Logger logger = LogUtil.log(getClass());

    @Value("${" + AppContextKey.SCHEDULE_DYNA_JOB_ENABLE + ":false}")
    private Boolean dynaJobEnable;

    @Value("${" + AppContextKey.SCHEDULE_DYNA_POOL_SIZE + ":5}")
    private Integer dynaPoolSize;

    @Value("${" + AppContextKey.SCHEDULE_QUARTZ_POOL_SIZE + ":5}")
    private Integer quartzPoolSize;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.setScheduler(jsmTaskExecutor());
        configJsmJob(scheduledTaskRegistrar);
        loadTaskJob();
        JobUtil.setScheduledTaskRegistrar(scheduledTaskRegistrar);
    }

    private void configJsmJob(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        logger.debug("=======================================configJsmJob");
        Map<String,JsmJob> jsmJobMap = SpringContext.getBeansOfType(JsmJob.class);
        Iterator<String> it = jsmJobMap.keySet().iterator();
        while (it.hasNext()){
            String key = it.next();
            JsmJob jsmJob = jsmJobMap.get(key);
            logger.debug("addTriggerTask:{}",key);
            jsmJob.onLoad();
            scheduledTaskRegistrar.addTriggerTask(() -> {
                if(jsmJob.isOpen()){
                    jsmJob.doJob();
                }
            }, triggerContext -> {
                String cron = jsmJob.getCron();
                logger.debug("next cron:{}", cron);
                if (!isCronExpression(cron)) {
                    throw new RuntimeException("不合法的cron表达式:"+cron);
                }
                CronTrigger trigger = new CronTrigger(cron);
                Date nextTime = trigger.nextExecutionTime(triggerContext);
                return nextTime;
            });
        }
    }

    public static boolean isCronExpression(String cronExpression) {
        try {
            new CronExpression(cronExpression);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void loadTaskJob() {
        if (dynaJobEnable) {
            try {
                Properties properties = new Properties();
                properties.put("org.quartz.threadPool.threadNamePrefix", "jsm-task");
                properties.put("org.quartz.threadPool.threadCount", dynaPoolSize + "");
                StdSchedulerFactory schedulerFactory = new StdSchedulerFactory(properties);
                QuartzUtil.setSchedulerFactory(schedulerFactory);
            } catch (SchedulerException e) {
                logger.error("init quartz error!", e);
            }
            // 加载初始化数据
            ScheduleTaskService scheduleTaskService = SpringContext.getBean(ScheduleTaskService.class);
            if (scheduleTaskService != null) {
                logger.warn("scheduleTaskService.loadTasks... 动态job请job自行解决集群环境中的问题");
                scheduleTaskService.loadTasks();
            }
        }
    }

    @Bean(name="jsmTaskExecutor",destroyMethod = "shutdown")
    public Executor jsmTaskExecutor() {
        logger.debug("setTaskExecutors!");

        return new ScheduledThreadPoolExecutor(quartzPoolSize, new ThreadFactory() {
            byte index = 0;

            @Override
            public Thread newThread(Runnable runnable) {
                return new Thread(runnable, "jsm-job" + (++index));
            }
        });
    }

}
