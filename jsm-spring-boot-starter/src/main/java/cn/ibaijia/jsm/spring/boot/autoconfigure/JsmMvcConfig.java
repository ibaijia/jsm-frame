package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.interceptor.JsmFilter;
import cn.ibaijia.jsm.context.interceptor.JsmRestInterceptor;
import cn.ibaijia.jsm.context.interceptor.JsmTimeoutCallableProcessingInterceptor;
import cn.ibaijia.jsm.context.listener.JsmHttpSessionListener;
import cn.ibaijia.jsm.context.listener.JsmServletContextListener;
import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.DruidProperties;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author longzl
 */
@Configuration
@ComponentScan(basePackages = {
        "cn.ibaijia.jsm.auth",
        "cn.ibaijia.jsm.license",
        "cn.ibaijia.jsm.aop",
        "cn.ibaijia.jsm.context",
        "cn.ibaijia.jsm.context.order",
        "cn.ibaijia.jsm.stat",
        "cn.ibaijia.jsm.stat.strategy",
        "cn.ibaijia.jsm.gen"})
@AutoConfigureAfter(AopAutoConfiguration.class)
public class JsmMvcConfig implements WebMvcConfigurer {

    protected Logger logger = LogUtil.log(getClass());

    @Value("${jsm.web.requestTimeout:60000}")
    private Long requestTimeout;
    @Value("${jsm.web.maxUploadSize:1024000}")
    private Integer maxUploadSize;
    @Value("${jsm.web.encoding:UTF-8}")
    private String encoding;
    @Value("${jsm.web.staticDir:/static/}")
    private String staticDir;
    @Value("${jsm.web.staticPath:/static/}")
    private String staticPath;
    @Value("${spring.messages.basename:messages/message}")
    private String messagesBaseName;

    @Resource
    private Environment environment;

    @Resource
    private DruidProperties druidProperties;

    @Override
    public void configureAsyncSupport(final AsyncSupportConfigurer configure) {
        configure.setDefaultTimeout(requestTimeout);
        configure.registerCallableInterceptors(timeoutInterceptor());
    }

    @Bean
    public JsmTimeoutCallableProcessingInterceptor timeoutInterceptor() {
        return new JsmTimeoutCallableProcessingInterceptor();
    }

    @Bean
    public ServletListenerRegistrationBean<JsmServletContextListener> jsmServletContextListener() {
        logger.debug("createJsmServletContextListenerBean");
        AppContext.setEnvironment(environment);
        return new ServletListenerRegistrationBean<>(new JsmServletContextListener());
    }

    @Bean
    public ServletListenerRegistrationBean<JsmHttpSessionListener> jsmHttpSessionListener() {
        logger.debug("createJsmHttpSessionListenerBean");
        AppContext.setEnvironment(environment);
        return new ServletListenerRegistrationBean<>(new JsmHttpSessionListener());
    }

    @Bean
    public FilterRegistrationBean<JsmFilter> jsmFilter() {
        logger.debug("createJsmFilterBean");
        FilterRegistrationBean<JsmFilter> bean = new FilterRegistrationBean();
        bean.setFilter(new JsmFilter());
        bean.addUrlPatterns("*");
        bean.addInitParameter("encoding", "UTF-8");
        bean.setName("jsmFilter");
        bean.setOrder(1);
        bean.addInitParameter("exclude", staticPath);
        return bean;
    }

    @Bean
    @ConditionalOnProperty(prefix = DruidProperties.PREFIX, name = "enableMonitor", havingValue = "true", matchIfMissing = false)
    public ServletRegistrationBean<StatViewServlet> statViewServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), druidProperties.getUri());
        Map<String, String> initParams = new HashMap<>(4);
        initParams.put("loginUsername", druidProperties.getLoginUsername());
        initParams.put("loginPassword", druidProperties.getLoginPassword());
        initParams.put("allow", druidProperties.getAllow());
        initParams.put("deny", druidProperties.getDeny());
        bean.setInitParameters(initParams);
        return bean;
    }

    @Bean
    @ConditionalOnProperty(prefix = DruidProperties.PREFIX, name = "enableMonitor", havingValue = "true", matchIfMissing = false)
    public FilterRegistrationBean<WebStatFilter> webStatFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean(new WebStatFilter());
        Map<String, String> initParams = new HashMap<>(1);
        initParams.put("exclusions", "*.js,*.css,*.html," + druidProperties.getUri());
        bean.setInitParameters(initParams);
        bean.setUrlPatterns(Arrays.asList(druidProperties.getUri()));
        return bean;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        logger.debug("addResourceHandlers");
        registry.addResourceHandler(staticPath + "swagger/**")
                .addResourceLocations("classpath:/META-INF/swagger/");
        registry.addResourceHandler(staticPath + "/**")
                .addResourceLocations("classpath:" + staticDir);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        logger.debug("addInterceptors");
        registry.addInterceptor(new JsmRestInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(staticPath + "**");
        registry.addInterceptor(new LocaleChangeInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(staticPath + "**");
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        logger.debug("configureMessageConverters");
        for (HttpMessageConverter converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                ((MappingJackson2HttpMessageConverter) converter).setObjectMapper(objectMapper());
            }
        }
    }

    @Bean
    public ObjectMapper objectMapper() {
        JsonUtil.getJsmJsonConfig().setNamingStrategy(AppContext.get(AppContextKey.JSON_NAMING_STRATEGY));
        JsonUtil.getJsmJsonConfig().setShowNull(AppContext.getAsBoolean(AppContextKey.JSON_SHOW_NULL, false));
        JsonUtil.getJsmJsonConfig().setBooleanNullFalse(AppContext.getAsBoolean(AppContextKey.JSON_BOOLEAN_NULL_EMPTY, false));
        JsonUtil.getJsmJsonConfig().setStringNullEmpty(AppContext.getAsBoolean(AppContextKey.JSON_STRING_NULL_EMPTY, false));
        JsonUtil.getJsmJsonConfig().setNumberNullZero(AppContext.getAsBoolean(AppContextKey.JSON_NUMBER_NULL_EMPTY, false));
        JsonUtil.getJsmJsonConfig().setArrayNullEmpty(AppContext.getAsBoolean(AppContextKey.JSON_ARRAY_NULL_EMPTY, false));
        return JsonUtil.createObjectMapper();
    }

    @Bean(name = "messageSource")
    public ResourceBundleMessageSource resourceBundleMessageSource() {
        logger.debug("createMessageResource");
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename(messagesBaseName);
        return messageSource;
    }

    @Bean(name = {"multipartResolver"})
    public CommonsMultipartResolver commonsMultipartResolver() {
        logger.debug("createCommonsMultipartResolver,maxUploadSize:{}",maxUploadSize);
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setMaxUploadSize(maxUploadSize);
        return commonsMultipartResolver;
    }

}