package cn.ibaijia.jsm.spring.boot.autoconfigure;

import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

/**
 * @author longzhili
 */
public class JsmExcludeTypeFilter implements TypeFilter {

    private static final String SWAGGER_CONTROLLER = "Swagger2Controller";
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        String className = classMetadata.getClassName();
        if (className.contains(SWAGGER_CONTROLLER)) {
            return true;
        }
        return false;
    }
}
