package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: longZL
 * @Date: 2022/6/6 14:56
 */
@ConfigurationProperties(prefix = CacheServiceL1Properties.PREFIX)
public class CacheServiceL1Properties {

    public final static String PREFIX = "jsm.cache.l1";

    public long live = 10;
    public int init = 500;
    public long max = 2000;

    public long getLive() {
        return live;
    }

    public void setLive(long live) {
        this.live = live;
    }

    public int getInit() {
        return init;
    }

    public void setInit(int init) {
        this.init = init;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }
}
