package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

/**
 * @author longzl
 */
@ConfigurationProperties(prefix = DruidProperties.PREFIX)
public class DruidProperties {
    public static final String PREFIX = "jsm.druid";
    private Boolean enableMonitor = true;
    /**
     * filters: stat,wall,slf4j
     * #通过别名的方式配置扩展插件，常用的插件有：监控统计用的filter:stat，日志用的filter: slf4j，防御sql注入的filter:wall
     */
    private String filters = "stat,wall,slf4j";
    private Boolean logSlowSql = true;
    private Boolean mergeSql = true;
    private Integer slowSqlMillis = 1000;

    private String uri = "/static/db-monitor/*";
    private String loginUsername = "jsmDba";
    private String loginPassword = "jsmDba!qaz";
    private String allow = "";
    private String deny = "";


    public Boolean getEnableMonitor() {
        return enableMonitor;
    }

    public void setEnableMonitor(Boolean enableMonitor) {
        this.enableMonitor = enableMonitor;
    }

    public Boolean getLogSlowSql() {
        return logSlowSql;
    }

    public void setLogSlowSql(Boolean logSlowSql) {
        this.logSlowSql = logSlowSql;
    }

    public Boolean getMergeSql() {
        return mergeSql;
    }

    public void setMergeSql(Boolean mergeSql) {
        this.mergeSql = mergeSql;
    }

    public Integer getSlowSqlMillis() {
        return slowSqlMillis;
    }

    public void setSlowSqlMillis(Integer slowSqlMillis) {
        this.slowSqlMillis = slowSqlMillis;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getAllow() {
        return allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    public String getDeny() {
        return deny;
    }

    public void setDeny(String deny) {
        this.deny = deny;
    }

    public Properties getConnectionProperties() {
        Properties properties = new Properties();
        properties.put("druid.stat.logSlowSql", this.logSlowSql);
        properties.put("druid.stat.slowSqlMillis", this.slowSqlMillis);
        properties.put("druid.stat.mergeSql", this.mergeSql);
        return properties;
    }
}