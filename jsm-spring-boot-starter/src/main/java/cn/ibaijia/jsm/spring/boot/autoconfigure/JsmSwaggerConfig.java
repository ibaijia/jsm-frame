package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.spring.boot.autoconfigure.properties.SwaggerProperties;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.AllowableListValues;
import springfox.documentation.service.AllowableValues;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.schema.ApiModelProperties;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author longzl
 */
@Configuration
@EnableSwagger2
@EnableConfigurationProperties({SwaggerProperties.class})
public class JsmSwaggerConfig implements InitializingBean {

    private static Logger logger = LogUtil.log(JsmSwaggerConfig.class);

    private SwaggerProperties swaggerProperties;

    public JsmSwaggerConfig(SwaggerProperties swaggerProperties) {
        this.swaggerProperties = swaggerProperties;
    }

    @Bean(name="jsmSwaggerDocket")
    @ConditionalOnProperty(prefix = SwaggerProperties.PREFIX, name = "enable", havingValue = "true", matchIfMissing = false)
    public Docket jsmSwaggerDocket() {
        logger.debug("jsmSwaggerDocket()");
        logger.info("swaggerEnable:{} ", swaggerProperties.enable);
        logger.info("swaggerPackage:{} ", swaggerProperties.restPackage);
        logger.info("swaggerTitle:{} ", swaggerProperties.title);
        logger.info("swaggerVersion:{} ", swaggerProperties.version);
        logger.info("swaggerDescription:{} ", swaggerProperties.description);
        logger.info("swaggerHeaders:{} ", swaggerProperties.headers);
        ApiInfo apiInfo = apiInfo();
        List<Parameter> params = new ArrayList<Parameter>();
        params.add(new Parameter(WebContext.JSM_MOCK, "是否Mock，非空则返回MOCK数据", "", false, false, new ModelRef("String"), null, null, "query", null, false, null));
        params.add(new Parameter(WebContext.JSM_DEBUG_TOKEN, "明文token, 开发模式下有效", "", false, false, new ModelRef("String"), null, null, "header", null, false, null));
        if (!StringUtil.isEmpty(swaggerProperties.headers)) {
            String[] headersArr = swaggerProperties.headers.split(",");
            for (String header : headersArr) {
                String[] headerArr = header.split(":");
                params.add(new Parameter(headerArr[0], "", "", false, false, new ModelRef("String"), null, null, "header", null, false, null));
            }
        }
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .enable(swaggerProperties.enable)
                .globalOperationParameters(params)
                .select()
                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.restPackage))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerProperties.title)
                .version(swaggerProperties.version)
                .description(swaggerProperties.description)
                .build();
    }

    private AllowableValues getMockAllowableValues() {
        List<String> options = Arrays.asList("是","否");
        AllowableValues allowable = new AllowableListValues(options, "LIST");
        return allowable;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.debug("JsmSwaggerConfig afterPropertiesSet");
    }
}