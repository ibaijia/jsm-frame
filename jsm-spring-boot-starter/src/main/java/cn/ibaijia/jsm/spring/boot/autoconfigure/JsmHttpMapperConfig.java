package cn.ibaijia.jsm.spring.boot.autoconfigure;

import cn.ibaijia.jsm.http.HttpMapperBeanRegister;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Author: LongZL
 * @Date: 2022/6/20 17:12
 */
@Configuration
@Import({HttpMapperBeanRegister.class})
public class JsmHttpMapperConfig {

}
