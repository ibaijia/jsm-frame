package cn.ibaijia.jsm.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author longzl
 */
@ConfigurationProperties(prefix = MybatisConfigProperties.PREFIX)
public class MybatisConfigProperties {

    public final static String PREFIX = "jsm.mybatis";

    public String typeAliasesPackage;
    public String[] mapperLocations;
    public String pageDbDialect;

    public String getTypeAliasesPackage() {
        return typeAliasesPackage;
    }

    public void setTypeAliasesPackage(String typeAliasesPackage) {
        this.typeAliasesPackage = typeAliasesPackage;
    }

    public String[] getMapperLocations() {
        return mapperLocations;
    }

    public void setMapperLocations(String[] mapperLocations) {
        this.mapperLocations = mapperLocations;
    }

    public String getPageDbDialect() {
        return pageDbDialect;
    }

    public void setPageDbDialect(String pageDbDialect) {
        this.pageDbDialect = pageDbDialect;
    }
}