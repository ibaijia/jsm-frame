package cn.ibaijia.jsm.log4j2;

import java.io.Serializable;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 17:08
 */
public interface LogStrategy {

    /**
     * 写出text
     * @param obj
     */
    public void write(Serializable obj);

}
