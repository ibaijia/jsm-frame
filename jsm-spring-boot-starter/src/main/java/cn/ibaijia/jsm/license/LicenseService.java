package cn.ibaijia.jsm.license;

/**
 * @author longzl
 */
public interface LicenseService {
    /**
     * 启动时调用
     * @return
     */
    boolean init();
    /**
     * 检查时调用
     * @return
     */
    boolean check();
    /**
     * 销毁时调用
     * @return
     */
    boolean destroy();

}
