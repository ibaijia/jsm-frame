package cn.ibaijia.jsm.license;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author longzl
 */
public class LicenseVo implements ValidateModel {

    @FieldAnn(required = false, maxLen = 30, comments = "客户端ID")
    public String clientId;

    @FieldAnn(required = false, comments = "开始时间")
    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    public Date beginTime;

    @FieldAnn(required = false, comments = "结束时间")
    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    public Date endTime;

    @FieldAnn(required = false, maxLen = 250, comments = "授权数据")
    public String authData;


    public boolean isValid() {
        if (DateUtil.currentDate().after(beginTime) && DateUtil.currentDate().before(endTime)) {
            return true;
        }
        return false;
    }

}