package cn.ibaijia.jsm.elastic.analyze;

import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author: LongZL
 * @Date: 2022/11/9 16:16
 */
public class Token implements ValidateModel {


    public Token() {

    }

    public Token(String token) {
        this.token = token;
    }

    public Token(String token, String type, int position, int startOffset, int endOffset) {
        this.token = token;
        this.type = type;
        this.position = position;
        this.startOffset = startOffset;
        this.endOffset = endOffset;
    }

    private String token;
    private String type;
    private int position;
    @JsonProperty("start_offset")
    private int startOffset;
    @JsonProperty("end_offset")
    private int endOffset;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getStartOffset() {
        return startOffset;
    }

    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return endOffset;
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }
}
