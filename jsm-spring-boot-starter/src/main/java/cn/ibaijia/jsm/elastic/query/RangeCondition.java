package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:08
 */
public class RangeCondition implements Condition {
    private static final String TPL = "{\"range\":{\"%s\":{\"%s\":\"%s\",\"%s\":\"%s\"}}}";
    private static final String TPL_ONE = "{\"range\":{\"%s\":{\"%s\":\"%s\"}}}";

    private String filed;
    private RangeItem item1;
    private RangeItem item2;

    public RangeCondition(String filed, RangeItem item) {
        this.filed = filed;
        this.item1 = item;
    }

    public RangeCondition(String filed, RangeItem item1, RangeItem item2) {
        this.filed = filed;
        this.item1 = item1;
        this.item2 = item2;
    }

    @Override
    public String toString() {
        if (item1 != null && item2 == null) {
            return String.format(TPL_ONE, filed, item1.getOperator(), item1.getValue());
        }
        if (item1 == null && item2 != null) {
            return String.format(TPL_ONE, filed, item2.getOperator(), item2.getValue());
        }
        return String.format(TPL, filed, item1.getOperator(), item1.getValue(), item2.getOperator(), item2.getValue());
    }
}
