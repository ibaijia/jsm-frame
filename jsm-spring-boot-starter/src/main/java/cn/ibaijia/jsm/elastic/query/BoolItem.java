package cn.ibaijia.jsm.elastic.query;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:40
 */
public class BoolItem {
    protected static final String TPL = "\"%s\":%s";
    protected static final String TPL_ARR = "\"%s\":[%s]";

    protected List<Condition> list = new ArrayList<>();

    protected ConditionOperator operator;


    public BoolItem(ConditionOperator operator) {
        this.operator = operator;
    }

    public BoolItem(ConditionOperator operator, Condition condition) {
        this.operator = operator;
        this.list.add(condition);
    }

    public BoolItem add(Condition condition) {
        this.list.add(condition);
        return this;
    }

    public BoolItem addIfTrue(boolean ifTrue, Condition condition) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(condition);
        return this;
    }

    public BoolItem term(String filed, Object value) {
        this.list.add(new TermCondition(filed, value));
        return this;
    }

    public BoolItem termIfTrue(boolean ifTrue, String filed, Object value) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(new TermCondition(filed, value));
        return this;
    }

    public BoolItem match(String filed, Object value) {
        this.list.add(new MatchCondition(filed, value));
        return this;
    }

    public BoolItem matchIfTrue(boolean ifTrue, String filed, Object value) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(new MatchCondition(filed, value));
        return this;
    }

    public BoolItem matchPhrase(String filed, Object value) {
        this.list.add(new MatchPhraseCondition(filed, value));
        return this;
    }

    public BoolItem matchPhraseIfTrue(boolean ifTrue, String filed, Object value) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(new MatchPhraseCondition(filed, value));
        return this;
    }

    public BoolItem multiMatch(String query, String... fields) {
        this.list.add(new MultiMatchCondition(query, fields));
        return this;
    }

    public BoolItem multiMatchIfTrue(boolean ifTrue, String query, String... fields) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(new MultiMatchCondition(query, fields));
        return this;
    }

    public BoolItem range(String filed, RangeOperator operator, Object value) {
        this.list.add(new RangeCondition(filed, new RangeItem(operator, value)));
        return this;
    }

    public BoolItem rangeIfTrue(boolean ifTrue, String filed, RangeOperator operator, Object value) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(new RangeCondition(filed, new RangeItem(operator, value)));
        return this;
    }

    public BoolItem bool(Bool bool) {
        this.list.add(bool);
        return this;
    }

    public BoolItem boolIfTrue(boolean ifTrue, Bool bool) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(bool);
        return this;
    }

    @Override
    public String toString() {
        if (this.list.size() == 1) {
            return String.format(TPL, this.operator, list.get(0).toString());
        }
        StringBuilder sb = new StringBuilder();
        for (Condition condition : list) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(condition.toString());
        }
        return String.format(TPL_ARR, this.operator, sb.toString());
    }
}
