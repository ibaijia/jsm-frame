package cn.ibaijia.jsm.elastic.highlight;

import cn.ibaijia.jsm.elastic.util.JsonUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2022/10/26 16:50
 */
public class Highlight {

    private Map<String, Object> map = new HashMap<>();
    private Map<String, HighlightField> fields = new HashMap<>();

    public Highlight(String... fields) {
        map.put("require_field_match", true);
        if (fields != null && fields.length > 0) {
            for (String field : fields) {
                this.fields.put(field, new HighlightField());
            }
        }
    }

    public Highlight setPreTags(String... tags) {
        map.put("pre_tags", tags);
        return this;
    }

    public Highlight setPostTags(String... tags) {
        map.put("post_tags", tags);
        return this;
    }

    public Highlight attr(String name, Object value) {
        map.put(name, value);
        return this;
    }

    public Highlight attrIfTrue(boolean ifTrue, String name, Object value) {
        if (ifTrue) {
            map.put(name, value);
        }
        return this;
    }

    public Highlight field(String field) {
        this.fields.put(field, new HighlightField());
        return this;
    }

    @Override
    public String toString() {
        this.map.put("fields", fields);
        return JsonUtil.toJsonString(map);
    }
}
