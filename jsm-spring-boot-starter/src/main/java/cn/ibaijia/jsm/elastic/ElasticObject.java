package cn.ibaijia.jsm.elastic;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2022/7/8 17:01
 */
public class ElasticObject<T> implements ValidateModel {

    @FieldAnn(comments = "索引名")
    @JsonProperty(value = "_index")
    @JSONField(name = "_index")
    public String index;

    @FieldAnn(comments = "类型")
    @JsonProperty(value = "_type")
    @JSONField(name = "_type")
    public String type;

    @FieldAnn(comments = "ID")
    @JsonProperty(value = "_id")
    @JSONField(name = "_id")
    public String id;
    @FieldAnn(comments = "score")
    @JsonProperty(value = "_score")
    @JSONField(name = "_score")
    public String score;

    @FieldAnn(comments = "对象内容")
    @JsonProperty(value = "_source")
    @JSONField(name = "_source")
    public T source;

    @FieldAnn(comments = "高亮")
    public Map<String, List<String>> highlight;

}
