package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:31
 */
public enum RangeOperator {
    /**
     *
     */
    GT("gt"),
    /**
     *
     */
    GTE("gte"),
    /**
     *
     */
    LT("lt"),
    /**
     *
     */
    LTE("lte"),
    /**
     *
     */
    EQ("eq"),
    /**
     *
     */
    NEQ("neq");
    private String value;

    RangeOperator(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
