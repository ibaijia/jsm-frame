package cn.ibaijia.jsm.elastic.analyze;

import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.jsm.utils.JsonUtil;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;

import java.util.List;

/**
 * @author longzl
 */
public class ElasticAnalyze {

    private Logger logger = StatusLogger.getLogger();

    private String address;

    private HttpClient httpClient = new HttpClient();

    public ElasticAnalyze(String address) {
        httpClient.setWithExtra(false);
        this.address = address + "/_analyze";
    }


    public String analyzeAsString(String text, String analyzer) {
        AnalyzeReq req = new AnalyzeReq(text, analyzer);
        String res = httpClient.post(address, JsonUtil.toJsonString(req));
        return res;
    }

    public String analyzeAsString(String text) {
        AnalyzeReq req = new AnalyzeReq(text);
        String res = httpClient.post(address, JsonUtil.toJsonString(req));
        return res;
    }

    public List<Token> analyze(String text) {
        AnalyzeReq req = new AnalyzeReq(text);
        String res = httpClient.post(address, JsonUtil.toJsonString(req));
        AnalyzeResp resp = JsonUtil.parseObject(res, AnalyzeResp.class);
        return resp != null ? resp.getTokens() : null;
    }

    public List<Token> analyze(String text, String analyzer) {
        AnalyzeReq req = new AnalyzeReq(text, analyzer);
        String res = httpClient.post(address, JsonUtil.toJsonString(req));
        AnalyzeResp resp = JsonUtil.parseObject(res, AnalyzeResp.class);
        return resp != null ? resp.getTokens() : null;
    }


}
