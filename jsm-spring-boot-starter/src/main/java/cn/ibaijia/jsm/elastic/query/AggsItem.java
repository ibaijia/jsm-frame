package cn.ibaijia.jsm.elastic.query;

import cn.ibaijia.jsm.elastic.util.JsonUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2022/10/12 11:01
 */
public class AggsItem {

    private Map<String, Object> fields = new HashMap<>();

    public AggsItem(String field) {
        this.fields.put("field", field);
    }

    public AggsItem setField(String field) {
        this.fields.put("field", field);
        return this;
    }

    public AggsItem setMissing(String missing) {
        this.fields.put("missing", missing);
        return this;
    }

    public AggsItem attr(String name, Object value) {
        fields.put(name, value);
        return this;
    }

    public AggsItem attrIfTrue(boolean ifTrue, String name, Object value) {
        if (ifTrue) {
            fields.put(name, value);
        }
        return this;
    }

    @Override
    public String toString() {
        return JsonUtil.toJsonString(fields);
    }

}
