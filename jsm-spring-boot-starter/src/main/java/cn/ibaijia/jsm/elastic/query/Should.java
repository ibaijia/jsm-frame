package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:40
 */
public class Should extends BoolItem {

    public Should() {
        super(ConditionOperator.SHOULD);
    }

    public Should(Condition condition) {
        super(ConditionOperator.SHOULD, condition);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
