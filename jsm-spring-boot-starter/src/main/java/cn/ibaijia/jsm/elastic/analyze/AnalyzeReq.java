package cn.ibaijia.jsm.elastic.analyze;

import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * analyzer 如下：
 * standard - ES index默认分词器，按词切分，小写处理，
 * simple - 按照非字母切分(符号被过滤), 小写处理,会去除数字
 * whitespace  - 按照空格切分，不转小写
 * stop - 小写处理，停用词过滤(the,a,is)
 * keyword - 不分词，直接将输入当作输出
 * ik_smart:会做最粗粒度的拆分
 * ik_max_word:会将文本做最细粒度的拆分
 *
 * @Author: LongZL
 * @Date: 2022/11/9 16:05
 */
public class AnalyzeReq implements ValidateModel {


    private String text;

    private String analyzer = "standard";


    public AnalyzeReq(String text) {
        this.text = text;
    }

    public AnalyzeReq(String text, String analyzer) {
        this.text = text;
        this.analyzer = analyzer;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnalyzer() {
        return analyzer;
    }

    public void setAnalyzer(String analyzer) {
        this.analyzer = analyzer;
    }
}
