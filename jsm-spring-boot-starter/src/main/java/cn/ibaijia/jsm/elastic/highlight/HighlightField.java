package cn.ibaijia.jsm.elastic.highlight;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author: LongZL
 * @Date: 2022/10/26 16:58
 */
public class HighlightField {

    @JSONField(name = "pre_tags")
    @JsonProperty("pre_tags")
    public String preTags;

    @JSONField(name = "post_tags")
    @JsonProperty("post_tags")
    public String postTags;

}
