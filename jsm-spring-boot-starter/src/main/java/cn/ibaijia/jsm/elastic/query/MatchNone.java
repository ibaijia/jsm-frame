package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 15:44
 */
public class MatchNone implements Query {
    private static final String TPL = "{\"match_none\":{}}";

    public MatchNone() {
    }

    @Override
    public String toString() {
        return TPL;
    }
}
