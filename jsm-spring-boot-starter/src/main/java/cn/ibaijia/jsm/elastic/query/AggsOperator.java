package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:31
 */
public enum AggsOperator {
    /**
     *
     */
    TERMS("terms"),
    /**
     *
     */
    MAX("max"),
    /**
     *
     */
    MIN("min"),
    /**
     *
     */
    AVG("avg"),
    /**
     *
     */
    SUM("sum"),
    /**
     *
     */
    MISSING("missing"),
    /**
     *
     */
    DATE_HISTOGRAM("date_histogram"),
    /**
     *
     */
    HISTOGRAM("histogram"),
    /**
     *
     */
    DATE_RANGE("date_range"),
    /**
     *
     */
    RANGE("range");
    private String value;

    AggsOperator(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
