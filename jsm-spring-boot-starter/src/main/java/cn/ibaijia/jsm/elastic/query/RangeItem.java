package cn.ibaijia.jsm.elastic.query;

import cn.ibaijia.jsm.elastic.util.JsonUtil;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:12
 */
public class RangeItem {

    private RangeOperator operator;
    private String value;

    public RangeItem(RangeOperator operator, String value) {
        this.operator = operator;
        this.value = value;
    }

    public RangeItem(RangeOperator operator, Object value) {
        this.operator = operator;
        if (value instanceof String) {
            this.value = (String) value;
        } else {
            this.value = JsonUtil.toJsonString(value);
        }
    }

    public RangeOperator getOperator() {
        return operator;
    }

    public String getValue() {
        return value;
    }
}
