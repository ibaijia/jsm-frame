package cn.ibaijia.jsm.elastic.query;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 15:44
 */
public class Bool implements Condition, Query {
    private static final String TPL = "{\"bool\":{%s}}";

    private List<BoolItem> list = new ArrayList<>();

    public Bool() {
    }

    public Bool(BoolItem item) {
        this.list.add(item);
    }

    public Bool add(BoolItem item) {
        this.list.add(item);
        return this;
    }

    public Must must() {
        Must must = new Must();
        this.list.add(must);
        return must;
    }

    public Should should() {
        Should should = new Should();
        this.list.add(should);
        return should;
    }

    public MustNot mustNot() {
        MustNot mustNot = new MustNot();
        this.list.add(mustNot);
        return mustNot;
    }

    public Filter filter() {
        Filter filter = new Filter();
        this.list.add(filter);
        return filter;
    }

    public Bool addIfTrue(boolean ifTrue, BoolItem item) {
        if (!ifTrue) {
            return this;
        }
        this.list.add(item);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (BoolItem item : list) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(item.toString());
        }
        return String.format(TPL, sb.toString());
    }
}
