package cn.ibaijia.jsm.elastic.analyze;

import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/11/9 16:14
 */
public class AnalyzeResp implements ValidateModel {

    private List<Token> tokens;

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
