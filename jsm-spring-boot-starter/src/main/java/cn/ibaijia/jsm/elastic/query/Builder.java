package cn.ibaijia.jsm.elastic.query;

import cn.ibaijia.jsm.elastic.highlight.Highlight;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/7/8 19:49
 */
public class Builder {

    private Query query;
    private Aggs aggs;
    private Highlight highlight;

    private Integer from = 0;

    private Integer size = 5000;

    private List<SortCondition> sort = new ArrayList<>();

    public Builder addSort(SortCondition sort) {
        this.sort.add(sort);
        return this;
    }

    public Bool bool(Bool bool) {
        this.query = bool;
        return bool;
    }

    public Bool bool() {
        Bool bool = new Bool();
        this.query = bool;
        return bool;
    }

    public Builder matchAll() {
        MatchAll matchAll = new MatchAll();
        this.query = matchAll;
        return this;
    }

    public Builder matchNone() {
        MatchNone matchNone = new MatchNone();
        this.query = matchNone;
        return this;
    }

    public Builder matchAll(String boost) {
        MatchAll matchAll = new MatchAll(boost);
        this.query = matchAll;
        return this;
    }

    public Builder size(Integer size) {
        this.size = size;
        return this;
    }

    public Builder from(Integer from) {
        this.from = from;
        return this;
    }

    public Aggs aggs(String name) {
        this.aggs = new Aggs(name);
        return this.aggs;
    }

    public Builder highlight(String field) {
        if (this.highlight == null) {
            this.highlight = new Highlight();
        }
        this.highlight.field(field);
        return this;
    }

    public Builder highlight(Highlight highlight) {
        this.highlight = highlight;
        return this;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"from\":").append(from);
        sb.append(",\"size\":").append(size);
        if (query != null) {
            sb.append(",\"query\":").append(query.toString());
        }
        if (aggs != null) {
            sb.append(",\"aggs\":").append(aggs.toString());
        }
        if (highlight != null) {
            sb.append(",\"highlight\":").append(highlight.toString());
        }
        if (!sort.isEmpty()) {
            StringBuilder shortSb = new StringBuilder();
            for (SortCondition condition : sort) {
                if (shortSb.length() > 0) {
                    shortSb.append(",");
                }
                shortSb.append(condition.toString());
            }
            sb.append(",\"sort\":[").append(shortSb).append("]");
        }
        sb.append("}");
        return sb.toString();
    }

}
