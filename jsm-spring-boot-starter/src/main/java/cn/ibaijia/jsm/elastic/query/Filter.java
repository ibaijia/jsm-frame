package cn.ibaijia.jsm.elastic.query;

/**
 * 同样是按条件匹配，filter不统计相关度，must统计相关度
 * must比filter计算更复杂，更耗时
 * 同时使用range做查询，must查询后，结果中的document的相关度默认均为1
 *
 * @Author: LongZL
 * @Date: 2022/7/12 11:40
 */
public class Filter extends BoolItem {

    public Filter() {
        super(ConditionOperator.FILTER);
    }

    public Filter(Condition condition) {
        super(ConditionOperator.FILTER, condition);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
