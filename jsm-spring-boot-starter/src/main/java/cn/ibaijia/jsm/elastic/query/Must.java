package cn.ibaijia.jsm.elastic.query;

/**
 * match:请求意味着它们被用来评定每个文档的匹配度的评分；
 * filter:它们将过滤出不匹配的文档，但不会影响匹配文档的分数;
 * filter会比query快
 *
 * @Author: LongZL
 * @Date: 2022/7/12 11:40
 */
public class Must extends BoolItem {

    public Must() {
        super(ConditionOperator.MUST);
    }

    public Must(Condition condition) {
        super(ConditionOperator.MUST, condition);
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
