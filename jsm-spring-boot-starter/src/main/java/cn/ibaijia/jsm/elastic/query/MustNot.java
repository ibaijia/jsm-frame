package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:40
 */
public class MustNot extends BoolItem {

    public MustNot() {
        super(ConditionOperator.MUST_NOT);
    }

    public MustNot(Condition condition) {
        super(ConditionOperator.MUST_NOT, condition);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
