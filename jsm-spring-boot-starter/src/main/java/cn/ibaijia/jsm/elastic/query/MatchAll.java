package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 15:44
 */
public class MatchAll implements Query {
    private static final String TPL = "{\"match_all\":{}}";
    private static final String TPL_1 = "{\"match_all\":{\"boost\":\"%s\"}}";

    private String boost;

    public MatchAll() {
    }

    public MatchAll(String boost) {
        this.boost = boost;
    }

    @Override
    public String toString() {
        return boost == null? TPL :String.format(TPL_1, boost);
    }
}
