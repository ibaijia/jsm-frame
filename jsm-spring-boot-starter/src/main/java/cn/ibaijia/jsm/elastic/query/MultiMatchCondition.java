package cn.ibaijia.jsm.elastic.query;


import cn.ibaijia.jsm.elastic.util.JsonUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * match：代表模糊匹配，搜索前会对搜索词进行分词解析，然后按搜索词匹配查找；
 *
 * @Author: LongZL
 * @Date: 2022/7/12 11:08
 */
public class MultiMatchCondition implements Condition {
    private static final String TPL = "{\"multi_match\":%s}";

    private Map<String, Object> map = new HashMap<>();

    public MultiMatchCondition(String query, String... fileds) {
        map.put("query", query);
        map.put("fields", fileds);
    }

    public MultiMatchCondition(Object query, String... fileds) {
        map.put("fields", fileds);
        if (query instanceof String) {
            map.put("query", query);
        } else {
            map.put("query", JsonUtil.toJsonString(query));
        }
    }

    public MultiMatchCondition attr(String name, Object value) {
        map.put(name, value);
        return this;
    }

    public MultiMatchCondition attrIfTrue(boolean ifTrue, String name, Object value) {
        if (ifTrue) {
            map.put(name, value);
        }
        return this;
    }

    @Override
    public String toString() {
        return String.format(TPL, JsonUtil.toJsonString(map));
    }
}
