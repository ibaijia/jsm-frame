package cn.ibaijia.jsm.elastic.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.TimeZone;

/**
 * @Author: LongZL
 * @Date: 2022/10/13 15:08
 */
public class JsonUtil {

    private static ObjectMapper objectMapper;

    private static ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.setTimeZone(TimeZone.getDefault());
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        return objectMapper;
    }

    public static String toJsonString(Object data) {
        try {
            return getObjectMapper().writeValueAsString(data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
