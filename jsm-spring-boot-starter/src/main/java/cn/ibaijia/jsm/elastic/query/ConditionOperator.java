package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 11:31
 */
public enum ConditionOperator {
    /**
     *
     */
    MUST("must"),
    /**
     *
     */
    MUST_NOT("must_not"),
    /**
     *
     */
    SHOULD("should"),
    /**
     *
     */
    FILTER("filter");
    private String value;

    ConditionOperator(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
