package cn.ibaijia.jsm.elastic;

import cn.ibaijia.jsm.context.dao.model.Page;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.ReflectionUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 17:11
 */
public class ElasticQueryService {

    protected Logger logger = LogUtil.log(this.getClass());

    private static final String BUCKETS = "buckets";

    protected ElasticClient elasticClient;

    public ElasticQueryService(ElasticClient elasticClient) {
        this.elasticClient = elasticClient;
    }

    public ElasticQueryService(String address, String idx) {
        elasticClient = new ElasticClient(String.format("%s/%s", address, idx));
    }

    public List<ElasticObject> listObject(String queryJson) {
        TypeReference<List<ElasticObject>> typeReference = new TypeReference<List<ElasticObject>>() {
        };
        return elasticClient.search(queryJson, typeReference);
    }

    public <T> List<ElasticObject<T>> pageListObject(String queryJson, Page<ElasticObject<T>> page) {
        TypeReference<List<ElasticObject>> typeReference = new TypeReference<List<ElasticObject>>() {
        };
        String res = elasticClient.searchAsString(queryJson);
        JsonNode hits1 = JsonUtil.readTree(res).get("hits");
        if (hits1 == null) {
            return null;
        }
        JsonNode total = hits1.get("total");
        page.totalCount = total.get("value").intValue();
        JsonNode hits2 = hits1.get("hits");
        if (hits2 == null) {
            return null;
        }
        String listStr = hits2.toString();
        List<ElasticObject<T>> esList = JsonUtil.parseObject(listStr, new TypeReference<List<ElasticObject<T>>>() {
        });
        page.list = esList;
        return esList;
    }

    public <T> List<T> list(String queryJson) {
        List<T> list = new ArrayList<>();
        String res = elasticClient.searchHits(queryJson);
        List<ElasticObject<T>> esList = JsonUtil.parseObject(res, new TypeReference<List<ElasticObject<T>>>() {
        });
        for (ElasticObject<T> elasticObject : esList) {
            list.add(elasticObject.source);
        }
        return list;
    }

    public <T> List<T> list(String queryJson, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        String res = elasticClient.searchHits(queryJson);
        List<ElasticObject> esList = JsonUtil.parseObject(res, new TypeReference<List<ElasticObject>>() {
        });
        for (ElasticObject elasticObject : esList) {
            list.add(JsonUtil.parseObject(JsonUtil.toJsonString(elasticObject.source), clazz));
        }
        return list;
    }

    public <T> List<T> pageList(String queryJson, Page<T> page) {
        List<T> list = new ArrayList<>();
        String res = elasticClient.searchAsString(queryJson);
        JsonNode hits1 = JsonUtil.readTree(res).get("hits");
        if (hits1 == null) {
            return null;
        }
        JsonNode total = hits1.get("total");
        page.totalCount = total.get("value").intValue();

        JsonNode hits2 = hits1.get("hits");
        if (hits2 == null) {
            return null;
        }
        String listStr = hits2.toString();
        List<ElasticObject<T>> esList = JsonUtil.parseObject(listStr, new TypeReference<List<ElasticObject<T>>>() {
        });
        for (ElasticObject<T> elasticObject : esList) {
            list.add(elasticObject.source);
        }
        page.list = list;
        return list;
    }

    public <T> List<T> aggs(String queryJson, Class<T> clazz) {
        try {
            String res = elasticClient.searchAggs(queryJson);
            JsonNode node = JsonUtil.readTree(res);
            List<T> list = new ArrayList<>();
            Map.Entry<String, JsonNode> entry = node.fields().next();
            readField(list, entry, clazz, null);
            return list;
        } catch (Exception e) {
            logger.error("aggs error!" + e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }


    private <T> void readField(List<T> list, Map.Entry<String, JsonNode> entry, Class<T> clazz, T parent) throws Exception {
        String fieldName = entry.getKey();
        JsonNode fieldNode = entry.getValue();
        if (fieldNode.has(BUCKETS)) {
            ArrayNode buckets = (ArrayNode) fieldNode.get(BUCKETS);
            for (JsonNode jsonNode : buckets) {
                T t = clazz.getDeclaredConstructor().newInstance();
                if (parent != null) {
                    BeanUtil.copy(parent, t);
                }
                ReflectionUtil.setFieldValue(t, fieldName, jsonNode.get("key").asText());
                ReflectionUtil.setFieldValue(t, "count", jsonNode.get("doc_count").intValue());
                Map.Entry<String, JsonNode> nextEntry = findNextEntry(jsonNode);
                if (nextEntry == null) {
                    list.add(t);
                    continue;
                } else {
                    String nextKey = nextEntry.getKey();
                    JsonNode nextJsonNode = nextEntry.getValue();
                    // avg min max
                    if (!nextJsonNode.has("buckets")) {
                        String value = nextJsonNode.get("value").asText();
                        ReflectionUtil.setFieldValue(t, nextKey, value);
                        list.add(t);
                        continue;
                    } else {
                        readField(list, nextEntry, clazz, t);
                    }
                }
            }
        }
    }

    private Map.Entry<String, JsonNode> findNextEntry(JsonNode jsonNode) {
        Iterator<Map.Entry<String, JsonNode>> it = jsonNode.fields();
        while (it.hasNext()) {
            Map.Entry<String, JsonNode> entry = it.next();
            it.remove();
            JsonNode nextJsonNode = entry.getValue();
            if (nextJsonNode.isObject()) {
                return entry;
            }
        }
        return null;
    }

    public String searchAsString(String queryJson) {
        return elasticClient.searchAsString(queryJson);
    }

    public String searchHits(String queryJson) {
        return elasticClient.searchHits(queryJson);
    }

    public String searchAggs(String queryJson) {
        return elasticClient.searchAggs(queryJson);
    }

}
