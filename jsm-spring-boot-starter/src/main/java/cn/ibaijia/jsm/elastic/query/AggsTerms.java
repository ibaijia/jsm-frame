package cn.ibaijia.jsm.elastic.query;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2022/10/12 11:02
 */
public class AggsTerms extends AggsItem {

    public AggsTerms(String field) {
        super(field);
    }

    public AggsTerms(String field, Integer size) {
        super(field);
        this.size = size;
    }

    private Map<String, String> order;

    private Integer size;

    public Integer getSize() {
        return size;
    }

    public Map<String, String> getOrder() {
        return order;
    }

    public AggsTerms size(Integer size) {
        this.size = size;
        return this;
    }

    public AggsTerms order(String filed, boolean asc) {
        this.order = new HashMap<>(1);
        this.order.put(filed, asc ? "asc" : "desc");
        return this;
    }

    public AggsTerms countOrder(boolean asc) {
        this.order = new HashMap<>(1);
        this.order.put("_count", asc ? "asc" : "desc");
        return this;
    }

    public AggsTerms keyOrder(boolean asc) {
        this.order = new HashMap<>(1);
        this.order.put("_key", asc ? "asc" : "desc");
        return this;
    }

    public AggsTerms termOrder(boolean asc) {
        this.order = new HashMap<>(1);
        this.order.put("_term", asc ? "asc" : "desc");
        return this;
    }

}
