package cn.ibaijia.jsm.elastic.query;

/**
 * @Author: LongZL
 * @Date: 2022/10/12 11:02
 */
public class AggsMin extends AggsItem {
    public AggsMin(String field) {
        super(field);
    }
}
