package cn.ibaijia.jsm.elastic.query;

import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @Author: LongZL
 * @Date: 2022/7/8 19:54
 */
public class SortCondition {

    private static final String TPL = "{\"%s\":{\"order\":\"%s\"}}";

    public SortCondition(String field, boolean asc) {
        this.field = field;
        this.asc = asc;
    }

    @FieldAnn(comments = "字段")
    private String field;

    @FieldAnn(comments = "是否升序")
    private boolean asc;

    @Override
    public String toString() {
        return String.format(TPL, this.field, this.asc ? "asc" : "desc");
    }
}
