package cn.ibaijia.jsm.elastic.query;


import cn.ibaijia.jsm.elastic.util.JsonUtil;

/**
 * match_phrase：代表模糊匹配，搜索前会对搜索词进行分词解析，匹配倒排索引，查找这个短语
 *
 * @Author: LongZL
 * @Date: 2022/7/12 11:08
 */
public class MatchPhraseCondition implements Condition {
    private static final String TPL = "{\"match_phrase\":{\"%s\":\"%s\"}}";

    private String filed;
    private String value;

    public MatchPhraseCondition(String filed, String value) {
        this.filed = filed;
        this.value = value;
    }

    public MatchPhraseCondition(String filed, Object value) {
        this.filed = filed;
        if (value instanceof String) {
            this.value = (String) value;
        } else {
            this.value = JsonUtil.toJsonString(value);
        }
    }

    @Override
    public String toString() {
        return String.format(TPL, filed, value);
    }
}
