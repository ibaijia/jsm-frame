package cn.ibaijia.jsm.elastic;

import cn.ibaijia.jsm.disruptor.JsmDisruptor;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.lmax.disruptor.EventHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 17:11
 */
public class ElasticService<V> extends ElasticQueryService {


    private AtomicLong idAi = new AtomicLong(0);
    private List<BatchObject> cacheMessage = new ArrayList<BatchObject>();
    private static final int BATCH_SIZE = 500;
    private static final int REMAIN_SIZE = 100;

    public ElasticService(ElasticClient elasticClient) {
        super(elasticClient);
    }

    public ElasticService(String address, String idx, String mappings) {
        super(address, idx);
        elasticClient.setIndexOptions(mappings);
        elasticClient.createIndexIfNotExists();
    }

    private JsmDisruptor<String> stringDisruptor = new JsmDisruptor<String>(new EventHandler<JsmDisruptor<String>.MessageEvent<String>>() {
        @Override
        public void onEvent(JsmDisruptor<String>.MessageEvent<String> stringMessageEvent, long sequence, boolean endOfBatch) {
            try {
                cacheMessage.add(new BatchObject(getId(), stringMessageEvent.message));
                if (endOfBatch || (sequence + 1) % BATCH_SIZE == 0) {
                    elasticClient.postBatch(cacheMessage);
                    cacheMessage.clear();
                }
            } catch (Exception e) {
                logger.error("onEvent process error.", e);
            }
        }
    }, 4 * 1024);

    public void write(String text) {
        stringDisruptor.pushMessage(text);
    }

    public void write(V object) {
        String text = JsonUtil.toJsonString(object);
        stringDisruptor.pushMessage(text);
    }

    private String getId() {
        try {
            long intCount = 0;
            intCount = idAi.getAndIncrement();
            if (intCount > Long.MAX_VALUE - REMAIN_SIZE) {
                idAi.set(0);
            }
            return String.format("%s-%s", DateUtil.format(DateUtil.currentDate(), DateUtil.COMPACT_DATE_PATTERN), intCount);
        } catch (Exception e) {
            return StringUtil.uuid();
        }
    }

    public String recreateIndex() {
        if (this.elasticClient != null) {
            return this.elasticClient.recreateIndex();
        }
        return null;
    }

}
