package cn.ibaijia.jsm.elastic.query;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: LongZL
 * @Date: 2022/7/12 15:44
 */
public class Aggs {

    private static final String TPL = "{\"%s\":{\"%s\":%s}}";
    private static final String TPL_AGGS = "{\"%s\":{\"%s\":%s,\"aggs\":%s}}";

    private Map<String, Object> fields = new HashMap<>();

    private String name;

    private AggsItem aggsItem;

    private AggsOperator operator;

    private Aggs aggs;


    public Aggs(String name) {
        this.name = name;
        this.terms();
    }

    public Aggs terms() {
        AggsTerms item = new AggsTerms(this.name);
        return terms(item);
    }

    public Aggs terms(AggsTerms item) {
        this.aggsItem = item;
        this.operator = AggsOperator.TERMS;
        return this;
    }

    public Aggs terms(Integer size) {
        AggsTerms item = new AggsTerms(this.name, size);
        return terms(item);
    }

    public Aggs max() {
        AggsMax item = new AggsMax(this.name);
        return this.maxIfTrue(true, item);
    }

    public Aggs max(AggsMax item) {
        return this.maxIfTrue(true, item);
    }

    public Aggs maxIfTrue(boolean ifTrue) {
        if (!ifTrue) {
            return this;
        }
        AggsMax item = new AggsMax(this.name);
        return this.maxIfTrue(true, item);
    }

    public Aggs maxIfTrue(boolean ifTrue, AggsMax item) {
        if (!ifTrue) {
            return this;
        }
        this.aggsItem = item;
        this.operator = AggsOperator.MAX;
        return this;
    }


    public Aggs min() {
        return this.minIfTrue(true);
    }

    public Aggs min(AggsMin item) {
        return this.minIfTrue(true, item);
    }

    public Aggs minIfTrue(boolean ifTrue) {
        if (!ifTrue) {
            return this;
        }
        AggsMin item = new AggsMin(this.name);
        return this.minIfTrue(true, item);
    }

    public Aggs minIfTrue(boolean ifTrue, AggsMin item) {
        if (!ifTrue) {
            return this;
        }
        this.aggsItem = item;
        this.operator = AggsOperator.MIN;
        return this;
    }


    public Aggs avg() {
        return this.avgIfTrue(true);
    }

    public Aggs avg(AggsAvg item) {
        return this.avgIfTrue(true, item);
    }

    public Aggs avgIfTrue(boolean ifTrue) {
        if (!ifTrue) {
            return this;
        }
        AggsAvg item = new AggsAvg(this.name);
        return this.avgIfTrue(true, item);
    }

    public Aggs avgIfTrue(boolean ifTrue, AggsAvg item) {
        if (!ifTrue) {
            return this;
        }
        this.aggsItem = item;
        this.operator = AggsOperator.AVG;
        return this;
    }

    public Aggs sum() {
        return this.sumIfTrue(true);
    }

    public Aggs sum(AggsSum item) {
        return this.sumIfTrue(true, item);
    }

    public Aggs sumIfTrue(boolean ifTrue) {
        if (!ifTrue) {
            return this;
        }
        AggsSum item = new AggsSum(this.name);
        return this.sumIfTrue(true, item);
    }

    public Aggs sumIfTrue(boolean ifTrue, AggsSum item) {
        if (!ifTrue) {
            return this;
        }
        this.aggsItem = item;
        this.operator = AggsOperator.SUM;
        return this;
    }

    public Aggs setOperatorAndItem(AggsOperator operator, AggsItem item) {
        this.operator = operator;
        this.aggsItem = item;
        return this;
    }

    public Aggs aggs(String name) {
        this.aggs = new Aggs(name);
        return this.aggs;
    }

    public Aggs aggsIfTrue(boolean ifTrue, String name) {
        if (!ifTrue) {
            return this;
        }
        this.aggs = new Aggs(name);
        return this.aggs;
    }

    @Override
    public String toString() {
        if (aggs == null) {
            return String.format(TPL, name, operator, aggsItem);
        } else {
            return String.format(TPL_AGGS, name, operator, aggsItem, aggs);
        }
    }
}
