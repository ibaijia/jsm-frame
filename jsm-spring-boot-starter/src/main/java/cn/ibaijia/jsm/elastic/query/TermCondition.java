package cn.ibaijia.jsm.elastic.query;

import cn.ibaijia.jsm.elastic.util.JsonUtil;

/**
 * term：代表完全匹配，也就是精确查询，搜索前不会再对搜索词进行分词解析，直接对搜索词进行查找；
 *
 * @Author: LongZL
 * @Date: 2022/7/12 11:08
 */
public class TermCondition implements Condition {
    private static final String TPL = "{\"term\":{\"%s\":\"%s\"}}";

    private String filed;
    private String value;

    public TermCondition(String filed, String value) {
        this.filed = filed;
        this.value = value;
    }

    public TermCondition(String filed, Object value) {
        this.filed = filed;
        if (value instanceof String) {
            this.value = (String) value;
        } else {
            this.value = JsonUtil.toJsonString(value);
        }
    }

    @Override
    public String toString() {
        return String.format(TPL, filed, value);
    }
}
