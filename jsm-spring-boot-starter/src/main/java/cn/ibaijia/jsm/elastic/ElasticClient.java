package cn.ibaijia.jsm.elastic;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.http.HttpAsyncClient;
import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpResponse;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.concurrent.FutureCallback;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author longzl
 */
public class ElasticClient {

    private Logger logger = StatusLogger.getLogger();

    private String username;
    private String password;
    private String indexAddress;
    private String type = "_doc";
    private String postAddress;
    private String header;
    private String batchHeader;
    private String indexOptions;
    private final static String VAR_ID = "varId";

    private HttpClient httpClient = new HttpClient();
    private HttpAsyncClient httpAsyncClient = new HttpAsyncClient();

    public ElasticClient(String indexAddress) {
        httpClient.setWithExtra(false);
        this.indexAddress = indexAddress;
        this.postAddress = indexAddress + "/" + this.type;
        String idx = indexAddress.substring(indexAddress.lastIndexOf("/") + 1);
        this.header = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"}}", idx, type);
        this.batchHeader = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}", idx, type, VAR_ID);
    }

    public ElasticClient(String indexAddress, String type) {
        this.indexAddress = indexAddress;
        if (type != null) {
            this.type = type;
        }
        this.postAddress = indexAddress + "/" + this.type;
        String idx = indexAddress.substring(indexAddress.lastIndexOf("/") + 1);
        this.header = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"}}", idx, type);
        this.batchHeader = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}", idx, type, VAR_ID);
    }

    public ElasticClient(String indexAddress, String type, String username, String password) {
        this.indexAddress = indexAddress;
        if (type != null) {
            this.type = type;
        }
        this.postAddress = indexAddress + "/" + this.type;
        this.username = username;
        this.password = password;
        String idx = indexAddress.substring(indexAddress.lastIndexOf("/") + 1);
        this.header = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"}}", idx, type);
        this.batchHeader = String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}", idx, type, VAR_ID);
    }

    public void setCredentialsProvider(CredentialsProvider credentialsProvider) {
        this.httpClient.setCredentialsProvider(credentialsProvider);
        this.httpAsyncClient.setCredentialsProvider(credentialsProvider);
    }

    public String search(String queryJson) {
        String address = indexAddress + "/_search";
        return httpClient.post(address, queryJson);
    }

    public String searchHits(String queryJson) {
        String address = indexAddress + "/_search";
        String res = httpClient.post(address, queryJson);
        JsonNode hits1 = JsonUtil.readTree(res).get("hits");
        if (hits1 == null) {
            logger.warn("hits1 is null");
            return null;
        }
        JsonNode hits2 = hits1.get("hits");
        if (hits2 == null) {
            logger.warn("hits2 is null");
            return null;
        }
        return hits2.toString();
    }

    public String searchAggs(String queryJson) {
        String address = indexAddress + "/_search";
        String res = httpClient.post(address, queryJson);
        JsonNode aggregations = JsonUtil.readTree(res).get("aggregations");
        if (aggregations == null) {
            logger.warn("aggregations is null");
            return null;
        }
        return aggregations.toString();
    }

    public String searchAsString(String queryJson) {
        String address = indexAddress + "/_search";
        String res = httpClient.post(address, queryJson);
        return res;
    }

    public <T> T search(String queryJson, TypeReference<T> type) {
        String res = searchHits(queryJson);
        return JsonUtil.parseObject(res, type);
    }

    public <T> T search(String queryJson, Class<T> type) {
        String res = searchHits(queryJson);
        return JsonUtil.parseObject(res, type);
    }

    public <T> T search(String queryJson, JavaType type) {
        String res = searchHits(queryJson);
        return JsonUtil.parseObject(res, type);
    }

    public String deleteByQuery(String queryJson) {
        String address = indexAddress + "/_delete_by_query";
        return httpClient.post(address, queryJson);
    }

    public String updateByQuery(String queryJson) {
        String address = indexAddress + "/_update_by_query";
        return httpClient.post(address, queryJson);
    }


    /**
     * 检查Index是否存在，存在返回true
     *
     * @return
     */
    public boolean checkIndex() {
        logger.debug("ElasticClient checkIndex:" + indexAddress);
        String res = httpClient.get(indexAddress);
        logger.debug("ElasticClient checkIndex res:" + res);
        Assert.notNull(res, "ElasticClient checkIndex result null.");
        JsonNode jsonNode = JsonUtil.readTree(res);
        Assert.notNull(jsonNode, "ElasticClient readTree null");
        JsonNode statusNode = jsonNode.get("status");
        return statusNode == null ? true : false;
    }

    /**
     * 创建索引
     *
     * @return
     */
    public String createIndex() {
        Assert.notNull(indexOptions, String.format("indexOptions is null,indexAddress:%s", indexAddress));
        logger.debug("ElasticClient createIndex:" + indexOptions);
        String res = httpClient.put(indexAddress, indexOptions);
        logger.debug("ElasticClient createIndex res:" + res);
        return res;
    }

    public String deleteIndex() {
        logger.debug("ElasticClient deleteIndex:" + this.indexAddress);
        String res = httpClient.delete(indexAddress);
        logger.debug("ElasticClient deleteIndex res:" + res);
        return res;
    }

    public String recreateIndex() {
        deleteIndex();
        return createIndex();
    }

    /**
     * 检查不存在就创建索引
     *
     * @return
     */
    public boolean createIndexIfNotExists() {
        try {
            boolean exists = checkIndex();
            if (!exists) {
                String res = createIndex();
                JsonNode jsonNode = JsonUtil.readTree(res);
                Assert.notNull(jsonNode, "createIndex result null");
                JsonNode errorNode = jsonNode.get("error");
                Assert.isNull(errorNode, "createIndex failed:" + res);
            } else {
                logger.info("ElasticClient index exists:" + indexAddress);
            }
            return true;
        } catch (Exception e) {
            logger.error("createIndexIfNotExists error:" + indexAddress);
            return false;
        }
    }

    /**
     * 添加文档
     *
     * @param jsonData
     * @return
     */
    public void post(String jsonData) {
        logger.debug("ElasticClient post:" + jsonData);
        httpAsyncClient.post(postAddress, jsonData, new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse result) {
                logger.debug("httpAsyncClient post res:" + result.getEntity());
            }

            @Override
            public void failed(Exception ex) {
                logger.debug("httpAsyncClient post failed!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post failed!"));
            }

            @Override
            public void cancelled() {
                logger.debug("httpAsyncClient post cancelled!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post cancelled!"));
            }
        });
    }

    public void post(String id, String jsonData) {
        logger.debug("ElasticClient post:" + jsonData);
        httpAsyncClient.post(postAddress + "/" + id, jsonData, new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse result) {
                logger.debug("httpAsyncClient post res:" + result.getEntity());
            }

            @Override
            public void failed(Exception ex) {
                logger.debug("httpAsyncClient post failed!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post failed!"));
            }

            @Override
            public void cancelled() {
                logger.debug("httpAsyncClient post cancelled!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post cancelled!"));
            }
        });
    }

    public void postBatch(List<BatchObject> batchObjectList) {
        if (batchObjectList == null || batchObjectList.isEmpty()) {
            return;
        }
        logger.debug("ElasticClient post batchObjectList size:" + batchObjectList.size());
        StringBuilder sb = new StringBuilder();
        for (BatchObject batchObject : batchObjectList) {
            sb.append(this.batchHeader.replace(VAR_ID, batchObject.id)).append("\n");
            sb.append(batchObject.body).append("\n");
        }
        postToElastic(sb);
    }

    private void postToElastic(StringBuilder sb) {
        httpAsyncClient.post(postAddress + "/_bulk", sb.toString(), new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse result) {
                logger.debug("httpAsyncClient post res:" + result.getEntity());
            }

            @Override
            public void failed(Exception ex) {
                logger.debug("httpAsyncClient post failed!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post failed!"));
            }

            @Override
            public void cancelled() {
                logger.debug("httpAsyncClient post cancelled!");
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_ELASTIC_CLIENT, "httpAsyncClient post cancelled!"));
            }
        });
    }

    public void postBatchStr(List<String> jsonDataList) {
        if (jsonDataList == null || jsonDataList.isEmpty()) {
            return;
        }
        logger.debug("ElasticClient post jsonDataList size:" + jsonDataList.size());
        StringBuilder sb = new StringBuilder();
        for (String jsonData : jsonDataList) {
            sb.append(header).append("\n");
            sb.append(jsonData).append("\n");
        }
        postToElastic(sb);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIndexAddress() {
        return indexAddress;
    }

    public void setIndexAddress(String indexAddress) {
        this.indexAddress = indexAddress;
    }

    public String getIndexOptions() {
        return indexOptions;
    }

    public void setIndexOptions(String indexOptions) {
        this.indexOptions = indexOptions;
    }

}
