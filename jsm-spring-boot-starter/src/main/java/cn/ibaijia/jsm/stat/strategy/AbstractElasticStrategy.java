package cn.ibaijia.jsm.stat.strategy;

import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.disruptor.JsmDisruptor;
import cn.ibaijia.jsm.elastic.BatchObject;
import cn.ibaijia.jsm.elastic.ElasticClient;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.lmax.disruptor.EventHandler;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 17:11
 */
public abstract class AbstractElasticStrategy implements LogStrategy {

    private ElasticClient elasticClient;
    private AtomicLong idAi = new AtomicLong(0);
    private List<BatchObject> cacheMessage = new ArrayList<BatchObject>();
    private static final int BATCH_SIZE = 500;
    private static final int REMAIN_SIZE = 100;

    private String username;
    private String pass;


    private JsmDisruptor<String> stringDisruptor = new JsmDisruptor<String>(new EventHandler<JsmDisruptor<String>.MessageEvent<String>>() {
        @Override
        public void onEvent(JsmDisruptor<String>.MessageEvent<String> stringMessageEvent, long sequence, boolean endOfBatch){
            try {
                cacheMessage.add(new BatchObject(getId(), stringMessageEvent.message));
                if (endOfBatch || (sequence + 1) % BATCH_SIZE == 0) {
                    elasticClient.postBatch(cacheMessage);
                    cacheMessage.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }, 4 * 1024);

    public void init(){
        if (elasticClient == null) {
            elasticClient = createElasticClient();
        }
    }
    @Override
    public void write(Serializable object) {
        String text = null;
        if (object instanceof String) {
            text = (String) object;
        } else {
            text = JsonUtil.toJsonString(object);
        }
        if (elasticClient == null) {
            elasticClient = createElasticClient();
            if (elasticClient == null) {
                return;
            } else {
               configDefault();
            }
        }
        stringDisruptor.pushMessage(text);
    }

    private void configDefault() {
        if(!StringUtil.isEmpty(username) && !StringUtil.isEmpty(pass)){
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,new UsernamePasswordCredentials(username,pass));
            elasticClient.setCredentialsProvider(credentialsProvider);
        }
    }

    private String getId() {
        try {
            long intCount = 0;
            intCount = idAi.getAndIncrement();
            if (intCount > Long.MAX_VALUE - REMAIN_SIZE) {
                idAi.set(0);
            }
            return String.format("%s-%s", DateUtil.format(DateUtil.currentDate(), DateUtil.COMPACT_DATE_PATTERN), intCount);
        } catch (Exception e) {
            return StringUtil.uuid();
        }
    }

    protected String getEsAddress(){
        String address = AppContext.get(AppContextKey.STAT_ELASTIC_ADDRESS);
        username = AppContext.get(AppContextKey.STAT_ELASTIC_USER);
        pass = AppContext.get(AppContextKey.STAT_ELASTIC_PASS);
        if (StringUtil.isEmpty(address)) {
            address = AppContext.get(AppContextKey.ELASTIC_ADDRESS);
            username = AppContext.get(AppContextKey.ELASTIC_USER);
            pass = AppContext.get(AppContextKey.ELASTIC_PASS);
        }
        if (StringUtil.isEmpty(address)) {
            System.out.println(AppContextKey.STAT_ELASTIC_ADDRESS + " or " + AppContextKey.ELASTIC_ADDRESS + " for ElasticStrategy not found.");
        }
        return address;
    }

    /**
     * 实现如下方法 返回ElasticClient
     *
     * @return 客户端
     */
    protected abstract ElasticClient createElasticClient();

}
