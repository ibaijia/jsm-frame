package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.stat.model.Alarm;

import java.util.List;

/**
 * @author longzl
 */
public interface JsmAlarmService {
    /**
     * 添加
     * @param alarm
     */
    public void add(Alarm alarm);
    /**
     * 清除
     */
    void clear();
    /**
     * 拉取
     * @return
     */
    List<Alarm> pull();
}
