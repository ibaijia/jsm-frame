package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.stat.model.AmsBossHealth;
import cn.ibaijia.jsm.stat.model.SystemStat;
import cn.ibaijia.jsm.utils.HttpClientUtil;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author longzl
 */
@Service
public class DefaultSysStatServiceImpl extends BaseService implements JsmSysStatService, InitializingBean {

    private LogStrategy logStrategy;

    @Value("${" + AppContextKey.SYS_STAT_INTERVAL + ":600}")
    private Integer interval;

    public DefaultSysStatServiceImpl() {
    }

    private void loadLogStrategy() {
        if (logStrategy == null) {
            String logType = AppContext.get(AppContextKey.SYS_STAT_TYPE, BaseConstants.STRATEGY_TYPE_NONE);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(logType)) {
                logStrategy = SpringContext.getBean("consoleStrategy");
            } else {
                logStrategy = SpringContext.getBean(logType + "SysStatStrategy");
            }
        }
    }

    @Override
    public void add(SystemStat sysStat) {
        loadLogStrategy();
        if (logStrategy != null) {
            logStrategy.write(sysStat);
        } else {
            logger.trace(JsonUtil.toJsonString(sysStat));
        }
    }

    @Override
    public void writeNow() {
        SystemStat systemStat = SystemUtil.getSystemStat();
        this.add(systemStat);
        String amsBossHost = AppContext.get(AppContextKey.AMS_BOSS_HOST);
        String amsBossAppKey = AppContext.get(AppContextKey.AMS_BOSS_APP_KEY);
        String amsBossAppSecret = AppContext.get(AppContextKey.AMS_BOSS_APP_SECRET);
        if(!StringUtil.isEmpty(amsBossHost) && !StringUtil.isEmpty(amsBossAppKey) && !StringUtil.isEmpty(amsBossAppSecret)){
            AmsBossHealth amsBossHealth = new AmsBossHealth();
            amsBossHealth.appKey = amsBossAppKey;
            amsBossHealth.appSecret = amsBossAppSecret;
            amsBossHealth.systemStat = systemStat;
            amsBossHealth.alarmList = SystemUtil.pullAlarms();
            try {
                HttpClientUtil.post(amsBossHost,amsBossHealth);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void start() {
        try {
            logger.info("JsmSysStatService delay:60 interval:{}", interval);
            ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(1,runnable -> new Thread(runnable, "jsm-stat-service-scheduler"));
            scheduler.scheduleAtFixedRate(this::writeNow, 60, interval, TimeUnit.SECONDS);
        } catch (Exception e) {
            logger.error("start JsmSysStatService error.", e);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        start();
    }
}
