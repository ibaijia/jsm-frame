package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.stat.model.ApiStat;

/**
 * @author longzl
 */
public interface JsmApiStatService {

    /**
     * 添加统计
     * @param apiStat
     */
    void add(ApiStat apiStat);

}
