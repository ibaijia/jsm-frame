package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author longzl
 */
public class AmsBossHealth extends SystemHealth {
    @FieldAnn(required = true, comments = "appKey")
    public String appKey;
    @FieldAnn(required = true, comments = "appSecret")
    public String appSecret;
}
