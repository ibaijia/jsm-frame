package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @author longzl
 */
public class ApiStat implements ValidateModel {

    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId = AppContext.get(AppContextKey.CLUSTER_ID, "");

    @FieldAnn(required = false, comments = "应用名")
    public String appName = AppContext.get(AppContextKey.APP_NAME, "");

    @FieldAnn(required = false, comments = "环境")
    public String env = AppContext.get(AppContextKey.ENV, "");

    @FieldAnn(required = false, comments = "GIT_COMMIT_ID")
    public String gitHash = AppContext.get(AppContextKey.GIT_HASH, "");
    @FieldAnn(required = false, maxLen = 50, comments = "客户端ID")
    public String clientId;
    @FieldAnn(required = false, maxLen = 50, comments = "请求ID")
    public String traceId;

    @FieldAnn(required = false, maxLen = 250, comments = "URL")
    public String url;

    @FieldAnn(required = false, maxLen = 10, comments = "Method")
    public String method;

    @FieldAnn(required = false, comments = "开始时间")
    public Long beginTime;

    @FieldAnn(required = false, comments = "结束时间")
    public Long endTime;

    @FieldAnn(required = false, comments = "耗时(毫秒)")
    public Long spendTime;

    @FieldAnn(required = false, comments = "Http响应代码")
    public Integer httpStatus;

    @FieldAnn(required = false, maxLen = 20, comments = "业务响应代码")
    public String respCode;

    @FieldAnn(required = false, comments = "时间")
    public Long time;
}
