package cn.ibaijia.jsm.stat.strategy;

import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.disruptor.JsmDisruptor;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.utils.JsonUtil;
import com.lmax.disruptor.EventHandler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 17:11
 */
public abstract class AbstractRedisStrategy implements LogStrategy {

    private List<String> cacheMessage = new ArrayList<>();
    private static final int BATCH_SIZE = 500;

    protected JedisService jedisService;

    protected boolean check(){
        if(jedisService == null){
            try {
                jedisService = SpringContext.getBean("statJedisService");
            } catch (Exception e){

            }
            if(jedisService == null){
                try {
                    jedisService = SpringContext.getBean("jedisService");
                } catch (Exception e){

                }
            }
        }
        return jedisService != null;
    }

    private JsmDisruptor<String> stringDisruptor = new JsmDisruptor<String>(new EventHandler<JsmDisruptor<String>.MessageEvent<String>>() {
        @Override
        public void onEvent(JsmDisruptor<String>.MessageEvent<String> stringMessageEvent, long sequence, boolean endOfBatch) {
            try {
                cacheMessage.add(stringMessageEvent.message);
                if (endOfBatch || (sequence + 1) % BATCH_SIZE == 0) {
                    for (String msg : cacheMessage) {
                        writeToRedis(msg);
                    }
                    cacheMessage.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }, 4 * 1024);

    @Override
    public void write(Serializable object) {
        String text = null;
        if (object instanceof String) {
            text = (String) object;
        } else {
            text = JsonUtil.toJsonString(object);
        }
        stringDisruptor.pushMessage(text);
    }

    /**
     * 写入redis
     * @param msg
     */
    protected abstract void writeToRedis(String msg);

}
