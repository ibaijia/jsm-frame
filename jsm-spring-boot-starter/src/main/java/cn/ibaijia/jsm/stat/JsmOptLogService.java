package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.dao.model.OptLog;

/**
 * @author longzl
 */
public interface JsmOptLogService {

	/**
	 * 添加操作日志
	 * @param optLog
	 */
	void add(OptLog optLog);

}
