package cn.ibaijia.jsm.stat.strategy;

import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.elastic.ElasticClient;
import cn.ibaijia.jsm.utils.StringUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 18:00
 */
@Component("elasticAlarmStrategy")
public class ElasticAlarmStrategy extends AbstractElasticStrategy {

    @Value("${" + AppContextKey.ALARM_IDX + ":jsm_alarm_idx}")
    private String idx;

    @Override
    protected ElasticClient createElasticClient() {
        String address = getEsAddress();
        if (StringUtil.isEmpty(address)) {
            return null;
        }
        ElasticClient elasticClient = new ElasticClient(String.format("%s/%s", address, idx));
        elasticClient.setIndexOptions(BaseConstants.ES_JSON_OPTIONS_ALARM);
        elasticClient.createIndexIfNotExists();
        return elasticClient;
    }
}
