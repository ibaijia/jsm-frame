package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.stat.model.ApiStat;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import cn.ibaijia.jsm.utils.ThreadLocalUtil;
import org.springframework.stereotype.Service;

/**
 * @author longzl
 */
@Service
public class DefaultApiStatServiceImpl extends BaseService implements JsmApiStatService {

    private int slowApiLimit;

    private LogStrategy logStrategy;

    public DefaultApiStatServiceImpl() {
    }

    private void loadLogStrategy() {
        if (logStrategy == null) {
            this.slowApiLimit = AppContext.getAsInteger(AppContextKey.SLOW_API_LIMIT, 3000);
            String logType = AppContext.get(AppContextKey.API_STAT_TYPE, BaseConstants.STRATEGY_TYPE_NONE);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(logType)) {
                logStrategy = SpringContext.getBean("consoleStrategy");
            } else {
                logStrategy = SpringContext.getBean(logType + "ApiStatStrategy");
            }
        }
    }

    @Override
    public void add(ApiStat apiStat) {
        loadLogStrategy();
        Integer customSlowApiLimit = ThreadLocalUtil.SLOW_API_LIMIT_TL.get();
        if(customSlowApiLimit != null){
            if (apiStat.spendTime > customSlowApiLimit) {
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_SLOW_API, JsonUtil.toJsonString(apiStat)));
            }
        }else{
            if (apiStat.spendTime > slowApiLimit) {
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_SLOW_API, JsonUtil.toJsonString(apiStat)));
            }
        }

        if (logStrategy != null) {
            logStrategy.write(apiStat);
        } else {
            logger.trace(JsonUtil.toJsonString(apiStat));
        }
    }
}
