package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @author longzl
 */
public class ContainerStat implements ValidateModel {

    @FieldAnn(required = false, comments = "环境")
    public String env;
    @FieldAnn(required = false, comments = "IP")
    public String ip;
    @FieldAnn(required = false, comments = "时间")
    public Long time;

    @FieldAnn(required = false, maxLen = 50, comments = "容器ID")
    public String id;
    @FieldAnn(required = false, comments = "名称")
    public String name;

    @FieldAnn(required = false, comments = "CPU使用率百分比")
    public Float cpuRatio;
    @FieldAnn(required = false, comments = "内存使用率百分比")
    public Float memRatio;

    @FieldAnn(required = false, comments = "总内存 单位B")
    public Long totalMemory;
    @FieldAnn(required = false, comments = "已用内存 单位B")
    public Long useMemory;

    @FieldAnn(required = false, comments = "网络输入 单位B")
    public Long netInput;
    @FieldAnn(required = false, comments = "网络输出 单位B")
    public Long netOutput;

    @FieldAnn(required = false, comments = "阻塞输入 单位B")
    public Long blockInput;
    @FieldAnn(required = false, comments = "阻塞输出 单位B")
    public Long blockOutput;

}
