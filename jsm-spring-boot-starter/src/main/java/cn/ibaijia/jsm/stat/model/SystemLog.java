package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;

/**
 * @Author: LongZL
 * @Date: 2022/10/24 11:01
 */
public class SystemLog implements ValidateModel {

    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId;
    @FieldAnn(required = false, comments = "应用名")
    public String appName;
    @FieldAnn(required = false, comments = "环境")
    public String env;
    @FieldAnn(required = false, comments = "GIT_COMMIT_ID")
    public String gitHash;
    @FieldAnn(required = false, maxLen = 50, comments = "客户端ID")
    public String clientId;
    @FieldAnn(required = false, maxLen = 50, comments = "请求ID")
    public String traceId;
    @FieldAnn(required = false, comments = "用户ID")
    public String uid;
    @FieldAnn(required = false, comments = "时间")
    public Long time;
    @FieldAnn(required = false, comments = "级别")
    public String level;
    @FieldAnn(required = false, comments = "线程")
    public String thread;
    @FieldAnn(required = false, comments = "类名")
    public String logName;
    @FieldAnn(required = false, comments = "方法名")
    public String methodName;
    @FieldAnn(required = false, comments = "日志内容")
    public String logMsg;
    @FieldAnn(required = false, comments = "异常内容")
    public String exMsg;
    @FieldAnn(required = false, comments = "异常名称")
    public String exName;
    @FieldAnn(required = false, comments = "异常栈")
    public String exTrace;

}
