package cn.ibaijia.jsm.stat.strategy;

import cn.ibaijia.jsm.cache.jedis.JedisCmd;
import cn.ibaijia.jsm.context.AppContextKey;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

/**
 * @Author: LongZL
 * @Date: 2022/1/27 15:18
 */
@Component("redisAlarmStrategy")
public class RedisAlarmStrategy extends AbstractRedisStrategy {

    @Value("${" + AppContextKey.ALARM_IDX + ":jsm_alarm_idx}")
    private String idx;

    @Override
    public void writeToRedis(String object) {
        if(!check()){
            return;
        }
        String finalText = object;
        jedisService.exec(new JedisCmd<Long>() {
            @Override
            public Long run(boolean cluster, Jedis jedis, JedisCluster jedisCluster) throws Exception {
                if (cluster) {
                    return jedisCluster.publish(idx, finalText);
                } else {
                    return jedis.publish(idx, finalText);
                }
            }

            @Override
            public void throwable(Throwable throwable) {
                jedisService.addAlarm("exec publish", throwable.getMessage());
            }
        });
    }
}
