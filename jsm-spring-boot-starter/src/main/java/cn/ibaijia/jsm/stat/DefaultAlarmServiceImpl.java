package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.context.service.BaseService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.JsonUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * @author longzl
 */
@Service
public class DefaultAlarmServiceImpl extends BaseService implements JsmAlarmService {

    private Queue<Alarm> alarmQueue = new ConcurrentLinkedDeque<>();
    private Integer maxCount;

    private LogStrategy logStrategy;

    public DefaultAlarmServiceImpl() {
    }

    private void loadLogStrategy() {
        if (logStrategy == null) {
            String logType = AppContext.get(AppContextKey.ALARM_TYPE, BaseConstants.STRATEGY_TYPE_NONE);
            maxCount = AppContext.getAsInteger(AppContextKey.ALARM_MAX_COUNT, 100);
            if (BaseConstants.STRATEGY_TYPE_NONE.equals(logType)) {
                return;
            }
            if (BaseConstants.STRATEGY_TYPE_CONSOLE.equals(logType)) {
                logStrategy = SpringContext.getBean("consoleStrategy");
            } else {
                logStrategy = SpringContext.getBean(logType + "AlarmStrategy");
            }
        }
    }

    @Override
    public void add(Alarm alarm) {
        loadLogStrategy();
        if (logStrategy != null) {
            logStrategy.write(alarm);
        } else {
            logger.trace(JsonUtil.toJsonString(alarm));
            if (alarmQueue.size() < maxCount) {
                alarmQueue.add(alarm);
            }
        }
    }

    @Override
    public void clear() {
        alarmQueue.clear();
    }

    @Override
    public List<Alarm> pull() {
        List<Alarm> alarms = new ArrayList<>();
        int len = alarmQueue.size();
        for (int i = 0; i < len; i++) {
            alarms.add(alarmQueue.poll());
        }
        return alarms;
    }

}
