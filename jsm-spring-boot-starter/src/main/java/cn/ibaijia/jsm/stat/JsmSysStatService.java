package cn.ibaijia.jsm.stat;

import cn.ibaijia.jsm.stat.model.SystemStat;

/**
 * @author longzl
 */
public interface JsmSysStatService {

    /**
     * 添加
     * @param sysStat
     */
    public void add(SystemStat sysStat);

    /**
     * 立即输入
     */
    public void writeNow();

    /**
     * 启动
     */
    public void start();

}
