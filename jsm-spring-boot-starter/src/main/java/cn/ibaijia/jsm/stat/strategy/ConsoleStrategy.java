package cn.ibaijia.jsm.stat.strategy;

import cn.ibaijia.jsm.log4j2.LogStrategy;
import cn.ibaijia.jsm.utils.JsonUtil;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Author: LongZL
 * @Date: 2021/11/18 17:11
 */
@Component("consoleStrategy")
public class ConsoleStrategy implements LogStrategy {

    @Override
    public void write(Serializable object) {
        if (object instanceof String) {
            System.out.println(object);
        } else {
            System.out.println(JsonUtil.toJsonString(object));
        }
    }

}
