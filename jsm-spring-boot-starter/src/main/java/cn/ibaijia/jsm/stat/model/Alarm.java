package cn.ibaijia.jsm.stat.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.IpUtil;

/**
 * @author longzl
 */
public class Alarm implements ValidateModel {
    @FieldAnn(required = false, comments = "发生时间")
    public Long time;
    @FieldAnn(required = false, comments = "级别 0 ERROR 1 WARN 2 INFO 3 DEBUG")
    public int level = 0;
    @FieldAnn(required = false, maxLen = 50, comments = "名称")
    public String name;
    @FieldAnn(required = false, maxLen = 100, comments = "描述")
    public String comments;
    @FieldAnn(required = false, maxLen = 50, comments = "客户端ID")
    public String clientId;
    @FieldAnn(required = false, maxLen = 50, comments = "请求ID")
    public String traceId;
    @FieldAnn(required = false, maxLen = 50, comments = "IP")
    public String ip;
    @FieldAnn(required = false, maxLen = 50, comments = "服务ID")
    public String clusterId;
    @FieldAnn(required = false, comments = "应用名")
    public String appName;
    @FieldAnn(required = false, comments = "环境")
    public String env;
    @FieldAnn(required = false, comments = "GIT_COMMIT_ID")
    public String gitHash;

    public Alarm(int level, String name, String comments) {
        this.level = level;
        this.name = name;
        this.comments = comments;
        this.clusterId = AppContext.getClusterId();
        this.time = DateUtil.currentTime();
        this.traceId = WebContext.getTraceId();
        this.clientId = WebContext.getClientId();
        this.ip = IpUtil.getHostIp();
        this.appName = AppContext.get(AppContextKey.APP_NAME, "");
        this.env = AppContext.get(AppContextKey.ENV, "");
        this.gitHash = AppContext.get(AppContextKey.GIT_HASH, "");
    }

    public Alarm(String name, String comments) {
        this.name = name;
        this.comments = comments;
        this.clusterId = AppContext.getClusterId();
        this.time = DateUtil.currentTime();
        this.traceId = WebContext.getTraceId();
        this.clientId = WebContext.getClientId();
        this.ip = IpUtil.getHostIp();
        this.appName = AppContext.get(AppContextKey.APP_NAME, "");
        this.env = AppContext.get(AppContextKey.ENV, "");
        this.gitHash = AppContext.get(AppContextKey.GIT_HASH, "");
    }

    public Alarm() {
    }
}
