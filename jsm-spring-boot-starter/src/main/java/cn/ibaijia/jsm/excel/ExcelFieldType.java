package cn.ibaijia.jsm.excel;

/**
 * @author longzl
 */

public enum ExcelFieldType {
	/**
	 *
	 */
	STRING(0,"string"),
	/**
	 *
	 */
	NUMBER(1,"number"),
	/**
	 *
	 */
	DATE(2,"date"),
	/**
	 *
	 */
	FLOAT(3,"float"),
	/**
	 *
	 */
	DATE_TIME(4,"datetime");
	private int v;
	private String t;
	ExcelFieldType(int value,String t){
		this.v = value;
		this.t = t;
	}
	public int v(){
		return v;
	}
	public static String findText(int v){
		ExcelFieldType[] values = ExcelFieldType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].v == v){
				return values[i].t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		ExcelFieldType[] values = ExcelFieldType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].t.equals(t)){
				return values[i].v;
			}
		}
		return 0;
	}
}
