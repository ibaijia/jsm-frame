package cn.ibaijia.jsm.excel;
/**
 * @author longzl
 */
public enum TrimType {
	/**
	 *
	 */
	NONE(0,"none"),
	/**
	 *
	 */
	SPACE(1,"space"),
	/**
	 *
	 */
	CRLF(2,"crlf"),
	/**
	 *
	 */
	TAB(3,"tab"),
	/**
	 *
	 */
	ALL(4,"all");
	private int v;
	private String t;
	TrimType(int value, String t){
		this.v = value;
		this.t = t;
	}
	public int v(){
		return v;
	}
	public static String findText(int v){
		TrimType[] values = TrimType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].v == v){
				return values[i].t;
			}
		}
		return null;
	}
	public static int findInt(String t){
		TrimType[] values = TrimType.values();
		for (int i=0;i<values.length;i++){
			if (values[i].t.equals(t)){
				return values[i].v;
			}
		}
		return 0;
	}
}
