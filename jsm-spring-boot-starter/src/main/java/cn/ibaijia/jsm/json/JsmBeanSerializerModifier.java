package cn.ibaijia.jsm.json;

import cn.ibaijia.jsm.utils.JsonUtil;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/4/8 16:13
 */
public class JsmBeanSerializerModifier extends BeanSerializerModifier {

    @Override
    public List<BeanPropertyWriter> changeProperties(SerializationConfig config, BeanDescription beanDesc, List<BeanPropertyWriter> beanProperties) {
        beanProperties.forEach(b -> {
            if (JsonUtil.getJsmJsonConfig().isShowNull()) {
                b.assignNullSerializer(new JsmNullJsonSerializer(b));
            }
        });
        return beanProperties;
    }
}
