package cn.ibaijia.jsm.json;

import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.utils.JsonUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * @Author: LongZL
 * @Date: 2022/4/8 16:14
 */
public class DicAnnJsonDeserializer extends JsonDeserializer<Object> {

    private static final String OBJ_START = "{";
    private static final String OBJ_END = "}";
    private static final String ARRAY_START = "[{";
    private static final String ARRAY_END = "}]";
    private static final String DQM = "\"";
    private static final String COMMA = ",";
    private static final String COLON = ":";

    private DicAnn dicAnn;
    private JavaType javaType;

    public DicAnnJsonDeserializer(DicAnn dicAnn, JavaType javaType) {
        this.dicAnn = dicAnn;
        this.javaType = javaType;
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        if (!dicAnn.toDicKV()) {
            //这种方式不可逆转
            return null;
        }
        if (javaType.isCollectionLikeType() || javaType.isArrayType()) {
            String text = getListDicText(jsonParser, dicAnn);
            return JsonUtil.parseObject(text, javaType);
        } else {
            String text = getOneDicText(jsonParser, dicAnn);
            return JsonUtil.parseObject(text, javaType);
        }
    }

    private String getListDicText(JsonParser jsonParser, DicAnn dicAnn) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        boolean findKey = false;
        while (true) {
            String text = jsonParser.getText();
            jsonParser.nextToken();
            if (ARRAY_END.equals(text) || text == null) {
                break;
            }
            if (dicAnn.dicKeyName().equals(text)) {
                findKey = true;
            }
            if (!findKey) {
                continue;
            }
            text = jsonParser.getText();
            jsonParser.nextToken();
            if (sb.length() > 1) {
                sb.append(COMMA);
            }
            sb.append(DQM).append(text).append(DQM);
            findKey = false;
        }
        sb.append("]");
        return sb.toString();
    }

    private String getOneDicText(JsonParser jsonParser, DicAnn dicAnn) throws IOException {
        boolean findKey = false;
        while (true) {
            String text = jsonParser.getText();
            if (text == null) {
                break;
            }
            jsonParser.nextToken();
            if (dicAnn.dicKeyName().equals(text)) {
                findKey = true;
            }
            if (!findKey) {
                continue;
            }
            text = jsonParser.getText();
            jsonParser.nextToken();
            return text;
        }
        return null;
    }
}
