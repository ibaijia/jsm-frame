package cn.ibaijia.jsm.json;

import cn.ibaijia.jsm.annotation.CacheType;
import cn.ibaijia.jsm.annotation.DicAnn;
import cn.ibaijia.jsm.cache.CacheL1;
import cn.ibaijia.jsm.cache.CacheL2;
import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.*;

/**
 * @Author: LongZL
 * @Date: 2022/4/8 16:14
 */
public class DicAnnJsonSerializer extends JsonSerializer<Object> {

    private DicAnn dicAnn;
    private JavaType javaType;

    public DicAnnJsonSerializer(DicAnn dicAnn, JavaType javaType) {
        this.dicAnn = dicAnn;
        this.javaType = javaType;
    }

    @Override
    public void serialize(Object value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (javaType.isCollectionLikeType() || javaType.isArrayType()) {
            String keyListStr = JsonUtil.toJsonString(value);
            List<String> keyList = JsonUtil.parseObject(keyListStr, new TypeReference<List<String>>() {
            });
            List list = new ArrayList<>();
            for (String o : keyList) {
                buildOneStringWithAnn(o, dicAnn, list);
            }
            jsonGenerator.writeObject(list.toArray());
        } else {
            writeOneStringWithAnn(value, jsonGenerator, dicAnn);
        }
    }

    private boolean buildOneStringWithAnn(String dicKey, DicAnn dicAnn, List list) {
        String dicValue = getDicValue(dicAnn, dicKey);
        buildStringWithAnn(list, dicAnn, dicKey, dicValue);
        return false;
    }

    private String getDicValue(DicAnn dicAnn, String dickKey) {
        if (dicAnn.cacheType().equals(CacheType.L1L2)) {
            CacheService cache = SpringContext.getBean(CacheService.class);
            if (cache != null) {
                String dicValue = cache.hget(dicAnn.name(), dickKey);
                return dicValue;
            }
        }

        if (dicAnn.cacheType().equals(CacheType.L2)) {
            CacheL2 cache = SpringContext.getBean(CacheL2.class);
            if (cache != null) {
                String dicValue = cache.hget(dicAnn.name(), dickKey);
                return dicValue;
            }
        }

        if (dicAnn.cacheType().equals(CacheType.L1)) {
            CacheL1 cache = SpringContext.getBean(CacheL1.class);
            if (cache != null) {
                String dicValue = cache.hget(dicAnn.name(), dickKey);
                return dicValue;
            }
        }
        return null;
    }

    private void writeOneStringWithAnn(Object value, JsonGenerator jsonGenerator, DicAnn dicAnn) throws IOException {
        String dicKey = StringUtil.toString(value);
        String dicValue = getDicValue(dicAnn, dicKey);
        writeStringWithAnn(jsonGenerator, dicAnn, dicKey, dicValue);
    }

    private void writeStringWithAnn(JsonGenerator jsonGenerator, DicAnn dicAnn, String dickKey, String dicValue) throws IOException {
        if (dicAnn.toDicKV()) {
            Map<String, String> json = new HashMap<>(16);
            json.put(dicAnn.dicKeyName(), dickKey);
            json.put(dicAnn.dicValueName(), dicValue);
            jsonGenerator.writeObject(json);
        } else {
            jsonGenerator.writeString(null == dicValue ? dickKey : dicValue);
        }
    }

    private void buildStringWithAnn(List list, DicAnn dicAnn, String dickKey, String dicValue) {
        if (dicAnn.toDicKV()) {
            Map<String, String> json = new HashMap<>(2);
            json.put(dicAnn.dicKeyName(), dickKey);
            json.put(dicAnn.dicValueName(), dicValue);
            list.add(json);
        } else {
            list.add(null == dicValue ? dickKey : dicValue);
        }
    }
}
