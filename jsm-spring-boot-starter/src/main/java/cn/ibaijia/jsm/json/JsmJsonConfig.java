package cn.ibaijia.jsm.json;

/**
 * @author longzl
 */
public class JsmJsonConfig {

    private String namingStrategy;
    private boolean showNull;
    private boolean booleanNullFalse;
    private boolean numberNullZero;
    private boolean stringNullEmpty;
    private boolean arrayNullEmpty;

    public String getNamingStrategy() {
        return namingStrategy;
    }

    public void setNamingStrategy(String namingStrategy) {
        this.namingStrategy = namingStrategy;
    }

    public boolean isShowNull() {
        return showNull;
    }

    public void setShowNull(boolean showNull) {
        this.showNull = showNull;
    }

    public boolean isBooleanNullFalse() {
        return booleanNullFalse;
    }

    public void setBooleanNullFalse(boolean booleanNullFalse) {
        this.booleanNullFalse = booleanNullFalse;
    }

    public boolean isNumberNullZero() {
        return numberNullZero;
    }

    public void setNumberNullZero(boolean numberNullZero) {
        this.numberNullZero = numberNullZero;
    }

    public boolean isStringNullEmpty() {
        return stringNullEmpty;
    }

    public void setStringNullEmpty(boolean stringNullEmpty) {
        this.stringNullEmpty = stringNullEmpty;
    }

    public boolean isArrayNullEmpty() {
        return arrayNullEmpty;
    }

    public void setArrayNullEmpty(boolean arrayNullEmpty) {
        this.arrayNullEmpty = arrayNullEmpty;
    }
}