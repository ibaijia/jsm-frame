package cn.ibaijia.jsm.json;

import cn.ibaijia.jsm.utils.JsonUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LongZL
 * @Date: 2022/4/8 16:14
 */
public class JsmNullJsonSerializer extends JsonSerializer<Object> {

    private static List<String> emptyList = new ArrayList<>();

    public JsmNullJsonSerializer(BeanProperty property) {
        this.property = property;
    }

    private BeanProperty property;

    @Override
    public void serialize(Object value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        JavaType javaType = property.getType();
        if (javaType.isTypeOrSubTypeOf(Boolean.class) && JsonUtil.getJsmJsonConfig().isBooleanNullFalse()) {
            jsonGenerator.writeBoolean(false);
        } else if (javaType.isTypeOrSubTypeOf(Number.class) && JsonUtil.getJsmJsonConfig().isNumberNullZero()) {
            jsonGenerator.writeNumber(0);
        } else if (javaType.isTypeOrSubTypeOf(String.class) && JsonUtil.getJsmJsonConfig().isStringNullEmpty()) {
            jsonGenerator.writeString("");
        } else if (javaType.isArrayType() || javaType.isCollectionLikeType()) {
            if (JsonUtil.getJsmJsonConfig().isArrayNullEmpty()) {
                jsonGenerator.writeObject(emptyList);
            } else {
                jsonGenerator.writeObject(null);
            }
        } else {
            jsonGenerator.writeObject(null);
        }
    }
}
