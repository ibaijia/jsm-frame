package cn.ibaijia.jsm.json;

import cn.ibaijia.jsm.annotation.DicAnn;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;

/**
 * @Author: LongZL
 * @Date: 2022/5/17 17:03
 */
public class DicAnnIntrospector extends NopAnnotationIntrospector {

    @Override
    public Object findSerializer(Annotated am) {
        DicAnn dicAnn = am.getAnnotation(DicAnn.class);
        if (dicAnn != null) {
            return new DicAnnJsonSerializer(dicAnn, am.getType());
        }
        return super.findSerializer(am);
    }

    @Override
    public Object findDeserializer(Annotated am) {
        DicAnn dicAnn = am.getAnnotation(DicAnn.class);
        if (dicAnn != null) {
            return new DicAnnJsonDeserializer(dicAnn, am.getType());
        }
        return super.findSerializer(am);
    }
}
