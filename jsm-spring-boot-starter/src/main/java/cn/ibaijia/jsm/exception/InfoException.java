package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author longzl
 */
public class InfoException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public InfoException(Pair errorPair) {
        super(errorPair.getMessage());
        this.errorPair = errorPair;
    }

    public InfoException(Pair errorPair, String msg) {
        super(msg);
        this.errorPair = errorPair;
        this.setMsg(msg);
    }
}
