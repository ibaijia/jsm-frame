package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;

/**
 * @author longzl
 */
public class NoLoginException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NoLoginException() {
        super(BasePairConstants.NO_LOGIN.getMessage());
        this.errorPair = BasePairConstants.NO_LOGIN;
    }
}
