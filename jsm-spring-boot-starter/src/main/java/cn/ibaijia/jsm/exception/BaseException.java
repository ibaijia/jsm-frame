package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author longzl / @createOn 2011-5-4
 *  自定义运行时异常
 */
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = -2656643850256885602L;

    protected Pair errorPair = BasePairConstants.ERROR;

    /**
     * 自定义错误信息
     */
    private String msg;

    public BaseException(String msg) {
        super(msg);
        setMsg(msg);
    }

    public BaseException(String msg, Throwable cause) {
        super(cause);
        setMsg(msg);
    }

    @Override
    public String toString() {
        return "BaseException{" +
                "errorPair=" + errorPair +
                ", msg='" + msg + '\'' +
                '}';
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Pair getErrorPair() {
        return errorPair;
    }

    public void setErrorPair(Pair errorPair) {
        this.errorPair = errorPair;
    }
}
