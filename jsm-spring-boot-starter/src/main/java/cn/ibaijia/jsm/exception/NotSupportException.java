package cn.ibaijia.jsm.exception;

/**
 * @Author: LongZL
 * @Date: 2022/4/27 18:41
 */
public class NotSupportException extends BaseException {

    public NotSupportException(String msg) {
        super(msg);
    }
}
