package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;

/**
 * @author longzl
 */
public class AlarmException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String name;

    public AlarmException(String name, String msg) {
        super(msg);
        this.setErrorPair(BasePairConstants.FAIL);
        this.setMsg(msg);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
