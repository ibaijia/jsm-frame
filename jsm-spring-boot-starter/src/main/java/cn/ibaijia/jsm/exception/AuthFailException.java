package cn.ibaijia.jsm.exception;

import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;

/**
 * @author longzl
 */
public class AuthFailException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public AuthFailException() {
        super(BasePairConstants.AUTH_FAIL.getMessage());
        this.setErrorPair(BasePairConstants.AUTH_FAIL);
    }

    public AuthFailException(Pair errorPair) {
        super(errorPair.getMessage());
        this.errorPair = errorPair;
    }

    public AuthFailException(Pair errorPair, String msg) {
        super(msg);
        this.errorPair = errorPair;
        this.setMsg(msg);
    }
}
