package cn.ibaijia.jsm.consts;

/**
 * @author longzl
 */
public class BaseConstants {

    public static final String SESSION_ATTR_KEY = "jsm:session:attrKey";
    public static final String SESSION_ID = "jsm:session:id";

    public static final String CAPTCHA_IMAGE_PREFIX = "captcha:image:";
    public static final String CAPTCHA_SMS_PREFIX = "captcha:sms:";
    public static final String CAPTCHA_EMAIL_PREFIX = "captcha:email:";
    public static final String CAPTCHA_WX_PREFIX = "captcha:wx:";

    public static final String SYSTEM_EMPTY_STRING = "";
    public static final String SYSTEM_STRING_OBJECT = "Object";
    public static final String SYSTEM_STRING_ARRAY = "Array";
    public static final String SYSTEM_STRING_WORKBOOK = "Workbook";
    public static final String SYSTEM_STRING_ANY = "any";
    public static final String SYSTEM_STRING_FILE = "File";
    public static final String SYSTEM_SYMBOL_OBJECT = "{}";
    public static final String SYSTEM_SYMBOL_ARRAY = "[]";
    public static final String SYSTEM_SYMBOL_QUERY = "?";
    public static final String SYSTEM_SYMBOL_AND = "&";
    public static final String SYSTEM_SYMBOL_EQUAL = "=";
    public static final String SYSTEM_SYMBOL_POINT = ".";
    public static final String SYSTEM_SYMBOL_PATH = "/";
    public static final String SYSTEM_SYMBOL_SEMICOLON = ";";
    public static final String SYSTEM_SYMBOL_COMMA = ",";
    public static final String SYSTEM_SYMBOL_HASH = "#";
    public static final String SYSTEM_SYMBOL_DOLLAR = "$";
    public static final String SYSTEM_SYMBOL_MIDDLE_LINE = "-";
    public static final String SYSTEM_SYMBOL_UNDERLINE = "_";
    public static final String SYSTEM_SYMBOL_COLON = ":";
    public static final String SYSTEM_SYMBOL_VERTICAL_LINE = "|";
    public static final String SYSTEM_SYMBOL_ESCAPE_VERTICAL_LINE = "\\|";
    public static final String SYSTEM_ALARM_TYPE_REST_AOP = "RestAop";
    public static final String SYSTEM_ALARM_TYPE_SLOW_SQL = "SlowSql";
    public static final String SYSTEM_ALARM_TYPE_JEDIS_SERVICE = "JedisService";
    public static final String SYSTEM_ALARM_TYPE_CLUSTER_JOB_AOP = "ClusterJobLockAop";
    public static final String SYSTEM_ALARM_TYPE_HTTP_CLIENT = "HttpClient";
    public static final String SYSTEM_ALARM_TYPE_HTTP_ASYNC_CLIENT = "HttpAsyncClient";
    public static final String SYSTEM_ALARM_TYPE_ELASTIC_CLIENT = "ElasticClient";
    public static final String SYSTEM_ALARM_TYPE_JSM_DISRUPTOR = "JsmDisruptor";
    public static final String SYSTEM_ALARM_TYPE_SLOW_API = "SlowApi";

    public static final String STRATEGY_TYPE_NONE = "none";
    public static final String STRATEGY_TYPE_MEMORY = "memory";
    public static final String STRATEGY_TYPE_CONSOLE = "console";
    public static final String STRATEGY_TYPE_ELASTIC = "elastic";
    public static final String STRATEGY_TYPE_REDIS = "redis";
    public static final String STRATEGY_TYPE_KAFKA = "kafka";
    public static final String STRATEGY_TYPE_RABBITMQ = "rabbitmq";
    public static final String STRATEGY_TYPE_DB = "db";

    public static final int STATUS_ENABLE = 1;
    public static final int STATUS_DISABLE = 2;

    public static final int BOOLEAN_YES = 1;
    public static final int BOOLEAN_NO = 0;

    public static final int LOCK_SPIN_MILLIS = 10;

    public static final String SESSION_TYPE_DEFAULT = "default";
    public static final String SESSION_TYPE_L2 = "L2";
    public static final String SESSION_TYPE_L1L2 = "L1L2";

    public static final String CONTENT_DISPLAY_INLINE = "_cd_inline";

    public static String SESSION_USERNAME_KEY = "username";

    public static final String PREFIX_HTTP = "http://";
    public static final String PREFIX_HTTPS = "https://";

    /**
     * authorization_code(授权码) implicit(隐藏式) password(密码式) client_credentials(客户端凭证)
     **/
    public static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
    public static final String GRANT_TYPE_IMPLICIT = "implicit";
    public static final String GRANT_TYPE_PASSWORD = "password";
    public static final String GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";

    public static final String APP_KEY = "appKey";
    public static final String APP_SECRETE = "appSecret";


    public static final String ES_JSON_OPTIONS_SYS_STAT = "{\"mappings\":{\"properties\":{\"time\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"clusterId\":{\"type\":\"keyword\"},\"appName\":{\"type\":\"keyword\"},\"env\":{\"type\":\"keyword\"},\"gitHash\":{\"type\":\"keyword\"},\"osName\":{\"type\":\"keyword\"},\"osVersion\":{\"type\":\"keyword\"},\"osArch\":{\"type\":\"keyword\"},\"userName\":{\"type\":\"keyword\"},\"userHome\":{\"type\":\"keyword\"},\"userDir\":{\"type\":\"keyword\"},\"userTimezone\":{\"type\":\"keyword\"},\"userLanguage\":{\"type\":\"keyword\"},\"tmpDir\":{\"type\":\"keyword\"},\"runtimeName\":{\"type\":\"keyword\"},\"runtimeVersion\":{\"type\":\"keyword\"},\"jvmName\":{\"type\":\"keyword\"},\"jvmVersion\":{\"type\":\"keyword\"},\"javaHome\":{\"type\":\"keyword\"},\"javaVersion\":{\"type\":\"keyword\"},\"fileEncoding\":{\"type\":\"keyword\"},\"catalinaHome\":{\"type\":\"keyword\"},\"totalMemory\":{\"type\":\"keyword\"},\"freeMemory\":{\"type\":\"keyword\"},\"useMemory\":{\"type\":\"keyword\"},\"cpuRatio\":{\"type\":\"keyword\"},\"threadCount\":{\"type\":\"keyword\"},\"diskInfo\":{\"type\":\"keyword\"}}}}";
    public static final String ES_JSON_OPTIONS_OPT_LOG = "{\"mappings\":{\"properties\":{\"time\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"clusterId\":{\"type\":\"keyword\"},\"appName\":{\"type\":\"keyword\"},\"env\":{\"type\":\"keyword\"},\"gitHash\":{\"type\":\"keyword\"},\"clientId\":{\"type\":\"keyword\"},\"traceId\":{\"type\":\"keyword\"},\"ip\":{\"type\":\"keyword\"},\"mac\":{\"type\":\"keyword\"},\"uid\":{\"type\":\"keyword\"},\"funcName\":{\"type\":\"keyword\"},\"optDesc\":{\"type\":\"keyword\"}}}}";
    public static final String ES_JSON_OPTIONS_LOG = "{\"mappings\":{\"properties\":{\"time\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"logName\":{\"type\":\"keyword\"},\"methodName\":{\"type\":\"keyword\"},\"clientId\":{\"type\":\"keyword\"},\"traceId\":{\"type\":\"keyword\"},\"level\":{\"type\":\"keyword\"},\"thread\":{\"type\":\"keyword\"},\"clusterId\":{\"type\":\"keyword\"},\"appName\":{\"type\":\"keyword\"},\"env\":{\"type\":\"keyword\"},\"gitHash\":{\"type\":\"keyword\"}}}}";
    public static final String ES_JSON_OPTIONS_API_STAT = "{\"mappings\":{\"properties\":{\"time\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"clusterId\":{\"type\":\"keyword\"},\"appName\":{\"type\":\"keyword\"},\"env\":{\"type\":\"keyword\"},\"gitHash\":{\"type\":\"keyword\"},\"clientId\":{\"type\":\"keyword\"},\"traceId\":{\"type\":\"keyword\"},\"respCode\":{\"type\":\"keyword\"},\"url\":{\"type\":\"keyword\"},\"method\":{\"type\":\"keyword\"},\"httpStatus\":{\"type\":\"keyword\"},\"beginTime\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"endTime\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"spendTime\":{\"type\":\"integer\"}}}}";
    public static final String ES_JSON_OPTIONS_ALARM = "{\"mappings\":{\"properties\":{\"time\":{\"type\":\"date\",\"format\":\"yyyyMMdd HH:mm:ss.SSS||epoch_millis\"},\"level\":{\"type\":\"keyword\"},\"name\":{\"type\":\"keyword\"},\"clientId\":{\"type\":\"keyword\"},\"traceId\":{\"type\":\"keyword\"},\"comments\":{\"type\":\"keyword\"},\"ip\":{\"type\":\"keyword\"},\"clusterId\":{\"type\":\"keyword\"},\"appName\":{\"type\":\"keyword\"},\"env\":{\"type\":\"keyword\"},\"gitHash\":{\"type\":\"keyword\"}}}}";
}
