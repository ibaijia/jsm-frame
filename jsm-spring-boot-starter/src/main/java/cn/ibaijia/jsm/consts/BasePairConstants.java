package cn.ibaijia.jsm.consts;

/**
 * @author longzl
 */
public class BasePairConstants {

    public static final Pair<String> OK = new Pair<String>("1001", "成功!");
    public static final Pair<String> FAIL = new Pair<String>("1002", "失败!");
    public static final Pair<String> ERROR = new Pair<String>("1003", "出错啦!");
    public static final Pair<String> NOT_FOUND = new Pair<String>("1004", "找不到资源!");
    public static final Pair<String> LICENSE_EXPIRED = new Pair<String>("1005", "授权过期!");

    public static final Pair<String> HTTP_URL_ERROR = new Pair<String>("1010", "URL调用出错!");

    public static final Pair<String> ACTIVITY_NOT_STARTED = new Pair<String>("1020", "抢购未开始！");
    public static final Pair<String> ACTIVITY_STARTED = new Pair<String>("1021", "抢购开始！");
    public static final Pair<String> ACTIVITY_OVER = new Pair<String>("1022", "抢购结束!");
    public static final Pair<String> ACTIVITY_QUEUEING = new Pair<String>("1023", "排队中!");
    public static final Pair<String> ACTIVITY_TURN = new Pair<String>("1024", "可下单!");
    public static final Pair<String> ACTIVITY_NO_STOCK = new Pair<String>("1025", "没有库存!");

    public static final Pair<String> AUTH_FAIL = new Pair<String>("1411", "鉴权失败!");
    public static final Pair<String> PARAMS_ERROR = new Pair<String>("1412", "参数错误!");
    public static final Pair<String> REPEAT_REQ_ERROR = new Pair<String>("1413", "请勿重复提交!");
    public static final Pair<String> NET_LIMIT = new Pair<String>("1414", "网络受限!");
    public static final Pair<String> IP_LIMIT = new Pair<String>("1415", "IP受限!");

    public static final Pair<String> NO_LOGIN = new Pair<String>("1416", "需要登录");

    public static final Pair<String> NO_PERMISSION = new Pair<String>("1417", "无权限");

    public static final Pair<String> AT_INVALID = new Pair<String>("1401", "AT无效");
    public static final Pair<String> RT_INVALID = new Pair<String>("1402", "RT无效");

}
