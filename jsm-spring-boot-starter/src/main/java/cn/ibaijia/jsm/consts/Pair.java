package cn.ibaijia.jsm.consts;

import java.util.Objects;

/**
 * @author longzl
 */
public class Pair<T> {
	private T code;
	private String message;
	public Pair(T code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	public T getCode() {
		return code;
	}
	public void setCode(T code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Pair [code=" + code + ", message=" + message + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Pair<?> pair = (Pair<?>) o;
		return Objects.equals(code, pair.code) &&
				Objects.equals(message, pair.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, message);
	}
}
