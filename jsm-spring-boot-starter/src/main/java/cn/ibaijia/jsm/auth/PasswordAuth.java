package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.cache.CacheService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.http.HttpClient;
import cn.ibaijia.jsm.oauth.password.OauthContext;
import cn.ibaijia.jsm.utils.JsonUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.RequestUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author longzl
 */
@Component
public class PasswordAuth implements Auth {

    private static final String ACCESS_TOKEN = "accessToken";
    private static final String PARAMS_TPL = "%s=%s&scope=%s";

    private Logger logger = LogUtil.log(PasswordAuth.class);

    private HttpClient httpClient = new HttpClient();
    @Value("${" + AppContextKey.OAUTH_PASSWORD_APP_KEY + ":''}")
    private String appKey;
    @Value("${" + AppContextKey.OAUTH_PASSWORD_APP_SECRET + ":''}")
    private String appSecret;
    @Value("${" + AppContextKey.OAUTH_PASSWORD_TOKEN_NAME + ":'accessToken'}")
    private String tokenName;
    @Value("${" + AppContextKey.OAUTH_PASSWORD_BASE_URL + ":''}")
    private String baseUrl;
    @Value("${" + AppContextKey.OAUTH_PASSWORD_VERIFY_URL + ":''}")
    private String verifyUrl;
    @Value("${" + AppContextKey.OAUTH_PASSWORD_EXPIRE_IN + ":5}")
    private Integer expireIn;
    @Autowired(required = false)
    private CacheService cacheService;
    private List<Header> headers = new ArrayList<>();

    @PostConstruct
    private void init() {
        headers.add(new BasicHeader(BaseConstants.APP_KEY, appKey));
        headers.add(new BasicHeader(BaseConstants.APP_SECRETE, appSecret));
    }


    @Override
    public String checkAuth(HttpServletRequest request, RestAnn resetAnn) {
        String token = RequestUtil.get(request, tokenName);
        if (AppContext.isDevModel() && StringUtil.isEmpty(token)) {
            token = RequestUtil.get(request, WebContext.JSM_DEBUG_TOKEN);
            if (!StringUtil.isEmpty(token)) {
                logger.warn("use dev model token:{}", WebContext.JSM_DEBUG_TOKEN);
            }
        }
        if (StringUtil.isEmpty(token)) {
            throw new AuthFailException(BasePairConstants.AT_INVALID, "缺少AT");
        }
        String permission = resetAnn.permission();
        //add cache
        boolean res = verifyOauthToken(token, permission);
        if (!res) {
            throw new AuthFailException(BasePairConstants.AT_INVALID);
        }
        return token;
    }

    /**
     * 再访问URL 验证Token
     *
     * @param accessToken
     * @param permission
     * @return
     */
    private RestResp<String> getVerifyOauthTokenResult(String accessToken, String permission) {
        try {
            if (StringUtil.isEmpty(verifyUrl)) {
                logger.error("oauth.password.verifyUrl not found, in AppContext!");
                return null;
            }
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append(baseUrl);
            urlBuilder.append(verifyUrl);
            urlBuilder.append(verifyUrl.contains(BaseConstants.SYSTEM_SYMBOL_QUERY) ? BaseConstants.SYSTEM_SYMBOL_AND : BaseConstants.SYSTEM_SYMBOL_QUERY);
            urlBuilder.append(String.format(PARAMS_TPL, tokenName, accessToken, permission));
            String res = httpClient.get(headers, urlBuilder.toString());
            logger.debug("res:{}", res);
            if (StringUtil.isEmpty(res)) {
                logger.error("get verify token info empty error.");
                return null;
            } else {
                RestResp<String> resp = JsonUtil.parseObject(res, new TypeReference<RestResp<String>>() {
                });
                return resp;
            }
        } catch (Exception e) {
            logger.error("getVerifyOauthTokenResult error:" + verifyUrl, e);
            return null;
        }
    }

    /**
     * 先取缓存 如果没有 再访问URL
     *
     * @param token
     * @param permission
     * @return
     */
    private boolean verifyOauthToken(String token, String permission) {
        try {
            String cacheKey = token + "_" + permission;
            RestResp<String> verifyTokenResp = cacheService.get(cacheKey, new TypeReference<RestResp<String>>() {
            });
            if (verifyTokenResp == null) {
                verifyTokenResp = getVerifyOauthTokenResult(token, permission);
                if (verifyTokenResp == null || !verifyTokenResp.isOk()) {
                    return false;
                }
                cacheService.set(cacheKey, expireIn, verifyTokenResp);
            }
            OauthContext.setAccessToken(token);
            OauthContext.setVerifyResult(verifyTokenResp.result);
            return verifyTokenResp.isOk();
        } catch (Exception e) {
            logger.error("verifyOauthToken error!", e);
            return false;
        }
    }

}
