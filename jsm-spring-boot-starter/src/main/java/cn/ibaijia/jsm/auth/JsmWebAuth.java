package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.*;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author longzl
 */
@Component
public class JsmWebAuth extends AbstractJsmAuth {

    private Logger logger = LogUtil.log(JsmWebAuth.class);

    @Override
    String decodeToken(HttpServletRequest request, RestAnn resetAnn) {
        String token = null;
        try {
            String at = RequestUtil.get(request, WebContext.JSM_AT);
            String ht = RequestUtil.get(request, WebContext.JSM_HT);
            logger.debug("checkSession, auth type {},at {}, ht {}", resetAnn.authType(), at, ht);
            if (!StringUtil.isEmpty(at) && !StringUtil.isEmpty(ht)) {
                String decAuth = EncryptUtil.base64Decode(at);
                if (decAuth == null || !decAuth.contains(BaseConstants.SYSTEM_SYMBOL_UNDERLINE)) {
                    logger.error("illegal auth: {}", at);
                    return null;
                }
                String[] arr = decAuth.split("_");
                token = arr[0];
                String timestamp = arr[1];
                Long cTime = Long.valueOf(timestamp);
                long sTime = DateUtil.currentTime();
                long res = Math.abs(sTime - cTime);
                int expireTime = AppContext.getAtExpireTime();
                if (res > expireTime) {
                    logger.error("sTime {} - cTime {} = {}", sTime, cTime, res);
                    return null;
                }

                String sAuth = EncryptUtil.base64Encode(token + "_" + timestamp);
                if (!sAuth.equals(at)) {
                    logger.error("sAt {} != at {}", sAuth, at);
                    return null;
                }

                String sHash = EncryptUtil.md5(token + "_" + timestamp);
                if (!sHash.equals(ht)) {
                    logger.error("sHt {} != ht {}", sHash, ht);
                    return null;
                }
            }
            if (AppContext.isDevModel() && StringUtil.isEmpty(token)) {
                token = RequestUtil.get(request, WebContext.JSM_DEBUG_TOKEN);
                if (!StringUtil.isEmpty(token)) {
                    logger.warn("use dev model token:{}", WebContext.JSM_DEBUG_TOKEN);
                }
            }
            request.setAttribute("token", token);
        } catch (Exception e) {
            logger.error("decodeWebToken error!", e);
            return null;
        }
        return token;
    }

}
