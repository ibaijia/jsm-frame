package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.exception.NoLoginException;
import cn.ibaijia.jsm.exception.NoPermissionException;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author longzl
 */
public abstract class AbstractJsmAuth implements Auth {
    private Logger logger = LogUtil.log(AbstractJsmAuth.class);

    @Override
    public String checkAuth(HttpServletRequest request, RestAnn resetAnn) {
        String token = "";
        if (!AppContext.getSessionType().equals(BaseConstants.SESSION_TYPE_DEFAULT)) {
            token = decodeToken(request, resetAnn);
        } else {
            token = (String)WebContext.getRequest().getSession().getAttribute(BaseConstants.SESSION_ID);
        }
        if (StringUtil.isEmpty(token)) {
            throw new AuthFailException();
        }
        Session session = new Session(token);
        if (session.isExpire()) {
            throw new NoLoginException();
        } else {
            session.needLive();
            request.setAttribute(BaseConstants.SESSION_ATTR_KEY, session);
        }
        //APP WEB 判断是否有系统权限
        if (!StringUtil.isEmpty(resetAnn.permission())) {
            if (!JsmConfigurer.getWebJsmSecurityService().hasPermission(resetAnn.permission())) {
                throw new NoPermissionException();
            }
        }
        return token;
    }

    /**
     * 解token的实现逻辑
     *
     * @param request
     * @param resetAnn
     * @return
     */
    abstract String decodeToken(HttpServletRequest request, RestAnn resetAnn);

}
