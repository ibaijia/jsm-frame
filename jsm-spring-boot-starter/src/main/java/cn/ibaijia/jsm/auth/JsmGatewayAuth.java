package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.AppContextKey;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.oauth.gateway.OauthContext;
import cn.ibaijia.jsm.utils.*;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author longzl
 */
@Component
public class JsmGatewayAuth implements Auth {

    private Logger logger = LogUtil.log(JsmGatewayAuth.class);

    private String tokenName;
    private String prefix;
    private String[] headers;

    private String getTokenName() {
        if (this.tokenName == null) {
            tokenName = AppContext.get(AppContextKey.OAUTH_GATEWAY_TOKEN_NAME,"jsm-gw-at");
        }
        return tokenName;
    }
    private String getPrefix() {
        if (this.prefix == null) {
            prefix = AppContext.get(AppContextKey.OAUTH_GATEWAY_PREFIX,"jsm-gw-");
        }
        return prefix;
    }

    private String[] getHeaders() {
        if (this.headers == null) {
            String headerStr = AppContext.get(AppContextKey.OAUTH_GATEWAY_HEADERS);
            if(!StringUtil.isEmpty(headerStr)){
                headers = headerStr.split(",");
            }
        }
        return headers;
    }

    @Override
    public String checkAuth(HttpServletRequest request, RestAnn resetAnn) {
        String token = RequestUtil.getHeader(request, getTokenName());
        if (StringUtil.isEmpty(token)) {
            throw new AuthFailException(BasePairConstants.AT_INVALID, "缺少AT");
        }

        OauthContext.setAccessToken(token);
        Map<String,String> prefixMap = RequestUtil.getHeadersByPrefix(request,getPrefix());
        if(getHeaders() != null){
            Map<String,String> headersMap = new HashMap<>(16);
            for(String name:getHeaders()){
                headersMap.put(name,prefixMap.get(name));
            }
            OauthContext.setVerifyResult(JsonUtil.toJsonString(headersMap));
        }else {
            OauthContext.setVerifyResult(JsonUtil.toJsonString(prefixMap));
        }
        return token;
    }

}
