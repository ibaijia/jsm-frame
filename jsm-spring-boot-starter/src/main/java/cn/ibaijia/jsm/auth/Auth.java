package cn.ibaijia.jsm.auth;

import cn.ibaijia.jsm.annotation.RestAnn;

import javax.servlet.http.HttpServletRequest;

/**
 * @author longzl
 */
public interface Auth {

    String GRANT_TYPE_CODE = "authorization_code";
    String GRANT_TYPE_IMPLICIT = "implicit";
    String GRANT_TYPE_PASSWORD = "password";
    String GRANT_TYPE_CLIENT = "client_credentials";

    /**
     * 检查授权的逻辑
     * @param request
     * @param resetAnn
     * @return token
     */
    String checkAuth(HttpServletRequest request, RestAnn resetAnn);

}
