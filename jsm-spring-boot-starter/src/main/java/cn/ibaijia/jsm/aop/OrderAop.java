package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.OrderAnn;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.consts.Pair;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.order.BaseOrderLine;
import cn.ibaijia.jsm.exception.InfoException;
import cn.ibaijia.jsm.utils.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

/**
 * 限流控制 使用在 controller 方法
 * @author longzl
 */
@Order(2)
@Aspect
@Component
public class OrderAop {
    private static Logger logger = LogUtil.log(OrderAop.class);

    private static Map<String, Semaphore> map = new ConcurrentHashMap<>();

    @Around("@annotation(orderAnn)")
    public Object intercept(ProceedingJoinPoint jpt, OrderAnn orderAnn) throws Throwable {
        logger.debug("RestLimitAop intercept");
        Pair<String> resPair = orderCheck(jpt, orderAnn);
        if (!resPair.equals(BasePairConstants.ACTIVITY_TURN)) {
            throw new InfoException(resPair);
        }else{
            return jpt.proceed();
        }

    }

    private Pair<String> orderCheck(ProceedingJoinPoint jpt, OrderAnn orderAnn) {
        String key = orderAnn.productKey();
        if (!StringUtil.isEmpty(key)) {
            key = TemplateUtil.formatWithContextVar(key);
        }
        key = String.format("%s:%s",orderAnn.type().v(),key);
        key = JsmFrameUtil.getOrderCheckKey(key, jpt);
        BaseOrderLine orderLine = SpringContext.getBean(orderAnn.type().t());
        return orderLine.order(key, orderAnn);
    }

}
