package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.RestFuseAnn;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.utils.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 熔断控制 使用在 controller 方法
 *
 * @author longzl
 */
@Order(4)
@Aspect
@Component
public class RestFuseAop {
    private static Logger logger = LogUtil.log(RestFuseAop.class);

    private Object lock = new Object();

    @Autowired(required = false)
    private JedisService jedisService;

    private boolean halfOpen = false;

    @Around("@annotation(restFuseAnn)")
    public Object intercept(ProceedingJoinPoint jpt, RestFuseAnn restFuseAnn) throws Throwable {
        logger.debug("RestFuseAop intercept");
        String key = restFuseAnn.key();
        if (!StringUtil.isEmpty(key)) {
            key = TemplateUtil.formatWithContextVar(key);
        }
        key = JsmFrameUtil.getFuseKey(key,jpt);
        try {
            logger.debug("fuse key: {}", key);
            if (jedisService.exists(key)) {
                halfOpen = true;
                return ReflectionUtil.invokeMethod(jpt, restFuseAnn.fallbackMethod());
            } else {
                Object result = jpt.proceed();
                halfOpen = false;
                return result;
            }
        } catch (Exception e) {
            String countKey = JsmFrameUtil.getFuseCountKey(restFuseAnn.key(),jpt);
            synchronized (lock){
                String countStr = jedisService.get(countKey);
                if(StringUtil.isEmpty(countStr)){
                    jedisService.setex(countKey, restFuseAnn.fuseDuration(), "1");
                } else {
                    Integer count = StringUtil.toInteger(countStr, 0);
                    if (halfOpen || (count + 1 >= restFuseAnn.value())) {
                        jedisService.setex(key, restFuseAnn.fuseDuration(), "1");
                    } else {
                        jedisService.exec(jedis -> {
                            jedis.incr(countKey);
                            return null;
                        });
                    }
                }
            }
            return ReflectionUtil.invokeMethod(jpt, restFuseAnn.fallbackMethod());
        }
    }

}
