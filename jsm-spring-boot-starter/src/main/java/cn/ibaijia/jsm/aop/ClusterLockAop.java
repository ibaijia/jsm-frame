package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.ClusterLockAnn;
import cn.ibaijia.jsm.context.lock.LockService;
import cn.ibaijia.jsm.utils.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 分布式锁 使用在 service方法
 * @author longzl
 */
@Order(2)
@Aspect
@Component
public class ClusterLockAop {
    private static Logger logger = LogUtil.log(ClusterLockAop.class);

    @Autowired
    private LockService lockService;

    @Around("@annotation(clusterLockAnn)")
    public Object intercept(ProceedingJoinPoint jpt, ClusterLockAnn clusterLockAnn) throws Throwable {
        Object result = null;
        if (TransactionUtil.getTransactionStatus() != null) {
            logger.warn("ClusterLockAnn not recommend run in transaction. it's may be make db lock.");
        }
        String key = clusterLockAnn.value();
        if (!StringUtil.isEmpty(key)) {
            key = TemplateUtil.formatWithContextVar(key);
        }
        key = JsmFrameUtil.getLockKey(key,jpt);
        try {
            logger.debug("clusterLockAnn lock:{}", key);
            boolean locked = lockService.lock(key,clusterLockAnn.timeout());
            if (!locked) {
                logger.error("clusterLockAnn lock:{} failed, return.", key);
                return null;
            }
            result = jpt.proceed();
        } catch (Exception e) {
            logger.error("clusterLockAnn error,lock key:" + key);
            throw e;
        } finally {
            lockService.unlock(key);
            logger.debug("clusterLockAnn unlock:{}", key);
        }
        return result;
    }

}
