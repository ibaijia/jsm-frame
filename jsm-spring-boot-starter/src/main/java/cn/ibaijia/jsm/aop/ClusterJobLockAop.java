package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.ClusterJobLockAnn;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.lock.LockService;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.JsmFrameUtil;
import cn.ibaijia.jsm.utils.LogUtil;
import cn.ibaijia.jsm.utils.SystemUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 分布式锁 使用在 job方法
 *
 * @author longzl
 */
@Order(1)
@Aspect
@Component
public class ClusterJobLockAop {
    private static Logger logger = LogUtil.log(ClusterJobLockAop.class);

    private AtomicInteger traceIdAi = new AtomicInteger(0);
    private static final int REMAIN_SIZE = 100;

    @Autowired
    private LockService lockService;

    @Around("@annotation(clusterJobLockAnn)")
    public Object intercept(ProceedingJoinPoint jpt, ClusterJobLockAnn clusterJobLockAnn) throws Throwable {
        Object result = null;
        String key = JsmFrameUtil.getJobLockKey(clusterJobLockAnn.value(),jpt);
        boolean locked = false;
        try {
            createTraceId();
            logger.debug("clusterJobLockAnn lock:{}", key);
            locked = lockService.tryLock(key);
            if (!locked) {
                logger.warn("clusterJobLockAnn lock:{} failed, return.", key);
                return null;
            }
            result = jpt.proceed();
        } catch (Exception e) {
            logger.error("clusterJobLockAnn error,clusterJobLockAnn key:" + key, e);
            SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_CLUSTER_JOB_AOP, key + ":Exception," + e.getMessage()));
        } finally {
            WebContext.clearTraceId();
            if (locked) {
                lockService.unlock(key);
            }
        }
        return result;
    }

    private void createTraceId() {
        int intCount = traceIdAi.getAndIncrement();
        if (intCount > Integer.MAX_VALUE - REMAIN_SIZE) {
            traceIdAi.set(0);
        }
        WebContext.setTraceId(String.format("job-%s-%s", AppContext.getClusterId(), intCount));
    }

}
