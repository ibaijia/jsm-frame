package cn.ibaijia.jsm.aop;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.auth.Auth;
import cn.ibaijia.jsm.cache.CaffeineCacheL1;
import cn.ibaijia.jsm.cache.jedis.JedisService;
import cn.ibaijia.jsm.consts.BaseConstants;
import cn.ibaijia.jsm.consts.BasePairConstants;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.JsmConfigurer;
import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.context.dao.model.OptLog;
import cn.ibaijia.jsm.context.lock.LockService;
import cn.ibaijia.jsm.context.rest.resp.OutputVo;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import cn.ibaijia.jsm.context.rest.resp.vo.*;
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.context.session.Session;
import cn.ibaijia.jsm.context.session.SessionUser;
import cn.ibaijia.jsm.exception.AlarmException;
import cn.ibaijia.jsm.exception.AuthFailException;
import cn.ibaijia.jsm.exception.BaseException;
import cn.ibaijia.jsm.exception.FailedException;
import cn.ibaijia.jsm.license.LicenseService;
import cn.ibaijia.jsm.mybatis.datasource.DynamicDataSourceHolder;
import cn.ibaijia.jsm.stat.JsmOptLogService;
import cn.ibaijia.jsm.stat.model.Alarm;
import cn.ibaijia.jsm.utils.*;
import org.apache.http.HttpStatus;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.lang.reflect.Method;

/**
 * 使用在 controller 方法
 *
 * @author longzl
 */
@Order(3)
@Aspect
@Component
public class RestAop {
    private static Logger logger = LogUtil.log(RestAop.class);
    private PlatformTransactionManager transactionManager;

    @Autowired(required = false)
    private LicenseService licenseService;

    @Autowired(required = false)
    private JedisService jedisService;

    @Autowired(required = false)
    private LockService lockService;

    @Autowired(required = false)
    private CaffeineCacheL1 repeatCache;

    @Around("@annotation(restAnn)")
    public Object intercept(ProceedingJoinPoint jpt, RestAnn restAnn) throws Throwable {
        logger.debug("RestAop intercept");
        Object result = null;
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        boolean useMock = false;
        try {
            if (!AppContext.isDevModel() && licenseService != null) {
                if (!licenseService.check()) {
                    throw new AuthFailException(BasePairConstants.LICENSE_EXPIRED);
                }
            }
            //判断是否mock data
            String jsmMock = RequestUtil.getParameter(WebContext.getRequest(), WebContext.JSM_MOCK);
            if (AppContext.isDevModel() && !StringUtil.isEmpty(jsmMock)) {
                useMock = true;
                validateParams(jpt, restAnn);
                String res = JsmFrameUtil.genMockResult(method);
                logger.warn("mock resp:{}", res);
                ResponseUtil.outputJson(WebContext.getResponse(), new JsonVo(res));
                return null;
            }
            //验证内网
            if (restAnn.internal() && !IpUtil.isInternalIp(WebContext.getFirstForwardIp())) {
                throw new AuthFailException(BasePairConstants.NET_LIMIT);
            }
            //验证IP
            if (!StringUtil.isEmpty(restAnn.ipCheckKey()) && !IpUtil.checkRemoteIp(WebContext.getFirstForwardIp(), restAnn.ipCheckKey())) {
                throw new AuthFailException(BasePairConstants.IP_LIMIT);
            }
            // 设置自定义慢API配置
            if (restAnn.apiLimit() != -1) {
                ThreadLocalUtil.SLOW_API_LIMIT_TL.set(restAnn.apiLimit());
            }
            //验证登录
            String authId = validateAuth(jpt, restAnn);
            //参数验证
            validateParams(jpt, restAnn);
            //验证重复提交
            repeatCheck(jpt, restAnn, authId);

            if (restAnn.sync()) {
                synchronized (method) {
                    result = process(jpt, restAnn);
                }
            } else {
                result = process(jpt, restAnn);
            }
        } catch (Exception e) {
            result = JsmFrameUtil.dealException(e);
            OptLogUtil.setLogStatus(e.getMessage());
        } finally {
            //记录日志
            if (!useMock) {
                optLogRecord(restAnn, jpt);
            }
            // 输出结果
            boolean outputOk = outputResult(result);
            if (outputOk) {
                result = null;
            }
        }
        return result;
    }

    private String validateAuth(ProceedingJoinPoint jpt, RestAnn resetAnn) {
        String authId = null;
        if (resetAnn.authType() != AuthType.NONE) {
            Auth auth = SpringContext.getBean(resetAnn.authType().t());
            if (auth == null) {
                logger.error("authBean not found.{}", resetAnn.authType().t());
            } else {
                logger.debug("authBean:{}", resetAnn.authType().t());
                authId = auth.checkAuth(WebContext.getRequest(), resetAnn);
            }
        }
        return authId;
    }

    private boolean outputResult(Object result) {
        boolean output = false;
        if (result instanceof RestResp) {
            WebContext.getRequest().setAttribute(WebContext.JSM_RESP_CODE, ((RestResp) result).code);
            if (WebContext.getResponse().getStatus() == HttpStatus.SC_OK) {
                WebContext.getResponse().setStatus(JsmConfigurer.getRestStatusStrategy().httpStatusCode((RestResp) result));
            }
            logger.info("resp:" + JsonUtil.toJsonString(result));
            RestResp resp = (RestResp) result;
            if (resp.result instanceof OutputVo) {
                output = true;
                try {
                    if (resp.result instanceof FileVo) {
                        ResponseUtil.outputFile(WebContext.getResponse(), (FileVo) resp.result);
                    } else if (resp.result instanceof ExcelVo) {
                        ResponseUtil.outputExcel(WebContext.getResponse(), (ExcelVo) resp.result);
                    } else if (resp.result instanceof TextVo) {
                        ResponseUtil.outputText(WebContext.getResponse(), (TextVo) resp.result);
                    } else if (resp.result instanceof JsonVo) {
                        ResponseUtil.outputJson(WebContext.getResponse(), (JsonVo) resp.result);
                    } else if (resp.result instanceof XmlVo) {
                        ResponseUtil.outputXml(WebContext.getResponse(), (XmlVo) resp.result);
                    } else if (resp.result instanceof HtmlVo) {
                        ResponseUtil.outputHtml(WebContext.getResponse(), (HtmlVo) resp.result);
                    } else if (resp.result instanceof FreeVo) {
                        ResponseUtil.outputFreeResp(WebContext.getResponse(), (FreeVo) resp.result);
                    } else if (resp.result instanceof RedirectVo) {
                        WebContext.getResponse().sendRedirect(((RedirectVo) resp.result).uri);
                    } else if (resp.result instanceof FollowVo) {
                        ServletRequest request = WebContext.getRequest();
                        ServletResponse response = WebContext.getResponse();
                        request.getRequestDispatcher(((FollowVo) resp.result).uri).forward(request, response);
                    }
                } catch (Exception e) {
                    logger.error("outputResult error.", e);
                }
            }
        } else {
            logger.warn("not except resp:" + JsonUtil.toJsonString(result));
        }
        return output;
    }

    private void repeatCheck(ProceedingJoinPoint jpt, RestAnn resetAnn, String authId) {
        if (WebContext.isRequestMethod(HttpMethod.POST.name()) || WebContext.isRequestMethod(HttpMethod.PUT.name())) {
            if (resetAnn.repeatCheck()) {
                boolean isRepeat = isRepeatRequest(jpt, authId);
                if (isRepeat) {
                    throw new FailedException(BasePairConstants.REPEAT_REQ_ERROR);
                }
            }
        }
    }

    private void validateParams(ProceedingJoinPoint jpt, RestAnn resetAnn) {
        if (resetAnn.validate()) {
            String message = validate(jpt);
            if (!StringUtil.isEmpty(message)) {
                throw new FailedException(BasePairConstants.PARAMS_ERROR, message);
            }
        }
    }

    private void optLogRecord(RestAnn resetAnn, ProceedingJoinPoint jpt) {
        if (!StringUtil.isEmpty(resetAnn.log())) {
            OptLog optLog = new OptLog();
            optLog.clusterId = AppContext.getClusterId();
            optLog.traceId = WebContext.getTraceId();
            optLog.ip = WebContext.getRemoteIp();
            Session session = WebContext.currentSession();
            optLog.mac = session != null ? (String) session.get("mac") : "";
            optLog.funcName = resetAnn.logType();

            SessionUser sessionUser = WebContext.currentUser();
            if (sessionUser != null) {
                OptLogUtil.setLogUserIfNot((String) WebContext.currentSession().get(BaseConstants.SESSION_USERNAME_KEY));
                optLog.uid = sessionUser.getId();
            }
            if (!OptLogUtil.checkLogContent()) {
                for (Object arg : jpt.getArgs()) {
                    if (arg == null) {
                        continue;
                    }
                    if (arg instanceof ValidateModel) {
                        OptLogUtil.setLogContentIfNot(jpt.getArgs());
                        break;
                    }
                }
            }
            optLog.optDesc = TemplateUtil.formatWithContextVar(resetAnn.log());
            optLog.time = DateUtil.currentTime();
            JsmOptLogService logService = SpringContext.getBean(JsmOptLogService.class);
            logService.add(optLog);
        }
    }

    private boolean isRepeatRequest(ProceedingJoinPoint jpt, String authId) {
        Method method = ((MethodSignature) (jpt.getSignature())).getMethod();
        Object[] args = jpt.getArgs();
        if (args != null && args.length > 0) {
            for (Object arg : args) {
                if (arg == null) {
                    continue;
                }
                if (arg instanceof ValidateModel) {
                    String reqMd5 = EncryptUtil.md5(JsonUtil.toJsonString(arg));
                    String key = "jsm:reqHash:" + method.getName() + ":" + reqMd5 + ":" + authId;
                    if (jedisService != null) {
                        String res = jedisService.setnx(key, 5, "1");
                        return !"OK".equals(res);
                    } else {
                        return repeatCache.exists(key);
                    }
                }
            }
        }
        return false;
    }

    private Object process(ProceedingJoinPoint jpt, RestAnn resetAnn) throws Throwable {
        Object result = null;
        //事务处理
        TransactionStatus status = null;
        String clusterSyncLock = resetAnn.clusterSyncLock();
        try {
            clusterSyncLock = TemplateUtil.formatWithContextVar(clusterSyncLock);
            if (!StringUtil.isEmpty(clusterSyncLock)) {
                lockService.lock(clusterSyncLock);
            }
            DynamicDataSourceHolder.setDataSource(false);
            if (!resetAnn.transaction().equals(Transaction.NONE)) {
                DynamicDataSourceHolder.setDataSource(resetAnn.transaction().equals(Transaction.READ));
                this.transactionManager = SpringContext.getBean(resetAnn.transManagerName());
                logger.debug("getTransaction begin.");
                status = this.transactionManager.getTransaction(createTransactionDefinition(resetAnn));
                logger.debug("getTransaction end.");
                TransactionUtil.setTransactionStatus(status);
            }
            result = jpt.proceed();

            if (!StringUtil.isEmpty(resetAnn.log())) {
                OptLogUtil.setLogStatusIfNot("成功");
            }
        } catch (Exception e) {
            if (status != null) {
                status.setRollbackOnly();
            }
            if (!StringUtil.isEmpty(resetAnn.log())) {
                OptLogUtil.setLogStatusIfNot("失败");
            }
            if (e instanceof AlarmException) {
                SystemUtil.addAlarm(new Alarm(((AlarmException) e).getName(), ((BaseException) e).getMsg()));
            } else if (e instanceof BaseException) {
                SystemUtil.addAlarm(new Alarm(e.getClass().getSimpleName(), ((BaseException) e).getMsg()));
            } else {
                SystemUtil.addAlarm(new Alarm(BaseConstants.SYSTEM_ALARM_TYPE_REST_AOP, "Exception," + e.getMessage()));
            }
            throw e;
        } finally {
            if (status != null && !status.isCompleted()) {
                if (status.isRollbackOnly()) {
                    logger.debug("transaction rollback!");
                    this.transactionManager.rollback(status);
                } else {
                    this.transactionManager.commit(status);
                }
            }
            TransactionUtil.removeTransactionStatus();
            if (!StringUtil.isEmpty(clusterSyncLock)) {
                lockService.unlock(clusterSyncLock);
            }
        }
        return result;
    }

    private String validate(ProceedingJoinPoint jpt) {
        String message = null;
        Object[] args = jpt.getArgs();
        if (args != null && args.length > 0) {
            for (Object arg : args) {
                if (arg == null) {
                    continue;
                }
                if (arg instanceof ValidateModel) {
                    logger.info("request arg:{}", JsonUtil.toJsonString(arg));
                    message = ValidateUtil.validate(arg);
                    if (!StringUtil.isEmpty(message)) {
                        break;
                    }
                } else {
                    logger.info("request arg type:{}", arg.getClass().getName());
                }
            }
        }
        return message;
    }

    private DefaultTransactionDefinition createTransactionDefinition(RestAnn restAnn) {
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setPropagationBehavior(restAnn.transPropagationBehavior());
        transactionDefinition.setIsolationLevel(restAnn.transIsolationLevel());
        transactionDefinition.setReadOnly(restAnn.transaction().equals(Transaction.READ));
        transactionDefinition.setTimeout(restAnn.transTimeout());
        return transactionDefinition;
    }

}
