<#setting number_format="#"/>
import ${req.ajaxLibName} from "${req.ajaxLibPath}"
import * as model from "${req.apiModelDir?starts_with('.')?string('','./')}${req.apiModelDir}/${req.apiName}Model"
const baseUrl = ${req.baseUri?contains('.')?string('','"')}${req.baseUri}${req.baseUri?contains('.')?string('','"')}

const parseQueryString = (params: any) => {
    let queryString = ''
    Object.keys(params).forEach((key) => {
        if (params[key] != undefined && params[key] != null) {
            queryString += (queryString ? "&" : "?")
            queryString += ${r'`${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`'}
        }
    })
    return queryString
}

<#list controllerInfo.apiInfos as apiInfo>
/**
 * @description ${apiInfo.comments}
 <#if apiInfo.pathVarList??>
 * @Path参数
 <#list apiInfo.pathVarList as field>
 * @param ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 </#list>
 </#if>
 <#if apiInfo.reqParamList??>
 * @请求参数 ${req.apiName}Model.${apiInfo.tsReqParamType?default("")}
 <#list apiInfo.reqParamList as field>
 * @param ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 </#list>
 </#if>
 <#if apiInfo.bodyParamList??>
 * @请求Body
 <#list apiInfo.bodyParamList as field>
 * @param ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 <#if field.fieldInfoList??>
 <#list field.fieldInfoList as field>
 * @param  ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 <#if field.fieldInfoList??>
 <#list field.fieldInfoList as field>
 * @param   ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 <#if field.fieldInfoList??>
 <#list field.fieldInfoList as field>
 * @param    ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 </#list>
 </#if>
 </#list>
 </#if>
 </#list>
 </#if>
 </#list>
 </#if>
 <#if apiInfo.respParamList??>
 * @响应Body
 <#list apiInfo.respParamList as field>
 * @param ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 <#if field.fieldInfoList??>
 <#list field.fieldInfoList as field>
 * @param  ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 <#if field.fieldInfoList??>
 <#list field.fieldInfoList as field>
 * @param   ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 <#if field.fieldInfoList??>
 <#list field.fieldInfoList as field>
 * @param    ${field.fieldName},${field.fieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 </#list>
 </#if>
 </#list>
 </#if>
 </#list>
 </#if>
 </#list>
 </#if>
 * @return ${req.apiName}Model.${apiInfo.tsRespBodyType?default("")}
 */
export const ${apiInfo.name} = (${apiInfo.tsReqVar?default("")}) => {
    return ${req.ajaxLibName}.${apiInfo.httpMethod}(`${r'${baseUrl}'}${apiInfo.frontUrl}`${apiInfo.frontReqVarName?default("")})
}
</#list>

export default {
<#list controllerInfo.apiInfos as apiInfo>
    ${apiInfo.name},
</#list>
}