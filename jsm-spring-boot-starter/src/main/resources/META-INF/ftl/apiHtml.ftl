<#setting number_format="#"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>API 详情</title>
    <style>
        .an {
            margin-top: 16px;
            display: block;
            background: #F5FAFA;
            font-weight: bold;
        }

        table {
            border-collapse: collapse;
            margin-top: 6px;
        }

        table td, table th {
            border: 1px solid #cad9ea;
            color: #666;
            height: 30px;
        }

        table th {
            background-color: #CCE8EB;
            min-width: 100px;
        }

        table tr:nth-child(odd) {
            background: #fff;
        }

        table tr:nth-child(even) {
            background: #F5FAFA;
        }

        #app a {
            display: block;
        }

        .tags > a {
            color: blue;
            cursor: pointer;
        }

        .tags > a:link {
            color: #06F;
            text-decoration: none;
        }

        .tags > a:visited {
            color: #999;
            text-decoration: line-through;
        }

        .tags > a:hover {
            color: #F00;
            text-decoration: underline;
        }

        .tags > a:active {
            color: #F0F;
        }

        .l1 {
            padding-left: 16px;
        }

        .l2 {
            padding-left: 28px;
        }

        .l3 {
            padding-left: 48px;
        }

        .l4 {
            padding-left: 68px;
        }

        .l5 {
            padding-left: 88px;
        }

        .l6 {
            padding-left: 108px;
        }
    </style>

</head>
<body>
<div id="app">
    <div class="opts">
        <button @click="exportTask()">导出任务</button>
    </div>
    <div class="tags">
        <#list controllerInfoList as controllerInfo>
            <#if controllerInfo.apiInfos??>
                <#list controllerInfo.apiInfos as apiInfo>
                    <a href="${r'#'}${controllerInfo.name}${apiInfo.name}Using${apiInfo.httpMethod?upper_case}">${apiInfo.httpMethod?upper_case} ${apiInfo.url}</a>
                </#list>
            </#if>
        </#list>
    </div>
    <#list controllerInfoList as controllerInfo>
        <#if controllerInfo.apiInfos??>
            <#list controllerInfo.apiInfos as apiInfo>
                <a class="an"
                   name="${controllerInfo.name}${apiInfo.name}Using${apiInfo.httpMethod?upper_case}">${apiInfo.httpMethod?upper_case} ${apiInfo.url} ${apiInfo.comments}</a>
                <#if apiInfo.pathVarList??>
                    <table>
                        <tr>
                            <th @click="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].pathVarListShow = !controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].pathVarListShow">
            <span>
                {{controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].pathVarListShow ? '-' : '+'}}
            </span>
                                请求Path
                            </th>
                            <th>必填</th>
                            <th>类型</th>
                            <th>长度</th>
                            <th>字段说明</th>
                        </tr>
                        <template
                                v-if="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].pathVarListShow">
                            <tr v-for="param in controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].pathVarList">
                                <td>{{param.fieldName}}</td>
                                <td>{{param.required ? '是' : '否'}}</td>
                                <td>{{param.fieldType}}</td>
                                <td>{{param.maxLength}}</td>
                                <td>{{param.comments}}</td>
                            </tr>
                        </template>
                    </table>
                </#if>
                <#if apiInfo.reqParamList??>
                    <table>
                        <tr>
                            <th @click="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].reqParamListShow = !controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].reqParamListShow">
            <span>
                {{controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].reqParamListShow ? '-' : '+'}}
            </span>
                                请求Param
                            </th>
                            <th>必填</th>
                            <th>类型</th>
                            <th>长度</th>
                            <th>字段说明</th>
                        </tr>
                        <template
                                v-if="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].reqParamListShow">
                            <tr v-for="param in controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].reqParamList">
                                <td>{{param.fieldName}}</td>
                                <td>{{param.required ? '是' : '否'}}</td>
                                <td>{{param.fieldType}}</td>
                                <td>{{param.maxLength}}</td>
                                <td>{{param.comments}}</td>
                            </tr>
                        </template>
                    </table>
                </#if>
                <#if apiInfo.bodyParamList??>
                    <table>
                        <tr>
                            <th @click="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].bodyParamListShow = !controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].bodyParamListShow">
           <span>
                {{controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].bodyParamListShow ? '-' : '+'}}
           </span>
                                请求Body
                            </th>
                            <th>必填</th>
                            <th>类型</th>
                            <th>长度</th>
                            <th>字段说明</th>
                        </tr>
                        <template
                                v-if="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].bodyParamListShow">
                            <template
                                    v-for="param in controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].bodyParamList">
                                <tr>
                                    <td>
                                        <template v-if="param.fieldInfoList != null && param.fieldInfoList.length > 0">
                                            <span @click="param.show=!param.show">{{param.show ? '-' : '+'}}</span>
                                        </template>
                                        <template v-else>
                                            &nbsp;
                                        </template>
                                        {{param.fieldName}}
                                    </td>
                                    <td>{{param.required ? '是' : '否'}}</td>
                                    <td>{{param.fieldType}}</td>
                                    <td>{{param.maxLength}}</td>
                                    <td>{{param.comments}}</td>
                                </tr>
                                <template v-if="param.show" v-for="param1 in param.fieldInfoList">
                                    <tr>
                                        <td class="l1">
                                            <template
                                                    v-if="param1.fieldInfoList != null && param1.fieldInfoList.length > 0">
                                                <span @click="param1.show=!param1.show">{{param1.show ? '-' : '+'}}</span>
                                            </template>
                                            <template v-else>
                                                &nbsp;
                                            </template>
                                            {{param1.fieldName}}
                                        </td>
                                        <td>{{param1.required ? '是' : '否'}}</td>
                                        <td>{{param1.fieldType}}</td>
                                        <td>{{param1.maxLength}}</td>
                                        <td>{{param1.comments}}</td>
                                    </tr>
                                    <template v-if="param1.show" v-for="param2 in param1.fieldInfoList">
                                        <tr>
                                            <td class="l2">
                                                <template
                                                        v-if="param2.fieldInfoList != null && param2.fieldInfoList.length > 0">
                                                    <span @click="param2.show=!param2.show">{{param2.show ? '-' : '+'}}</span>
                                                </template>
                                                <template v-else>
                                                    &nbsp;
                                                </template>
                                                {{param2.fieldName}}
                                            </td>
                                            <td>{{param2.required ? '是' : '否'}}</td>
                                            <td>{{param2.fieldType}}</td>
                                            <td>{{param2.maxLength}}</td>
                                            <td>{{param2.comments}}</td>
                                        </tr>
                                        <template v-if="param2.show" v-for="param3 in param2.fieldInfoList">
                                            <tr>
                                                <td class="l3">
                                                    &nbsp;{{param3.fieldName}}
                                                </td>
                                                <td>{{param3.required ? '是' : '否'}}</td>
                                                <td>{{param3.fieldType}}</td>
                                                <td>{{param3.maxLength}}</td>
                                                <td>{{param3.comments}}</td>
                                            </tr>
                                            <template v-if="param3.show" v-for="param4 in param3.fieldInfoList">
                                                <tr>
                                                    <td class="l4">
                                                        &nbsp;{{param4.fieldName}}
                                                    </td>
                                                    <td>{{param4.required ? '是' : '否'}}</td>
                                                    <td>{{param4.fieldType}}</td>
                                                    <td>{{param4.maxLength}}</td>
                                                    <td>{{param4.comments}}</td>
                                                </tr>
                                                <template v-if="param4.show" v-for="param5 in param4.fieldInfoList">
                                                    <tr>
                                                        <td class="l5">
                                                            &nbsp;{{param5.fieldName}}
                                                        </td>
                                                        <td>{{param5.required ? '是' : '否'}}</td>
                                                        <td>{{param5.fieldType}}</td>
                                                        <td>{{param5.maxLength}}</td>
                                                        <td>{{param5.comments}}</td>
                                                    </tr>
                                                    <template v-if="param5.show" v-for="param6 in param5.fieldInfoList">
                                                        <tr>
                                                            <td class="l6">
                                                                &nbsp;{{param6.fieldName}}
                                                            </td>
                                                            <td>{{param6.required ? '是' : '否'}}</td>
                                                            <td>{{param6.fieldType}}</td>
                                                            <td>{{param6.maxLength}}</td>
                                                            <td>{{param6.comments}}</td>
                                                        </tr>
                                                    </template>
                                                </template>
                                            </template>
                                        </template>
                                    </template>
                                </template>
                            </template>
                        </template>
                    </table>
                </#if>

                <#if apiInfo.respParamList??>
                    <table>
                        <tr>
                            <th @click="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].respParamListShow = !controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].respParamListShow  ">
              <span>
                {{controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].respParamListShow ? '-' : '+'}}
           </span>
                                响应
                            </th>
                            <th>必填</th>
                            <th>类型</th>
                            <th>长度</th>
                            <th>字段说明</th>
                        </tr>
                        <template
                                v-if="controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].respParamListShow">
                            <template
                                    v-for="param in controllerInfoList[${controllerInfo_index}].apiInfos[${apiInfo_index}].respParamList">
                                <tr>
                                    <td>
                                        <template v-if="param.fieldInfoList != null && param.fieldInfoList.length > 0">
                                            <span @click="param.show=!param.show">{{param.show ? '-' : '+'}}</span>
                                        </template>
                                        <template v-else>
                                            &nbsp;
                                        </template>
                                        {{param.fieldName}}
                                    </td>
                                    <td>{{param.required ? '是' : '否'}}</td>
                                    <td>{{param.fieldType}}</td>
                                    <td>{{param.maxLength}}</td>
                                    <td>{{param.comments}}</td>
                                </tr>
                                <template v-if="param.show" v-for="param1 in param.fieldInfoList">
                                    <tr>
                                        <td class="l1">
                                            <template
                                                    v-if="param1.fieldInfoList != null && param1.fieldInfoList.length > 0">
                                                <span @click="param1.show=!param1.show">{{param1.show ? '-' : '+'}}</span>
                                            </template>
                                            <template v-else>
                                                &nbsp;
                                            </template>
                                            {{param1.fieldName}}
                                        </td>
                                        <td>{{param1.required ? '是' : '否'}}</td>
                                        <td>{{param1.fieldType}}</td>
                                        <td>{{param1.maxLength}}</td>
                                        <td>{{param1.comments}}</td>
                                    </tr>
                                    <template v-if="param1.show" v-for="param2 in param1.fieldInfoList">
                                        <tr>
                                            <td class="l2">
                                                <template
                                                        v-if="param2.fieldInfoList != null && param2.fieldInfoList.length > 0">
                                                    <span @click="param2.show=!param2.show">{{param2.show ? '-' : '+'}}</span>
                                                </template>
                                                <template v-else>
                                                    &nbsp;
                                                </template>
                                                {{param2.fieldName}}
                                            </td>
                                            <td>{{param2.required ? '是' : '否'}}</td>
                                            <td>{{param2.fieldType}}</td>
                                            <td>{{param2.maxLength}}</td>
                                            <td>{{param2.comments}}</td>
                                        </tr>
                                        <template v-if="param2.show != null" v-for="param3 in param2.fieldInfoList">
                                            <tr>
                                                <td class="l3">
                                                    &nbsp;{{param3.fieldName}}
                                                </td>
                                                <td>{{param3.required ? '是' : '否'}}</td>
                                                <td>{{param3.fieldType}}</td>
                                                <td>{{param3.maxLength}}</td>
                                                <td>{{param3.comments}}</td>
                                            </tr>

                                            <template v-if="param3.show != null" v-for="param4 in param3.fieldInfoList">
                                                <tr>
                                                    <td class="l4">
                                                        &nbsp;{{param4.fieldName}}
                                                    </td>
                                                    <td>{{param4.required ? '是' : '否'}}</td>
                                                    <td>{{param4.fieldType}}</td>
                                                    <td>{{param4.maxLength}}</td>
                                                    <td>{{param4.comments}}</td>
                                                </tr>

                                                <template v-if="param4.show != null"
                                                          v-for="param5 in param4.fieldInfoList">
                                                    <tr>
                                                        <td class="l5">
                                                            &nbsp;{{param5.fieldName}}
                                                        </td>
                                                        <td>{{param5.required ? '是' : '否'}}</td>
                                                        <td>{{param5.fieldType}}</td>
                                                        <td>{{param5.maxLength}}</td>
                                                        <td>{{param5.comments}}</td>
                                                    </tr>

                                                    <template v-if="param5.show != null"
                                                              v-for="param6 in param5.fieldInfoList">
                                                        <tr>
                                                            <td class="l6">
                                                                &nbsp;{{param6.fieldName}}
                                                            </td>
                                                            <td>{{param6.required ? '是' : '否'}}</td>
                                                            <td>{{param6.fieldType}}</td>
                                                            <td>{{param6.maxLength}}</td>
                                                            <td>{{param6.comments}}</td>
                                                        </tr>
                                                    </template>
                                                </template>
                                            </template>
                                        </template>
                                    </template>
                                </template>
                            </template>
                        </template>
                    </table>
                </#if>
            </#list>
        </#if>
    </#list>
</div>
</body>
<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
<script type="text/javascript">
    var app = new Vue({
        el: "#app",
        data: {
            controllerInfoList: ${controllerInfoJson}
        },
        methods: {
            exportTask() {
                let str = `模块,Method&API,描述\n`;
                for (let i = 0; i < this.controllerInfoList.length; i++) {
                    let controllerInfo = this.controllerInfoList[i];
                    if (controllerInfo.apiInfos) {
                        let apiInfos = controllerInfo.apiInfos;
                        for (let j = 0; j < apiInfos.length; j++) {
                            let modelName = apiInfos[j].comments.split('-')[0];
                            let url = apiInfos[j].httpMethod.toUpperCase() + " " + apiInfos[j].url;
                            let comments = apiInfos[j].comments;
                            str = str + (modelName + ',' + url + ',' + comments + '\n');
                        }
                    }
                }
                const uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(str);
                const link = document.createElement("a");
                link.href = uri;
                link.download = "任务.csv";
                link.click();
            }
        }
    });
</script>
</html>