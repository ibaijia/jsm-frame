<#setting number_format="#"/>
package ${constPackageName};

import cn.ibaijia.jsm.consts.BasePermissionConstants;

/**
* @author jsm_auto_gen
*/
public class Permissions extends BasePermissionConstants {

<#list permissionList as item>
    /**
    * ${item.name?default("")}
    */
    public static final String ${item.code} = "${item.code}";
</#list>

}