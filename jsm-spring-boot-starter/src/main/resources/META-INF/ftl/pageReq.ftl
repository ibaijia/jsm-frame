<#setting number_format="#"/>
package ${restReqPackageName};
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.context.dao.model.Page;

/**
 * @author jsm_auto_gen
 */
public class ${className}PageReq<T> extends Page<T> {
    private static final long serialVersionUID = 1L;

    @FieldAnn(required = false, maxLen = 100, comments = "关键字")
    public String keyword;

    public String getKeyword() {
    return keyword;
    }

    public void setKeyword(String keyword) {
    this.keyword = keyword;
    }
}