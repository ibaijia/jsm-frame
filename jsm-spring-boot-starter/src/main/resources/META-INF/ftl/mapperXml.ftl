<#setting number_format="#"/>
<?xml version="1.0" encoding="UTF-8"?>  
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://www.mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${mapperPackageName}.${className}Mapper">

    <!-- 基础方法 以注解的加在${mapperPackageName}.${className}Mapper里 -->
    <!-- 其它业务方法加到这 xml里更方便 -->

	<!-- 分页查询 -->
	<select id="pageList" resultType="${className}" parameterType="Page">
	    select * from ${tableName} where 1=1
	    <!--
		<if test="keyword != null and keyword != ''">
			and `name` like concat(concat('%',${r'#{'}keyword${r'}'}),'%')
		</if>
		 -->
		order by id desc
	</select>

	<#if genApi?exists && genApi>
    <select id="pageListVo" resultType="${className}Vo" parameterType="Page">
        select * from ${tableName} where 1=1
        <!--
		<if test="keyword != null and keyword != ''">
			and `name` like concat(concat('%',${r'#{'}keyword${r'}'}),'%')
		</if>
		 -->
        order by id desc
    </select>
	</#if>

</mapper>