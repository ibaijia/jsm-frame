<#setting number_format="#"/>
package ${restVoPackageName};
<#if hasDate?exists>
import java.util.Date;
import cn.ibaijia.jsm.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
import cn.ibaijia.jsm.context.rest.validate.ValidateModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author jsm_auto_gen
 */
public class ${className}Vo implements ValidateModel  {
    private static final long serialVersionUID = 1L;

    @FieldAnn(required = true, comments = "记录ID")
    public Long id;

	<#list fieldList as item>
	/**
	 * defaultVal:${item.defaultVal?default("")}
	 * comments:${item.comments?default("")}
	 */
    <#if item.fieldType == "String">
    @FieldAnn(required = ${item.required?string('true', 'false')}, maxLen = ${item.maxLength?default("1")}, comments = "${item.comments?default("")}")
    <#else>
    @FieldAnn(required = ${item.required?string('true', 'false')}, comments = "${item.comments?default("")?replace("\r\n","")}")
    </#if>
    <#if item.fieldType == "Date">
    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    </#if>
	public ${item.fieldType?default("")} ${item.fieldName?default("")};
	</#list>
}