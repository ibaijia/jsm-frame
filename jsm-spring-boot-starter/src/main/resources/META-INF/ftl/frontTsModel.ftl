<#setting number_format="#"/>
<#if tsTypeInfoList??>
    <#list tsTypeInfoList as item>
        <#if item.fieldInfoList??>
/**
 * @description ${item.typeName}
 <#list item.fieldInfoList as field>
 * @param ${field.fieldName},${field.tsFieldType},${field.required?string('是', '否')},${field.comments?default("")},${field.defaultVal?default("")}
 </#list>
 */
export type ${item.typeName} = {
 <#list item.fieldInfoList as field>
    ${field.fieldName}${field.required?string('', '?')}: ${field.tsFieldType}
 </#list>
}
    </#if>
    <#if item.arrayElementName??>
/**
 * @description ${item.arrayElementName}数组
 */
export type ${item.typeName} = ${item.arrayElementName}[]
    </#if>
</#list>
</#if>
