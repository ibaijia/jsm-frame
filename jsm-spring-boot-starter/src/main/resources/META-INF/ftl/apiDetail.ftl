<#setting number_format="#"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>API 详情</title>
    <style>
         #app {
            min-width:800px;
         }
         #app .an {
            margin-top: 16px;
            display: block;
            background: rgba(97,175,254,.2);
            font-weight: bold;
        }

         #app table {
            border-collapse: collapse;
            margin-top: 6px;
            width:100%;
        }

         #app table td,  #app table th {
            border: 1px solid #cad9ea;
            color: #666;
            height: 30px;
        }

         #app table th {
            background: rgba(97,175,254,.3);
            min-width: 50px;
        }

         #app table tr:nth-child(odd) {
            background: #fff;
        }

         #app table tr:nth-child(even) {
            background: rgba(97,175,254,.1);
        }

        #app a {
            display: block;
        }

         #app .tags > a {
            color: blue;
            cursor: pointer;
        }

         #app .tags > a:link {
            color: #06F;
            text-decoration: none;
        }

         #app .tags > a:visited {
            color: #999;
            text-decoration: line-through;
        }

         #app .tags > a:hover {
            color: #F00;
            text-decoration: underline;
        }

         #app .tags > a:active {
            color: #F0F;
        }

         #app .l1 {
            padding-left: 16px;
        }

         #app .l2 {
            padding-left: 28px;
        }

         #app .l3 {
            padding-left: 48px;
        }

         #app .l4 {
            padding-left: 68px;
        }

         #app .l5 {
            padding-left: 88px;
        }

         #app .l6 {
            padding-left: 108px;
        }
        #app .max60 {
            width:60px;
        }
        #app .max200 {
            width:200px;
        }
    </style>
</head>
<body>
<div id="app">
    <#if controllerInfo.apiInfos??>
        <#list controllerInfo.apiInfos as apiInfo>
            <a class="an" name="${controllerInfo.name}${apiInfo.name}Using${apiInfo.httpMethod?upper_case}">${apiInfo.httpMethod?upper_case} ${apiInfo.url} ${apiInfo.comments}</a>
            <#if apiInfo.pathVarList??>
                <table>
                    <tr>
                        <th class="max200" @click="controllerInfo.apiInfos[${apiInfo_index}].pathVarListShow = !controllerInfo.apiInfos[${apiInfo_index}].pathVarListShow">
                            <span>
                                {{controllerInfo.apiInfos[${apiInfo_index}].pathVarListShow ? '-' : '+'}}
                            </span>
                            请求Path
                        </th>
                        <th class="max60">必填</th>
                        <th class="max200">类型</th>
                        <th class="max60">长度</th>
                        <th>字段说明</th>
                    </tr>
                    <template v-if="controllerInfo.apiInfos[${apiInfo_index}].pathVarListShow">
                        <tr v-for="param in controllerInfo.apiInfos[${apiInfo_index}].pathVarList">
                            <td>{{param.fieldName}}</td>
                            <td>{{param.required ? '是' : '否'}}</td>
                            <td>{{param.fieldType}}</td>
                            <td>{{param.maxLength}}</td>
                            <td>{{param.comments}}</td>
                        </tr>
                    </template>
                </table>
            </#if>
            <#if apiInfo.reqParamList??>
                <table>
                    <tr>
                        <th class="max200" @click="controllerInfo.apiInfos[${apiInfo_index}].reqParamListShow = !controllerInfo.apiInfos[${apiInfo_index}].reqParamListShow">
                            <span>
                                {{controllerInfo.apiInfos[${apiInfo_index}].reqParamListShow ? '-' : '+'}}
                            </span>
                            请求Param
                        </th>
                        <th class="max60">必填</th>
                        <th class="max200">类型</th>
                        <th class="max60">长度</th>
                        <th>字段说明</th>
                    </tr>
                    <template v-if="controllerInfo.apiInfos[${apiInfo_index}].reqParamListShow">
                        <tr v-for="param in controllerInfo.apiInfos[${apiInfo_index}].reqParamList">
                            <td>{{param.fieldName}}</td>
                            <td>{{param.required ? '是' : '否'}}</td>
                            <td>{{param.fieldType}}</td>
                            <td>{{param.maxLength}}</td>
                            <td>{{param.comments}}</td>
                        </tr>
                    </template>
                </table>
            </#if>
            <#if apiInfo.bodyParamList??>
                <table>
                    <tr>
                        <th class="max200" @click="controllerInfo.apiInfos[${apiInfo_index}].bodyParamListShow = !controllerInfo.apiInfos[${apiInfo_index}].bodyParamListShow">
                           <span>
                                {{controllerInfo.apiInfos[${apiInfo_index}].bodyParamListShow ? '-' : '+'}}
                           </span>
                            请求Body
                        </th>
                        <th class="max60">必填</th>
                        <th class="max200">类型</th>
                        <th class="max60">长度</th>
                        <th>字段说明</th>
                    </tr>
                    <template v-if="controllerInfo.apiInfos[${apiInfo_index}].bodyParamListShow">
                        <template v-for="param in controllerInfo.apiInfos[${apiInfo_index}].bodyParamList">
                            <tr>
                                <td>
                                    <template v-if="param.fieldInfoList != null && param.fieldInfoList.length > 0">
                                        <span @click="param.show=!param.show">{{param.show ? '-' : '+'}}</span>
                                    </template>
                                    <template v-else>
                                        &nbsp;
                                    </template>
                                    {{param.fieldName}}
                                </td>
                                <td>{{param.required ? '是' : '否'}}</td>
                                <td>{{param.fieldType}}</td>
                                <td>{{param.maxLength}}</td>
                                <td>{{param.comments}}</td>
                            </tr>
                            <template v-if="param.show" v-for="param1 in param.fieldInfoList">
                                <tr>
                                    <td class="l1">
                                        <template v-if="param1.fieldInfoList != null && param1.fieldInfoList.length > 0">
                                            <span @click="param1.show=!param1.show">{{param1.show ? '-' : '+'}}</span>
                                        </template>
                                        <template v-else>
                                            &nbsp;
                                        </template>
                                        {{param1.fieldName}}
                                    </td>
                                    <td>{{param1.required ? '是' : '否'}}</td>
                                    <td>{{param1.fieldType}}</td>
                                    <td>{{param1.maxLength}}</td>
                                    <td>{{param1.comments}}</td>
                                </tr>
                                <template v-if="param1.show" v-for="param2 in param1.fieldInfoList">
                                    <tr>
                                        <td class="l2">
                                            <template v-if="param2.fieldInfoList != null && param2.fieldInfoList.length > 0">
                                                <span @click="param2.show=!param2.show">{{param2.show ? '-' : '+'}}</span>
                                            </template>
                                            <template v-else>
                                                &nbsp;
                                            </template>
                                            {{param2.fieldName}}
                                        </td>
                                        <td>{{param2.required ? '是' : '否'}}</td>
                                        <td>{{param2.fieldType}}</td>
                                        <td>{{param2.maxLength}}</td>
                                        <td>{{param2.comments}}</td>
                                    </tr>
                                    <template v-if="param2.show" v-for="param3 in param2.fieldInfoList">
                                        <tr>
                                            <td class="l3">
                                                &nbsp;{{param3.fieldName}}
                                            </td>
                                            <td>{{param3.required ? '是' : '否'}}</td>
                                            <td>{{param3.fieldType}}</td>
                                            <td>{{param3.maxLength}}</td>
                                            <td>{{param3.comments}}</td>
                                        </tr>
                                        <template v-if="param3.show" v-for="param4 in param3.fieldInfoList">
                                            <tr>
                                                <td class="l4">
                                                    &nbsp;{{param4.fieldName}}
                                                </td>
                                                <td>{{param4.required ? '是' : '否'}}</td>
                                                <td>{{param4.fieldType}}</td>
                                                <td>{{param4.maxLength}}</td>
                                                <td>{{param4.comments}}</td>
                                            </tr>
                                            <template v-if="param4.show" v-for="param5 in param4.fieldInfoList">
                                                <tr>
                                                    <td class="l5">
                                                        &nbsp;{{param5.fieldName}}
                                                    </td>
                                                    <td>{{param5.required ? '是' : '否'}}</td>
                                                    <td>{{param5.fieldType}}</td>
                                                    <td>{{param5.maxLength}}</td>
                                                    <td>{{param5.comments}}</td>
                                                </tr>
                                                <template v-if="param5.show" v-for="param6 in param5.fieldInfoList">
                                                    <tr>
                                                        <td class="l6">
                                                            &nbsp;{{param6.fieldName}}
                                                        </td>
                                                        <td>{{param6.required ? '是' : '否'}}</td>
                                                        <td>{{param6.fieldType}}</td>
                                                        <td>{{param6.maxLength}}</td>
                                                        <td>{{param6.comments}}</td>
                                                    </tr>
                                                </template>
                                            </template>
                                        </template>
                                    </template>
                                </template>
                            </template>
                        </template>
                    </template>
                </table>
            </#if>

            <#if apiInfo.respParamList??>
                <table>
                    <tr>
                        <th class="max200" @click="controllerInfo.apiInfos[${apiInfo_index}].respParamListShow = !controllerInfo.apiInfos[${apiInfo_index}].respParamListShow  ">
                          <span>
                            {{controllerInfo.apiInfos[${apiInfo_index}].respParamListShow ? '-' : '+'}}
                          </span>
                           响应
                        </th>
                        <th class="max60">必填</th>
                        <th class="max200">类型</th>
                        <th class="max60">长度</th>
                        <th>字段说明</th>
                    </tr>
                    <template v-if="controllerInfo.apiInfos[${apiInfo_index}].respParamListShow">
                        <template v-for="param in controllerInfo.apiInfos[${apiInfo_index}].respParamList">
                            <tr>
                                <td>
                                    <template v-if="param.fieldInfoList != null && param.fieldInfoList.length > 0">
                                        <span @click="param.show=!param.show">{{param.show ? '-' : '+'}}</span>
                                    </template>
                                    <template v-else>
                                        &nbsp;
                                    </template>
                                    {{param.fieldName}}
                                </td>
                                <td>{{param.required ? '是' : '否'}}</td>
                                <td>{{param.fieldType}}</td>
                                <td>{{param.maxLength}}</td>
                                <td>{{param.comments}}</td>
                            </tr>
                            <template v-if="param.show" v-for="param1 in param.fieldInfoList">
                                <tr>
                                    <td class="l1">
                                        <template v-if="param1.fieldInfoList != null && param1.fieldInfoList.length > 0">
                                            <span @click="param1.show=!param1.show">{{param1.show ? '-' : '+'}}</span>
                                        </template>
                                        <template v-else>
                                            &nbsp;
                                        </template>
                                        {{param1.fieldName}}
                                    </td>
                                    <td>{{param1.required ? '是' : '否'}}</td>
                                    <td>{{param1.fieldType}}</td>
                                    <td>{{param1.maxLength}}</td>
                                    <td>{{param1.comments}}</td>
                                </tr>
                                <template v-if="param1.show" v-for="param2 in param1.fieldInfoList">
                                    <tr>
                                        <td class="l2">
                                            <template v-if="param2.fieldInfoList != null && param2.fieldInfoList.length > 0">
                                                <span @click="param2.show=!param2.show">{{param2.show ? '-' : '+'}}</span>
                                            </template>
                                            <template v-else>
                                                &nbsp;
                                            </template>
                                            {{param2.fieldName}}
                                        </td>
                                        <td>{{param2.required ? '是' : '否'}}</td>
                                        <td>{{param2.fieldType}}</td>
                                        <td>{{param2.maxLength}}</td>
                                        <td>{{param2.comments}}</td>
                                    </tr>
                                    <template v-if="param2.show != null" v-for="param3 in param2.fieldInfoList">
                                        <tr>
                                            <td class="l3">
                                                &nbsp;{{param3.fieldName}}
                                            </td>
                                            <td>{{param3.required ? '是' : '否'}}</td>
                                            <td>{{param3.fieldType}}</td>
                                            <td>{{param3.maxLength}}</td>
                                            <td>{{param3.comments}}</td>
                                        </tr>

                                        <template v-if="param3.show != null" v-for="param4 in param3.fieldInfoList">
                                            <tr>
                                                <td class="l4">
                                                    &nbsp;{{param4.fieldName}}
                                                </td>
                                                <td>{{param4.required ? '是' : '否'}}</td>
                                                <td>{{param4.fieldType}}</td>
                                                <td>{{param4.maxLength}}</td>
                                                <td>{{param4.comments}}</td>
                                            </tr>

                                            <template v-if="param4.show != null" v-for="param5 in param4.fieldInfoList">
                                                <tr>
                                                    <td class="l5">
                                                        &nbsp;{{param5.fieldName}}
                                                    </td>
                                                    <td>{{param5.required ? '是' : '否'}}</td>
                                                    <td>{{param5.fieldType}}</td>
                                                    <td>{{param5.maxLength}}</td>
                                                    <td>{{param5.comments}}</td>
                                                </tr>

                                                <template v-if="param5.show != null" v-for="param6 in param5.fieldInfoList">
                                                    <tr>
                                                        <td class="l6">
                                                            &nbsp;{{param6.fieldName}}
                                                        </td>
                                                        <td>{{param6.required ? '是' : '否'}}</td>
                                                        <td>{{param6.fieldType}}</td>
                                                        <td>{{param6.maxLength}}</td>
                                                        <td>{{param6.comments}}</td>
                                                    </tr>
                                                </template>
                                            </template>
                                        </template>
                                    </template>
                                </template>
                            </template>
                        </template>
                    </template>
                </table>
            </#if>
        </#list>
    </#if>

</div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: "#app",
        data: {
            controllerInfo: ${controllerInfoJson}
        },
        methods: {
        }
    });
</script>
</html>