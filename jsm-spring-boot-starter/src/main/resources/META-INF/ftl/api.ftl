<#setting number_format="#"/>
package ${restPackageName};

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.context.rest.BaseRest;
import cn.ibaijia.jsm.context.rest.resp.RestResp;
import ${servicePackageName}.${className}Service;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import javax.annotation.Resource;
import ${restReqPackageName}.*;
import ${restVoPackageName}.*;
import ${modelPackageName}.${className};

/**
 * @author jsm_auto_gen
 */
@RestController
@RequestMapping("/v1/${apiName}")
public class ${className}Api extends BaseRest {

    @Resource
    private ${className}Service ${className?uncap_first}Service;

    @ApiOperation(value = "${className}分页列表")
    @GetMapping(value = "/page-list")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<${className}PageReq<${className}Vo>> pageList(${className}PageReq<${className}Vo> req) {
        ${className?uncap_first}Service.pageList(req);
        return success(req);
    }

    @ApiOperation(value = "${className}新增")
    @PostMapping(value = "/add")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Boolean> add(@RequestBody ${className}AddReq req) {
        int result = ${className?uncap_first}Service.add(req);
        return success(result > 0);
    }

    @ApiOperation(value = "${className}详情")
    @GetMapping(value = "/{id}")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
    public RestResp<${className}> findById(@PathVariable("id") Long id) {
        ${className} result = ${className?uncap_first}Service.findById(id);
        return success(result);
    }

    @ApiOperation(value = "${className}修改")
    @PutMapping(value = "/update")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Boolean> update(@RequestBody ${className}UpdateReq req) {
        int result = ${className?uncap_first}Service.update(req);
        return success(result > 0);
    }

    @ApiOperation(value = "${className}删除")
    @DeleteMapping(value = "/delete/{id}")
    @RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
    public RestResp<Boolean> deleteById(@PathVariable("id") Long id) {
        int result = ${className?uncap_first}Service.deleteById(id);
        return success(result > 0);
    }

}