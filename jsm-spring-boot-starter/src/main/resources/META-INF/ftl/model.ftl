<#setting number_format="#"/>
package ${modelPackageName};
<#if hasDate?exists>
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibaijia.jsm.utils.DateUtil;
</#if>
import cn.ibaijia.jsm.context.dao.BaseModel;
import cn.ibaijia.jsm.annotation.FieldAnn;

/**
 * @author jsm_auto_gen
 * tableName:${tableName}
 */
public class ${className} extends BaseModel {
    private static final long serialVersionUID = 1L;

	<#list fieldList as item>
	/**
	 * defaultVal:${item.defaultVal?default("")}
	 * comments:${item.comments?default("")}
	 */
	<#if item.fieldType == "String">
    @FieldAnn(required = ${item.required?string('true', 'false')}, maxLen = ${item.maxLength?default("1")}, comments = "${item.comments?default("")}")
    <#else>
    @FieldAnn(required = ${item.required?string('true', 'false')}, comments = "${item.comments?default("")?replace("\r\n","")}")
    </#if>
    <#if item.fieldType == "Date">
    @JsonFormat(pattern = DateUtil.DATETIME_PATTERN)
    </#if>
	public ${item.fieldType?default("")} ${item.fieldName?default("")};
	</#list>
}