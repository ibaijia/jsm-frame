<#setting number_format='#'/>
package ${mapperPackageName};

import org.apache.ibatis.annotations.*;
import java.util.List;
import ${modelPackageName}.${className};
import cn.ibaijia.jsm.context.dao.model.Page;
<#if genApi?exists && genApi>
import ${restVoPackageName}.*;
</#if>

/**
 * @author jsm_auto_gen
 * tableName:${tableName}
 */
public interface ${className}Mapper {

    /**
	 * 新增${className}
	 * @param ${className?uncap_first}
	 * @return 影响行
	 */
    @Insert("<script>insert into ${tableName} (<#list fieldList as item><#if item_index gt 0>,</#if>${item.fieldName?default('')}</#list>) values (<#list fieldList as item><#if item_index gt 0>,</#if>${r'#{'}${item.fieldName?default('')}${r'}'}</#list>)</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(${className} ${className?uncap_first});

    /**
	 * 批量新增${className}
     * @param ${className?uncap_first}List
     * @return 影响行
	 */
    @Insert("<script>insert into ${tableName} (<#list fieldList as item><#if item_index gt 0>,</#if>${item.fieldName?default('')}</#list>) values <foreach collection='list' item='item' index='index' separator=','> (<#list fieldList as item><#if item_index gt 0>,</#if>${r'#{item.'}${item.fieldName?default('')}${r'}'}</#list>)</foreach></script>")
	int addBatch(List<${className}> ${className?uncap_first}List);

	/**
	 * 修改${className}(配合findByIdForUpdate更佳)
     * @param ${className?uncap_first}
     * @return 影响行
	 */
    @Update("<script>update ${tableName} set id=${r'#{id}'}<#list fieldList as item><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.${item.fieldName?default('')},${item.fieldName?default('')})'>,${item.fieldName?default('')}=${r'#{'}${item.fieldName?default('')}${r'}'}</if></#list> where id=${r'#{id}'}</script>")
	int update(${className} ${className?uncap_first}); 
	
	/**
	 * 查询ById
     * @param id
     * @return ${className}
     */
    @Select("<script>select t.* from ${tableName} t where t.id=${r'#{id}'}</script>")
	${className} findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
     * @param id
     * @return ${className}
     */
    @Select("<script>select t.* from ${tableName} t where t.id=${r'#{id}'}</script>")
	${className} findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
     * @param id
     * @return 影响行
     */
    @Delete("<script>delete from ${tableName} where id=${r'#{id}'}</script>")
	int deleteById(@Param("id") Long id);
	
	/**
	 * 查询出所有（谨用）
     * @return List<${className}>
     */
    @Select("<script>select t.* from ${tableName} t order by t.id desc</script>")
	List<${className}> listAll();
	
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 * @param page
     * @return List<${className}> 返回值一般无需接收，已经注入到page.list
     */
	List<${className}> pageList(Page<${className}> page);
    <#if genApi?exists && genApi>
	/**
	 * 分页查询（PageReq对象新增查询条件）
	 * @param page
     * @return List<${className}Vo>  返回值一般无需接收，已经注入到page.list
     */
	List<${className}Vo> pageListVo(Page<${className}Vo> page);
    </#if>
}