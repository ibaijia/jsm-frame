#!/usr/bin/env node

/**
 * @author longzhili2005@126.com
 */

"use strict";

function onFatalError(error) {
    process.exitCode = 2;
    console.error(error);
}

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------

(async function main() {
    //    console.log(process.argv)
    if (process.argv.includes("init")) {
        let init = require("../lib/init.js")
//        console.log(init)
        init(process.argv.includes("--with-ajax-lib"))
    } else if (process.argv.includes("genAjaxLib")) {
        let gen = require("../lib/gen.js")
        gen.genAjaxLib()
    } else {
        let gen = require("../lib/gen.js")
//        console.log(gen)
        gen.genApi()
    }
}()).catch(onFatalError);
