const fs = require('fs')
const path = require("path")

const tpl = require("../tpl/tpl") 

const configJson = {
    "apiDir": "src/api",
    "apiModelDir": "./model",
    "ajaxLibPath": "../lib/AjaxUtil",
    "ajaxLibName": "ajax",
    "override": true,
    "lang": "ts",
    "apis": [
        {
            "api": "http://localhost:8080/base-api",
            "dir": "base-api",
            "baseUri": "base-api"
        }
    ]
}

const init = (withAjaxLib) => {
    let name = "jsm.config.json"
    if (!fileExists(name)) {
        let content = JSON.stringify(configJson, null, 2)
        fs.writeFile(name, content, "utf8", function (err) {
            if (err) {
                return console.log('jsm init error:' + err.message)
            }
            console.log('jsm init ok, see: ' + name)
        })
    }
    if (withAjaxLib) {
        tpl.copyLibFiles(configJson)
    }
}

const fileExists = (filename) => {
    return fs.existsSync(filename)
}

const mkdirsSync = (dirname) => {
    if (fs.existsSync(dirname)) {
        return true
    } else {
        if (mkdirsSync(path.dirname(dirname))) {
            fs.mkdirSync(dirname);
            return true
        }
    }
}

module.exports = init