const fs = require('fs')
const url = require('url')
const path = require("path")
const tpl = require("../tpl/tpl")

var config = null

const genAjaxLib = () => {
    config = loadConfig()
    tpl.copyLibFiles(config)
}

const genApi = () => {
    config = loadConfig()
    //    console.log(config)
    if (config && config.apis) {
        config.apis.forEach(api => {
            getApiNames(createApis, api)
        })
    }
}

const createApis = (apiNames, api) => {
    //    console.log(apiNames)
    if (apiNames) {
        apiNames.forEach(apiName => {
            genApiAndModelFile(apiName, api)
        })
    }
}

const genApiAndModelFile = (apiName, api) => {
    let config = loadConfig()
    let params = {
        "lang": config.lang,
        "apiName": apiName,
        "apiModelDir": config.apiModelDir,
        "ajaxLibName": config.ajaxLibName,
        "ajaxLibPath": config.ajaxLibPath,
        "baseUri": api.baseUri,
        "fileType": "api"
    }
    let dir = config.apiDir + "/" + api.dir
    mkdirsSync(dir)
    let apiFileName = dir + "/" + apiName + "." + config.lang
    genApiFile(url, params, apiFileName, api)

    if(config.lang == 'ts'){
        params.fileType = "model"
        dir = config.apiDir + "/" + api.dir + "/" + config.apiModelDir
        mkdirsSync(dir)
        let apiModelFileName = dir + "/" + apiName + "Model." + config.lang
        genApiFile(url, params, apiModelFileName, api)
    }

}

const mkdirsSync = (dirname) => {
    if (fs.existsSync(dirname)) {
        return true;
    } else {
        if (mkdirsSync(path.dirname(dirname))) {
            fs.mkdirSync(dirname);
            return true;
        }
    }
}

const genApiFile = (url, params, apiFileName, api) => {
    let paramData = JSON.stringify(params)
    let http = getHttpByUrl(api.api)
    let urlOptions = url.parse(api.api + "/v1/swagger-api-script")
    let options = {
        method: 'POST',
        hostname: urlOptions.hostname,
        port: urlOptions.port ? urlOptions.port : '80',
        path: urlOptions.path,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': paramData.length
        }
    }
    let req = http.request(options, res => {
        let data = '';
        res.on('data', chunk => {
            data += chunk;
        });
        res.on('end', () => {
            writeToFile(apiFileName, data);
        });
    }).on('error', err => {
        console.log('Error: ', err.message);
    })
    req.write(paramData + "\n");
    req.end();
}

const writeToFile = (filename, content) => {
    // console.log("fileExists:", fileExists(filename), !config.override)
    if (!config.override && fileExists(filename)) {
        console.log('createFile exists, ignore: ' + filename)
        return
    }
    fs.writeFile(filename, content, "utf8", function (err) {
        if (err) {
            return console.log('createFile ' + filename + ', error:' + err.message)
        }
        console.log('createFile ok.: ' + filename)
    })
}

const loadConfig = () => {
    try {
        let config = require(path.resolve('jsm.config.json'))
        return config
    } catch (err) {
        console.error("please run [jsm init] first.")
    }
}

const getApiNames = (callback, api) => {
    let url = api.api + "/v1/swagger-api-names"
    let http = getHttpByUrl(api.api)
    http.get(url, res => {
        let data = '';
        res.on('data', chunk => {
            data += chunk;
        });
        res.on('end', () => {
            if (data) {
                callback(JSON.parse(data), api);
            } else {
                console.log("swagger maybe disable.", api.api)
            }
        });
    }).on('error', err => {
        console.log('Error: ', err.message);
    })
}

const getHttpByUrl = (url) => {
    return url.startsWith("https://") ? require('https') : require('http')
}

const getApiScript = (api) => {
    let url = api.api + "/v1/swagger-api-script"
}

const fileExists = (filename) => {
    return fs.existsSync(filename)
}

module.exports = { genApi, genAjaxLib }