export default {
    showLoading() {
        // TODO 这里根据自己引入的UI框架修改实现
        console.log("showLoading")
    },
    hideLoading() {
        // TODO 这里根据自己引入的UI框架修改实现
        console.log("hideLoading")
    },
    showErrorMessage(msg) {
        // TODO 这里根据自己引入的UI框架修改实现
        alert(msg)
    },
    showInfoMessage(msg) {
       // TODO 这里根据自己引入的UI框架修改实现
       alert(msg)
    },
    showMessage(code, msg) {
        if (code == '1001') {
            this.showInfoMessage(msg)
        } else {
            this.showErrorMessage(msg)
        }
    },
    gotoLogin() {
        // TODO 这里根据自己业务实现跳转到登录页面的效果
    },
    useJsmMock() {
        // TODO 这里根据自己的环境修改，通常是取环境变更
        return false
    },
}