const fs = require('fs')
const path = require("path")

const copyLibFiles = (config) => {
    let destDir = config.apiDir + "/lib"
    mkdirsSync(destDir)
    let sourceFile = path.resolve(__dirname + "/AjaxUtil.js")
    let destFile =  path.resolve(destDir + "/AjaxUtil.js")
    if (!fileExists(destFile)) {
        copyFile(sourceFile, destFile)
    }
    sourceFile = path.resolve(__dirname + "/AjaxConfig.js")
    destFile =  path.resolve(destDir + "/AjaxConfig.js")
    if (!fileExists(destFile)) {
        copyFile(sourceFile, destFile)
    }
}

const fileExists = (filename) => {
    return fs.existsSync(filename)
}

const mkdirsSync = (dirname) => {
    if (fs.existsSync(dirname)) {
        return true
    } else {
        if (mkdirsSync(path.dirname(dirname))) {
            fs.mkdirSync(dirname);
            return true
        }
    }
}

const copyFile = (sourcePath, destPath) => {
    // console.log(sourcePath, destPath)
    const extName = path.extname(sourcePath)
    const fileName = path.basename(sourcePath)
    const sourceDir = path.dirname(sourcePath)
    const destDir = path.dirname(destPath)
    fs.readFile(sourcePath, (err, data) => {
        fs.writeFile(`${destDir}/${fileName}`, data, err => {
            if (err) {
                console.log(`createFile error. ${destPath}`)
            } else {
                console.log(`createFile ok. ${destPath}`)
            }
        })
    })
}

module.exports = {copyLibFiles}