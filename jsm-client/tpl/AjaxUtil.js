import axios from "axios"
import AjaxConfig from './AjaxConfig'
import { Base64 } from 'js-base64'
import { md5 } from 'js-md5'

const _ajax = Symbol("_ajax")
// 默认超时 60s
const timeout = 60 * 1000
// 上传超时 1h
const uploadTimeout = 1 * 60 * 60 * 1000
// 默认头信息 headers
const headers = {}
// 上传头信息 uploadHeaders
const uploadHeaders = {
    'Content-Type': 'multipart/form-data'
}
const formHeaders = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

const instance = axios.create({
    timeout: timeout
})

/** 去掉空值 */
const removeEmptyValue = (obj) => {
    if (obj instanceof FormData) {
        return obj
    }
    let newObject = {}
    Object.keys(obj).forEach(key => {
        if (obj[key] != undefined && obj[key] != null) {
            newObject[key] = obj[key]
        }
    })
    return newObject
}

const getJsmWebAuthHeaders = () => {
    let encToken = sessionStorage.getItem("token") || localStorage.getItem("token")
    let timeDiff = sessionStorage.getItem("timeDiff") || localStorage.getItem("timeDiff")
    if (encToken) {
        let token = Base64.decode(encToken)
        let ajaxDate = new Date().getTime()
        let serverTime = ajaxDate + Number(timeDiff)
        let baseString = token + '_' + serverTime
        let at = Base64.encode(baseString)
        let ht = md5(token + '_' + serverTime).toUpperCase()
        return { "at": at, "ht": ht }
    }
    return {}
}

const getOauthAccessTokenHeader = () => {
    let accessToken = sessionStorage.getItem("accessToken") || localStorage.getItem("accessToken")
    if (accessToken) {
        return { "accessToken": accessToken }
    }
    return {}
}

const parseQueryString = (params) => {
    let queryString = ''
    Object.keys(params).forEach((key) => {
        if (params[key] != undefined && params[key] != null) {
            queryString += (queryString ? "&" : "?")
            queryString += (encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
        }
    })
    return queryString
}

const parseObjectToFormData = (params) => {
    if (params instanceof Array) {
        console.log("parseObjectToFormData but params is array.")
        return null
    }
    const formData = new FormData()
    if (params instanceof File) {
        formData.append("file", params)
    } else {
        for (let [key, value] of Object.entries(params)) {
            formData.append(key, value)
        }
    }
    return formData
}

const isFileOrContainsFile = (params) => {
    if (params instanceof Array) {
        return false
    }
    if (params instanceof FormData) {
        return false
    }
    if (params instanceof File) {
        return true
    }
    for (let value of Object.values(params)) {
        if (value instanceof File) {
            return true
        }
    }
    return false
}

const isFormDataAndFileUpload = (params) => {
    if (params instanceof FormData) {
        for (let value of formData.values()) {
            if (value instanceof File) {
                return true
            }
        }
    }
}

const isFormData = (params) => {
    return (params instanceof FormData)
}

instance.interceptors.request.use(function (config) {
    AjaxConfig.showLoading()
    let { url } = config
    Object.assign(config.headers, getJsmWebAuthHeaders())
    Object.assign(config.headers, getOauthAccessTokenHeader())
    console.log("AjaxConfig.useJsmMock():", AjaxConfig.useJsmMock())
    if (AjaxConfig.useJsmMock()) {
        if (url.includes("?")) {
            url += "&jsm-mock=1"
        } else {
            url += "?jsm-mock=1"
        }
        config.url = url
    }
    return config

}, function (error) {
    AjaxConfig.hideLoading()
    console.error("request error:", error)
    if (error.response && error.response.data && error.response.data.code) {
        AjaxConfig.showInfoMessage(error.response.data.code, error.response.data.message)
    } else {
        AjaxConfig.showErrorMessage('请求错误.')
    }
    return Promise.reject(error)
})

instance.interceptors.response.use(function (response) {
    AjaxConfig.hideLoading()
    return response
}, async function (error) {
    AjaxConfig.hideLoading()
    console.log("response error:", error)
    if (error.response.status === 401) {
        AjaxConfig.gotoLogin();
        return Promise.reject(new Error('401 去登录吧'))
    }
    return Promise.reject(error)
})

export default {
    async [_ajax](method, url, param, headers, timeout) {
        let config = {
            headers: headers,
            timeout: timeout
        }
        if (method === "get") {
            config.params = removeEmptyValue(param);
        } else if (method === "post" || method === "put" || method === "delete") {
            config.data = removeEmptyValue(param)
        }
        try {
            const response = await instance({
                method,
                url,
                ...config
            });
            let responseData = response.data
            let { code, message } = responseData
            let arr = ['1002', '1003', '1004', '1201']
            if (arr.indexOf(code) > -1) {
                AjaxConfig.showErrorMessage(message)
            }
            //登录过期 或 未登录
            if (code == '1201' || code == '1101') {
                AjaxConfig.gotoLogin()
            }
            return responseData
        } catch (error) {
            console.log(error)
            return error.response.data
        }
    },
    /**
     * POST 请求
     * @param {*} url
     * @param {*} param 通常是post是JSON数据，文件上传时应该为FormData类型
     * @returns
     */
    post(url, param = {}) {
        if (isFileOrContainsFile(param)) {
            let formData = parseObjectToFormData(param)
            return this[_ajax]('post', url, formData, uploadHeaders, uploadTimeout)
        }
        if (isFormDataAndFileUpload(param)) {
            return this[_ajax]('post', url, param, uploadHeaders, uploadTimeout)
        }
        if (isFormData(param)) {
            return this[_ajax]('post', url, param, formHeaders, uploadTimeout)
        }
        return this[_ajax]('post', url, param, headers, timeout)
    },
    /**
     * GET 请求
     * @param {*} url
     * @param {*} param
     * @returns
     */
    get(url, param = {}) {
        return this[_ajax]('get', url, param, headers, timeout)
    },
    /**
     * PUT 请求
     * @param {*} url
     * @param {*} param 通常是post是JSON数据，文件上传时应该为FormData类型
     * @returns
     */
    put(url, param = {}) {
        if (isFileOrContainsFile(param)) {
            let formData = parseObjectToFormData(param)
            return this[_ajax]('put', url, formData, uploadHeaders, uploadTimeout)
        }
        if (isFormDataAndFileUpload(param)) {
            return this[_ajax]('put', url, param, uploadHeaders, uploadTimeout)
        }
        if (isFormData(param)) {
            return this[_ajax]('put', url, param, formHeaders, timeout)
        }
        return this[_ajax]('put', url, param, headers, timeout)
    },
    /**
     * DELETE 请求
     * @param {*} url
     * @param {*} param
     * @returns
     */
    delete(url, param = {}) {
        return this[_ajax]('delete', url, param, headers, timeout)
    },
    parseQueryString(params) {
        return parseQueryString(params)
    },
    getJsmWebAuthHeaders() {
        return getJsmWebAuthHeaders()
    },
    getJsmWebAuthHeaderStr() {
        let params = getJsmWebAuthHeaders()
        return "at=" + params.at + "&ht=" + params.ht
    }
}
